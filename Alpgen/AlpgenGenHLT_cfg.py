import FWCore.ParameterSet.Config as cms
import random
import sys


process = cms.Process("HLT")

process.load("Configuration/StandardSequences/Services_cff")
# seed generation
seed1 = random.randint(0, 900000000)
seed2 = random.randint(0, 900000000)
seed3 = random.randint(0, 900000000)
seed4 = random.randint(0, 900000000)
print "Initial seeds for random number service:"
print "  generator  = ", seed1
print "  VtxSmeared = ", seed2
print "  g4SimHits  = ", seed3
print "  mix        = ", seed4
process.RandomNumberGeneratorService.generator.initialSeed  = seed1
process.RandomNumberGeneratorService.VtxSmeared.initialSeed = seed2
process.RandomNumberGeneratorService.g4SimHits.initialSeed  = seed3
process.RandomNumberGeneratorService.mix.initialSeed        = seed4

process.load("FWCore/MessageService/MessageLogger_cfi")

process.options = cms.untracked.PSet(
  wantSummary = cms.untracked.bool(False),
)

process.maxEvents = cms.untracked.PSet(
  input = cms.untracked.int32(100)
)

# generator level information
process.load("Configuration/StandardSequences/Generator_cff")

# Pythia settings
process.load("Configuration/Generator/PythiaUESettings_cfi")

# Input source
process.source = cms.Source("AlpgenSource",
  fileNames = cms.untracked.vstring('file:z2jee'),
  writeAlpgenWgtFile = cms.untracked.bool(False),
  skipEvents = cms.untracked.uint32(0)
)

# set run number:
frun = random.randint(0, 1000000)
print "Source initialization:"
print "  firstRun  = ", frun
process.source.firstRun = cms.untracked.uint32(frun)

# Generator settings:
process.generator = cms.EDFilter("Pythia6HadronizerFilter",
  pythiaHepMCVerbosity = cms.untracked.bool(False),
  maxEventsToPrint = cms.untracked.int32(0),
  pythiaPylistVerbosity = cms.untracked.int32(0),
  filterEfficiency = cms.untracked.double(1.0),
  comEnergy = cms.double(7000.0),
  PythiaParameters = cms.PSet(
    process.pythiaUESettingsBlock,
    lhesourcePars = cms.vstring(
      'MSEL=0                  ! User defined processes'
    ),
    parameterSets = cms.vstring('pythiaUESettings', 'lhesourcePars')
  )
)

process.load('Configuration/StandardSequences/GeometryDB_cff')
process.load('Configuration/StandardSequences/MagneticField_38T_cff')
process.load('IOMC/EventVertexGenerators/VtxSmearedRealistic7TeVCollision_cfi')

# Conditions data for calibration and alignment
# the following page should be checked for the latest global tags at any given time
# https://twiki.cern.ch/twiki/bin/view/CMS/SWGuideFrontierConditions
process.load("Configuration/StandardSequences/FrontierConditions_GlobalTag_cff")
process.GlobalTag.globaltag = 'START41_V0::All'

# Simulation and digitization
process.load("Configuration/StandardSequences/Simulation_cff")

# Pile-Up configuration
# general information:
#   https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideMixingModule
# pileup scenarios:
#   https://twiki.cern.ch/twiki/bin/view/CMS/PileupInformation
#
# No Pile-Up:
process.load("SimGeneral/MixingModule/mixNoPU_cfi")
# 2011 Pileup Scenario:
#process.load("SimGeneral/MixingModule/mix_E7TeV_FlatDist10_2011EarlyData_50ns_cfi")

process.load('Configuration/StandardSequences/EndOfProcess_cff')

process.load("Configuration/StandardSequences/SimL1Emulator_cff")
process.load("Configuration/StandardSequences/DigiToRaw_cff")

# run HLT
process.load('HLTrigger/Configuration/HLT_GRun_cff')

process.hltTrigReport = cms.EDAnalyzer("HLTrigReport",
  HLTriggerResults = cms.InputTag('TriggerResults', '', 'HLT')
)

# Output module
process.load('Configuration/EventContent/EventContent_cff')
process.output = cms.OutputModule("PoolOutputModule",
  process.FEVTDEBUGHLTEventContent,
  fileName = cms.untracked.string('hlt.root')
)

# Path and EndPath definitions
process.generator_step     = cms.Path(process.generator)
process.generation_step    = cms.Path(process.pgen)
# psim + pdigi
process.simulation_step    = cms.Path(process.simulation)
process.L1simulation_step  = cms.Path(process.SimL1Emulator)
process.digi2raw_step      = cms.Path(process.DigiToRaw)
process.hltTrigReport_step = cms.Path(process.hltTrigReport)
process.endjob_step        = cms.Path(process.endOfProcess)
process.out_step           = cms.EndPath(process.output)

# Schedule definition
process.schedule = cms.Schedule(process.generator_step,
                                process.generation_step,
                                process.simulation_step,
                                process.L1simulation_step,
                                process.digi2raw_step)
process.schedule.extend(process.HLTSchedule)
process.schedule.append(process.hltTrigReport_step)
process.schedule.extend([process.endjob_step,process.out_step])
