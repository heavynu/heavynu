
=====================================================================
  Visualization of events with Fireworks (Official CMSSW tool)
=====================================================================

  The Fireworks draws events from the .root file just from the first 
event in the file. You can draw the next events by clicking on the fat 
green arrow on the top left side of the Fireworks window. It looks like 
it is impossible to jump to the event you need. So you have to extract 
the needed event from the .root file.

  To draw a picture of one selected event, for example, the event with
run = 143827, lumi = 354, number = 496077002, you have to pass the 
following steps: 

1. You need the recent version of CMSSW correctly installed and go to
   ..../CMSSW_3_6_1_patch5/src/.....

2. Type 'cmsenv'. This command will set all needed CMSSW environment.
If you have any complains during processing of 'cmsenv' command you
have to check the definitions of your environment, for example, if you
redefined environmental variable SCRAM_ARCH you can get any funny results 
with CMSSW. When you enter your CMS account the variable SCRAM_ARCH
will be defined automatically!

3. You have to find this event in the .root files in DB (DataBase), type:

dbsql "find file where run=143827 and lumi=354 and dataset like *RECO*"   
 
You will have the number of files with these particular run number and 
lumi section (it is impossible to find the particular event, only 
corresponding run number and lumi section)

............................................................................ 
/store/data/Run2010A/HcalNZS/ALCARECO/v4/000/143/827/AE095E1F-E4AF-DF11-9BF4-0030487CD7CA.root
/store/data/Run2010A/EGMonitor/RECO/v4/000/143/827/D6D59F2D-E6AF-DF11-8D21-0030487CD6B4.root
/store/data/Run2010A/EGMonitor/RAW-RECO/v6/000/143/827/D46E3ACB-7AB0-DF11-A1F3-0015179EDB88.root
/store/data/Run2010A/EG/RECO/v4/000/143/827/66C76E9F-E2AF-DF11-A23C-001617C3B6CC.root
/store/data/Run2010A/EG/ALCARECO/v4/000/143/827/D46C5453-1CB0-DF11-B40F-003048D2C108.root
/store/data/Run2010A/Cosmics/RECO/v4/000/143/827/CEE23B7F-93B0-DF11-B87A-00215E20F7F4.root
..........................................................................

In our case we are looking for events skimmed for EG selection, so we
can try to use the following file
/store/data/Run2010A/EG/RECO/v4/000/143/827/66C76E9F-E2AF-DF11-A23C-001617C3B6CC.root

4. You have to checkout from CVS the 'EventNumberFilter' part of 
heavynu staff, or you can update your previous installation. This 
EventNumberFilter is taken from ECAL online software. Only arrangement
of directories and python file were modified.

    cd EventNumberFilter
    scramv1 b
    cd test

5. Edit file 'eventNumberSkim_cfg.py' to substitute
    a) the event numbers:
      InterestingEvents = cms.untracked.vint32(496077002)
    
    b) input .root file name:
      source.fileNames = ...

6. Start events extracting

   cmsRun eventNumberSkim_cfg.py > &output_log&

After some time you will have in this directory the output file Events.root.

7. Draw selected event with the Fireworks

   cmsShow Events.root

   Enjoy !

8. If you need to draw events with Fireworks on Windows's machines, using
Putty connection and Xming X server, then use Xming-mesa. This version
compatible with OpenGL/GLX components.     
=====================================================================

Usefull links:
  https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookFireworks
  https://cmsweb.cern.ch/filemover/
