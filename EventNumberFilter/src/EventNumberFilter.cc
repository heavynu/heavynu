#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDFilter.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include <iostream>
using namespace std;
using namespace edm;

// class declaration
class EventNumberFilter : public EDFilter {
  public:
    explicit EventNumberFilter(const ParameterSet&);
    ~EventNumberFilter();
    
  private:
    bool filter(Event&, const EventSetup&);
    
    // vector of interesting events:
    const vector<int> vie_;
};

// constructor
EventNumberFilter::EventNumberFilter(const ParameterSet& iConfig)
: vie_(iConfig.getUntrackedParameter<vector<int> >("InterestingEvents"))
{
  ostringstream oss;
  
  for (size_t i = 0; i < vie_.size(); ++i)
    oss << vie_[i] << " ";
  
  cout << "Events to select are: " << oss.str() << endl;
}

// destructor
EventNumberFilter::~EventNumberFilter() {}

// filter function
bool EventNumberFilter::filter(Event& iEvent, const EventSetup&)
{
  const int ievt = iEvent.id().event();
  const bool passed = (find(vie_.begin(), vie_.end(), ievt) != vie_.end());
  
  if (passed)
    cout << "Event number: " << ievt << " passed! " << endl;
  
  return passed;
}

//define this as a plug-in
DEFINE_FWK_MODULE(EventNumberFilter);
