import FWCore.ParameterSet.Config as cms

process = cms.Process("EventNumberSkim")

# decrease progress verbosity:
process.load("FWCore.MessageLogger.MessageLogger_cfi")
process.MessageLogger.cerr.FwkReport.reportEvery = 1000

# select events of interest:
process.eventNumberFilter = cms.EDFilter("EventNumberFilter",
  InterestingEvents = cms.untracked.vint32(68095, 68105)
)

# select input files:
process.source = cms.Source("PoolSource",
  fileNames = cms.untracked.vstring(
    'file:/afs/cern.ch/work/d/dtlisov/private/HeavyNu/CMSSW_5_3_7_patch2/src/heavynu/022630E3-D0EC-E111-8E23-00261894380A.root',
#    'file:~/scratch1/data/EEDAA411-5F7B-E011-A687-0025901D4C3E.root'
  )
)

# process all events from input files:
process.maxEvents = cms.untracked.PSet(
  input = cms.untracked.int32(-1)
)

# define output file:
process.out = cms.OutputModule("PoolOutputModule",
  fileName = cms.untracked.string('Events.root'),
  SelectEvents = cms.untracked.PSet(
    SelectEvents = cms.vstring('p')
  )
)

process.p = cms.Path(process.eventNumberFilter)
process.e = cms.EndPath(process.out)
