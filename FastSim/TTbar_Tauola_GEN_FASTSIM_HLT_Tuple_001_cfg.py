
# Auto generated configuration file
# using: 
# Revision: 1.207 
# Source: /cvs_server/repositories/CMSSW/CMSSW/Configuration/PyReleaseValidation/python/ConfigBuilder.py,v 
# with command line options: TTbar_Tauola_7TeV_cfi.py -s GEN,FASTSIM,HLT:GRun --pileup=NoPileUp --geometry DB --conditions=auto:mc --eventcontent=FEVTDEBUGHLT --datatier GEN-SIM-DIGI-RECO -n 10
import FWCore.ParameterSet.Config as cms

process = cms.Process('HLT')

# import of standard configurations
process.load('Configuration.StandardSequences.Services_cff')
process.load('SimGeneral.HepPDTESSource.pythiapdt_cfi')
process.load('FWCore.MessageService.MessageLogger_cfi')
process.load('FastSimulation.Configuration.RandomServiceInitialization_cff')
process.load('FastSimulation.PileUpProducer.PileUpSimulator7TeV_cfi')
process.load('FastSimulation.Configuration.FamosSequences_cff')
process.load('Configuration.StandardSequences.MagneticField_38T_cff')
process.load('Configuration.StandardSequences.Generator_cff')
process.load('FastSimulation.Configuration.FamosSequences_cff')
process.load('IOMC.EventVertexGenerators.VtxSmearedParameters_cfi')
process.load('FastSimulation.Configuration.HLT_GRun_cff')
process.load('FastSimulation.Configuration.CommonInputs_cff')
process.load('FastSimulation.Configuration.EventContent_cff')

process.configurationMetadata = cms.untracked.PSet(
    version = cms.untracked.string('$Revision: 1.1 $'),
    annotation = cms.untracked.string('TTbar_Tauola_7TeV_cfi.py nevts:10'),
    name = cms.untracked.string('PyReleaseValidation')
)
process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(100000)
)
process.MessageLogger.cerr.FwkReport.reportEvery = 25
process.options = cms.untracked.PSet(

)
# Input source
process.source = cms.Source("EmptySource",
    firstRun=cms.untracked.uint32(101),

                            )


process.RandomNumberGeneratorService.generator.initialSeed = cms.untracked.uint32(20010827)
process.RandomNumberGeneratorService.VtxSmeared.initialSeed = cms.untracked.uint32(20015386)
process.RandomNumberGeneratorService.famosPileUp.initialSeed = cms.untracked.uint32(20010224)
process.RandomNumberGeneratorService.famosSimHits.initialSeed = cms.untracked.uint32(20018180)
process.RandomNumberGeneratorService.siTrackerGaussianSmearingRecHits.initialSeed = cms.untracked.uint32(20015032)
process.RandomNumberGeneratorService.ecalRecHit.initialSeed = cms.untracked.uint32(20019720)
process.RandomNumberGeneratorService.ecalPreshowerRecHit.initialSeed = cms.untracked.uint32(20019773)
process.RandomNumberGeneratorService.hbhereco.initialSeed = cms.untracked.uint32(20016995)
process.RandomNumberGeneratorService.horeco.initialSeed = cms.untracked.uint32(20019015)
process.RandomNumberGeneratorService.hfreco.initialSeed = cms.untracked.uint32(20016190)
process.RandomNumberGeneratorService.paramMuons.initialSeed = cms.untracked.uint32(20014977)
process.RandomNumberGeneratorService.l1ParamMuons.initialSeed = cms.untracked.uint32(20010067)
process.RandomNumberGeneratorService.MuonSimHits.initialSeed = cms.untracked.uint32(20017383)
process.RandomNumberGeneratorService.simMuonRPCDigis.initialSeed = cms.untracked.uint32(20014923)
process.RandomNumberGeneratorService.simMuonCSCDigis.initialSeed = cms.untracked.uint32(20014044)
process.RandomNumberGeneratorService.simMuonDTDigis.initialSeed = cms.untracked.uint32(20018410)


# Additional output definition

# Other statements
process.famosPileUp.PileUpSimulator = process.PileUpSimulatorBlock.PileUpSimulator
process.famosPileUp.PileUpSimulator.averageNumber = 0
process.famosSimHits.SimulateCalorimetry = True
process.famosSimHits.SimulateTracking = True
process.simulation = cms.Sequence(process.simulationWithFamos)
process.HLTEndSequence = cms.Sequence(process.reconstructionWithFamos)

# set correct vertex smearing
process.Realistic7TeVCollisionVtxSmearingParameters.type = cms.string("BetaFunc")
process.famosSimHits.VertexGenerator = process.Realistic7TeVCollisionVtxSmearingParameters
process.famosPileUp.VertexGenerator = process.Realistic7TeVCollisionVtxSmearingParameters
# Apply Tracker and Muon misalignment
process.famosSimHits.ApplyAlignment = True
process.misalignedTrackerGeometry.applyAlignment = True

process.misalignedDTGeometry.applyAlignment = True
process.misalignedCSCGeometry.applyAlignment = True

process.GlobalTag.globaltag = 'MC_38Y_V10::All'

#process.load('Configuration/StandardSequences/GeometryExtended_cff')
process.load('Configuration/StandardSequences/MagneticField_38T_cff')

# PAT Layer 0+1
process.load("PhysicsTools/PatAlgos/patSequences_cff")

# cleaning
# 1st step
# select muons which pass relIso, pt and MuonID cut
process.cleanPatMuons.preselection      = "((0.75*ecalIso + 0.75*hcalIso + trackIso)/pt < 0.2) && isGood('TMLastStationTight')"
process.cleanPatMuons.checkOverlaps     = cms.PSet()
process.cleanPatMuons.finalCut          = "(pt > 4)"

# 2nd step
# select electrons which pass relIso, pt and ElectronID cut
process.cleanPatElectrons.preselection  = "((0.75*ecalIso + 0.75*hcalIso + trackIso)/pt < 0.2) && (electronID('eidRobustLoose') > 0)"
process.cleanPatElectrons.checkOverlaps = cms.PSet()
process.cleanPatElectrons.finalCut      = "(pt > 4)"

# 3rd step
# default photon cleaning used:
# throw away photons which share the same SuperCluster with cleanPatElectrons
# for more info follow to "PhysicsTools/PatAlgos/python/cleaningLayer1/photonCleaner_cfi.py"

# 4th step
# select jets which do not intersect with cleanPatMuons, cleanPatElectrons (in cone dR < 0.5) and pass pt cut
process.cleanPatJets.checkOverlaps.muons.requireNoOverlaps = True
process.cleanPatJets.checkOverlaps.electrons.requireNoOverlaps = True
process.cleanPatJets.finalCut           = "pt > 20"

# === PAT dump module ===
# dump module
process.patdump = cms.EDAnalyzer('PATDump',
  # input collections
  electronTag = cms.InputTag("cleanPatElectrons"),
  muonTag     = cms.InputTag("cleanPatMuons"),
  #tauTag      = cms.InputTag("selectedLayer1Taus"),
  jetTag      = cms.InputTag("cleanPatJets"),
  photonTag   = cms.InputTag("cleanPatPhotons"),
  metTag      = cms.InputTag("patMETs"),
  triggerResults = cms.InputTag("TriggerResults::HLT"),
  
  cfgWeight = cms.double(1),
  cfgPid    = cms.int32(-1),
  
  jetThreshold    = cms.double(20),
  photonThreshold = cms.double(10),
  leptonThreshold = cms.double(20),
  
  minLeptons = cms.int32(1),
  minJets    = cms.int32(1),
  
  debug = cms.bool(False)
)

# output file
process.TFileService = cms.Service("TFileService",
       fileName = cms.string("/local/cms/user/jmmans/TTbar_Tauola_GEN_FASTSIM_HLT_Tuple/TTbar_Tauola_GEN_FASTSIM_HLT_Tuple_001.root"),
)

process.generator = cms.EDFilter("Pythia6GeneratorFilter",
    ExternalDecays = cms.PSet(
        Tauola = cms.untracked.PSet(
            UseTauolaPolarization = cms.bool(True),
            InputCards = cms.PSet(
                mdtau = cms.int32(0),
                pjak2 = cms.int32(0),
                pjak1 = cms.int32(0)
            )
        ),
        parameterSets = cms.vstring('Tauola')
    ),
    pythiaPylistVerbosity = cms.untracked.int32(0),
    filterEfficiency = cms.untracked.double(1.0),
    pythiaHepMCVerbosity = cms.untracked.bool(False),
    comEnergy = cms.double(7000.0),
    maxEventsToPrint = cms.untracked.int32(0),
    PythiaParameters = cms.PSet(
        pythiaUESettings = cms.vstring('MSTJ(11)=3     ! Choice of the fragmentation function', 
            'MSTJ(22)=2     ! Decay those unstable particles', 
            'PARJ(71)=10 .  ! for which ctau  10 mm', 
            'MSTP(2)=1      ! which order running alphaS', 
            'MSTP(33)=0     ! no K factors in hard cross sections', 
            'MSTP(51)=10042 ! structure function chosen (external PDF CTEQ6L1)', 
            'MSTP(52)=2     ! work with LHAPDF', 
            'MSTP(81)=1     ! multiple parton interactions 1 is Pythia default', 
            'MSTP(82)=4     ! Defines the multi-parton model', 
            'MSTU(21)=1     ! Check on possible errors during program execution', 
            'PARP(82)=1.8387   ! pt cutoff for multiparton interactions', 
            'PARP(89)=1960. ! sqrts for which PARP82 is set', 
            'PARP(83)=0.5   ! Multiple interactions: matter distrbn parameter', 
            'PARP(84)=0.4   ! Multiple interactions: matter distribution parameter', 
            'PARP(90)=0.16  ! Multiple interactions: rescaling power', 
            'PARP(67)=2.5    ! amount of initial-state radiation', 
            'PARP(85)=1.0  ! gluon prod. mechanism in MI', 
            'PARP(86)=1.0  ! gluon prod. mechanism in MI', 
            'PARP(62)=1.25   ! ', 
            'PARP(64)=0.2    ! ', 
            'MSTP(91)=1      !', 
            'PARP(91)=2.1   ! kt distribution', 
            'PARP(93)=15.0  ! '),
        processParameters = cms.vstring('MSEL      = 0     ! User defined processes', 
            'MSUB(81)  = 1     ! qqbar to QQbar', 
            'MSUB(82)  = 1     ! gg to QQbar', 
            'MSTP(7)   = 6     ! flavour = top', 
            'PMAS(6,1) = 175.  ! top quark mass'),
        parameterSets = cms.vstring('pythiaUESettings', 
            'processParameters')
    )
)

# Path and EndPath definitions
process.generation_step = cms.Path(cms.SequencePlaceholder("randomEngineStateProducer"))
process.reconstruction = cms.Path(process.reconstructionWithFamos+process.patDefaultSequence)
process.out_step = cms.EndPath(process.patdump)

# Schedule definition
process.schedule = cms.Schedule(process.generation_step)
process.schedule.extend(process.HLTSchedule)
process.schedule.extend([process.reconstruction,process.out_step])

# output file
process.TFileService = cms.Service("TFileService",
       fileName = cms.string("/local/cms/user/jmmans/TTbar_Tauola_GEN_FASTSIM_HLT_Tuple/TTbar_Tauola_GEN_FASTSIM_HLT_Tuple_001.root"),
)

# special treatment in case of production filter sequence
for path in process.paths: 
    getattr(process,path)._seq = process.generator*getattr(process,path)._seq
