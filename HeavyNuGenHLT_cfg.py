import FWCore.ParameterSet.Config as cms
import random
import sys

# check command line parameters
if len(sys.argv) != 3:
  print "ERROR: incorrect command line parameters"
  print "       use: cmsRun HeavyNuGenHLT_cfg.py <pythia_setting_name>"
  print "       for available <pythia_setting_name> see heavynu_cfi.py"
  print ""
  sys.exit(1)


process = cms.Process("HLT")

process.load("Configuration/StandardSequences/Services_cff")
# seed generation
seed1 = random.randint(0, 900000000)
seed2 = random.randint(0, 900000000)
seed3 = random.randint(0, 900000000)
seed4 = random.randint(0, 900000000)
print "Initial seeds for random number service:"
print "  generator  = ", seed1
print "  VtxSmeared = ", seed2
print "  g4SimHits  = ", seed3
print "  mix        = ", seed4
process.RandomNumberGeneratorService.generator.initialSeed  = seed1
process.RandomNumberGeneratorService.VtxSmeared.initialSeed = seed2
process.RandomNumberGeneratorService.g4SimHits.initialSeed  = seed3
process.RandomNumberGeneratorService.mix.initialSeed        = seed4

process.load("FWCore/MessageService/MessageLogger_cfi")

process.options = cms.untracked.PSet(
  wantSummary = cms.untracked.bool(False),
)

process.maxEvents = cms.untracked.PSet(
  input = cms.untracked.int32(10)
)

# generator level information
process.load("Configuration/StandardSequences/Generator_cff")

# Pythia settings
process.load("Configuration/Generator/PythiaUESettings_cfi")
import heavynu_cfi

settingName = sys.argv[2]
print "PythiaParameters:"
print "  pythiaUESettings"
print " ", settingName, ":"

settingCard = heavynu_cfi.mkHeavyNuSettings(settingName)
if settingCard:
  heavynu_cfi.customSettingsBlock.signalCard = settingCard
  settingName = "signalCard"

print heavynu_cfi.customSettingsBlock.getParameter(settingName)

# Input source
process.source = cms.Source("EmptySource")
# set run number:
frun = random.randint(0, 1000000)
print "Source initialization:"
print "  firstRun  = ", frun
process.source.firstRun = cms.untracked.uint32(frun)

# Generator settings:
process.generator = cms.EDFilter("Pythia6GeneratorFilter",
  pythiaHepMCVerbosity = cms.untracked.bool(False),
  ToPrint = cms.untracked.int32(0),
  pythiaPylistVerbosity = cms.untracked.int32(0),
  filterEfficiency = cms.untracked.double(1.0),
  comEnergy = cms.double(8000.0),
  PythiaParameters = cms.PSet(
    process.pythiaUESettingsBlock,
    heavynu_cfi.customSettingsBlock,
    parameterSets = cms.vstring('pythiaUESettings', settingName)
  )
)

process.load('Configuration/StandardSequences/GeometryDB_cff')

process.load('Configuration/StandardSequences/MagneticField_38T_cff')
process.load('IOMC/EventVertexGenerators/VtxSmearedRealistic8TeVCollision_cfi')

# run HLT
process.load('HLTrigger/Configuration/HLT_GRun_cff')

# Conditions data for calibration and alignment
# the following page should be checked for the latest global tags at any given time
# https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideFrontierConditions
process.load("Configuration/StandardSequences/FrontierConditions_GlobalTag_cff")
from Configuration.AlCa.GlobalTag import GlobalTag
process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:mc', '')
print "global tag  = ", process.GlobalTag.globaltag

# Simulation and digitization
process.load("Configuration/StandardSequences/Simulation_cff")

# Pile-Up configuration
# general information:
#   https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideMixingModule
# pileup scenarios:
#   https://twiki.cern.ch/twiki/bin/view/CMS/PileupInformation
#
# No Pile-Up:
process.load("SimGeneral/MixingModule/mixNoPU_cfi")
# Spring11 production pileup scenario:
#process.load("SimGeneral/MixingModule/mix_E7TeV_FlatDist10_2011EarlyData_50ns_cfi")
# Summer11 production PU_S4 pileup scenario:
#process.load("SimGeneral/MixingModule/mix_E7TeV_FlatDist10_2011EarlyData_50ns_PoissonOOT")

# Summer12 production PU_S7 pileup scenario:
#process.load("SimGeneral/MixingModule/mix_2012_Startup_50ns_PoissonOOTPU_cfi")


# MinBias files for Summer11 PU_S4 scenario are removed from CASTOR area,
# that is why we re-point mixing module to our private area with
# samples from dataset:
#   /MinBias_TuneZ2_7TeV-pythia6/Summer11-IDEAL_PU_S4_MC_42_V11-v2/GEN-SIM-RECO:
# 
# Private area situated at two places:
#   pcrfcms14:/moscow31/heavynu/42x-minbias/
#   /castor/cern.ch/user/a/akorneev/heavynu/
#
# see also discussion at:
#   https://hypernews.cern.ch/HyperNews/CMS/get/physics-validation/1465/1/1/2/2/1/1/1.html
#process.mix.input.fileNames = cms.untracked.vstring(
#  'rfio:/castor/cern.ch/user/a/akorneev/heavynu/006AF92D-46FA-E011-93B3-0026189438BD.root',
#  'rfio:/castor/cern.ch/user/a/akorneev/heavynu/006BEA54-6EFA-E011-B765-001A92971B74.root'
#)


process.load('Configuration/StandardSequences/EndOfProcess_cff')

process.load("Configuration/StandardSequences/SimL1Emulator_cff")
process.load("Configuration/StandardSequences/DigiToRaw_cff")


process.hltTrigReport = cms.EDAnalyzer("HLTrigReport",
  HLTriggerResults = cms.InputTag('TriggerResults', '', 'HLT')
)

# Output module
process.load('Configuration/EventContent/EventContent_cff')
process.output = cms.OutputModule("PoolOutputModule",
  process.FEVTDEBUGHLTEventContent,
  fileName = cms.untracked.string('hlt.root')
)

# Path and EndPath definitions
process.generator_step     = cms.Path(process.generator)
process.generation_step    = cms.Path(process.pgen)
# psim + pdigi
process.simulation_step    = cms.Path(process.simulation)
process.L1simulation_step  = cms.Path(process.SimL1Emulator)
process.digi2raw_step      = cms.Path(process.DigiToRaw)
process.hltTrigReport_step = cms.Path(process.hltTrigReport)
process.endjob_step        = cms.Path(process.endOfProcess)
process.out_step           = cms.EndPath(process.output)

# Schedule definition
process.schedule = cms.Schedule(process.generator_step,
                                process.generation_step,
                                process.simulation_step,
                                process.L1simulation_step,
                                process.digi2raw_step)
process.schedule.extend(process.HLTSchedule)
process.schedule.append(process.hltTrigReport_step)
process.schedule.extend([process.endjob_step,process.out_step])
