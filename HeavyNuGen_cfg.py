import FWCore.ParameterSet.Config as cms
import random
import sys
from Configuration.Generator.Pythia8CommonSettings_cfi import *
from Configuration.Generator.Pythia8CUEP8M1Settings_cfi import *

# check command line parameters
if len(sys.argv) != 3:
  print "ERROR: incorrect command line parameters"
  print "       use: cmsRun HeavyNuGen_cfg.py <pythia_setting_name>"
  print "       for available <pythia_setting_name> see heavynu_cfi.py"
  print ""
  sys.exit(1)


process = cms.Process("GEN")

process.load("Configuration/StandardSequences/Services_cff")
# seed generation
seed1 = random.randint(0, 900000000)
print "Initial seeds for random number service:"
print "  generator  = ", seed1
process.RandomNumberGeneratorService.generator.initialSeed  = seed1

process.load("FWCore/MessageService/MessageLogger_cfi")
# supress message "Begin processing the ... record" to 1 time from
process.MessageLogger.cerr.FwkReport.reportEvery = 100
process.options = cms.untracked.PSet(
  wantSummary = cms.untracked.bool(False),
)

process.maxEvents = cms.untracked.PSet(
  input = cms.untracked.int32(500)
)

# generator level information. Is it needed?
#process.load("Configuration/StandardSequences/Generator_cff")

# Pythia settings
import heavynu_cfi

settingName = sys.argv[2]
print "PythiaParameters:"
print "  pythia8CommonSettings"
print "  pythia8CUEP8M1Settings"
print " ", settingName, ":"

settingCard = heavynu_cfi.mkHeavyNuSettings(settingName)
if settingCard:
  heavynu_cfi.customSettingsBlock.signalCard = settingCard
  settingName = "signalCard"

print heavynu_cfi.customSettingsBlock.getParameter(settingName)

# Input source
process.source = cms.Source("EmptySource")

# Generator settings:
process.generator = cms.EDFilter("Pythia8GeneratorFilter",
  pythiaHepMCVerbosity = cms.untracked.bool(False),
  maxEventsToPrint = cms.untracked.int32(0),
  pythiaPylistVerbosity = cms.untracked.int32(0),
  filterEfficiency = cms.untracked.double(1.0),
  comEnergy = cms.double(13000.0),
  PythiaParameters = cms.PSet(
    pythia8CommonSettingsBlock,
    pythia8CUEP8M1SettingsBlock,
    heavynu_cfi.customSettingsBlock,
    parameterSets = cms.vstring('pythia8CommonSettings', 'pythia8CUEP8M1Settings', settingName)
  )
)

# Path and EndPath definitions
process.generator_step     = cms.Path(process.generator)

# Schedule definition
process.schedule = cms.Schedule(process.generator_step)
