struct Lepts {
  int   nle;
  TLorentzVector lepton[100];
  TVector3 vtx[100];
  int   kl[100], indlep[100], lhit[100], mrl[100], lisol[100];
  float xisol[100];
  int   ilhnu[100], nhits[100];
  float chi2[100];
};

struct GSFelectrons {
  int   nle;
  TLorentzVector lepton[100];
  TVector3 vtx[100];
  float xisol[100];
  int   nhits[100];
  float chi2[100];
  int lostHits[100];
  float HtoE[100];
  float sigmaIetaIeta[100];
  std::vector<std::string> triggers[100];
  std::vector<int> prescales[100];
};

struct Photons {
  int   nphot;
  TLorentzVector phot[50];
  TVector3 vtx[50];
};

struct JetOut {
  int   njg;
  TLorentzVector jet[50];
  TVector3 vtx[50];
};

struct JeMark {
  int mrkjet[50], ihnu[50];
};

int readsample(const char *, int, int, float, float, float, float *, float*,
           std::ofstream *, std::ofstream *);
