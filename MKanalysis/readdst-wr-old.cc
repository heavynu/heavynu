#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <vector>
#include <map>
#include <math.h>

#include <TH1F.h>
#include <TH2F.h>
#include <TF1.h>
#include <TFile.h>
#include <TTree.h>
#include <TVector2.h>
#include <TVector3.h>
#include <TLorentzVector.h>
#include <TFeldmanCousins.h>

#include "hist.h"

#include "sampledb.h"

#include "readsample.h"

#include "HeavyNuCuts.h"
#include "Event.h"

using namespace std;

HeavyNuCuts mycuts;
Lepts lepts;
GSFelectrons gsfelectrons;
Photons photons;
JetOut jetout;
Event ev;

float NevTrig=0., NevZ=0.;
TH1D* pileuphisto=0;


bool debug = false; // true;

//=============================================================================

int procev(int, int, float, ofstream *, ofstream *, int*);

int parinv(float, float *, float *, int, float *);

void BookHistos(void);

//=============================================================================

int main(void)
{
  float bfc[20] = { 0., .5, 1., 1.5, 2., 2.5, 3., 3.5, 4., 5., 6.,
		    7., 8., 9., 10., 11., 12., 13., 14., 15. };
  float fc90[20] = { 2.44, 2.86, 3.28, 3.62, 3.94, 4.2, 4.42, 4.63,
		     4.83, 5.18, 5.53, 5.9, 6.18, 6.49, 6.76, 7.02,
		     7.28, 7.51, 7.75, 7.99 };

  float statbgwh, statbgzh, statbgzw, cs, csbg, stat;

  int   isample, isampletype;
  float ecm, lum;
  float csbgwg, statbgwg;
  float csbgqcd, statbgqcd;
  float csbgdoublew;
  int   ihbas, nread;
  float limbg, limbk;
  float lumfb, lumpb;

  float statbgdoublew;
  float csbgwh, csbgzh;
  float grtogl, csbgzw;
  string CMSSW;
  string Prefixsign, Prefixbg, Prefixdata;

  int   iaccept;
  float fluctbg, intbgcs, intsign, expsign, intdatatot=0.;
  float statsign;
  const char* Filenamechar;

  extern float NevTrig, NevZ;
  extern TH1D* pileuphisto;

  // LEPTS. 
  // ...Desired leptons found by user (PROLEP, then after CISOL check and, 
  // ...possibly, after GLOBJF), 
  // ...PL - 4-VECTORS,  KL - code :  +-1(2) for e(mu)-+ ; 
  // ...INDLEP - number of each lepton in /PLIST/, LHIT - in /HITPAR/,
  // ...MRL - origin (code of parent, =999 for SUSY particles), 
  // ...LISOL - isolation mark (0 - isolated, 1 - non-isolated in tracker, 
  // ... 2 - non-isolated in calo, 3 - non-isolated in both tracker and calo, 
  // ... 4 - isolated in jet,),   NLE - number of leptons. 
  
  // JETOUT. 
  // ...Jets found by subroutine GLOBJF 

  BookHistos();

  //TFile* fpileup = new TFile("204pb_10May_Rereco.root");
  //TFile* fpileup = new TFile("36pb_21April_Rereco.root");
  TFile* fpileup = new TFile("PURunA.root");
  //TFile* fpileup = new TFile("PURunB.root");
  pileuphisto = (TH1D*) fpileup->Get("pileup");
  cout << "histo pointer = " << pileuphisto << endl;

  // definition of the cuts
  mycuts.xmmaxisol = 10000.;
  mycuts.xmwcut = 0.;
  mycuts.ptlept = 40.;     // cut for all leptons, default 20 (30 for 2011)
  mycuts.ptlept1 = 40.;    // there should be at least one lepton above this, default 42
  mycuts.ptlepttrig = 40.; // as above, but applied before control
                           // samples analysis, default 42 
  mycuts.ptjcut1 = 40.;    // default 40
  mycuts.ptjcut2 = 40.;    // default 40
  mycuts.ntrhitmin = 0;
  mycuts.chi2max = 1000.;
  mycuts.xmaxisol[0] = 100.;
  mycuts.xmaxisol[1] = 100.;
  mycuts.ifchecksign = 0;
  mycuts.isamesign = 1;
  mycuts.iftakeminpt = 1;

  const char *Fname = "cards.dat";
  ifstream inFile(Fname);
  if (inFile == NULL) {
    cout << "Cannot open input file " << Fname << endl;
    exit(1);
  }

  char Filename[200] = {" "};
  string Filenamedb;

  char str[200] = {" "};

  inFile >> mycuts.hnumassgen;
  inFile.getline(str, 200);
  inFile >> mycuts.wrmassgen;
  inFile.getline(str, 200);
  inFile >> mycuts.mylept;
  inFile.getline(str, 200);
  inFile >> ecm;
  inFile.getline(str, 200);
  inFile >> lumfb;
  inFile.getline(str, 200);
  inFile >> grtogl;
  inFile.getline(str, 200);
  inFile >> CMSSW;
  inFile.getline(str, 200);
  inFile >> Prefixsign;
  inFile.getline(str, 200);
  inFile >> Prefixbg;
  inFile.getline(str, 200);
  inFile >> Prefixdata;
  inFile.getline(str, 200);

  inFile >> mycuts.ifchecksign;
  inFile.getline(str, 200);
  inFile >> mycuts.isamesign;
  inFile.getline(str, 200);
  inFile >> mycuts.nlegoodmin;
  inFile.getline(str, 200);
  inFile >> mycuts.nlegoodmax;
  inFile.getline(str, 200);
  inFile >> mycuts.zmasscut;
  inFile.getline(str, 200);
  inFile >> mycuts.ptmmax;
  inFile.getline(str, 200);
  inFile >> mycuts.xmcut1;
  inFile.getline(str, 200);
  inFile >> mycuts.xmcut2;
  inFile.getline(str, 200);
  inFile >> mycuts.xmwcut;
  inFile.getline(str, 200);
  inFile >> mycuts.xmaxisol[0];
  inFile.getline(str, 200);
  inFile >> mycuts.xmaxisol[1];
  inFile.getline(str, 200);

  inFile.close();

  cout << endl;
  cout << "At ECM = " << ecm 
       << " Looking for a Nu mass " << mycuts.hnumassgen
       << "  Wr mass " << mycuts.wrmassgen;
  if (mycuts.mylept == 1) {
    cout << "  coupling with electron " << endl;
  }
  else if (mycuts.mylept == 2) {
	cout << "  coupling with muon " << endl;
  }
  cout << endl << " gr/gl = " << grtogl << endl;

  bool icutsfound = false;
  if ( mycuts.xmcut1 < 0 ) {
    float hnum=mycuts.hnumassgen;
    if ( fabs(hnum-300.) < 10. )  mycuts.xmcut1 = 200.;  icutsfound=true;
    if ( fabs(hnum-500.) < 10. )  mycuts.xmcut1 = 370.;  icutsfound=true;
    if ( fabs(hnum-600.) < 10. )  mycuts.xmcut1 = 480.;  icutsfound=true;
    if ( fabs(hnum-700.) < 10. )  mycuts.xmcut1 = 580.;  icutsfound=true;
    if ( fabs(hnum-800.) < 10. )  mycuts.xmcut1 = 680.;  icutsfound=true;
    if ( fabs(hnum-1100.) < 10. ) mycuts.xmcut1 = 950.;  icutsfound=true;
    if ( fabs(hnum-1200.) < 10. ) mycuts.xmcut1 = 1050.; icutsfound=true;
    if ( fabs(hnum-1300.) < 10. ) mycuts.xmcut1 = 1150.; icutsfound=true;
    if ( fabs(hnum-1600.) < 10. ) mycuts.xmcut1 = 1100.; icutsfound=true;
  } else {
    icutsfound=true;
    cout << "using specified value for xmcut1" << endl;
    if (mycuts.xmcut1 > mycuts.hnumassgen - 20. ) {
      cout << "ATTENTION! specified in cards.dat xmcut1 seems to be too high!"
           << endl;
      if (mycuts.xmcut1 > mycuts.hnumassgen ) icutsfound=false;
    }
  }
  icutsfound=false;
  if ( mycuts.xmcut2 < 0 ) {
    float hnum=mycuts.hnumassgen;
    if ( fabs(hnum-300.) < 10. )  mycuts.xmcut2 = 390.;  icutsfound=true;
    if ( fabs(hnum-500.) < 10. )  mycuts.xmcut2 = 610.;  icutsfound=true;
    if ( fabs(hnum-600.) < 10. )  mycuts.xmcut2 = 710.;  icutsfound=true;
    if ( fabs(hnum-700.) < 10. )  mycuts.xmcut2 = 810.;  icutsfound=true;
    if ( fabs(hnum-800.) < 10. )  mycuts.xmcut2 = 910.;  icutsfound=true;
    if ( fabs(hnum-1100.) < 10. ) mycuts.xmcut2 = 1240.; icutsfound=true;
    if ( fabs(hnum-1200.) < 10. ) mycuts.xmcut2 = 1340.; icutsfound=true;
    if ( fabs(hnum-1300.) < 10. ) mycuts.xmcut2 = 1440.; icutsfound=true;
    if ( fabs(hnum-1600.) < 10. ) mycuts.xmcut2 = 1800.; icutsfound=true;
  } else {
    icutsfound=true;
    cout << "using specified value for xmcut2" << endl;
    if (mycuts.xmcut2 < mycuts.hnumassgen + 10. ) {
      cout << "ATTENTION! specified in cards.dat xmcut2 seems to be too low!"
           << endl;
      if (mycuts.xmcut2 < mycuts.hnumassgen ) icutsfound=false;
    }
  }
  if ( !icutsfound ) {
    cout << "ERROR: mass window cuts not found in the code, exiting" << endl;
    exit(1);
  }


  lumpb = lumfb * 1e3;
  lum = lumpb * 1e9;

  cs *= grtogl; // if right-handed coupling smaller than left-hand 

  // write some dst information
  ofstream outSignal("dstsign.d");
  ofstream outBcg("dstbg.d");

  //-------------------------------------------------------------------------

  // ---- data events ---- data events ---- data events ----

    // database of all samples
  SampleDB db, db1;
  cout << "Using database SampleDB" << endl;

  cout << endl;
  cout << "reading data" << endl;

  db1 = db.find("type", 2);
  if(db1.size() > 1) {
    cout << "Found " << db1.size() << " data samples:" << endl;
    cout << db1 << endl;
    float intdataw, intdata;
    int isamples[10], nsamples=0;

//  -- electron samples -------
//    if(mycuts.mylept == 1) {isamples[0]=2; isamples[1]=4;
//                            isamples[2]=6; isamples[3]=8; nsamples=4;}
//    if(mycuts.mylept == 1) {isamples[0]=30; nsamples=1;}
//    if(mycuts.mylept == 1) {isamples[0]=53; nsamples=1;} // apr21 rereco of 2010
//    if(mycuts.mylept == 1) {isamples[0]=36; isamples[1]=41; nsamples=2;}
//    if(mycuts.mylept == 1) {isamples[0]=49; isamples[1]=57; isamples[2]=59; nsamples=3;}
//    if(mycuts.mylept == 1) {isamples[0]=49; nsamples=1;}
//    if(mycuts.mylept == 1) {isamples[0]=67; nsamples=1;} // SEle 2011A
//    if(mycuts.mylept == 1) {isamples[0]=71; nsamples=1;}  // DEle 2011A
    if(mycuts.mylept == 1) {isamples[0]=72; nsamples=1;}  // DEle 2011B
//    if(mycuts.mylept == 1) {isamples[0]=69; isamples[1]=70; nsamples=2;}  // DEle 2011A+B


//    if(mycuts.mylept == 1) {isamples[0]=44; nsamples=1;} // photons


//  -- muon samples -----------
//    if(mycuts.mylept == 2) {isamples[0]=38; nsamples=1;}
    if(mycuts.mylept == 2) {isamples[0]=50; nsamples=1;}

//  -- samples for e - mu signature ---
//    if(mycuts.mylept == 3) {isamples[0]=2; isamples[1]=4;
//                            isamples[2]=5; isamples[3]=6;
//                            isamples[4]=7; isamples[5]=8;
//                            isamples[6]=9; nsamples=7;}
//    if(mycuts.mylept == 3) {isamples[0]=30; isamples[1]=27; nsamples=2;}
//    if(mycuts.mylept == 3) {isamples[0]=38; nsamples=1;}
    if(mycuts.mylept == 3) {isamples[0]=49; nsamples=1;}

//  -- photon samples  ----------------
//    if(mycuts.mylept == 4) {isamples[0]=75; nsamples=1;} // Single photon test
//    if(mycuts.mylept == 4) {isamples[0]=74; nsamples=1;} // Single photon run B
    if(mycuts.mylept == 4) {isamples[0]=75; isamples[1]=76; isamples[2]=77;
                            isamples[3]=78; nsamples=4;} // Single photon test

    double lumpbdata = 0.;
    for(isample=0; isample < nsamples; isample++) {
      Filenamedb = db1[isamples[isample]].fname;
      Filenamedb = Prefixdata + Filenamedb;
      isampletype = 5;
      Filenamechar = Filenamedb.c_str();
      readsample(Filenamechar, 5, 9000,
                 1., 1., 1., &intdataw, &intdata, &outSignal, &outBcg);
      intdatatot += intdata;
      lumpbdata += db1[isamples[isample]].lumi;
    }
    if(lumpbdata > 0.000001) {
      lumpb = lumpbdata;
      lumfb = lumpb * 0.001;
      lum = lumpb * 1e9;
      cout << endl
           << " Data were used, redefined integrated luminosity: " << lumpb
           << " pb" << endl << endl;
    }
  }

  // ---- signal events ---- signal events ---- signal events ---- 

  int NOMC=0;
  if(!NOMC) {

  db1 = db.find("ECM", (int)ecm)
          .find("MW", (int)mycuts.wrmassgen)
          .find("MNu", (int)mycuts.hnumassgen);
  if(mycuts.mylept < 3) db1 = db1.find("channel",mycuts.mylept);
  if(!db1.size()) {
    cout << "ERROR: signal sample parameters not found in SampleDB, exiting"
         << endl;
    exit(1);
  }
  isample=0;
  if(db1.size() > 1) {
    cout << "Found " << db1.size() << " signal samples:" << endl;
    cout << db1 << endl;
//    cout << "Enter number from 0 to " << db1.size()-1 << " Number? > ";
//    cin >> isample;
    cout << endl;
  }
  isample = 0;
  Filenamedb = db1[isample].fname;
  cout << "Signal file " << Filenamedb << " selected" << endl;
  Filenamedb = Prefixsign + Filenamedb;
  cs = db1[isample].CS*1.e-9;
  statsign = db1[isample].stat;
  isampletype = 5;

  ihbas = 0;
  float intsignw = 0.;
  intsign = 0.;

  cout << "  lum = " << lum << " mb**-1  cs = " << cs << " mb  statsign = " 
       << statsign << endl;

  Filenamechar = Filenamedb.c_str();

  cout << "  Filename = " << Filenamechar << endl;

  readsample(Filenamechar, isampletype, ihbas, lum, cs, statsign,
             &intsignw, &intsign, &outSignal, &outBcg);

  cout << " read from dst: Signal events passed: " 
       << intsign << ",  eff = " << intsign/statsign << endl;

  cout << " Signal primary selection efficiency: "
       << DHGetI(10003, 1)/statsign << ",  error = " 
       <<  sqrt(DHGetI(10003, 1))/statsign << endl;


  // ---- bg ---- bg ---- bg ---- bg ---- bg ---- bg ---- bg ---- bg ---- 

  cout << endl << endl << " ------- reading background; luminocity = "
       << lumfb << " fb**-1" << endl << endl;
    
  float intbg = 0.;
  intbgcs = 0.;

// reading ttbar, AlCa: START36_V9, MC_38Y_V10::All, START38_V12::All, START41_V0::All START42_V11::All START42_V15B::All
  db1 = db.find("name", "TTTo2L2Nu2B")
//  db1 = db.find("name", "ttbar")
//  db1 = db.find("name", "TTJets")
          .find("ECM", (int)ecm)
          .find("AlCa", "START42_V15B::All");
  if(!db1.size()) {
    cout << "ERROR: ttbar sample parameters not found in SampleDB, exiting"
         << endl;
    exit(1);
  }
  isample=0;
  if(db1.size() > 1) {
    cout << "Found " << db1.size() << " ttbar samples:" << endl;
    cout << db1 << endl;
    //cout << "Enter number from 0 to " << db1.size()-1 << " Number? > ";
    //cin >> isample;
    //cout << endl;
  }
  isample=0; // samples > 1 : for syst. studies
  Filenamedb = db1[isample].fname;
  cout << "File " << Filenamedb << " selected" << endl;
//  Filenamedb = Prefixbg + Filenamedb;
//  Filenamedb = string("../dst41/") + Filenamedb;
  Filenamedb = string("../dst42/") + Filenamedb;
  csbg = db1[isample].CS*1.e-9;
  stat = db1[isample].stat;
  isampletype = 5;
  Filenamechar = Filenamedb.c_str();
  readsample(Filenamechar, isampletype, 1000,
         lum, csbg, stat, &intbgcs, &intbg, &outSignal, &outBcg);
  //csbg = 317.0e-9 * 1.3; // k-factor from HEEP
  //stat = 10000;
  //readsample("../dst2010/TTbar_Tauola_GEN_FASTSIM_HLT.root", 5, 1000,
  //       lum, csbg, stat, &intbgcs, &intbg, &outSignal, &outBcg);

// reading Zg ------------
// AlCa: START36_V9 , START36_V10::All , START38_V12::All , START41_V0::All
  cout << endl;
  db1 = db.find("name", "DYToLL")
//  db1 = db.find("name", "Z+jets")
//  db1 = db.find("name", "ZJetToEE")
          .find("ECM", (int)ecm);
//          .find("AlCa", "START41_V0::All");
//  db1 = db.find("name", "Z+2jets smallerISRFSR");
//  db1 = db.find("name", "ZJetToEE");
  if(!db1.size()) {
    cout << "ERROR: Z+jets sample parameters not found in SampleDB, exiting"
         << endl;
    exit(1);
  }
  isample=0;
  if(db1.size() > 0) {
    cout << "Found " << db1.size() << " Zg samples:" << endl;
    cout << db1 << endl;
    //cout << "Enter number from 0 to " << db1.size()-1 << " Number? > ";
    //cin >> isample;
    //cout << endl;
  }
//  stat = 0.;
//  for(isample=0; isample < db1.size(); isample++) {
//    stat += db1[isample].stat;
//  }
  for(isample=0; isample < 1; isample++) {  // > 20: samples for syst. studies
    Filenamedb = db1[isample].fname;
    cout << "File " << Filenamedb << " selected" << endl;
//    Filenamedb = Prefixbg + Filenamedb;
    Filenamedb = string("../dst42/") + Filenamedb;
    csbg = db1[isample].CS*1.e-9;
    stat = db1[isample].stat;
    cout << " cs, stat = " << csbg << " " << stat << endl;
    isampletype = 6;
    Filenamechar = Filenamedb.c_str();
    readsample(Filenamechar, isampletype, 2000,
           lum, csbg, stat, &intbgcs, &intbg, &outSignal, &outBcg);
  }
#if 0
  //csbg=3600.0e-9;
  //csbg=0.67e-9; // Mll > 200
  //csbg=0.9277e-9; // Mll > 180
  //csbg=0.75e-9; // Z103 is a mixture of above
  csbg=0.164e-9; // Alpgen simulation: ee with 2 jets
  stat=1000.;
  readsample("../dst38HEEP/ZjetsAlpgen.root", 5, 2000,
         lum, csbg, stat, &intbgcs, &intbg, &outSignal, &outBcg);
#endif

// reading Wg
  cout << endl;
#if 0
  if(mycuts.mylept == 1) {
    db1 = db.find("name", "Wenu")
            .find("ECM", (int)ecm)
            .find("AlCa", CMSSW);
  } if(mycuts.mylept == 2) {
    db1 = db.find("name", "Wmunu")
            .find("ECM", (int)ecm)
            .find("AlCa", CMSSW);
  } if(mycuts.mylept == 3) {
    db1 = db.find("name", "Wenu")
            .find("ECM", (int)ecm)
            .find("AlCa", CMSSW);
    db2 = db.find("name", "Wmunu")
            .find("ECM", (int)ecm)
            .find("AlCa", CMSSW);
  }
#endif
  db1 = db.find("name", "WJetsToLNu")
          .find("ECM", (int)ecm);
//          .find("AlCa", "START38_V14::All");
  if(!db1.size()) {
    cout << "ERROR: W+jets sample parameters not found in SampleDB, exiting"
         << endl;
    exit(1);
  }
  isample=0;
  if(db1.size() > 1) {
    cout << "Found " << db1.size() << " Wg samples:" << endl;
    cout << db1 << endl;
    //cout << "Enter number from 0 to " << db1.size()-1 << " Number? > ";
    //cin >> isample;
    cout << endl;
  }
  isample=1;
  Filenamedb = db1[isample].fname;
  cout << "File " << Filenamedb << " selected" << endl;
  Filenamedb = Prefixbg + Filenamedb;
  csbg = db1[isample].CS*1.e-9;
  stat = db1[isample].stat;
  isampletype = 5;
  Filenamechar = Filenamedb.c_str();
  readsample(Filenamechar, isampletype, 3000,
         lum, csbg, stat, &intbgcs, &intbg, &outSignal, &outBcg);


// reading gamma + jets , AlCa: START38_V14::All START41_V0::All START42_V15B::All
  cout << endl;
//  db1 = db.find("name", "Gamma+Jets")
  db1 = db.find("name", "GJets")
          .find("ECM", (int)ecm)
          .find("AlCa", CMSSW);
  if(!db1.size()) {
    cout << "ERROR: gamma+jets sample parameters not found in SampleDB, exiting"
         << endl;
    exit(1);
  }
  if(db1.size() > 1) {
    cout << "Found " << db1.size() << " Gamma+Jets samples:" << endl;
    cout << db1 << endl;
  }
  for(isample=0; isample < db1.size(); isample++) {
    Filenamedb = db1[isample].fname;
    cout << "File " << Filenamedb << " selected" << endl;
    Filenamedb = Prefixbg + Filenamedb;
//    Filenamedb = string("../dst41/") + Filenamedb;
    csbg = db1[isample].CS*1.e-9;
    stat = db1[isample].stat;
    isampletype = 5;
    Filenamechar = Filenamedb.c_str();
    readsample(Filenamechar, isampletype, 4000,
           lum, csbg, stat, &intbgcs, &intbg, &outSignal, &outBcg);
  }



#if 0
// reading QCD
  cout << endl;
  db1 = db.find("name", "QCD")
          .find("ECM", (int)ecm)
          .find("AlCa", "START36_V10");
  if(!db1.size()) {
    cout << "ERROR: QCD sample parameters not found in SampleDB, exiting"
         << endl;
    exit(1);
  }
  isample=0;
  if(db1.size() > 1) {
    cout << "Found " << db1.size() << " QCD samples:" << endl;
    cout << db1 << endl;
    //cout << "Enter number from 0 to " << db1.size()-1 << " Number? > ";
    //cin >> isample;
    //cout << endl;
  }
  for(isample=0; isample < db1.size(); isample++) {
    Filenamedb = db1[isample].fname;
    Filenamedb = Prefixbg + Filenamedb;
    csbg = db1[isample].CS*1.e-9;
    stat = db1[isample].stat;
    isampletype = 5;
    Filenamechar = Filenamedb.c_str();
    readsample(Filenamechar, isampletype, 5000,
         lum, csbg, stat, &intbgcs, &intbg, &outSignal, &outBcg);
  }
#endif
#if 0
  cout << "------ start reading fake QCD background -----------" << endl;
  // pythia8 + tests/analyserhepmc + fake rate, 7 TeV
  csbgwh = 10.0e-9;
  statbgwh = 100000;
  readsample("../dst2010/dst-QCD-fake.d", 3, 5000, lumfb, csbgwh,
             statbgwh, &intbgcs, &intbg, &outSignal, &outBcg); // 3: with weight
#endif
  cout << "------ start reading fake QCD background -----------" << endl;
  // LQ superclusters sample + LQ fake rate, 7 TeV
  csbgwh = 10.0e-9;
  statbgwh = 100000;
  readsample("../dstLQ4/dstmkLQ5.d", 3, 5000, lumfb, csbgwh,
             statbgwh, &intbgcs, &intbg, &outSignal, &outBcg); // 3: with weight

// reading WW
  cout << endl;
  db1 = db.find("name", "WW")
          .find("ECM", (int)ecm)
          .find("AlCa", CMSSW);
  if(!db1.size()) {
    cout << "ERROR: WW sample parameters not found in SampleDB, exiting"
         << endl;
    exit(1);
  }
  isample=0;
  if(db1.size() > 1) {
    cout << "Found " << db1.size() << " WW samples:" << endl;
    cout << db1 << endl;
    cout << "Enter number from 0 to " << db1.size()-1 << " Number? > ";
    cin >> isample;
    cout << endl;
  }
  Filenamedb = db1[isample].fname;
  cout << "File " << Filenamedb << " selected" << endl;
  Filenamedb = Prefixbg + Filenamedb;
  csbg = db1[isample].CS*1.e-9;
  stat = db1[isample].stat;
  isampletype = 5;
  Filenamechar = Filenamedb.c_str();
  readsample(Filenamechar, isampletype, 6000,
         lum, csbg, stat, &intbgcs, &intbg, &outSignal, &outBcg);

// reading WZ , AlCa : CMSSW START41_V0::All
  cout << endl;
  db1 = db.find("name", "WZ")
          .find("ECM", (int)ecm)
          .find("AlCa", CMSSW);
  if(!db1.size()) {
    cout << "ERROR: WZ sample parameters not found in SampleDB, exiting"
         << endl;
    exit(1);
  }
  isample=0;
  if(db1.size() > 1) {
    cout << "Found " << db1.size() << " WZ samples:" << endl;
    cout << db1 << endl;
    cout << "Enter number from 0 to " << db1.size()-1 << " Number? > ";
    cin >> isample;
    cout << endl;
  }
  Filenamedb = db1[isample].fname;
  cout << "File " << Filenamedb << " selected" << endl;
  Filenamedb = Prefixbg + Filenamedb;
  csbg = db1[isample].CS*1.e-9;
  stat = db1[isample].stat;
  isampletype = 5;
  Filenamechar = Filenamedb.c_str();
  readsample(Filenamechar, isampletype, 7000,
         lum, csbg, stat, &intbgcs, &intbg, &outSignal, &outBcg);

// reading ZZ
  cout << endl;
  db1 = db.find("name", "ZZ")
          .find("ECM", (int)ecm)
          .find("AlCa", CMSSW);
  if(!db1.size()) {
    cout << "ERROR: ZZ sample parameters not found in SampleDB, exiting"
         << endl;
    exit(1);
  }
  isample=0;
  if(db1.size() > 1) {
    cout << "Found " << db1.size() << " ZZ samples:" << endl;
    cout << db1 << endl;
    cout << "Enter number from 0 to " << db1.size()-1 << " Number? > ";
    cin >> isample;
    cout << endl;
  }
  Filenamedb = db1[isample].fname;
  cout << "File " << Filenamedb << " selected" << endl;
  Filenamedb = Prefixbg + Filenamedb;
  csbg = db1[isample].CS*1.e-9;
  stat = db1[isample].stat;
  isampletype = 5;
  Filenamechar = Filenamedb.c_str();
  readsample(Filenamechar, isampletype, 8000,
         lum, csbg, stat, &intbgcs, &intbg, &outSignal, &outBcg);

// reading tW
  cout << endl;
  db1 = db.find("name", "tW")
          .find("ECM", (int)ecm)
          .find("AlCa", CMSSW);
  if(!db1.size()) {
    cout << "ERROR: tW sample parameters not found in SampleDB, exiting"
         << endl;
    exit(1);
  }
  for(isample=0; isample < db1.size(); isample++) {
    Filenamedb = db1[isample].fname;
    cout << "File " << Filenamedb << " selected" << endl;
    Filenamedb = Prefixbg + Filenamedb;
    csbg = db1[isample].CS*1.e-9;
    stat = db1[isample].stat;
    isampletype = 5;
    Filenamechar = Filenamedb.c_str();
    readsample(Filenamechar, isampletype, 8000,
           lum, csbg, stat, &intbgcs, &intbg, &outSignal, &outBcg);
  }

// ------------ "Other backgrounds" -------------------------------

#if 0
  cout << "------ start reading tW background -----------" << endl;
  // toprex + pythia + tests/analyserhepmc, 7 TeV
  csbgwh = 10.0e-9;
  statbgwh = 100000;
  readsample("../dst2010/dst-tW-toprex.d", 0, 8000, lum, csbgwh,
             statbgwh, &intbgcs, &intbg, &outSignal, &outBcg);
#endif

#if 0
  csbgqcd = 15000000.0e-9;
  statbgqcd = 12405956.;
  readsample("../dstnew/mkdst-qcd100to250.d", 2, 5000,
             lum, csbgqcd, statbgqcd, &intbgcs, &intbg, &outSignal, &outBcg);
  csbgqcd = 400000.0e-9;
  statbgqcd = 5064539.;
  readsample("../dstnew/mkdst-qcd250to500.d", 2, 5000,
             lum, csbgqcd, statbgqcd, &intbgcs, &intbg, &outSignal, &outBcg);
  csbgqcd = 14000.0e-9;
  statbgqcd = 4719134.;
  readsample("../dstnew/mkdst-qcd500to1000.d", 2, 5000,
             lum, csbgqcd, statbgqcd, &intbgcs, &intbg, &outSignal, &outBcg);
  csbgqcd = 370.0e-9;
  statbgqcd = 1066863.;
  readsample("../dstnew/mkdst-qcd1000toInf.d", 2, 5000,
             lum, csbgqcd, statbgqcd, &intbgcs, &intbg, &outSignal, &outBcg);

//  cout << "------ start reading stew -----------------" << endl;
//  if (mycuts.mylept == 1) {
//    readsample("../dstnew/stew-electrons.d", 4, 8000, lumfb, 
//	       0., 0., &intbgcs, &intbg, &outSignal, &outBcg);
//  } else {
//    readsample("../dstnew/stew-muons.d", 4, 8000, lumfb, 
//	       0., 0., &intbgcs, &intbg, &outSignal, &outBcg);
//  }

  cout << "------ start reading diboson background -----------" << endl;
  
//  readsample("../dstnew/dst-ww-cmssw16.d", 4, 6000, lumfb, 
//	     0., 0., &intbgcs, &intbg, &outSignal, &outBcg);
  csbgzw = 44.8e-9 * 1.65; // k-factor from HEEP
  statbgzw = 204722;
  readsample("../dstnew/dst-WW-cmssw22.d", 2, 6000,
             lum, csbgzw, statbgzw, &intbgcs, &intbg, &outSignal, &outBcg);

//  readsample("../dstnew/dst-wz-cmssw16.d", 4, 7000, lumfb, 
//	     0., 0., &intbgcs, &intbg, &outSignal, &outBcg);
  csbgzw = 17.3e-9 * 1.65; // unknown; estimated with pythia6; k-factor fr. HEEP
  statbgzw = 248800;
  readsample("../dstnew/dst-WZ-cmssw22.d", 2, 7000,
             lum, csbgzw, statbgzw, &intbgcs, &intbg, &outSignal, &outBcg);

//  readsample("../dstnew/dst-zz-cmssw16.d", 4, 8000, lumfb, 
//	     0., 0., &intbgcs, &intbg, &outSignal, &outBcg);

  cout << "------ start reading zh background -----------" << endl;
  // cmsjet, 14 TeV
  csbgzh = 0.3753e-9;
  statbgzh = 5e5;
  readsample("../dstnew/dst-zh.d", 0, 8000, lum, csbgzh, 
	     statbgzh, &intbgcs, &intbg, &outSignal, &outBcg);

  cout << "------ start reading wh background -----------" << endl;
  // cmsjet, 14 TeV
  csbgwh = 0.6818e-9;
  statbgwh = 4.26e5;
  readsample("../dstnew/dstnew-wh-1.d", 0, 8000, lum, csbgwh, 
	         statbgwh, &intbgcs, &intbg, &outSignal, &outBcg);

  cout << "------ start reading tW background -----------" << endl;
  // toprex + pythia + tests/analyserhepmc, 10 TeV
  csbgwh = 26.0e-9;
  statbgwh = 100000;
  readsample("../dstnew/dst-tW-toprex.d", 0, 8000, lum, csbgwh,
             statbgwh, &intbgcs, &intbg, &outSignal, &outBcg);

  cout << "------ start reading doubleW background -----------" << endl;
  // pythia8 - private CMSSW 1.4.8 - 1.6.9, 14 TeV
  csbgdoublew = 4.8e-13;
  statbgdoublew = 1e3;
  readsample("../dstnew/dst-doubleW.d", 0, 8000, lum, 
             csbgdoublew, statbgdoublew, &intbgcs, &intbg, &outSignal, &outBcg);
#endif

  // ---- Final calculations --------------------------------------- 

  } //NOMC:
  cout << endl;
  cout << "--------------------- final results -------------------" << endl;
  cout << endl;
  cout << "Events in the trigger sample  : " << NevTrig << endl;
  cout << "Events in the control Z sample: " << NevZ << endl;

  fluctbg = sqrt(intbgcs);
  cout << endl;
  cout << " intbgcs = " << intbgcs << " fluctbg = " << fluctbg << endl;
  if (fluctbg < intbgcs * 0.15) fluctbg = intbgcs * 0.15;
  if (intbgcs > 15.) limbg = fluctbg * 2.0;
  else {
    parinv(intbgcs, bfc, fc90, 20, &limbg);
  }

  expsign = lum * cs * intsign / statsign;
  float lim = limbg / expsign;
  cout << "   expsign = " <<  expsign << "  lim = " << lim << endl;

  limbk = fluctbg * 5.0 + 6.25; // Bityukov - Krasnikov 
  limbk /= expsign;

  cout << endl << " limit: " << lim << "  signal and bcg: " << expsign 
       << "  " << intbgcs << endl;
  cout << " Discovery potential (Bityukov-Krasnikov): " 
       << limbk << endl << endl;

  cout << "F - C calc" << endl;
  TFeldmanCousins f(0.95);
  limbg = f.CalculateUpperLimit(intdatatot, intbgcs);
  lim = limbg / expsign;
  cout << "   data = " <<  intdatatot << "  lim = " << lim << endl;

  cout << endl;
  cout << "case 1 and 0.2 " << f.CalculateUpperLimit(1, 0.2) << endl;
  parinv(0.2, bfc, fc90, 20, &limbg);
  cout << " fc for 0.2 = " << limbg << endl;
  parinv(0.4, bfc, fc90, 20, &limbg);
  cout << " fc for 0.4 = " << limbg << endl;

  double xlim = 0., totprob=0.;
  cout << "case bg 0.2" << endl;
  xlim += exp(-0.2) * 3.;
  cout << " probability of 0 is " << exp(-0.2) << " limit is 3" << endl;
  totprob += exp(-0.2);
  xlim += 0.2*exp(-0.2) * 4.7;
  cout << " probability of 1 is " << 0.2*exp(-0.2) << " limit is 4.7" << endl;
  totprob += 0.2*exp(-0.2);
  xlim += (0.04*exp(-0.2)/2.) * 6.2;
  cout << " probability of 2 is " << 0.02*exp(-0.2) << " limit is 6.2" << endl;
  totprob += 0.02*exp(-0.2);
  xlim /= totprob;
  cout << "expected limit is " << xlim << endl;

  totprob = 0.;
  xlim = 0.;
  cout << "case bg 0.4" << endl;
  xlim += exp(-0.4) * 3.;
  cout << " probability of 0 is " << exp(-0.4) << " limit is 3" << endl;
  totprob += exp(-0.4);
  xlim += 0.4*exp(-0.4) * 4.5;
  cout << " probability of 1 is " << 0.4*exp(-0.4) << " limit is 4.5" << endl;
  totprob += 0.4*exp(-0.4);
  xlim += (0.16*exp(-0.4)/2.) * 6.1; 
  cout << " probability of 2 is " << 0.08*exp(-0.4) << " limit is 6.1" << endl;
  totprob += 0.08*exp(-0.4);
  xlim /= totprob;
  cout << "expected limit is " << xlim << endl;
  xlim = 2.07*2.07*2.07*2.07*2.07*exp(-2.07)/(2.*3.*4.*5.);
  cout << "probability of 5 with exp 2.07 = " << xlim << endl;


  outSignal.close();
  outBcg.close();

  DHROUT();
  DHBookTerm();

  return 0;
} 


//=============================================================================

void BookHistos(void)
{
  DHBook1(30031, "leptons eta", 14, -2.8, 2.8);
  DHBook1(30032, "leptons eta", 14, -2.6, 3.);
  DHBook1(30033, "leptons eta in peak", 14, -2.8, 2.8);

  DHBook1(30051, "control sample Z, Mll", 40, 0., 200.);
  DHBook1(30052, "control sample Z, Pt", 100, 0., 500.);
  DHBook1(30053, "control sample Z, Pt l", 100, 0., 500.);
  DHBook1(30054, "control sample Z, jet dist", 100, 0., 10.);
  DHBook1(30055, "control sample Z, cos Z-jets", 100, -1., 1.);
  DHBook1(30056, "control sample Z, vtx xy", 50, 0., 2.5);
  DHBook1(30057, "control sample Z, vtx z", 100, -25., 25.);
  DHBook1(30058, "control sample Z, dvtx z", 100, -25., 25.);
  DHBook1(30059, "control sample Z, jet vtx z", 100, -25., 25.);

  DHBook1(30061, "same sign Z, Mll", 100, 0., 200.);
  DHBook1(30062, "same sign Z, Pt", 100, 0., 500.);
  DHBook1(30063, "same sign Z, Pt l", 100, 0., 500.);
  DHBook1(30064, "wrong sign l from Z, Pt", 100, 0., 500.);

  DHBook1(30071, "isovalue matched e", 100, -1., 1.);
  DHBook1(30072, "isovalue unmatched e", 100, -1., 1.);
  DHBook1(30073, "isovalue matched mu", 100, -1., 1.);
  DHBook1(30074, "isovalue unmatched mu", 100, -1., 1.);
  DHBook1(30075, "isovalue far jet", 50, 0., 5.);
  DHBook1(30076, "isovalue close jet", 50, 0., 5.);
  DHBook1(30077, "pt far jet", 100, 0., 200.);
  DHBook1(30078, "pt close jet", 100, 0., 200.);

  DHBook1(40101, "npileup data", 40, -0.5, 39.5);
  DHBook1(40201, "npileup MC", 40, -0.5, 39.5);

  DHBook1(30081, "signal, lept-jet dist", 100, 0., 10.);
  DHBook1(30085, "isovalue far jet", 50, 0., 0.5);
  DHBook1(30086, "isovalue close jet", 50, 0., 0.5);
  DHBook1(30087, "pt far jet", 100, 0., 200.);
  DHBook1(30088, "pt close jet", 100, 0., 200.);

  DHBook1(30091, "M tt all", 50, 0., 1000.);
  DHBook1(30092, "M tt nontt", 50, 0., 1000.);
  DHBook1(30093, "M tt true tt", 50, 0., 1000.);

  /* Data histos */
  DHBook1(31051, "Data, control sample Z, Mll", 40, 0., 200.);
  DHBook1(31052, "Data, control sample Z, Pt", 100, 0., 500.);
  DHBook1(31056, "Data, control sample Z, vtx xy", 50, 0., 2.5);
  DHBook1(31057, "Data, control sample Z, vtx z", 100, -25., 25.);
  DHBook1(31058, "Data, control sample Z, dvtx z", 100, -25., 25.);
  DHBook1(31059, "Data, control sample Z, jet vtx z", 100, -25., 25.);
  DHBook1(31031, "Data, leptons eta", 14, -2.8, 2.8);
  DHBook1(31032, "Data, leptons eta", 14, -2.6, 3.);
  DHBook1(31033, "Data, leptons eta in peak", 14, -2.8, 2.8);
  DHBook1(31091, "Data, M tt all", 50, 0., 1000.);
  DHBook1(31075, "isovalue far jet", 50, 0., 5.);
  DHBook1(31076, "isovalue close jet", 50, 0., 5.);
  DHBook1(31077, "pt far jet", 100, 0., 200.);
  DHBook1(31078, "pt close jet", 100, 0., 200.);

  /* signal histos */
  DHBook1(10002, "Selection signal ", 10, .5, 10.5);
  DHBook1(10003, "Selection signal unw.", 10, .5, 10.5);
  DHBook1(10401, "MWR", 100, 0., 5000.);
  DHBook1(10402, "MN", 100, 0., 5000.);
  DHBook1(10403, "MN/MWR", 50, 0., 1.);
  DHBook1(10301, "phidist", 80, 0., 4.);
  DHBook1(10302, "ptratio", 50, 0., 5.);
  DHBook1(10303, "pt", 25, 0., 500.);
  DHBook1(10304, "pt barrel", 50, 0., 2000.);
  DHBook1(10305, "pt endcup", 50, 0., 2000.);
  DHBook1(10414, "M neutrino correct rec.", 300, 0., 6000.);
  DHBook1(10424, "M neutrino correct lepton", 300, 0., 6000.);
  DHBook1(10484, "M neutrino wide bin", 200, 0., 6000.);
  DHBook1(10405, "MZ", 200, 0., 2000.);
  DHBook1(10417, "M WR correct rec.", 100, 0., 6000.);
  DHBook1(10427, "M WR correct leptons", 100, 0., 6000.);
  DHBook1(10487, "M WR wide bin", 100, 0., 6000.);

  /*  tt bg histos */
  DHBook1(11002, "Selection ttbar ", 10, .5, 10.5);
  DHBook1(11003, "Selection ttbar unw.", 10, .5, 10.5);
  DHBook1(11401, "MWR", 50, 0., 5000.);
  DHBook1(11402, "MN", 50, 0., 5000.);
  DHBook1(11403, "MN/MWR", 20, 0., 1.);
  DHBook1(11414, "M neutrino correct rec.", 300, 0., 6000.);
  DHBook1(11405, "MZ", 200, 0., 2000.);
  DHBook1(11406, "Mll if 2 jets", 10, 0., 300.);
  DHBook1(11301, "phidist", 80, 0., 4.);
  DHBook1(11302, "ptratio", 50, 0., 5.);
  DHBook1(11303, "ej pt", 25, 0., 500.);
  DHBook1(11304, "ej pt barrel", 25, 0., 500.);
  DHBook1(11305, "ej pt endcup", 25, 0., 500.);
  DHBook1(11306, "ej pt endcup2", 25, 0., 500.);
  DHBook1(11307, "ej ptmiss", 20, 0., 200.);
  DHBook1(11313, "ecj pt", 25, 0., 500.);
  DHBook1(11314, "ecj pt barrel", 25, 0., 500.);
  DHBook1(11315, "ecj pt endcup", 25, 0., 500.);
  DHBook1(11316, "ecj pt endcup2", 25, 0., 500.);

  /*  Zg bg histos */
  DHBook1(12002, "Selection Zjets ", 10, .5, 10.5);
  DHBook1(12003, "Selection Zjets unw.", 10, .5, 10.5);
  DHBook1(12401, "MWR", 50, 0., 5000.);
  DHBook1(12402, "MN", 50, 0., 5000.);
  DHBook1(12403, "MN/MWR", 20, 0., 1.);
  DHBook1(12414, "M neutrino correct rec.", 300, 0., 6000.);
  DHBook1(12405, "MZ", 200, 0., 2000.);
  DHBook1(12406, "Mll if 2 jets", 10, 0., 300.);
  DHBook1(12301, "phidist", 80, 0., 4.);
  DHBook1(12302, "ptratio", 50, 0., 5.);
  DHBook1(12303, "ej pt", 25, 0., 500.);
  DHBook1(12304, "ej pt barrel", 25, 0., 500.);
  DHBook1(12305, "ej pt endcup", 25, 0., 500.);
  DHBook1(12306, "ej pt endcup2", 25, 0., 500.);
  DHBook1(12307, "ej ptmiss", 20, 0., 200.);
  DHBook1(12313, "ecj pt", 25, 0., 500.);
  DHBook1(12314, "ecj pt barrel", 25, 0., 500.);
  DHBook1(12315, "ecj pt endcup", 25, 0., 500.);
  DHBook1(12316, "ecj pt endcup2", 25, 0., 500.);

  /*  Wjets bg histos */
  DHBook1(13002, "Selection Wjets ", 10, .5, 10.5);
  DHBook1(13003, "Selection Wjets unw.", 10, .5, 10.5);
  DHBook1(13401, "MWR", 50, 0., 5000.);
  DHBook1(13402, "MN", 50, 0., 5000.);
  DHBook1(13403, "MN/MWR", 20, 0., 1.);
  DHBook1(13414, "M neutrino correct rec.", 300, 0., 6000.);
  DHBook1(13405, "MZ", 200, 0., 2000.);
  DHBook1(13406, "Mll if 2 jets", 10, 0., 300.);
  DHBook1(13301, "phidist", 80, 0., 4.);
  DHBook1(13302, "ptratio", 50, 0., 5.);
  DHBook1(13303, "ej pt", 25, 0., 500.);
  DHBook1(13304, "ej pt barrel", 25, 0., 500.);
  DHBook1(13305, "ej pt endcup", 25, 0., 500.);
  DHBook1(13306, "ej pt endcup2", 25, 0., 500.);
  DHBook1(13307, "ej ptmiss", 20, 0., 200.);
  DHBook1(13313, "ecj pt", 25, 0., 500.);
  DHBook1(13314, "ecj pt barrel", 25, 0., 500.);
  DHBook1(13315, "ecj pt endcup", 25, 0., 500.);
  DHBook1(13316, "ecj pt endcup2", 25, 0., 500.);

  /* Gammajets bg histos */
  DHBook1(14002, "Selection Gammajets ", 10, .5, 10.5);
  DHBook1(14003, "Selection Gammajets unw.", 10, .5, 10.5);
  DHBook1(14401, "MWR", 50, 0., 5000.);
  DHBook1(14402, "MN", 50, 0., 5000.);
  DHBook1(14403, "MN/MWR", 20, 0., 1.);
  DHBook1(14414, "M neutrino correct rec.", 300, 0., 6000.);
  DHBook1(14405, "MZ", 200, 0., 2000.);
  DHBook1(14301, "phidist", 80, 0., 4.);
  DHBook1(14302, "ptratio", 50, 0., 5.);
  DHBook1(14303, "ej pt", 25, 0., 500.);
  DHBook1(14304, "ej pt barrel", 25, 0., 500.);
  DHBook1(14305, "ej pt endcup", 25, 0., 500.);
  DHBook1(14306, "ej pt endcup2", 25, 0., 500.);
  DHBook1(14307, "ej ptmiss", 20, 0., 200.);
  DHBook1(14313, "ecj pt", 25, 0., 500.);
  DHBook1(14314, "ecj pt barrel", 25, 0., 500.);
  DHBook1(14315, "ecj pt endcup", 25, 0., 500.);
  DHBook1(14316, "ecj pt endcup2", 25, 0., 500.);

  /* QCD bg histos */
  DHBook1(15002, "Selection QCD ", 10, .5, 10.5);
  DHBook1(15003, "Selection QCD unw.", 10, .5, 10.5);
  DHBook1(15401, "MWR", 50, 0., 5000.);
  DHBook1(15402, "MN", 50, 0., 5000.);
  DHBook1(15403, "MN/MWR", 20, 0., 1.);
  DHBook1(15414, "M neutrino correct rec.", 300, 0., 6000.);
  DHBook1(15405, "MZ", 200, 0., 2000.);
  DHBook1(15301, "phidist", 80, 0., 4.);
  DHBook1(15302, "ptratio", 50, 0., 5.);
  DHBook1(15303, "pt", 25, 0., 500.);
  DHBook1(15304, "pt barrel", 50, 0., 2000.);
  DHBook1(15305, "pt endcup", 50, 0., 2000.);
  DHBook1(15306, "pthat", 100, 0., 2000.);
  DHBook1(15307, "pthat Mlljj above 1000", 100, 0., 2000.);
  DHBook1(15308, "pthat Mlljj above 1500", 100, 0., 2000.);

  /* WW bg histos */
  DHBook1(16002, "Selection WW ", 10, .5, 10.5);
  DHBook1(16003, "Selection WW unw.", 10, .5, 10.5);
  DHBook1(16401, "MWR", 50, 0., 5000.);
  DHBook1(16402, "MN", 50, 0., 5000.);
  DHBook1(16403, "MN/MWR", 20, 0., 1.);
  DHBook1(16414, "M neutrino correct rec.", 300, 0., 6000.);
  DHBook1(16405, "MZ", 200, 0., 2000.);
  DHBook1(16406, "Mll if 2 jets", 10, 0., 300.);
  DHBook1(16303, "pt", 25, 0., 500.);

  /* WZ bg histos */
  DHBook1(17002, "Selection WZ ", 10, .5, 10.5);
  DHBook1(17003, "Selection WZ unw.", 10, .5, 10.5);
  DHBook1(17401, "MWR", 50, 0., 5000.);
  DHBook1(17402, "MN", 50, 0., 5000.);
  DHBook1(17403, "MN/MWR", 20, 0., 1.);
  DHBook1(17414, "M neutrino correct rec.", 300, 0., 6000.);
  DHBook1(17405, "MZ", 200, 0., 2000.);
  DHBook1(17303, "pt", 25, 0., 500.);

  /* Other bg histos */
  DHBook1(18002, "Selection Other ", 10, .5, 10.5);
  DHBook1(18003, "Selection Other unw.", 10, .5, 10.5);
  DHBook1(18401, "MWR", 50, 0., 5000.);
  DHBook1(18402, "MN", 50, 0., 5000.);
  DHBook1(18403, "MN/MWR", 20, 0., 1.);
  DHBook1(18414, "M neutrino correct rec.", 300, 0., 6000.);
  DHBook1(18405, "MZ", 200, 0., 2000.);
  DHBook1(18303, "pt", 25, 0., 500.);

  /* DATA histos */
  DHBook1(19002, "Selection Data ", 10, .5, 10.5);
  DHBook1(19003, "Selection Data unw.", 10, .5, 10.5);
  DHBook1(19401, "MWR", 50, 0., 5000.);
  DHBook1(19402, "MN", 50, 0., 5000.);
  DHBook1(19403, "MN/MWR", 20, 0., 1.);
  DHBook1(19414, "M neutrino correct rec.", 300, 0., 6000.);
  DHBook1(19405, "MZ", 200, 0., 2000.);
  DHBook1(19406, "Mll if 2 jets", 10, 0., 300.);
  DHBook1(19301, "phidist", 80, 0., 4.);
  DHBook1(19302, "ptratio", 50, 0., 5.);
  DHBook1(19303, "ej pt", 25, 0., 500.);
  DHBook1(19304, "ej pt barrel", 25, 0., 500.);
  DHBook1(19305, "ej pt endcup", 25, 0., 500.);
  DHBook1(19306, "ej pt endcup2", 25, 0., 500.);
  DHBook1(19307, "ej ptmiss", 20, 0., 200.);
  DHBook1(19313, "ecj pt", 25, 0., 500.);
  DHBook1(19314, "ecj pt barrel", 25, 0., 500.);
  DHBook1(19315, "ecj pt endcup", 25, 0., 500.);
  DHBook1(19316, "ecj pt endcup2", 25, 0., 500.);
  DHBook1(19317, "ecj pt", 25, 0., 500.);
  DHBook1(19501, "QCD BG to e-mu", 10, 0., 300.);
  DHBook1(49001, "photon pt barrel", 25, 0., 500.);
  DHBook1(49002, "photon pt endcap", 25, 0., 500.);
  DHBook1(49003, "photon pt endcap2", 25, 0., 500.);
  DHBook1(49011, "electron pt barrel", 25, 0., 500.);
  DHBook1(49012, "electron pt endcap", 25, 0., 500.);
  DHBook1(49013, "electron pt endcap2", 25, 0., 500.);

  DHBook1(41001, "photon pt barrel", 25, 0., 500.);
  DHBook1(42001, "photon pt barrel", 25, 0., 500.);
  DHBook1(43001, "photon pt barrel", 25, 0., 500.);
  DHBook1(44001, "photon pt barrel", 25, 0., 500.);
  DHBook1(45001, "photon pt barrel", 25, 0., 500.);
  DHBook1(46001, "photon pt barrel", 25, 0., 500.);
  DHBook1(47001, "photon pt barrel", 25, 0., 500.);
  DHBook1(48001, "photon pt barrel", 25, 0., 500.);

  DHBook1(41002, "photon pt endcap", 25, 0., 500.);
  DHBook1(42002, "photon pt endcap", 25, 0., 500.);
  DHBook1(43002, "photon pt endcap", 25, 0., 500.);
  DHBook1(44002, "photon pt endcap", 25, 0., 500.);
  DHBook1(45002, "photon pt endcap", 25, 0., 500.);
  DHBook1(46002, "photon pt endcap", 25, 0., 500.);
  DHBook1(47002, "photon pt endcap", 25, 0., 500.);
  DHBook1(48002, "photon pt endcap", 25, 0., 500.);

  DHBook1(41003, "photon pt endcap2", 25, 0., 500.);
  DHBook1(42003, "photon pt endcap2", 25, 0., 500.);
  DHBook1(43003, "photon pt endcap2", 25, 0., 500.);
  DHBook1(44003, "photon pt endcap2", 25, 0., 500.);
  DHBook1(45003, "photon pt endcap2", 25, 0., 500.);
  DHBook1(46003, "photon pt endcap2", 25, 0., 500.);
  DHBook1(47003, "photon pt endcap2", 25, 0., 500.);
  DHBook1(48003, "photon pt endcap2", 25, 0., 500.);

  DHBook1(41011, "electron pt barrel", 25, 0., 500.);
  DHBook1(42011, "electron pt barrel", 25, 0., 500.);
  DHBook1(43011, "electron pt barrel", 25, 0., 500.);
  DHBook1(44011, "electron pt barrel", 25, 0., 500.);
  DHBook1(45011, "electron pt barrel", 25, 0., 500.);
  DHBook1(46011, "electron pt barrel", 25, 0., 500.);
  DHBook1(47011, "electron pt barrel", 25, 0., 500.);
  DHBook1(48011, "electron pt barrel", 25, 0., 500.);

  DHBook1(41012, "electron pt endcap", 25, 0., 500.);
  DHBook1(42012, "electron pt endcap", 25, 0., 500.);
  DHBook1(43012, "electron pt endcap", 25, 0., 500.);
  DHBook1(44012, "electron pt endcap", 25, 0., 500.);
  DHBook1(45012, "electron pt endcap", 25, 0., 500.);
  DHBook1(46012, "electron pt endcap", 25, 0., 500.);
  DHBook1(47012, "electron pt endcap", 25, 0., 500.);
  DHBook1(48012, "electron pt endcap", 25, 0., 500.);

  DHBook1(41013, "electron pt endcap2", 25, 0., 500.);
  DHBook1(42013, "electron pt endcap2", 25, 0., 500.);
  DHBook1(43013, "electron pt endcap2", 25, 0., 500.);
  DHBook1(44013, "electron pt endcap2", 25, 0., 500.);
  DHBook1(45013, "electron pt endcap2", 25, 0., 500.);
  DHBook1(46013, "electron pt endcap2", 25, 0., 500.);
  DHBook1(47013, "electron pt endcap2", 25, 0., 500.);
  DHBook1(48013, "electron pt endcap2", 25, 0., 500.);

  /* Sum over all bg histos */
  DHBook1(28484, "MN bg", 50, 0., 5000.);
  DHBook1(28487, "MWR bg", 50, 0., 5000.);
  DHBook1(28488, "MN/MWR bg", 20, 0., 1.);
  DHBook1(30303, "ej pt", 25, 0., 500.);
  DHBook1(30304, "ej pt barrel", 25, 0., 500.);
  DHBook1(30305, "ej pt endcup", 25, 0., 500.);
  DHBook1(30306, "ej pt endcup2", 25, 0., 500.);
  DHBook1(30313, "ecj pt", 25, 0., 500.);
  DHBook1(30314, "ecj pt barrel", 25, 0., 500.);
  DHBook1(30315, "ecj pt endcup", 25, 0., 500.);
  DHBook1(30316, "ecj pt endcup2", 25, 0., 500.);
  DHBook1(30401, "MWR bg without QCD", 50, 0., 5000.);
  DHBook1(30406, "Mll if 2 jets", 10, 0., 300.);


  /* Signal on top of bg histos */
  DHBook1(29484, "MN all", 100, 0., 5000.);
  DHBook1(29487, "MWR all", 100, 0., 5000.);

  /* Sum over all bg histos */
  DHBook1(38484, "M neutrino wide bin tW", 100, 0., 5000.);
  DHBook1(38487, "M WR wide bin tW", 100, 0., 5000.);

  /* Two - dimensional histos */
  DHBook2(28500, "M WR - M N bg", 60, 0., 3000., 50, 0., 2500.);
  DHBook2(29500, "M WR - M N all", 60, 0., 3000., 50, 0., 2500.);

#if 0

  /* signal histos */
  h10471 = new TH2F("h10471", "m4-m3 electron1 sg", 100, 0., 5000.,
  		    100, 0., 5000.);
  h10472 = new TH2F("h10472", "m4-m3 electron2 sg", 100, 0., 5000.,
  		    100, 0., 5000.);
  h10420 = new TH2F("h10420", "pt-pt signal", 50, 0., 2000., 50,
  		    0., 2000.);

  h10421 = new TH1F("h10421", "pt signal all", 25, 0., 2000.); 
  h10422 = new TH1F("h10422", "pt signal accepted events", 25, 0., 2000.); 
  
  h28489 = new TH1F("h28489", "M N/M WR MW.lt.900 bg", 20, 0., 1.);
  h28490 = new TH1F("h28490", "M N/M WR MW.gt.900 bg", 20, 0., 1.);
  h28491 = new TH1F("h28491", "M N/M WR sign", 20, 0., 1.); 

  h28471 = new TH2F("h28471", "m4-m3 electron1 bg", 100, 0., 5000.,
      	    100, 0., 5000.); 
  h28472 = new TH2F("h28472", "m4-m3 electron2 bg", 100, 0., 5000.,
		    100, 0., 5000.); 

  h28421 = new TH1F("h28421", "pt bg", 25, 0., 2000.);

  //---------------------------------------------------------------------------
  
#endif
}


double EtaPhiDist(double eta1, double phi1, double eta2, double phi2)
{
  double r1 = phi1 - phi2;
  if(fabs(r1-6.28318531) < fabs(r1)) r1 = r1 - 6.28318531;
  if(fabs(r1+6.28318531) < fabs(r1)) r1 = r1 + 6.28318531;
  double r2 = eta1 - eta2;
  return sqrt(r1*r1+r2*r2);
}


double fakerate(double eta, double pt, int iclosej)
{
    //cout << "iclosej = " << iclosej << endl;
    double w;
    if(fabs(eta) < 1.44) {                            // barrel
      if(!iclosej) { // no close jet
        if(pt >= 50.) w = 0.004;
        if(pt < 50.) w = 0.0024 + 0.0016*(pt-30.)/20.;
        if(pt > 60.) w = 0.0052;
      } else { // close jet
        if(pt >= 50.) w = 0.0032;
        if(pt < 50.) w = 0.0019 + 0.0013*(pt-30.)/20.;
        if(pt > 60.) w = 0.0052;
      }
    }
    if(fabs(eta) >= 1.44 && fabs(eta) < 2.) {         // endcap
      if(!iclosej) { // no close jet
        if(pt >= 50.) w = 0.01;
        if(pt < 50.) w = 0.005 + 0.005*(pt-30.)/20.;
        if(pt > 60.) w = 0.013;
        if(pt > 84.) w = 0.0068 + 0.0000784274 * pt;
      } else { // close jet
        if(pt >= 50.) w = 0.0089;
        if(pt < 50.) w = 0.0035 + 0.0054*(pt-30.)/20.;
        if(pt > 60.) w = 0.013;
        if(pt > 84.) w = 0.0068 + 0.0000784274 * pt;
      }
    }
    if(fabs(eta) >= 2.) {                             // endcap2
      if(!iclosej) { // no close jet
        if(pt >= 50.) w = 0.023;
        if(pt < 50.) w = 0.010 + 0.013*(pt-30.)/20.;
        if(pt > 60.) w = 0.0277 + 0.00004 * pt;
      } else { // close jet
        if(pt >= 50.) w = 0.0206;
        if(pt < 50.) w = 0.0086 + 0.012*(pt-30.)/20.;
        if(pt > 60.) w = 0.0277 + 0.00004 * pt;
      }
    }
    return w;
}


double fakerateA(double eta, double pt)
{
    double w;
    if(fabs(eta) < 1.44) {                            // barrel
      if(pt < 60.) w = 0.065;
      if(pt >= 60. && pt < 160.) w = 0.092 - 0.0003277*pt;
      if(pt >= 160.) w = 0.04;
    }
    if(fabs(eta) >= 1.44 && fabs(eta) < 2.) {         // endcap
      if(pt < 60.) w = 0.09;
      if(pt >= 60. && pt < 180.) w = 0.1;
      if(pt >= 180.) w = 0.04;
    }
    if(fabs(eta) >= 2.) {                             // endcap2
      if(pt < 240.) w = 0.16;
      if(pt >= 240.) w = 0.06;
    }
    return w;
}


vector<double> generate_flat10_weights(TH1D* data_npu_estimated){
   // see SimGeneral/MixingModule/python/mix_E7TeV_FlatDist10_2011EarlyData_inTimeOnly_cfi.py; copy and paste from there:
// for spring11 and summer11, if out of time is not important:
//   const double npu_probs[25] = {0.0698146584, 0.0698146584, 0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,
//          0.0630151648,0.0526654164,0.0402754482,0.0292988928,0.0194384503,0.0122016783,0.007207042,0.004003637,0.0020278322,
//          0.0010739954,0.0004595759,0.0002229748,0.0001028162,4.58337152809607E-05 /* <-- 24 */};
// for summer11 PU_S4, obtained by averaging:
   const double npu_probs[25] = {0.104109, 0.0703573, 0.0698445, 0.0698254, 0.0697054, 0.0697907, 0.0696751, 0.0694486, 0.0680332,
                                 0.0651044, 0.0598036, 0.0527395, 0.0439513, 0.0352202, 0.0266714, 0.019411, 0.0133974, 0.00898536,
                                 0.0057516, 0.00351493, 0.00212087, 0.00122891, 0.00070592, 0.000384744, 0.000219377};
   vector<double> result(25);
   double s = 0.0;
   for(int npu=0; npu<25; ++npu){
       double npu_estimated = data_npu_estimated->GetBinContent(data_npu_estimated->GetXaxis()->FindBin(npu));                              
       result[npu] = npu_estimated / npu_probs[npu];
       s += npu_estimated;
   }
   // normalize weights such that the total sum of weights over thw whole sample is 1.0, i.e., sum_i  result[i] * npu_probs[i] should be 1.0 (!)
   for(int npu=0; npu<25; ++npu){
       result[npu] /= s;
   }
   return result;
}

//=============================================================================

int procev(int ihbas, int ipr, float ww, ofstream *outSignal, ofstream *outBcg,
           int* itrig)
{
  float xmindist = 1.e5;
  int ilhnutmp[100];
  float zmassmin = 0.;
  int lisoltmp[100], nhitstmp[100];
  float xmindist1 = 1.e5;
  int indleptmp[100], ij1, ich, iok = 0;
  float eta1, eta2;
  int nle0, njg1;
  float phi1, phi2;
  //int iok1;
  int ijw1, ijw2;
  float xmindistall = 1.e5;
  int nele;
  float etaj, phij, dist;
  double primvtx[3];

  extern float NevTrig,NevZ;
  extern TH1D* pileuphisto;

  int nmuo;
  TLorentzVector psum, psumj;

  int kltmp[100];
  float ptmax, xmass, zmass;
  TLorentzVector pltmp[100];
  TVector3 vtxtmp[50];
  float xmass1 = 0., xmass2 = 0.;
  int ilemax;
  int ifisol, iilept, isumch;
  int nletmp, ij;
  float dzmass, costet, zmassi;
  int mrltmp[100];
  float chi2tmp[100], xisoltmp[100];
  int nlegood;
  float pthighl;
  int lhittmp[100], ievtype, iZ;
  bool iokkind;

  // LEPTS. 
  // ...Desired leptons found by user (PROLEP, then after CISOL check and, 
  // ...possibly, after GLOBJF), 
  // ...PL - 4-VECTORS,  KL - code :  +-1(2) for e(mu)-+ ; 
  // ...INDLEP - number of each lepton in /PLIST/, LHIT - in /HITPAR/,
  // ...MRL - origin (code of parent, =999 for SUSY particles), 
  // ...LISOL - isolation mark (0 - isolated, 1 - non-isolated in tracker, 
  // ... 2 - non-isolated in calo, 3 - non-isolated in both tracker and calo,
  // ... 4 - isolated in jet,),   NLE - number of leptons. 

//---------------------------- start executable statements ----

  int iaccept = 0;
  *itrig = 0;
    
  if (ihbas < 0) return iaccept; // exotic events, defined in routine readev 

//  if(ev.irun > 163000) goto L9;

  ievtype = 0;
    
  if (ihbas > 3000 && ihbas < 4000) {
    cout << "not implemented" << endl;
    exit(1);
  }
    
  if(ihbas < 8900 && ihbas != 5000) {
    if(ev.npileup < 25) {
      float wwadd =  generate_flat10_weights(pileuphisto)[ev.npileup];
      //cout << "N pileup vertices = " << ev.npileup << " weight = "
      //     << wwadd << endl;
      ww *= wwadd;
    } else { //should not happen as we have a weight for all simulated npileup
      ww = 0.;
    }
  }

  ifisol = 1;
    
  if (mycuts.hnumassgen > mycuts.xmmaxisol) ifisol = 0;
  nletmp = 0;
  nle0 = 0;
  pthighl = 0.;
  nele = 0;
  nmuo = 0;

  primvtx[0] = lepts.vtx[0].x();
  primvtx[1] = lepts.vtx[0].y();
  primvtx[2] = lepts.vtx[0].z();

  if(ihbas == 5000 && lepts.nle > 1) {
//    cout << "QCD with 2 leptons " << lepts.kl[0] << " " << lepts.kl[1] << endl;
  }

  for (int ile = 0; ile < lepts.nle; ile++) { // - leptons selection -----

    double pt = lepts.lepton[ile].Pt();
      
    if (ihbas > 900 && pt > mycuts.ptlept) {
      if (abs(lepts.kl[ile]) == 1 && lepts.xisol[ile] > 0.02) {
	if (lepts.ilhnu[ile] != 0) DHF1(30071, lepts.xisol[ile], 1.);
	if (lepts.ilhnu[ile] == 0) DHF1(30072, lepts.xisol[ile], 1.);
      }
      if (abs(lepts.kl[ile]) == 2 && lepts.xisol[ile] > 0.02) {
	if (lepts.ilhnu[ile] != 0) DHF1(30073, lepts.xisol[ile], 1.);
	if (lepts.ilhnu[ile] == 0) DHF1(30074, lepts.xisol[ile], 1.);
      }
    }
    double vtxxy = sqrt(lepts.vtx[ile].x()*lepts.vtx[ile].x() +
                        lepts.vtx[ile].y()*lepts.vtx[ile].y());
    if (pt > mycuts.ptlept && lepts.lisol[ile] * ifisol <= 0 &&
        fabs(lepts.vtx[ile].z()-primvtx[2]) < 1.5 &&
        vtxxy < 0.6 ) ++nle0;
      
    iokkind = false;
    if (abs(lepts.kl[ile]) == mycuts.mylept) iokkind = true;
    if (mycuts.mylept > 2) iokkind = true;

    //if(abs(lepts.kl[ile]) == 2 && fabs(lepts.lepton[ile].Eta()) > 2.1) continue;


    if (pt > mycuts.ptlept && lepts.lisol[ile] * ifisol <= 0 && iokkind
	    && lepts.nhits[ile] >= mycuts.ntrhitmin
        && lepts.chi2[ile] < mycuts.chi2max
        && fabs(lepts.xisol[ile]) < mycuts.xmaxisol[abs(lepts.kl[ile]) - 1]
        && fabs(lepts.vtx[ile].z()-primvtx[2]) < 1.5
        && vtxxy < 0.6 ) {
	
      if (pt > pthighl) pthighl = pt;
	
      if (abs(lepts.kl[ile]) == 1) ++nele;
      else if (abs(lepts.kl[ile]) == 2) ++nmuo;
	
      //if(fabs(lepts.lepton[ile].Eta()) < 1.6) continue;
      pltmp[nletmp] = lepts.lepton[ile];
//      if(ihbas == 9000 && lepts.kl[ile] == 1) { // ECAL correction in data
//        double corfact=1.01;                    // needed for reco < 22 dec
//        if(fabs(pltmp[nletmp].Eta()) > 1.44) corfact=1.08;
//        pltmp[nletmp] *= corfact;
//      }
      kltmp[nletmp] = lepts.kl[ile];
      indleptmp[nletmp] = lepts.indlep[ile];
      lhittmp[nletmp] = lepts.lhit[ile];
      mrltmp[nletmp] = lepts.mrl[ile];
      lisoltmp[nletmp] = lepts.lisol[ile];
      ilhnutmp[nletmp] = lepts.ilhnu[ile];
      nhitstmp[nletmp] = lepts.nhits[ile];
      chi2tmp[nletmp] = lepts.chi2[ile];
      xisoltmp[nletmp] = lepts.xisol[ile];
      ++nletmp;
    }
  }
  lepts.nle = nletmp;

  for (int ile = 0; ile < lepts.nle; ile++) {

    lepts.lepton[ile] = pltmp[ile];
    lepts.kl[ile] = kltmp[ile];
    lepts.indlep[ile] = indleptmp[ile];
    lepts.lhit[ile] = lhittmp[ile];
    lepts.mrl[ile] = mrltmp[ile];
    lepts.lisol[ile] = lisoltmp[ile];
    lepts.ilhnu[ile] = ilhnutmp[ile];
    lepts.nhits[ile] = nhitstmp[ile];
    lepts.chi2[ile] = chi2tmp[ile];
    lepts.xisol[ile] = xisoltmp[ile];
  }

  //DHF1(10002+ihbas, 8., ww*((double)lepts.nle));

  // remove bad jets
  njg1 = 0;
  for (ij = 0; ij < jetout.njg; ++ij) {
    if (fabs(jetout.vtx[ij].z()-primvtx[2]) > 1.5) continue;
    if (fabs(jetout.jet[ij].Eta()) > 2.5) continue;

    phij = jetout.jet[ij].Phi();
    etaj = jetout.jet[ij].Eta();
    int ibaddist=0;
    for (int il=0; il < lepts.nle; il++) { // check for leptons close to jets
      phi1 = lepts.lepton[il].Phi();
      eta1 = lepts.lepton[il].Eta();
      dist = EtaPhiDist(eta1, phi1, etaj, phij);
      if(dist < 0.495 && abs(lepts.kl[il]) == 1) { // todo: add muons to jets
        cout << "lepton close to jet, lept type = " << lepts.kl[il]
             << " pt = " << lepts.lepton[il].Pt()
             << " ptjet = " << jetout.jet[ij].Pt()
             << " dist = " << dist << endl;
        ibaddist=1;
      }
    }
    if(ibaddist) continue;

    pltmp[njg1] = jetout.jet[ij];
    vtxtmp[njg1] = jetout.vtx[ij];
    njg1++;
  }
  jetout.njg = njg1;
  for (ij = 0; ij < jetout.njg; ++ij) {
    jetout.jet[ij] = pltmp[ij];
    jetout.vtx[ij] = vtxtmp[ij];
  }

  //  QCD fake rate from data: -------------------------

#if 0
  if(ihbas == 9000) {
    for (int il=0; il < lepts.nle; il++) {
      if(abs(lepts.kl[il]) != 1) continue;
      phi1 = lepts.lepton[il].Phi();
      eta1 = lepts.lepton[il].Eta();
      double egmind=10000.;
      for (int ij=0; ij < photons.nphot; ij++) {
        phij = photons.phot[ij].Phi();
        etaj = photons.phot[ij].Eta();
        dist = EtaPhiDist(eta1, phi1, etaj, phij);
        if(dist < egmind) egmind = dist;
      }
      if(egmind > 0.15) {
        cout << "No close photon, distmin = " << egmind;
        cout << " pt = " << lepts.lepton[il].Pt();
        cout << " eta = " << lepts.lepton[il].Eta() << endl;
      }
    }
  }
#endif

  if(ihbas == 9000) {
    for (int il=0; il < lepts.nle; il++) {
      if(abs(lepts.kl[il]) != 1) continue;
      phi1 = lepts.lepton[il].Phi();
      eta1 = lepts.lepton[il].Eta();
      double egmind=10000.;
      for (int ij=0; ij < gsfelectrons.nle; ij++) {
        phij = gsfelectrons.lepton[ij].Phi();
        etaj = gsfelectrons.lepton[ij].Eta();
        dist = EtaPhiDist(eta1, phi1, etaj, phij);
        if(dist < egmind) egmind = dist;
      }
      if(egmind > 0.05) {
        cout << "No close gsfelectron, distmin = " << egmind;
        cout << " pt = " << lepts.lepton[il].Pt();
        cout << " eta = " << lepts.lepton[il].Eta() << endl;
      }
    }
  }

  string hltnames[10];
  int hltnamlen[10];
  hltnames[0] = "HLT_Photon30_CaloIdVL_v";  hltnamlen[0] = 23;
  hltnames[1] = "HLT_Photon50_CaloIdVL_v";  hltnamlen[1] = 23;
  hltnames[2] = "HLT_Photon75_CaloIdVL_v";  hltnamlen[2] = 23;
  hltnames[3] = "HLT_Photon90_CaloIdVL_v";  hltnamlen[3] = 23;
  hltnames[4] = "HLT_Photon90_v";           hltnamlen[4] = 14;
  hltnames[5] = "HLT_Photon125_CaloIdVL_v"; hltnamlen[5] = 24;
  hltnames[6] = "HLT_Photon125_v";          hltnamlen[6] = 15;
  hltnames[7] = "HLT_Photon135_CaloIdVL_v"; hltnamlen[7] = 24;
  hltnames[8] = "HLT_Photon135_v";          hltnamlen[8] = 15;

  double prescmin = 100000.;
  // triggers for the whole event:
  int iphotontrig=0;
  for ( int ib=0; ib < (ev.hltnames).size(); ib++ ) {
    for ( int in=0; in < 9; in++) {
      if ( ((ev.hltnames)[ib]).substr(0, hltnamlen[in]) == hltnames[in] ) {
        if ( (double)(ev.prescales)[ib] > 0. &&
             (double)(ev.prescales)[ib] < prescmin ) {
           prescmin = (double)(ev.prescales)[ib];
        }
      }
    }
  }
  if(prescmin < 0.5) {cout << "bad prescale" << endl; exit(1);}
  if(prescmin < 9900.) iphotontrig = 1;


  if ( gsfelectrons.nle == 1 && jetout.njg) {
    int ipair = 0;
    for (ij = 0; ij < jetout.njg; ++ij) {
      if ( EtaPhiDist( gsfelectrons.lepton[0].Eta(),
                       gsfelectrons.lepton[0].Phi(),
                       jetout.jet[ij].Eta(),
                       jetout.jet[ij].Phi() ) > 1.6) ipair = 1;
    }
    if(ipair && ev.ptMiss.Mod() < 20.) {
      
//      if( gsfelectrons.lepton[0].Pt() > 160.) {
//        cout << " pair, N bits = " << (gsfelectrons.triggers[0]).size() << endl;
//        for ( int ib=0; ib < (gsfelectrons.triggers[0]).size(); ib++ ) {
//          cout << "     " << (gsfelectrons.triggers[0])[ib] << "  "
//               << (gsfelectrons.prescales[0])[ib] << endl;
//        }
//      }

#if 0
      // trigger match:
      int iphtrig=0;
      for ( int ib=0; ib < (gsfelectrons.triggers[0]).size(); ib++ ) {
        for ( int in=0; in < 4; in++) {
          if ( ((gsfelectrons.triggers[0])[ib]).substr(0, 23) == hltnames[in] ) {
            if ( (double)(gsfelectrons.prescales[0])[ib] > 0. &&
                 (double)(gsfelectrons.prescales[0])[ib] < prescmin )
              prescmin = (double)(gsfelectrons.prescales[0])[ib];
          }
        }
        if ( ((gsfelectrons.triggers[0])[ib]).substr(0, 15) == hltnames[4] ) {
          if ( (double)(gsfelectrons.prescales[0])[ib] < prescmin ) {
            prescmin = (double)(gsfelectrons.prescales[0])[ib];
            iphtrig=1;
          }
        }
        if ( ((gsfelectrons.triggers[0])[ib]).substr(0, 24) == hltnames[5] ) {
          if ( (double)(gsfelectrons.prescales[0])[ib] < prescmin ) {
            prescmin = (double)(gsfelectrons.prescales[0])[ib];
            iphtrig=1;
          }
        }

      }
#endif

      //if( prescmin < 99000.) cout << "pt = " << gsfelectrons.lepton[0].Pt()
      //                            << " prescale = " << prescmin << endl;
//      if(gsfelectrons.lepton[0].Pt() > 160. && abs(lepts.kl[0]) == 1 ) {
//        cout << " photon trigger: " << iphotontrig << endl;
//        if(!iphotontrig) {
//          cout << "no photon trigger for hard electron = " << endl;
//          for ( int ib=0; ib < (gsfelectrons.triggers[0]).size(); ib++ ) {
//            cout << "     " << (gsfelectrons.triggers[0])[ib] << "  "
//                 << (gsfelectrons.prescales[0])[ib] << endl;
//          }
//        }
//      }

      if(prescmin > 9900.) prescmin=0.;

      if(fabs(gsfelectrons.lepton[0].Eta()) < 1.44)
        DHF1(40001+ihbas, gsfelectrons.lepton[0].Pt(), ww*prescmin);
      if(fabs(gsfelectrons.lepton[0].Eta()) >= 1.44 &&
         fabs(gsfelectrons.lepton[0].Eta()) < 2.)
        DHF1(40002+ihbas, gsfelectrons.lepton[0].Pt(), ww*prescmin);
      if(fabs(gsfelectrons.lepton[0].Eta()) >= 2.)
        DHF1(40003+ihbas, gsfelectrons.lepton[0].Pt(), ww*prescmin);

      if (lepts.nle && abs(lepts.kl[0]) == 1 ) {
        if(fabs(lepts.lepton[0].Eta()) < 1.44)
          DHF1(40011+ihbas, lepts.lepton[0].Pt(), ww*prescmin);
        if(fabs(lepts.lepton[0].Eta()) >= 1.44 &&
           fabs(lepts.lepton[0].Eta()) < 2.)
          DHF1(40012+ihbas, lepts.lepton[0].Pt(), ww*prescmin);
        if(fabs(lepts.lepton[0].Eta()) >= 2.)
          DHF1(40013+ihbas, lepts.lepton[0].Pt(), ww*prescmin);
      }

    }
  }

  //  QCD closure test: -------------------------
  if (gsfelectrons.nle == 2 && nele < 2 && jetout.njg > 0) {
    int ipair = 0;
    for (ij = 0; ij < jetout.njg; ++ij) {
      if ( EtaPhiDist( gsfelectrons.lepton[0].Eta(),
                       gsfelectrons.lepton[0].Phi(),
                       jetout.jet[ij].Eta(),
                       jetout.jet[ij].Phi() ) > 0.5 &&
           EtaPhiDist( gsfelectrons.lepton[1].Eta(),
                       gsfelectrons.lepton[1].Phi(),
                       jetout.jet[ij].Eta(),
                       jetout.jet[ij].Phi() ) > 0.5 ) ipair = 1;
    }
    if ( fabs(gsfelectrons.lepton[0].Eta()) > 1.44 &&
         fabs(gsfelectrons.lepton[1].Eta()) > 1.44 )
      ipair = 0; // require at least one in the barrel
    if ( gsfelectrons.lepton[0].Pt() < 40. ||
         gsfelectrons.lepton[1].Pt() < 40. )
      ipair = 0; // energy cut

    double xmcc = (gsfelectrons.lepton[0] + gsfelectrons.lepton[1]).M();
    int iokmass=1;
    if(fabs(xmcc - 91.) < 10.)iokmass = 0;
    if(xmcc < 12.) iokmass = 0;

    if(iphotontrig && ev.ptMiss.Mod() < 20. && iokmass && ipair) {

      double prob = fakerateA(gsfelectrons.lepton[0].Eta(), gsfelectrons.lepton[0].Pt());
      DHF1(ihbas+10313, gsfelectrons.lepton[0].Pt(), ww*prescmin*prob);
      double prob1 = fakerateA(gsfelectrons.lepton[1].Eta(), gsfelectrons.lepton[1].Pt());
      DHF1(ihbas+10313, gsfelectrons.lepton[1].Pt(), ww*prescmin*prob1);
      if(nele) DHF1(ihbas+10317, lepts.lepton[0].Pt(), ww*prescmin);

      //cout << " closure pair, nele = " << nele << " prob = " << prob
      //     << " prob1 = " << prob1 << " prescale = " << prescmin << endl;

      if(fabs(lepts.lepton[0].Eta()) < 1.44)
        DHF1(ihbas+10314, lepts.lepton[0].Pt(), ww);
      if(fabs(lepts.lepton[0].Eta()) >= 1.44 &&
         fabs(lepts.lepton[0].Eta()) < 2.)
        DHF1(ihbas+10315, lepts.lepton[0].Pt(), ww);
      if(fabs(lepts.lepton[0].Eta()) >= 2.)
        DHF1(ihbas+10316, lepts.lepton[0].Pt(), ww);

      if(ihbas && ihbas != 9000) { // ------------ BG sum ----
      if(nele) DHF1(30313, lepts.lepton[0].Pt(), ww*prescmin);
        if(fabs(lepts.lepton[0].Eta()) < 1.44)
          DHF1(30314, lepts.lepton[0].Pt(), ww);
        if(fabs(lepts.lepton[0].Eta()) >= 1.44 &&
           fabs(lepts.lepton[0].Eta()) < 2.)
          DHF1(30315, lepts.lepton[0].Pt(), ww);
        if(fabs(lepts.lepton[0].Eta()) >= 2.)
          DHF1(30316, lepts.lepton[0].Pt(), ww);
      }
    }
  }


  if (pthighl < mycuts.ptlepttrig) goto L9;
  //if (jetout.njg < 1) goto L9;
  //if (jetout.jet[0].Pt() < 20.) goto L9;
  //if (jetout.njg < 2) goto L9;
  //if (jetout.jet[1].Pt() < 40.) goto L9;

  if(ihbas != 9000) NevTrig += ww;
  *itrig = 1;

  //  Jets above threshold: 
  njg1 = 0;
  for (ij = 0; ij < jetout.njg; ++ij)
    if (jetout.jet[ij].Pt() > mycuts.ptjcut2) ++njg1;

  //  control sample: 
  if (lepts.nle > 1 && ihbas > 0) {
    xmindistall = 1000.;
      
    for (int ile = 0; ile < lepts.nle; ile++) {

      if(ihbas != 9000) {
        DHF1(30031, lepts.lepton[ile].Eta(), ww);
        DHF1(30032, lepts.lepton[ile].Eta(), ww);
      }
      if(ihbas == 9000) {
        DHF1(31031, lepts.lepton[ile].Eta(), ww);
        DHF1(31032, lepts.lepton[ile].Eta(), ww);
      }
      for (int ile1 = 0; ile1 < lepts.nle; ile1++) {
	//	cout << " ile = " << ile << "  ile1 = " << ile1;
	//	cout << " lepts.kl[ile] = " <<  lepts.kl[ile] << "  lepts.kl[ile1] = "
	//     << lepts.kl[ile] 
	//     << " ilhnu[ile] = " << lepts.ilhnu[ile] << "  ilhnu[ile1] = "
	//     << lepts.ilhnu[ile1] << endl;

	if (ile < ile1 && abs(lepts.kl[ile]) == abs(lepts.kl[ile1]) && 
	    lepts.kl[ile] * lepts.kl[ile1] < 0) {

          bool imctruth=true;
          if (lepts.kl[ile] ==  1 && lepts.ilhnu[ile] !=  -11) imctruth=false;
          if (lepts.kl[ile] == -1 && lepts.ilhnu[ile] != 11) imctruth=false;
          if (lepts.kl[ile] ==  2 && lepts.ilhnu[ile] !=  -13) imctruth=false;
          if (lepts.kl[ile] == -2 && lepts.ilhnu[ile] != 13) imctruth=false;
          imctruth=true; // take all leptons, not only matched

          //if (lepts.lepton[ile].Pt() < 30.) continue;  //goto L3389;
          //if (lepts.lepton[ile1].Pt() < 30.) continue; //goto L3389;
	  
          //if(fabs(lepts.lepton[ile].Eta()) < 1.56 ||
          //   fabs(lepts.lepton[ile1].Eta()) < 1.56 ) continue; // only E

          psum = lepts.lepton[ile] + lepts.lepton[ile1];
          xmass = psum.M();
          if(ihbas == 9000) { // data
            DHF1(31051, xmass, ww);
            if(ihbas != 5000 && xmass > 81. && xmass < 101.)
              DHF1(40101, (double)(ev.nvertices), ww);
          }
          if(imctruth && ihbas != 9000) { //MC
            DHF1(30051, xmass, ww);
	    if(ihbas != 5000 && xmass > 81. && xmass < 101.)
              DHF1(40201, (double)(ev.nvertices), ww);
          }

          if (debug) cout << "  xmass = " << xmass << endl;

          //if (xmass > 85. && xmass < 95.) { // control Z event
          if (xmass > 80. && xmass < 100.) { // control Z event 
          //if(ihbas == 9000) cout << "Z cand. found, M = " << xmass << " "
          //                       << ev.irun << " " << ev.ilumi << " "
          //                       << ev.iev << endl;
          if(ihbas != 9000) {
            DHF1(30033, lepts.lepton[ile].Eta(), ww);
            DHF1(30033, lepts.lepton[ile1].Eta(), ww);
            DHF1(30056, sqrt(lepts.vtx[ile].x()*lepts.vtx[ile].x() +
                             lepts.vtx[ile].y()*lepts.vtx[ile].y()), ww);
            DHF1(30057, lepts.vtx[ile1].z(), ww);
            DHF1(30057, lepts.vtx[ile].z(), ww);
            DHF1(30058, lepts.vtx[ile1].z() - lepts.vtx[ile].z(), ww);
            if(psum.Pt() > 20.)
              DHF1(30059, jetout.vtx[0].z()-lepts.vtx[ile].z(), ww);
          }
          if(ihbas == 9000) {
            if(lepts.nle > 3) cout << "Z cand. with 2 more lepts" << endl;      
            DHF1(31033, lepts.lepton[ile].Eta(), ww);
            DHF1(31033, lepts.lepton[ile1].Eta(), ww);
            DHF1(31056, sqrt(lepts.vtx[ile].x()*lepts.vtx[ile].x() +
                             lepts.vtx[ile].y()*lepts.vtx[ile].y()), ww);
            DHF1(31057, lepts.vtx[ile1].z(), ww);
            DHF1(31057, lepts.vtx[ile].z(), ww);
            DHF1(31058, lepts.vtx[ile1].z() - lepts.vtx[ile].z(), ww);
            if(psum.Pt() > 20.)
            DHF1(31059, jetout.vtx[0].z()-lepts.vtx[ile].z(), ww);
          }
          double pt0 = psum.Pt();
          if(ihbas == 9000) DHF1(31052, pt0, ww);
          if(imctruth && ihbas != 9000)DHF1(30052, pt0, ww);
          if(ihbas != 9000) NevZ += ww;
          double pt = lepts.lepton[ile].Pt();
          double pt1 = lepts.lepton[ile1].Pt();
          if (pt > pt1) {DHF1(30053, pt, ww);} else {DHF1(30053, pt1, ww);}
	    
          phi1 = lepts.lepton[ile].Phi();
          eta1 = lepts.lepton[ile].Eta();
          phi2 = lepts.lepton[ile1].Phi();
          eta2 = lepts.lepton[ile1].Eta();
	    
          xmindist = 1e3;
          xmindist1 = 1e3;
          double ejmindist=0., ejmindist1=0.;
          psumj.SetPxPyPzE(0., 0., 0., 0.);
	    
	    for (int ij = 0; ij < jetout.njg; ++ij) {
	      psumj += jetout.jet[ij];
	      
	      double pt = jetout.jet[ij].Pt();
	      if (pt > 30.) {
		phij = jetout.jet[ij].Phi();
		etaj = jetout.jet[ij].Eta();
		double r1 = phi1 - phij;
		double r2 = eta1 - etaj;
		dist = sqrt(r1 * r1 + r2 * r2);
		if (dist < xmindist) {xmindist = dist; ejmindist=jetout.jet[ij].Pt();}
		if (dist < xmindistall) xmindistall = dist;
		
		r1 = phi2 - phij;
		r2 = eta2 - etaj;
		dist = sqrt(r1 * r1 + r2 * r2);
		if (dist < xmindist1) {xmindist1 = dist;ejmindist1=jetout.jet[ij].Pt();}
		if (dist < xmindistall) xmindistall = dist;
	      }
	    }
	    if (lepts.xisol[ile] < 0.) lepts.xisol[ile] = 0.;
	    
	    pt = lepts.lepton[ile].Pt();
	    if (xmindist > 1.2) {
          if(ihbas == 9000) {
             //DHF1(31075, lepts.xisol[ile]*pt/ejmindist, ww);
             DHF1(31075, lepts.xisol[ile], ww);
	        DHF1(31077, pt, ww);
          } else {
            //DHF1(30075, lepts.xisol[ile]*pt/ejmindist, ww);
            DHF1(30075, lepts.xisol[ile], ww);
            DHF1(30077, pt, ww);
          }
	    }
	    if (xmindist < 0.8 && xmindist > 0.5) {
          if(ihbas == 9000) {
            //DHF1(31076, lepts.xisol[ile]*pt/ejmindist, ww);
            DHF1(31076, lepts.xisol[ile], ww);
            DHF1(31078, pt, ww);
          } else {
            //DHF1(30076, lepts.xisol[ile]*pt/ejmindist, ww);
            DHF1(30076, lepts.xisol[ile], ww);
            DHF1(30078, pt, ww);
          }
	    }
	    if (lepts.xisol[ile1] < 0.) lepts.xisol[ile1] = 0.;
	    
	    pt1 = lepts.lepton[ile1].Pt();
	    if (xmindist1 > 1.2) {
          if(ihbas == 9000) {
            //DHF1(31075, lepts.xisol[ile1]*pt1/ejmindist1, ww);
            DHF1(31075, lepts.xisol[ile1], ww);
            DHF1(31077, pt1, ww);
          } else {
            //DHF1(30075, lepts.xisol[ile1]*pt1/ejmindist1, ww);
            DHF1(30075, lepts.xisol[ile1], ww);
            DHF1(30077, pt1, ww);
          }
        }
	    if (xmindist1 < 0.8 && xmindist1 > 0.5) {
          if(ihbas == 9000) {
            //DHF1(31076, lepts.xisol[ile1]*pt1/ejmindist1, ww);
            DHF1(31076, lepts.xisol[ile1], ww);
            DHF1(31078, pt1, ww);
          } else {
            //DHF1(30076, lepts.xisol[ile1]*pt1/ejmindist1, ww);
            DHF1(30076, lepts.xisol[ile1], ww);
            DHF1(30078, pt1, ww);
          }
	    }
	   
	    psum.SetPz(0.);
	    psumj.SetPz(0.);	    
	    double angle = psum.Angle(psumj.Vect());
	    costet = cos(angle);
	    DHF1(30055, costet, ww);
	  }
	  //L3389:;
	}
      }
    }
    if (xmindistall < 999.) DHF1(30054, xmindistall, ww);
  }

  //  control sample same sign: 
  if (lepts.nle > 1 && ihbas > 0) {
    xmindist = 1000.;
    
    for (int ile = 0; ile < lepts.nle; ++ile) {
      for (int ile1 = 0; ile1 < lepts.nle; ++ile1) {

	if (ile < ile1 && abs(lepts.kl[ile]) ==  abs(lepts.kl[ile1]) && 
	    lepts.kl[ile] * lepts.kl[ile1] > 0) {

	  psum = lepts.lepton[ile] + lepts.lepton[ile1];
	  xmass = psum.M();
	  DHF1(30061, xmass, ww);
	  
	  if (xmass > 85. && xmass < 95.) { // control Z event 
	    DHF1(30062, psum.Pt(), ww);
	    double pt = lepts.lepton[ile].Pt();
	    double pt1 = lepts.lepton[ile1].Pt();
	    if (pt > pt1) DHF1(30063, pt, ww);
	    else DHF1(30063, pt1, ww);
	    
	    if (abs(lepts.ilhnu[ile]) == 11 
		|| abs(lepts.ilhnu[ile]) == 13) {
	      if (lepts.kl[ile] * lepts.ilhnu[ile] < 0)
            DHF1(30064, pt, ww);
	    }
	    if (abs(lepts.ilhnu[ile1]) == 11 
		|| abs(lepts.ilhnu[ile1]) == 13) {
	      if (lepts.kl[ile1] * lepts.ilhnu[ile1] < 0) 
 		    DHF1(30064, pt1, ww);
	    }
	  }

	}
      }
    }
  }

  //  control sample QCD:
  if (lepts.nle == 1) {
    phi1 = lepts.lepton[0].Phi();
    int iokj=0;
    int iclosej=0;
    double ptratio;
    for(ij=0; ij < jetout.njg; ij++) {
      phij = jetout.jet[ij].Phi();
      double r1 = phi1 - phij;
      if(fabs(r1-6.282) < fabs(r1)) r1 = r1-6.282;
      if(fabs(r1+6.282) < fabs(r1)) r1 = r1+6.282;
      if(fabs(r1) > 1.57) {
        iokj=1;
        ptratio = lepts.lepton[0].Pt()/jetout.jet[ij].Pt();
        dist = r1;
      }
      double r2 = lepts.lepton[0].Eta() - jetout.jet[ij].Eta();
      if(sqrt(r1*r1+r2*r2) < 1.) iclosej = 1;
      //if(dist < 0.7 && ptratio > 0.4 && ptratio < 1.4) {
    }
    if(iokj && ev.ptMiss.Mod() < 20. && !iclosej) {
      DHF1(ihbas+10301, dist, ww);
      DHF1(ihbas+10302, ptratio, ww);
      DHF1(ihbas+10303, lepts.lepton[0].Pt(), ww);
      DHF1(ihbas+10307, ev.ptMiss.Mod(), ww);
      if(fabs(lepts.lepton[0].Eta()) < 1.44)
        DHF1(ihbas+10304, lepts.lepton[0].Pt(), ww);
      if(fabs(lepts.lepton[0].Eta()) >= 1.44 &&
         fabs(lepts.lepton[0].Eta()) < 2.)
        DHF1(ihbas+10305, lepts.lepton[0].Pt(), ww);
      if(fabs(lepts.lepton[0].Eta()) >= 2.)
        DHF1(ihbas+10306, lepts.lepton[0].Pt(), ww);

      if(ihbas && ihbas != 9000) { // ------------ BG sum ----
        DHF1(30303, lepts.lepton[0].Pt(), ww);
        if(fabs(lepts.lepton[0].Eta()) < 1.44)
          DHF1(30304, lepts.lepton[0].Pt(), ww);
        if(fabs(lepts.lepton[0].Eta()) >= 1.44 &&
           fabs(lepts.lepton[0].Eta()) < 2.)
          DHF1(30305, lepts.lepton[0].Pt(), ww);
        if(fabs(lepts.lepton[0].Eta()) >= 2.)
          DHF1(30306, lepts.lepton[0].Pt(), ww);
      }
    }
  }

  //  QCD BG to e - mu sample: -------------------------
  iZ=0;
//  if(lepts.nle > 1) {
//    if(fabs( (lepts.lepton[0] + lepts.lepton[1]).M() - 91. ) < 10.) iZ=1;
//  } 
  if(lepts.nle > 0 && photons.nphot > 0 && jetout.njg > 1 && !iZ ) {
    if(lepts.lepton[0].Pt() > 60. || photons.phot[0].Pt() > 60.) {
      int iclosej = 0;
      double prob =
               fakerate(photons.phot[0].Eta(), photons.phot[0].Pt(), iclosej);
      double xmcc = (lepts.lepton[0] + photons.phot[0]).M();
      phi1 = photons.phot[0].Phi();
      int ngjemu=0;
      for(ij=0; ij < jetout.njg; ij++) {
        phij = jetout.jet[ij].Phi();
        double r1 = phi1 - phij;
        if(fabs(r1-6.282) < fabs(r1)) r1 = r1-6.282;
        if(fabs(r1+6.282) < fabs(r1)) r1 = r1+6.282;
        double r2 = photons.phot[0].Eta() - jetout.jet[ij].Eta();
        if(sqrt(r1*r1+r2*r2) > 0.5 && jetout.jet[ij].Pt() > 40. &&
           fabs(jetout.jet[ij].Eta()) < 2.5) ngjemu++;
      }
      if(ngjemu >= 2) {      
        if(ihbas == 9000) DHF1(19501, xmcc, ww * prob * 1.26); // 1.26=185/147
      }
    }
    if(photons.nphot > 1) {
    if(lepts.lepton[0].Pt() > 60. || photons.phot[1].Pt() > 60.) {
      int iclosej = 0;
      double prob =
               fakerate(photons.phot[1].Eta(), photons.phot[1].Pt(), iclosej);
      double xmcc = (lepts.lepton[0] + photons.phot[1]).M();
      phi1 = photons.phot[1].Phi();
      int ngjemu=0;
      for(ij=0; ij < jetout.njg; ij++) {
        phij = jetout.jet[ij].Phi();
        double r1 = phi1 - phij;
        if(fabs(r1-6.282) < fabs(r1)) r1 = r1-6.282;
        if(fabs(r1+6.282) < fabs(r1)) r1 = r1+6.282;
        double r2 = photons.phot[1].Eta() - jetout.jet[ij].Eta();
        if(sqrt(r1*r1+r2*r2) > 0.5 && jetout.jet[ij].Pt() > 40. &&
           fabs(jetout.jet[ij].Eta()) < 2.5) ngjemu++;
      }
      if(ngjemu >= 2) {
        if(ihbas == 9000) DHF1(19501, xmcc, ww * prob * 1.26);
      }
    }     
    }

  }

  //  signal sample studies: 
  if (lepts.nle > 1 && ihbas == 0) {

    if (debug) cout << " ^^^^^^ readev: nlepts = " <<  lepts.nle << endl;

    for (int ile = 0; ile < lepts.nle; ++ile) {
      double pt = lepts.lepton[ile].Pt();
      
      if (debug) cout << "  ile = " << ile << " Pt(ile) = " 
		      << lepts.lepton[ile].Pt() << endl;

      if (abs(lepts.ilhnu[ile]) == 11 || 
	  abs(lepts.ilhnu[ile]) == 13 && pt < 500.) {
	phi1 = lepts.lepton[ile].Phi();
	eta1 = lepts.lepton[ile].Eta();
	
	if (debug) cout << "  phi1 = " << phi1 << "  eta1 = " << eta1 << endl;

	xmindist = 1000.;
        double ejmindist=0.;
	for (int ij = 0; ij < jetout.njg; ++ij) {
	  double ptg = jetout.jet[ij].Pt();
	  if (ptg > 40.) {
	    phij = jetout.jet[ij].Phi();
	    etaj = jetout.jet[ij].Eta();
	    double r1 = phi1 - phij;
	    double r2 = eta1 - etaj;
	    dist = sqrt(r1 * r1 + r2 * r2);
	    if (dist < xmindist) {xmindist = dist; ejmindist = jetout.jet[ij].Pt();}
	  }
	}
	DHF1(30081, xmindist, ww);
	if (lepts.xisol[ile] < 0.) lepts.xisol[ile] = 0.;
	if (xmindist > 1.2) {
	  DHF1(30085, lepts.xisol[ile]*pt/ejmindist, ww);
	  DHF1(30087, pt, ww);
	}
	if (xmindist < 0.8 && xmindist > 0.5) {
	  DHF1(30086, lepts.xisol[ile]*pt/ejmindist, ww);
	  DHF1(30088, pt, ww);
	}
      }
    }
  }
  
  //  control sample ttbar:
  if (lepts.nle == 1 && jetout.njg == 4 && ihbas > 0) {
    for (int ij = 0; ij < jetout.njg; ++ij) {
      xmass = (lepts.lepton[0]+jetout.jet[ij]).M(); 
      if (xmass > 80. && xmass < 150.) {
        iok = 0;
        for (int ij1 = 0; ij1 < jetout.njg; ++ij1) {
          for (int ij2 = ij1 + 1; ij2 < jetout.njg; ++ij2) {
            if (ij1 != ij && ij2 != ij) {
              xmass1 = (jetout.jet[ij1]+jetout.jet[ij2]).M();  
              if (xmass1 > 71. && xmass1 < 90.) iok = 1;
	        }
          }
        }
        if (iok > 0) {
	      psum.SetPxPyPzE(0., 0., 0., 0.);
	      for (int ij1 = 0; ij1 < jetout.njg; ++ij1) 
	        if (ij1 != ij) psum += jetout.jet[ij1];
	      xmass1 = psum.M();
          bool truelepton=true;
          if (lepts.kl[0] ==  1 && lepts.ilhnu[0] !=  -11) truelepton=false;
          if (lepts.kl[0] ==  -1 && lepts.ilhnu[0] !=  11) truelepton=false;
          if (lepts.kl[0] ==  2 && lepts.ilhnu[0] !=  -13) truelepton=false;
          if (lepts.kl[0] ==  -2 && lepts.ilhnu[0] !=  13) truelepton=false;
        if(truelepton){
	      if(ihbas != 9000) DHF1(30091, xmass1, ww);
	      if (ihbas != 1000 && ihbas != 9000) DHF1(30092, xmass1, ww);
          if (ihbas == 1000) DHF1(30093, xmass1, ww);
          if(ihbas == 9000) DHF1(31091, xmass1, ww);
        }
        }
      }
    }
  }

  // Start selection.
  // nle0 - initial (read) isolated leptons with additional pt cut
  // lepts.nle - after additional cut on the isolation value, nhit, chi**2

  if (njg1 < 2) goto L9;

//  if(ihbas == 1000 && lepts.nle > 1) {
//    if(abs(lepts.kl[0]) == 1 && abs(lepts.kl[1]) == 1)
//      DHF1(11002, 7., ww);
//    if(abs(lepts.kl[0]) == 2 && abs(lepts.kl[1]) == 2)
//      DHF1(11002, 8., ww);
//    if(abs(lepts.kl[0]) == 1 && abs(lepts.kl[1]) == 2)
//      DHF1(11002, 9., ww);
//    if(abs(lepts.kl[0]) == 2 && abs(lepts.kl[1]) == 1)
//      DHF1(11002, 9., ww);
//  }


  if (lepts.nle < mycuts.nlegoodmin) goto L9;

  if (pthighl < mycuts.ptlept1) goto L9;

  if(fabs(lepts.lepton[0].Eta()) > 1.56 &&
     fabs(lepts.lepton[1].Eta()) > 1.56 ) goto L9; // one lepton must be barrel

  DHF1(ihbas + 10002, 1., ww); // primary selection
  DHF1(ihbas + 10003, 1., 1.);

  if (lepts.nle < mycuts.nlegoodmin || lepts.nle > 
      mycuts.nlegoodmax) goto L9;

  if (mycuts.mylept > 2 && (nele < 1 || nmuo < 1)) goto L9;
  
  if(fabs(lepts.lepton[0].Eta()) > 1.56 &&
     fabs(lepts.lepton[1].Eta()) > 1.56 ) goto L9; // one lepton must be barrel

  //if(fabs(lepts.lepton[0].Eta()) > 1.56 ||
  //   fabs(lepts.lepton[1].Eta()) > 1.56 ) goto L9; //both leptons must be barrel

  //if(fabs(lepts.lepton[0].Eta()) > 1.44 &&
  //   fabs(lepts.lepton[0].Eta()) < 1.56 ) {
  //  cout << "lepton in the gap " << ww << endl;
  //  goto L9;
  //}

  //if(fabs(lepts.lepton[1].Eta()) > 1.44 &&
  //   fabs(lepts.lepton[1].Eta()) < 1.56 ) {
  //  cout << "lepton in the gap " << ww << endl;
  //  goto L9;
  //}

  for (int il=0; il < lepts.nle; il++) { // check for leptons close to jets
    phi1 = lepts.lepton[il].Phi();
    eta1 = lepts.lepton[il].Eta();
    for (int ij = 0; ij < jetout.njg; ++ij) {
      phij = jetout.jet[ij].Phi();
      etaj = jetout.jet[ij].Eta();
      dist = EtaPhiDist(eta1, phi1, etaj, phij);
      if(dist < 0.495 && abs(lepts.kl[il]) == 1) {
        cout << "lepton close to jet: " << dist << endl;
        goto L9;
      }
    }
  }

  nlegood = 0;
  isumch = 0; //  check number of leptons and charges: 
  
  for (int ile = 0; ile < lepts.nle; ++ile) {
    ++nlegood;
    ich = lepts.kl[ile];
    if (abs(ich) > 1) ich /= 2;
    isumch += ich;
  }
  if (mycuts.ifchecksign > 0) {
    if (mycuts.isamesign <= 0 && abs(isumch) == nlegood) goto L9; // diff.sign
    if (mycuts.isamesign > 0 && abs(isumch) != nlegood) goto L9;  // same sign
  }
  DHF1(ihbas + 10002, 2., ww);
  DHF1(ihbas + 10003, 2., 1.);
  if (lepts.nle > 1) { //  cut against bg events with Z:
    zmass = 90.5;
    dzmass = 8.;
    zmassmin = 1e5;
    for (int ile = 0; ile < lepts.nle-1; ++ile) {
      for (int ile1 = ile + 1; ile1 < lepts.nle; ++ile1) {
	zmassi = (lepts.lepton[ile] + lepts.lepton[ile1]).M();
	if (zmassi < zmassmin) zmassmin = zmassi;
      }
    }
    DHF1(ihbas + 10405, zmassmin, 1.);
    if (zmassmin < mycuts.zmasscut) goto L9;
  }
  DHF1(ihbas + 10002, 3., ww);
  DHF1(ihbas + 10003, 3., 1.);
  
  DHF1(ihbas + 10406, (lepts.lepton[0]+lepts.lepton[1]).M(), ww);
  if( ihbas && ihbas != 9000)
    DHF1(30406, (lepts.lepton[0]+lepts.lepton[1]).M(), ww);

  if (lepts.nle > 0 && jetout.njg > 1) {
    nlegood = 0;
    ilemax = 0;
    ptmax = 0.;
    isumch = 0; //  check number of leptons and charges: 
    for (int ile = 0; ile < lepts.nle; ++ile) {
      ++nlegood;
      ich = lepts.kl[ile];
      if (abs(ich) > 1) ich /= 2;
      isumch += ich;
      double pt = lepts.lepton[ile].Pt();
      if (pt > ptmax) {
	ptmax = pt;
	ilemax = ile;
      }
    }
    
    double pt = ev.ptMiss.Mod();
    //DHF1(ihbas + 10404, pt, 1.);
    if (pt > mycuts.ptmmax) goto L9;
    
    //   find two jets with maximal PT: 
    ijw1 = -1;
    ijw2 = -1;
    ptmax = 0.;
    for (int ij = 0; ij < jetout.njg; ++ij) {
      double pt = jetout.jet[ij].Pt();
      if (pt > mycuts.ptjcut1 && pt > ptmax) {
	ptmax = pt;
	ijw1 = ij;
      }
    }
    ptmax = 0.;
    for (int ij = 0; ij < jetout.njg; ++ij) {
      double pt = jetout.jet[ij].Pt();
      if (pt > mycuts.ptjcut2 && ij != ijw1 && pt > ptmax) {
	ptmax = pt;
	ijw2 = ij;
      }
    }
    if (ijw1 < 0 || ijw2 < 0) goto L9;
    
    xmass2 = 0.;
    for (int ile = 0; ile < lepts.nle; ++ile) { // random choice of lepton 
      if (mycuts.iftakeminpt > 0 && ile == ilemax) continue; //goto L6661;
      int ij = ijw1;
      ij1 = ijw2;
      xmass = (jetout.jet[ij] + jetout.jet[ij1] + lepts.lepton[ile]).M();

      for (int ile1 = 0; ile1 < lepts.nle; ++ile1) { // other lepton for WR
	
	if (ile1 != ile) {
	  xmass1 = (jetout.jet[ij] + jetout.jet[ij1] + lepts.lepton[ile] + 
		    lepts.lepton[ile1]).M();

	  if (xmass1 > xmass2) xmass2 = xmass1;

	  if (ihbas > 500) { // bg 
//	    if (ile == 0) h28471->Fill(xmass1, xmass, ww); // TH2F
//	    if (ile == 1) h28472->Fill(xmass1, xmass, ww); // TH2F
	  } else { 	     // signal 
//	    if (ile == 0) h10471->Fill(xmass1, xmass, ww); // TH2F
//	    if (ile == 1) h10472->Fill(xmass1, xmass, ww); // TH2F
	  }
	}
      }
      //L6661:;
    }
    
    if (debug) cout << "  xmass2 = " << xmass2 << "  cut = " 
		    << mycuts.xmwcut << endl;

    if (xmass2 < mycuts.xmwcut) goto L9;
    DHF1(ihbas + 10002, 4., ww);
    DHF1(ihbas + 10003, 4., 1.);
    iilept = 0;
    
    for (int ile = 0; ile < lepts.nle; ++ile) { // random choice of lepton 
      if (mycuts.iftakeminpt > 0 && ile == ilemax) continue; //goto L6662;
      ++iilept;
      int ij = ijw1;
      ij1 = ijw2;
      
      psum = jetout.jet[ij] + jetout.jet[ij1] + lepts.lepton[ile];
      xmass = psum.M();
      
      if (debug) cout << " 1  xmass = " << xmass << endl;

      int ile1 = 0;
      if (ile == 0) ile1 = 1;

//      if (ihbas < 500) {  // signal 
//	double pt1 = lepts.lepton[ile1].Pt();
//	h10420->Fill(psum.Pt(), pt1, 1.); // TH2F
//	histos[10421].Fill(pt1, 1.);
//      } else histos[28421].Fill(lepts.lepton[ile1].Pt(), 1.);  // bg 
      
      DHF1(ihbas + 10402, xmass, 1.);
      DHF1(29484, xmass, ww);
      if (ihbas < 500) DHF1(10484, xmass, ww);
      if (ihbas > 500 && ihbas != 9000) DHF1(28484, xmass, ww);
      if (ihbas == 8000) DHF1(38484, xmass, ww);
      if (lepts.ilhnu[ile] == 2) DHF1(ihbas + 10424, xmass, 1.);

      for (int ile1 = 0; ile1 < lepts.nle; ++ile1) { // other lepton for Wr
	
	if (iilept == 1 && ile1 != ile) {
          xmass1 = (jetout.jet[ij] + jetout.jet[ij1] + lepts.lepton[ile] 
		    + lepts.lepton[ile1]).M();
          DHF1(ihbas + 10401, xmass1, ww);
          DHF1(ihbas + 10403, xmass/xmass1, ww);
      DHF1(29487, xmass1, ww);
      if(ihbas == 5000) {
        double pthat=-1.;
        if(ev.ievtype == 27) pthat = 45.;
        if(ev.ievtype == 28) pthat = 60.;
        if(ev.ievtype == 29) pthat = 82.5;
        if(ev.ievtype == 30) pthat = 112.5;
        if(ev.ievtype == 31) pthat = 165.;
        if(ev.ievtype == 32) pthat = 250.;
        if(ev.ievtype == 33) pthat = 400.;
        if(ev.ievtype == 34) pthat = 650.;
        if(ev.ievtype == 35) pthat = 1400.;
        DHF1(ihbas + 10306, pthat, ww);
        if(xmass1 > 1000.) DHF1(ihbas + 10307, pthat, ww);
        if(xmass1 > 1500.) DHF1(ihbas + 10308, pthat, ww);
      }

      if (ihbas > 500 && ihbas != 9000) {
        double wwwork=ww;
        if(ihbas == 5000 && mycuts.ifchecksign) wwwork=ww*0.5;
        DHF1(28487, xmass1, wwwork);
        if(ihbas != 5000) DHF1(30401, xmass1, ww);
        if (ihbas == 8000) DHF1(38487, xmass1, ww);
        DHF1(28488, xmass/xmass1, ww);
      }
      else if (ihbas < 500) {
        DHF1(10487, xmass1, ww);
      }
      if (lepts.ilhnu[ile] > 0 && lepts.ilhnu[ile1] > 0) 
	DHF1(ihbas + 10427, xmass1, 1.);
	}
      }
      if (ihbas < 500) { // 4 colomn output
	*outSignal << xmass1 << "  " << xmass << "  "  << xmass/xmass1 << "  "
               << ww << endl;
      }
      if (ihbas > 500) {
    *outBcg << xmass1 << "  " << xmass << "  "  << xmass/xmass1 << "  "
            << ww << endl;
      }
#if 0
      if (ihbas < 500) { // 3 colomn output
    *outSignal << xmass1 << "  " << xmass/xmass1 << "  "
               << ww << endl;
      }
      if (ihbas > 500) {
    *outBcg << xmass1 << "  " << xmass/xmass1 << "  "
            << ww << endl;
      }
#endif
      DHF2(29500, xmass1, xmass, ww);   // TH2
      if (ihbas > 500) DHF2(28500, xmass1, xmass, ww); // TH2
      
      if(ihbas == 9000) cout << "Data event passed, MWR = " << xmass1 << " "
                             << " MN = " << xmass 
                             << " signsp = " << lepts.kl[0]*lepts.kl[1] << " "
                             << ev.irun << " " << ev.ilumi << " "
                             << ev.iev << " "
                             << lepts.lepton[0].Eta() << " "
                             << lepts.lepton[1].Eta() << endl;
                             //<< lepts.vtx[0].z() << " " << lepts.vtx[1].z()

      if (xmass > mycuts.xmcut1 && xmass < mycuts.xmcut2) { // final accept
	
        if (debug) cout << " 2  xmass = " << xmass << endl;

        if (xmass1 > mycuts.wrmassgen - 250. &&
            xmass1 < mycuts.wrmassgen + 220.)   {
          DHF1(ihbas + 10002, 5., ww);
          DHF1(ihbas + 10003, 5., 1.);
          int ile1 = 0;
          if (ile == 0) ile1 = 1;
	
          double pt = lepts.lepton[ile1].Pt();

          if (debug) cout << "  pt = " << pt << endl;

	  iaccept = 1;
	  if (ipr > 0) {
	    cout << " Candidate found! zmassmin = " << zmassmin << endl;
	  }
	  if (ihbas == 0) DHF1(10422, pt, 1.);
        }
      }
      //L6662:;
    }
    
  }
  L9: if (debug) cout << "  iaccept = " << iaccept << endl; 
  return iaccept;
} 


//=============================================================================

int parinv(float x, float *a, float *f, int n, float *r)
{
  float c, b1, b2, b3, b4, b5, b6;
  int   k1, k2, k3;

  // Interpolation. Function F(A) is tabulated in arrays A, F with 
  // dimension N. Float argument is X. Result is put into R.

  if (x < a[0]) goto L11;
  if (x > a[n-1]) goto L4;
  k1 = 0;
  k2 = n-1;
 L2:
  k3 = k2 - k1;
  if (k3 <= 1) goto L6;
  k3 = k1 + int(k3 / 2.);
  
  if (a[k3] < x) goto L7;
  if (a[k3] == x) goto L8;
  if (a[k3] > x) goto L9;
 L7:
  k1 = k3;
  goto L2;
 L9:
  k2 = k3;
  goto L2;
 L8:
  *r = f[k3];
  return 0;
 L3:
  b1 = a[k1];
  b2 = a[k1 + 1];
  b3 = a[k1 + 2];
  b4 = f[k1];
  b5 = f[k1 + 1];
  b6 = f[k1 + 2];
  *r = b4 * ((x - b2) * (x - b3)) / ((b1 - b2) * (b1 - b3)) + 
       b5 * ((x - b1) * (x - b3)) / ((b2 - b1) * (b2 - b3)) + 
       b6 * ((x - b1) * (x - b2)) / ((b3 - b1) * (b3 - b2));
  return 0;
 L6:
  if (k2 != n) goto L3;
  k1 = n - 3;
  goto L3;
 L4:
  c = fabs(x - a[n-1]);
  if (c < 1e-8f) goto L5;
  k1 = n - 3;
 L13:
  goto L3;
 L5:
  *r = f[n-1];
  return 0;
 L11:
  c = fabs(x - a[0]);
  if (c < 1e-8) goto L12;
  k1 = 0;
  goto L13;
 L12:
  *r = f[0];
  return 0;
  /* L14: */
  *r = 0.;
  return 0;
} 

/*
//=============================================================================
//-----------------------------------------------------------------------------

 1.    HnuMassGen = 500.
       WrMassGen = 2000.

       NleGoodMin = 2          //     LRRP
       NleGoodMax = 2
       ZmassCut = 200.
       ptmmax = 1139.
       xmcut1 = 390.
       xmcut2 = 590.
       xmwcut = 1600.

       mylept = 1               // electron
       statsign = 2241          // CMSSW statictics
       cs = 0.5*1.655E-10       // for 2000-500 GeV

       file = '../dstnew/dst2000-500-cmssw16.d'

       mylept = 2               // muon
       statsign = 2241          // CMSSW statictics
       file = '../dstnew/dst2000-500-cmssw16-muons.d'

       isignfound=1

       HnuMassGen = 499.
       WrMassGen = 2000.
       cs = 1.655e-10         // for 2000-499 GeV
       statsign = 10000.*1.26 // FAMOS140 statictics
       if (mylept == 1) {
         file = '../dstnew/dstfamos140-2000-499.d';
       }



//-----------------------------------------------------------------------------

 2.    HnuMassGen = 500.
       WrMassGen = 1200.

       isignfound = 1

       NleGoodMin = 2
       NleGoodMax = 2
       ZmassCut = 200.
       ptmmax = 1139.
       xmcut1 = 370.
       xmcut2 = 610.
       xmwcut = 1000.

       cs = 1.25E-9     // for 1200-500 GeV
       statsign = 500   // CMSSW 1_4_8 - 1_6_10 statictics
       if (mylept == 1) {
       file = '../dstnew/dst1200-500-cmssw16-tskim.d';
       }	

      
//-----------------------------------------------------------------------------

 3.    HnuMassGen = 600.
       WrMassGen = 1500.
       isignfound = 1
       NleGoodMin = 2      //   LRRP1-100pb
       NleGoodMax = 2
       ZmassCut = 200.
       ptmmax = 1139.
       xmcut1 = 480.
       xmcut2 = 710.
       xmwcut = 1000.

       cs = 4.9E-10        //  for 1500-600 GeV
       statsign = 1500.    //  CMSSW 1_4_8 - 1_6_10 statictics
       if (mylept == 1) {
         file = '../dstnew/dst1500-600-cmssw16.d';
       } 
       if (mylept == 2) {
         statsign = 2000   // CMSSW statictics
         file = '../dstnew/dst1500-600-cmssw16-muons.d';
       }


//-----------------------------------------------------------------------------

 4.    HnuMassGen = 700.
       WrMassGen = 1200.
       isignfound = 1
       NleGoodMin = 2
       NleGoodMax = 2
       ZmassCut = 200.
       ptmmax = 1139.
       xmcut1 = 580.
       xmcut2 = 810.
       xmwcut = 500.

       cs = 9.067E-10     //  for 1200-700 GeV
       statsign = 500     //  CMSSW 1_4_8 - 1_6_12 statictics
       if (mylept == 1) {
         file = '../dstnew/dst1200-700e.d';
       }



//-----------------------------------------------------------------------------

 5.    HnuMassGen = 1200.
       WrMassGen = 2000.
       isignfound = 1
       NleGoodMin = 2     //   LRRP1-100pb
       NleGoodMax = 2
       ZmassCut = 200.
       ptmmax = 1139.
       xmcut1 = 1050.
       xmcut2 = 1340.
       xmwcut = 500.

       cs = 8.834E-11      // for 2000-1200 GeV
       statsign = 500      // CMSSW 1_4_8 - 1_6_12 statictics
       if (mylept == 1) {
         file = '../dstnew/dst2000-1200e.d';
       }


//-----------------------------------------------------------------------------

 6.    HnuMassGen = 1600.
       WrMassGen = 2000.
       isignfound = 1
       NleGoodMin = 2      //   LRRP1-100pb
       NleGoodMax = 2
       ZmassCut = 200.
       ptmmax = 1139.
       xmcut1 = 1000.
       xmcut2 = 1800.
       xmwcut = 1200.

       cs = 3.294E-11      // for 2000-1600 GeV
       statsign = 500      // CMSSW 1_4_8 - 1_6_12 statictics
       if (mylept == 1) {
         file = '../dstnew/dst2000-1600e.d';
       }



//-----------------------------------------------------------------------------

 7.    HnuMassGen = 1100.
       WrMassGen = 1500.
       isignfound = 1
       NleGoodMin = 2      //   LRRP1-100pb
       NleGoodMax = 2
       ZmassCut = 200.
       ptmmax = 1139.
       xmcut1 = 990.
       xmcut2 = 1200.
       xmwcut = 500.

       cs = 1.907E-10      // for 1500-1100 GeV
       statsign = 500      // CMSSW 1_4_8 - 1_6_12 statictics
       if (mylept == 1) {
         file = '../dstnew/dst1500-1100e.d';
       }



//-----------------------------------------------------------------------------

 8.    HnuMassGen = 900.
       WrMassGen = 1000.
       isignfound = 1
       NleGoodMin = 2
       NleGoodMax = 2
       ZmassCut = 200.
       ptmmax = 1139.
       xmcut1 = 800.
       xmcut2 = 990.
       xmwcut = 700.
       cs = 2.131E-10         // for 1000-900 GeV
       statsign = 10000.*1.21 // FAMOS140 statictics
       if (mylept == 1) {
         file = '../dstnew/dstfamos140-1000-900.d';
       }



//-----------------------------------------------------------------------------

 9.    HnuMassGen = 200.
       WrMassGen = 1000.
       isignfound = 1
       NleGoodMin = 2
       NleGoodMax = 2
       ZmassCut = 200.
       ptmmax = 1139.
       xmcut1 = 160.
       xmcut2 = 235.
       xmwcut = 700.
       cs = 3.46E-9             // for 1000-200 GeV
       statsign = 10000.*2.0566 // FAMOS140 statictics
       if (mylept == 1) {
          file = '../dstnew/dstfamos140-1000-200.d';
       }



//-----------------------------------------------------------------------------

 10.   HnuMassGen = 500.
       WrMassGen = 1000.
       isignfound = 1
       NleGoodMin = 2
       NleGoodMax = 2
       ZmassCut = 200.
       ptmmax = 1139.
       xmcut1 = 440.
       xmcut2 = 550.
       xmwcut = 800.
       cs = 2.364E-9          // for 1000-500 GeV
       statsign = 10000.*1.39 // FAMOS132 statictics
       if (mylept == 1) {
         file = '../dstnew/dstfamos132-1000-500.d';
       }


//-----------------------------------------------------------------------------

 11.   HnuMassGen = 800.
       WrMassGen = 2000.
       isignfound = 1
       NleGoodMin = 2
       NleGoodMax = 2
       ZmassCut = 200.
       ptmmax = 1139.
       xmcut1 = 740.
       xmcut2 = 860.
       xmwcut = 1000.

       cs = 1.375E-10          // for 2000-800 GeV
       statsign = 10000.*1.165 // FAMOS1324 statictics
       if (mylept == 1) {
          file = '../dstnew/dstfamos1324-2000-800.d';
       }



//-----------------------------------------------------------------------------

 12.   HnuMassGen = 300.
       WrMassGen = 2000.
       isignfound = 1
       NleGoodMin = 2
       NleGoodMax = 2
       ZmassCut = 200.
       ptmmax = 1139.
       xmcut1 = 225.
       xmcut2 = 370.
       xmwcut = 1500.

       cs = 1.763E-10 // for 2000-300 GeV
       statsign = 500 // CMSSW 1_4_8 - 1_6_12 statictics
       if (mylept == 1) {
         file = '../dstnew/dst2000-300e.d';
       }



//-----------------------------------------------------------------------------

 13.   HnuMassGen = 750.
       WrMassGen = 1500.
       isignfound = 1
       NleGoodMin = 2
       NleGoodMax = 2
       ZmassCut = 200.
       ptmmax = 1139.
       xmcut1 = 690.
       xmcut2 = 810.
       xmwcut = 1000.

       cs = 4.386E-10          // for 1500-750 GeV
       statsign = 10000.*1.197 // FAMOS1324 statictics
       if (mylept == 1) {
         file = '../dstnew/dstfamos132-1500-750.d';
       }


//-----------------------------------------------------------------------------

 14.   HnuMassGen = 1000.
       WrMassGen = 2000.
       isignfound = 1
       NleGoodMin = 2
       NleGoodMax = 2
       ZmassCut = 200.
       ptmmax = 1139.
       xmcut1 = 920.
       xmcut2 = 1080.
       xmwcut = 1000.

       cs = 1.159E-10          // for 2000-1000 GeV
       statsign = 10000.*1.134 // FAMOS132 statictics
       if (mylept == 1) {
         file = '../dstnew/dstfamos132-2000-1000.d';
       }


//-----------------------------------------------------------------------------

 15.   HnuMassGen = 1250.
       WrMassGen = 2500.
       isignfound = 1
       NleGoodMin = 2
       NleGoodMax = 2
       ZmassCut = 200.
       ptmmax = 1139.
       xmcut1 = 1150.
       xmcut2 = 1345.
       xmwcut = 1000.

       cs = 3.568E-11          // for 2500-1250 GeV
       statsign = 10000.*1.102 // FAMOS140 statictics
       if (mylept == 1) {
         file = '../dstnew/dstfamos140-2500-1250.d';
       }



//-----------------------------------------------------------------------------

 16.   HnuMassGen = 500.
       WrMassGen = 3000.
       isignfound = 1
       NleGoodMin = 2
       NleGoodMax = 2
       ZmassCut = 200.
       ptmmax = 1139.
       xmcut1 = 430.
       xmcut2 = 570.
       xmwcut = 1500.

       cs = 1.919E-11          // for 3000-500 GeV
       statsign = 10000.*1.217 // FAMOS140 statictics
       if (mylept == 1) {
         file = '../dstnew/dstfamos140-3000-500.d';
       }



//-----------------------------------------------------------------------------

 17.   HnuMassGen = 2300.
       WrMassGen = 3000.
       isignfound = 1
       NleGoodMin = 2
       NleGoodMax = 2
       ZmassCut = 200.
       ptmmax = 1139.
       xmcut1 = 2150.
       xmcut2 = 2440.
       xmwcut = 1500.

       cs = 4.261E-12           // for 3000-2300 GeV
       statsign = 10000.*1.0581 // FAMOS140 statictics
       if (mylept == 1) {
         file = '../dstnew/dstfamos140-3000-2300.d';
       }



//-----------------------------------------------------------------------------

 18.   HnuMassGen = 500.
       WrMassGen = 3200.
       isignfound = 1
       NleGoodMin = 2
       NleGoodMax = 2
       ZmassCut = 200.
       ptmmax = 1139.
       xmcut1 = 430.
       xmcut2 = 570.
       xmwcut = 1500.

       cs = 1.284E-11       // for 500-3200 GeV
       statsign = 499.      // ORCA statictics
       if (mylept == 1) {
         file = '../dstnew/dst3200-500-fullreco365-873.d';
       }
       if (mylept == 2) {
         file = '../dstnew/dst2000-500.d';
       }


//-----------------------------------------------------------------------------

 19.   HnuMassGen = 1300.
       isignfound = 1
       NleGoodMin = 2
       NleGoodMax = 2
       ZmassCut = 200.
       ptmmax = 1122.
       xmcut1 = 1150.
       xmcut2 = 1375.
       ptjcut1 = 100.
       ptjcut2 = 50.

       cs = 7.397e-11
       if (mylept == 1) {
         file = '../dstnew/dst1300-2000.d';
       }
       if (mylept == 2) {
         file = '../dstnew/dst1300-2000.d';
       }



//-----------------------------------------------------------------------------

 20.   HnuMassGen = 1500.
       WrMassGen = 3000.
       isignfound = 1
       NleGoodMin = 2
       NleGoodMax = 2
       ZmassCut = 200.
       ptmmax = 1122.
       xmwcut = 1000.
       xmcut1 = 1380.
       xmcut2 = 1610.
       ptjcut1 = 100.
       ptjcut2 = 50.

       cs = 1.23E-11            // for 3000-1500 GeV
       statsign = 10000.*1.0795 // FAMOS140 statictics
       if (mylept == 1) {
         file = '../dstnew/dstfamos140-3000-1500.d';
       }


//-----------------------------------------------------------------------------

 21.   HnuMassGen = 1750.
       WrMassGen = 3500.
       isignfound = 1
       NleGoodMin = 2
       NleGoodMax = 2
       ZmassCut = 200.
       ptmmax = 1122.
       xmwcut = 1000.
       xmcut1 = 1610.
       xmcut2 = 1880.
       ptjcut1 = 100.
       ptjcut2 = 50.

       cs = 4.381e-12           // for 3500-1750 GeV
       statsign = 10000.*1.0744 // FAMOS140 statictics

       if (mylept == 1) {
         file = '../dstnew/dstfamos140-3500-1750.d';
       }


//-----------------------------------------------------------------------------

 22.   HnuMassGen = 2000.
       isignfound = 1
       NleGoodMin = 2
       NleGoodMax = 2
       ZmassCut = 200.
       ptmmax = 1139.
       xmcut1 = 1850.
       xmcut2 = 2140.
       xmwcut = 1000.
       ptjcut1 = 100.
       ptjcut2 = 50.

       WrMassGen = 3200.

       cs = 5.63e-12            // for 3200-2000 GeV
       statsign = 10000.*1.0659 // FAMOS140 statictics

       if (mylept == 1) {
         file = '../dstnew/dstfamos140-3200-2000.d';
       }

       WrMassGen = 3000.

       cs = 7.256e-12          // for 2000-3000 GeV
       statsign = 500.         // ORCA statictics

       if (mylept == 1) {
         file = '../dstnew/dst3000-2000-fullreco365-873.d';
       }

       if (mylept == 2) {
         file = '../dstnew/dst3000-2000-fullreco365-873.d';
       }

//-----------------------------------------------------------------------------
//=============================================================================
*/

/*  input file 'cards.dat'
 
500.      HnuMassGen
2000.     WrMassGen
1         lepton type (1 - electron)
0         if check sign
1         if only same sign (0 - only opposite sign). Active if check sign != 0
2         NleGoodMin
2         NleGoodMax
200.      ZMassCut
1139.     PtmMax
390.      XmCut1
590.      XmCut2
1600.     XmWCut
30.       luminocity (inverse fb)
1.        gr/gl (default 1.)
2241.     statsign          // CMSSW statictics
8.275e-11 cs
../dstnew/dst2000-500-cmssw16.d // filename

*/
