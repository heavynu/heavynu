/* +++++++++++++++++++ hbook-cpp routines ++++++++++++++++++++++++ */
/* ++++++++++++++++++++++ cpp Include Files ++++++++++++++++++++++ */
#include <stdlib.h>
#include <string>
#include <iostream>
#include <fstream>
#include <math.h>
#include <map>
#include <algorithm>
/* ++++++++++++++++++++++ ROOT Include Files ++++++++++++++++++++++ */
#include <TH1F.h>
#include <TH2F.h>
#include <TF1.h>
#include <TFile.h>

/* ++++++++++++++++++++++ RD Include Files ++++++++++++++++++++++ */
#include "hist.h"

using namespace std; // since ifstream and ofstream are std::*



typedef map <int, TH1D, less<int> > HISTOMAP;
typedef map <int, TH2D, less<int> > HISTOMAP2;


//
// Non member functions:
//


HISTOMAP* DHistoMap=0;
HISTOMAP2* DHistoMap2=0;
TFile* hOutputFile=0;

void DHBook1(int IdI, const char* TitlI, int NChI, double HLowI, double HUpI)
{
  extern HISTOMAP* DHistoMap;
  extern TFile* hOutputFile;

  if(!DHistoMap) {
    DHistoMap = new HISTOMAP;
    TH1::SetDefaultSumw2();
    hOutputFile = new TFile("hist.root", "RECREATE");
  }
  char histid[6];
  sprintf(histid, "%d", IdI);
  pair<HISTOMAP::iterator, bool> result = 
    DHistoMap->insert(make_pair(IdI, TH1D(histid, TitlI, NChI, HLowI, HUpI)));
  if(!result.second) cout << "DHBook1: Histo id = " << IdI
                          << " not inserted! Check for duplicate Id" << endl;
  return;
}


void DHBook2(int IdI, const char* TitlI, int NChI, double HLowI, double HUpI,
                                   int NChYI, double HLowYI, double HUpYI)
{
  extern HISTOMAP* DHistoMap;
  extern HISTOMAP2* DHistoMap2;

  if(!DHistoMap2) DHistoMap2 = new HISTOMAP2;
  if(!DHistoMap) {
    DHistoMap = new HISTOMAP;
    hOutputFile = new TFile("hist.root", "RECREATE");
  }
  char histid[6];
  sprintf(histid, "%d", IdI);
  pair<HISTOMAP2::iterator, bool> result = 
    DHistoMap2->insert(make_pair(IdI, TH2D(histid, TitlI, NChI, HLowI, HUpI,
                                                      NChYI, HLowYI, HUpYI)));
  if(!result.second) cout << "DHBook2: Histo id = " << IdI
                          << " not inserted! Check for duplicate Id" << endl;
  return;
}


void DHBookTerm()
{
  extern HISTOMAP* DHistoMap;
  extern HISTOMAP2* DHistoMap2;
  extern TFile* hOutputFile;

  delete DHistoMap;
  DHistoMap = 0;
  delete DHistoMap2;
  DHistoMap2 = 0;
  delete hOutputFile;
  hOutputFile = 0;
  return;
}


int DHGetNY(int id)
{
#if 0
  extern HISTOMAP* DHistoMap;

  if(!DHistoMap) {cout << "DHF1: no histograms were booked!" <<endl; return -1;}
  HISTOMAP::iterator ihist = DHistoMap->find(id);
  if(ihist == DHistoMap->end()) return -1;
  return ihist->second.GetNChY();
#endif
  return 0;
}


double DHGetI(int id, int ibin)
{
  extern HISTOMAP* DHistoMap;

  if(!DHistoMap) {cout << "DHF1: no histograms were booked!" << endl; return -1;}
  HISTOMAP::iterator ihist = DHistoMap->find(id);
  if(ihist == DHistoMap->end()) return -1;
  return ihist->second.GetBinContent(ibin);
}


void DHF1(int id, double v, double w)
{
  extern HISTOMAP* DHistoMap;

  if(!DHistoMap) {cout << "DHF1: no histograms were booked!" << endl; return;}
  HISTOMAP::iterator ihist = DHistoMap->find(id);
  if(ihist == DHistoMap->end()) return;
  ihist->second.Fill(v, w);
  return;
}


void DHF2(int id, double vx, double vy, double w)
{
  extern HISTOMAP2* DHistoMap2;

  if(!DHistoMap2) {cout << "DHF2: no histograms were booked!" << endl; return;}
  HISTOMAP2::iterator ihist = DHistoMap2->find(id);
  if(ihist == DHistoMap2->end()) return;
  ihist->second.Fill(vx, vy, w);
  return;
}


#if 0
double DXYWeight(int id, double vx, double vy)
{
  extern HISTOMAP* DHistoMap;

  if(!DHistoMap) {cout << "DHF1: no histograms were booked!" << endl; return 0;}
  HISTOMAP::iterator ihist = DHistoMap->find(id);
  if(ihist == DHistoMap->end()) return 0.;
  return ihist->second.GetXYWeight(vx, vy);
}
#endif


void DHDelete(int id)
{
  extern HISTOMAP* DHistoMap;

  DHistoMap->erase(id);
}


void DHRIN()
{DHRIN("htemp.d");}


void DHRIN(const char* HistoFileName)
{
#if 0
  extern HISTOMAP* DHistoMap;
  int iok = 1, nread = 0;
  HISTO* histo;
  pair<HISTOMAP::iterator, bool> result;

  if(!DHistoMap) DHistoMap = new HISTOMAP;
  ifstream histoFile( HistoFileName );
  while(iok) {
    histo = new HISTO(histoFile); 
    if(!histo->GetNChX()) {
      delete histo;
      histo=0;
    }
    nread++;
    if(histo) {
      result = DHistoMap->insert(make_pair(histo->GetId(), *histo));
      if(!result.second) cout << "DHRIN: Histo id = " << histo->GetId()
                              << " from " << HistoFileName
                              << " not inserted! Check for duplicate Id" <<endl;
      delete histo;
    }
    if(!histo) iok=0;
  } 
//cout << "DHRIN: " << nread << " histograms read from file htemp.d" << endl;
  histoFile.close();
  return;
#endif
}


void DHROUT()
{
  extern HISTOMAP* DHistoMap;
  extern TFile* hOutputFile;

  if(!DHistoMap) return;
  hOutputFile->Write();
//  hOutputFile->Close();
}
