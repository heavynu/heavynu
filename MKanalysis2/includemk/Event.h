class Event {
public:
  Event() {;}
public:
  int irun, ilumi, iev, ievtype, npileup, nvertices;
  float truepileup;
  TVector2 ptMiss, ptMissTrue;
  std::vector<std::string> hltnames;
  std::vector<int> prescales;
};
