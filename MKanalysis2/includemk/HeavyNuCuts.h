class HeavyNuCuts {
public:
  HeavyNuCuts() {;}
public:
  float hnumassgen, wrmassgen, xmmaxisol;
  int   nlegoodmin, nlegoodmax;
  float zmasscut, ptmmax, xmcut1, xmcut2, xmwcut, ptjcut1, ptjcut2;
  int   ntrhitmin;
  float chi2max, xmaxisol[2];
  int   ifchecksign, isamesign, mylept;
  float ptlept, ptlept1, ptlepttrig;
  int   iftakeminpt;
  int   irunab;
};
