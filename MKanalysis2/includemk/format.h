#ifndef PATDump_format_h
#define PATDump_format_h

#include "Math/Vector4D.h"
#include "Math/Point3D.h"
#include "TTree.h"
#include "TFile.h"
#include "TKey.h"

#include <string>
#include <vector>
#include <iostream>
#include <algorithm>
#include <ctime>

#include "../../sampledbMK2.h"

#ifdef __MAKECINT__
#pragma link C++ class std::vector<int>+;
#endif

struct Particle {
  // 1
  int id;         // PDG id
  // 2  
  int mcId;
  // 3
  ROOT::Math::XYZTVector p4;
  // 4
  ROOT::Math::XYZPoint vertex;
  // 5
  int charge;

  // 6 HLT bits matches
   std::vector<int> bits;
  
  // 7
  float trackIso; // isolation value, for tau tauID byLooseCombinedIsolationDeltaBetaCorr3Hits
  // 8
  float ecalIso; //  isolation value, for tau tauID byMediumCombinedIsolationDeltaBetaCorr3Hits
  // 9
  float hcalIso; //  isolation value, for tau tauID byTightCombinedIsolationDeltaBetaCorr3Hits
  // 10
  float hcal1Iso; // hcal layer 1 isolation, for muons all particles isolaton  for tau ISOdiscr byCombinedIsolationDeltaBetaCorrRaw3Hits
  
  // 11
  float chi2;     // chi-squared of the fit  
                  // for tau tauID againstElectronVLooseMVA5
  // 12
  float ndof;     // NDOF of the fit 
                  //for tau ISOdiscr  byPileupWeightedIsolationRaw3Hits
  // 13
  int validHits;  // number of valid hits found
  // 14
  int lostHits;   // number of cases where track crossed a layer without getting a hit
  //+ leptonID ?
  //+ emEnergyFraction ?
  //+ photonID ?

  // 15
  float  Fem;     // for jet electromagnetic energy fraction, hadronic fraction = 1 - Fem
                  // for electron, supercluster, photon H/E  
                  // for muon p.pfIsolationR04().sumPUPt            
                  // for tau   tauID   byLoosePileupWeightedIsolation3Hits
  //jet                  
  // 16
  int numTracks;  // number of tracks in jet 
                  // for electron - HEEPID (0,1)
                  //   for tau tauID decayModeFindingNewDMs 

  // met
  // 17
  float sumEt; // for electron - VetoID (0,1)
               // for muon muonID("GlobalMuonPromptTight")
               // for tau   tauID   byMediumPileupWeightedIsolation3Hits
  // 18
  float mEtSignif; // for electron - LooseID (0,1)
                   // for muon isLooseMuon
                   // for tau   tauID   byTightPileupWeightedIsolation3Hits
  // 19
  float bDiscr; // for electron - MediumID (0,1) for Jets - bdiscr 
                // for muon isTightMuon
                // for tau   tauID  againstMuonLoose3
  // 20
  float JECunc; // for electron - TightID (0,1) for Jets - JECunc
                // for muon isSoftMuon
                // for tau   tauID  againstMuonTight3
  // 21
  int ID;       // for electron - HEEPID (-1,0,1,2,3,4), muon muonID (0,1,2,3,4,5), tauID (0,1,2,3) jet  JETID (0,1,3,5)
  // 22 shower shape info: sigmaIetaIeta for electrons, width for clusters, for tau tau ID againstElectronLooseMVA5 
                                       // for muon isHighPtMuon
  float ScEtaWidth;
  // 23 for electrons delta Phi, for tau tauID againstElectronMediumMVA5
                                       // for muon muonID("AllGlobalMuons")
  float ScPhiWidth;         
// 24 is PF ? (0,1)
  int isPFlow;
 


  float caloIso() const
  {
    return ecalIso + hcalIso;
  }
  
  float hcal2Iso() const
  {
    return hcalIso - hcal1Iso;
  }
  
  // combined relative isolation value
  float iso() const
  {
    return (0.75*ecalIso + 0.75*hcalIso + 1.0*trackIso) / p4.pt();
  }


  float MuonIso() {
      if(isPFlow==1)  {
          double max = 0.;
          if ((hcalIso + trackIso - 0.5*Fem)>0.) max = hcalIso + trackIso - 0.5*Fem ;
          return  (hcal1Iso+max) / p4.pt();
//        https://twiki.cern.ch/twiki/bin/view/CMS/SWGuideMuonIdRun2
//        (mu->pfIsolationR04().sumChargedHadronPt + max(0., mu->pfIsolationR04().sumNeutralHadronEt + mu->pfIsolationR04().sumPhotonEt - 0.5*mu->pfIsolationR04().sumPUPt))/mu->pt()
         }

      else 
         return trackIso / p4.pt();
//         mu->isolationR03().sumPt/mu->pt()

  }


  float isoHEEP() const
  {
    double xisol = 0.;
    double cut1 = 2. + 0.03*p4.Et();
    if(fabs(p4.Eta()) > 1.44) {
      if(p4.Et() <= 50.) cut1 = 2.5;
      if(p4.Et() > 50.) cut1 = 2.5+0.03*(p4.Et()-50.);
    }
    double ehisol = ecalIso + hcal1Iso;
    if(ehisol/cut1 > xisol) xisol = ehisol/cut1;
    if(fabs(p4.Eta()) > 1.44) {
      double cut2 = 0.5;
      if(hcal2Iso()/cut2 > xisol) xisol = hcal2Iso()/cut2;
    }
    double cut3 = 7.5;
    if(fabs(p4.Eta()) > 1.44) cut3 = 15.;
    if(trackIso/cut3 > xisol) xisol = trackIso/cut3;
    return xisol;
  }

  float isoHEEP32() const
  {
    double xisol = 0.;
    double cut1 = 2. + 0.03*p4.Et();
    if(fabs(p4.Eta()) > 1.44) {
      if(p4.Et() <= 50.) cut1 = 2.5;
      if(p4.Et() > 50.) cut1 = 2.5+0.03*(p4.Et()-50.);
    }
    double ehisol = ecalIso + hcal1Iso;
    if(ehisol/cut1 > xisol) xisol = ehisol/cut1;
    double cut3 = 5.;
    if(trackIso/cut3 > xisol) xisol = trackIso/cut3;
    return xisol;
  }


};

struct PrimaryVertex {
  ROOT::Math::XYZPoint pos;  // vertex position - MC/RECO
  int bunch;                 // bunch crossing  - MC
  float chi2;                //                 - RECO
  float ndof;                //                 - RECO
};

struct HLTtable {
  int irun;                   // run number
  unsigned int ievent;        // event number
  int ilumi;                  // lumi section number
  
  std::string L1menu;
  std::string HLTmenu;
  std::vector<int> prescales;
  std::vector<std::string> names;
  
  // function return true if event number 'run:event' goes starting from 'irun:ievent'
  bool isEventIncluded(const int run, const unsigned int event) const
  {
    if (run == irun)
      return (event >= ievent);
    else
      return (run >= irun);
  }
};

std::ostream& operator<<(std::ostream& os, const HLTtable& hlt)
{
  os << "HLT table:" << "\n"
     << "  run:event:lumi = " << hlt.irun << " " << hlt.ievent << " " << hlt.ilumi << "\n"
     << "  L1 menu = " << hlt.L1menu << "\n"
     << "  HLT menu = " << hlt.HLTmenu << "\n"
     << "  #bits (prescales / names) = " << hlt.prescales.size() << " / " << hlt.names.size() << "\n";
  //   << "bit \tprescale \tname" << "\n";
  
  //for (size_t i = 0; i < hlt.prescales.size(); ++i)
  //  cout << i << " \t" << hlt.prescales[i] << " \t" << hlt.names[i] << "\n";
  
  return os;
}

// check two HLT tables are equal
bool hltEqualCriterion(const HLTtable& hlt1, const HLTtable& hlt2)
{
  if (hlt1.L1menu != hlt2.L1menu) return false;
  if (hlt1.HLTmenu != hlt2.HLTmenu) return false;
  if (hlt1.prescales != hlt2.prescales) return false;
  if (hlt1.names != hlt2.names) return false;
  return true;
}

// rule for sorting of HLT tables in increasing run:event order
bool hltSortCriterion(const HLTtable& hlt1, const HLTtable& hlt2)
{
  if (hlt1.irun == hlt2.irun)
    return (hlt1.ievent < hlt2.ievent);
  else
    return (hlt1.irun < hlt2.irun);
}


typedef std::vector<HLTtable> HLTtables;

void writeHLT(TTree* t, const HLTtables& tables)
{
  HLTtable table;
  std::string* L1menu = &table.L1menu;
  std::string* HLTmenu = &table.HLTmenu;
  std::vector<int>* prescales = &table.prescales;
  std::vector<std::string>* names = &table.names;
  
  t->Branch("irun",    &table.irun,   "irun/I");
  t->Branch("ievent",  &table.ievent, "ievent/I");
  t->Branch("ilumi",   &table.ilumi,  "ilumi/I");
  t->Branch("L1menu",     &L1menu);
  t->Branch("HLTmenu",     &HLTmenu);
  t->Branch("prescales", &prescales);
  t->Branch("names",     &names);
  
  for (size_t i = 0; i < tables.size(); ++i) {
    table = tables[i];
    t->Fill();
  }
}

HLTtables readHLT(TTree* t)
{
  HLTtable table;
  std::string* L1menu = &table.L1menu;
  std::string* HLTmenu = &table.HLTmenu;
  std::vector<int>* prescales = &table.prescales;
  std::vector<std::string>* names = &table.names;
  
  // attach branches to variables
  t->SetBranchAddress("irun",    &table.irun);
  t->SetBranchAddress("ievent",  (int*) &table.ievent); // (int*) is a hack, see TheEvent::attach() for explanation
  t->SetBranchAddress("ilumi",   &table.ilumi);
  t->SetBranchAddress("L1menu",     &L1menu);
  t->SetBranchAddress("HLTmenu",     &HLTmenu);
  t->SetBranchAddress("prescales", &prescales);
  t->SetBranchAddress("names",     &names);
  
  HLTtables tables;
  
  for (int i = 0; i < t->GetEntries(); ++i) {
    t->GetEntry(i);
    tables.push_back(table);
  }
  
  // sort tables according to run:event numbers
  std::sort(tables.begin(), tables.end(), hltSortCriterion);
  
  // drop dublicates
  HLTtables::iterator range_end = std::unique(tables.begin(), tables.end(), hltEqualCriterion);
  tables.erase(range_end, tables.end());
  
  return tables;
}

std::ostream& operator<<(std::ostream& os, const Particle& p)
{
  os << "pdgId = "  << p.id
     << " mcId = "   << p.mcId
     << " p = "      << p.p4.P()
     << " pT = "     << p.p4.Pt()
     << " eta = "    << p.p4.eta()
     << " phi = "    << p.p4.phi()
     << " charge = " << p.charge
     << " matched bits = " << p.bits.size();
  return os;
}

// rule for sorting of Particles in increasing pT order
bool ptSortCriterion(const Particle& p1, const Particle& p2)
{
  return (p1.p4.Pt() > p2.p4.Pt());
}


struct TheEvent {
  // constructor for tree write
  TheEvent();
  
  // constructor for tree read
  //   [fname] - input .root file name
  TheEvent(const std::string fname);
  
  ~TheEvent();
  
  // sample info
  Sample sample;
  
  // event information
  int irun;                   // run number
  unsigned int ievent;        // event number
  int ilumi;                  // lumi section number
  float Gen_weight;           // generator weight
  float TrueNumInter;         // true number of interaction
  ULong64_t timestamp;        // time of event:
                              //   high 32 bits - standard unix time_t (number of second elapsed since 1970)
                              //   low 32 bits  - number of nanosecond ellapsed in the second
  std::vector<int>* l1Bits;   // L1 bits
  std::vector<int>* hltBits;  // HLT bits
  float weight;               // event weight
  int pid;                    // event process ID
  
  std::vector<Particle> electrons;
  std::vector<Particle> superclusters;
  std::vector<Particle> muons;
  std::vector<Particle> taus;
  std::vector<Particle> jets;
  std::vector<Particle> mets;
  std::vector<Particle> photons;
  std::vector<Particle> gens;
  
  // return HLT table bits names and prescales
  HLTtable HLT() const
  {
    // return empty table if HLT tables are missing from the file
    if (hlttables.empty()) return HLTtable();
    
    // look up HLT table for current irun:ievent number
    for (size_t i = 0; i < hlttables.size(); ++i)
      if (! hlttables[i].isEventIncluded(irun, ievent)) {
        if (i > 0)
          return hlttables[i - 1];
        else
          return HLTtable(); // return empty table if current event goes before first HLT table
      }
    
    return hlttables[hlttables.size() - 1];
  }
  
  std::vector<PrimaryVertex> pileup;     // MC info on Pile-Up
  std::vector<PrimaryVertex> vertices;   // RECO info on primary vertices
  
  // return number of interaction for given bunch crossing bx
  int num_interactions(const int bx) const
  {
    int num = 0;
    for (size_t i = 0; i < pileup.size(); ++i)
      if (pileup[i].bunch == bx)
        num++;
    
    return num;
  }
  
  // return number of In Time (bx == 0) pileup:
  int pileupIT() const {return num_interactions(0); }
  
  // return number of Out Of Time (bx != 0) pileup:
  int pileupOOT() const
  {
    return (pileup.size() - pileupIT());
  }
  
  
  // NOTE: if you want to add or remove elements from this enum, don't change
  // existing correspondence between particle name and number, because this
  // will break posibillity to correctly read previously produced data trees
  enum {ELECTRON = 1, MUON = 2, PHOTON = 3, JET = 4, MET = 5, GEN = 6, TAU = 7, SUPERCLUSTER = 8};
  
  // return total number of events in tree
  int totalEvents() { return good ? inTree->GetEntries() : 0; }
  
  // read one event
  void readEvent(const int ievt);
  
  // return time of event
  time_t time() const { return (timestamp >> 32); }
  unsigned int timens() const { return (unsigned int) timestamp; }

private:
  // declared as "private" to prevent direct use
  // for analysis, call readEvent() and use corresponding vector<>
  // of electrons, muons, jets, ...
  
  // particle list
  int n;        // total number of particles in event
  enum {MAX = 400};
  int type[MAX]; // lepton, photon, jet, met, gen
  // lepton
  int id[MAX]; // PDG id
  int mcId[MAX];
  float px[MAX], py[MAX], pz[MAX], E[MAX]; // kinematic
  float vx[MAX], vy[MAX], vz[MAX]; // coordinates of reference point (closest approach to beam)
  int charge[MAX];
  float trackIso[MAX]; // isolation value
  float ecalIso[MAX];
  float hcalIso[MAX];
  float hcal1Iso[MAX];
  float chi2[MAX];     // chi-squared of the fit
  float ndof[MAX];     // NDOF of the fit
  int validHits[MAX];  // number of valid hits found
  int lostHits[MAX];   // number of cases where track crossed a layer without getting a hit
  //+ leptonID ?
  //+ emEnergyFraction ?
  //+ photonID ?
  
  // jet
  float  Fem[MAX];     // electromagnetic and hadronic energy fractions
  int numTracks[MAX];  // number of tracks in jet
  //+ caloJet vs PFJet ?
  
  // met
  float sumEt[MAX];
  float mEtSignif[MAX];
  float bDiscr[MAX];
  float JECunc[MAX];
  int ID[MAX];
  float DeltaVertexx[MAX],DeltaVertexy[MAX],DeltaVertexz[MAX];
  float PFVertexx[MAX],PFVertexy[MAX],PFVertexz[MAX];
  float DeltaPFVertexx[MAX],DeltaPFVertexy[MAX],DeltaPFVertexz[MAX];
  float ScEtaWidth[MAX];
  float ScPhiWidth[MAX];
  int isPFlow[MAX];
  
  // matched HLT bits
  int matches[MAX];   // number of matched bits
  enum {MAXBITS = 100000};
  int n_bits;         // following is a flat array
  int bits[MAXBITS];  // for all particles
  // essentially, fields matches[], n_bits, bits[] are imitation of 2D array HLTBITS[particle][bit]
  
private:
  // list of primary vertices (mc and reco)
  int n_vert;
  enum {MAXVERT = 400};
  int vert_type[MAXVERT];   // 0 = mc, 1 = reco
  float vert_x[MAXVERT], vert_y[MAXVERT], vert_z[MAXVERT]; // vertex
  int vert_bunch[MAXVERT];  // associated bunch crossing
  float vert_chi2[MAXVERT];
  float vert_ndof[MAXVERT];

private:
  HLTtables hlttables;
  
private:
  TFile* inFile;      // input root file
  TTree* inTree;      // input tree
  bool good;          // status flag
  
public:
  operator bool() const {return good;}
  
  // =======================================
  // prepare tree for writing
  void book(TTree* t);
  
  // print event
  void print();
  
  // print all HLT tables
  void printHLT();
  
  // put particle into array
  // return index of pushed particle in internal array
  int push(int type_, int id_, int mcId_,
            ROOT::Math::XYZTVector p4_,
            ROOT::Math::XYZPoint v_,
            int charge_,
            const std::vector<int>& bits_ = std::vector<int>(),
            float trackIso_ = 0, float ecalIso_ = 0, float hcalIso_ = 0, float hcal1Iso_ = 0,
            float chi2_ = 0, float ndof_ = 0,
            int validHits_ = 0, int lostHits_ = 0,
            float Fem_ = -2., int numTracks_ = 0,
            float sumEt_ = 0, float mEtSignif_ = 0,
            float bDiscr_ = 0, float JECunc_ = 0,
            int ID_ = -101,
            float ScEtaWidth_=-1.,float ScPhiWidth_=-1.,int isPFlow_=-19 )
            
  {
    //if (n < MAX) {

      type[n] = type_;
      id[n]   = id_;
      mcId[n] = mcId_;
      px[n]   = p4_.px();
      py[n]   = p4_.py();
      pz[n]   = p4_.pz();
      E[n]    = p4_.E();
      vx[n]   = v_.x();
      vy[n]   = v_.y();
      vz[n]   = v_.z();
      charge[n] = charge_;
      trackIso[n] = trackIso_;
      ecalIso[n]  = ecalIso_;
      hcalIso[n]  = hcalIso_;
      hcal1Iso[n]  = hcal1Iso_;
      chi2[n] = chi2_;
      ndof[n] = ndof_;
      validHits[n] = validHits_;
      lostHits[n]  = lostHits_;
      Fem[n] = Fem_;
      numTracks[n] = numTracks_;
      sumEt[n] = sumEt_;
      mEtSignif[n] = mEtSignif_;
      bDiscr[n] = bDiscr_;
      JECunc[n] = JECunc_;
      ID[n] = ID_;
      ScEtaWidth[n] = ScEtaWidth_;
      ScPhiWidth[n] = ScPhiWidth_;
      isPFlow[n] = isPFlow_;   

      matches[n] = bits_.size();
      for (size_t i = 0; i < bits_.size(); ++i) {
	if (n_bits < MAXBITS) {
	  bits[n_bits] = bits_[i];
	  n_bits++;
	}
      }
      
      if (n_bits >= MAXBITS)
	//edm::LogError("UserAnalysis") << "HLT bits buffer owerflow!";
	std::cerr << "ERROR: TheEvent::push() - HLT bits buffer owerflow!" << std::endl;
      
      n++;
      
      if (n >= MAX)
	//edm::LogError("UserAnalysis") << "Dump buffer owerflow!";
	std::cerr << "ERROR: TheEvent::push() - Dump buffer owerflow!" << std::endl;
    //}

    return n - 1;
  }
  
  // put a vertex (MC or RECO)
  void add_vertex(int vert_type_, ROOT::Math::XYZPoint pos, int vert_bunch_, float vert_chi2_, float vert_ndof_)
  {
    vert_type[n_vert] = vert_type_;    // MC (empty) = -1 / MC = 0 / RECO = 1
    vert_x[n_vert] = pos.x();          // vertex position
    vert_y[n_vert] = pos.y();
    vert_z[n_vert] = pos.z();
    vert_bunch[n_vert] = vert_bunch_;  // bunch crossing
    vert_chi2[n_vert] = vert_chi2_;
    vert_ndof[n_vert] = vert_ndof_;
    n_vert++;
  }
  
  // clear event
  void clear();
  
private:
  // prepare tree for reading
  void attach(TTree* t);
  
  Particle get(int i);
};


TheEvent::TheEvent()
: sample(Sample()), inFile(0), inTree(0), good(true)
{}

TheEvent::TheEvent(const std::string fname)
: sample(Sample()), inFile(0), inTree(0), good(false)
{
  // open file
  inFile = new TFile(fname.c_str());
  if (!inFile->IsOpen()) {
    std::cerr << "ERROR: can't open file " << fname << std::endl;
    return;
  }
  
  // check file format integrity
  
  TDirectory* patdump = inFile->GetDirectory("patdump");
  if (! patdump) {
    std::cerr << "ERROR: can't find patdump/ directory inside the file" << std::endl;
    return;
  }
  
  // count number of 'data' and 'hlt' trees
  int ntot = 0;
  int ndata = 0;
  int nhlt = 0;
  
  TKey* key;
  TIter next(patdump->GetListOfKeys());
  while ((key = (TKey*) next())) {
    const TString name = key->GetName();
    //cout << name << endl;
    ntot++;
    if (name == "data") ndata++;
    if (name == "hlt") nhlt++;
  }
  
  // proper file should have one 'data' tree and (optionally) one 'hlt' tree
    const bool integrity = (ntot == 2 && ndata == 1 && nhlt == 1) || (ntot == 1 && ndata == 1 && nhlt == 0);
  
   if (!integrity) {
    std::cerr << "ERROR: incorrect file format (should be only one 'data' tree and one or zero 'hlt' tree)" << std::endl;
  //  return;
   }
  
  // get events
  inTree = (TTree*) inFile->Get("patdump/data");
  if (!inTree) {
    std::cerr << "ERROR: can't find data tree ('patdump/data')" << std::endl;
    return;
  }
  
  // lookup for "fname" sample
  const SampleDB sadb = SampleDB().find("fname", fname);
  
  if (sadb.size() == 0) {
    std::cerr << "WARNING: can't find sample " << fname << " in SampleDB,\n"
              << "         you can read sample safely, but sample description will not be available.\n"
              << "         Please, add corresponding entry to sampledb.h to supress this warning."
              << std::endl;
    
    sample.name = "unknown";
    sample.type = 3; // unknown sample
    sample.fname = fname;
    sample.CS = -1;
    sample.pid = 0;
  }
  else {
    // we have this sample in database
    sample = sadb[0];
    
    if (sadb.size() > 1)
      std::cerr << "WARNING: more then 1 sample with file name " << sample.fname
                << " found in SampleDB, first one is used" << std::endl;
  }
  
  std::cout << "INFO: opening " << sample.name << " sample (" << sample.fname << ")" << std::endl;
  
  if (sample.isIntegratedWeightPid())
    std::cout << "INFO: will read weight and process id from the tree" << std::endl;
  
  // we open .root file, get tree and find sample successfully
  good = true;
  
  attach(inTree);
  
  // read 1st event to populate fields
  readEvent(0);
  
  // get HLT table
  // try new format
  TTree* inHLT = (TTree*) inFile->Get("patdump/hlt");
  if (inHLT) {
    hlttables = readHLT(inHLT);
    return;
  }
  
  // try old format
  hlttables.resize(1);
  HLTtable& table = hlttables[0];
  const TObjArray* hltNames = (TObjArray*) inFile->Get("patdump/hltNames");
  if (hltNames) {
    for (int i = 0; i < hltNames->GetEntries(); ++i) {
      TObjString* s = (TObjString*) hltNames->At(i);
      table.names.push_back(s->GetString().Data());
    }
  }
  else {
    std::cerr << "WARNING: Names of HLT bits are missing" << std::endl;
    
    // hltBits array corresponds to 1st event
    table.names.assign(hltBits->size(), "");
  }
  
  table.irun = 0;
  table.ievent = 0;
  table.ilumi = 0;
  table.L1menu = "";
  table.HLTmenu = sample.HLTmenu;
  table.prescales = std::vector<int>(table.names.size(), 1);
}

TheEvent::~TheEvent()
{
  //delete inFile;
}

void TheEvent::book(TTree* t)
{
  l1Bits = new std::vector<int>;
  hltBits = new std::vector<int>;
  
  t->Branch("irun",    &irun,   "irun/I");
  t->Branch("ievent",  &ievent, "ievent/I");
  t->Branch("ilumi",   &ilumi,  "ilumi/I");
  t->Branch("Gen_weight",   &Gen_weight,  "Gen_weight/F");
  t->Branch("TrueNumInter",   &TrueNumInter,  "TrueNumInter/F");
  t->Branch("timestamp", &timestamp, "timestamp/l");
  t->Branch("l1Bits",  &l1Bits);
  t->Branch("hltBits", &hltBits);
  t->Branch("n",       &n,      "n/I");
  
  // particle list
  t->Branch("type",      type,      "type[n]/I");
  t->Branch("id",        id,        "id[n]/I");
  t->Branch("mcId",      mcId,      "mcId[n]/I");
  t->Branch("px",        px,        "px[n]/F");
  t->Branch("py",        py,        "py[n]/F");
  t->Branch("pz",        pz,        "pz[n]/F");
  t->Branch("E",         E,         "E[n]/F");
  t->Branch("vx",        vx,        "vx[n]/F");
  t->Branch("vy",        vy,        "vy[n]/F");
  t->Branch("vz",        vz,        "vz[n]/F");
  t->Branch("charge",    charge,    "charge[n]/I");
  t->Branch("trackIso",  trackIso,  "trackIso[n]/F");
  t->Branch("ecalIso",   ecalIso,   "ecalIso[n]/F");
  t->Branch("hcalIso",   hcalIso,   "hcalIso[n]/F");
  t->Branch("hcal1Iso",  hcal1Iso,  "hcal1Iso[n]/F");
  t->Branch("chi2",      chi2,      "chi2[n]/F");
  t->Branch("ndof",      ndof,      "ndof[n]/F");
  t->Branch("validHits", validHits, "validHits[n]/I");
  t->Branch("lostHits",  lostHits,  "lostHits[n]/I");

  t->Branch("Fem",       Fem,       "Fem[n]/F");
  t->Branch("numTracks", numTracks, "numTracks[n]/I");

  t->Branch("sumEt",     sumEt,     "sumEt[n]/F");
  t->Branch("mEtSignif", mEtSignif, "mEtSignif[n]/F");
  t->Branch("bDiscr"   , bDiscr,    "bDiscr[n]/F");
  t->Branch("JECunc"   , JECunc,    "JECunc[n]/F");
  t->Branch("ID"    , ID,     "ID[n]/I");
  t->Branch("ScEtaWidth"    , ScEtaWidth,     "ScEtaWidth[n]/F");
  t->Branch("ScPhiWidth"    , ScPhiWidth,     "ScPhiWidth[n]/F");
  t->Branch("isPFlow"    , isPFlow,     "isPFlow[n]/I");
  
  t->Branch("matches",    matches, "matches[n]/I");
  t->Branch("n_bits",     &n_bits, "n_bits/I");
  t->Branch("bits",       bits,    "bits[n_bits]/I");
  
  // primary vertices
  t->Branch("n_vert",     &n_vert,    "n_vert/I");
  t->Branch("vert_type",  vert_type,  "vert_type[n_vert]/I");
  t->Branch("vert_x",     vert_x,     "vert_x[n_vert]/F");
  t->Branch("vert_y",     vert_y,     "vert_y[n_vert]/F");
  t->Branch("vert_z",     vert_z,     "vert_z[n_vert]/F");
  t->Branch("vert_bunch", vert_bunch, "vert_bunch[n_vert]/I");
  t->Branch("vert_chi2",  vert_chi2,  "vert_chi2[n_vert]/F");
  t->Branch("vert_ndof",  vert_ndof,  "vert_ndof[n_vert]/F");
}

void TheEvent::attach(TTree* t)
{
  // init trigget bits arrays
  l1Bits = new std::vector<int>;
  hltBits = new std::vector<int>;
  
  // reset fields to default/zero values to take into
  // account the case if corresponding branch missing
  // from the tree - in general this is for compatibility
  // with old trees
  clear();
  pid = sample.pid;
  weight = sample.weight(100); // integrated luminosity 100 pb^-1
  for (int i = 0; i < MAX; ++i) {
    hcal1Iso[i] = 0;
    bDiscr[i] = 0;
    JECunc[i] = 0;
    ID[i] = 0;
    matches[i] = 0;
  }
  for (int i = 0; i < MAXVERT; ++i) {
    vert_x[i] = 0;
    vert_y[i] = 0;
    vert_z[i] = 0;
  }
  
  // attach branches to variables
  t->SetBranchAddress("irun",    &irun);
  
  // Suddenly, the event number can be higher than 2^31, and
  // the type of 'ievent' was changed accordingly: 'int' -> 'unsigned int'
  // But the data type inside out trees is still 'int', which gives the warning:
  //   Error in <TTree::SetBranchAddress>: The pointer type given "UInt_t" (13) does not correspond to the type needed "Int_t" (3) by the branch: ievent
  // Following (int*) cast is a way to suppress the warning
  // (fortunatelly both: 'int' and 'unsigned int' have same size of 4 bytes)
  t->SetBranchAddress("ievent",  (int*) &ievent);
  
  t->SetBranchAddress("ilumi",   &ilumi);
  t->SetBranchAddress("Gen_weight",   &Gen_weight);
  t->SetBranchAddress("TrueNumInter",   &TrueNumInter);
  t->SetBranchAddress("timestamp", &timestamp);
  t->SetBranchAddress("l1Bits",  &l1Bits);
  t->SetBranchAddress("hltBits", &hltBits);
  
  if (sample.isIntegratedWeightPid()) {
    t->SetBranchAddress("pid",     &pid);
    t->SetBranchAddress("weight",  &weight);
  }
  
  t->SetBranchAddress("n",       &n);
  
  // particle list
  t->SetBranchAddress("type",      type);
  t->SetBranchAddress("id",        id);
  t->SetBranchAddress("mcId",      mcId);
  t->SetBranchAddress("px",        px);
  t->SetBranchAddress("py",        py);
  t->SetBranchAddress("pz",        pz);
  t->SetBranchAddress("E",         E);
  t->SetBranchAddress("vx",        vx);
  t->SetBranchAddress("vy",        vy);
  t->SetBranchAddress("vz",        vz);
  t->SetBranchAddress("charge",    charge);
  t->SetBranchAddress("trackIso",  trackIso);
  t->SetBranchAddress("ecalIso",   ecalIso);
  t->SetBranchAddress("hcalIso",   hcalIso);
  t->SetBranchAddress("hcal1Iso",  hcal1Iso);
  t->SetBranchAddress("chi2",      chi2);
  t->SetBranchAddress("ndof",      ndof);
  t->SetBranchAddress("validHits", validHits);
  t->SetBranchAddress("lostHits",  lostHits);
  
  t->SetBranchAddress("Fem",       Fem);
  t->SetBranchAddress("numTracks", numTracks);
  
  t->SetBranchAddress("sumEt",     sumEt);
  t->SetBranchAddress("mEtSignif", mEtSignif);
  t->SetBranchAddress("bDiscr",    bDiscr);
  t->SetBranchAddress("JECunc",    JECunc);
  t->SetBranchAddress("ID",     ID);
  t->SetBranchAddress("ScEtaWidth",  ScEtaWidth);
  t->SetBranchAddress("ScPhiWidth",  ScPhiWidth);
  t->SetBranchAddress("isPFlow",  isPFlow);
  
  // HLT bits matches
  t->SetBranchAddress("matches",    matches);
  t->SetBranchAddress("n_bits",     &n_bits);
  t->SetBranchAddress("bits",       bits);
  
  // primary vertices
  t->SetBranchAddress("n_vert",     &n_vert);
  t->SetBranchAddress("vert_type",  vert_type);
  t->SetBranchAddress("vert_x",     vert_x);
  t->SetBranchAddress("vert_y",     vert_y);
  t->SetBranchAddress("vert_z",     vert_z);
  t->SetBranchAddress("vert_bunch", vert_bunch);

  t->SetBranchAddress("vert_chi2",  vert_chi2);
  t->SetBranchAddress("vert_ndof",  vert_ndof);
}

void TheEvent::clear()
{
  irun = 0;
  ievent = 0;
  ilumi = 0;
  Gen_weight = 0.;
  TrueNumInter = 0.;
  timestamp = 0;
  l1Bits->clear();
  hltBits->clear();
  weight = 0;
  pid = 0;
  n = 0;
  n_bits = 0;
  n_vert = 0;
}

void TheEvent::print() 
{
  std::cout << "==== TheEvent::print() ====\n"
       << " [run:event:lumi] = " << irun << ":" << ievent << ":" << ilumi
       << " (time = " << time_t(timestamp >> 32) << "." << time_t(timestamp & 0xFFFFFFFF) << ")"
       << " ProcessID = " << pid
       << " Gen_Weight = " << Gen_weight
       << " TrueNumInter = " << TrueNumInter   
       << " EventWeight = " << weight
       << "\n";
  
  std::cout << "HLT(" << hltBits->size() << "): ";
  for (unsigned int i = 0; i < hltBits->size(); ++i)
    if ((*hltBits)[i])
      std::cout << i << " "; 
  std::cout << "\n";
  
  std::cout << "Primary Vertices:\n";
  for (int i = 0; i < n_vert; ++i) {
    const char* name[] = {"MC", "RECO"};
    std::cout << name[vert_type[i]]
              << " Vz = " << vert_z[i]
              << ", bunch = " << vert_bunch[i]
              << ", chi2 = " << vert_chi2[i]
              << ", ndof = " << vert_ndof[i]
              << "\n";
  }
  std::cout << "\n";
  
  std::cout << "Particles:\n";
  for (int i = 0; i < n; ++i) {
    const char* name[] = {"", "ELECTRON", "MUON", "PHOTON", "JET", "MET", "GEN", "TAU", "SUPERCLUSTER"};
    std::cout << i << " "
              << std::left << std::setw(8) << name[type[i]] << std::setw(0) << " "
              << get(i) << "\n";
  }
}

void TheEvent::printHLT()
{
  std::cout << "Total number of HLT tables = " << hlttables.size() << "\n";
  for (size_t i = 0; i < hlttables.size(); ++i)
    std::cout << hlttables[i] << std::endl;
}


Particle TheEvent::get(int i)
{
  // find a padding in bits[] array corresponding to particle "i"
  int pad = 0;
  for (int j = 0; j < i; ++j) pad += matches[j];
  
  std::vector<int> bits_;
  for (int j = 0; j < matches[i]; ++j)
    bits_.push_back(bits[pad + j]);
  
  const Particle p = {
    id[i],
    mcId[i],
    ROOT::Math::XYZTVector(px[i], py[i], pz[i], E[i]),
    ROOT::Math::XYZPoint(vx[i], vy[i], vz[i]),
    charge[i],
    bits_,
    trackIso[i], ecalIso[i], hcalIso[i], hcal1Iso[i],
    chi2[i], ndof[i], validHits[i], lostHits[i],
    Fem[i], numTracks[i],
    sumEt[i], mEtSignif[i], bDiscr[i], JECunc[i], ID[i], 
    ScEtaWidth[i], ScPhiWidth[i], isPFlow[i]  
  };
  
  return p;
}

void TheEvent::readEvent(const int ievt)
{
  // read event from the tree
  inTree->GetEntry(ievt);
  
  // clear all vectors
  electrons.clear();
  superclusters.clear();
  muons.clear();
  taus.clear();
  jets.clear();
  mets.clear();
  photons.clear();
  gens.clear();
  
  for (int i = 0; i < n; ++i) {
    Particle p = get(i);
    if (p.mcId >= 0 && type[i] != GEN) {
      gens.push_back(get(p.mcId));
      p.mcId = gens.size() - 1;
    }
    
    switch (type[i]) {
      case ELECTRON: electrons.push_back(p); break;
      case SUPERCLUSTER: superclusters.push_back(p); break;
      case MUON:     muons.push_back(p);     break;
      case TAU:      taus.push_back(p);      break;
      case JET:      jets.push_back(p);      break;
      case MET:      mets.push_back(p);      break;
      case PHOTON:   photons.push_back(p);   break;
    }
  }
  
  std::sort(electrons.begin(), electrons.end(), ptSortCriterion);
  std::sort(superclusters.begin(), superclusters.end(), ptSortCriterion);
  std::sort(muons.begin(),     muons.end(),     ptSortCriterion);
  std::sort(taus.begin(),      taus.end(),      ptSortCriterion);
  std::sort(jets.begin(),      jets.end(),      ptSortCriterion);
  std::sort(mets.begin(),      mets.end(),      ptSortCriterion);
  std::sort(photons.begin(),   photons.end(),   ptSortCriterion);
  
  pileup.clear();
  vertices.clear();
  
  for (int i = 0; i < n_vert; ++i) {
    const PrimaryVertex v = {ROOT::Math::XYZPoint(vert_x[i], vert_y[i], vert_z[i]),
                             vert_bunch[i], vert_chi2[i], vert_ndof[i]};
    if      (vert_type[i] == 0) pileup.push_back(v);   // MC
    else if (vert_type[i] == 1) vertices.push_back(v); // RECO
  }
}

#endif
