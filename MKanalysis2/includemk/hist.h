void DHBook1(int IdI, const char* TitlI, int NChI, double HLowI, double HUpI);
void DHBook2(int IdI, const char* TitlI, int NChI, double HLowI, double HUpI,
                                   int NChYI, double HLowYI, double HUpYI);
void DHRIN(void);
void DHRIN(const char* HistoFileName);
void DHROUT(void);
int DHGetNY(int id);
double DHGetI(int id, int ibin);
void DHF1(int id, double v, double w);
void DHF2(int id, double vx, double vy, double w);
double DXYWeight(int id, double ValueX, double ValueY);
void DHDelete(int id);
void DHBookTerm(void);
