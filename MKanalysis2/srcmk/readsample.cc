#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <vector>
#include <map>

#include <TH1F.h>
#include <TH2F.h>
#include <TF1.h>
#include <TFile.h>
#include <TTree.h>
#include <TVector2.h>
#include <TVector3.h>
#include <TLorentzVector.h>

#include "format.h"

#include "readsample.h"
#include "Event.h"

using namespace std;

extern Lepts lepts;
extern GSFelectrons gsfelectrons;
extern Photons photons;
extern JetOut jetout;
extern Event ev;

extern bool debug;

int printtable;

int readev(ifstream *, int, float *, int *);

int readPAT(TheEvent *, int, float *, int *);

int procev(int, int, float, ofstream *, ofstream *, int *);

//=============================================================================

int readsample(const char *filename, int modread, int ihbas, float lum, 
	       float cssample, float statsample, float *intcs, float *intsample,
	       ofstream *outSignal, ofstream *outBcg)
{
  extern int printtable;
  
  printtable=0;

  *intsample = 0.;

  int ihbasr;
  float weight, sumweight=0., sumtrig=0., intw = 0.;
  
  int iaccept;
  
  int ierr = 0;
  int nread = 0;
  float weight0, weightpat;

  if (modread == 5 || modread == 6) { // read our PAT tuples

    TheEvent evt(filename);
    
    cout << "Total events: " << evt.totalEvents() << endl;
    
    weight = lum * cssample / statsample;
    weight0 = weight;
    
    int maxev = evt.totalEvents();
    //if(maxev > 0) maxev = 4;
    for (int i = 0; i < maxev; ++i) {

      ierr = readPAT(&evt, i, &weightpat, &ihbas);
      if(modread == 6) {
        //weight = weightpat;
        weight = weight0*weightpat;
      }
      if (ierr > 0) {
        cerr << "  error in the input file " << endl;
        exit(1);
      }

//      int lepttype=0;
//      for (int igen=0; igen < evt.gens.size(); igen++) {
//        if(abs(evt.gens[igen].id) == 11 || abs(evt.gens[igen].id) == 13 ||
//           abs(evt.gens[igen].id) == 15 ) {
//          lepttype = abs(evt.gens[igen].id);
//          break;
//        }
//      }
//      cout << " lepton type = " << lepttype << endl;


      if (debug) {
        cout << endl << " ===== Event number = " << ev.iev
             << " ============== " << endl;

        cout << " ----- Number of leptons = " << lepts.nle << endl;
 
        for (int j = 0; j < lepts.nle; j++) {
          cout << "     lepton: P = " << lepts.lepton[j].P() 
               << "  Pt = " <<  lepts.lepton[j].Pt()
               << "  eta = " << lepts.lepton[j].Eta()
               << "  phi = " << lepts.lepton[j].Phi()
               << "  kl = "  << lepts.kl[j] << "  ilhnu = " 
               << lepts.ilhnu[j] << endl;
          cout << "  lisol = " << lepts.lisol[j]
               << "  xisol = " << lepts.xisol[j] << endl;
        }
        cout << " ----- Number of jets = " << jetout.njg << endl;
 
        for (int j = 0; j < jetout.njg; j++) {
          cout << "     jet: P = " << jetout.jet[j].P() 
               << "  Pt = " <<  jetout.jet[j].Pt()
               << "  eta = " << jetout.jet[j].Eta() 
               << "  phi = " << jetout.jet[j].Phi() << endl;
        }
      }

      int itrig=0;
      iaccept = procev(ihbas, 0, weight, outSignal, outBcg, &itrig);

      *intsample += (float) iaccept;
      sumweight += weight;
      sumtrig += weight*itrig;
      intw += (float) iaccept * weight;
      nread++;
    }

    *intcs += intw;
  
    cout << " read: " << nread
         << " (wsum " << sumweight
         << "), trig " << sumtrig
         << " , passed: " << *intsample << ", passed(wgt): " << intw << endl;
    
    ierr = 0;
    return ierr;
  }
  
  cout << " start reading sample, file " << filename << endl;
  
  ifstream inFile(filename);
  if (inFile == NULL) {
    cout << "Cannot open input file " << filename << endl;
    exit(1);
  }

  char str[200] = {" "};

  while (!inFile.eof()) {
    
    inFile >> ev.iev;
  
    inFile.getline(str, 200);      
    ++nread;
    
    ierr = readev(&inFile, modread, &weight, &ihbasr);
    
    if (ierr > 0) {
      cout << "  error in the input file " << endl;
      exit(1);
    }
    
    if (modread >= 3) {
      weight = weight * lum / 0.1;  // lum in fb**-1 
    } else {
      weight = lum * cssample / statsample; // lum in mb**-1 
    }
    if (modread < 3) {
      int itrig;
      iaccept = procev(ihbas, 0, weight, outSignal, outBcg, &itrig);
    } else {
      int itrig;
      iaccept = procev(ihbasr, 0, weight, outSignal, outBcg, &itrig);
    }
    *intsample += (float) iaccept;
    intw += (float) iaccept * weight;
  }
  
  inFile.close();
  
  *intcs += intw;
  
  cout << " read from dst: " << nread << "  events, passed: "
       << *intsample << ",  passed(weighted): " << intw << endl << endl;
  
  return ierr;
}
		      
//=============================================================================

int readPAT(TheEvent *evt, int i, float *weight, int *ihbas) 
{
  extern int printtable;

  if (debug) cout << " ----------------- readPAT --------------" << endl;

  evt->readEvent(i);
  if (debug) evt->print();
  
  const HLTtable& hlt = evt->HLT();
  if(printtable) {
    cout << "HLT summary:" << "\n"
         << "menu = " << hlt.HLTmenu << "\n"
         << "bits = " << hlt.names.size() << "\n"
         << "bit \tprescale \tname" << endl;

    for (size_t ib = 0; ib < hlt.names.size(); ib++)
      cout << ib << " \t" << hlt.prescales[ib] <<" \t" << hlt.names[ib] << endl;
    printtable=0;

  }

  ev.irun = evt->irun;
  ev.ilumi = evt->ilumi;
  ev.iev = evt->ievent;
  //ev.npileup = evt->pileup.size();
  ev.npileup = evt->pileupIT();
  ev.truepileup = evt->TrueNumInter;
  ev.nvertices = evt->vertices.size();

  ev.ptMiss.Set(evt->mets[0].p4.px(), evt->mets[0].p4.py());

  if(ev.hltnames.size()) {
    ev.hltnames.erase(ev.hltnames.begin(),ev.hltnames.end());
    ev.prescales.erase(ev.prescales.begin(),ev.prescales.end());
  }
  for (int ib = 0; ib < (evt->hltBits)->size(); ib++) {
    if ( (*(evt->hltBits))[ib] && ib < hlt.names.size() ) {
      string tname = hlt.names[ib];
      ev.hltnames.push_back(tname);
      ev.prescales.push_back(hlt.prescales[ib]);
    }
  }

  //*weight = evt->weight;
  //*weight = evt->Gen_weight;
  float tmpweight = 1.;
  if(evt->Gen_weight < -0.001) tmpweight = -1.;
  *weight = tmpweight;
  
  if (evt->ievent > 18 && debug) exit(1); // this is just for debug

  //if (debug) cout << "  event weight = " << evt->Gen_weight << endl;

  // now we can work with vectors e.electrons[], e.muons[], ...
  
  if (debug) cout << "event " << evt->ievent << "  run = " << evt->irun << endl;
  //cout << "  electrons = " << evt->electrons.size() << "  jets = " 
  //     <<  evt->jets.size() << endl;
  
  //if ((evt->electrons.size() >= 2) && (evt->jets.size() >= 2)) {
    
  //  const float WR = (evt->electrons[0].p4 + evt->electrons[1].p4 + evt->jets[0].p4 + 
  //   		evt->jets[1].p4).M();
  //  cout << "event " << evt->ievent << ": M_WR = " << WR << endl;

  //}    

  //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 

  // Read Leptons informations

  if (debug) cout << "  number of electrons = " << evt->electrons.size()
                  << endl;

  lepts.nle = 0;
    
  int neletmp = evt->electrons.size();

  if (neletmp) {
      
    for (int i = 0; i < neletmp; ++i) {

//      cout << " electron, Nbits = " << evt->electrons[i].bits.size();
//      if(evt->electrons[i].bits.size())
//        cout << " bit1 = " << evt->electrons[i].bits[0]
//             << " name = " << hlt.names[ evt->electrons[i].bits[0] ]
//             << " prescale = " << hlt.prescales[ evt->electrons[i].bits[0] ]
//             << endl;

      //if(evt->electrons[i].ID) continue; // take only HEEP electrons, check in RUN I
      //if(evt->electrons[i].ID != 1) continue; // take only HEEP electrons
      if(evt->electrons[i].ID < 4) continue; // take only HEEP electrons
      //if(fabs(evt->electrons[i].ScPhiWidth) > 0.06) continue; // additional for HEEP 3.2

      lepts.nle++;

      int j = lepts.nle - 1;

      if (debug) cout << "  i = " << i << "  pt = " <<  evt->electrons[i].p4.Pt() 
		      << "  charge = " << evt->electrons[i].charge << endl;
      if (debug) cout << " trackIso = " << evt->electrons[i].trackIso
                      << "  caloIso = " << evt->electrons[i].caloIso() << endl; 
	
      lepts.kl[j] = 1 * evt->electrons[i].charge;
      TLorentzVector vv(evt->electrons[i].p4.px(),
                        evt->electrons[i].p4.py(),
                        evt->electrons[i].p4.pz(), evt->electrons[i].p4.e());
      lepts.lepton[j] = vv;
      TVector3 vvv(evt->electrons[i].vertex.x(),
                   evt->electrons[i].vertex.y(),
                   evt->electrons[i].vertex.z());
      lepts.vtx[j] = vvv;
      lepts.lisol[j] = 0;
      int mcid=0;
      if(evt->electrons[i].mcId > -1)
        mcid = evt->gens[evt->electrons[i].mcId].id;
      lepts.ilhnu[j] = mcid;
      lepts.nhits[j] = int(evt->electrons[i].ndof);
      lepts.chi2[j] = evt->electrons[i].chi2;
      lepts.xisol[j] = evt->electrons[i].isoHEEP();
      
    }
  }

  if (debug) cout << "  number of muons = " << evt->muons.size() << endl;
      
  if (evt->muons.size() > 0) {
      
    int nmuons=0;

    for (int i = 0; i < evt->muons.size(); ++i) {

      if(evt->muons[i].ID < 3) continue; // take only good muons

      nmuons++;

      if (debug) cout << "  i = " << i << "  pt = " <<  evt->muons[i].p4.Pt() 
		      << "  charge = " << evt->muons[i].charge << endl;
      if (debug) cout << " trackIso = " << evt->muons[i].trackIso
                      << "  caloIso = " << evt->muons[i].caloIso() << endl; 

      int j = lepts.nle + nmuons - 1;

      lepts.kl[j] = 2 * evt->muons[i].charge;
      TLorentzVector vv(evt->muons[i].p4.px(),
                        evt->muons[i].p4.py(),
                        evt->muons[i].p4.pz(), evt->muons[i].p4.e());
      lepts.lepton[j] = vv;
      TVector3 vvv(evt->muons[i].vertex.x(),
                   evt->muons[i].vertex.y(),
                   evt->muons[i].vertex.z());
      lepts.vtx[j] = vvv;
      lepts.lisol[j] = 0;
      int mcid=0;
      if(evt->muons[i].mcId > -1) mcid = evt->gens[evt->muons[i].mcId].id;
      lepts.ilhnu[j] = mcid;
      lepts.nhits[j] = int(evt->muons[i].ndof);
      lepts.chi2[j] = evt->muons[i].chi2;
      lepts.xisol[j] = evt->muons[i].iso();

    }

    lepts.nle += nmuons;

  }
  
  // Read GSF Electrons (all electrons) informations

  if (debug) cout << "  number of gsf electrons = " << evt->electrons.size()
                  << endl;

  for (int i = 0; i < 100; i++) {
    if((gsfelectrons.triggers[i]).size()) {
      (gsfelectrons.triggers[i]).erase( (gsfelectrons.triggers[i]).begin(),
                                        (gsfelectrons.triggers[i]).end() );
      (gsfelectrons.prescales[i]).erase( (gsfelectrons.prescales[i]).begin(),
                                         (gsfelectrons.prescales[i]).end() );
    }
  }

  gsfelectrons.nle = evt->electrons.size();

  if (gsfelectrons.nle > 0) {

    for (int i = 0; i < gsfelectrons.nle; ++i) {

      if(i > 99) break;
      TLorentzVector vv(evt->electrons[i].p4.px(),
                        evt->electrons[i].p4.py(),
                        evt->electrons[i].p4.pz(), evt->electrons[i].p4.e());
      gsfelectrons.lepton[i] = vv;
      TVector3 vvv(evt->electrons[i].vertex.x(),
                   evt->electrons[i].vertex.y(),
                   evt->electrons[i].vertex.z());
      gsfelectrons.vtx[i] = vvv;
      gsfelectrons.xisol[i] = evt->electrons[i].isoHEEP();
      gsfelectrons.nhits[i] = int(evt->electrons[i].ndof);
      gsfelectrons.chi2[i] = evt->electrons[i].chi2;
      gsfelectrons.lostHits[i] = evt->electrons[i].lostHits;
      gsfelectrons.HtoE[i] = evt->electrons[i].Fem;
      gsfelectrons.sigmaIetaIeta[i] = evt->electrons[i].ScEtaWidth;
//      cout << "gsfelectron widths = " << evt->electrons[i].ScEtaWidth
//           << " " <<  evt->electrons[i].ScPhiWidth << endl;

      if(evt->electrons[i].bits.size()) {
        for (int ib = 0; ib < evt->electrons[i].bits.size() ; ib++) {
          int index = evt->electrons[i].bits[ib];
          if(index < 0 || index >= hlt.names.size()) {
            if(*ihbas > 8500) cout << " bad hlt index: " << index << endl;
          }
          if(index > -1 && index < hlt.names.size()) {
            string tname = hlt.names[index];
            gsfelectrons.triggers[i].push_back(tname);
            gsfelectrons.prescales[i].push_back(hlt.prescales[index]);
          }
        }
      }

    }
  }


  // Read Photons informations

  if (debug) cout << "  number of photons = " << evt->photons.size() << endl;

  photons.nphot = 0;

  for (int i = 0; i < evt->photons.size(); ++i) {
    int icoinc=0;
    for (int j = 0; j < evt->electrons.size(); ++j) {
      double r1 = evt->photons[i].p4.Phi() - evt->electrons[j].p4.Phi();
      if(fabs(r1-6.282) < fabs(r1)) r1 = r1-6.282;
      if(fabs(r1+6.282) < fabs(r1)) r1 = r1+6.282;
      double r2 = evt->photons[i].p4.Eta() - evt->electrons[j].p4.Eta();
      if(sqrt(r1*r1+r2*r2) < 0.3) icoinc=1;
    }
    double hoe = evt->photons[i].hcalIso/evt->photons[i].p4.e();
    double ecalisocut = 6. + 0.01*evt->photons[i].p4.Pt();
    if(evt->photons[i].p4.Eta() > 1.44) {
      ecalisocut = 6.;
      if(evt->photons[i].p4.Pt() > 50.)
        ecalisocut = 6. + 0.01*(evt->photons[i].p4.Pt() - 50.);
    }
//    if(!icoinc && hoe < 0.05 && evt->photons[i].ecalIso < ecalisocut) {
//    if(hoe < 0.05 && evt->photons[i].ecalIso < ecalisocut) {
    if(hoe < 500.) {
      TLorentzVector vv(evt->photons[i].p4.px(),
                        evt->photons[i].p4.py(),
                        evt->photons[i].p4.pz(), evt->photons[i].p4.e());
      photons.phot[photons.nphot] = vv;
      photons.nphot++;
    }
  }

  // Read Jets informations
  
  if (debug) cout << "  number of jets = " << evt->jets.size() << endl;
 
  int njtmp = evt->jets.size();
    
  jetout.njg = 0;

  if (njtmp > 0) {
      
    for (int i = 0; i < njtmp; ++i) {

      if(!evt->jets[i].ID) continue; // take only good jets

      jetout.njg++;

      int j = jetout.njg - 1;

      TLorentzVector vv(evt->jets[i].p4.px(),
                        evt->jets[i].p4.py(),
                        evt->jets[i].p4.pz(), evt->jets[i].p4.e());
      jetout.jet[j] = vv;
      TVector3 vvv(evt->jets[i].vertex.x(),
                   evt->jets[i].vertex.y(),
                   evt->jets[i].vertex.z());
      jetout.vtx[j] = vvv;
      
    }
  }

  //$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$$ 

  return 0;

}

//=============================================================================

int readev(ifstream *inFile, int ifhnu, float *weight, int *ihbasr)
{
  int ifcor, itype;

  // PTMIS. 
  // ...Et missing true (neutrinos), Et miss. measured, reconstructed Z-comp.
  // ...of neutrino from W -> l nu decay (if any). 

  // LEPTS. 
  // ...Desired leptons found by user (PROLEP, then after CISOL check and, 
  // ...possibly, after GLOBJF), 
  // ...PL - 4-VECTORS,  KL - code :  +-1(2) for e(mu)-+ ; 
  // ...INDLEP - number of each lepton in /PLIST/, LHIT - in /HITPAR/,
  // ...MRL - origin (code of parent, =999 for SUSY particles), 
  // ...LISOL - isolation mark (0 - isolated, 1 - non-isolated in tracker, 
  // ... 2 - non-isolated in calo, 3 - non-isolated in both tracker and calo,
  // ... 4 - isolated in jet,),   NLE - number of leptons. 

  // NCLUS. 
  // ...Max. number of preclusters/ clusters/jets 
  // JETOUT. 
  // ...Jets found by subroutine GLOBJF 


  // ifhnu - type of events to read: 0 - old samples, incl cmsjet 
  //                                 1 - added jet origin 
  //                                 2 - added nhit, chi**2 to leptons 
  //                                 3 - chowder, weighted events 
  //                                 4 - added isol. value 

  int ierr = 1;
  *weight = 0.;
  itype = 0;

  float pl[4], pjg[4];
  float ptm[2], ptmt[2];

  char str[200] = {" "};

  if (ifhnu == 3 || ifhnu == 4) {

    *inFile >> jetout.njg;
    *inFile >> *weight;
    *inFile >> itype;
    ev.ievtype = itype;
    inFile->getline(str, 200);

  } else {

    *inFile >> jetout.njg;
    inFile->getline(str, 200);
  }

  *ihbasr = 1000;

  if (ifhnu >= 3) {

    *ihbasr = 8000;

    if (itype >= 22 && itype <= 26)   {*ihbasr = 1000;} // CSA07 ttjets 
    if (itype >= 11 && itype <= 21)   {*ihbasr = 2000;} // CSA07 Zjets 
    if (itype < 11)  {*ihbasr = 3000;}  // CSA07 Wjets 
    if (itype >= 48 && itype <= 57)   {*ihbasr = 4000;} // CSA07 Gammajets
 
    if (itype >= 27 && itype <= 47)   {*ihbasr = 5000;} // CSA07 QCD 
    //if (itype != 28)   {*ihbasr = 8000;} // keep only the QCD bin #2
    if (itype == 59) {*ihbasr = -1;}  // it is Z' M=1000 in CSA07 Gumbo 
    if (itype >= 100 && itype <= 109) {*ihbasr = 6000;} // CSA07 WWjets 
    if (itype >= 110 && itype <= 119) {*ihbasr = 7000;} // CSA07 WZjets 
  }

  if (jetout.njg > 0) {

    // read Jets informations      
    int jemarkmrkjet, jemarkihnu;
    for (int i = 0; i < jetout.njg; ++i) {
      if (ifhnu > 0) {
	  
	*inFile >> jemarkmrkjet;
	for (int j = 0; j < 4; j++) *inFile >> pjg[j];
	*inFile >> jemarkihnu;
	inFile->getline(str, 200);
	  
      } else {
	  
	*inFile >> jemarkmrkjet;
	for (int j = 0; j < 4; j++) *inFile >> pjg[j];
	inFile->getline(str, 200);
	jemarkihnu = 0;
      }

      jetout.jet[i].SetPxPyPzE(pjg[0], pjg[1], pjg[2], pjg[3]);
      jetout.vtx[i].SetXYZ(0.,0.,0.);
    }
  }

  // Read Leptons informations
  *inFile >> lepts.nle;
  inFile->getline(str, 200);
    
  if (lepts.nle > 0) {
      
    for (int i = 0; i < lepts.nle; ++i) {
	
      if (ifhnu > 1) {
	  
	*inFile >> lepts.kl[i];
	for (int j = 0; j < 4; ++j) *inFile >> pl[j];
	lepts.lepton[i].SetPxPyPzE(pl[0], pl[1], pl[2], pl[3]);
        lepts.vtx[i].SetXYZ(0.,0.,0.);
	*inFile >> lepts.lisol[i];
	*inFile >> lepts.ilhnu[i];
	*inFile >> lepts.nhits[i];
	*inFile >> lepts.chi2[i];
	*inFile >> lepts.xisol[i];
	inFile->getline(str, 200);
	  
//      } else if (ifhnu > 1) {
//	  
//	*inFile >> lepts.kl[i];
//	for (int j = 0; j < 4; ++j) *inFile >> pl[j];
//	lepts.lepton[i].SetPxPyPzE(pl[0], pl[1], pl[2], pl[3]);
//	*inFile >> lepts.lisol[i];
//	*inFile >> lepts.ilhnu[i];
//	*inFile >> lepts.nhits[i];
//	*inFile >> lepts.chi2[i];
//	lepts.xisol[i] = 0.;
//	inFile->getline(str, 200);

      } else if (ifhnu == 1) {
	
	*inFile >> lepts.kl[i];
	for (int j = 0; j < 4; ++j) *inFile >> pl[j];
	lepts.lepton[i].SetPxPyPzE(pl[0], pl[1], pl[2], pl[3]);
	*inFile >> lepts.lisol[i];
	*inFile >> lepts.ilhnu[i];
	lepts.nhits[i] = 20;
	lepts.chi2[i] = 0.;
	lepts.xisol[i] = 0.;
	inFile->getline(str, 200);
      }
	
      if (ifhnu <= 0) {
	  
	*inFile >> lepts.kl[i];
	for (int j = 0; j < 4; ++j) *inFile >> pl[j];
	lepts.lepton[i].SetPxPyPzE(pl[0], pl[1], pl[2], pl[3]);
	*inFile >> lepts.lisol[i];
	lepts.ilhnu[i] = 0;
	lepts.nhits[i] = 20;
	lepts.chi2[i] = 0.;
	lepts.xisol[i] = 0.;
	inFile->getline(str, 200);
      }

    }
  }

  *inFile >> ptm[0];
  *inFile >> ptm[1];
  ev.ptMiss.Set(ptm[0], ptm[1]);
  *inFile >> ptmt[0];
  *inFile >> ptmt[1];
  ev.ptMissTrue.Set(ptmt[0], ptmt[1]);
  inFile->getline(str, 200);

  ierr = 0;

  return ierr;
}
