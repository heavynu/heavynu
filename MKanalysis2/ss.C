void ss()
{
  Int_t ibin;
  Double_t pt, dpt, vdata, vmc, err1, err2, err, errdata, errmc;
  Float_t arg[6], arger[6], rat[6], rater[6];

  TFile* myfile = new TFile("hist.root");

  TH1D* hMC = (TH1D*)myfile->Get("30062;1");
  TH1D* hDATA = (TH1D*)myfile->Get("31062;1");

  for (ibin=2; ibin <= 5; ibin++) {
    pt = 50.*((double)ibin) - 25.;
    if(ibin == 2) pt = 80.;
    dpt = 25.;
    if(ibin == 2) dpt = 20.;
    r = hDATA->GetBinContent(ibin)/hMC->GetBinContent(ibin);
    err1 = hDATA->GetBinError(ibin)/hDATA->GetBinContent(ibin);
    err2 = hMC->GetBinError(ibin)/hMC->GetBinContent(ibin);
    err = sqrt(err1*err1 + err2*err2);
    err *= r;
    cout << pt << " +- " << dpt << "    " << r << " +- " << err << endl;
    arg[ibin-2] = pt;
    arger[ibin-2] = dpt;
    rat[ibin-2] = r;
    rater[ibin-2] = err;
  }

  pt = 325.;
  dpt = 75.;
  vdata = 0.;
  vmc = 0.;
  errdata = 0.;
  errmc = 0.;
  for (ibin=6; ibin <= 8; ibin++) {
    vdata += hDATA->GetBinContent(ibin);
    vmc += hMC->GetBinContent(ibin);
    err1 = hDATA->GetBinError(ibin);
    errdata += err1*err1;
    err1 = hMC->GetBinError(ibin);
    errmc += err1*err1;
  }
  double errdata = sqrt(errdata)/vdata;
  double errmc = sqrt(errmc)/vmc;
  r = vdata/vmc;
  cout << pt << " +- " << dpt << "    " << r << " +- " << err << endl;
  arg[4] = pt;
  arger[4] = dpt;
  rat[4] = r;
  rater[4] = err;

  pt = 500.;
  dpt = 100.;
  vdata = 0.;
  vmc = 0.;
  errdata = 0.;
  errmc = 0.;
  for (ibin=9; ibin <= 14; ibin++) {
    vdata += hDATA->GetBinContent(ibin);
    vmc += hMC->GetBinContent(ibin);
    err1 = hDATA->GetBinError(ibin);
    errdata += err1*err1;
    err1 = hMC->GetBinError(ibin);
    errmc += err1*err1;
  } 
  double errdata = sqrt(errdata)/vdata;
  double errmc = sqrt(errmc)/vmc;
  r = vdata/vmc;
  cout << pt << " +- " << dpt << "    " << r << " +- " << err << endl;
  arg[5] = pt;
  arger[5] = dpt;
  rat[5] = r;
  rater[5] = err;

  TCanvas* c1 = new TCanvas("SameSign","SameSign",200,10,900,600);
  TH1F* frame = c1->DrawFrame(0, 0, 700., 2.);

  frame->SetXTitle("Max pT of a lepton in the pair");
  frame->SetYTitle("Ratio Data/MC");

  TGraphErrors* gr1 = new TGraphErrors(6,arg,rat,arger,rater);
  gr1->SetMarkerStyle(21);
  gr1->SetMarkerColor(kRed);
  double xmsize = gr1->GetMarkerSize();
  gr1->SetMarkerSize(1.5*xmsize);
  gr1->Draw("P");

  TLatex * tex = new TLatex(0.6,0.8,"Same sign pairs barrel Mll 81 - 101 GeV");
  tex->SetNDC();
  tex->SetTextAlign(22);
  tex->SetTextFont(22);
  tex->SetTextSize(0.05);
  //tex->SetLineWidth(3);
  tex->Draw();

}
