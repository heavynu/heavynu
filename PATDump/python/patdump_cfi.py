import FWCore.ParameterSet.Config as cms

# dump module
patdump = cms.EDAnalyzer('PATDump',
  # input collections
  electronTag = cms.InputTag("cleanPatElectrons"),
  muonTag     = cms.InputTag("cleanPatMuons"),
  #tauTag      = cms.InputTag("selectedLayer1Taus"),
  jetTag      = cms.InputTag("cleanPatJets"),
  photonTag   = cms.InputTag("cleanPatPhotons"),
  metTag      = cms.InputTag("patMETs"),
  triggerResults = cms.InputTag("TriggerResults::HLT"),
  pileupTag = cms.InputTag("addPileupInfo"),
  
  minLeptons = cms.int32(0),
  minJets    = cms.int32(0),
  
  debug = cms.bool(False)
)
