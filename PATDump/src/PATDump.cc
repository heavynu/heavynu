// this module creates root files with ntuples
// which can be read without CMSSW

// CMSSW
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/EventSetup.h"
#include "FWCore/Framework/interface/ESHandle.h"
#include "FWCore/Framework/interface/MakerMacros.h"
#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/Utilities/interface/InputTag.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "CommonTools/UtilAlgos/interface/TFileService.h"
#include "DataFormats/Candidate/interface/Particle.h"
#include "DataFormats/PatCandidates/interface/Electron.h"
#include "DataFormats/PatCandidates/interface/Muon.h"
#include "DataFormats/PatCandidates/interface/Tau.h"
#include "DataFormats/PatCandidates/interface/Jet.h"
#include "DataFormats/PatCandidates/interface/Photon.h"
#include "DataFormats/PatCandidates/interface/MET.h"
#include "DataFormats/PatCandidates/interface/TriggerEvent.h"
#include "DataFormats/Common/interface/View.h"
#include "DataFormats/Common/interface/TriggerResults.h"
#include "FWCore/MessageLogger/interface/MessageLogger.h"
#include "SimDataFormats/PileupSummaryInfo/interface/PileupSummaryInfo.h"
#include "DataFormats/VertexReco/interface/Vertex.h"
#include "DataFormats/VertexReco/interface/VertexFwd.h"
#include "JetMETCorrections/Objects/interface/JetCorrectionsRecord.h"
#include "CondFormats/JetMETObjects/interface/JetCorrectorParameters.h"
#include "CondFormats/JetMETObjects/interface/JetCorrectionUncertainty.h"
#include "PhysicsTools/SelectorUtils/interface/JetIDSelectionFunctor.h"
#include "PhysicsTools/SelectorUtils/interface/PFJetIDSelectionFunctor.h"
#include "DataFormats/JetReco/interface/JPTJet.h"
#include "DataFormats/EgammaReco/interface/SuperCluster.h"
#include "DataFormats/EgammaReco/interface/SuperClusterFwd.h"
#include "SimDataFormats/GeneratorProducts/interface/GenEventInfoProduct.h"
#include "RecoEgamma/EgammaTools/interface/HoECalculator.h"
#include "RecoEgamma/EgammaTools/interface/HoECalculator.h"
#include "DataFormats/JetReco/interface/PFJet.h"
#include "DataFormats/JetReco/interface/PFJetCollection.h"
#include "DataFormats/METReco/interface/PFMET.h"
#include "DataFormats/TauReco/interface/PFTau.h"
#include "DataFormats/Common/interface/ValueMap.h"
#include "DataFormats/PatCandidates/interface/VIDCutFlowResult.h"
#include "DataFormats/EgammaCandidates/interface/ConversionFwd.h"
#include "DataFormats/EgammaCandidates/interface/Conversion.h"
#include "RecoEgamma/EgammaTools/interface/ConversionTools.h"
#include "SimDataFormats/GeneratorProducts/interface/LHEEventProduct.h"
#include "SimDataFormats/GeneratorProducts/interface/LHERunInfoProduct.h"
// ROOT
#include "TTree.h"
#include "TH1.h"
#include "TObjArray.h"
#include "TObjString.h"
#include "Math/VectorUtil.h"

// c++
#include <algorithm>
#include <cmath>

//
#include "format.h"

using namespace edm;
using namespace std;

class PATDump: public edm::EDAnalyzer {
public:
  explicit PATDump(const edm::ParameterSet&);
  ~PATDump();

private:
  void beginJob();
  void analyze(const edm::Event&, const edm::EventSetup&);
  void endJob();
  int MuonID(const pat::Muon& m, Handle<reco::VertexCollection> pvHand) const;
  int TauID(const pat::Tau&) const; 
  int jetID(const pat::Jet&) const; 
  
  // input parameters
  edm::InputTag eleLabel;
  edm::InputTag SuperClusterLabel;
  edm::InputTag muoLabel;
  edm::InputTag tauLabel;
  edm::InputTag jetLabel;
  edm::InputTag metLabel;
  edm::InputTag phoLabel;
  
  edm::InputTag triggerLabel;
  edm::InputTag pileupLabel;
  edm::InputTag GenLabel;

  edm::EDGetTokenT<GenEventInfoProduct> tokenGenEvent_;
  edm::EDGetTokenT<reco::VertexCollection> tokenVertex_;
  edm::EDGetTokenT<View<pat::Electron> > tokenElectron_;
  edm::EDGetTokenT<View<reco::SuperCluster> > tokenSuperCluster_;
  edm::EDGetTokenT<View<pat::Muon> > tokenMuon_;
  edm::EDGetTokenT<View<pat::Tau> > tokenTau_;
  edm::EDGetTokenT<View<reco::Photon> > tokenPhoton_;
  edm::EDGetTokenT<View<pat::MET> > tokenMET_;
  edm::EDGetTokenT<View<pat::Jet> > tokenJet_;
  edm::EDGetTokenT<reco::GenParticleCollection> tokenPrunedGenParticles_;
  edm::EDGetTokenT<edm::ValueMap<bool> >  tokenEleVetoIdMap_;
  edm::EDGetTokenT<SortedCollection<HBHERecHit, edm::StrictWeakOrdering<HBHERecHit> > > tokenHBHEReco_;
  edm::EDGetTokenT<LHEEventProduct> tokenLHEEvent_;
  edm::EDGetTokenT<vector<PileupSummaryInfo>> tokenPileup_;

  int minLeptons;                // Minimum number of leptons
  int minJets;                   // Minimum number of jets
  float electronEcut = 0.;
  float clusterEcut = 0.;
  float muonEcut = 0.;
  float tauEcut = 0.;
  float jetEcut = 0.;
  float photonEcut = 0.;
  float METEcut = 0.;
  edm::InputTag conversions;
  edm::InputTag eleVetoIdMap; 
  edm::InputTag eleLooseIdMap; 
  edm::InputTag eleMediumIdMap; 
  edm::InputTag eleTightIdMap; 
  edm::InputTag eleHEEPIdMap;   
  bool debug;

  // counters for efficiency calculation
  int NeleMC, NeleReco, NeleRecoMatched;
  int NmuMC, NmuReco, NmuRecoMatched;
   int NTauMC, NTauReco, NTauRecoMatched;
  /*
  std::vector<const reco::GenParticle *> EleGens;
  std::vector<const reco::GenParticle *> MuGens;
  */
  // 
  double totalEvents;            // Number of events
  float totalIntegral;            // Number of events with weights

  TTree* theTree;
  TH1F* theHist;
  
  vector<HLTtable> tables;
  
  // particles information
  TheEvent evt;
};

PATDump::PATDump(const edm::ParameterSet& iConfig):
  eleLabel(iConfig.getParameter<InputTag>("electronTag")),
  SuperClusterLabel(iConfig.getParameter<InputTag>("superclusterTag")),  
  muoLabel(iConfig.getParameter<InputTag>("muonTag")),
  tauLabel(iConfig.getParameter<InputTag>("tauTag")),
  jetLabel(iConfig.getParameter<InputTag>("jetTag")),
  metLabel(iConfig.getParameter<InputTag>("metTag")),
  phoLabel(iConfig.getParameter<InputTag>("photonTag")),
  triggerLabel(iConfig.getParameter<InputTag>("triggerTag")),
  pileupLabel(iConfig.getParameter<InputTag>("pileupTag")),
  GenLabel(iConfig.getParameter<InputTag>("GeneratorTag")),

  //tokenGenEvent_(consumes<GenEventInfoProduct>(iConfig.getParameter<InputTag>("GeneratorTag"))),
  tokenGenEvent_(consumes<GenEventInfoProduct>(edm::InputTag(iConfig.getUntrackedParameter("moduleLabel",std::string("generator")) ))),
  tokenVertex_(consumes<reco::VertexCollection>(edm::InputTag("offlineSlimmedPrimaryVertices"))),
  tokenElectron_(consumes<View<pat::Electron> >(iConfig.getParameter<InputTag>("electronTag"))),
  tokenSuperCluster_(consumes<View<reco::SuperCluster>>(iConfig.getParameter<InputTag>("superclusterTag"))),
  tokenMuon_(consumes<View<pat::Muon> >(edm::InputTag(iConfig.getParameter<InputTag>("muonTag")))),
  tokenTau_(consumes<View<pat::Tau> >(edm::InputTag(iConfig.getParameter<InputTag>("tauTag")))),
  tokenPhoton_(consumes<View<reco::Photon> >(iConfig.getParameter<InputTag>("photonTag"))),
  tokenMET_(consumes<View<pat::MET> >(iConfig.getParameter<InputTag>("metTag"))),
  tokenJet_(consumes<View<pat::Jet> >(iConfig.getParameter<InputTag>("jetTag"))),
  tokenPrunedGenParticles_(consumes<reco::GenParticleCollection> (edm::InputTag("prunedGenParticles"))),
  //tokenEleVetoIdMap_(consumes<View<pat::ValueMap> >(iConfig.getParameter<InputTag>("eleVetoIdMap"))),
  tokenEleVetoIdMap_(consumes<edm::ValueMap<bool> >(iConfig.getParameter<InputTag>("eleVetoIdMap"))),
  tokenHBHEReco_(consumes<SortedCollection<HBHERecHit, edm::StrictWeakOrdering<HBHERecHit> > >(edm::InputTag("HBHERecHitTag"))),
  tokenLHEEvent_(consumes<LHEEventProduct>(iConfig.getUntrackedParameter<edm::InputTag>("moduleLabel", std::string("externalLHEProducer")))),
  tokenPileup_(consumes<vector<PileupSummaryInfo> >(iConfig.getParameter<InputTag>("pileupTag"))),

  minLeptons(iConfig.getParameter<int>("minLeptons")),
  minJets(iConfig.getParameter<int>("minJets")),
  electronEcut(iConfig.getParameter<double>("electronEcut")),
  clusterEcut(iConfig.getParameter<double>("clusterEcut")),
  muonEcut(iConfig.getParameter<double>("muonEcut")),
  tauEcut(iConfig.getParameter<double>("tauEcut")),
  jetEcut(iConfig.getParameter<double>("jetEcut")),
  photonEcut(iConfig.getParameter<double>("photonEcut")),
  METEcut(iConfig.getParameter<double>("METEcut")),
 
  conversions(iConfig.getParameter<InputTag>("conversions")),
  eleVetoIdMap(iConfig.getParameter<InputTag>("eleVetoIdMap")), 
  eleLooseIdMap(iConfig.getParameter<InputTag>("eleLooseIdMap")), 
  eleMediumIdMap(iConfig.getParameter<InputTag>("eleMediumIdMap")), 
  eleTightIdMap(iConfig.getParameter<InputTag>("eleTightIdMap")), 
  eleHEEPIdMap(iConfig.getParameter<InputTag>("eleHEEPIdMap")), 

  debug(iConfig.getParameter<bool>("debug")),

  // counters for efficiency calculation
  NeleMC(0), NeleReco(0), NeleRecoMatched(0),
  NmuMC(0), NmuReco(0), NmuRecoMatched(0),
  NTauMC(0), NTauReco(0), NTauRecoMatched(0),

  totalEvents(0),
  totalIntegral(0),
  theTree(0),
  theHist(0)
{}

PATDump::~PATDump() {}

// ------ method called once each job just before starting event loop  -------
void PATDump::beginJob()
{
  edm::Service<TFileService> fs;
  theTree = fs->make<TTree>("data", "data");
  theHist = fs->make<TH1F>("gen_weight", "gen_weight",20000000,-100000, 100000);
  // Initialise Root Tree to store events
  evt.book(theTree);
}

void PATDump::endJob()
{
  // dump HLT tables
  edm::Service<TFileService> fs;
  TTree* theHLT = fs->make<TTree>("hlt", "hlt");
  writeHLT(theHLT, tables);
  
  // electrons' reconstruction efficiency
  const float EleEff = (NeleMC == 0) ? 0. : float(NeleReco)/float(NeleMC);
  const float EleEffMatched = (NeleMC == 0) ? 0. : float(NeleRecoMatched)/float(NeleMC);
  
  cout << "\n"
       << " Electron reco efficiency = "             << EleEff << "\n"
       << " Electron reco efficiency (Matched e) = " << EleEffMatched << "\n"
       << "  MC electrons = "           << NeleMC << "\n"
       << "  RECO electrons = "         << NeleReco << "\n"
       << "  Matched RECO electrons = " << NeleRecoMatched << "\n"
       << endl;
  
  // muons' reconstruction efficiency
  const float MuEff = (NmuMC == 0) ? 0. : float(NmuReco)/float(NmuMC);
  const float MuEffMatched = (NmuMC == 0) ? 0. : float(NmuRecoMatched)/float(NmuMC);
  
  cout << "\n"
       << " Muon reco efficiency = "              << MuEff << "\n"
       << " Muon reco efficiency (Matched mu) = " << MuEffMatched << "\n"
       << "  MC muons = "           << NmuMC << "\n"
       << "  RECO muons = "         << NmuReco << "\n"
       << "  Matched RECO muons = " << NmuRecoMatched << "\n"
       << endl;

  // muons' reconstruction efficiency
  const float TauEff = (NTauMC == 0) ? 0. : float(NTauReco)/float(NTauMC);
  const float TauEffMatched = (NTauMC == 0) ? 0. : float(NTauRecoMatched)/float(NTauMC);
  
  cout << "\n"
       << " Tau reco efficiency = "              << TauEff << "\n"
       << " Tau reco efficiency (Matched tau) = " << TauEffMatched << "\n"
       << "  MC Tauons = "           << NTauMC << "\n"
       << "  RECO Tauons = "         << NTauReco << "\n"
       << "  Matched RECO Tauons = " << NTauRecoMatched << "\n"
       << endl; 

  cout << "\n"
       << " Total events = "   <<   totalEvents << "\n"
       << " Total integral = " << totalIntegral << "\n"
       << endl;         
}

HLTtable stripHLT(const pat::TriggerEvent* te)
{

  // HLT/L1 prescale extracted from PAT TriggerEvent object.
  // Another way is to extract prescales with help of HLTConfigProvider,
  //   see docs (just for reference):
  //     https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideHighLevelTrigger#Access_to_the_HLT_configuration
  
  HLTtable table;
  
  table.L1menu = te->nameL1Menu();
  table.HLTmenu = te->nameHltTable();
  
  const vector<pat::TriggerPath>* paths = te->paths();
  
  for (size_t i = 0; i < paths->size(); ++i) {
    const pat::TriggerPath& path = (*paths)[i];
    //if (path.index() != i) --> crash!;
    
    // get HLT prescale
    int prescale = path.prescale();
    
    // list of corresponding L1 seeds:
    const pat::L1SeedCollection& seeds = path.l1Seeds();
    
    // prepare combined HLT * L1 prescale:
    if (seeds.size() == 1) {
      const pat::TriggerAlgorithm* alg0 = te->algorithm(seeds[0].second);
      if (alg0)
        prescale *= alg0->prescale();
      else
        prescale = -1;
    }
    else if (seeds.size() > 1) {
      prescale = -1;
    }
    
    table.prescales.push_back(prescale);
    table.names.push_back(path.name());
  }
  
  return table;

}

template <class ObjectType>
vector<int> getBits(const pat::PATObject<ObjectType>& p, const pat::TriggerEvent* te)
{
  vector<int> bits;
  
  // loop over all trigger objects
   for (unsigned int kl = 0;  kl < p.triggerObjectMatches().size(); ++kl) {
    const vector<string> paths = p.triggerObjectMatches().at(kl).pathNames(true,false) ;
    // cout << "Matches to HLT:\n";
    for (size_t j = 0; j < paths.size(); ++j) {
      const string& name = paths[j];
      // resolve path name to bit number:
      const pat::TriggerPath* tp = te->path(name);
      const int bit = tp ? tp->index() : 0;
      
      bits.push_back(bit);
      // cout <<"  "<< p.triggerObjectMatches().size() <<"  " << bit << " " << name << endl;
    }
  }
  
  return bits;
}


void PATDump::analyze(const Event& iEvent, const EventSetup& iSetup)
{
  evt.clear();
  
  // --------------- Count all events ---------------
  totalEvents++;
  
  
  // ------------------- Event and Run numbers ----------------------
  if (debug) {
    cout << endl
         << " ===== Run:Event:Lumi = "
         << iEvent.id().run() << ":"
         << iEvent.id().event() << ":"
         << iEvent.luminosityBlock()
         << " =============="
         << endl;
  }
  
  evt.irun   = iEvent.id().run();
  evt.ievent = iEvent.id().event();
  evt.ilumi  = iEvent.luminosityBlock();
  evt.timestamp = iEvent.time().value();
  
  // generator event weight (essential for Sherpa)
  if (!GenLabel.label().empty()) {
    Handle<GenEventInfoProduct> geneventinfo;
    //iEvent.getByLabel(GenLabel, geneventinfo);
    iEvent.getByToken(tokenGenEvent_, geneventinfo);
    evt.Gen_weight = geneventinfo->weight();
    std::vector<double> evtWeights = geneventinfo->weights();
    
    if (debug) {  
      cout << " Generator event weight = " << geneventinfo->weight() << endl;
    }

    edm::Handle<LHEEventProduct> EvtHandle ;
    //iEvent.getByLabel( "externalLHEProducer" , EvtHandle ) ;
    iEvent.getByToken(tokenLHEEvent_, EvtHandle);

//    int whichWeight = XXX;
//    theWeight *= EvtHandle->weights()[whichWeight].wgt/EvtHandle->originalXWGTUP(); 

    for (unsigned int i=1; i<EvtHandle->weights().size(); ++i)    
    cout <<"   "<< geneventinfo->weight()<<"  "<< EvtHandle->originalXWGTUP()<<"  "<< EvtHandle->weights()[i].wgt <<endl;
/* 
    edm::Handle<LHERunInfoProduct> run; 
    typedef std::vector<LHERunInfoProduct::Header>::const_iterator headers_const_iterator;
 
    iRun.getByLabel( "externalLHEProducer", run );
    LHERunInfoProduct myLHERunInfoProduct = *(run.product());
 
    for (headers_const_iterator iter=myLHERunInfoProduct.headers_begin(); iter!=myLHERunInfoProduct.headers_end(); iter++){
        std::cout << iter->tag() << std::endl;
        std::vector<std::string> lines = iter->lines();
        for (unsigned int iLine = 0; iLine<lines.size(); iLine++) {
           std::cout << lines.at(iLine);
        }
    }
*/
  }
  else evt.Gen_weight = 1.;

  totalIntegral += evt.Gen_weight;
  theHist->Fill(evt.Gen_weight);
  
  // --------- Get the trigger results and check validity -----------
  // PAT Trigger
  // https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuidePATTrigger
  // http://cmslxr.fnal.gov/lxr/source/DataFormats/PatCandidates/interface/TriggerEvent.h
//  Handle<pat::TriggerEvent> triggerHandle;

//  Handle<trigger::TriggerEvent> triggerHandle;

 
//  iEvent.getByLabel(triggerLabel, triggerHandle);
/*  
  // HLT paths
  // http://cmslxr.fnal.gov/lxr/source/DataFormats/PatCandidates/interface/TriggerPath.h
  const vector<pat::TriggerPath>* paths = triggerHandle->paths();
  
  if (debug) {
    cout << " ----- Number of HLT bits = " << paths->size() << "\n"
         << "L1 menu  = " << triggerHandle->nameL1Menu() << "\n"
         << "HLT menu = " << triggerHandle->nameHltTable() << "\n"
         << endl;
    
    const vector<pat::TriggerAlgorithm>* algos = triggerHandle->algorithms();
    cout << "L1 algos:\n";
    for (size_t i = 0; i < algos->size(); ++i) {
      const pat::TriggerAlgorithm& algo = (*algos)[i];
      cout << " " << algo.bit()
           << " " << algo.decision()
           << " " << algo.prescale()
           << " " << algo.name()
           << "\n";
    }
    
    cout << "HLT paths:\n";
    for (size_t i = 0; i < paths->size(); ++i) {
      const pat::TriggerPath& path = (*paths)[i];
      cout << " " << path.index()
           << " " << path.wasAccept()
           << " " << path.prescale()
           << " " << path.name()
           << "\n";
      
      // list of decision and name of L1 seeds:
      const pat::L1SeedCollection& seeds = path.l1Seeds();
      for (size_t j = 0; j < seeds.size(); ++j) {
        const pat::TriggerAlgorithm* alg = triggerHandle->algorithm(seeds[j].second);
        const int decision = (alg) ? alg->decision() : -1;
        const int prescale = (alg) ? alg->prescale() : -1;
        const int index = (alg) ? alg->bit() : -1;
        
        cout << "        "
             << " " << index
             << " " << decision
             << " " << prescale
             << " " << seeds[j].second
             << "\n";
      }
    }
    cout << endl;
  }
  
  // update HLT bits names and prescale
  HLTtable table = stripHLT(triggerHandle.product());
  table.irun = evt.irun;
  table.ievent = evt.ievent;
  table.ilumi = evt.ilumi;
  
  const int nHLT = tables.size();
  
  if ((nHLT == 0) ||
      (tables[nHLT-1].L1menu != table.L1menu) ||
      (tables[nHLT-1].HLTmenu != table.HLTmenu) ||
      (tables[nHLT-1].prescales != table.prescales) ||
      (tables[nHLT-1].names != table.names)) {
    tables.push_back(table);
  }
  
  // copy HLT trigger
  for (size_t i = 0; i < paths->size(); ++i)
    evt.hltBits->push_back((*paths)[i].wasAccept());
  
*/  
  // ------------ Pile-Up information -------------------------
  // https://twiki.cern.ch/twiki/bin/view/CMS/PileupInformation#Accessing_PileupSummaryInfo_in_r
  
  // extract Pile-Up information only if the label was specified
  // (to avoid stop on real data, there no such collection exists)
  if (!pileupLabel.label().empty()) {
    if (debug)
      cout << " ----- Pile-Up information -----" << endl;
      
    Handle<vector<PileupSummaryInfo> > pileupHandle;
    //iEvent.getByLabel(pileupLabel, pileupHandle);
    iEvent.getByToken(tokenPileup_, pileupHandle);
    
    std::vector<PileupSummaryInfo>::const_iterator PVI;

    float Tnvtx = -1.0;
    for(PVI = pileupHandle->begin(); PVI != pileupHandle->end(); ++PVI) {

    int BX = PVI->getBunchCrossing();

    if(BX == 0) { 
     Tnvtx = PVI->getTrueNumInteractions(); 
     continue;
    }

    }
    
    evt.TrueNumInter = Tnvtx;
      
    
    for (size_t i = 0; i < pileupHandle->size(); ++i) {
      const PileupSummaryInfo& psi = pileupHandle->at(i);
      
      // bunch crossing number
      const int bx = psi.getBunchCrossing();
      
      // number of interactions at this bunch crossing
      const size_t num_interactions = psi.getPU_NumInteractions();
      
      if (debug) {
        cout << "bunch crossing: # = " << bx <<", True_num_int = " << evt.TrueNumInter << ", num_interactions = " << num_interactions << endl;
      }
      
      // following check is a workaround for crash in call to 'psi.getPU_zpositions()'
      // if 'num_interactions == 0'
      if (num_interactions == 0) continue;
      
      const vector<float>& zpos = psi.getPU_zpositions();
      
      for (size_t j = 0; j < zpos.size(); ++j) {
        // number of PV can be smaller than number of interactions,
        // add 'empty' vertex (type == -1) in this case
        const bool isEmpty = (j >= zpos.size());
        const int type = isEmpty ? -1 : 0;
        const ROOT::Math::XYZPoint position(0, 0, (isEmpty ? -1000 : zpos[j]));
        
        evt.add_vertex(type, position, bx, 0, 0);
      }
      
      if (debug) {
        const vector<float>& sum_pT = psi.getPU_sumpT_highpT();
        const vector<int>& n_trks = psi.getPU_ntrks_highpT();
        
        for (size_t j = 0; j < zpos.size(); ++j)
          cout << "    z = " << zpos[j] << ", sum_pT = " << sum_pT[j] << ", n_trks = " << n_trks[j] << endl;
      }
    }
  }
  
  // ------------ Primary Vertices -------------------------
  // https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookOfflinePrimaryVertexFinding
  // http://cmssw.cvs.cern.ch/cgi-bin/cmssw.cgi/CMSSW/DPGAnalysis/Skims/src/GoodVertexFilter.cc?view=markup
  if (debug)
    cout << " ----- Primary Vertices -----" << endl;
  
  InputTag vertexSrc("offlineSlimmedPrimaryVertices");
  Handle<reco::VertexCollection> pvHandle;
  //iEvent.getByLabel(vertexSrc, pvHandle);
  iEvent.getByToken( tokenVertex_ , pvHandle );
  
  for (size_t i = 0; i < pvHandle->size(); ++i) {
    const reco::Vertex& vtx = pvHandle->at(i);
    
    if (! vtx.isFake())
      evt.add_vertex(1, vtx.position(), 0, vtx.chi2(), vtx.ndof());
    
    if (debug) {
      cout << "Vertex: chi2 = " << vtx.chi2() << ", ndof = " << vtx.ndof() << ", z = " << vtx.z() << ", z_err = " << vtx.zError() << ", d0 = " << vtx.position().rho() << ", isFake = " << vtx.isFake() << endl;
    }
  }
  
  
  // leptons and jets counters
  int nLeptons = 0;
  int nJets = 0;
  /*
  EleGens.clear();
  MuGens.clear();
  */
  // ------------------- electrons --------------------------
  // get the electrons
  if (!eleLabel.label().empty()) {
     Handle<View<pat::Electron> > electronHandle;
     //iEvent.getByLabel(eleLabel, electronHandle);                                                                                           
     iEvent.getByToken(tokenElectron_, electronHandle);

     Handle<edm::ValueMap<bool> > eleVetoIdMapHandle;
     //     iEvent.getByLabel(eleVetoIdMap, eleVetoIdMapHandle);
     iEvent.getByToken(tokenEleVetoIdMap_, eleVetoIdMapHandle);

     Handle<edm::ValueMap<bool> > eleLooseIdMapHandle;
     //     iEvent.getByLabel(eleVetoIdMap, eleLooseIdMapHandle);
     iEvent.getByToken(tokenEleVetoIdMap_, eleLooseIdMapHandle); 
     Handle<edm::ValueMap<bool> > eleMediumIdMapHandle;
     //     iEvent.getByLabel(eleVetoIdMap, eleMediumIdMapHandle);
     iEvent.getByToken(tokenEleVetoIdMap_, eleMediumIdMapHandle);
     Handle<edm::ValueMap<bool> > eleTightIdMapHandle;
     //    iEvent.getByLabel(eleVetoIdMap, eleTightIdMapHandle);
     iEvent.getByToken(tokenEleVetoIdMap_, eleTightIdMapHandle);
     Handle<edm::ValueMap<bool> > eleHEEPIdMapHandle;
     //    iEvent.getByLabel(eleVetoIdMap, eleHEEPIdMapHandle);
     iEvent.getByToken(tokenEleVetoIdMap_, eleHEEPIdMapHandle); 

  if (debug) cout << " ----- Number of electrons = " << electronHandle->size() << endl;
  
  for (unsigned int i = 0; i < electronHandle->size(); ++i) {
       const pat::Electron& p = electronHandle->at(i);
       const auto el = electronHandle->ptrAt(i);
      // skip weak electron
       if (p.energy() < electronEcut) continue;
       nLeptons++;
      // MC object will be stored (matched to the reconstructed object)
       int mcId = -1;
       if (p.genLepton()) {
           mcId = evt.push(TheEvent::GEN, p.genLepton()->pdgId(), -10,
	                       p.genLepton()->p4(), p.genLepton()->vertex(), 
	                       p.genLepton()->charge());
	    }
    // extract trigger objects bits
//     const vector<int>& bits = getBits(p, triggerHandle.product());
       const reco::GsfTrackRef& ref = p.gsfTrack();
       bool isPassVeto = (*eleVetoIdMapHandle)[el];
       bool isPassLoose  = (*eleLooseIdMapHandle)[el];
       bool isPassMedium = (*eleMediumIdMapHandle)[el];
       bool isPassTight  = (*eleTightIdMapHandle)[el];
       bool isPassHEEP = (*eleHEEPIdMapHandle)[el];
       int eleID = -1;
       if (isPassHEEP) eleID = 4;
       else {
          if (isPassTight) eleID = 3; 
          else {     
             if (isPassMedium) eleID = 2;
             else {
                if (isPassLoose) eleID = 1;
                else if (isPassVeto) eleID = 0;
             }
          }
       }
       evt.push(TheEvent::ELECTRON, p.pdgId(), mcId,  p.p4(), p.vertex(), p.charge(),
//              bits,
                vector<int>(),
                p.dr03TkSumPt(), p.dr03EcalRecHitSumEt(), p.dr03HcalTowerSumEt(), p.dr03HcalDepth1TowerSumEt(),
                !ref ? 0 : ref->chi2(), !ref ?   0 : ref->ndof(),
                !ref ? 0 : ref->found(), !ref ?   0 : ref->lost(),
                p.hadronicOverEm(), (int)isPassHEEP, (int)isPassVeto, (int)isPassLoose, (int)isPassMedium, (int)isPassTight, eleID ,
                p.sigmaIetaIeta(), p.deltaPhiSuperClusterTrackAtVtx(), -19 );

    // counters for efficiency calculation
       NeleReco++;
       if (mcId != -1) NeleRecoMatched++;
       if (debug) {
          cout << "     electron: P = " << p.p()
               << "  Pt = " <<  p.pt()
               << "  eta = " << p.eta()
               << "  phi = " << p.phi()
               << "  charge = "  << p.charge()
               << "  relIso = " << (p.dr03EcalRecHitSumEt() + p.dr03HcalTowerSumEt() + p.dr03TkSumPt())/p.pt()
               << endl;
       }
    }
 }  
  // ------------------- SuperClusters --------------------------
  // get the superclusters
  if (! SuperClusterLabel.label().empty()) {
     //Handle<reco::SuperClusterCollection> SuperClusterHandle;
     Handle<View<reco::SuperCluster> > SuperClusterHandle;
     //iEvent.getByLabel(SuperClusterLabel, SuperClusterHandle);
     iEvent.getByToken(tokenSuperCluster_, SuperClusterHandle);

     if (debug) cout << " ----- Number of SuperClusters = " << SuperClusterHandle->size() << endl;
     edm::Handle<SortedCollection<HBHERecHit, edm::StrictWeakOrdering<HBHERecHit> > > hbheRecHitsHandle;
     //iEvent.getByLabel("hbhereco", hbheRecHitsHandle);
     iEvent.getByToken(tokenHBHEReco_, hbheRecHitsHandle);
     const SortedCollection<HBHERecHit,edm::StrictWeakOrdering<HBHERecHit> >* hbheRecHits = hbheRecHitsHandle.failedToGet () ? 0 : &*hbheRecHitsHandle;
  
     for (size_t i = 0; i < SuperClusterHandle->size(); ++i) {
         const reco::SuperCluster& p = SuperClusterHandle->at(i);
    
       // skip weak superclusters
        if (p.energy() < clusterEcut) continue;
    
        double HoE = -1.;
        if (hbheRecHits) {
           HoECalculator calc_HoE; 
           HoE = calc_HoE(&p, iEvent, iSetup);
        }
        const double k = p.energy()/sqrt(p.position().Mag2());
        const ROOT::Math::XYZTVector SC_P4(p.x()*k, p.y()*k, p.z()*k, p.energy());
    
           evt.push(TheEvent::SUPERCLUSTER,
           //1  2   3            4       5
             0, 0., SC_P4, p.position(), 0,
             vector<int>(),
           //7   8   9  10 11 12 13 14   15 16  17 18 19 20  21      
             0., 0., 0., 0.,0, 0, 0, 0, HoE, 0,  0, 0, 0, 0,-30,
           //  22                23      24
             p.etaWidth(),p.phiWidth(), -31);
              
        if (debug) {
           cout <<"    Cluster Energy   =  " << SC_P4.E() 
                << "    SCluster Pt     =  " << SC_P4.Pt()
                << " SCluster P4 Eta    =  " << SC_P4.Eta() 
                << " SCluster P4 Phi    =  " << SC_P4.Phi()  
                << "    EtaWidth        =  " << p.etaWidth()
                << "    PhiWidth        =  " << p.phiWidth()
                << "    Cluster HoE     =  " << HoE
                << endl; 
           
        }
     }
  }
 


 // ------------------- muons --------------------------
  if (! muoLabel.label().empty()) {
     Handle<View<pat::Muon> > muonHandle;
     //  iEvent.getByLabel(muoLabel, muonHandle);
     iEvent.getByToken(tokenMuon_, muonHandle);

     if (debug) cout << " ----- Number of muons = " << muonHandle->size() << endl;
     for (unsigned int i = 0; i < muonHandle->size(); ++i) {
//        const reco::Muon& p = muonHandle->at(i);
        const pat::Muon& p = muonHandle->at(i);
//        if (!p.isGlobalMuon()) continue;  
        if (p.energy() < muonEcut) continue; 
        nLeptons++;
        int mcId = -1;
        if (p.genLepton()) {
            mcId = evt.push(TheEvent::GEN, p.genLepton()->pdgId(), -10,
	                       p.genLepton()->p4(), p.genLepton()->vertex(), 
	                       p.genLepton()->charge());
	}
//      const vector<int>& bits = getBits(p, triggerHandle.product());

//      const reco::TrackRef& ref = p.combinedMuon();
        const reco::TrackRef& ref =  p.tunePMuonBestTrack();
//        cout << ref->p()<<"  "<<p.p()<< endl;
//        cout << ref->charge()<<"  "<<p.charge()<< endl;     

        int a=-1;
        int b=-1;
        int c=-1;
        int d=-1;   
        for (size_t j = 0; j < pvHandle->size(); ++j) {
        const reco::Vertex& vtx = pvHandle->at(j);
            if ((!vtx.isFake())) {
                if (muon::isLooseMuon(p)) a = 1;
                if (muon::isTightMuon(p,vtx)) b = 1;
                if (muon::isSoftMuon(p,vtx)) c = 1;
                if (muon::isHighPtMuon(p,vtx)) d = 1;
            }
        }
 
        double trackIso=-1;
        double ecalIso=-1;
        double hcalIso=-1;
        double hcal1Iso=-1;
        double PUIso=-1;
        if (p.isPFMuon()) {
            trackIso=p.pfIsolationR04().sumPhotonEt;
            ecalIso=p.pfIsolationR04().sumChargedParticlePt;
            hcalIso=p.pfIsolationR04().sumNeutralHadronEt;
            hcal1Iso=p.pfIsolationR04().sumChargedHadronPt;
            PUIso = p.pfIsolationR04().sumPUPt;
        }
        else {
            trackIso=p.isolationR03().sumPt;
            ecalIso=p.isolationR03().emEt;
            hcalIso=p.isolationR03().hadEt;
            hcal1Iso=0.;
            PUIso=0.;
        }



        evt.push(TheEvent::MUON, p.pdgId(), mcId,
              p.p4(), p.vertex(), p.charge(),
//              bits,
                vector<int>(),
               // hadronic layer 1 isolation not available for reco::Muon,
               // fill field 'hcal1Iso' by value of full hadronic isolation
               trackIso, ecalIso, hcalIso,  hcal1Iso,
               !ref ? 0 : ref->chi2(), !ref ?   0 : ref->ndof(),
               !ref ? 0 : ref->found(), !ref ?   0 : ref->lost(),
//             15,    16,                                 17, 18,19,  20,                   21, 22,                         23,         24
               PUIso,  0,  p.muonID("GlobalMuonPromptTight"),  a,  b,  c, MuonID(p, pvHandle) ,   d, p.muonID("AllGlobalMuons"), (int) p.isPFMuon());

  // counters for efficiency calculation
        NmuReco++;
        if (mcId != -1) NmuRecoMatched++;
        if (debug) {
           cout << "     muon: P = " << p.p()
                << "  Pt = " <<  p.pt()
                << "  eta = " << p.eta()
                << "  phi = " << p.phi()
                << "  charge = "  << p.charge()
                << "  relIso = " << (p.isolationR03().emEt + p.isolationR03().hadEt + p.isolationR03().sumPt)/p.pt()
                << endl;
           
        }
     }
  }

// ------------------- taus --------------------------
  if (! tauLabel.label().empty()) {
     Handle<View<pat::Tau> > tauHandle;
     //     iEvent.getByLabel(tauLabel, tauHandle);
            iEvent.getByToken(tokenTau_, tauHandle);

//     Handle<reco::PFTauDiscriminator> discriminator;
//     iEvent.getByLabel(discriminatorLabel, discriminator);

     for (unsigned int i = 0; i < tauHandle->size(); ++i) {    
             const pat::Tau& p = tauHandle->at(i);
             if (p.energy() < tauEcut) continue;
             nLeptons++;
             int mcId = -1;
             if (p.genLepton()) {
                 mcId = evt.push(TheEvent::GEN, p.genLepton()->pdgId(), -10,
	                       p.genLepton()->p4(), p.genLepton()->vertex(), 
	                       p.genLepton()->charge());
	     }
             const reco::TrackRef& ref = p.leadTrack();
/*
             double a=-1;
             double b=-1;
             double c=-1;
             double d=-1;
             if (p.isPFTau()) {
                 d=p.maximumHCALPFClusterEt();
                 b=p.isolationPFGammaCandsEtSum();
                 c=p.isolationPFChargedHadrCandsPtSum();
             }
             if (p.isCaloTau()) {
                 a=p.isolationTracksPtSum();
                 b=p.isolationECALhitsEtSum();
             }
*/  

             evt.push(TheEvent::TAU, p.pdgId(), mcId,
                          p.p4(), p.vertex(), p.charge(), vector<int>(),
          /*7,8,9,10*/    p.tauID("byLooseCombinedIsolationDeltaBetaCorr3Hits"),p.tauID("byMediumCombinedIsolationDeltaBetaCorr3Hits"),p.tauID("byTightCombinedIsolationDeltaBetaCorr3Hits"),p.tauID("byCombinedIsolationDeltaBetaCorrRaw3Hits"),
                          p.tauID("againstElectronVLooseMVA5") , p.tauID("byPileupWeightedIsolationRaw3Hits"),
                          !ref ? 0 : ref->found(), !ref ?   0 : ref->lost(),
         /*15,16,17,18*/  p.tauID("byLoosePileupWeightedIsolation3Hits"), p.tauID("decayModeFindingNewDMs"), p.tauID("byMediumPileupWeightedIsolation3Hits"), p.tauID("byTightPileupWeightedIsolation3Hits"), 
       /*19,20,21,22,23*/ p.tauID("againstMuonLoose3"), p.tauID("againstMuonTight3"), TauID(p) ,  p.tauID("againstElectronLooseMVA5"),  p.tauID("againstElectronMediumMVA5"), (int) p.isPFTau());
                 // counters for efficiency calculation
                 NTauReco++;
                 if (mcId != -1) NTauRecoMatched++;
     }
  }
  
// ------------------- photons --------------------------
  // skip photons if collection name is empty
  // this is relevant to Particle Flow, which do not supports photons now
  if (! phoLabel.label().empty()) {
     Handle<View<reco::Photon> > phoHandle;
     //iEvent.getByLabel(phoLabel, phoHandle);
     iEvent.getByToken(tokenPhoton_, phoHandle);
     if (debug)   cout << " ----- Number of photons = " << phoHandle->size() << endl;
     for (unsigned int i = 0; i < phoHandle->size(); ++i) {
        const pat::Photon& p = phoHandle->at(i);
        if (p.energy() < photonEcut) continue;  
        int mcId = -1;
        if (p.genPhoton()) {
            mcId = evt.push(TheEvent::GEN, p.genPhoton()->pdgId(), -10,
	                       p.genPhoton()->p4(), p.genPhoton()->vertex(), 
	                       p.genPhoton()->charge());
	}
//      const vector<int>& bits = getBits(p, triggerHandle.product());
      
        evt.push(TheEvent::PHOTON, p.pdgId(), mcId,
               p.p4(), p.vertex(), p.charge(),
//               bits,
               vector<int>(),
               p.trkSumPtSolidConeDR03(), p.ecalRecHitSumEtConeDR03(), p.hcalTowerSumEtConeDR03(), p.hcalDepth1TowerSumEtConeDR03(),
               0, 0, 0, 0,
               p.hadronicOverEm(), 0., 0., 0., 0., 0., 0., 0., 0., -21);

        if (debug) {
           cout << "     photon: P = " << p.p()
                << "  Pt = " <<  p.pt()
                << "  eta = " << p.eta()
                << "  phi = " << p.phi()
                << "  charge = "  << p.charge()
                << "  relIso = " << (p.ecalRecHitSumEtConeDR03() + p.hcalTowerSumEtConeDR03() +  p.trkSumPtSolidConeDR03())/p.pt()
                << endl;
       }
    }
  }
  
// ------------------- jets --------------------------
  if (! jetLabel.label().empty()) {
  Handle<View<pat::Jet> > jetHandle;
  //  iEvent.getByLabel(jetLabel, jetHandle);
  iEvent.getByToken(tokenJet_, jetHandle);

  ESHandle<JetCorrectorParametersCollection> JetCorParColl;
  iSetup.get<JetCorrectionsRecord>().get("AK4PFchs", JetCorParColl); 
  const JetCorrectorParameters& JetCorPar = (*JetCorParColl)["Uncertainty"];
  JetCorrectionUncertainty jecUnc(JetCorPar);


  
  if (debug)
    cout << " ----- Number of jets = " << jetHandle->size() << endl;
  
  for (unsigned int i = 0; i < jetHandle->size(); ++i) {
      const pat::Jet& p = jetHandle->at(i);
      if (p.energy() < jetEcut) continue;  
      nJets++;
      int mcId = -1;
      if (p.genJet()) {
            mcId = evt.push(TheEvent::GEN, p.genJet()->pdgId(), -10,
	                       p.genJet()->p4(), p.genJet()->vertex(), 
	                       p.genJet()->charge());
      }
     // EM energy fraction
      float emEnergyFraction = 0.;

      // PF Jet:
      emEnergyFraction = p.chargedEmEnergyFraction() + p.neutralEmEnergyFraction();
   
     //JEC Unsertainty
     jecUnc.setJetEta(p.p4().Eta());
     jecUnc.setJetPt(p.p4().Pt()); // here you must use the CORRECTED jet pt
    const double uncup = jecUnc.getUncertainty(true);
    jecUnc.setJetEta(p.p4().Eta());
    jecUnc.setJetPt(p.p4().Pt());
    const double uncdown = jecUnc.getUncertainty(false);
    const double unc = max(uncup,uncdown);

    double NHF = p.neutralHadronEnergyFraction();
    double NEMF = p.neutralEmEnergyFraction();
    double CHF = p.chargedHadronEnergyFraction();
    double MUF = p.muonEnergyFraction();
    double CEMF = p.chargedEmEnergyFraction();
    double NumConst = p.chargedMultiplicity()+p.neutralMultiplicity();
    double NumNeutralParticles =p.neutralMultiplicity();
    double CHM = p.chargedMultiplicity(); 

      
    evt.push(TheEvent::JET, p.pdgId(), mcId,
             p.p4(), p.vertex(), p.charge(),
//             bits,
             vector<int>(),
//           7,      8,   9,  10,   11,       12,                  13,     14,   
             NHF, NEMF, CHF, MUF, CEMF, NumConst, NumNeutralParticles,    CHM,
//                         15,                16, 17, 18,                                                                19,  20,       21, 22, 23,         24
             emEnergyFraction, p.nConstituents(),  0,  0, p.bDiscriminator("pfCombinedInclusiveSecondaryVertexV2BJetTags") , unc, jetID(p), 0., 0., int (p.isPFJet()));
    
    if (debug) {
      const float ef = emEnergyFraction;
      const float hf = 1 - emEnergyFraction;
      cout << "  jet: P = " << p.p()
           << "  Pt = " <<  p.pt()
           << "  eta = " << p.eta()
           << "  phi = " << p.phi()
           << "  charge = " << p.charge()
           << "  hadFract = " << hf
           << "  emFract = " << ef
           << "  h/e = " << ((ef != 0.) ? hf/ef : 1e6)
           << "  jet: unc = " << unc
           << "  jet: jetID = " << jetID(p)
           << endl;
      }
    }
  } 

// ------------------- METs --------------------------
  if (!metLabel.label().empty()) {
     Handle<View<pat::MET> > metHandle;
     //     iEvent.getByLabel(metLabel, metHandle);
     iEvent.getByToken(tokenMET_, metHandle);
     if (debug)  cout << " ----- Number of METs = " << metHandle->size() << endl;
     for (unsigned int i = 0; i < metHandle->size(); ++i) {
         const pat::MET& p = metHandle->at(i);
         if (p.energy() < METEcut) continue;
         int mcId = -1;
          if (p.genMET()) {
            mcId = evt.push(TheEvent::GEN, p.genMET()->pdgId(), -10,
	                       p.genMET()->p4(), p.genMET()->vertex(), 
	                       p.genMET()->charge());
	 }
    evt.push(TheEvent::MET, p.pdgId(), mcId,
             p.p4(), p.vertex(), p.charge(),
             vector<int>(),
             0, 0, 0, 0, 0, 0, 0, 0, 0, 0, p.sumEt(), p.mEtSig(), 0., 0., 0., 0., 0., int (p.isPFMET()));
    
     if (debug) cout << "     MET: Pt = " <<  p.pt() << endl;
     }
  }
  

// -------------- Gen Particles ----------------------
  if (!iEvent.isRealData()) {
 
     edm::Handle<reco::GenParticleCollection> genHandle;
     //iEvent.getByLabel("prunedGenParticles", genHandle);
     iEvent.getByToken(tokenPrunedGenParticles_, genHandle);   

     // counters for efficiency calculation
     for (size_t i = 0; i < genHandle->size(); ++i) {
         const reco::GenParticle& p = genHandle->at(i);
         if (p.pt() < 20) continue; // suppress weak lepton
//         if (p.status() != 3) continue;
         if (fabs(p.eta()) < 2.4) {
            if (abs(p.pdgId()) == 11) {           // electrons
                NeleMC++;
            }
            else if (abs(p.pdgId()) == 13) {      // muons
                NmuMC++;
            }
            else if (abs(p.pdgId()) == 15) {      // tauons
                NTauMC++;
            }	
         }
     }
     if (debug) { cout << " ----- Number of gens = " << genHandle->size() << endl;
    
        for (size_t i = 0; i < min((size_t) 20, genHandle->size()); ++i) {
            const reco::GenParticle& p = genHandle->at(i); 
            if (p.status() != 3) continue;
           // mother index
            int idx = -1;
            reco::GenParticleCollection::const_iterator candIter;
            for (unsigned int j = 0; j < genHandle->size(); ++j) {
                 const reco::GenParticle& m = genHandle->at(j);
                 if (&m == p.mother()) { 
                    idx = j;
                    break;
                 }
            }
// convert pdgId to particle name
            string name;
            switch (p.pdgId()) {
               case    1: name = "d";     break;
               case   -1: name = "dbar";  break;
               case    2: name = "u";     break;
        case   -2: name = "ubar";  break;
        case    3: name = "s";     break;
        case   -3: name = "sbar";  break;
        case    4: name = "c";     break;
        case   -4: name = "cbar";  break;
        case    5: name = "b";     break;
        case   -5: name = "bbar";  break;
        case    6: name = "t";     break;
        case   -6: name = "tbar";  break;
        case   11: name = "e-";    break;
        case  -11: name = "e+";    break;
        case   12: name = "nu_e";    break;
        case  -12: name = "nu_ebar";    break;
        case   13: name = "mu-";   break;
        case  -13: name = "mu+";   break;
        case   14: name = "nu_mu";   break;
        case  -14: name = "nu_mubar";   break;
        case   15: name = "tau-";  break;
        case  -15: name = "tau+";  break;
        case   16: name = "nu_tau";     break;
        case  -16: name = "nu_taubar";  break;
        case   21: name = "g";     break;
        case   22: name = "gamma"; break;
        case   24: name = "W+";    break;
        case  -24: name = "W-";    break;
        case 2212: name = "p+";    break;
        case  9900024: name = "W_R+";  break;
        case -9900024: name = "W_R-";  break;
        case 9900012: name = "nu_Re";  break;
              case 9900014: name = "nu_Rmu"; break;
              default: { ostringstream ss; ss << p.pdgId(); name = ss.str(); }
           }
    
           if (debug) {
              cout << "     gen: " << setw(6) << left << name << setw(0)
                   << "  P = " << p.p()
                   << "  Pt = " <<  p.pt()
                   << "  eta = " << p.eta()
             << "  phi = " << p.phi()
             << "  charge = "  << p.charge()
             << "  status = " << p.status()
             << "  daughters = " << p.numberOfDaughters()
             << "  mothers = " << p.numberOfMothers()
             << "  mother idx = " << idx
             << endl;
           }
        }
    
     } // end debug for "genParticles"
  
  }
  
  // Fill the tree
     if ((nLeptons >= minLeptons) && (nJets >= minJets)) theTree->Fill();
     if (debug)  evt.print();
}


/*
// Function to extract VBTFloose and isVBTFloose
int PATDump::MuonID(const reco::Muon& m) const
{
    int val = -1;
    //isVBTFloose
    if (m.isGlobalMuon()&&(m.tunePMuonBestTrack()->numberOfValidHits()>10))
      val = 1;
    
    reco::TrackRef gt = m.globalTrack();
    if (!gt.isNull()) {
        //isVBTFtight
        if ((m.tunePMuonBestTrack()->numberOfValidHits()>10) &&
             m.isGlobalMuon()&&
            (gt->hitPattern().numberOfValidMuonHits()>2) &&
            (gt->hitPattern().numberOfValidPixelHits()>2))
        val = 3;
    }
    
    return val;
}

*/


int PATDump::MuonID(const pat::Muon& m, Handle<reco::VertexCollection> pvHand) const
{
    if (!m.isPFMuon()) return 0;
    bool looseMuID = false;
    bool mediumMuID = false;
    bool tightMuID = false;
    bool tightHighPtMuID = false;
    bool tightHighPtGlTiMuID = false;
    for (size_t j = 0; j < pvHand->size(); ++j) {
        const reco::Vertex& vtx = pvHand->at(j);
        if ((!vtx.isFake())) {
                if (muon::isLooseMuon(m))  looseMuID = true;
                if (muon::isMediumMuon(m)) mediumMuID = true;
                if (muon::isTightMuon(m,vtx))tightMuID = true;
                if (muon::isTightMuon(m,vtx)&&muon::isHighPtMuon(m,vtx)) tightHighPtMuID = true;
                if (muon::isTightMuon(m,vtx)&&muon::isHighPtMuon(m,vtx)&&m.muonID("GlobalMuonPromptTight")) tightHighPtGlTiMuID = true;
            }
        }
    if (tightHighPtGlTiMuID) return 5; 
    if (tightHighPtMuID) return 4; 
    if (tightMuID) return 3; 
    if (mediumMuID) return 2;
    if (looseMuID) return 1;
    return 0;
}


int PATDump::TauID(const pat::Tau& t) const
{
    if (!t.isPFTau()) return -1;
    bool looseTauID = t.tauID("decayModeFindingNewDMs")&&t.tauID("againstElectronVLooseMVA5")&&t.tauID("againstMuonLoose3");
    bool looseTauIDiso = t.tauID("decayModeFindingNewDMs")&&t.tauID("byLooseCombinedIsolationDeltaBetaCorr3Hits")&&t.tauID("byLoosePileupWeightedIsolation3Hits")&&t.tauID("againstElectronVLooseMVA5")&&t.tauID("againstMuonLoose3"); 
    bool mediumTauID = t.tauID("decayModeFindingNewDMs")&&t.tauID("byMediumCombinedIsolationDeltaBetaCorr3Hits")&&t.tauID("byMediumPileupWeightedIsolation3Hits")&&t.tauID("againstElectronLooseMVA5")&&t.tauID("againstMuonTight3");
    bool tightTauID = t.tauID("decayModeFindingNewDMs")&&t.tauID("againstElectronMediumMVA5")&&t.tauID("againstMuonTight3");
    bool tightTauIDiso = t.tauID("decayModeFindingNewDMs")&&t.tauID("byTightCombinedIsolationDeltaBetaCorr3Hits")&&t.tauID("byTightPileupWeightedIsolation3Hits")&&t.tauID("againstElectronMediumMVA5")&&t.tauID("againstMuonTight3");
    if (tightTauIDiso) return 4;
    if (tightTauID) return 3; 
    if (mediumTauID) return 2;
    if (looseTauIDiso) return 0;
    if (looseTauID) return 0;
    return -1;
}

// Function for extract JetID
// https://twiki.cern.ch/twiki/bin/view/CMS/JetID
int PATDump::jetID(const pat::Jet& j) const
{
//    PFJetIDSelectionFunctor tight(PFJetIDSelectionFunctor::FIRSTDATA, PFJetIDSelectionFunctor::TIGHT);
//    PFJetIDSelectionFunctor loose(PFJetIDSelectionFunctor::FIRSTDATA, PFJetIDSelectionFunctor::LOOSE);
    
//    if (tight(j)) return 3;
//    if (loose(j)) return 1;

//  https://twiki.cern.ch/twiki/bin/view/CMS/JetID#Recommendations_for_13_TeV_data
    if (!j.isPFJet()) return 0;
     
    double NHF = j.neutralHadronEnergyFraction();
    double NEMF = j.neutralEmEnergyFraction();
    double CHF = j.chargedHadronEnergyFraction();
    double MUF = j.muonEnergyFraction();
    double CEMF = j.chargedEmEnergyFraction();
    double NumConst = j.chargedMultiplicity()+j.neutralMultiplicity();
    double NumNeutralParticles =j.neutralMultiplicity();
    double CHM = j.chargedMultiplicity(); 
    double eta = j.eta();

    bool looseJetID = ((NHF<0.99 && NEMF<0.99 && NumConst>1) && ((abs(eta)<=2.4 && CHF>0 && CHM>0 && CEMF<0.99) || abs(eta)>2.4) && abs(eta)<=3.0)|| (NEMF<0.90 && NumNeutralParticles>10 && abs(eta)>3.0 ) ;
    bool tightJetID = ((NHF<0.90 && NEMF<0.90 && NumConst>1) && ((abs(eta)<=2.4 && CHF>0 && CHM>0 && CEMF<0.99) || abs(eta)>2.4) && abs(eta)<=3.0)|| (NEMF<0.90 && NumNeutralParticles>10 && abs(eta)>3.0 );
    bool tightLepVetoJetID = (NHF<0.90 && NEMF<0.90 && NumConst>1 && MUF<0.8) && ((abs(eta)<=2.4 && CHF>0 && CHM>0 && CEMF<0.90) || abs(eta)>2.4) && abs(eta)<=3.0 ;

    if (tightLepVetoJetID) return 4;
    if (tightJetID) return 3;
    if (looseJetID) return 1;
  
  // neither tight or loose:
  return 0;
}


//define this as a plug-in
DEFINE_FWK_MODULE(PATDump);
