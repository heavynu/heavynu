import FWCore.ParameterSet.Config as cms

process = cms.Process("PATDump")

# verbosity
#process.load("FWCore.MessageService.MessageLogger_cfi")
process.options = cms.untracked.PSet( wantSummary = cms.untracked.bool(False))

# source
process.source = cms.Source("PoolSource", 
  #fileNames = cms.untracked.vstring('file:/store/relval/CMSSW_4_2_3/Electron/RECO/GR_R_42_V12_electron2010B-v2/0000/2CAEFEA6-2C7B-E011-AB6B-00261894395A.root')
  fileNames = cms.untracked.vstring('file:reco.root')
)
process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(-1) )

# this is necessary for PAT to run:
process.load('Configuration/StandardSequences/GeometryDB_cff')
process.load('Configuration/StandardSequences/FrontierConditions_GlobalTag_cff')
process.GlobalTag.globaltag = 'START42_V11::All'
process.load('Configuration/StandardSequences/MagneticField_38T_cff')

# PAT Layer 0+1
process.load("PhysicsTools/PatAlgos/patSequences_cff")

# dump module
process.load("heavynu.PATDump.patdump_cfi")
process.patdump.debug = cms.bool(True)

process.TFileService = cms.Service("TFileService", fileName = cms.string('heavynu.root') )

process.p = cms.Path(
  process.patDefaultSequence +
  process.patdump
)
