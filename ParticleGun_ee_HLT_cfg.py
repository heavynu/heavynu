import FWCore.ParameterSet.Config as cms

process = cms.Process("HLT")

process.load("Configuration.StandardSequences.Services_cff")
process.load("FWCore.MessageService.MessageLogger_cfi")
process.options = cms.untracked.PSet(
  wantSummary = cms.untracked.bool(False)
)

process.maxEvents = cms.untracked.PSet(
  input = cms.untracked.int32(10)
)

process.source = cms.Source("EmptySource")

process.generator = cms.EDProducer("FlatRandomEGunProducer",
    PGunParameters = cms.PSet(
        # you can request more than 1 particle
        # since PartID is a vector, you can place in as many
        # PDG id's as you wish, comma seaparated
        #
        PartID = cms.vint32(11),
        MinEta = cms.double(-2.4),
        MaxEta = cms.double(2.4),
        MinPhi = cms.double(-3.14159265359),
        MaxPhi = cms.double(3.14159265359),
        MinE = cms.double(100.0),
        MaxE = cms.double(100.0)
    ),
    Verbosity = cms.untracked.int32(0), ## set to 1 (or greater)  for printouts
    AddAntiParticle = cms.bool(True)    ## back-to-back particles
)

process.load("Configuration.StandardSequences.GeometryExtended_cff")
process.load("Configuration.StandardSequences.MagneticField_38T_cff")
process.load("Configuration.StandardSequences.VtxSmearedRealistic7TeVCollision_cff")

# Conditions: fake or frontier
#process.load("Configuration.StandardSequences.FakeConditions_cff")
# https://twiki.cern.ch/twiki/bin/view/CMS/SWGuideFrontierConditions
process.load("Configuration.StandardSequences.FrontierConditions_GlobalTag_cff")
process.GlobalTag.globaltag = 'START38_V14::All'

process.load("Configuration.StandardSequences.Generator_cff")
process.load("Configuration.StandardSequences.Simulation_cff")
process.load("Configuration.StandardSequences.MixingNoPileUp_cff")
process.load("Configuration.StandardSequences.L1Emulator_cff")
process.load("Configuration.StandardSequences.DigiToRaw_cff")
process.load('Configuration/StandardSequences/EndOfProcess_cff')

# run HLT
process.load('HLTrigger/Configuration/HLT_GRun_cff')

process.hltTrigReport = cms.EDAnalyzer( "HLTrigReport",
  HLTriggerResults = cms.InputTag( 'TriggerResults','','HLT' )
)

process.load("Configuration.EventContent.EventContent_cff")
process.FEVT = cms.OutputModule("PoolOutputModule",
    process.FEVTDEBUGHLTEventContent,
    fileName = cms.untracked.string('hlt.root')
)



# Path and EndPath definitions
process.generator_step     = cms.Path(process.generator)
process.generation_step    = cms.Path(process.pgen)
process.simulation_step    = cms.Path(process.simulation)
process.L1simulation_step  = cms.Path(process.SimL1Emulator)
process.digi2raw_step      = cms.Path(process.DigiToRaw)
process.hltTrigReport_step = cms.Path(process.hltTrigReport)
process.endjob_step        = cms.Path(process.endOfProcess)
process.out_step           = cms.EndPath(process.FEVT)

# Schedule definition
process.schedule = cms.Schedule(process.generator_step,
                                process.generation_step,
                                process.simulation_step,
                                process.L1simulation_step,
                                process.digi2raw_step)
process.schedule.extend(process.HLTSchedule)
process.schedule.append(process.hltTrigReport_step)
process.schedule.extend([process.endjob_step,process.out_step])




#process.GenSimDigiL1Raw = cms.Path(process.generator+process.pgen+process.simulation+process.L1Emulator+process.DigiToRaw)
#process.schedule = cms.Schedule(process.GenSimDigiL1Raw)
#process.schedule.extend( process.HLTSchedule )
#process.HLTAnalyzerEndpath = cms.EndPath(  process.hltTrigReport )
#process.schedule.append(process.HLTAnalyzerEndpath)
#process.outpath = cms.EndPath(process.FEVT)
#process.schedule.append(process.outpath)
