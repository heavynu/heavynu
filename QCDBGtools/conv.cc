#include <iostream>
#include <fstream>
#include <cmath>
#include "TLorentzVector.h"

using namespace std;

double fakerate(double eta, double pt, int iclosej)
{
    //cout << "iclosej = " << iclosej << endl;
    double w;
    if(fabs(eta) < 1.44) {                            // barrel
      if(!iclosej) { // no close jet
        if(pt >= 50.) w = 0.004;
        if(pt < 50.) w = 0.0024 + 0.0016*(pt-30.)/20.;
        if(pt > 60.) w = 0.0052;
      } else { // close jet
        if(pt >= 50.) w = 0.0032;
        if(pt < 50.) w = 0.0019 + 0.0013*(pt-30.)/20.;
        if(pt > 60.) w = 0.0052;
      }
    }
    if(fabs(eta) >= 1.44 && fabs(eta) < 2.) {         // endcap
      if(!iclosej) { // no close jet
        if(pt >= 50.) w = 0.01;
        if(pt < 50.) w = 0.005 + 0.005*(pt-30.)/20.;
        if(pt > 60.) w = 0.013;
        if(pt > 84.) w = 0.0068 + 0.0000784274 * pt;
      } else { // close jet
        if(pt >= 50.) w = 0.0089;
        if(pt < 50.) w = 0.0035 + 0.0054*(pt-30.)/20.;
        if(pt > 60.) w = 0.013;
        if(pt > 84.) w = 0.0068 + 0.0000784274 * pt;
      }
    }
    if(fabs(eta) >= 2.) {                             // endcap2
      if(!iclosej) { // no close jet
        if(pt >= 50.) w = 0.023;
        if(pt < 50.) w = 0.010 + 0.013*(pt-30.)/20.;
        if(pt > 60.) w = 0.0277 + 0.00004 * pt;
      } else { // close jet
        if(pt >= 50.) w = 0.0206;
        if(pt < 50.) w = 0.0086 + 0.012*(pt-30.)/20.;
        if(pt > 60.) w = 0.0277 + 0.00004 * pt;
      }
    }
    return w;
}

int main(void)
{
  int iev, irun, ilumi, nclusg, nclusb, njet, njetg, iclus, ijet, ijet1, ijet2;
  int nele, neleg, HEEPID, ifile, iele, iclusg;
  double tmp2, tmp3, tmp4;
  double px, py, pz, e;
  double pxcg[100], pycg[100], pzcg[100], ecg[100], etacg[100];
  double pxcb[100], pycb[100], pzcb[100], ecb[100], etacb[100];
  double pxe[100], pye[100], pze[100], ee[100];
  double pxj[100], pyj[100], pzj[100], ej[100];
  double pxjg[100], pyjg[100], pzjg[100], ejg[100], etajg[100];
  int lisol, nclus;
  char jopa[200];
  double tmp1, xisol, hoe;
  double pt, theta, eta, eta1, phi, phi1, r, r1;
  double pxmet, pymet, pmet;
  int coinc, NZPeak=0, NEleHard=0;
  string filenames[13] = {"datlq1.dat", "datlq2.dat", "datlq3.dat",
                         "datlq4.dat", "datlq5.dat", "datlq6.dat",
                         "datlq7.dat", "datlq8.dat",
                         "datlq11.dat", "datlq12.dat", "datlq13.dat",
                         "datlq14.dat", "datlq15.dat"};


  ofstream fout("dstmk.d");
  ifstream fin;
  for(ifile=0; ifile<13; ifile++) {
  fin.open(filenames[ifile].c_str());
  cout << "opened file " << filenames[ifile] << endl;
  while (!fin.eof()) {
    fin >> iev >> irun >> ilumi;
    fin.getline(jopa, 200);
    fin >> nclus;
    nclusg = 0;
    nclusb = 0;
    for(iclus=0; iclus < nclus; iclus++) {
      fin >> px >> py >> pz >> e;
      fin >> tmp1 >> xisol >> hoe;
      lisol = 0;
      if(hoe > 0.05) lisol = 1;
      pt = sqrt(px*px+py*py);
      theta = atan(pt/fabs(pz));
      eta = -log(tan(0.5*theta));
      if(pz < 0.) eta = -eta;
      if(fabs(eta) < 1.44) {
        if(xisol > 6.+0.01*pt) lisol = 1;
      } else {
        if(pt <= 50.) {
          if(xisol > 6.) lisol = 1;
        } else {
          if(xisol > 6.+0.01*(pt-50.)) lisol = 1;
        }
      }
      if(!lisol) {
        pxcg[nclusg]=px; pycg[nclusg]=py; pzcg[nclusg]=pz; ecg[nclusg]=e;
        etacg[nclusg] = eta;
        nclusg++;
      } else {
        pxcb[nclusg]=px; pycb[nclusg]=py; pzcb[nclusg]=pz; ecb[nclusg]=e;
        etacb[nclusg] = eta;
        nclusb++;
      }
      fin.getline(jopa, 200);
    }
    fin >> nele;
    neleg = 0;
    for(iele=0; iele < nele; iele++) {
      fin >> pxe[neleg] >> pye[neleg] >> pze[neleg] >> ee[neleg];
      fin >> tmp1 >> tmp2 >> tmp3 >> tmp4 >> hoe;
      fin >> HEEPID;
      if(!HEEPID) neleg++;
      fin.getline(jopa, 200);
    }
    fin >> njet;
    for(ijet=0; ijet < njet; ijet++) {
      fin >> pxj[ijet] >> pyj[ijet] >> pzj[ijet] >> ej[ijet];
      fin.getline(jopa, 200);
    }
    fin >> pxmet >> pymet >> pmet;
    fin.getline(jopa, 200);

    if(nclusg < 2) continue;

    njetg=0;                                    // clean jets
    for(ijet=0; ijet < njet; ijet++) {
      pt = sqrt(pxj[ijet]*pxj[ijet] + pyj[ijet]*pyj[ijet]);
      theta = atan(pt/fabs(pzj[ijet]));
      eta = -log(tan(0.5*theta));
      if(pzj[ijet] < 0.) eta = -eta;
      phi = acos(pxj[ijet]/pt);
      coinc=0;
      for(iclus=0; iclus < 2; iclus++) {
        pt = sqrt(pxcg[iclus]*pxcg[iclus]+pycg[iclus]*pycg[iclus]);
        eta1 = etacg[iclus];
        phi1 = acos(pxcg[iclus]/pt);
        r1 = phi - phi1;
        if(fabs(r1-6.282) < fabs(r1)) r1 = r1-6.282;
        if(fabs(r1+6.282) < fabs(r1)) r1 = r1+6.282;
        r = eta - eta1;
        double d = sqrt(r1*r1 + r*r);
        if(d < 0.5)coinc=1;
      }
      if(!coinc) {
        pxjg[njetg] = pxj[ijet]; pyjg[njetg] = pyj[ijet];
        pzjg[njetg] = pzj[ijet]; ejg[njetg] = ej[ijet];
        etajg[njetg] = eta;
        njetg++;
      }
    }

    if(neleg >= 2 && njetg > 1) {
      TLorentzVector p1(pxe[0], pye[0], pze[0], ee[0]);
      TLorentzVector p2(pxe[1], pye[1], pze[1], ee[1]);
      if(p1.Pt() > 60. || p2.Pt() > 60.) {
        double xmcc = (p1 + p2).M();
        if(fabs(xmcc-90.) < 15.) NZPeak++;
      }
    }
    if(neleg && njetg) {
      TLorentzVector p1(pxe[0], pye[0], pze[0], ee[0]);
      if(p1.Pt() > 60.) NEleHard++;
    }

    if(njetg + nclusb + (nclusg-2) < 2) continue;

    if(nclusb) {
      for(iclus=0; iclus < nclusb; iclus++) {
        pt = sqrt(pxcb[iclus]*pxcb[iclus]+pycb[iclus]*pycb[iclus]);
        phi = acos(pxcb[iclus]/pt);
        coinc = 0;
        for(ijet=0; ijet < njetg; ijet++) {
          pt = sqrt(pxjg[ijet]*pxjg[ijet] + pyjg[ijet]*pyjg[ijet]);
          phi1 = acos(pxjg[ijet]/pt);
          r1 = phi - phi1;
          if(fabs(r1-6.282) < fabs(r1)) r1 = r1-6.282;
          if(fabs(r1+6.282) < fabs(r1)) r1 = r1+6.282;
          r = etacb[iclus] - etajg[ijet];
          double d = sqrt(r1*r1 + r*r);
          if(d < 0.5)coinc=1;
        }
        for(iclusg=0; iclusg < 2; iclusg++) {
          pt = sqrt(pxcg[iclusg]*pxcg[iclusg]+pycg[iclusg]*pycg[iclusg]);
          phi1 = acos(pxcg[iclusg]/pt);
          r1 = phi - phi1;
          if(fabs(r1-6.282) < fabs(r1)) r1 = r1-6.282;
          if(fabs(r1+6.282) < fabs(r1)) r1 = r1+6.282;
          r = etacb[iclus] - etacg[iclusg];
          double d = sqrt(r1*r1 + r*r);
          if(d < 0.5)coinc=1;
        }
        if(!coinc) {
          pxjg[njetg] = pxcb[iclus]; pyjg[njetg] = pycb[iclus];
          pzjg[njetg] = pzcb[iclus]; ejg[njetg] = ecb[iclus];
          etajg[njetg] = etacb[iclus];
          njetg++;
        }
      }
    }

    if(nclusg > 2) {
      for(iclus=2; iclus < nclusg; iclus++) {
        pt = sqrt(pxcg[iclus]*pxcg[iclus]+pycg[iclus]*pycg[iclus]);
        phi = acos(pxcg[iclus]/pt);
        coinc = 0;
        for(ijet=0; ijet < njetg; ijet++) {
          pt = sqrt(pxjg[ijet]*pxjg[ijet] + pyjg[ijet]*pyjg[ijet]);
          phi1 = acos(pxjg[ijet]/pt);
          r1 = phi - phi1;
          if(fabs(r1-6.282) < fabs(r1)) r1 = r1-6.282;
          if(fabs(r1+6.282) < fabs(r1)) r1 = r1+6.282;
          r = etacg[iclus] - etajg[ijet];
          double d = sqrt(r1*r1 + r*r);
          if(d < 0.5)coinc=1;
        }
        for(iclusg=0; iclusg < 2; iclusg++) {
          pt = sqrt(pxcg[iclusg]*pxcg[iclusg]+pycg[iclusg]*pycg[iclusg]);
          phi1 = acos(pxcg[iclusg]/pt);
          r1 = phi - phi1;
          if(fabs(r1-6.282) < fabs(r1)) r1 = r1-6.282;
          if(fabs(r1+6.282) < fabs(r1)) r1 = r1+6.282;
          r = etacg[iclus] - etacg[iclusg];
          double d = sqrt(r1*r1 + r*r);
          if(d < 0.5)coinc=1;
        }
        if(!coinc) {
          pxjg[njetg] = pxcg[iclus]; pyjg[njetg] = pycg[iclus];
          pzjg[njetg] = pzcg[iclus]; ejg[njetg] = ecg[iclus];
          etajg[njetg] = etacg[iclus];
          njetg++;
        }
      }
    }

    pt = sqrt(pxcg[0]*pxcg[0]+pycg[0]*pycg[0]);
    phi = acos(pxcg[0]/pt);
    int iclosej = 0;
    for(ijet=0; ijet < njetg; ijet++) {
      pt = sqrt(pxjg[ijet]*pxjg[ijet] + pyjg[ijet]*pyjg[ijet]);
      phi1 = acos(pxjg[ijet]/pt);
      r1 = phi - phi1;
      if(fabs(r1-6.282) < fabs(r1)) r1 = r1-6.282;
      if(fabs(r1+6.282) < fabs(r1)) r1 = r1+6.282;
      r = etacg[0] - etajg[ijet];
      double d = sqrt(r1*r1 + r*r);
      if(d < 1.) iclosej=1;
    }
    int icloseclus = 0;
    pt = sqrt(pxcg[1]*pxcg[1]+pycg[1]*pycg[1]);
    phi1 = acos(pxcg[1]/pt);
    r1 = phi - phi1;
    if(fabs(r1-6.282) < fabs(r1)) r1 = r1-6.282;
    if(fabs(r1+6.282) < fabs(r1)) r1 = r1+6.282;
    r = etacg[0] - etacg[1];
    double d = sqrt(r1*r1 + r*r);
    if(d < 1.) icloseclus=1;
    if(icloseclus) iclosej=1;
    pt = sqrt(pxcg[0]*pxcg[0]+pycg[0]*pycg[0]);
    double w = fakerate(etacg[0], pt, iclosej);
    double w1 = w;
    double w2 = 0.;

    pt = sqrt(pxcg[1]*pxcg[1]+pycg[1]*pycg[1]);
    phi = acos(pxcg[1]/pt);
    iclosej = 0;
    for(ijet=0; ijet < njetg; ijet++) {
      pt = sqrt(pxjg[ijet]*pxjg[ijet] + pyjg[ijet]*pyjg[ijet]);
      phi1 = acos(pxjg[ijet]/pt);
      r1 = phi - phi1;
      if(fabs(r1-6.282) < fabs(r1)) r1 = r1-6.282;
      if(fabs(r1+6.282) < fabs(r1)) r1 = r1+6.282;
      r = etacg[1] - etajg[ijet];
      double d = sqrt(r1*r1 + r*r);
      if(d < 1.) iclosej=1;
    }
    if(icloseclus) iclosej=1;
    pt = sqrt(pxcg[1]*pxcg[1]+pycg[1]*pycg[1]);
    w2 = fakerate(etacg[1], pt, iclosej);

    w = w1 * w2;

    if(njetg < 2) continue;
    //if(nclusg > 2) cout << "more than 2 good clus" << endl;

    ijet1 = 0;
    double ptmax = 0.;
    for(ijet=0; ijet < njetg; ijet++) {
      pt = sqrt(pxjg[ijet]*pxjg[ijet] + pyjg[ijet]*pyjg[ijet]);
      if(pt > ptmax) {ptmax = pt; ijet1 = ijet;}
    }
    ijet2 = 0;
    ptmax = 0.;
    for(ijet=0; ijet < njetg; ijet++) {
      pt = sqrt(pxjg[ijet]*pxjg[ijet] + pyjg[ijet]*pyjg[ijet]);
      if(ijet != ijet1 && pt > ptmax) {ptmax = pt; ijet2 = ijet;}
    }

    if(fabs(etacg[0]) > 1.44) { // corrections for ECAL degradation
      pxcg[0]*=1.047; pycg[0]*=1.047; pzcg[0]*=1.047; ecg[0]*=1.047;
    }
    if(fabs(etacg[1]) > 1.44) {
      pxcg[1]*=1.047; pycg[1]*=1.047; pzcg[1]*=1.047; ecg[1]*=1.047;
    }

    fout << iev << " event" << endl;
    fout << " 2 " << 2.78*w << " 27" << endl; // weight renorm 36/pb� -> 100/pb

    fout << "0 " << pxjg[ijet1] << " " <<  pyjg[ijet1] << " "
         << pzjg[ijet1] << " " << ejg[ijet1] << " 0" << endl;
    fout << "0 " << pxjg[ijet2] << " " <<  pyjg[ijet2] << " " 
         << pzjg[ijet2] << " " << ejg[ijet2] << " 0" << endl;

    nclus=2;
    fout << nclus << " leptons" << endl;
    for(iclus=0; iclus < nclus; iclus++) {
      fout << "1 " << pxcg[iclus] << " " <<  pycg[iclus] << " "
           << pzcg[iclus] << " " << ecg[iclus] << " 0"
           << " 0 40 0 0" << endl;
    }
    fout << "0 0 0 0" << endl;
  }
  cout << "closing file " << filenames[ifile] << endl;
  fin.close();
  }
  cout << "N events in Z peak = " << NZPeak << endl;
  cout << "N hard electrons = " << NEleHard << endl;
}
