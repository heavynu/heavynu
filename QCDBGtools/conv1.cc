// Determine fake rates: ratio e / superclusters in c + jets events

#include <iostream>
#include <fstream>
#include <cmath>
#include "TFile.h"
#include "TH1.h"

using namespace std;

int main(void)
{
  int iev, irun, ilumi, nclusg, nclusb, njet, njetg, iclus, ijet, ijet1, ijet2;
  int nele, neleg, iele, HEEPID, nejj=0, neletot=0, nelecut;
  double nejjpred=0.;
  double px, py, pz, e;
  double pxcg[100], pycg[100], pzcg[100], ecg[100], etacg[100];
  double pxcb[100], pycb[100], pzcb[100], ecb[100], etacb[100];
  double pxe[100], pye[100], pze[100], ee[100];
  double pxj[100], pyj[100], pzj[100], ej[100];
  double pxjg[100], pyjg[100], pzjg[100], ejg[100], etajg[100];
  int lisol, nclus;
  char jopa[200];
  double tmp1, tmp2, tmp3, tmp4, xisol, hoe;
  double pt, theta, eta, eta1, phi, phi1, r, r1;
  double pxmet, pymet, pmet;
  int coinc, ifile;
  string filenames[13] = {"datlq1.dat", "datlq2.dat", "datlq3.dat",
                         "datlq4.dat", "datlq5.dat", "datlq6.dat",
                         "datlq7.dat", "datlq8.dat",
                         "datlq11.dat", "datlq12.dat", "datlq13.dat",
                         "datlq14.dat", "datlq15.dat"};

  TFile* hOutputFile;
  hOutputFile = new TFile("hist.root","RECREATE");
  
  TH1D* PtBarrel = new TH1D("PtBarrel", "Pt", 25, 0., 500.);
  TH1D* PtBarrelPred = new TH1D("PtBarrelPred", "Pt", 25, 0., 500.);
  TH1D* PtEndcap = new TH1D("PtEndcap", "Pt", 25, 0., 500.);
  TH1D* PtEndcapPred = new TH1D("PtEndcapPred", "Pt", 25, 0., 500.);
  TH1D* PtEndcap2 = new TH1D("PtEndcap2", "Pt", 25, 0., 500.);
  TH1D* PtEndcap2Pred = new TH1D("PtEndcap2Pred", "Pt", 25, 0., 500.);

  ifstream fin;
  for(ifile=8; ifile<13; ifile++) {
  fin.open(filenames[ifile].c_str());
  cout << "opened file " << filenames[ifile] << endl;
  //ifstream fin("dstlq.d");
  while (!fin.eof()) {
    fin >> iev >> irun >> ilumi;
    fin.getline(jopa, 200);
    if(irun > 145762) cout << "Run B !" << endl;
    fin >> nclus;
    nclusg = 0;
    nclusb = 0;
    for(iclus=0; iclus < nclus; iclus++) {
      fin >> px >> py >> pz >> e;
      fin >> tmp1 >> xisol >> hoe;
      lisol = 0;
      if(hoe > 0.05) lisol = 1;
      pt = sqrt(px*px+py*py);
      theta = atan(pt/fabs(pz));
      eta = -log(tan(0.5*theta));
      if(pz < 0.) eta = -eta;
      if(fabs(eta) < 1.56) {
        if(xisol > 6.+0.01*pt) lisol = 1;
      } else {
        if(pt <= 50.) {
          if(xisol > 6.) lisol = 1;
        } else {
          if(xisol > 6.+0.01*(pt-50.)) lisol = 1;
        }
      }
      if(!lisol) {
        pxcg[nclusg]=px; pycg[nclusg]=py; pzcg[nclusg]=pz; ecg[nclusg]=e;
        etacg[nclusg] = eta;
        nclusg++;
      } else {
        pxcb[nclusg]=px; pycb[nclusg]=py; pzcb[nclusg]=pz; ecb[nclusg]=e;
        etacb[nclusg] = eta;
        nclusb++;
      }
      fin.getline(jopa, 200);
    }
    fin >> nele;
    neleg = 0;
    nelecut = 0;
    for(iele=0; iele < nele; iele++) {
      fin >> pxe[neleg] >> pye[neleg] >> pze[neleg] >> ee[neleg];
      fin >> tmp1 >> tmp2 >> tmp3 >> tmp4 >> hoe;
      fin >> HEEPID;
      if(!HEEPID && sqrt(pxe[neleg]*pxe[neleg]+pye[neleg]*pye[neleg]) > 42.)
        nelecut++;
      if(!HEEPID) neleg++;
      fin.getline(jopa, 200);
    }
    fin >> njet;
    for(ijet=0; ijet < njet; ijet++) {
      fin >> pxj[ijet] >> pyj[ijet] >> pzj[ijet] >> ej[ijet];
      fin.getline(jopa, 200);
    }
    fin >> pxmet >> pymet >> pmet;
    fin.getline(jopa, 200);

    if(nclusg < 1) continue;

    njetg=0;                                   // clean jets
    for(ijet=0; ijet < njet; ijet++) {
      pt = sqrt(pxj[ijet]*pxj[ijet] + pyj[ijet]*pyj[ijet]);
      theta = atan(pt/fabs(pzj[ijet]));
      eta = -log(tan(0.5*theta));
      if(pzj[ijet] < 0.) eta = -eta;
      phi = acos(pxj[ijet]/pt);
      coinc=0;
      for(iclus=0; iclus < nclusg; iclus++) {
        pt = sqrt(pxcg[iclus]*pxcg[iclus]+pycg[iclus]*pycg[iclus]);
        eta1 = etacg[iclus];
        phi1 = acos(pxcg[iclus]/pt);
        r1 = phi - phi1;
        if(fabs(r1-6.282) < fabs(r1)) r1 = r1-6.282;
        if(fabs(r1+6.282) < fabs(r1)) r1 = r1+6.282;
        r = eta - eta1;
        double d = sqrt(r1*r1 + r*r);
        if(d < 0.5)coinc=1;
      }
      if(!coinc) {
        pxjg[njetg] = pxj[ijet]; pyjg[njetg] = pyj[ijet];
        pzjg[njetg] = pzj[ijet]; ejg[njetg] = ej[ijet];
        etajg[njetg] = eta;
        njetg++;
      }
    }

    if(nclusb) {          // add bad clusters to jets if they don't coinside
      for(iclus=0; iclus < nclusb; iclus++) {
        pt = sqrt(pxcb[iclus]*pxcb[iclus]+pycb[iclus]*pycb[iclus]);
        phi = acos(pxcb[iclus]/pt);
        coinc = 0;
        for(ijet=0; ijet < njetg; ijet++) {
          pt = sqrt(pxjg[ijet]*pxjg[ijet] + pyjg[ijet]*pyjg[ijet]);
          phi1 = acos(pxjg[ijet]/pt);
          r1 = phi - phi1;
          if(fabs(r1-6.282) < fabs(r1)) r1 = r1-6.282;
          if(fabs(r1+6.282) < fabs(r1)) r1 = r1+6.282;
          r = etacb[iclus] - etajg[ijet];
          double d = sqrt(r1*r1 + r*r);
          if(d < 0.5)coinc=1;
        }
        if(!coinc) {
          pxjg[njetg] = pxcb[iclus]; pyjg[njetg] = pycb[iclus];
          pzjg[njetg] = pzcb[iclus]; ejg[njetg] = ecb[iclus];
          etajg[njetg] = etacb[iclus];
          njetg++;
        }
      }
    }

    if(njetg + (nclusg-neleg) > 0) neletot += nelecut;

    if(nclusg > 1) continue;
    if(njetg < 1) continue;
    if(pmet > 20.) continue;

    pt = sqrt(pxcg[0]*pxcg[0]+pycg[0]*pycg[0]);
    phi = acos(pxcg[0]/pt);
    int iokj=0;
    int iclosej=0;
    for(ijet=0; ijet < njetg; ijet++) {
      double pt1 = sqrt(pxjg[ijet]*pxjg[ijet] + pyjg[ijet]*pyjg[ijet]);
      phi1 = acos(pxjg[ijet]/pt1);
      r1 = phi - phi1;
      if(fabs(r1-6.282) < fabs(r1)) r1 = r1-6.282;
      if(fabs(r1+6.282) < fabs(r1)) r1 = r1+6.282;
      if(fabs(r1) > 1.57) iokj=1;
      double r2 = etacg[0] - etajg[ijet];
      if(sqrt(r1*r1+r2*r2) < 1.) iclosej = 1;
    }
    if(!iokj || iclosej) continue;

    //if(neleg) cout << iev << endl;

    if(fabs(etacg[0]) < 1.44)
      PtBarrelPred->Fill(sqrt(pxcg[0]*pxcg[0]+pycg[0]*pycg[0]));
    if(fabs(etacg[0]) >= 1.44 && fabs(etacg[0]) < 2.)
      PtEndcapPred->Fill(sqrt(pxcg[0]*pxcg[0]+pycg[0]*pycg[0]));
    if(fabs(etacg[0]) >= 2.)
      PtEndcap2Pred->Fill(sqrt(pxcg[0]*pxcg[0]+pycg[0]*pycg[0]));
    if(neleg) {
      pt = sqrt(pxe[0]*pxe[0] + pye[0]*pye[0]);
      theta = atan(pt/fabs(pze[0]));
      eta = -log(tan(0.5*theta));
      if(pze[0] < 0.) eta = -eta;
      if(fabs(eta) < 1.44) PtBarrel->Fill(sqrt(pxe[0]*pxe[0]+pye[0]*pye[0]));
      if(fabs(eta) >= 1.44 && fabs(eta) < 2.)
        PtEndcap->Fill(sqrt(pxe[0]*pxe[0]+pye[0]*pye[0]));
      if(fabs(eta) >= 2.) PtEndcap2->Fill(sqrt(pxe[0]*pxe[0]+pye[0]*pye[0]));
    }

  }
  cout << "closing file " << filenames[ifile] << endl;
  fin.close();
  }

  cout << "N Total electrons = " << neletot << endl;

  hOutputFile->Write();
  hOutputFile->Close();

}
