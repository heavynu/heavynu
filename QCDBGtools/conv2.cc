// closure test: compare ec+jets and prediction from cc+jets

#include <iostream>
#include <fstream>
#include <cmath>
#include "TFile.h"
#include "TH1.h"
#include "TLorentzVector.h"

using namespace std;

double fakerate(double eta, double pt, int iclosej)
{
    //cout << "iclosej = " << iclosej << endl;
    double w;
    if(fabs(eta) < 1.44) {                            // barrel
      if(!iclosej) { // no close jet
        if(pt >= 50.) w = 0.004;
        if(pt < 50.) w = 0.0024 + 0.0016*(pt-30.)/20.;
        if(pt > 60.) w = 0.0052;
      } else { // close jet
        if(pt >= 50.) w = 0.0032;
        if(pt < 50.) w = 0.0019 + 0.0013*(pt-30.)/20.;
        if(pt > 60.) w = 0.0052;
      }
    }
    if(fabs(eta) >= 1.44 && fabs(eta) < 2.) {         // endcap
      if(!iclosej) { // no close jet
        if(pt >= 50.) w = 0.01;
        if(pt < 50.) w = 0.005 + 0.005*(pt-30.)/20.;
        if(pt > 60.) w = 0.013;
        if(pt > 84.) w = 0.0068 + 0.0000784274 * pt;
      } else { // close jet
        if(pt >= 50.) w = 0.0089;
        if(pt < 50.) w = 0.0035 + 0.0054*(pt-30.)/20.;
        if(pt > 60.) w = 0.013;
        if(pt > 84.) w = 0.0068 + 0.0000784274 * pt;
      }
    }
    if(fabs(eta) >= 2.) {                             // endcap2
      if(!iclosej) { // no close jet
        if(pt >= 50.) w = 0.023;
        if(pt < 50.) w = 0.010 + 0.013*(pt-30.)/20.;
        if(pt > 60.) w = 0.0277 + 0.00004 * pt;
      } else { // close jet
        if(pt >= 50.) w = 0.0206;
        if(pt < 50.) w = 0.0086 + 0.012*(pt-30.)/20.;
        if(pt > 60.) w = 0.0277 + 0.00004 * pt;
      }
    }
    return w;
}


int main(void)
{
  int iev, irun, ilumi, nclusg, nclusb, njet, njetg, iclus, ijet, ijet1, ijet2;
  int nele, neleg, iele, HEEPID, necj=0;
  double necjpred=0.;
  double px, py, pz, e;
  double pxcg[100], pycg[100], pzcg[100], ecg[100], etacg[100];
  double pxcb[100], pycb[100], pzcb[100], ecb[100], etacb[100];
  double pxe[100], pye[100], pze[100], ee[100];
  double pxj[100], pyj[100], pzj[100], ej[100];
  double pxjg[100], pyjg[100], pzjg[100], ejg[100], etajg[100];
  int lisol, nclus;
  char jopa[200];
  double tmp1, tmp2, tmp3, tmp4, xisol, hoe;
  double pt, theta, eta, eta1, phi, phi1, r, r1;
  double pxmet, pymet, pmet;
  int coinc, ifile;
  string filenames[13] = {"datlq1.dat", "datlq2.dat", "datlq3.dat",
                         "datlq4.dat", "datlq5.dat", "datlq6.dat",
                         "datlq7.dat", "datlq8.dat",
                         "datlq11.dat", "datlq12.dat", "datlq13.dat",
                         "datlq14.dat", "datlq15.dat"};

  TFile* hOutputFile;
  hOutputFile = new TFile("hist.root","RECREATE");
  
  TH1D* PtBarrel = new TH1D("PtBarrel", "Pt", 25, 0., 500.);
  TH1D* PtBarrelPred = new TH1D("PtBarrelPred", "Pt", 25, 0., 500.);
  TH1D* PtEndcap = new TH1D("PtEndcap", "Pt", 25, 0., 500.);
  TH1D* PtEndcapPred = new TH1D("PtEndcapPred", "Pt", 25, 0., 500.);
  TH1D* PtEndcap2 = new TH1D("PtEndcap2", "Pt", 25, 0., 500.);
  TH1D* PtEndcap2Pred = new TH1D("PtEndcap2Pred", "Pt", 25, 0., 500.);

  cout << "creating fin" << endl;
  ifstream fin;
  for(ifile=0; ifile<13; ifile++) {
  fin.open(filenames[ifile].c_str());
  cout << "opened file " << filenames[ifile] << endl;
  while (!fin.eof()) {
    fin >> iev >> irun >> ilumi;
    fin.getline(jopa, 200);
    fin >> nclus;
    nclusg = 0;
    nclusb = 0;
    for(iclus=0; iclus < nclus; iclus++) {
      fin >> px >> py >> pz >> e;
      fin >> tmp1 >> xisol >> hoe;
      lisol = 0;
      if(hoe > 0.05) lisol = 1;
      pt = sqrt(px*px+py*py);
      theta = atan(pt/fabs(pz));
      eta = -log(tan(0.5*theta));
      if(pz < 0.) eta = -eta;
      if(fabs(eta) < 1.44) {
        if(xisol > 6.+0.01*pt) lisol = 1;
      } else {
        if(pt <= 50.) {
          if(xisol > 6.) lisol = 1;
        } else {
          if(xisol > 6.+0.01*(pt-50.)) lisol = 1;
        }
      }
      if(!lisol) {
        pxcg[nclusg]=px; pycg[nclusg]=py; pzcg[nclusg]=pz; ecg[nclusg]=e;
        etacg[nclusg] = eta;
        nclusg++;
      } else {
        pxcb[nclusg]=px; pycb[nclusg]=py; pzcb[nclusg]=pz; ecb[nclusg]=e;
        etacb[nclusg] = eta;
        nclusb++;
      }
      fin.getline(jopa, 200);
    }
    fin >> nele;
    neleg = 0;
    for(iele=0; iele < nele; iele++) {
      fin >> pxe[neleg] >> pye[neleg] >> pze[neleg] >> ee[neleg];
      fin >> tmp1 >> tmp2 >> tmp3 >> tmp4 >> hoe;
      fin >> HEEPID;
      if(!HEEPID) {
//        pt = sqrt(pxe[neleg]*pxe[neleg]+pye[neleg]*pye[neleg]);
//        theta = atan(pt/fabs(pz));
//        eta = -log(tan(0.5*theta));
//        if(pz < 0.) eta = -eta;
//        cout << eta << " " << tmp3 << " " << tmp4 << " " << hoe
//             << " " << HEEPID << endl;
//        if(fabs(eta) < 1.44 && tmp4 > 7.5) cout << "bad!" << endl;
//        if(fabs(eta) < 1.44 && tmp3 > (2.+0.03*pt)) cout << "bad!" << endl;
//        if(fabs(eta) > 1.56) {
//          if(tmp4 > 15.) cout << "bad!" << endl;
//          if(pt < 50.) {
//            if(tmp3 > 2.5) cout << "bad!" << endl;
//          } else {
//            if(tmp3 > 2.5+0.03*(pt-50.)) cout << "bad!" << endl;
//          }
//        }
        neleg++;
      }
      fin.getline(jopa, 200);
    }
    fin >> njet;
    for(ijet=0; ijet < njet; ijet++) {
      fin >> pxj[ijet] >> pyj[ijet] >> pzj[ijet] >> ej[ijet];
      fin.getline(jopa, 200);
    }
    fin >> pxmet >> pymet >> pmet;
    fin.getline(jopa, 200);

    if(nclusg < 2) continue;
    if(nclusg > 2) continue;
    if(pmet > 20.) continue;
    if(neleg > 1) continue;
    pt = sqrt(pxcg[0]*pxcg[0]+pycg[0]*pycg[0]);
    double pt1 = sqrt(pxcg[1]*pxcg[1]+pycg[1]*pycg[1]);
    //if(pt < 40. && pt1 < 40.) continue;


    njetg=0;                                // clean jets:
    for(ijet=0; ijet < njet; ijet++) {
      pt = sqrt(pxj[ijet]*pxj[ijet] + pyj[ijet]*pyj[ijet]);
      theta = atan(pt/fabs(pzj[ijet]));
      eta = -log(tan(0.5*theta));
      if(pzj[ijet] < 0.) eta = -eta;
      phi = acos(pxj[ijet]/pt);
      coinc=0;
      for(iclus=0; iclus < 2; iclus++) {
        pt = sqrt(pxcg[iclus]*pxcg[iclus]+pycg[iclus]*pycg[iclus]);
        eta1 = etacg[iclus];
        phi1 = acos(pxcg[iclus]/pt);
        r1 = phi - phi1;
        if(fabs(r1-6.282) < fabs(r1)) r1 = r1-6.282;
        if(fabs(r1+6.282) < fabs(r1)) r1 = r1+6.282;
        r = eta - eta1;
        double d = sqrt(r1*r1 + r*r);
        if(d < 0.5)coinc=1;
      }
      if(!coinc) {
        pxjg[njetg] = pxj[ijet]; pyjg[njetg] = pyj[ijet];
        pzjg[njetg] = pzj[ijet]; ejg[njetg] = ej[ijet];
        etajg[njetg] = eta;
        njetg++;
      }
    }

    if(njetg + nclusb < 1) continue;

    if(nclusb) {                                // add bad clusters to jets:
      for(iclus=0; iclus < nclusb; iclus++) {
        pt = sqrt(pxcb[iclus]*pxcb[iclus]+pycb[iclus]*pycb[iclus]);
        phi = acos(pxcb[iclus]/pt);
        coinc = 0;
        for(ijet=0; ijet < njetg; ijet++) {
          pt = sqrt(pxjg[ijet]*pxjg[ijet] + pyjg[ijet]*pyjg[ijet]);
          phi1 = acos(pxjg[ijet]/pt);
          r1 = phi - phi1;
          if(fabs(r1-6.282) < fabs(r1)) r1 = r1-6.282;
          if(fabs(r1+6.282) < fabs(r1)) r1 = r1+6.282;
          r = etacb[iclus] - etajg[ijet];
          double d = sqrt(r1*r1 + r*r);
          if(d < 0.5)coinc=1;
        }
        if(!coinc) {
          pxjg[njetg] = pxcb[iclus]; pyjg[njetg] = pycb[iclus];
          pzjg[njetg] = pzcb[iclus]; ejg[njetg] = ecb[iclus];
          etajg[njetg] = etacb[iclus];
          njetg++;
        }
      }
    }

    if(nclusg > 2) {                              // add clusters > 2 to jets:
      for(iclus=2; iclus < nclusg; iclus++) {
        pt = sqrt(pxcg[iclus]*pxcg[iclus]+pycg[iclus]*pycg[iclus]);
        phi = acos(pxcg[iclus]/pt);
        coinc = 0;
        for(ijet=0; ijet < njetg; ijet++) {
          pt = sqrt(pxjg[ijet]*pxjg[ijet] + pyjg[ijet]*pyjg[ijet]);
          phi1 = acos(pxjg[ijet]/pt);
          r1 = phi - phi1;
          if(fabs(r1-6.282) < fabs(r1)) r1 = r1-6.282;
          if(fabs(r1+6.282) < fabs(r1)) r1 = r1+6.282;
          r = etacg[iclus] - etajg[ijet];
          double d = sqrt(r1*r1 + r*r);
          if(d < 0.5)coinc=1;
        }
        if(!coinc) {
          pxjg[njetg] = pxcg[iclus]; pyjg[njetg] = pycg[iclus];
          pzjg[njetg] = pzcg[iclus]; ejg[njetg] = ecg[iclus];
          etajg[njetg] = etacg[iclus];
          njetg++;
        }
      }
    }

    pt = sqrt(pxcg[0]*pxcg[0]+pycg[0]*pycg[0]);
    phi = acos(pxcg[0]/pt);
    int iclosej = 0;
    for(ijet=0; ijet < njetg; ijet++) {
      pt = sqrt(pxjg[ijet]*pxjg[ijet] + pyjg[ijet]*pyjg[ijet]);
      phi1 = acos(pxjg[ijet]/pt);
      r1 = phi - phi1;
      if(fabs(r1-6.282) < fabs(r1)) r1 = r1-6.282;
      if(fabs(r1+6.282) < fabs(r1)) r1 = r1+6.282;
      r = etacg[0] - etajg[ijet];
      double d = sqrt(r1*r1 + r*r);
      if(d < 1.) iclosej=1;
    }
    int icloseclus = 0;
    pt = sqrt(pxcg[1]*pxcg[1]+pycg[1]*pycg[1]);
    phi1 = acos(pxcg[1]/pt);
    r1 = phi - phi1;
    if(fabs(r1-6.282) < fabs(r1)) r1 = r1-6.282;
    if(fabs(r1+6.282) < fabs(r1)) r1 = r1+6.282;
    r = etacg[0] - etacg[1];
    double d = sqrt(r1*r1 + r*r);
    if(d < 1.) icloseclus=1;
    if(icloseclus) iclosej=1;

    pt = sqrt(pxcg[0]*pxcg[0]+pycg[0]*pycg[0]);
    double w = fakerate(etacg[0], pt, iclosej);
    double w1 = w;
    double w2 = 0.;
    double w3 = 0.;
    double w4 = 0.;

    pt = sqrt(pxcg[1]*pxcg[1]+pycg[1]*pycg[1]);
    phi = acos(pxcg[1]/pt);
    iclosej = 0;
    for(ijet=0; ijet < njetg; ijet++) {
      pt = sqrt(pxjg[ijet]*pxjg[ijet] + pyjg[ijet]*pyjg[ijet]);
      phi1 = acos(pxjg[ijet]/pt);
      r1 = phi - phi1;
      if(fabs(r1-6.282) < fabs(r1)) r1 = r1-6.282;
      if(fabs(r1+6.282) < fabs(r1)) r1 = r1+6.282;
      r = etacg[1] - etajg[ijet];
      double d = sqrt(r1*r1 + r*r);
      if(d < 1.) iclosej=1;
    }
    if(icloseclus) iclosej=1;
    pt = sqrt(pxcg[1]*pxcg[1]+pycg[1]*pycg[1]);
    w2 = fakerate(etacg[1], pt, iclosej);
//    if(nclusg > 2)
//      w3 = fakerate(etacg[2], sqrt(pxcg[2]*pxcg[2]+pycg[2]*pycg[2]));
//    if(nclusg > 3)
//      w4 = fakerate(etacg[3], sqrt(pxcg[3]*pxcg[3]+pycg[3]*pycg[3]));

    TLorentzVector p1(pxcg[0], pycg[0], pzcg[0], ecg[0]);
    TLorentzVector p2(pxcg[1], pycg[1], pzcg[1], ecg[1]);
    double xmcc = (p1 + p2).M();
    int iokmass = 1;
    if(fabs(xmcc-90.) < 10.) iokmass = 0;
    if(xmcc < 12.) iokmass = 0;
    
    if(!iokmass) continue;
    if(njetg + nclusg < 3) continue;
    //if(nclusg > 2) cout << "more than 2 good clus" << endl;

    ijet1 = 0;
    double ptmax = 0.;
    for(ijet=0; ijet < njetg; ijet++) {
      pt = sqrt(pxjg[ijet]*pxjg[ijet] + pyjg[ijet]*pyjg[ijet]);
      if(pt > ptmax) {ptmax = pt; ijet1 = ijet;}
    }
    ijet2 = 0;
    ptmax = 0.;
    for(ijet=0; ijet < njetg; ijet++) {
      pt = sqrt(pxjg[ijet]*pxjg[ijet] + pyjg[ijet]*pyjg[ijet]);
      if(ijet != ijet1 && pt > ptmax) {ptmax = pt; ijet2 = ijet;}
    }

    if(neleg && njetg && (nclusg >= 2)) {
      necj++;
      pt = sqrt(pxe[0]*pxe[0] + pye[0]*pye[0]);
      theta = atan(pt/fabs(pze[0]));
      eta = -log(tan(0.5*theta));
      if(pze[0] < 0.) eta = -eta;
      if(fabs(eta) < 1.44) PtBarrel->Fill(sqrt(pxe[0]*pxe[0]+pye[0]*pye[0]));
      if(fabs(eta) >= 1.44 && fabs(eta) < 2.)
        PtEndcap->Fill(sqrt(pxe[0]*pxe[0]+pye[0]*pye[0]));
      if(fabs(eta) >= 2.) 
        PtEndcap2->Fill(sqrt(pxe[0]*pxe[0]+pye[0]*pye[0]));
    }
    if(njetg && (nclusg >= 2)) {
      necjpred += (w1+w2+w3+w4);
      if(fabs(etacg[0]) < 1.44)
        PtBarrelPred->Fill(sqrt(pxcg[0]*pxcg[0]+pycg[0]*pycg[0]), w1);
      if(fabs(etacg[0]) >= 1.44 && fabs(etacg[0]) < 2.)
        PtEndcapPred->Fill(sqrt(pxcg[0]*pxcg[0]+pycg[0]*pycg[0]), w1);
      if(fabs(etacg[0]) >= 2.)
        PtEndcap2Pred->Fill(sqrt(pxcg[0]*pxcg[0]+pycg[0]*pycg[0]), w1);
      if(w2 > 0.) {
        if(fabs(etacg[1]) < 1.44)
          PtBarrelPred->Fill(sqrt(pxcg[1]*pxcg[1]+pycg[1]*pycg[1]), w2);
        if(fabs(etacg[1]) >= 1.44 && fabs(etacg[1]) < 2.)
          PtEndcapPred->Fill(sqrt(pxcg[1]*pxcg[1]+pycg[1]*pycg[1]), w2);
        if(fabs(etacg[1]) >= 2.)
          PtEndcap2Pred->Fill(sqrt(pxcg[1]*pxcg[1]+pycg[1]*pycg[1]), w2);
      }
      if(w3 > 0.) {
        if(fabs(etacg[2]) < 1.44)
          PtBarrelPred->Fill(sqrt(pxcg[2]*pxcg[2]+pycg[2]*pycg[2]), w3);
        if(fabs(etacg[2]) >= 1.44 && abs(etacg[1]) < 2.)
          PtEndcapPred->Fill(sqrt(pxcg[2]*pxcg[2]+pycg[2]*pycg[2]), w3);
        if(fabs(etacg[2]) >= 2.)
          PtEndcap2Pred->Fill(sqrt(pxcg[2]*pxcg[2]+pycg[2]*pycg[2]), w3);
      }
      if(w4 > 0.) {
        if(fabs(etacg[3]) < 1.56)
          PtBarrelPred->Fill(sqrt(pxcg[3]*pxcg[3]+pycg[3]*pycg[3]), w4);
        if(fabs(etacg[2]) >= 1.44 && abs(etacg[1]) < 2.)
          PtEndcapPred->Fill(sqrt(pxcg[3]*pxcg[3]+pycg[3]*pycg[3]), w4);
        if(fabs(etacg[3]) >= 2.)
          PtEndcap2Pred->Fill(sqrt(pxcg[3]*pxcg[3]+pycg[3]*pycg[3]), w4);
      }
    }

  }
  cout << "closing file " << filenames[ifile] << endl;
  fin.close();
  }

  cout << "N ecj = " << necj << "  N ecj predicted = " << necjpred << endl;

  hOutputFile->Write();
  hOutputFile->Close();

}
