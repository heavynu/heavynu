import FWCore.ParameterSet.Config as cms

process = cms.Process("RECO")

process.load("Configuration/StandardSequences/Services_cff")
process.load("FWCore/MessageService/MessageLogger_cfi")

process.source = cms.Source("PoolSource",
  fileNames = cms.untracked.vstring('file:hlt.root')
)

process.maxEvents = cms.untracked.PSet(
  input = cms.untracked.int32(-1)
)

# Summer11 PU_S4 pileup:
# (see more explanations in file HeavyNuGenHLT_cfg.py)
#process.load("SimGeneral/MixingModule/mix_2012_Startup_50ns_PoissonOOTPU_cfi")
#process.mix.input.fileNames = cms.untracked.vstring(
#  'rfio:/castor/cern.ch/user/a/akorneev/heavynu/006AF92D-46FA-E011-93B3-0026189438BD.root',
#  'rfio:/castor/cern.ch/user/a/akorneev/heavynu/006BEA54-6EFA-E011-B765-001A92971B74.root'
#)


process.load('Configuration/StandardSequences/GeometryDB_cff')
process.load('Configuration/StandardSequences/MagneticField_38T_cff')
process.load('Configuration/StandardSequences/RawToDigi_cff')
process.load('Configuration/StandardSequences/Reconstruction_cff')
process.load('Configuration/StandardSequences/EndOfProcess_cff')

# Conditions data for calibration and alignment
process.load('Configuration/StandardSequences/FrontierConditions_GlobalTag_cff')
from Configuration.AlCa.GlobalTag import GlobalTag
process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:mc', '')
print "global tag  = ", process.GlobalTag.globaltag

process.load('Configuration/EventContent/EventContent_cff')
process.output = cms.OutputModule("PoolOutputModule",
  process.RECOSIMEventContent,
  fileName = cms.untracked.string('reco.root')
)

# Path and EndPath definitions
process.raw2digi_step       = cms.Path(process.RawToDigi)
process.reconstruction_step = cms.Path(process.reconstruction)
process.endjob_step         = cms.Path(process.endOfProcess)
process.out_step            = cms.EndPath(process.output)

# Schedule definition
process.schedule = cms.Schedule(process.raw2digi_step,
                                process.reconstruction_step,
                                process.endjob_step,
                                process.out_step)
