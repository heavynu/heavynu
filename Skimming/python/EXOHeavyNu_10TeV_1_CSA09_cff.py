import FWCore.ParameterSet.Config as cms

from HLTrigger.HLTfilters.hltHighLevel_cfi import *

#Define the HLT path to be used to define HeavyNu Electrons candidates
exoticaHeavyNuElectronHLT = hltHighLevel.clone()
exoticaHeavyNuElectronHLT.HLTPaths =['HLT_Ele15_LW_L1R']#,'HLT_Ele15_SC10_LW_L1R']
exoticaHeavyNuElectronHLT.TriggerResultsTag = cms.InputTag("TriggerResults","","HLT8E29")

#Define the HLT path to be used to define HeavyNu Muons candidates
exoticaHeavyNuMuonHLT = hltHighLevel.clone()
exoticaHeavyNuMuonHLT.HLTPaths =['HLT_Mu9']
exoticaHeavyNuMuonHLT.TriggerResultsTag = cms.InputTag("TriggerResults","","HLT8E29")

#Define the HLT path to be used to define HeavyNu Electron+Muon candidates
exoticaHeavyNuElectronMuonHLT = hltHighLevel.clone()
exoticaHeavyNuElectronMuonHLT.HLTPaths =['HLT_Ele10_LW_L1R','HLT_Mu9']
exoticaHeavyNuElectronMuonHLT.TriggerResultsTag = cms.InputTag("TriggerResults","","HLT8E29")

from HLTrigger.HLTfilters.hltSummaryFilter_cfi import *

#Define the HLT quality cut for electrons selection 
exoticaHLTDiElectronFilter = hltSummaryFilter.clone(
    summary = cms.InputTag("hltTriggerSummaryAOD","","HLT8E29"), # trigger summary
    member  = cms.InputTag("hltL1NonIsoHLTNonIsoSingleElectronLWEt10EleIdDphiFilter","","HLT8E29"),
    cut     = cms.string("pt>20"),         # cut on trigger object
    minN    = cms.int32(2)                  # min. number of passing objects needed
)
   
exoticaHLTElectronHighThrFilter = hltSummaryFilter.clone(
    summary = cms.InputTag("hltTriggerSummaryAOD","","HLT8E29"), # trigger summary
    member  = cms.InputTag("hltL1NonIsoHLTNonIsoSingleElectronLWEt10EleIdDphiFilter","","HLT8E29"),
    cut     = cms.string("pt>50"),         # cut on trigger object
    minN    = cms.int32(1)                  # min. number of passing objects needed
)

exoticaHLTElectronFilter = hltSummaryFilter.clone(
    summary = cms.InputTag("hltTriggerSummaryAOD","","HLT8E29"), # trigger summary
    member  = cms.InputTag("hltL1NonIsoHLTNonIsoSingleElectronLWEt10EleIdDphiFilter","","HLT8E29"),
    cut     = cms.string("pt>20"),         # cut on trigger object
    minN    = cms.int32(1)                  # min. number of passing objects needed
)

#Define the HLT quality cut for muons selection 
exoticaHLTDiMuonFilter = hltSummaryFilter.clone(
    summary = cms.InputTag("hltTriggerSummaryAOD","","HLT8E29"), # trigger summary
    member  = cms.InputTag("hltL3MuonCandidates","","HLT8E29"),  # filter or collection
    cut     = cms.string("pt>20"),         # cut on trigger object
    minN    = cms.int32(2)                  # min. number of passing objects needed
)
   
exoticaHLTMuonHighThrFilter = hltSummaryFilter.clone(
    summary = cms.InputTag("hltTriggerSummaryAOD","","HLT8E29"), # trigger summary
    member  = cms.InputTag("hltL3MuonCandidates","","HLT8E29"),  # filter or collection
    cut     = cms.string("pt>50"),         # cut on trigger object
    minN    = cms.int32(1)                  # min. number of passing objects needed
)

exoticaHLTMuonFilter = hltSummaryFilter.clone(
    summary = cms.InputTag("hltTriggerSummaryAOD","","HLT8E29"), # trigger summary
    member  = cms.InputTag("hltL3MuonCandidates","","HLT8E29"),  # filter or collection
    cut     = cms.string("pt>20"),         # cut on trigger object
    minN    = cms.int32(1)                  # min. number of passing objects needed
)

#Define the HLT quality cut for jet selection 
exoticaHLTSingleJetFilter = hltSummaryFilter.clone(
    summary = cms.InputTag("hltTriggerSummaryAOD","","HLT8E29"),     # trigger summary
    member  = cms.InputTag("hltMCJetCorJetIcone5HF07","","HLT8E29"), # filter or collection
    cut     = cms.string("pt>12"),         # cut on trigger object
    minN    = cms.int32(1)                  # min. number of passing objects needed
)


#Define the Reco quality cut for electrons
exoticaRecoDiElectronFilter = cms.EDFilter("PtMinGsfElectronCountFilter",
    src       = cms.InputTag("gsfElectrons"),
    ptMin     = cms.double(20.0),
    minNumber = cms.uint32(2)							   
)
exoticaRecoElectronHighThrFilter = cms.EDFilter("PtMinGsfElectronCountFilter",
    src       = cms.InputTag("gsfElectrons"),
    ptMin     = cms.double(50.0),
    minNumber = cms.uint32(1)									   
)
exoticaRecoElectronFilter = cms.EDFilter("PtMinGsfElectronCountFilter",
    src       = cms.InputTag("gsfElectrons"),
    ptMin     = cms.double(20.0),
    minNumber = cms.uint32(1)									   
)

#Define the Reco quality cut for muons
exoticaRecoDiMuonFilter = cms.EDFilter("PtMinMuonCountFilter",
    src       = cms.InputTag("muons"),
    ptMin     = cms.double(20.0),
    minNumber = cms.uint32(2)									   
)
exoticaRecoMuonHighThrFilter = cms.EDFilter("PtMinMuonCountFilter",
    src       = cms.InputTag("muons"),
    ptMin     = cms.double(50.0),
    minNumber = cms.uint32(1)									   
)
exoticaRecoMuonFilter = cms.EDFilter("PtMinMuonCountFilter",
    src       = cms.InputTag("muons"),
    ptMin     = cms.double(20.0),
    minNumber = cms.uint32(1)									   
)

#Define the Reco quality cut for jet
exoticaRecoSingleJetFilter = cms.EDFilter("CaloJetSelector",
    src       = cms.InputTag("antikt5CaloJets"),
    cut       = cms.string("pt>12"),
    filter    = cms.bool(True)									   
)


#Define group sequence, using HLT quality cut. 
exoticaHeavyNuElectronHLTQualitySeq = cms.Sequence(
    exoticaHeavyNuElectronHLT+
    exoticaHLTDiElectronFilter+exoticaHLTElectronHighThrFilter+
    exoticaHLTSingleJetFilter
)
exoticaHeavyNuMuonHLTQualitySeq = cms.Sequence(
    exoticaHeavyNuMuonHLT+
    exoticaHLTDiMuonFilter+exoticaHLTMuonHighThrFilter+
    exoticaHLTSingleJetFilter
)
#Define group sequence, using HLT quality cut: Ele+Mu(HighThr)
exoticaHeavyNuMuonElectronHLTQualitySeq = cms.Sequence(
    exoticaHeavyNuElectronMuonHLT+
    exoticaHLTElectronFilter+exoticaHLTMuonHighThrFilter+
    exoticaHLTSingleJetFilter
)
#Define group sequence, using HLT quality cut: Ele(HighThr)+Mu
exoticaHeavyNuElectronMuonHLTQualitySeq = cms.Sequence(
    exoticaHeavyNuElectronMuonHLT+
    exoticaHLTElectronHighThrFilter+exoticaHLTMuonFilter+
    exoticaHLTSingleJetFilter
)


#Define group sequence, using Reco quality cut. 
exoticaHeavyNuElectronRecoQualitySeq = cms.Sequence(
    exoticaHeavyNuElectronHLT+
    exoticaRecoDiElectronFilter+exoticaRecoElectronHighThrFilter+
    exoticaRecoSingleJetFilter
)
exoticaHeavyNuMuonRecoQualitySeq = cms.Sequence(
    exoticaHeavyNuMuonHLT+
    exoticaRecoDiMuonFilter+exoticaRecoMuonHighThrFilter+
    exoticaRecoSingleJetFilter
)
#Define group sequence, using Reco quality cut: Ele+Mu(HighThr) 
exoticaHeavyNuMuonElectronRecoQualitySeq = cms.Sequence(
    exoticaHeavyNuElectronMuonHLT+
    exoticaRecoElectronFilter+exoticaRecoMuonHighThrFilter+
    exoticaRecoSingleJetFilter
)
#Define group sequence, using Reco quality cut: Ele(HighThr)+Mu 
exoticaHeavyNuElectronMuonRecoQualitySeq = cms.Sequence(
    exoticaHeavyNuElectronMuonHLT+
    exoticaRecoElectronHighThrFilter+exoticaRecoMuonFilter+
    exoticaRecoSingleJetFilter
)

