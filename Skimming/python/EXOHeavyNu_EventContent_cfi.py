import FWCore.ParameterSet.Config as cms
from Configuration.EventContent.EventContent_cff import *


exoticaHeavyNuOutputModule = cms.OutputModule("PoolOutputModule",
    outputCommands = cms.untracked.vstring(),
    SelectEvents = cms.untracked.PSet(
       SelectEvents = cms.vstring("exoticaHeavyNuElectronSkimPath",        # e-e-jet
                                  "exoticaHeavyNuMuonSkimPath",            # mu-mu-jet
                                  "exoticaHeavyNuElectronMuonSkimPath",    # e-mu-jet
                                  "exoticaHeavyNuMuonElectronSkimPath")    # mu-e-jet
      ),
    dataset = cms.untracked.PSet(
        filterName = cms.untracked.string('EXOHeavyNu'),
        dataTier = cms.untracked.string('EXOGroup')
    ),
    fileName = cms.untracked.string('exoticaheavynutest.root') # can be modified later 
  )


#default output contentRECOSIMEventContent
exoticaHeavyNuOutputModule.outputCommands.extend(RECOSIMEventContent.outputCommands)

#add specific content you need. 
SpecifiedEvenetContent = cms.PSet(
    outputCommands = cms.untracked.vstring(
      "keep *_exotica*_*_*",
      )
    )
exoticaHeavyNuOutputModule.outputCommands.extend(SpecifiedEvenetContent.outputCommands)

