import FWCore.ParameterSet.Config as cms

process = cms.Process("EXOHeavyNuSkim")
process.options = cms.untracked.PSet(
    wantSummary = cms.untracked.bool(True)
)
# ----- number of Events to be skimmed ------------------
process.maxEvents = cms.untracked.PSet(
    input = cms.untracked.int32(-1)
)

process.load("FWCore/MessageService/MessageLogger_cfi")
# supress message "Begin processing the ... record" to 1 time from 10
process.MessageLogger.cerr.FwkReport.reportEvery = 10

# ----- Names  of the files we want to skim -----------------
#process.source = cms.Source("PoolSource",
#  fileNames = cms.untracked.vstring(
#    #"file:/moscow21/heavynu/31x/WR1500_nuRe600/49208454/reco.root"
#    #"file:/moscow21/heavynu/31x/WR1500_nuRmu600/49208494/reco.root"
#    "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49653624/reco.root"
#  ),
#  duplicateCheckMode = cms.untracked.string('noDuplicateCheck')
#)
process.load('InputFiles_cfi') # load the file names

# ---- load the EventContent and Skim cff/i files for EXOHeavyNu skim -----------
#process.load('SUSYBSMAnalysis.Skimming.EXOHeavyNu_EventContent_cfi')
#process.load('SUSYBSMAnalysis.Skimming.EXOHeavyNu_cff')
process.load('heavynu.Skimming.EXOHeavyNu_EventContent_cfi')
process.load('heavynu.Skimming.EXOHeavyNu_10TeV_1_cff')


# -------------- output file name ------------------- 
process.exoticaHeavyNuOutputModule.fileName = cms.untracked.string('HeavyNu_Skim.root')


# -------------- HLTQualitySeq: selection based on HLT calculations -------------------
#process.exoticaHeavyNuElectronSkimPath = cms.Path(process.exoticaHeavyNuElectronHLTQualitySeq)
#process.exoticaHeavyNuMuonSkimPath = cms.Path(process.exoticaHeavyNuMuonHLTQualitySeq)
#process.exoticaHeavyNuElectronMuonSkimPath = cms.Path(process.exoticaHeavyNuElectronMuonHLTQualitySeq)
#process.exoticaHeavyNuMuonElectronSkimPath = cms.Path(process.exoticaHeavyNuMuonElectronHLTQualitySeq)

# -------------- RecoQualitySeq: selection based on Reconstruction --------------------
process.exoticaHeavyNuElectronSkimPath = cms.Path(process.exoticaHeavyNuElectronRecoQualitySeq)
process.exoticaHeavyNuMuonSkimPath = cms.Path(process.exoticaHeavyNuMuonRecoQualitySeq)
process.exoticaHeavyNuElectronMuonSkimPath = cms.Path(process.exoticaHeavyNuElectronMuonRecoQualitySeq)
process.exoticaHeavyNuMuonElectronSkimPath = cms.Path(process.exoticaHeavyNuMuonElectronRecoQualitySeq)

# -------------- the endPath definition -----------------------
process.endPath = cms.EndPath(process.exoticaHeavyNuOutputModule)
