import FWCore.ParameterSet.Config as cms

# ----- Names  of the files we want to skim -----------------
source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(
     "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49653624/reco.root",
     "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49653625/reco.root",
#     "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49653626/reco.root",
#     "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49653627/reco.root",
#     "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49653628/reco.root",
#     "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49653630/reco.root",
#     "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49653632/reco.root",
#     "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49653634/reco.root",
#     "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49653636/reco.root",
#     "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49653638/reco.root",
#     "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49653639/reco.root",
#     "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49653641/reco.root",
#     "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49653642/reco.root",
#     "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49653643/reco.root",
#     "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49653646/reco.root",
#     "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49926384/reco.root",
#     "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49926385/reco.root",
#     "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49926386/reco.root",
#     "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49926387/reco.root",
#     "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49926388/reco.root",
#     "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49926390/reco.root",
#     "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49926391/reco.root",
#     "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49926392/reco.root",
#     "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49926393/reco.root",
#     "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49926394/reco.root",
#     "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49926395/reco.root",
#     "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49926396/reco.root",
#     "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49926397/reco.root",
#     "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49926398/reco.root",
     "file:/moscow21/toropin/CMSSW/heavynu/31X-TTbar/49926399/reco.root"
    ),

    duplicateCheckMode = cms.untracked.string('noDuplicateCheck')
)
