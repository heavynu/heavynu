
   HeavyNu Skimming package.
   -------------------------

1. Working software CMSSW_3_1_5

2. Main cfg file EXOHeavyNu_10TeV_1_cfg.py

3. Works with HLT Trigger table HLT8E29 and 10TeV

4. List of input files for skimming in InputFiles_cfi.py

5. Start skimming:
    cmsenv
    cmdRun EXOHeavyNu_10TeV_1_cfg.py

6. Output file HeavyNu_Skim.root

7. To produce HeavyNu Trees:
    cmsenv
    cmsRun dump_cfg.py

8. skimcheck.cc - simple program to test skims' output
   (simple program to look for any information in heavynu.root)
    cmsenv
    make
    ./skimcheck
    gv skimcheck.ps
