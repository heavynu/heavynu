import FWCore.ParameterSet.Config as cms
import sys, os

process = cms.Process("dump")

# Message logger
process.load("FWCore/MessageService/MessageLogger_cfi")
# supress message "Begin processing the ... record" to 1 time from 100
process.MessageLogger.cerr.FwkReport.reportEvery = 100

process.options = cms.untracked.PSet(
  wantSummary = cms.untracked.bool(False)
)

# Input file
process.source = cms.Source("PoolSource",
  #fileNames = cms.untracked.vstring('file:reco.root'),
  fileNames = cms.untracked.vstring(
    #'file:TTbar_Skim.root'
    'file:HeavyNu_Skim.root'
    #'file:EXOEleOct09.root'
    #'file:EXODiLeptonOct09.root'
    ),
  duplicateCheckMode = cms.untracked.string('noDuplicateCheck')
)

## list recursively all 'reco.root' if specified first parameter of dump_cfg.py
#if len(sys.argv) > 2:
#  rootdir = sys.argv[2] # path to directory where 'reco.root' files situated
#  process.source.fileNames = []
#  
#  # recurse through directories to find all reco.root
#  for root, dirs, files in os.walk(rootdir):
#    for name in files:
#      if name == "reco.root":
#        process.source.fileNames.append("file:" + os.path.join(root, name))
#  
#  if len(process.source.fileNames) == 0:
#    print "ERROR: no reco.root files found in " + rootdir
#    sys.exit(1)
#
#print len(process.source.fileNames), "files will be processed:"
#for name in process.source.fileNames:
#  print "  " + name
#

process.maxEvents = cms.untracked.PSet(
  input = cms.untracked.int32(-1)
)

# === PAT ===
# this is necessary for PAT to run:
process.load('Configuration/StandardSequences/GeometryIdeal_cff')
process.load('Configuration/StandardSequences/FrontierConditions_GlobalTag_cff')
#process.GlobalTag.globaltag = 'MC_31X_V5::All'
process.GlobalTag.globaltag = 'STARTUP31X_V4::All'

process.load('Configuration/StandardSequences/MagneticField_38T_cff')

# PAT Layer 0+1
process.load("PhysicsTools/PatAlgos/patSequences_cff")

# cleaning
# 1st step
# select muons which pass relIso, pt and MuonID cut
process.cleanLayer1Muons.preselection      = "(0.75*ecalIso + 0.75*hcalIso + trackIso)/pt < 0.2"
process.cleanLayer1Muons.checkOverlaps     = cms.PSet()
process.cleanLayer1Muons.finalCut          = "(pt > 3) && isGood('TMLastStationTight')"

# 2nd step
# select electrons which pass relIso, pt and ElectronID cut
process.cleanLayer1Electrons.preselection  = "(0.75*ecalIso + 0.75*hcalIso + trackIso)/pt < 0.2"
process.cleanLayer1Electrons.checkOverlaps = cms.PSet()
process.cleanLayer1Electrons.finalCut      = "(pt > 3) && (electronID('eidRobustLoose') > 0)"

# 3rd step
# default photon cleaning used:
# throw away photons which share the same SuperCluster with cleanLayer1Electrons
# for more info follow to "PhysicsTools/PatAlgos/python/cleaningLayer1/photonCleaner_cfi.py"

# 4th step
# select jets which do not intersect with cleanLayer1Muons, cleanLayer1Electrons (in cone dR < 0.5) and pass pt cut
process.cleanLayer1Jets.checkOverlaps.muons.requireNoOverlaps = True
process.cleanLayer1Jets.checkOverlaps.electrons.requireNoOverlaps = True
process.cleanLayer1Jets.finalCut           = "pt > 20"

# === PAT dump module ===
# dump module
process.patdump = cms.EDAnalyzer('PATDump',
  # input collections
  electronTag = cms.InputTag("cleanLayer1Electrons"),
  muonTag     = cms.InputTag("cleanLayer1Muons"),
  #tauTag      = cms.InputTag("selectedLayer1Taus"),
  jetTag      = cms.InputTag("cleanLayer1Jets"),
  photonTag   = cms.InputTag("cleanLayer1Photons"),
  metTag      = cms.InputTag("layer1METs"),
  triggerResults = cms.InputTag("TriggerResults::HLT8E29"),
  #triggerResults = cms.InputTag("TriggerResults::HLT"),
  
  cfgWeight = cms.double(1.0),
  cfgPid    = cms.int32(-1),
  
  jetThreshold    = cms.double(20),
  photonThreshold = cms.double(10),
  leptonThreshold = cms.double(20),
  
  minLeptons = cms.int32(2),
  minJets    = cms.int32(1),
  
  #debug = cms.bool(True)
  debug = cms.bool(False)
)

# output file
process.TFileService = cms.Service("TFileService",
  fileName = cms.string('heavynu.root')
)

# Define the paths
process.p = cms.Path(
  process.patDefaultSequence +
  process.patdump
)
