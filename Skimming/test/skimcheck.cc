#include <iostream>
#include <fstream>
#include <string>
#include <stdio.h>
#include <vector>
#include <map>

#include <TH1F.h>
#include <TH2F.h>
#include <TF1.h>
#include <TFile.h>
#include <TTree.h>
#include <TVector2.h>
#include <TVector3.h>
#include <TLorentzVector.h>
#include "TCanvas.h"

#include "../../PATDump/src/format.h"

using namespace std;

int main (Int_t  argc, Char_t **argv) 
{
  char* fname = "heavynu.root";
  if (argc > 1) fname = argv[1];

  cout << " filename = " << fname << endl;

  TheEvent e(fname);
  
  const int nEvents = e.totalEvents();
  cout << "Total events = " << nEvents << endl;
  
  e.readEvent(0);
  int nBits = (*e.hltBits).size();

  // histograms
  TH1F hHLT("HLTbits", "HLT efficiency;#bit;%", nBits, 0, nBits);
  TH1F hWR(  "WR", "WR;M, GeV;events/25 GeV", 100, 0, 2500);
  TH1F hNuR("NuR", "NuR;M, GeV;events/25 GeV", 100, 0, 2500);
  TH1F hMll("Mll", "Mll;M, GeV;events/25 GeV", 100, 0, 2500);
  TH1F hMjj("Mjj", "Mjj;M, GeV;events/25 GeV", 100, 0, 2500);
  TH1F hElRes("ElRes", "ElRes", 100, -0.3, 0.3);
  TH1F hElMult("ElMult", "ElMult", 10, 0, 10);
  TH1F hJetMult("JetMult", "JetMult", 20, 0, 20);
  
  bool pass(false);

  for (int i = 0; i < nEvents; ++i) {
    e.readEvent(i);
    if (i == 1) e.print();
    pass = true;

    // check HLT bits 
    for (int j = 0; j < nBits; ++j)
      if ((*e.hltBits)[j]) {
        hHLT.Fill(j);
	if (e.triggerNames[j] == "HLT_Mu9" || 
	    e.triggerNames[j] == "HLT_Ele15_LW_L1R") {
	  pass = false;
	  break;
	}
      }
    if (pass) continue;

    // check Skimming conditions: 
    // 1) Pt1(e) > 20,  Pt2(e) > 50,  Pt(jet) > 12; 
    // 2) Pt1(mu) > 20, Pt2(mu) > 50, Pt(jet) > 12; 
    // 3) Pt1(e) > 50,  Pt2(mu) > 20, Pt(jet) > 12; 
    // 4) Pt1(mu) > 50, Pt2(e) > 20,  Pt(jet) > 12; 
    
    int lowThrEle(0), highThrEle(0);
    pass = true;
    for (unsigned int j = 0; j < e.electrons.size(); ++j) {
      if (e.electrons[j].p4.Pt() > 20.) lowThrEle++;
      if (e.electrons[j].p4.Pt() > 50.) highThrEle++;
    }
    int lowThrMu(0), highThrMu(0);
    for (unsigned int k = 0; k < e.muons.size(); ++k) {
      if (e.muons[k].p4.Pt() > 20.) lowThrMu++;
      if (e.muons[k].p4.Pt() > 50.) highThrMu++;
    }
    if (lowThrMu > 1 && highThrMu > 0) pass = false;
    else if (lowThrEle > 1 && highThrEle > 0) pass = false; 
    else if (lowThrMu > 0 && highThrEle > 0) pass = false; 
    else if (highThrMu > 0 && lowThrEle > 0) pass = false;
    if (pass) continue;

    pass = true;
    for (unsigned int j = 0; j < e.jets.size(); ++j) {
      if (e.jets[j].p4.Pt() > 12.) {
	pass = false;
	break;
      }
    }
    if (pass) continue; 

    // electron energy resolution
    for (unsigned int j = 0; j < e.electrons.size(); ++j) {
      const Particle& reco = e.electrons[j];

      if (reco.mcId >= 0) {
        const Particle& mc = e.gens[reco.mcId];
        const float r = (reco.p4.E() - mc.p4.E())/mc.p4.E();
        hElRes.Fill(r);
      }
    }
    
    // mass distributions
    if ((e.electrons.size() >= 2) && (e.jets.size() >= 2)) {
      const float WR = (e.electrons[0].p4 + e.electrons[1].p4 +
			e.jets[0].p4 + e.jets[1].p4).M();
      hWR.Fill(WR, e.weight);
      
      const float NuR_l1 = (e.electrons[1].p4 + e.jets[0].p4 + e.jets[1].p4).M();
      hNuR.Fill(NuR_l1, e.weight);
      
      const float Mll = (e.electrons[0].p4 + e.electrons[1].p4).M();
      hMll.Fill(Mll, e.weight);
      
      const float Mjj = (e.jets[0].p4 + e.jets[1].p4).M();
      hMjj.Fill(Mjj, e.weight);
    }
    
    // Multiplicity
    hElMult.Fill(e.electrons.size());
    hJetMult.Fill(e.jets.size());
  }
  
  // print all histograms to .ps
  TCanvas c("skimcheck");
  c.Print("skimcheck.ps[");
  c.Divide(2,2);

  //hWR.Fit("gaus");
  c.cd(1);   hWR.Draw();
  c.cd(2);   hNuR.Draw();
  c.cd(3);   hMll.Draw();
  c.cd(4);   hMjj.Draw(); 
  c.Print("skimcheck.ps");
 
  hHLT.Scale(100.0/nEvents);
  c.cd(1);   hHLT.Draw();
  c.cd(2);   
  c.cd(2);   hElRes.Fit("gaus","R");  
             hElRes.Draw(); 
  c.cd(3);   hElMult.Draw();  
  c.cd(4);   hJetMult.Draw(); 
  c.Print("skimcheck.ps");
  
  c.Print("skimcheck.ps]");

  return 0;
}
