// ROOT
#include "TObject.h"
#include "TCanvas.h"
#include "THStack.h"
#include "TFile.h"
#include "TTree.h"
#include "TCut.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TLorentzVector.h"
#include "TString.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TError.h"

// c++
#include <iostream>
using namespace std;

// histogramming and printing functions
#include "analysis.h"

// function for plotting of control distributions
void control(const char* fname = "heavynu.root",
             const Channel ch = chEE,
             const TCut cut = "weight")
{
  cout << "Preparing control distribution for:\n"
       << "    fname = " << fname << "\n"
       << "  channel = " << ch << "\n"
       << "      cut = " << cut << endl;
    
  
  cout << "Histogramming..." << endl;
  const Model m(ch, cut, fname);
  
  cout << "Printing..." << endl;
  m.print("control");
  
  cout << "Done! See results in control.ps\n"
       << endl;
}

void compos_csa09()
{
  const Channel ch = chEE;
  const TCut cut = "weight*(LL > 200)";
  
  cout << "Histogramming...\n"
       << "  channel = " << ch << "\n"
       << "      cut = " << cut << endl;
  
  vector<string> qcd;
  qcd.push_back("/moscow31/heavynu/31x/QCD_EMEnriched_Pt20to30-SD_Ele15.root");
  qcd.push_back("/moscow31/heavynu/31x/QCD_EMEnriched_Pt30to80-SD_Ele15.root");
  qcd.push_back("/moscow31/heavynu/31x/QCD_EMEnriched_Pt80to170-SD_Ele15.root");
  qcd.push_back("/moscow31/heavynu/31x/QCD_BCtoE_Pt20to30-SD_Ele15.root");
  qcd.push_back("/moscow31/heavynu/31x/QCD_BCtoE_Pt30to80-SD_Ele15.root");
  qcd.push_back("/moscow31/heavynu/31x/QCD_BCtoE_Pt80to170-SD_Ele15.root");
  
  vector<Model> csa09;
  csa09.push_back(Model(ch, cut, "/moscow31/heavynu/22x/WR1500_nuRe600_1e31_pat2.root"));
  csa09.push_back(Model(ch, cut, qcd));
  csa09.push_back(Model(ch, cut, "/moscow31/heavynu/31x/TTbar-SD_Ele15.root"));
  csa09.push_back(Model(ch, cut, "/moscow31/heavynu/31x/Wenu-SD_Ele15.root"));
  csa09.push_back(Model(ch, cut, "/moscow31/heavynu/31x/Zee-SD_Ele15.root"));
  
  cout << "Printing..." << endl;
  print(csa09, "compos1.ps", "wr");
  
  cout << "Done! See results in compos1.ps\n"
       << endl;
  
  // background reduction table
  vector<TCut> cuts;
  cuts.push_back(TCut("initial", "weight"));
  cuts.push_back(TCut("primary", "(nlept >= 2)*(njets >= 2)*(l1.pt > 60 || l2.pt > 60)"));
  cuts.push_back(TCut("Mll cut", "LL > 200"));
  cuts.push_back(TCut("MWR cut", "WR > 500"));
  cuts.push_back(TCut("2D peak", "(1250 < WR)*(WR < 1700)*(480 < L2JJ)*(L2JJ < 710)"));
  
  brt(csa09, cuts, ch);
}

void compos_data()
{
  const Channel ch = chEE;
  const TCut cut = "";
  
  cout << "Histogramming...\n"
       << "  channel = " << ch << "\n"
       << "      cut = " << cut << endl;
  
  vector<Model> data;
  data.push_back(Model(ch, cut, "/moscow31/heavynu/data/132440.root"));
  data.push_back(Model(ch, cut, "/moscow31/heavynu/data/132599.root"));
  data.push_back(Model(ch, cut, "/moscow31/heavynu/data/132601.root"));
  data.push_back(Model(ch, cut, "/moscow31/heavynu/data/132605.root"));
  data.push_back(Model(ch, cut, "/moscow31/heavynu/data/132716.root"));
  
  cout << "Printing..." << endl;
  print(data, "compos1.ps", "wr");
  
  cout << "Done! See results in compos1.ps\n"
       << endl;
  
  // background reduction table
  vector<TCut> cuts;
  cuts.push_back(TCut("initial", "weight"));
  cuts.push_back(TCut("primary", "(nlept >= 2)*(njets >= 2)*(l1.pt > 60 || l2.pt > 60)"));
  cuts.push_back(TCut("Mll cut", "LL > 200"));
  cuts.push_back(TCut("MWR cut", "WR > 500"));
  cuts.push_back(TCut("2D peak", "(1250 < WR)*(WR < 1700)*(480 < L2JJ)*(L2JJ < 710)"));
  
  //brt(data, cuts, ch);
}

void sigbg(const TCut cut = "weight*(LL > 250)")
{
  // signal & background plots
  // ee-channel
  Model sigEE(chEE, cut, "/moscow31/heavynu/22x/WR1200_nuRe500.root");
  TH1* h1 = sigEE.hist["wr"];
  Model bgEE(chEE, cut, "/moscow31/heavynu/22x/TauolaTTbar.root");
  TH1* h2 = bgEE.hist["wr"];
  
  // mumu-channel
  // HistMap sigMuMu = hist("/moscow31/heavynu/22x/WR1200_nuRmu500.root", chMuMu, cut);
  // TH1* h3 = sigMuMu["wr"];
  // HistMap bgMuMu = hist("/moscow31/heavynu/22x/TauolaTTbar.root", chMuMu, cut);
  // TH1* h4 = bgMuMu["wr"];
  
  gStyle->SetOptStat("");
  TCanvas c("sig_bg", "sig_bg");
  c.Print(".ps[");
  
  // c.SetLogy();
  // ee-channel
  // h1->SetLineColor(kRed);
  h1->SetFillColor(kRed);
  h1->SetFillStyle(3005);
  h1->SetLineWidth(2);
  h2->SetLineWidth(2);
  h1->Draw(); h2->Draw("same");
  h1->Fit("gaus", "EMI", "same", 1000, 1400);
  ((TF1*)h1->GetListOfFunctions()->FindObject("gaus"))->SetLineColor(kRed);
  c.Print(".ps");
  // THStack hs1; hs1.Add(&h2); hs1.Add(&h1);
  // hs1.Draw(); c.Print(".ps");
  // mumu-channel
  // h3->SetLineColor(kBlue);
  
  // h3->SetFillColor(kBlue);
  // h3->SetFillStyle(3005);
  // h3->SetLineWidth(2);
  // h4->SetLineWidth(2);
  // h3->Draw(); h4->Draw("same");
  // h3->Fit("gaus", "EMI", "same", 1700, 2200);
  // ((TF1*)h3->GetListOfFunctions()->FindObject("gaus"))->SetLineColor(kBlue);
  // c.Print(".ps");
  
  // THStack hs2; hs2.Add(&h4); hs2.Add(&h3);
  // hs2.Draw(); c.Print(".ps)");
  
  c.Print(".ps]");
}

void bgcompos(const char* bgname, const TCut cut = "weight")
{
  const TCut cutAll = cut;
  const TCut cutWj = cut * "0 <= pid && pid <= 10";
  const TCut cutZj = cut * "11 <= pid && pid <= 21";
  const TCut cutTT = cut * "22 <= pid && pid <= 26";
  
  // fill
  TTree* bg = 0; // = reap(bgname, chEE);
  TH1F histAll("histAll", TString("Background (L = 100 pb^{-1}): ") + bgname + ", cut: " + cutAll.GetTitle() + ";M_{W_{R}}, GeV/c^{2};events/30 GeV", 100, 0, 3000);
  TH1F histWj("histWj", "W+jets;M_{W_{R}}, GeV/c^{2};events/30 GeV", 100, 0, 3000);
  TH1F histZj("histZj", "Z+jets;M_{W_{R}}, GeV/c^{2};events/30 GeV", 100, 0, 3000);
  TH1F histTT("histTT",  "ttbar;M_{W_{R}}, GeV/c^{2};events/30 GeV", 100, 0, 3000);
  bg->Draw("WR>>histAll", cutAll, "nodraw");
  bg->Draw("WR>>histWj", cutWj, "nodraw");
  bg->Draw("WR>>histZj", cutZj, "nodraw");
  bg->Draw("WR>>histTT", cutTT, "nodraw");
  
  // plot
  gStyle->SetOptStat("");
  TCanvas c("bgcompos", "bgcompos");
  histAll.SetLineColor(kBlack); histAll.SetLineWidth(2); histAll.Draw();
  histWj.SetLineColor(kRed);    histWj.SetLineWidth(2);  histWj.Draw("same");
  histZj.SetLineColor(kGreen);  histZj.SetLineWidth(2);  histZj.Draw("same");
  histTT.SetLineColor(kBlue);   histTT.SetLineWidth(2);  histTT.Draw("same");
  TLegend leg(0.7, 0.7, 0.89, 0.89);
  leg.AddEntry(&histAll, "total");
  leg.AddEntry(&histWj);
  leg.AddEntry(&histZj);
  leg.AddEntry(&histTT);
  leg.Draw();
  c.Print(".ps");
  
  delete bg;
}

void bgWj(const char* bgname, const TCut cut = "weight")
{
  const TCut cutAll = cut;
  const TCut cutWj = cut * "0 <= pid && pid <= 10";
  const TCut cutZj = cut * "11 <= pid && pid <= 21";
  const TCut cutTT = cut * "22 <= pid && pid <= 26";
  
  // fill
  TTree* bg = 0; // = reap(bgname, chEE);
  TH1F histAll("histAll", TString("Background (L = 100 pb^{-1}): ") + bgname + ", cut: " + cutAll.GetTitle() + ";M_{W_{R}}, GeV/c^{2};events/30 GeV", 100, 0, 3000);
  TH1F histWj("histWj", "W+jets;M_{W_{R}}, GeV/c^{2};events/30 GeV", 100, 0, 3000);
  TH1F histZj("histZj", "Z+jets;M_{W_{R}}, GeV/c^{2};events/30 GeV", 100, 0, 3000);
  TH1F histTT("histTT",  "ttbar;M_{W_{R}}, GeV/c^{2};events/30 GeV", 100, 0, 3000);
  bg->Draw("WR>>histAll", cutAll, "nodraw");
  bg->Draw("WR>>histWj", cutWj, "nodraw");
  bg->Draw("WR>>histZj", cutZj, "nodraw");
  bg->Draw("WR>>histTT", cutTT, "nodraw");
  
  // plot
  gStyle->SetOptStat("");
  TCanvas c("bgcompos", "bgcompos");
  histAll.SetLineColor(kBlack); histAll.SetLineWidth(2); histAll.Draw();
  histWj.SetLineColor(kRed);    histWj.SetLineWidth(2);  histWj.Draw("same");
  histZj.SetLineColor(kGreen);  histZj.SetLineWidth(2);  histZj.Draw("same");
  histTT.SetLineColor(kBlue);   histTT.SetLineWidth(2);  histTT.Draw("same");
  TLegend leg(0.7, 0.7, 0.89, 0.89);
  leg.AddEntry(&histAll, "total");
  leg.AddEntry(&histWj);
  leg.AddEntry(&histZj);
  leg.AddEntry(&histTT);
  leg.Draw();
  c.Print(".ps");
  
  delete bg;
}

void list(const char* fname = "heavynu.root")
{
  TheEvent e(fname);
  if (!e) return;
  
  // HLT Trigger bits names:
  cout << "HLT Trigger bits names:" << endl;
  for (size_t i = 0; i < e.triggerNames.size(); ++i)
    cout << i << ": " << e.triggerNames[i] << endl;
  
  // events:
  cout << "Total events: " << e.totalEvents() << endl;
  
  for (int i = 0; i < e.totalEvents(); ++i) {
    e.readEvent(i);
    // now we can work with vectors e.electrons[], e.muons[], ...
    
    if ((e.electrons.size() >= 2) && (e.jets.size() >= 2)) {
      const float WR  = (e.electrons[0].p4 + e.electrons[1].p4 + e.jets[0].p4 + e.jets[1].p4).M();
      const float MNu = (e.electrons[1].p4 + e.jets[0].p4 + e.jets[1].p4).M();
      cout << "event "    << e.ievent
           << ": M_WR = " << WR
           << ", MET = "  << e.mets[0].p4.Et()
           << endl;
    }
  }
}
