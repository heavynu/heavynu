// ROOT
#include "TObject.h"
#include "TCanvas.h"
#include "THStack.h"
#include "TFile.h"
#include "TTree.h"
#include "TCut.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TLorentzVector.h"
#include "TString.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TError.h"

// c++
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
using namespace std;

// TheEvent, Particle
#include "PATDump/src/format.h"

// analysis channel
enum Channel {chEE, chMuMu, chEMu};

std::ostream& operator<<(std::ostream& os, const Channel& ch)
{
  switch (ch) {
    case chEE  : os << "e-e";   break;
    case chMuMu: os << "mu-mu"; break;
    case chEMu : os << "e-mu";  break;
    default:     os << "unknown";
  }
  
  return os;
}

struct MiniParticle {
  float pt, iso, chi2, fem;
  int charge, nhits;
  
  MiniParticle& operator=(const Particle& p)
  {
    pt = p.p4.pt();
    iso = p.iso();
    chi2 = p.chi2;
    fem = p.Fem;
    charge = p.charge;
    nhits = p.validHits;
    return *this;
  }
};

typedef map<string, TH1*> HistMap;

// purpose of this class is to hold

class Model {
public:
  //   [ch]    - analysis channel
  Model(const Channel ch,
        const TCut ct,
        const string fname)
  : fnames(vector<string>(1, fname))
  {
    reApplyCuts(ch, ct);
  }
  
  Model(const Channel ch,
        const TCut ct,
        const vector<string> fns)
  : fnames(fns)
  {
    reApplyCuts(ch, ct);
  }
  
  Channel channel;
  TCut cut;
  Sample sample;
  HistMap hist;
  vector<string> fnames;
  
  void print(const char* fname) const;
  
  void reApplyCuts(const Channel ch, const TCut ct);
  
private:
  // This function read events from our own .root files
  // and produces TTree with variables which we will use
  // to draw histograms
  //TTree* reap(TheEvent*);
  
  HistMap hist1(TheEvent&);
  
  HistMap hist2(TheEvent&);
  
  HistMap doHist(const string fname);
  
  HistMap summHist();
};

HistMap Model::hist1(TheEvent& e)
{
  if (channel != chEE) {
    cerr << "ERROR: only chEE implemented now" << endl;
    return HistMap();
  }
  
  // output tree
  TTree slice("slice", "slice");
  slice.SetDirectory(0); // in-memory only (not connected with current open file)
  float WR, L1JJ, L2JJ, JJ, LL; // invariant masses
  float MET;
  int nlept, njets, nele, nmu;
  MiniParticle l1, l2, j1, j2;
  
  slice.Branch("hlt", &e.hltBits);
  slice.Branch("weight",  &e.weight, "weight/F");
  slice.Branch("pid",     &e.pid, "pid/I");
  slice.Branch("WR", &WR, "WR/F");
  slice.Branch("L1JJ", &L1JJ, "L1JJ/F");
  slice.Branch("L2JJ", &L2JJ, "L2JJ/F");
  slice.Branch("JJ", &JJ, "JJ/F");
  slice.Branch("LL", &LL, "LL/F");
  slice.Branch("MET", &MET, "MET/F");
  slice.Branch("nele", &nele, "nele/I");
  slice.Branch("nmu", &nmu, "nmu/I");
  slice.Branch("nlept", &nlept, "nlept/I");
  slice.Branch("njets", &njets, "njets/I");
  slice.Branch("l1", &l1, "pt/F:iso/F:chi2/F:fem/F:charge/I:nhits/I");
  slice.Branch("l2", &l2, "pt/F:iso/F:chi2/F:fem/F:charge/I:nhits/I");
  slice.Branch("j1", &j1, "pt/F:iso/F:chi2/F:fem/F:charge/I:nhits/I");
  slice.Branch("j2", &j2, "pt/F:iso/F:chi2/F:fem/F:charge/I:nhits/I");
  
  const int nEvents = e.totalEvents();
  
  for (int i = 0; i < nEvents; ++i) {
    e.readEvent(i);
    
    if ((e.electrons.size() >= 2) && (e.jets.size() >= 2)) {
      WR   = (e.electrons[0].p4 + e.electrons[1].p4 + e.jets[0].p4 + e.jets[1].p4).M();
      L1JJ = (e.electrons[0].p4 + e.jets[0].p4 + e.jets[1].p4).M();
      L2JJ = (e.electrons[1].p4 + e.jets[0].p4 + e.jets[1].p4).M();
      JJ   = (e.jets[0].p4 + e.jets[1].p4).M();
      LL   = (e.electrons[0].p4 + e.electrons[1].p4).M();
      MET  = e.mets[0].p4.Et();
      
      nele = e.electrons.size();
      nmu  = e.muons.size();
      nlept = nele + nmu;
      njets = e.electrons.size();
      
      l1 = e.electrons[0];
      l2 = e.electrons[1];
      j1 = e.jets[0];
      j2 = e.jets[1];
      
      slice.Fill();
    }
  }
  
  // book histograms
  HistMap hm;
  //hm["HLTbits"] = new TH1F("HLTbits", "HLT efficiency;bit;%", 120, 0, 120);
  hm["wr"]         = new TH1F("wr", "W_{R} invariant mass;GeV/c^{2};events/30 GeV", 100, 0, 3000);
  hm["l1jj"]       = new TH1F("l1jj", "l_{1}jj invariant mass;GeV/c^{2};events/30 GeV", 100, 0, 3000);
  hm["l2jj"]       = new TH1F("l2jj", "l_{2}jj invariant mass;GeV/c^{2};events/30 GeV", 100, 0, 3000);
  hm["jj"]         = new TH1F("jj", "jj invariant mass;GeV/c^{2};events/30 GeV", 100, 0, 3000);
  hm["ll"]         = new TH1F("ll", "ll invariant mass;GeV/c^{2};events/30 GeV", 100, 0, 3000);
  hm["met"]        = new TH1F("met", "Missing Et;GeV", 100, 0, 1000);
  hm["wr_ll"]      = new TH2D("wr_ll", "wr_ll mass", 100, 0, 4000, 100, 0, 4000);
  hm["wr_jj"]      = new TH2D("wr_jj", "wr_jj mass", 100, 0, 4000, 100, 0, 4000);
  hm["ll_jj"]      = new TH2D("ll_jj", "ll_jj mass", 100, 0, 4000, 100, 0, 4000);
  hm["l1jj_l2jj"]  = new TH2D("l1jj_l2jj", "l1jj_l2jj mass", 100, 0, 4000, 100, 0, 4000);
  hm["l1jj_wr"]    = new TH2D("l1jj_wr", "l1jj_wr mass", 100, 0, 4000, 100, 0, 4000);
  hm["l2jj_wr"]    = new TH2D("l2jj_wr", "l2jj_wr mass", 100, 0, 4000, 100, 0, 4000);
  
  hm["nlept"]      = new TH1F("nlept", "nlept", 10, 0, 10);
  hm["l1iso"]      = new TH1F("l1iso", "l1 Isolation", 100, 0, 1);
  //hm["l1fem"]      = new TH1F("l1fem", "l1 Electromagnetic energy fraction", 100, -2, 2);
  hm["l1nhits"]    = new TH1F("l1nhits", "l1 Number of hits", 100, 0, 30);
  hm["l1chi2"]     = new TH1F("l1chi2", "l1 #chi^{2} of the track fit", 100, 0, 1000);
  hm["l1charge"]   = new TH1F("l1charge", "l1 Charge", 4, -2, 2);
  hm["l1pt"]       = new TH1F("l1pt", "l1 pT;GeV/c", 100, 0, 3000);
  hm["l2pt"]       = new TH1F("l2pt", "l2 pT;GeV/c", 100, 0, 3000);
  
  hm["njets"]      = new TH1F("njets", "njets", 20, 0, 20);
  //hm["j1iso"]      = new TH1F("j1iso", "j1iso", 100, -2, 2);
  hm["j1fem"]      = new TH1F("j1fem", "j1 Electromagnetic energy fraction", 100, -2, 2);
  //hm["j1nhits"]    = new TH1F("j1nhits", "j1nhits", 100, 0, 20);
  //hm["j1chi2"]     = new TH1F("j1chi2", "j1chi2", 100, -10, 10);
  //hm["j1charge"]   = new TH1F("j1charge", "j1charge", 20, -10, 10);
  hm["j1pt"]       = new TH1F("j1pt", "j1 pT;GeV/c", 100, 0, 3000);
  hm["j2pt"]       = new TH1F("j2pt", "j2 pT;GeV/c", 100, 0, 3000);
  
  // fill histograms
  slice.Draw("WR>>wr",       cut, "goff");
  slice.Draw("L1JJ>>l1jj",   cut, "goff");
  slice.Draw("L2JJ>>l2jj",   cut, "goff");
  slice.Draw("JJ>>jj",       cut, "goff");
  slice.Draw("LL>>ll",       cut, "goff");
  slice.Draw("MET>>met",     cut, "goff");            
  slice.Draw("WR:LL>>wr_ll", cut, "goff COLZ");
  slice.Draw("WR:JJ>>wr_jj", cut, "goff COLZ");
  slice.Draw("LL:JJ>>ll_jj", cut, "goff COLZ");
  slice.Draw("L1JJ:L2JJ>>l1jj_l2jj", cut, "goff COLZ");
  slice.Draw("L1JJ:WR>>l1jj_wr", cut, "goff COLZ");
  slice.Draw("L2JJ:WR>>l2jj_wr", cut, "goff COLZ");
  
  slice.Draw("nlept>>nlept",   cut, "goff");
  slice.Draw("l1.iso>>l1iso",     cut, "goff");
  //slice.Draw("l1.fem>>l1fem",     cut, "goff");
  slice.Draw("l1.nhits>>l1nhits", cut, "goff");
  slice.Draw("l1.chi2>>l1chi2",   cut, "goff");
  slice.Draw("l1.charge>>l1charge", cut, "goff");
  slice.Draw("l1.pt>>l1pt",       cut, "goff");
  slice.Draw("l2.pt>>l2pt",       cut, "goff");
  
  slice.Draw("njets>>njets",      cut, "goff");       
  //slice.Draw("j1.iso>>j1iso",     cut, "goff");      
  slice.Draw("j1.fem>>j1fem",     cut, "goff");      
  //slice.Draw("j1.nhits>>j1nhits", cut, "goff");    
  //slice.Draw("j1.chi2>>j1chi2",   cut, "goff");     
  //slice.Draw("j1.charge>>j1charge", cut, "goff");   
  slice.Draw("j1.pt>>j1pt",       cut, "goff");  
  slice.Draw("j2.pt>>j2pt",       cut, "goff");  
  
  // set histograms as in-memory only (not connected with current open file)
  // so that they are not deleted after exit from this function
  for (HistMap::iterator i = hm.begin(); i != hm.end(); ++i)
    i->second->SetDirectory(0);
  
  return hm;
}

HistMap Model::hist2(TheEvent& e)
{
  // TODO: do we need to take into account weight during histograms filling?
  
  // total HLT bits:
  const int nBits = e.triggerNames.size();
  
  // book histograms
  HistMap hm;
  hm["HLTbits"] = new TH1F("HLTbits", "HLT efficiency;#bit;%", nBits, 0, nBits);
  hm["ElRes"]   = new TH1F("ElRes", "electrons resolution", 100, -0.3, 0.3);
  hm["MuRes"]   = new TH1F("MuRes", "muons resolution", 100, -0.3, 0.3);
  hm["ElMult"]  = new TH1F("ElMult", "electrons multiplicity", 10, 0, 10);
  hm["MuMult"]  = new TH1F("MuMult", "muons multiplicity", 10, 0, 10);
  hm["JetMult"] = new TH1F("JetMult", "jets multiplicity", 20, 0, 20);
  
  const int nEvents = e.totalEvents();
  
  for (int i = 0; i < nEvents; ++i) {
    e.readEvent(i);
    
    // HLT bits distribution
    for (int j = 0; j < nBits; ++j)
      if ((*e.hltBits)[j])
        hm["HLTbits"]->Fill(j);
    
    // electron energy resolution
    for (unsigned int j = 0; j < e.electrons.size(); ++j) {
      const Particle& reco = e.electrons[j];
      if (reco.mcId >= 0) {
        const Particle& mc = e.gens[reco.mcId];
        const float r = (reco.p4.E() - mc.p4.E())/mc.p4.E();
        hm["ElRes"]->Fill(r);
      }
    }
    
    // muon energy resolution
    for (unsigned int j = 0; j < e.muons.size(); ++j) {
      const Particle& reco = e.muons[j];
      if (reco.mcId >= 0) {
        const Particle& mc = e.gens[reco.mcId];
        const float r = (reco.p4.E() - mc.p4.E())/mc.p4.E();
        hm["MuRes"]->Fill(r);
      }
    }
    
    // Multiplicity
    hm["ElMult"]->Fill(e.electrons.size());
    hm["MuMult"]->Fill(e.muons.size());
    hm["JetMult"]->Fill(e.jets.size());
  }
  
  // normalize to 100%
  hm["HLTbits"]->Scale(100.0/nEvents);
  
  // set histograms as in-memory only (not connected with current open file)
  // so that they are not deleted after exit from this function
  for (HistMap::iterator i = hm.begin(); i != hm.end(); ++i)
    i->second->SetDirectory(0);
  
  return hm;
}

HistMap Model::doHist(const string fname)
{
  TheEvent event(fname.c_str());
  if (!event)
    return HistMap();
  
  sample = event.sample;
  
  const HistMap hm1 = hist1(event);
  const HistMap hm2 = hist2(event);
  
  // merge histograms to one collection
  HistMap hm;
  hm.insert(hm1.begin(), hm1.end());
  hm.insert(hm2.begin(), hm2.end());
  
  return hm;
}

HistMap Model::summHist()
{
  HistMap summ;
  
  for (size_t i = 0; i < fnames.size(); ++i) {
    const HistMap hm = doHist(fnames[i]);
    
    if (i == 0) summ = hm;
    else
      for (HistMap::const_iterator i = hm.begin(); i != hm.end(); ++i)
        summ[i->first]->Add(i->second);
  }
  
  return summ;
}

void Model::reApplyCuts(const Channel ch, const TCut ct)
{
  channel = ch;
  cut = ct;
  hist = summHist();
}

void Model::print(const char* fname) const
{
  // set style
  //gStyle->SetOptStat("nemruo");
  //gStyle->SetPalette(1);
  
  // decrease verbosity
  gErrorIgnoreLevel = kWarning;
  
  TCanvas c(fname, fname);
  c.Print(".ps[");
  
  for (HistMap::const_iterator i = hist.begin(); i != hist.end(); ++i) {
    //cout << i->first << endl;
    i->second->Draw();
    c.Print(".ps");
  }
  
  c.Print(".ps]");
  
  // restore verbosity
  gErrorIgnoreLevel = kUnset;
}

// =================================================================================================

void print(const vector<Model> stack,
           const char* fname,
           const char* list)
{
  const int col[] = {kRed, kGreen, kBlue, kYellow, kMagenta, kCyan,
                     kOrange, kSpring, kTeal, kAzure, kViolet, kPink};
  THStack hs(list, list);
  TLegend leg(0.7, 0.75, 0.89, 0.89);
  for (size_t i = 0; i < stack.size(); ++i) {
    HistMap hm = stack[i].hist;
    TH1* h = hm[list];
    h->SetFillColor(col[i] - 9);
    hs.Add(h);
    leg.AddEntry(h, stack[i].sample.name.c_str(), "f");
  }
  
  // decrease verbosity
  gErrorIgnoreLevel = kWarning;
  
  TCanvas c(fname, fname);
  hs.Draw();
  leg.Draw();
  c.Print(".ps");
  //c.Print(".ps[");
  
  // for (size_t i = 0; i < stack.size(); ++i) {
    // HistMap hm = stack[i].hist;
    // TH1* h = hm[list];
    // if (i == 0) h->Draw();
    // else        h->Draw("same");
  // }
  // c.Print(".ps");
  // c.Print(".ps]");
  
  // restore verbosity
  gErrorIgnoreLevel = kUnset;
}

// background reduction table
void brt(vector<Model> stack,
         const vector<TCut> cuts,
         const Channel ch)
{
  // calculate number of events and entries with
  // different cuts
  float* nEvents = new float[stack.size() * cuts.size()];
  int* nEntries = new int[stack.size() * cuts.size()];
  TCut cut;
  for (size_t i = 0; i < cuts.size(); ++i) {
    cut *= cuts[i];
    
    for (size_t j = 0; j < stack.size(); ++j) {
      stack[j].reApplyCuts(ch, cut);
      TH1* h = stack[j].hist["wr"];
      
      nEvents[i*stack.size() + j] = h->Integral();
      nEntries[i*stack.size() + j] = (int) h->GetEntries();
    }
  }
  
  cout << endl;
  cout << "Background reduction table:" << endl;
  
  // print header
  cout << "step\t";
  for (size_t j = 0; j < stack.size(); ++j)
    cout << stack[j].sample.name << "\t";
  cout << endl;
  
  // print table
  for (size_t i = 0; i < cuts.size(); ++i) {
    cout << cuts[i].GetName() << "\t";
    
    for (size_t j = 0; j < stack.size(); ++j) {
      cout << nEvents[i*stack.size() + j] << "/"
           << nEntries[i*stack.size() + j] << "\t";
    }
    
    cout << endl;
  }
  
  delete [] nEvents;
  delete [] nEntries;
}

// saves 1D histogram as CSV-file
void save(const TH1& h, const char* fname)
{
  ofstream f(fname);
  
  f << h.GetTitle() << "\n";
  f << h.GetXaxis()->GetTitle() << "," << h.GetYaxis()->GetTitle() << "\n";
  
  for (int i = 1; i <= h.GetNbinsX(); i++)
    f << h.GetXaxis()->GetBinLowEdge(i) << "," << h.GetBinContent(i) << "\n";
}
