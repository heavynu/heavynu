#!/bin/bash

if [[ "$1" != "slave-full" && "$1" != "slave-xs" ]]; then
  # --- master mode ---
  # (this part is going on local machine)
  
  # print usage info
  if [[ "$#" < "4" ]]; then
    echo "batch-alpgen.sh: tool for alpgen samples simulation on LXBATCH"
    echo "Usage:"
    echo "  $0 [queue] [njobs] [lhe_file_name] [destination]"
    echo "           [queue] - LXBATCH queue name (8nm 1nh 8nh 1nd 2nd 1nw 2nw)"
    echo "           [njobs] - number of jobs to submit"
    echo "     [events_file] - path to alpgen events files, for ex.: Alpgen/z2jee"
    echo "     [destination] - place for output files (with format host:/path)"
    echo "Example:"
    echo "  $0 2nd 5 Alpgen/z2jee pcrfcms14:/moscow41/$(whoami)/alpgen-z2jee"
    echo ""
    
    echo "Possible [events_file] values:"
    find Alpgen/ -maxdepth 1 -name *.unw | cut -d . -f 1
    
    exit 1
  fi
  
  # parameters
  queue=$1
  njobs=$2
  alpfile=$3
  dst=$4
  cmssw=$CMSSW_BASE
  
  # check CMSSW:
  if [[ "$cmssw" == "" ]]; then
    echo "ERROR: CMSSW environment not defined"
    exit 1
  fi
  
  if [ ! -d "$cmssw" ]; then
    echo "ERROR: wrong CMSSW environment"
    echo "       CMSSW_BASE = $CMSSW_BASE"
    echo '       path do not exists'
    exit 1
  fi
  
  fs whereis "$cmssw" &> /dev/null
  if [[ "$?" == "1" ]]; then
    echo "ERROR: local CMSSW area situated on non-AFS folder:"
    echo "       CMSSW_BASE = $CMSSW_BASE"
    echo "       which can't be accessed from LXBATCH servers"
    exit 1
  fi
  
  # check destination host
  rhost=$(echo $dst | cut -d : -f 1)
  if [[ "$rhost" == "" ]] ; then
    echo "ERROR: host not defined in destination path"
    echo "       destination = $dst"
    exit 1
  fi
  
  # connect to destination host and create destination path
  rpath=$(echo $dst | cut -d : -f 2-)
  ssh -x $rhost "mkdir -p $rpath"
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: fail to create destination path"
    echo "         dst   = $dst"
    echo "         rhost = $rhost"
    echo "         rpath = $rpath"
    exit 1
  fi
  
  # prepare job submission directory
  jobfiles="$0 ${alpfile}.unw ${alpfile}_unw.par Alpgen/AlpgenGenHLT_cfg.py RecoFromHLT_cfg.py heavynu_cfi.py"
  batchdir="$(pwd)/batch-$(date +%F-%H.%M.%S)"
  mkdir $batchdir
  cp $jobfiles $batchdir
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: fail to copy job files to batch directory"
    echo "       probably, job files do not situated in current directory"
    echo "         job files   = $jobfiles"
    echo "         current dir = $(pwd)"
    echo "         batch dir   = $batchdir"
    exit 1
  fi
  
  # enter to the batch directory
  cd $batchdir
  
  # check do we have the same global tag in all job files
  tagStep1=$(cat AlpgenGenHLT_cfg.py | grep "process.GlobalTag.globaltag" | cut -d = -f 2)
  tagStep2=$(cat RecoFromHLT_cfg.py   | grep "process.GlobalTag.globaltag" | cut -d = -f 2)
  if [[ "$tagStep1" != "$tagStep2" ]] ; then
    echo "ERROR: different global tags specified in input files:"
    echo "         AlpgenGenHLT_cfg.py: globaltag = $tagStep1"
    echo "         RecoFromHLT_cfg.py : globaltag = $tagStep2"
    exit 1
  fi
  
  if [[ "$tagStep1" == "" ]] ; then
    echo "ERROR: fail to determine global tag of job files"
    echo "         correct the submission script"
    exit 1
  fi
  
  # TODO: add chech for the same geometry and magnetic field in all job files
  
  # edit steering file to set name of input .lhe file:
  alpname=$(basename $alpfile)
  sed -e "/fileNames/ s,([^)]*),('file:$alpname')," -i AlpgenGenHLT_cfg.py
  
  # submit jobs:
  echo "Job submissioning to LXBATCH"
  echo "Batch directory = $(pwd)"
  chmod a+x $0
  # make jobfiles readonly to prevent changes
  chmod a-w *
  
  for i in $(seq $njobs) ; do
    echo -n "$i "
    bsub -q $queue $(pwd)/$0 "slave-full" $alpname $dst $cmssw $i
  done
  
  # submit job which will do cross-section calculation:
  # (not implemented for Alpgen)
  #  echo -n "xs "
  #  bsub -q $queue $(pwd)/$0 "slave-xs" $alpname $dst $cmssw
  
else
  # --- slave mode ---
  # (this part is going on "lxbatch" server)
  
  src=$LS_SUBCWD
  jobid=$LSB_BATCH_JID
  mode=$1
  alpname=$2
  dst=$3/$jobid
  cmssw=$4
  jobnum=$5
  
  # copying job files
  cp $src/${alpname}.unw $src/${alpname}_unw.par $src/AlpgenGenHLT_cfg.py $src/RecoFromHLT_cfg.py $src/heavynu_cfi.py .
  
  # setting up environment
  echo "$(date): Setting up environment..."
  source /afs/cern.ch/cms/sw/cmsset_default.sh
  oldpwd=$(pwd)
  cd $cmssw/src
  eval `scramv1 runtime -sh`
  
  echo "$(date): showtags -c -u"
  showtags -c -u
  
  cd $oldpwd
  
  if [[ "$mode" == "slave-full" ]] ; then
    # simulation
    echo "$(date): Running full simulation chain"
    echo "$(date): Alpgen events is $alpname"
    
    # extract number of events per one job from steering file:
    eventsPerJob=$(cat AlpgenGenHLT_cfg.py | grep -A 1 process.maxEvents | grep input | tr '()' ',,' | cut -d , -f 2)
    
    # modify steering file to skip a number of events according to job number:
    let 'skipEvents=eventsPerJob*(jobnum-1)'
    sed -e "/skipEvents/ s,([0-9]*),($skipEvents)," -i AlpgenGenHLT_cfg.py
    
    # 1st step
    echo "$(date): Running GEN SIM DIGI L1 DIGI2RAW HLT"
    echo "$(date): cmsRun AlpgenGenHLT_cfg.py &> AlpgenGenHLT_cfg.log"
    cmsRun AlpgenGenHLT_cfg.py &> AlpgenGenHLT_cfg.log
    if [[ "$?" != "0" ]] ; then
      echo "ERROR: failed to finish step #1"
      echo "       log-file AlpgenGenHLT_cfg.log:"
      echo "=================================================================="
      cat AlpgenGenHLT_cfg.log
      echo "=================================================================="
      exit 1
    fi
    
    # 2nd step
    echo "$(date): Running RAW2DIGI RECO"
    echo "$(date): cmsRun RecoFromHLT_cfg.py &> RecoFromHLT_cfg.log"
    cmsRun RecoFromHLT_cfg.py &> RecoFromHLT_cfg.log
    if [[ "$?" != "0" ]] ; then
      echo "ERROR: failed to finish step #2"
      echo "       log-file RecoFromHLT_cfg.log:"
      echo "=================================================================="
      cat RecoFromHLT_cfg.log
      echo "=================================================================="
      exit 1
    fi
    
    # remove files from intermediate steps
    rm hlt.root
    
  else
    # cross-section
    echo "$(date): Running cross-section calculation"
    echo "WARNING: not implemented for Alpgen"
  fi
  
  # the jobs which you submit typically start and finish at the
  # same time, to prevent destination host from a large amount of
  # simultaneuos 'scp' connections do a random delay [0 - 1000 seconds]
  sleep $(( $RANDOM % 1000 ))
  
  # remove .unw file from job directory (to prevent copy)
  rm -f ${alpname}.unw
  
  # and copy results
  echo "$(date): Copying results to $dst"
  scp -Br $(pwd)/ $dst
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: failed to copy results"
    echo "         src = $(pwd)/"
    echo "         dst = $dst"
    exit 1
  fi
  
  echo "$(date): Job finished successfully"
fi
