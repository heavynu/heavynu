#!/bin/bash

function read_points {
  local fname="$1"
  local channel="$2"
  
  cat $fname | while read line ; do
    # skip empty lines and comments
    if [[ "$line" == "" || "${line:0:1}" == "#" ]] ; then
      continue
    fi
    
    local MW=$(echo "$line" | cut -d : -f 1)
    local MN_list=$(echo "$line" | cut -d : -f 2)
    
    for mn in $MN_list ; do
      # print configuration name:
      echo "WR${MW}_nuR${channel}${mn}"
    done
  done | sort | uniq
}

if [[ "$1" == "all" ]] ; then
  fname="signal-points.txt"
  ch="e"         # channel: e, mu
  nevents="10000" # number of events per mass point
  
  # extract CMSSW version from environment and GlobalTag from .py steering file:
  ver=$(echo $CMSSW_VERSION | cut -d _ -f 2-4)
  tag=$(python HeavyNuGenHLT_cfg.py x x | grep "global tag" | cut -d "'" -f 2 | cut -d : -f 1)
  
  # root of results destination directory:
  dstroot="pcrfcms14:/moscow41/$(whoami)/signal-$(date +%F-%H.%M.%S)/$ver-$tag-"
  
  logfile="batch-signal-all.log"
  
  echo "Submitting all combinations of signal samples to LXBATCH..."
  echo "Points: $fname"
  echo "Channel: $ch"
  echo "Destination: ${dstroot}*"
  echo ""
  
  # read mass points from steering file
  for conf in $(read_points "$fname" "$ch"); do
    # submit jobs:
    $0 $nevents $conf ${dstroot}${conf}
  done 2>&1 | tee $logfile
  
  echo ""
  echo "===================================="
  echo "Submission complete"
  echo "Log file: $logfile"
  echo "Total number of combinations: $(grep '^Submitting ' $logfile | wc -l)"
  echo "Total number of jobs: $(grep 'Job.*is submitted' $logfile | wc -l)"
  echo "Total size: ~ totalJobs * eventsPerJob * 0.7 Mb"
  echo ""
  
elif [[ "$1" != "slave-full" && "$1" != "slave-xs" ]]; then
  # --- master mode ---
  # (this part is going on local machine)
  
  # print usage info
  if [[ "$#" < "3" ]]; then
    echo "batch-signal.sh: tool for signal samples simulation on LXBATCH"
    echo "Usage:"
    echo "  $0 [nevents] [pythia_setting] [destination]"
    echo "         [nevents] - number of events to simulate"
    echo "  [pythia_setting] - name of pythia configuration set from heavynu_cfi.py"
    echo "     [destination] - place for output files (with format host:/path)"
    echo "Example:"
    echo "  $0 1000 WR1200_nuRe500 pcrfcms14:/moscow41/$(whoami)/WR1200_nuRe500"
    echo ""
    
    echo "Possible [pythia_setting] values (as extracted from heavynu_cfi.py):"
    cat heavynu_cfi.py | grep vstring | (read ; cut -d = -f 1)
    echo "  WR[mass]_nuR[e|mu|tau][mass]"
    
    exit 1
  fi
  
  echo "Submitting [$*]"
  
  # parameters
  nevents=$1
  pythia=$2
  dst=$3
  cmssw=$CMSSW_BASE
  
  # check 'pythia_setting':
  # does it looks like signal name?
  if [[ "$(echo $pythia | grep -P '^WR[\d]+_nuR[emutau]+[\d]+$')" != "$pythia" ]] ; then
  
    # does it name of card from heavynu_cfi.py ?
    all=" $(cat heavynu_cfi.py | grep vstring | (read; cut -d = -f 1) | xargs) "
    if [[ "${all/ $pythia /}" == "$all" ]] ; then
      echo "ERROR: can not find specified pythia configuration set"
      echo "       in sets definition file 'heavynu_cfi.py'"
      echo "         pythia_setting = $pythia"
      echo "       check parameter"
      exit 1
    fi
  fi
  
  # check do we have the same global tag in all job files
  tagStep1=$(cat HeavyNuGenHLT_cfg.py | grep "process.GlobalTag.*=" | cut -d = -f 2)
  tagStep2=$(cat RecoFromHLT_cfg.py   | grep "process.GlobalTag.*=" | cut -d = -f 2)
  if [[ "$tagStep1" != "$tagStep2" ]] ; then
    echo "ERROR: different global tags specified in input files:"
    echo "         HeavyNuGenHLT_cfg.py: globaltag = $tagStep1"
    echo "         RecoFromHLT_cfg.py  : globaltag = $tagStep2"
    exit 1
  fi
  
  if [[ "$tagStep1" == "" ]] ; then
    echo "ERROR: fail to determine global tag of job files"
    echo "         correct the submission script"
    exit 1
  fi
  
  energyXS=$(cat HeavyNuGen_cfg.py | grep "comEnergy.*=" | cut -d '(' -f 2 | cut -d ')' -f 1)
  energySIM=$(cat HeavyNuGenHLT_cfg.py   | grep "comEnergy.*=" | cut -d '(' -f 2 | cut -d ')' -f 1)
  if [[ "$energyXS" != "$energySIM" ]] ; then
    echo "ERROR: different beam energies ($energyXS and $energySIM) in files"
    echo "       HeavyNuGen_cfg.py and HeavyNuGenHLT_cfg.py"
    exit 1
  fi
  
  # TODO: add check for the same geometry and magnetic field in all job files
  
  # check CMSSW:
  if [[ "$cmssw" == "" ]]; then
    echo "ERROR: CMSSW environment is not defined"
    exit 1
  fi
  
  if [ ! -d "$cmssw" ]; then
    echo "ERROR: wrong CMSSW environment"
    echo "       CMSSW_BASE = $CMSSW_BASE"
    echo '       path does not exists'
    exit 1
  fi
  
  fs whereis "$cmssw" &> /dev/null
  if [[ "$?" == "1" ]]; then
    echo "ERROR: local CMSSW area situated on non-AFS folder:"
    echo "       CMSSW_BASE = $CMSSW_BASE"
    echo "       which can't be accessed from LXBATCH servers"
    exit 1
  fi
  
  # check destination host
  rhost=$(echo $dst | cut -d : -f 1)
  if [[ "$rhost" == "" ]] ; then
    echo "ERROR: host not defined in destination path"
    echo "       destination = $dst"
    exit 1
  fi
  
  # connect to destination host and create destination path
  rpath=$(echo $dst | cut -d : -f 2-)
  ssh -x $rhost "mkdir -p $rpath"
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: fail to create destination path"
    echo "         dst   = $dst"
    echo "         rhost = $rhost"
    echo "         rpath = $rpath"
    exit 1
  fi
  
  # prepare job submission directory
  batchdir="$(pwd)/batch-$(date +%F-%H.%M.%S)"
  mkdir $batchdir
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: fail to create jobs submission directory"
    echo "         batch dir   = $batchdir"
    exit 1
  fi
  
  cp $0 HeavyNuGen_cfg.py HeavyNuGenHLT_cfg.py RecoFromHLT_cfg.py heavynu_cfi.py $batchdir/ && \
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: fail to copy job files to batch directory"
    echo "       probably, job files do not situated in current directory"
    echo "         current dir = $(pwd)"
    echo "         batch dir   = $batchdir"
    exit 1
  fi
  
  # enter to the batch directory
  cd $batchdir
  
  jobsQueue="1nd"     # name of lxbatch queue which should be sufficient to run $eventsPerJob events
  eventsPerJob="100"  # number of events per one job
  njobs=$(($nevents / $eventsPerJob))  # number of jobs to submit
  
  # submit jobs:
  echo "Batch directory = $(pwd)"
  echo "Batch queue = $jobsQueue"
  echo "Total events = $nevents ($njobs jobs * $eventsPerJob events)"
  
  # correct HeavyNuGenHLT_cfg.py to set number of events per job
  sed -e "s,input = cms.untracked.int32(.*),input = cms.untracked.int32($eventsPerJob)," \
      -i HeavyNuGenHLT_cfg.py
  
  # make jobfiles readonly to prevent changes
  chmod a-w *
  jobcmd="$(pwd)/$0"
  
  for i in $(seq $njobs) ; do
    echo -n "$i "
    # options:
    #   -q: set queue
    #   -r: re-run job if execution host failed
    #   -R: require to have at least 2000Mb RAM on execution host
    #       (neccessary to prevent jobs crashes due to insufficient memory)
    #   -J: set job name
    bsub -q $jobsQueue -r -R "rusage[mem=2000]" -J "$pythia" $jobcmd "slave-full" $pythia $dst $cmssw
  done
  
  # submit job which will do cross-section calculation:
    echo -n "xs "
    bsub -q "1nh" -r -R "rusage[mem=300]" -J "$pythia XS" $jobcmd "slave-xs" $pythia $dst $cmssw
  
else
  # --- slave mode ---
  # (this part is going on "lxbatch" server)
  
  src=$LS_SUBCWD
  jobid=$LSB_BATCH_JID
  mode=$1
  pythia=$2
  dst=$3/$jobid
  cmssw=$4
  
  # copying job files
  echo "$(date): Copying job files..."
  cp $src/*.py . || exit 1
  
  # setting up environment
  echo "$(date): Setting up environment..."
  oldpwd=$(pwd)
  cd $cmssw/src
  eval `scramv1 runtime -sh`
  echo "  CMSSW_RELEASE_BASE = $CMSSW_RELEASE_BASE"
  echo "  CMSSW_VERSION = $CMSSW_VERSION"
  echo "  CMSSW_BASE = $CMSSW_BASE"
  
  echo "$(date): Checking local packages versions..."
  for i in * ; do
    gitinfo=$(cd $i && git describe --tags 2> /dev/null)
    svninfo=$(cd $i && svnversion)
    echo "  $i: git: $gitinfo | svn: $svninfo"
  done
  
  cd $oldpwd
  
  if [[ "$mode" == "slave-full" ]] ; then
    # simulation
    echo "$(date): Running full simulation chain"
    echo "$(date): Pythia configuration is $pythia"
    
    # 1st step
    echo "$(date): Running GEN SIM DIGI L1 DIGI2RAW HLT"
    echo "$(date): cmsRun HeavyNuGenHLT_cfg.py $pythia &> HeavyNuGenHLT_cfg.log"
    cmsRun HeavyNuGenHLT_cfg.py $pythia &> HeavyNuGenHLT_cfg.log
    if [[ "$?" != "0" ]] ; then
      echo "ERROR: failed to finish step #1"
      echo "       log-file HeavyNuGenHLT_cfg.log:"
      echo "=================================================================="
      cat HeavyNuGenHLT_cfg.log
      echo "=================================================================="
      exit 1
    fi
    
    # 2nd step
    echo "$(date): Running RAW2DIGI RECO"
    echo "$(date): cmsRun RecoFromHLT_cfg.py &> RecoFromHLT_cfg.log"
    cmsRun RecoFromHLT_cfg.py &> RecoFromHLT_cfg.log
    if [[ "$?" != "0" ]] ; then
      echo "ERROR: failed to finish step #2"
      echo "       log-file RecoFromHLT_cfg.log:"
      echo "=================================================================="
      cat RecoFromHLT_cfg.log
      echo "=================================================================="
      exit 1
    fi
    
    # remove files from intermediate steps
    rm hlt.root
    
  else
    # cross-section
    echo "$(date): Running cross-section calculation"
    echo "$(date): Pythia configuration is $pythia"
    
    echo "$(date): Running GEN"
    echo "$(date): cmsRun HeavyNuGen_cfg.py $pythia &> xs-$pythia.log"
    cmsRun HeavyNuGen_cfg.py $pythia &> xs-$pythia.log
    if [[ "$?" != "0" ]] ; then
      echo "ERROR: failed to calc cross-section"
      echo "       log-file xs-$pythia.log:"
      echo "=================================================================="
      cat xs-$pythia.log
      echo "=================================================================="
      exit 1
    fi
  fi
  
  # the jobs which you submit typically start and finish at the
  # same time, to prevent destination host from a large amount of
  # simultaneuos 'scp' connections do a random delay [0 - 1000 seconds]
  sleep $(( $RANDOM % 1000 ))
  
  # clean destination directory
  rhost=${dst%%:*}
  rpath=${dst#*:}
  ssh -x $rhost "rm -rf \"$rpath\""
  
  # and copy results
  echo "$(date): Copying results to $dst"
  scp -Br $(pwd)/ $dst
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: failed to copy results"
    echo "         src = $(pwd)/"
    echo "         dst = $dst"
    exit 1
  fi
  
  echo "$(date): Job finished successfully"
fi
