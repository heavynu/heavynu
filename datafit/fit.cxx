// RooFit
#ifndef __CINT__
#include "RooGlobalFunc.h"
#endif
#include "RooMsgService.h"
#include "RooRandom.h"
#include "RooAbsGenContext.h"
#include "RooChi2Var.h"
#include "RooNLLVar.h"
#include "RooMinuit.h"
#include "RooFitResult.h"
#include "RooRealVar.h"
#include "RooBreitWigner.h"
#include "RooGaussian.h"
#include "RooExponential.h"
#include "RooExtendPdf.h"
#include "RooLandau.h"
#include "RooFormulaVar.h"
#include "RooNDKeysPdf.h"
#include "RooKeysPdf.h"
#include "RooHistPdf.h"
#include "RooAbsData.h"
#include "RooAbsDataStore.h"
#include "RooTreeDataStore.h"
#include "RooAddPdf.h"
#include "RooProdPdf.h"
#include "RooLinkedList.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooPlot.h"
#include "RooMCStudy.h"
#include "RooDLLSignificanceMCSModule.h"
using namespace RooFit;

// ROOT
#include "TTree.h"
#include "TPaveText.h"
#include "TStyle.h"
#include "TF1.h"
#include "TH1.h"
#include "TH2.h"
#include "TCanvas.h"
#include "TString.h"
#include "TFile.h"
#include "TROOT.h"
#include "TText.h"
#include "THStack.h"
#include "TLegend.h"
#include "TGraphErrors.h"

// c/c++
#include <cmath>
#include <vector>
#include <algorithm>
#include <utility>
using namespace std;

//#include "tdrstyle.h"

// wrapper function to simplify Draw() of internal TTree
void Draw(const RooDataSet* ds, const char* var, const char* sel = "")
{
  TTree& t = ((RooTreeDataStore*) ds->store())->tree();
  t.Draw(var, sel, "goff");
}

// wrapper for RooAbsPdf to allow to call the protected function RooAbsPdf::genContext()
class RooAbsPdfWrap: public RooAbsPdf {
public:
  using RooAbsPdf::genContext;
};

//
float CalcMinus2LnQ(RooAbsPdf& sb_model,
                    RooAbsPdf& b_model,
                    RooAbsData& data)
{
  RooNLLVar sb_nll("sb_nll", "sb_nll", sb_model, data, Extended(kTRUE));
  RooNLLVar  b_nll( "b_nll",  "b_nll",  b_model, data, Extended(kTRUE));

  const float m2lnQ = 2*(sb_nll.getVal() - b_nll.getVal());
  return m2lnQ;
}

struct Ensemble
{
  // parameters
  float nsig;          // number of signal events
  float syst;          // systematic uncertanty
  
  // results
  vector<float> m2LnQ; // value of -2 Ln Q
  float pval;          // p-value
  
  // median value of m2LnQ
  float median()
  {
    vector<float> v(m2LnQ);
    sort(v.begin(), v.end());
    const int n = v.size();
    const int i = n/2;
    if (n % 2) return v[i];
    else       return (v[i] + v[i - 1])/2;
  }
};

std::ostream& operator<<(std::ostream& os, const Ensemble& e)
{
  os << "Ensemble"
     << " nsig = " << e.nsig
     << " pval = " << e.pval
     << "\n";
  
  for (size_t i = 0; i < e.m2LnQ.size(); ++i)
    os << e.m2LnQ[i] << " ";
  os << "\n";
  
  return os;
}

Ensemble CreateEnsemble(const RooArgSet& vars, RooAbsPdf& model, RooRealVar& nsig, RooAbsPdf& bkg, int ntoys, float syst)
{
  vector<float> m2LnQ(ntoys);
  const float nsig_orig = nsig.getVal();
  
  cout << "Ensemble: nsig = " << nsig_orig << " | "
       << "ntoys = " << ntoys << " syst = " << syst << " "
       << flush;
  
  // create generator context to speed-up generation
  RooAbsGenContext* context = ((RooAbsPdfWrap*) &model)->genContext(vars);
  
  for (int i = 0; i < ntoys; ++i) {
    if ((i + 1) % (ntoys / 20) == 0)
      cout << "." << flush;
    
    // vary nsig to take into account systematics
    float k = RooRandom::randomGenerator()->Gaus(1, syst);
    if (k < 0) k = 0;
    nsig.setVal(nsig_orig*k);
    // take into account statistical fluctuations
    const Double_t expected = model.expectedEvents(vars);
    const Int_t nevents = RooRandom::randomGenerator()->Poisson(expected);
    
    // generate data sample (pseudo-experiment)
    RooDataSet* data = context->generate(nevents);
    // restore nsig
    //nsig.setVal(nsig_orig);
    
    // calculate - 2 * ln Q
    m2LnQ[i] = CalcMinus2LnQ(model, bkg, *data);
    
    delete data;
  }
  
  cout << endl;
  
  const Ensemble ensemble = {nsig_orig, syst, m2LnQ, -1};
  return ensemble;
}

// count part of elements which less than threshold
float count_less_part(const vector<float>& v, const float thr)
{
  const int n = v.size();
  int k = 0;
  for (int i = 0; i < n; ++i)
    if (v[i] < thr) k++;
  
  return (float) k / (float) n;
}

vector<Ensemble> LimitCalc(const RooArgSet& vars,
                           RooAbsPdf& model,
                           RooRealVar& nsig,
                           RooAbsPdf& bkg,
                           int ntoys,              // number of pseudo-experiments
                           float syst)             // value of systematic uncertanties
{
  vector<Ensemble> team;
  #define DIM(x) (sizeof(x)/sizeof(*x))
  float ns[] = {0, 0.3, 0.6, 1, 2, 2.5, 2.75, 3, 3.25, 3.5, 3.75, 4, 4.25, 4.5, 5, 6, nsig.getVal()};
  
  // calculate vectors of m2LnQ values for different nsig
  for (size_t i = 0; i < DIM(ns); ++i) {
    nsig.setVal(ns[i]);
    const Ensemble e = CreateEnsemble(vars, model, nsig, bkg, ntoys, syst);
    team.push_back(e);
  }
  
  // calc the reference m2LnQ  (from nsig=0 ensemble)
  const float ref = team[0].median();
  
  // calculate p-value
  for (size_t i = 0; i < team.size(); ++i)
    team[i].pval = count_less_part(team[i].m2LnQ, ref);
  
  // print results
  //cout << "m2LnQ values:\n";
  //for (int i = 0; i < team.size(); ++i)
  //  cout << team[i] << endl;
  
  cout << "Reference m2LnQ from [nsig = 0] ensemble = " << ref << "\n";
  cout << "nsig\tp-value\n";
  for (size_t i = 0; i < team.size(); ++i)
    cout << team[i].nsig << "\t"
         << team[i].pval << endl;
  
  return team;
}

typedef map<string, TObject*> HistMap;

// forward declaration
void doGraphics(TString fname);

// fit of samples of signal and background with 2D function
// statistical calculations
// ...
void fit2D(const char* strMW,
           const char* strMN,
           const char* strCh,
           int nSamples = 1000, // number of pseudo-experiments for limits and significance calculation
           float syst = 0)      // value of systematic error for limits calculation
{
  // names of datafiles
  const TString signal_name = TString("dstsign") + strMW + "-" + strMN + strCh + ".d";
  const TString background_name = TString("dstbg-") + strCh + ".d";
  
  // === histogramming ===============================================
  HistMap hm;
  
  // all new histograms will automatically activate the storage of the sum of squares of errors
  TH1::SetDefaultSumw2(kTRUE);
  
  // decrease level of verbosity
  RooMsgService::instance().setGlobalKillBelow(PROGRESS);
  
  /// === observables ================================================
  RooRealVar MW("MW", "M(W_{R})", 500, 3000, "GeV/c^{2}"); MW.setBins(25);
  RooRealVar MN("MN", "M(#nu_{R})", 0, 2500, "GeV/c^{2}"); MN.setBins(25);
  RooRealVar NoW("NoW", "M(#nu_{R})/M(W_{R})", 0, 1, "");  NoW.setBins(25);
  
  /// === Data definition ==============================================
  RooRealVar weight("weight", "weight", 1);
  
  // signal
  TTree stree(signal_name, signal_name);
  stree.ReadFile(signal_name, "MW/D:MN/D:NoW/D:weight/D");
  // temporary fix for bug with import of weighted trees
  // (http://root.cern.ch/phpBB2/viewtopic.php?t=9535)
  RooDataSet stmp("stmp", "stmp", RooArgSet(MW, MN, NoW, weight), Import(stree));
  RooDataSet sdata(signal_name, signal_name,
                   RooArgSet(MW, MN, NoW, weight),
                   Import(stmp), WeightVar("weight"));
  cout << "signal: " << sdata.GetTitle()
       << ", sumEntries = " << sdata.sumEntries()
       << ", numEntries = " << sdata.numEntries()
       << endl;
  
  // background
  TTree btree(background_name, background_name);
  btree.ReadFile(background_name, "MW/D:MN/D:NoW/D:weight/D");
  RooDataSet btmp("btmp", "btmp", RooArgSet(MW, MN, NoW, weight), Import(btree));
  RooDataSet bdata(background_name, background_name,
                   RooArgSet(MW, MN, NoW, weight),
                   Import(btmp), WeightVar("weight"));
  cout << "background: " << bdata.GetTitle()
       << ", sumEntries = " << bdata.sumEntries()
       << ", numEntries = " << bdata.numEntries()
       << endl;
  
  // mix (signal + background)
  // temporary fix for bug with merging of weighted datasets:
  {
    ifstream f1(signal_name);
    ifstream f2(background_name);
    ofstream f("mix.d");
    string line;
    
    while (getline(f1, line)) f << line << "\n";
    while (getline(f2, line)) f << line << "\n";
  }
  TTree tree("mix.d", "mix.d");
  tree.ReadFile("mix.d", "MW/D:MN/D:NoW/D:weight/D");
  RooDataSet tmp("tmp", "tmp", RooArgSet(MW, MN, NoW, weight), Import(tree));
  RooDataSet data("mix.d", "mix.d",
                   RooArgSet(MW, MN, NoW, weight),
                   Import(tmp), WeightVar("weight"));
  cout << "mix: " << data.GetTitle()
       << ", sumEntries = " << data.sumEntries()
       << ", numEntries = " << data.numEntries()
       << endl;
  
  //RooDataSet data("data", (TString(sdata.GetTitle()) + bdata.GetTitle()).Data(),
  //                 RooArgSet(MW, MN, NoW, weight),
  //                 Import(stmp), WeightVar("weight"));
  //data.append(btmp);
  //RooDataSet data(sdata, "data");
  //data.SetTitle((TString(sdata.GetTitle()) + bdata.GetTitle()).Data());
  //data.append(bdata);
  //data->setWeightVar(weight);
  
  hm["hNoW_MWs"] = new TH2F("hNoW_MWs", "Signal;MW [GeV/c^{2}];NoW", MW.getBins(), MW.getMin(), MW.getMax(), NoW.getBins(), NoW.getMin(), NoW.getMax());
  hm["hNoW_MWb"] = new TH2F("hNoW_MWb", "Background;MW [GeV/c^{2}];NoW", MW.getBins(), MW.getMin(), MW.getMax(), NoW.getBins(), NoW.getMin(), NoW.getMax());
  hm["hNoW_MW"] = new TH2F("hNoW_MW", "Mix;MW [GeV/c^{2}];NoW", MW.getBins(), MW.getMin(), MW.getMax(), NoW.getBins(), NoW.getMin(), NoW.getMax());
  Draw(&sdata, "NoW:MW >> hNoW_MWs", "weight");
  Draw(&bdata, "NoW:MW >> hNoW_MWb", "weight");
  Draw( &data, "NoW:MW >> hNoW_MW", "weight");
  
  /// === Model definition ==========================================
  // - signal -
  // BW x BW
  RooRealVar meanW("meanW", "meanW", 1500, 900, 4000);
  RooRealVar widthW("widthW", "widthW", 100, 10, 1000);
  RooBreitWigner signalW("signalW", "signalW", MW, meanW, widthW);
  RooRealVar meanNoW("meanNoW", "meanNoW", 0.5, 0, 10);
  RooRealVar widthNoW("widthNoW", "widthNoW", 0.1, 0, 10);
  RooBreitWigner signalNoW("signalNoW", "signalNoW", NoW, meanNoW, widthNoW);
  RooProdPdf signal("sig_BWxBW", "sig_BWxBW", RooArgSet(signalW, signalNoW));
  
  // - background -
  // keys
  //RooNDKeysPdf KEYS("bkg_KEYS", "bkg_KEYS", MW, NoW, bdata);
  RooKeysPdf KEYS("bkg_KEYS", "bkg_KEYS", NoW, bdata);
  //adding artificial exponent to background
  RooRealVar coef("coef", "coef", 0., -1., 1.);
  RooExponential exp("exp", "exp", MW, coef);
  RooProdPdf background("bkg_KEYS_exp", "bkg_KEYS_exp", RooArgSet(KEYS, exp));
  
  // - model -
  RooRealVar nsig("nsig", "number of signal events", 10, 0, 1000);
  RooRealVar nbkg("nbkg", "number of background events", 10, 0, 1000);
  RooAddPdf model("model",  "sig_BWxBW + bkg_KEYS", RooArgList(signal,  background), RooArgList(nsig, nbkg));

  // separate models for signal/background only
  RooExtendPdf sigModel("sigModel", "sigModel", signal, nsig);
  RooExtendPdf bkgModel("bkgModel", "bkgModel", background, nbkg);
  
  /// === Fit to data ==============================================
  RooLinkedList fitOpt;
  fitOpt.Add((TObject*) new RooCmdArg(Minos(kFALSE)));
  fitOpt.Add((TObject*) new RooCmdArg(PrintLevel(-1)));
  fitOpt.Add((TObject*) new RooCmdArg(Save(kTRUE)));
  fitOpt.Add((TObject*) new RooCmdArg(Extended(kTRUE)));
  fitOpt.Add((TObject*) new RooCmdArg(SumW2Error(kTRUE)));
  
  //putting  observables (1D pictures) and fit results into plots
  TCanvas* cc = new TCanvas("ratio", "ratio", 1200, 900);
  cc->Divide(3, 2);
  RooPlot* xframe1= MW.frame();
  RooPlot* xframe2= MW.frame();
  RooPlot* xframe3= MW.frame();
  RooPlot* xframe4= NoW.frame();
  RooPlot* xframe5= NoW.frame();
  RooPlot* xframe6= NoW.frame();
  
  // fit signal model to signal data
  sigModel.fitTo(sdata, fitOpt)->Print();
  // prepare histogram
  TH1* h1 = sigModel.createHistogram(sigModel.GetTitle(), MW, YVar(NoW));
  h1->SetName("hsigmodel");
  h1->Scale(sigModel.expectedEvents(RooArgSet())/h1->Integral()); // rescale to number of events
  hm["hsigmodel"] = h1;
  
  sdata.plotOn(xframe1, DataError(RooAbsData::SumW2));
  sigModel.plotOn(xframe1);  
  sdata.plotOn(xframe4, DataError(RooAbsData::SumW2));
  sigModel.plotOn(xframe4); 
  cc->cd(1); xframe1->Draw();
  cc->cd(4); xframe4->Draw();
  
  // fit background model to background data
  bkgModel.fitTo(bdata, fitOpt)->Print();
  // prepare histogram
  TH1* h2 = bkgModel.createHistogram(bkgModel.GetTitle(), MW, YVar(NoW));
  h2->SetName("hbkgmodel");
  h2->Scale(bkgModel.expectedEvents(RooArgSet())/h2->Integral()); // rescale to number of events
  hm["hbkgmodel"] = h2;
  
  bdata.plotOn(xframe2, DataError(RooAbsData::SumW2));
  bkgModel.plotOn(xframe2);   
  bdata.plotOn(xframe5, DataError(RooAbsData::SumW2));
  bkgModel.plotOn(xframe5);  
  cc->cd(2); xframe2->Draw();
  cc->cd(5); xframe5->Draw();

  // fit model to data
  model.fitTo(data, fitOpt)->Print();
  // create histogram from fit function (from model)
  TH1* hmodel = model.createHistogram(model.GetTitle(), MW, YVar(NoW));
  hmodel->SetName("hmodel");
  hmodel->Scale(model.expectedEvents(RooArgSet())/hmodel->Integral()); // rescale to number of events
  hm["hmodel"] = hmodel;
  
  data.plotOn(xframe3, DataError(RooAbsData::SumW2));
  model.plotOn(xframe3);  
  data.plotOn(xframe6, DataError(RooAbsData::SumW2));
  model.plotOn(xframe6);
  cc->cd(3); xframe3->Draw();
  cc->cd(6); xframe6->Draw();

  gROOT->SetStyle("Plain");
  gStyle->SetPalette(1);
  cc->Print(".eps");
  delete cc;
  
  /// === Toy MC ==================================================
  const bool calcLimit = true;
  const bool calcSignificance = false;
  
  // limit calculation
  if (calcLimit) {
    // preserve fit values of nsig and nbkg
    RooRealVar nsig2(nsig);
    RooRealVar nbkg2(nbkg);
    
    meanW.setConstant(kTRUE);
    meanNoW.setConstant(kTRUE);
    widthW.setConstant(kTRUE);
    widthNoW.setConstant(kTRUE);
    RooExtendPdf bkg("bkg", "bkg", background, nbkg2);
    const vector<Ensemble> team = LimitCalc(RooArgSet(MW, NoW), model, nsig, bkg, nSamples, syst);
    
    // restore nsig and nbkg
    nsig.setVal(nsig2.getVal()); nsig.setError(nsig2.getError());
    nbkg.setVal(nbkg2.getVal()); nbkg.setError(nbkg2.getError());
    
    // prepare plots
    float x[team.size()];
    float y[team.size()];
    float ey[team.size()];
    for (size_t i = 0; i < team.size(); ++i) {
      x[i] = team[i].nsig;
      y[i] = team[i].pval;
      ey[i] = y[i]/sqrt(team[i].m2LnQ.size());
    }
    
    TGraphErrors* grPval = new TGraphErrors(team.size(), x, y, 0, ey);
    //TGraph* grPval = new TGraph(team.size(), x, y);
    grPval->SetNameTitle("grPval", "P-value;Nsig;p-val");
    hm["grPval"] = grPval;
  }
  
  // significance
  if (calcSignificance) {
    meanW.setConstant(kFALSE);
    meanNoW.setConstant(kFALSE);
    widthW.setConstant(kTRUE);
    widthNoW.setConstant(kTRUE);
    RooMCStudy toy(model, RooArgSet(MW, NoW), Extended(kTRUE), FitOptions(Minos(kFALSE), Extended(kTRUE), PrintLevel(-1)));
    RooDLLSignificanceMCSModule DLLModule(nsig);
    toy.addModule(DLLModule);
    toy.generateAndFit(nSamples, 0, kTRUE);
    
    // pseudo-experiments
    hm["h1NoW_MW"] = new TH2I("h1NoW_MW", "Pseudo-experiment #1;MW [GeV/c^{2}];NoW", 25, 500, 3000, 25, 0, 1);
    hm["h2NoW_MW"] = new TH2I("h2NoW_MW", "Pseudo-experiment #2;MW [GeV/c^{2}];NoW", 25, 500, 3000, 25, 0, 1);
    hm["h3NoW_MW"] = new TH2I("h3NoW_MW", "Pseudo-experiment #3;MW [GeV/c^{2}];NoW", 25, 500, 3000, 25, 0, 1);
    Draw(toy.genData(0), "NoW:MW >> h1NoW_MW");
    Draw(toy.genData(1), "NoW:MW >> h2NoW_MW");
    Draw(toy.genData(2), "NoW:MW >> h3NoW_MW");
    
    // === field available in toy.fitParDataSet() TTree:
    // coef,coeferr,coefpull,
    // meanNoW,meanNoWerr,meanNoWpull,
    // meanW,meanWerr,meanWpull,
    // nbkg,nbkgerr,nbkgpull,
    // nsig,nsigerr,nsigpull,
    // widthNoW,widthNoWerr,widthNoWpull,
    // widthW,widthWerr,widthWpull,
    // NLL,
    // ngen,
    // nll_nullhypo_nsig,dll_nullhypo_nsig,significance_nullhypo_nsig

    hm["hToyNLL"] = new TH1D("hToyNLL", "hToyNLL", 100, -500, 500);
    hm["hToyMeanW"] = new TH1D("hToyMeanW", "hToyMeanW", 30, 500, 2000);
    Draw(&toy.fitParDataSet(), "NLL >> hToyNLL");
    Draw(&toy.fitParDataSet(), "meanW >> hToyMeanW");
    
    // hm["toyErrorW"] = toy.plotError(meanW)->Draw();
    // hm["toyPullW"] = toy.plotPull(meanW, FrameRange(-10, 10))->Draw();
    
    // hm["toyMeanNoW"] = toy.plotParam(meanNoW)->Draw();
    // hm["toyErrorNoW"] = toy.plotError(meanNoW)->Draw();
    // hm["toyPullNoW"] = toy.plotPull(meanNoW, FrameRange(-10, 10))->Draw();
    
    // hm["toyParamNsig"] = toy.plotParam(nsig)->Draw();
    // hm["toyErrorNsig"] = toy.plotError(nsig)->Draw();
    // hm["toyPullNsig"] = toy.plotPull(nsig, FrameRange(-10, 10))->Draw();
    
    // hm["toyParamNbkg"] = toy.plotParam(nbkg)->Draw();
    // hm["toyErrorNbkg"] = toy.plotError(nbkg)->Draw();
    // hm["toyPullNbkg"] = toy.plotPull(nbkg, FrameRange(-10, 10))->Draw();
    
    // significance, as given in CMS PTDR-2, Appendix A.
    hm["hS_c1"] = new TH1I("hS_c1", "hS_c1", 20, 0, 10);
    hm["hS_L"] = new TH1I("hS_L", "sqrt(2 Ln(Lsb/Lb))", 40, 0, 20);
    hm["hS_cL"] = new TH1I("hS_cL", "hS_cL", 20, 0, 10);
    hm["hS_c12"] = new TH1I("hS_c12", "hS_c12", 20, 0, 10);
    hm["hS_nsig"] = new TH1I("hS_nsig", "nsig/nsig_err", 20, 0, 10);
    Draw(&toy.fitParDataSet(), "nsig/sqrt(nbkg) >> hS_c1");
    Draw(&toy.fitParDataSet(), "significance_nullhypo_nsig >> hS_L");
    Draw(&toy.fitParDataSet(), "sqrt(2*((nsig + nbkg)*log(1 + nsig/nbkg) - nsig)) >> hS_cL");
    Draw(&toy.fitParDataSet(), "2*(sqrt(nsig + nbkg) - sqrt(nbkg)) >> hS_c12");
    Draw(&toy.fitParDataSet(), "nsig/nsig_err >> hS_nsig");
  }
  
  // === Write histograms to .root file ==============================
  const TString fname = "fit-" + signal_name + ".root";
  TFile f(fname, "RECREATE");
  for (HistMap::iterator i = hm.begin(); i != hm.end(); ++i)
    i->second->Write();
  f.Close();
  
  // plot *.eps files with histograms
  doGraphics(fname);
  
  cout << "\n"
       << "All histograms written to: " << fname << "\n"
       << "If you want to re-do figures type:\n"
       << "  doGraphics(\"" << fname + "\")" << endl;
}

void doGraphics(TString fname)
{
  /// === Draws ====================================================
  // all new histograms will automatically activate the storage of the sum of squares of errors
  TH1::SetDefaultSumw2(kTRUE);
  
  //setTDRStyle();
  gROOT->SetStyle("Plain");
  gStyle->SetPalette(1);
  gStyle->SetPadLeftMargin(0.15);
  gStyle->SetTitleOffset(1.5, "y");
  gStyle->SetTitleOffset(0.9, "x");
  gStyle->SetTitleSize(0.05, "xyz");
  gStyle->SetOptStat("nemr");
  gROOT->ForceStyle();
  
  TFile f(fname);
  // data:
  TH2F* hNoW_MW =  (TH2F*) f.Get("hNoW_MW");  // sig + bkg
  TH2F* hNoW_MWs = (TH2F*) f.Get("hNoW_MWs"); // signal
  TH2F* hNoW_MWb = (TH2F*) f.Get("hNoW_MWb"); // background
  // fit to data:
  TH2F* hsigmodel = (TH2F*) f.Get("hsigmodel");
  TH2F* hbkgmodel = (TH2F*) f.Get("hbkgmodel");
  TH2F* hmodel =    (TH2F*) f.Get("hmodel");
  
  // toy MC (significance):
  TH1* hToyNLL =   (TH1*) f.Get("hToyNLL");
  TH1* hToyMeanW = (TH1*) f.Get("hToyMeanW");
  TH2F* h1NoW_MW = (TH2F*) f.Get("h1NoW_MW");
  TH2F* h2NoW_MW = (TH2F*) f.Get("h2NoW_MW");
  TH2F* h3NoW_MW = (TH2F*) f.Get("h3NoW_MW");
  TH1* hS_L    = (TH1*) f.Get("hS_L");
  TH1* hS_nsig = (TH1*) f.Get("hS_nsig");
  TH1* hS_c1   = (TH1*) f.Get("hS_c1");
  TH1* hS_cL   = (TH1*) f.Get("hS_cL");
  TH1* hS_c12  = (TH1*) f.Get("hS_c12");
  
  // limit:
  TGraphErrors* grPval = (TGraphErrors*) f.Get("grPval");
  
  // watermark
  TText mark(0.5, 0.5, "CMS preliminary");
  mark.SetNDC(kTRUE);
  mark.SetTextColor(17);
  mark.SetTextAlign(22); // centered
  mark.SetTextSize(0.10);
  mark.SetTextAngle(45);

  // do plots:
  TCanvas c(fname, fname, 800, 600);
  c.Print(".eps[");
  
  // === data & fit =============================================
  // data
  c.Divide(3, 2);
  hNoW_MWs->SetLineColor(kRed);
  hNoW_MWb->SetLineColor(kBlue);
  c.cd(1); hNoW_MWs->Draw("box");
  c.cd(2); hNoW_MWb->Draw("box");
  c.cd(3); hNoW_MW->Draw("box");
  c.cd(4); hNoW_MWs->Draw("contz");
  c.cd(5); hNoW_MWb->Draw("contz");
  c.cd(6); hNoW_MW->Draw("contz");
  c.Print(".eps");
  
  c.cd(1); hNoW_MWs->Draw("lego fb");
  c.cd(2); hNoW_MWb->Draw("lego fb");
  c.cd(3); hNoW_MW->Draw("lego fb");
  c.cd(4)->Clear();
  c.cd(5)->Clear();
  c.cd(6)->Clear();
  c.Print(".eps");

  // fit
  c.Clear();
  c.Divide(2, 2);
  c.cd(1); hNoW_MW->Draw("lego fb");
  c.cd(2); hNoW_MW->Draw("contz");
  c.cd(3); hmodel->Draw("lego fb");
  c.cd(4); hmodel->Draw("contz");
  c.Print(".eps");
  
  // comparison plots of initial data distribution and result of fit
  // mc vs fit
  c.Clear();
  c.Divide(2, 1);
  gStyle->SetOptStat("");
  hNoW_MW->SetTitle("MC data;M_{W_{R}}, GeV/c^{2};M_{#nu_{R}}, GeV/c^{2}");
  // as two contour plots
  c.cd(1); hNoW_MW->Draw("contz"); mark.Draw();
  hmodel->SetTitle("Fit;M_{W_{R}}, GeV/c^{2};M_{#nu_{R}}, GeV/c^{2}");
  c.cd(2); hmodel->Draw("contz"); mark.Draw();
  gStyle->SetOptStat("nemr");
  c.Print(".eps");
  
  // as projections
  {
  c.cd(1);
  TH1D* mcW = hNoW_MW->ProjectionX("mcW", 0, -1, "e");
        mcW->SetLineColor(kBlue);
        mcW->SetOption("e1");
  TH1D* fitW = hmodel->ProjectionX("fitW", 0, -1);
        fitW->SetLineColor(kRed);
  THStack s1("s1", "MC Data vs Fit (W_{R} mass);M_{W_{R}}, GeV/c^{2}");
  s1.Add(mcW);
  s1.Add(fitW);
  s1.Draw("nostack");
  TLegend leg1(0.6, 0.8, 0.89, 0.89);
  leg1.SetFillColor(0);
  leg1.SetTextSize(0.04);
  leg1.AddEntry(mcW, "MC Data", "L");
  leg1.AddEntry(fitW, "Fit", "L");
  leg1.Draw();
  mark.Draw();

  c.cd(2);
  TH1D* mcN = hNoW_MW->ProjectionY("mcN", 0, -1, "e");
        mcN->SetLineColor(kBlue);
        mcN->SetOption("e1");
  TH1D* fitN = hmodel->ProjectionY("fitN", 0, -1);
        fitN->SetLineColor(kRed);
  THStack s2("s2", "MC Data vs Fit (#nu_{R} mass);M_{#nu_{R}}, GeV/c^{2}");
  s2.Add(mcN);
  s2.Add(fitN);
  s2.Draw("nostack");
  TLegend leg2(0.6, 0.8, 0.89, 0.89);
  leg2.SetFillColor(0);
  leg2.SetTextSize(0.04);
  leg2.AddEntry(mcN, "MC Data", "L");
  leg2.AddEntry(fitN, "Fit", "L");
  leg2.Draw();
  
  mark.Draw();
  // mc_vs_fit-projections
  c.Print(".eps");
  }
  
  // as projections (signal)
  {
  c.cd(1);
  TH1D* mcW = hNoW_MWs->ProjectionX("mcW", 0, -1, "e");
        mcW->SetLineColor(kBlue);
        mcW->SetOption("e1");
  TH1D* fitW = hsigmodel->ProjectionX("fitW", 0, -1);
        fitW->SetLineColor(kRed);
  THStack s1("s1", "Signal: MC Data vs Fit (W_{R} mass);M_{W_{R}}, GeV/c^{2}");
  s1.Add(mcW);
  s1.Add(fitW);
  s1.Draw("nostack");
  TLegend leg1(0.6, 0.8, 0.89, 0.89);
  leg1.SetFillColor(0);
  leg1.SetTextSize(0.04);
  leg1.AddEntry(mcW, "MC Data", "L");
  leg1.AddEntry(fitW, "Fit", "L");
  leg1.Draw();
  mark.Draw();

  c.cd(2);
  TH1D* mcN = hNoW_MWs->ProjectionY("mcN", 0, -1, "e");
        mcN->SetLineColor(kBlue);
        mcN->SetOption("e1");
  TH1D* fitN = hsigmodel->ProjectionY("fitN", 0, -1);
        fitN->SetLineColor(kRed);
  THStack s2("s2", "Signal: MC Data vs Fit (#nu_{R} mass);M_{#nu_{R}}, GeV/c^{2}");
  s2.Add(mcN);
  s2.Add(fitN);
  s2.Draw("nostack");
  TLegend leg2(0.6, 0.8, 0.89, 0.89);
  leg2.SetFillColor(0);
  leg2.SetTextSize(0.04);
  leg2.AddEntry(mcN, "MC Data", "L");
  leg2.AddEntry(fitN, "Fit", "L");
  leg2.Draw();
  
  mark.Draw();
  // mc_vs_fit-projections-signal
  c.Print(".eps");
  }
  
  // as projections (background)
  {
  c.cd(1);
  TH1D* mcW = hNoW_MWb->ProjectionX("mcW", 0, -1, "e");
        mcW->SetLineColor(kBlue);
        mcW->SetOption("e1");
  TH1D* fitW = hbkgmodel->ProjectionX("fitW", 0, -1);
        fitW->SetLineColor(kRed);
  THStack s1("s1", "Background: MC Data vs Fit (W_{R} mass);M_{W_{R}}, GeV/c^{2}");
  s1.Add(mcW);
  s1.Add(fitW);
  s1.Draw("nostack");
  TLegend leg1(0.6, 0.8, 0.89, 0.89);
  leg1.SetFillColor(0);
  leg1.SetTextSize(0.04);
  leg1.AddEntry(mcW, "MC Data", "L");
  leg1.AddEntry(fitW, "Fit", "L");
  leg1.Draw();
  mark.Draw();

  c.cd(2);
  TH1D* mcN = hNoW_MWb->ProjectionY("mcN", 0, -1, "e");
        mcN->SetLineColor(kBlue);
        mcN->SetOption("e1");
  TH1D* fitN = hbkgmodel->ProjectionY("fitN", 0, -1);
        fitN->SetLineColor(kRed);
  THStack s2("s2", "Background: MC Data vs Fit (#nu_{R} mass);M_{#nu_{R}}, GeV/c^{2}");
  s2.Add(mcN);
  s2.Add(fitN);
  s2.Draw("nostack");
  TLegend leg2(0.6, 0.8, 0.89, 0.89);
  leg2.SetFillColor(0);
  leg2.SetTextSize(0.04);
  leg2.AddEntry(mcN, "MC Data", "L");
  leg2.AddEntry(fitN, "Fit", "L");
  leg2.Draw();
  
  mark.Draw();
  // mc_vs_fit-projections-background
  c.Print(".eps");
  }
  
  // === significance calculation pseudo-experiments =====================
  if (hToyNLL == NULL) goto skip1;
  
  // lego-plots of pseudo-experiments
  c.Clear();
  c.Divide(2, 2);
  c.cd(1); h1NoW_MW->Draw("lego fb");
  c.cd(2); h2NoW_MW->Draw("lego fb");
  c.cd(3); h3NoW_MW->Draw("lego fb");
  c.Print(".eps");
  
  // significance
  c.Clear();
  c.Divide(2, 3);
  c.cd(1); hS_c1->Draw();
  c.cd(2); hS_L->Draw();
  c.cd(3); hS_cL->Draw();
  c.cd(4); hS_c12->Draw();
  c.cd(5)->Clear();
  c.cd(6); hS_nsig->Draw();
  c.Print(".eps");
  
  // -log(L), meanW distributions of toy MC
  // TODO: meanNoW, nsig, nbkg
  c.Clear();
  c.Divide(2, 2);
  c.cd(1); hToyNLL->Draw();
  c.cd(2); hToyMeanW->Draw();
  c.Print(".eps");
  
  // Significance
  c.Clear();
  hS_L->SetTitle("Significance");
  hS_L->Draw();
  c.Print(".eps");
  
skip1:
  
  // === limit =============================================
  if (grPval == NULL) goto skip2;
  
  c.Clear();
  grPval->Draw("A*");
  {
    TF1* fitf = new TF1("fitf", "1 - exp(-[1]*(x - [0]))" , 1.5, 50);
    fitf->SetParameters(0, 1);
    grPval->Fit(fitf, "R");
    cout << "nsig(p-val=0.95) = " << fitf->GetX(0.95) << endl;
  }
  c.Print(".eps");
  
skip2:
  
  // finalize printing
  c.Print(".eps]");
}

int main(int argc, char** argv)
{
  if (argc < 6) {
    cout << "Usage: " << argv[0] << " [MW] [MN] [Ch] [ntoys] [syst]" << endl;
    return 1;
  }
  
  const char* mw = argv[1];
  const char* mn = argv[2];
  const char* ch = argv[3];
  int ntoys = atoi(argv[4]);
  float syst = atof(argv[5]);
  
  cout << "Sample: " << mw << " " << mn << " " << ch << " | "
       << "ntoys = " << ntoys << " syst = " << syst
       << endl;
  
  fit2D(mw, mn, ch, ntoys, syst);
}
