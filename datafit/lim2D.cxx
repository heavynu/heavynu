// utility to draw exclusion plot
// to run do:
//   $ cd CMSSW/src/heavynu/datafit
//   $ root -b -q -l 'lim2D.cxx+'

#include "TROOT.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TGraph2D.h"
#include "TAxis.h"
#include "TList.h"
#include "TGraph.h"
#include "TLine.h"
#include "TLatex.h"

#include "../sampledb.h"
#include "../examples/tdrstyle.C"

#include <vector>
#include <algorithm>
using namespace std;

struct XYZ {
  int x, y;
  float z;
  
  bool operator == (const XYZ& v) const
  {
    return (x == v.x) && (y == v.y);
  }
};

struct LimitsResults {
  TGraph2D* limit_gr;  // limit crosssection
  TGraph2D* xs_gr;     // signal crossection
  TGraph2D* diff_gr;   // xs_gr - limit_gr
  TGraph* cont0;       // contour of zero level of diff_gr
};

LimitsResults calcLimits(const char* fname)
{
  // === read limits:
  TGraph2D* limit_gr = new TGraph2D(fname);
  limit_gr->SetNameTitle("limit", "limit;WR;nuR;#sigma");
  
  // === read cross-sections:
  // select latest signal samples with ECM = 7 TeV:
  const SampleDB sig7 = SampleDB().find("type", 1).find("ECM", 7).find("AlCa", "START38_V14::All");
  
  // remove dublicates:
  vector<XYZ> ps;
  for (int i = 0; i < sig7.size(); ++i) {
    const XYZ p = {sig7[i].MW, sig7[i].MNu, sig7[i].CS};
    if (find(ps.begin(), ps.end(), p) == ps.end())
      ps.push_back(p);
  }
  
  // and fill graph:
  TGraph2D* xs_gr = new TGraph2D(ps.size());
  xs_gr->SetNameTitle("xs", "xs;WR;nuR;#sigma");
  
  for (size_t i = 0; i < ps.size(); ++i) {
    xs_gr->SetPoint(i, ps[i].x, ps[i].y, ps[i].z);
  }
  
  cout << "xs: WR = " << xs_gr->GetXmin() << " - " << xs_gr->GetXmax()
       << " NuR = "   << xs_gr->GetYmin() << " - " << xs_gr->GetYmax()
       << endl;
  
  cout << "lim: WR = " << limit_gr->GetXmin() << " - " << limit_gr->GetXmax()
       << " NuR = "    << limit_gr->GetYmin() << " - " << limit_gr->GetYmax()
       << endl;
  
  // === calc difference:
  TGraph2D* diff_gr = new TGraph2D();
  diff_gr->SetNameTitle("diff", "xs;WR;nuR;#sigma");
  
  const float xmin = max(limit_gr->GetXmin(), xs_gr->GetXmin());
  const float xmax = min(limit_gr->GetXmax(), xs_gr->GetXmax());
  const float ymin = max(limit_gr->GetYmin(), xs_gr->GetYmin());
  const float ymax = min(limit_gr->GetYmax(), xs_gr->GetYmax());
  
  int i = 0;
  for (float wr = xmin; wr < xmax + 1; wr += (xmax - xmin)/40.) {
    for (float nur = ymin; nur < ymax + 1; nur += (ymax - ymin)/40.) {
      const float diff = xs_gr->Interpolate(wr, nur) - limit_gr->Interpolate(wr, nur);
      diff_gr->SetPoint(i, wr, nur, diff);
      i++;
    }
  }
  
  //diff_gr->Draw("surf1");
  //TList* list = el.diff_gr->GetContourList(0);
  //TGraph* cont0 = (TGraph*) list->At(0);
  
  const LimitsResults res = {limit_gr, xs_gr, diff_gr, 0};
  return res;
}

void lim2D()
{
  gROOT->SetStyle("Plain");
  gStyle->SetPalette(1);
  setTDRStyle();
  
  TCanvas* c = new TCanvas("xxx", "xxx", 800, 800);
  c->Divide(2, 2);
  
  //const LimitsResults el = calcLimits("Data_CS_Up_Lim_E_All.txt");
  const LimitsResults el = calcLimits("Data_CS_Up_Lim_E_All_11.txt");
  //const LimitsResults el = calcLimits("Data_CS_Up_Lim_Mu_All.txt");
  
  // =========== plot limits:
  c->cd(1);
  el.limit_gr->Draw("surf1");
  
  // =========== plot crossection:
  c->cd(2);
  el.xs_gr->DrawClone("surf1");
  //el.xs_gr->Draw("pcol");
  
  c->cd(3);
  el.xs_gr->GetXaxis()->SetRangeUser(1000, 1300);
  el.xs_gr->Draw("surf1");
  
  
  // =========== plot difference:
  c->cd(4);
  el.diff_gr->Draw("surf1");
  //el.diff_gr->Draw("CONT");
  c->Print("lim2D.ps");
  
  // uncomment `return` if you want to see source plots:
  //return;
  
  // ============ plot contour:
  c->Clear();
  c->SetLeftMargin(0.20);
  c->SetRightMargin(0.05);
  gStyle->SetTitleOffset(1.5, "Y");
  
  TList* list = el.diff_gr->GetContourList(0.01);
  TGraph* cont0 = (TGraph*) list->At(0);
  cont0->SetNameTitle("limit-cont", "Exclusion mass region;M_{W}, GeV;M_{#nu}, GeV");
  cont0->GetXaxis()->SetLimits(650, 1800);
  cont0->GetYaxis()->SetRangeUser(0, 1200);
  
  // show exclusion zone
  cont0->SetLineWidth(+ 4 + 30*100);
  cont0->SetLineColor(kRed - 3);
  cont0->SetFillStyle(3004);
  cont0->SetFillColor(kRed - 4);
  cont0->Draw("AC");
  
  // ============== plot region M_nuR > M_WR:
  TLine line;
  line.SetLineWidth(2);
  line.SetLineStyle(2);
  line.DrawLine(650., 650., 1200., 1200.);
  
  // ============== plot Tevatron limit region:
  const float tvt_x[] = {760, 760};
  const float tvt_y[] = {0, 1200};
  TGraph* txt_gr = new TGraph(2, tvt_x, tvt_y);
  
  txt_gr->SetLineWidth(2 + 15*100);
  txt_gr->SetFillStyle(3004);
  txt_gr->Draw("L");
  
  // ============= plot labels:
  TLatex text;
  text.SetTextSize(0.05);
  text.DrawLatex(770, 1090, "M_{#nu} > M_{W}");
  
  text.SetTextSize(0.04);
  text.SetTextAngle(90);
  text.DrawLatex(750, 50, "Excluded by Tevatron");
  
  c->Print("lim2D-cont0.pdf");
  c->Print("lim2D-cont0.png");
}
