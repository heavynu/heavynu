// To prepare plots do:
//   $ root -b -q -l limit.cxx

#include "TROOT.h"

// calc extrapolation factor
float rescale(const int ECM,        // beam energy, TeV
              const float mwr,      // mass of WR, GeV
              const float mnr,      // mass of NR, GeV
              const float nsig95,   // nsig for which CL = 95%
              const float nsig_org, // nsig from signal sample
              const float syst = 0) // value of systematic uncertancy (to be taken into account by 'valenki' method)
{
  // algorithm example:
  // nsign = gR/gL * nsign-nominal
  // rescale points in such a way, that gR/gL == const
  // gR proportional to xsection
  // algorithm for point 2000 - 500:
  //  find xsection of 2TeV WR (fig. 2 in note) = ~ 2 pb
  //  take into account systematics by 'valenki' method:
  //  our systematics uncertancy ~ 15%
  //  nsig_original_syst = 0.85*7.93898 = 6.748
  //  calc rescale factor = nsig/nsig_nominal_syst = 0.75/6.748 = 0.1111
  //  find WR mass for which xsection = 2 pb * 0.1111 = 0.222 pb  @  3 TeV
  //  rescale mass of NR in the proportion as WR rescaled:
  //  NR = 500 * (3 / 2) = 750 GeV
  // finally we have:
  // 2000 500  (2 pb) ->  3000 750  (0.222 pb)
  
  
  // The dependence of the cross section sigma(pp -> WR) on the WR mass
  // from CMS AN-2009/112
  // WR mass in GeV
  Double_t WR[] = {1000, 1500, 2000, , 3000, , 4000};
  // corresponding crossection in pb (values given in powers of e)
  Double_t cs14[5] = {3.66612, 1.97852, 0.63074, -1.54787, -3.43176};   // 14 TeV
  Double_t cs10[5] = {2.94916, 1.08451, -0.448947, -3.06981, -5.16746}; // 10 TeV
  Double_t cs7[4] = {2.07292, -0.0759096, -1.94072, -4.94429};          // 7 TeV
  
  int N;
  Double_t* lnxs;
  switch (ECM) {
    case 14: N = DIM(cs14); lnxs = cs14; break;
    case 10: N = DIM(cs10); lnxs = cs10; break;
    case  7: N =  DIM(cs7); lnxs =  cs7; break;
    default:
      cerr << "ERROR: incorrect beam energy\n"
           << "       ECM = " << ECM << "\n"
           << endl;
      return 0;
      break;
  }
  
  TSpline3 m2lnxs("xsect", WR, lnxs, N); // mass -> crossection
  TSpline3 lnxs2m( "mass", lnxs, WR, N); // crossection -> mass
  
  //  find xsection
  const float xs = exp(m2lnxs.Eval(mwr));
  
  // calc crossection rescale factor
  //  take into account systematics by 'valenki' method:
  const float k = nsig95/(nsig_org*(1 - syst));
  
  // find WR mass for which xsection = k*xs
  const float mwr2 = lnxs2m.Eval(log(k*xs));
  const float f = mwr2/mwr;
  
  cout << "beam = " << ECM << " TeV | "
       << "M_WR/M_NR [GeV]: "
       << mwr  << "/" <<   mnr << " (" <<   xs << " pb) -> "
       << mwr2 << "/" << f*mnr << " (" << k*xs << " pb)" << endl;
  
  return f;
}

// draw exclusion plots
void plotLimit(TCanvas* canvas, TString title,
               TGraph* gr1, char* gr1txt,
               TGraph* gr2, char* gr2txt,
               TGraph* gr3 = 0, char* gr3txt = 0)
{
  TH1F* frame = canvas->DrawFrame(1000, 0, 3000, 2000);
  frame->SetTitle(title + ";M_{W_{R}}, GeV/c^{2};M_{N_{l}}, GeV/c^{2}");
  frame->SetTitleOffset(1.8, "Y");
  frame->GetXaxis()->SetNdivisions(505);
  
  // watermark
  TText* mark = new TText(0.5, 0.5, "CMS preliminary");
  mark->SetNDC(kTRUE);
  mark->SetTextColor(17);
  mark->SetTextAlign(22); // centered
  mark->SetTextSize(0.10);
  mark->SetTextAngle(45);
  mark->Draw();

  // exclusion lines
  TLine line;
  line.SetLineWidth(3);
  line.DrawLine(1000., 1000., 2000., 2000.);
  line.DrawLine(1000., 215., 3000., 215.);

  // 
  TLatex text;
  text.SetTextSize(0.04);
  text.DrawLatex(1100, 1800, "Virtual N");
  text.DrawLatex(1100, 1650, "M_{N_{l}} > M_{W_{R}}");
  text.SetTextSize(0.03);
  text.DrawLatex(2100.,110.,"Excluded by L3 (LEP)");
  
  // limits
  gr1->SetLineColor(kGreen);
  gr1->SetLineWidth(3);
  gr1->Draw("C*");
  
  gr2->SetLineColor(kRed);
  gr2->SetLineWidth(3);
  gr2->Draw("C*");
  
  if (gr3) {
    gr3->SetLineColor(kBlue);
    gr3->SetLineWidth(3);
    gr3->Draw("C*");
  }
  
  // legend
  TLegend* legend = new TLegend(0.6, 0.8, 0.89, 0.89);
  legend->AddEntry(gr1, gr1txt, "L");
  legend->AddEntry(gr2, gr2txt, "L");
  if (gr3) legend->AddEntry(gr3, gr3txt, "L");
  legend->Draw();
}

// all limit are for L = 100pb of integrated luminosity
void limit()
{
  gROOT->SetStyle("Plain");
  TCanvas* canvas = new TCanvas("limit", "limit", 30, 30, 850, 800);
  canvas->SetLeftMargin(0.19); // to get place for Y Axis Title
  canvas->Print(".eps[");
  
  #define DIM(x) (sizeof(x)/sizeof(*x))
  
  // =======================================================================
  // 16x (systematics already taken into account as 15%)
  rescale(14, 2000,  300, 3.1, 3.347);
  rescale(14, 2000,  500, 3.1, 6.32);
  rescale(14, 2000, 1200, 3.5, 2.875);
  rescale(14, 1500, 1100, 3.6, 4.236);
  cout << endl;
  //                             1     2     3     4
  Float_t wr1[] = {1001, 1500, 2031, 2304, 1921, 1555, 1001};
  Float_t nr1[] = { 130,  190,  305,  576, 1153, 1141,  910};
  TGraph* gr1 = new TGraph(DIM(wr1), wr1, nr1);
  
  // 16x (systematics = 15% added during extrapolation by 'valenki' method)
  rescale(14, 2000,  300, 3.2, 3.347, 0.15);
  rescale(14, 2000,  500, 3.4,  6.32, 0.15);
  rescale(14, 2000, 1200, 3.7, 2.875, 0.15);
  rescale(14, 1500, 1100, 3.7, 4.236, 0.15);
  cout << endl;
  //                             1     2     3     4
  Float_t wr2[] = {1001, 1500, 1953, 2192, 1836, 1491, 1001};
  Float_t nr2[] = { 130,  195,  293,  548, 1102, 1093,  910};
  TGraph* gr2 = new TGraph(DIM(wr2), wr2, nr2);
  
  // difference between two methods of taking into account systematics:
  plotLimit(canvas, "Difference between two methods of systematics calculation (14 TeV, sys = 15%, 16x)",
            gr1, "true systematics",
            gr2, "valenki systematics");
  canvas->Print(".eps");
  
  // =======================================================================
  // 22x (systematics already taken into account as 15%)
  // 10 TeV simulation, but by mistake assumed 14 TeV at limit calcualtion
  // still here just for reference
  rescale(14, 1200, 500, 4.9, 29);
  rescale(14, 1200, 700, 5.44, 19);
  rescale(14, 1500, 600, 4.27, 10.6);
  rescale(14, 2000, 500, 3.72, 2.3);
  cout << endl;
  //                                   4     3     1     2
  Float_t wr3[] = {1001, 1250, 1600, 1817, 1820, 1795, 1607, 1001};
  Float_t nr3[] = { 130,  160,  250,  454, 728, 748,  937,  910};
  TGraph* gr3 = new TGraph(DIM(wr3), wr3, nr3);
  
  // 22x (systematics already taken into account as 15%)
  rescale(10, 1200,  500, 4.9, 29);
  rescale(10, 1200,  700, 5.44, 19);
  rescale(10, 1500, 600, 4.27, 10.6);
  rescale(10, 2000, 500, 3.72, 2.3);
  cout << endl;
  //                                   4     3     1     2
  Float_t wr4[] = {1001, 1250, 1600, 1837, 1789, 1724, 1557, 1001};
  Float_t nr4[] = { 130,  160,  250,  459,  716,  718,  908,  910};
  TGraph* gr4 = new TGraph(DIM(wr4), wr4, nr4);
  
  // effect of mistake:
  plotLimit(canvas, "Effect of mistake",
            gr3, "22x (@ 14 TeV by mistake)",
            gr4, "22x (10 TeV)");
  canvas->Print(".eps");
  
  // comparison of 14 TeV and 10 TeV limits:
  //plotLimit(canvas, "comparison of 14 TeV and 10 TeV limits",
  //          gr1, "16x (14 TeV)",
  //          gr4, "22x (10 TeV)");
  //canvas->Print(".eps");
  
  // =======================================================================
  // 31x e channel (no systematics)
  rescale(7, 1200, 300, 3.71994, 8.2271);
  rescale(7, 1200, 500, 4.98846, 13.386);
  rescale(7, 1200, 700, 5.99946, 6.2202);
  rescale(7, 1500, 600, 3.86058, 4.2764);
  cout << endl;
  //                              1     4     2     3
  Float_t wr5[] = {1001,  1190, 1387, 1526, 1434, 1209, 1001};
  Float_t nr5[] = { 130,   200,  347,  610,  598,  705,  910};
  TGraph* gr5 = new TGraph(DIM(wr5), wr5, nr5);
  
  // comparison of 14, 10, 7 TeV limits
  plotLimit(canvas, "comparison of 14, 10, 7 TeV limits",
            gr1, "14 TeV, syst = 15% (16x)",
            gr4, "10 TeV, syst = 15% (22x)",
            gr5, "7 TeV, syst = 0% (31x)");
  canvas->Print(".eps");
  
  // 31x mu channel (no systematics)
  rescale(7, 1200, 300, 3.82835, 7.3709);
  rescale(7, 1200, 500, 5.66715, 10.612);
  rescale(7, 1200, 700, 6.59268, 7.6170);
  rescale(7, 1500, 600, 4.2463, 2.7976);
  cout << endl;
  //                              1     4     2     3
  Float_t wr6[] = {1001,  1190, 1354, 1398, 1347, 1234, 1001};
  Float_t nr6[] = { 130,   200,  339,  559,  561,  720,  910};
  TGraph* gr6 = new TGraph(DIM(wr6), wr6, nr6);
  
  // comparison of 7 TeV limits for e and mu channels
  plotLimit(canvas, "comparison of 7 TeV (syst = 0%, 31x) limits for e and mu channels",
            gr5, "e channel",
            gr6, "mu channel");
  canvas->Print(".eps");
  
  canvas->Print(".eps]");
}
