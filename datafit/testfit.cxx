// ROOT
#include "TROOT.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TGraphErrors.h"
#include "TF1.h"

void testfit()
{
  TH1::SetDefaultSumw2(kTRUE);
  gROOT->SetStyle("Plain");
  
  char* fnames[8] = {
    "fit-dstsign1200-300e.d.root",
    "fit-dstsign1200-300mu.d.root",
    "fit-dstsign1200-500e.d.root",
    "fit-dstsign1200-500mu.d.root",
    "fit-dstsign1200-700e.d.root",
    "fit-dstsign1200-700mu.d.root",
    "fit-dstsign1500-600e.d.root",
    "fit-dstsign1500-600mu.d.root"};
  
  TCanvas c("pval", "pval", 800, 600);
  c.Print(".eps[");
  
  for (int i = 0; i < 8; ++i) {
    TFile f(fnames[i]);
    // skip if file do not opened (do not exists)
    if (!f.IsOpen()) continue;
    
    TGraphErrors* grPval = (TGraphErrors*) f.Get("grPval");
  
    c.Clear();
    grPval->Draw("A*");
    TF1* fitf = new TF1("fitf", "1 - exp(-[1]*(x - [0]))" , 1.5, 50);
    fitf->SetParameters(0, 1, 0.1);
    grPval->Fit(fitf, "R");
    cout << "nsig(p-val=0.95) = " << fitf->GetX(0.95) << endl;
    c.Print(".eps");
  }
  
  c.Print(".eps]");
}
