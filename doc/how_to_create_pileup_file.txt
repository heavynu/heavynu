1. after dumpdata_cfg.py get number run and interval of LumiSection
2. create file MyAnalysisJSON.txt and write in this file {"number run": [[LumiSection_min1, LumiSection_max1]], [[LumiSection_min2, LumiSection_max2]]...}
examples JSON file http://cms-service-dqm.web.cern.ch/cms-service-dqm/CAF/certification/Collisions15/13TeV/ 
For Data 2015 we use Cert_246908-260627_13TeV_PromptReco_Collisions15_25ns_JSON_Silver.txt 
3. download file with PileUp information http://cms-service-dqm.web.cern.ch/cms-service-dqm/CAF/certification/Collisions15/13TeV/PileUp
For Data 2015 we use pileup_JSON_11-19-2015.txt
4. download script pileupCalc.py https://github.com/cms-cvs-history/RecoLuminosity-LumiDB/blob/master/scripts/pileupCalc.py
5. pileupCalc.py -i MyAnalysisJSON.txt --inputLumiJSON pileup_JSON_11-19-2015.txt  --calcMode true --minBiasXsec 69000 --maxPileupBin 50 --numPileupBins 50  MyDataPileupHistogram.root

Reweight 

Option A:

1) Use LumiReweightingStandAlone.h in you analysis code #include "LumiReweightingStandAlone.h"
https://github.com/cms-sw/cmssw/blob/CMSSW_7_4_14/PhysicsTools/Utilities/interface/LumiReweightingStandAlone.h

also in heavynu/dtlisov folder

2) Create MC profile histogramm from set of MC true Number of PV distribution with heavynu/dtlisov/makeMChisoPileup.cc script.
For 2015 Data 25 ns we use:
https://github.com/cms-sw/cmssw/blob/CMSSW_7_4_X/SimGeneral/MixingModule/python/mix_2015_25ns_Startup_PoissonOOTPU_cfi.py


3) Usage in code: 

// Reweighting 
  
  reweight::LumiReWeighting LumiWeights= reweight::LumiReWeighting("MyMCPileupHistogram.root", //MC histo file
                                                                   "MyDataPileupHistogram.root", // Data histo file
                                                                   "MC_pileup", // MC histoname
                                                                   "pileup"); // Data histoname

  LumiWeights.weight(e.pileupIT())

Option B:

1) Use in your code the function :

  vector<double> generate_PileUP_weights(TH1D* data_npu_estimated){
   // For  Spring15 see https://github.com/cms-sw/cmssw/blob/CMSSW_7_4_X/SimGeneral/MixingModule/python/mix_2015_25ns_Startup_PoissonOOTPU_cfi.py copy and paste from there:
   const double npu_probs[52] = {/* 0 -->*/
                        4.8551E-07,
                        1.74806E-06,
                        3.30868E-06,
                        1.62972E-05,
                        4.95667E-05,
                        0.000606966,
                        0.003307249,
                        0.010340741,
                        0.022852296,
                        0.041948781,
                        0.058609363,
                        0.067475755,
                        0.072817826,
                        0.075931405,
                        0.076782504,
                        0.076202319,
                        0.074502547,
                        0.072355135,
                        0.069642102,
                        0.064920999,
                        0.05725576,
                        0.047289348,
                        0.036528446,
                        0.026376131,
                        0.017806872,
                        0.011249422,
                        0.006643385,
                        0.003662904,
                        0.001899681,
                        0.00095614,
                        0.00050028,
                        0.000297353,
                        0.000208717,
                        0.000165856,
                        0.000139974,
                        0.000120481,
                        0.000103826,
                        8.88868E-05,
                        7.53323E-05,
                        6.30863E-05,
                        5.21356E-05,
                        4.24754E-05,
                        3.40876E-05,
                        2.69282E-05,
                        2.09267E-05,
                        1.5989E-05,
                        4.8551E-06,
                        2.42755E-06,
                        4.8551E-07,
                        2.42755E-07,
                        1.21378E-07,
                        4.8551E-08)* <-- 51 */};
   vector<double> result(50);
   double s = 0.0;
   for(int npu=0; npu<50; ++npu){
       double npu_estimated = data_npu_estimated->GetBinContent(data_npu_estimated->GetXaxis()->FindBin(npu));                              
       result[npu] = npu_estimated / npu_probs[npu];
       s += npu_estimated;
   }
   // normalize weights such that the total sum of weights over thw whole sample is 1.0, i.e., sum_i  result[i] * npu_probs[i] should be 1.0 (!)
   for(int npu=0; npu<50; ++npu){
       result[npu] /= s;
   }
   return result;
  }

2) Usage in code: 


  TFile* f = new TFile("MyDataPileupHistogram.root");
  TH1D* histo = (TH1D*) f->Get("pileup");

  if(e.pileupIT()<50){
     Weight_PileUp = generate_PileUP_weights(histo)[e.pileupIT()] 
  else{ //should not happen as we have a weight for all simulated ZJ_e.pileup.size() multiplicities!
     Weight_PileUp = 0;}
