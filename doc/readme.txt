=====================================================================
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++  
 Receipt
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
=====================================================================

=====================================================================
   Start and install the analisys code
=====================================================================


Setup up CMSSW (as of 11.10.2015):
  
  $ ssh -Y lxplus
  $ cmsrel CMSSW_7_4_14
  $ cd CMSSW_7_4_14/src
  $ cmsenv
  
The reference for the recomended versions of CMSSW and additional tags is:
    https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookWhichRelease
    https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuidePATReleaseNotes52X
  
Check out HeavyNu code:
  
  $ svn co https://svn.cern.ch/reps/heavynu/trunk/heavynu
  
Build the code:
  
  $ scram build -j 9


=====================================================================
  Private Signal Samples simulation
=====================================================================

To simulate privatly HeavyNu events and extract of data ntuple:

     $ cd heavynu
     $ cmsRun HeavyNuGenHLT_cfg.py WR1200_nuRe500
     $ cmsRun RecoFromHLT_cfg.py
     $ cmsRun dump_cfg.py

Example of simple fast Analysis (reading data ntuple):
     $ root -b -q -l examples/analysis.cxx+
     $ root -b -q -l examples/control.cxx+

=====================================================================
  CMS Central signal Samples simulation
=====================================================================

CMS may centrally simulate the signal samples

#How to request? #

to see avaliable  central samples use DAS (need GRID Certificate properly installed):

https://cmsweb.cern.ch/das/request?view=list&limit=50&instance=prod%2Fglobal&input=dataset%3D%2FWRToNuEToEEJJ_MW-4000_MNu-2000_TuneCUETP8M1_13TeV-pythia8%2F*%2F*

Process of centrally produced MC sample ineractively (good way to debug code, bad way to process multiple file):
  
  Download file using one option via DAS (need GRID Certificate properly installed):
   
     https://cmsweb.cern.ch/das/download?lfn=/store/mc/RunIISpring15DR74/WRToNuEToEEJJ_MW-4000_MNu-2000_TuneCUETP8M1_13TeV-pythia8/AODSIM/Asympt25ns_MCRUN2_74_V9-v1/50000/426A99DC-B207-E511-92D6-00259073E45E.root
  
  For example (need GRID Certificate properly installed):
   
  xrdcp root://xrootd.unl.edu//store/mc/RunIISpring15DR74/WRToNuEToEEJJ_MW-4000_MNu-2000_TuneCUETP8M1_13TeV-pythia8/AODSIM/Asympt25ns_MCRUN2_74_V9-v1/50000/426A99DC-B207-E511-92D6-00259073E45E.root .   

 
  $ mv 426A99DC-B207-E511-92D6-00259073E45E.root reco.root
  $ cmsRun dump_cfg.py

  Fast simple Analisys:
  $ root -b -q -l examples/control.cxx+

  to see available collection in sample
  $ edmDumpEventContent reco.root

  the pick of the right global tag in dump_cfg.py:
    for Spring15 50ns MC: global tag is 'auto:run2_mc_50ns'
    for Spring15 25ns MC: global tag is 'auto:run2_mc'
    for Run 2 data: global tag is 'auto:run2_data'
  as a rule, find the "auto" global tag in $CMSSW_RELEASE_BASE/src/Configuration/AlCa/python/autoCond.py
  This auto global tag will look up the "proper" global tag
  that is typically found in the DAS under the Configs for given dataset
  (although it can be "overridden" by requirements of a given release)


=====================================================================
  Process CMS Central Signal Samples, Central BG Samples and Data at Dubna Servers
=====================================================================

Copy sample to AFS  via DAS (need GRID Certificate properly installed):

  $xrdcp root://xrootd.unl.edu//store/mc/RunIISpring15DR74/WRToNuEToEEJJ_MW-4000_MNu-2000_TuneCUETP8M1_13TeV-pythia8/AODSIM/Asympt25ns_MCRUN2_74_V9-v1/50000/426A99DC-B207-E511-92D6-00259073E45E.root .


Copy from AFS  to mars.jinr.ru (15 Gb avaliabe) or cmsroc.jinr.ru (5 Gb avaliabe):

  $scp 426A99DC-B207-E511-92D6-00259073E45E.root tlisov@mars.jinr.ru:/home/tlisov/HeavyNu/SignalSamples_7_4/

Ssh to mars.jinr.ru or cmsroc.jinr.ru install CMSSW and heavynu  and process the sample


=====================================================================
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++   
    Analisys
+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
=====================================================================

=====================================================================
   Introduction
=====================================================================

Simulation is based on CMSSW framework. Use "The CMS Offline WorkBook"
as an entry point for documentation:

  https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBook

Chapter "3.2 Which CMSSW release to use" will give you an idea on which
version of CMSSW to use (see section "Current Analysis Release"):

  https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookWhichRelease

Sometimes the above page is slightly out-of-date, and more recent version
may be available. For example, if recomended version is 5.3.7, do a list of
all 5.3.x versions and use the latest one (CMSSW_5_3_X or CMSSW_5_3_X_patchY):

  $ scramv1 list CMSSW | grep CMSSW_5_3

For the data processing and analysis we use a number of commonly developed
tools:

  * https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBook

  * PAT (Physics Analysis Toolkit)
    https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookPAT
    https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuidePATReleaseNotes
    https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuidePhysicsCutParser
    https://cmssdt.cern.ch/SDT/doxygen/CMSSW_7_4_14/doc/html/d8/d2e/electronProducer__cfi_8py_source.html
    https://cmssdt.cern.ch/SDT/doxygen/CMSSW_7_4_14/doc/html/d5/d49/muonProducer__cfi_8py_source.html
    https://cmssdt.cern.ch/SDT/doxygen/CMSSW_7_4_14/doc/html/d3/d16/tauProducer__cfi_8py_source.html
    
    See "Learning and Training in CMS" indico for very nice PAT usage course:
    https://indico.cern.ch/categoryDisplay.py?categId=2063
  
  * PF (Particle Flow)
    https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideParticleFlow
    https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuidePF2PAT
    https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuidePATPF2PATExercise
  
  * HEEP (High Energy Electron Pairs) ID
    https://twiki.cern.ch/twiki/bin/view/CMS/SUSYBSMHeep
    https://twiki.cern.ch/twiki/bin/view/CMS/HEEPSelector
    https://twiki.cern.ch/twiki/bin/view/CMS/HEEPElectronID
    
  * Muon ID
    https://twiki.cern.ch/twiki/bin/view/CMS/ExoticaMuons#1_Muon_ID_Recommendations
    https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookPATExampleMuon
    https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideMuonId#Tight_Muon
    https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuidePATLayer0#Muon_selection
 
  * Tau ID
    https://twiki.cern.ch/twiki/bin/view/CMS/ExoticaTaus
    https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuidePFTauID
    https://twiki.cern.ch/twiki/bin/view/CMS/TauIDRecommendation
    
  * PF Jet, Jet ID, JEC, JEC uncertainties
    https://twiki.cern.ch/twiki/bin/view/CMS/ExoticaHighPtJets#JetId  
    https://twiki.cern.ch/twiki/bin/view/CMS/JECDataMC
    https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookJetEnergyCorrections#JetEnCor2012Fall12
    https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookJetEnergyCorrections#JetEnCorPFnoPU2012
    https://twiki.cern.ch/twiki/bin/view/CMS/JECUncertaintySources#Main_uncertainties_2012_53X
    
  * B tagging
    https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookBTagging
    https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideBTagging
    https://twiki.cern.ch/twiki/bin/viewauth/CMS/BtagPOG 
    
  * PileUp
    https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideFastSimPileUp
    https://twiki.cern.ch/twiki/bin/view/CMS/PileupMCReweightingUtilities
    https://twiki.cern.ch/twiki/bin/view/CMS/PileupJSONFileforData
    https://twiki.cern.ch/twiki/bin/view/CMS/PileupInformation
    https://twiki.cern.ch/twiki/bin/view/CMS/PileupReweighting (old)
   
  * Global Tags for Conditions Data
    https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideFrontierConditions

  * Exotica analyses main page:
    https://twiki.cern.ch/twiki/bin/view/CMS/EXOTICA
  
  * HeavyNu analyses 
    https://twiki.cern.ch/twiki/bin/view/CMS/ExoticaWRHeavyNu 
 
  * DataAgregation service
    https://cmsweb.cern.ch/das/request?view=list&limit=10&instance=cms_dbs_prod_global&input=dataset+dataset%3D*DYJetsToLL_M-50_TuneZ2Star_8TeV-madgraph-tarball*

  * PREP
    http://cms.cern.ch/iCMS/prep/requestmanagement?status=any&pwg=&campid=Summer12_DR53X&dsn=
    
  * CMSSW finder
    http://cmslxr.fnal.gov/lxr/

=====================================================================
   Cross cleaning
=====================================================================

Important problem is to disentangle the information
concerning reconstructed physical objects: leptons, jets, photons,
METs. Every object from these groups can be constructed from
the same reconstructed objects, for instance, from the same calorimeter
cells. It can cause the disbalance of energy in the event due to
a possible double counting energy in the calorimeters. Several
cleaning frameworks exist in CMSSW:

  * PAT integrated cleaning (currently using)
    https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuidePATCrossCleaning
    
    Can do quite simple things: flag overlapped objects, delete
    one overlapped object. For example delete jet which coincide
    with electron. But in the most common case we have partly
    overlapped objects and in this case we cannot simply delete
    one of them. We overcome this limitation by constrain (cut)
    on isolation of reconstructed object.
  
  * SusyPatCrossCleaner (obsolete)
    https://twiki.cern.ch/twiki/bin/view/CMS/SusyPatCrossCleaner
    
    More powerfull than PAT cleaning - supports cases of
    overlapped objects, but unsupported by authors and obsolete.
    Also, above page consists nice explanation of cleaning
    purpose and algorithms.
  
  * SusyAnalyser package (obsolete)
    https://twiki.cern.ch/twiki/bin/view/CMS/SusyAnalyzer


=====================================================================
   Signal simulation
=====================================================================

Simulation of signal samples is easy business - all that you need
is just to execute two config files:
  
  $ cmsRun HeavyNuGenHLT_cfg.py WR1200_nuRe500
  $ cmsRun RecoFromHLT_cfg.py

1. HeavyNuGenHLT_cfg.py      - W_R/nu_R generation GEN-SIM-DIGI-HLT
2. RecoFromHLT.py            - RAWtoDIGI-RECO

Important thing is to check that the same Geometry, MagneticField and
FrontierConditions defined in HeavyNuGenHLT_cfg.py, RecoFromHLT_cfg.py
and dump_cfg.py.

The only problem is simulation speed. Typical time to simulate one
event is ~ 6 minutes (Xeon 3GHz, RAM 4Gb). For 1000 events it
will take ~ 4 days. The way to overcome the problem is to run
simulation jobs in parallel on CERN LXBATCH facilities.

To simplify submition of simulation jobs to LXBATCH, shell script was
developed:

  batch-signal.sh [nevents] [pythia_setting] [destination]
         [nevents] - number of events to simulate
                     
  [pythia_setting] - name of pythia configuration set from heavynu_cfi.py
                     
     [destination] - place to put output files

Example:
  
  $ ./batch-signal.sh 1000 WR1200_nuRe500 pcrfcms14:/moscow41/xxx/WR1200_nuRe500

There are two important things, which the script assume:

  1. you install CMSSW on some AFS folder (for example, your home dir)
     this is needed because only AFS folders visible from LXBATCH machines
     
  2. files, which will be used for simulation:

       heavynu_cfi.py
       HeavyNuGenHLT_cfg.py
       RecoFromHLT_cfg.py 
       
     are situated in current directory
     

Now, to list submited jobs and these status, do:

  $ bjobs

Jobs will have PEND (waiting for processing) or RUN (running) status.
Empty jobs list mean all jobs are done. Follow to [destination] to
find results.

The last step is to produce ntuples for Heavy Neutrino analysis:
  
  $ cmsRun dump_cfg.py samplesdir=/moscow31/xxx/WR1200_nuRe500

Script dump_cfg.py runs PAT, PatCrossCleaner and PATDump modules.
You will find heavynu.root with tree for analysis in your current
directory.

Usually, you want to do more, than just to produce dump file. For this
case use script dump.sh:
  
  $ ./dump.sh /moscow31/xxx/WR1200_nuRe500

The script will create subdirectory 'dump_${date}/' on the given path,
where will be placed dump_cfg.py, log file of dump process and
resulting .root file.

To calculate cross-section of given decay channel do:

  $ cmsRun HeavyNuGen_cfg.py [pythia_setting]
  
This will run only generation step of 50000 events with pythia
configuration set name [pythia_setting]. You will see sross-section
in last lines of output.

---
To make simulation of multiple signal mass points on LXBATCH edit
file "signal-points.txt" to define points and run:
  
  $ ./batch-signal.sh all

Results will go to directory:
  
  pcrfcms14:/moscow41/{user}/signal-{date}
  
To "dump" all produced samples, do:
  
  $ ssh pcrfcms14
  # setup CMSSW environment
  $ cd CMSSW/src/heavynu
  $ find /moscow41/{user}/signal-{date} -mindepth 1 -maxdepth 1 -type d | xargs -L 1 ./dump.sh

Extract records for sampledb:
  
  $ cat /moscow41/{user}/signal-{date}/*/dump*/dump.log | sed -n 's,^sampledb record :, , p' | sort -n -k3 -k4


=====================================================================
   Background processing
=====================================================================

Most of CMS analyses have the same list of background processes. That
is why background samples centrally produced in frame of
CSA (Computer Simulation and Analysis challenge). The way to access
these samples is to submit your analysis jobs to GRID. For this purpose
exists special tool - CRAB (CMS Remote Analysis Builder).

These are the steps to process QCD events from CSA10:

1. Find dataset name:
     
     To find path, reffer to "Fall10 Monte Carlo production" page
     (link mentioned in end of this section).
     It is adviced to use "AODSIM" samples.
     For example lets use RECO dataset
     
     /ZJetToEE_Pt_15to3000_TuneZ2_Flat_7TeV_pythia6/Fall10-START38_V12-v1/GEN-SIM-RECO
     
     from   
      
     http://vdutta.web.cern.ch/vdutta/Fall10ProductionRECOglobal.html#request_1000083 
            
2. Now, prepare configuration files:
       
       $ mkdir ZEE
       $ cp dump_cfg.py crab.cfg ZEE/
       $ cd ZEE
     
     Edit files:
       * crab.cfg to define selected dataset
       * dump_cfg.py to define global tag ('START38_V13::All') of this dataset from
       
           https://twiki.cern.ch/twiki/bin/viewauth/CMS/SWGuideFrontierConditions
       
           and to define correct TriggerResults input tag, according to
             
           https://hypernews.cern.ch/HyperNews/CMS/get/physTools/2025.html
           
           the tag will be "TriggerResults::REDIGI38X".

3. And execute crab:
       
       $ crab -create -cfg crab.cfg
       $ crab -submit
       $ crab -status
       $ crab -getoutput

4. Sometimes you will see part of jobs failed, typically this is due to
   temporary GRID problems like fail of execution host or fail of access
   to data files. In this case try to re-submit failed jobs with:
     
     $ crab -resubmit n1,n2,n3,...
   
   If it not helps and fraction of failed jobs small (less 5%), you still
   can continue to merge step, but first query crab to actual amount of events
   which you read (and this value should be added to sampledb.h file). See
   field "Total Events read" in output of:
     
     $ crab -report

4. Merge CRAB output files. All output .root files will be located
   in ZEE/crab_*/res subdirectory. Use mergebg.sh utility
   to merge:
     
     $ cd ZEE
     $ ./mergebg.sh .
   
   File ZEE.root will be created in current directory.
   
5. Move all files to common area at:
     
     pcrfcms14:/moscow31/heavynu/38x


Sometimes you want to process many datasets. Use the following script:
  
  $ ./mcrab.sh [list.txt] [crab.cfg] [dump_cfg.py]
  
, where:
    
    [list.txt] - text file with list of datasets to proceed
    [crab.cfg] - path to config file for CRAB
    [dump_cfg.py] - path to config file for CMSSW

6. Sometimes CRAB allows to create a set of jobs, but when you submit them, it
gives a message like: "Unable to ship the project to the server". In this case
we should add a line to crab.cfg file:

    server_name=slc5cern

into the [CRAB] area. 

Latest FALL 2010 MC 3.8.5 production:
  https://twiki.cern.ch/twiki/bin/view/CMS/ProductionFall2010
  
Monte Carlo Sample Production Page:
  https://twiki.cern.ch/twiki/bin/view/CMS/GeneratorProduction

CMS Data Discovery Page:
  https://cmsweb.cern.ch/dbs_discovery/

CMS Dashboard:
  http://dashboard.cern.ch/cms/

CRAB FAQ:
  https://twiki.cern.ch/twiki/bin/view/CMS/SWGuideCrabFaq



=====================================================================
   Collision data processing
=====================================================================

The main entry point to understand situation with currently available
collision data datasets is "Physics Validation Group" page:

  https://twiki.cern.ch/twiki/bin/view/CMS/PVTMain

On the page you can find a link to 2011 year collisions data information:

  https://twiki.cern.ch/twiki/bin/view/CMS/Collisions2011Analysis

As an example we will read one of "May 10 Reprocessing" datasets in RECO format.
Corresponding entry on the page gives you the following essential information:

  * CMSSW version:
      CMSSW_4_2_8_patch4
      (you should use at least 4_2_3 to be able to read dataset, but generally
       it is better to use latest available in 4_2_* series)
  
  * Global Tag:
      GR_R_42_V22A::All
      (you will specify the tag later in steering file)
      
      if tag information is missig follow to:
         https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideFrontierConditions
         (sections "Global tags for collision data reprocessing" or "Global Tags for Data Taking")
  
  * file with list (mask) of good limusections ("JSON"):
      Masks for 2011 year situated here:
        http://cern.ch/cms-service-dqm/CAF/certification/Collisions11/7TeV/Prompt/
      
      Select the latest/greatest one:
        Cert_160404-163869_7TeV_PromptReco_Collisions11_JSON.txt
  
  * dataset name:
      Follow the "Announcement" link, there you will find list of available datasets.
      We will use:
        /SingleElectron/Run2011A-May10ReReco-v1/RECO

Now, prepare config files to run crab:
  
  $ cd CMSSW_4_2_8_patch4/src/heavynu
  $ mkdir data
  $ cp dumpdata_cfg.py crabdata.cfg data/
  $ cd data/
  $ wget http://cern.ch/cms-service-dqm/CAF/certification/Collisions11/7TeV/Prompt/Cert_160404-163869_7TeV_PromptReco_Collisions11_JSON.txt

Edit file crabdata.cfg to define dataset and mask:
  
  datasetpath = /SingleElectron/Run2011A-May10ReReco-v1/RECO
  lumi_mask   = Cert_160404-163869_7TeV_PromptReco_Collisions11_JSON.txt

Edit file dumpdata_cfg.py to set correct global tag:
  
  process.GlobalTag.globaltag = 'FT_R_42_V13A::All'

Now, everythis are ready to submit jobs:
  
  $ crab -create -cfg crabdata.cfg -submit
  
To check progress use:
  
  $ crab -status
  
To get results do:
  
  $ crab -get

Merge all job files to one file:
  
  $ ../mergebg.sh .

Now, add new record about processed dataset to file sampledb.h.
For this you need to know two parameters:
  - total number of processed events
  - integrated luminosity of processed events

Run:
  
  $ crab -report

In the output you are interested in two fields:
  - 'Total Events read'
      - with the total number of events
  
  - 'Luminosity section summary file'
      - with the path to json file to calculate
        integrated luminosity (NOTE: this is not the same
        "lumi_mask" file which you use above)

To calculate integrated luminosity run special script:
Look at https://twiki.cern.ch/twiki/bin/viewauth/CMS/LumiCalc
Prepare environment: 
                 cd CMSSW_4_2_8_patch4/src
                 cvs co  -r V03-03-07 RecoLuminosity/LumiDB
                 cd RecoLuminosity/LumiDB
                 scramv1 b
                 cmsenv
  
  $ lumiCalc2.py -c frontier://LumiCalc/CMS_LUMI_PROD -i crab_*/res/lumiSummary.json -b stable -norm pp7TeV --nowarning overview

And see column 'Recorded(/ub)' in total summary.




=====================================================================
  How to create JSON file 
=====================================================================
For reading of full dataset of collision data one needs a specified file,
which contains all good lumisections - JSON file. It has been produced
centrally, but it is possible to create it with two helper scripts. The link for 
downloadind these scripts (runregparse.py.txt and runreg.cfg)  is:
     
https://twiki.cern.ch/twiki/bin/viewauth/CMS/Collisions2010Recipes#Good_Run_List_and_LS_ranges

For creating of JSON file with predifined range of runs we set parameters in runreg.cfg.
Choose RunMin and RunMax values for definition of run range.  Then we choose which subdetectors
should be good in these runs, marking flag QFLAGS as GOOD. Fix the minimum
value of magnetick field parameter  BField_thr. After that execute file runregparse.py.txt:

  $ python runregparse.py.txt    

JSON file will be created in the current directory.


=====================================================================
   Analysis
=====================================================================

To prepate plots of control distributions do:
  
  $ root -b -q -l 'examples/control.cxx+("/path/to/heavynu.root")'

You will see results in file control.pdf.

To prepare more plots in ee channel use:

  $ root -b -q -l EE_Analis.cxx+
  
to change cuts change lines:
  
// Cuts for event selection
  unsigned int NofLept = 2;
  unsigned int NofJets = 2;
  float Lep1Pt = 60.;
  float Lep2Pt = 20.;
  float Jet1Pt = 40.;
  float Jet2Pt = 40.;
  float EtaCut1L = 1.44; //1.44
  float EtaCut2L = EtaCut1L;
  float EtaCut1J = 2.5;
  float EtaCut2J = 2.5; 
  float ISO = 1.;
  float Vertex = 0.03;
  float MllCut = 200.0;
  float MW_Cut = 0.0; 
  
You can change signal sample in corresponging part of code
The result of this code will be OUTPUT.ps, some .pdf and .png files.

To prepare more plots in emu channel for cheking ttbar bg use:

  $ root -b -q -l EMu_Analis.cxx+
    
The result will be OUTPUT-EMu.ps, some .pdf and .png files.





=====================================================================
   BG renormalization
=====================================================================
To renormilize Z+Jets bg.

First in script EE_Analis.cxx+ make changes:

1) uncomment

TH1::SetDefaultSumw2(kTRUE);

2) uncomment
  // ============================================
  // save to .root file M_l1 histograms with:
  //  1. MC: summ of all backgrounds
  //  2. MC: signal
  //  3. DATA:
  //{
  // ZJ_hMll.Sumw2();
  // TW_hMll.Sumw2();
 //	RD_hMll.Sumw2();
    TFile f2("fit-input-Mll.root", "RECREATE");
    TW_hMll.Clone("Other")->Write();
   // S_hMll.Clone("Signal")->Write();
    ZJ_hMll.Clone("ZJ")->Write();
    RD_hMll.Clone("Data")->Write();
    f2.Close();
  //}
  // ============================================
  
 3) comment

  //TTb_hMll.Add(&ZJ_hMll); 
  
run:

  $ root -b -q -l EE_Analis.cxx+

fit-input-Mll.root will appear as result.


Using fit-input-Mll.root output file from EE_Analis.cxx code do:

  $ root -b -q -l Chi2.cxx+

Put right name of .root file in line:
      TFile* f = new TFile("fit-input-Mll.root");
 
uncomment line: 
      TH1F* sig = (TH1F*) f->Get("ZJ");
	  
	  TH1F* sig = (TH1F*) f->Get("TTb"); line should be commented
	  
You will recive Mult.pdf, WR.pdf and the same .png outoput files with renormilized histogram.
A renormilise faktor will be in promt. You can use it in  EE_Analis.cxx, EMu_Analis.cxx and other code to rescale Z+jets in line:

        float Z_tune = 1.;

Also now you have to put it in line TLatex *   tex = new TLatex(0.3,0.8,"multiplier = 1.03+/-0.16"); and run script again to recive right Mult.* plot






To renormilize TTbar bg 

In script EMu_Analis.cxx+ make changes:

1) uncomment

TH1::SetDefaultSumw2(kTRUE);

2) uncomment
  // ============================================
  // save to .root file M_WR histograms with:
  //  1. MC: summ of all backgrounds
  //  2. MC: signal
  //  3. DATA:
  {
    TFile f("fit-input-EMu-Mll.root", "RECREATE");
	TTb_hMll.Sumw2();
	TW_hMll.Sumw2();
	RD_hMll.Sumw2();
    TTb_hMll.Clone("TTb")->Write();;
    TW_hMll.Clone("Other")->Write();
    RD_hMll.Clone("Data")->Write();
    f.Close();
  }
  // ============================================

  
 3) change
 
 // TTb_hMll.Add(&ZJ_hMll);
 // QCD_hMll.Add(&TTb_hMll);
 // WJ_hMll.Add(&QCD_hMll);
    WJ_hMll.Add(&ZJ_hMll);


  
run:

  $ root -b -q -l EMu_Analis.cxx+

fit-input-EMu-Mll.root will appear as result.


Using fit-input-EMu-Mll.root output files from EMu_Analis.cxx code do:

  $ root -b -q -l Chi2.cxx+

Put right name of .root file in line:
      TFile* f = new TFile("fit-input-EMu_Mll.root");
 
 line should be commented:
      TH1F* sig = (TH1F*) f->Get("ZJ");
	
uncomment line:	  
	  TH1F* sig = (TH1F*) f->Get("TTb"); 
	  
You will recive Mult.pdf, WR.pdf and the same .png outoput files with renormilized histogram.
A renormilise faktor will be in promt. You can use it in  EE_Analis.cxx, EMu_Analis.cxx and other code to rescale TTb:

        float TTb_tune = 1.;

Also now you have to put it in line TLatex *   tex = new TLatex(0.3,0.8,"multiplier = 1.03+/-0.16"); and run script again to recive right Mult.* plot


=====================================================================
   Calculate efficiencies and  number of BG events
=====================================================================
Use code:
   $root -b -q -l EE_Cut_Optimize.cxx+
With setted up  appropriate cuts for each W_R masses signal samples (default (1000 GeV,500 GeV)) and produce OptimumCuts.txt file.
Cuts can be setted in lines:
1) for M_W cut:   
     int   MW_C = 75;  
  means 750 GeV cut  (default) or directly in line:
      for (int Cut_W = MW_C; Cut_W < (MW_C+1); ++Cut_W) {
  MW_cut= Cut_W*10; 
  
  Notice, in line 
      float MW_cut =  0.;
  value of precut shold be less than or equal final cut in lines, menshioned above (default 0). 
  
2) for M_ll cut (default 200 GeV):

  for (int Cut_LL = 20; Cut_LL < 21; ++Cut_LL) {
  Mll_cut= Cut_LL*10;
  
  Notice, in line 
        float Mll_cut = 200.; 
  value of precut shold be less than or equal final cut in lines, menshioned above (default 200).

3) for other cut:

   the similar way as in 2)

For eejj channel OptimumCuts_E.txt contain all availaible mass points,
For mumujj channel OptimumCuts_Mu.txt.


=====================================================================
   Setup Limits
=====================================================================
Do:

$ root -b -q -l EE_Upper_Limit.cxx+


OptimumCuts.txt and Systematics.txt are input files. By defalt OptimumCuts_E.txt and Systematics_ALL_E.txt are used.
Now reading of real data is commented in order to boost speed of this code, temporary code used instead:

  for (int S_i = 0; S_i < sa.size(); ++S_i) {
   S_CS[S_i] = sa[S_i].CS;
   S_CS_Error[S_i] = 0.10;
   RD_n[S_i] = 0;   
  }

In this part of code you also could change the relative PDF and scale uncertainty.

Output files: 

Data_CS_Up_Lim.txt is used for next step to make exclusion limit plot
CS_Upper_Limits.pdf - limits plot
Limit_Distr.ps - auxilary plots to check gaussian fit of poisson. For upper W_R mass it is not always correct. In this case uncomment line:

    ex_sigma_sigma[S_i] = 0.065;

=====================================================================
   Exclusion mass plot
=====================================================================

go to /heavynu/datafit/ folder, copy here Data_CS_Up_Lim.txt file. Here are already Data_CS_Up_Lim_E_All.txt and Data_CS_Up_Lim_Mu_All.txt by default.

Do:

   $ root -b -q -l 'lim2D.cxx+'
   
Put name of input file in line (Data_CS_Up_Lim_Mu_All.txt is default):

const LimitsResults el = calcLimits("Data_CS_Up_Lim_Mu_All.txt");

Output will be in files lim2D-cont0.pdf and lim2D-cont0.png


=====================================================================
   How to prepare discovery plot
=====================================================================

The last step in simulation is preparation of limits/discovery plots.
All the tools situated in datafit/ subdirectory.

Use root 5.24 to work with fit.cxx.

Data files for fit.cxx situated on:
  
  pcrfcms14:/moscow31/heavynu/data-10TeV.tar.gz

...

The receipt is the following (on example of dstsign1500-600e.d):

  $ cd datafit/
  $ root
  [] .L fit.cxx+
  [] fit2D(3, 10, 0)
  [] doGraphics("fit-data=dstsign1500-600e.d-samples=10.root")
  [] .q
  
Now you will see .root and .eps files with plots in your current directory.


=====================================================================
   Signal/background/data samples
=====================================================================

Place:
  pcrfcms14:/moscow31/heavynu/{16x,22x,31x,36x,38x,data}

Cross section for signal samples calculated from 10 000 pythia events.
Weights in dump root trees correspond to integrated luminosity 100 pb-1.

Table with all signal and background samples are in sampledb.h now,
please update this file if you add new samples.

Some 22x samples trees do not have integrated HLT maps, to find correspondence use
following files:
  
  pcrfcms14:/moscow31/heavynu/22x/22x-HLT_{1e31,2e30}.txt

Details on HLT menus you can find at:
  
  https://twiki.cern.ch/twiki/bin/view/CMS/SWGuideGlobalHLT
  https://twiki.cern.ch/twiki/bin/view/CMS/TriggerTables

Global Tags:
  
  https://twiki.cern.ch/twiki/bin/view/CMS/SWGuideFrontierConditions

Backup:
  
  $ ssh pcrfcms14
  $ rsync -av --delete --ignore-errors /moscow31/heavynu pcrfcms8:/moscow31 &> ~/rsync.log &

=====================================================================
   Infrastructure
=====================================================================

Source code / SVN repository:
  https://svnweb.cern.ch/trac/heavynu/browser

SVN librarian account:
  https://account.cern.ch/account/CERNAccount/AccountStatus.aspx?login=inrhnu

E-groups / mailing and write access to SVN:
  https://e-groups.cern.ch/e-groups/EgroupsSearch.do?searchValue=heavynu-team

Twiki:
  https://twiki.cern.ch/twiki/bin/view/CMS/ExoticaWRHeavyNu

Computers / samples storage:
  pcrfcms14.cern.ch:/moscow*/heavynu


=====================================================================
   Heavynu code
=====================================================================

All current development goes in:

  https://svnweb.cern.ch/trac/heavynu/browser/trunk/heavynu
  
Change log:

  https://svnweb.cern.ch/trac/heavynu/log/

Tags:

  https://svnweb.cern.ch/trac/heavynu/browser/tags
  
  Note: tags are only neccessary if you want to prepare MC samples or
        read GRID datasets of/with some ancient version of CMSSW (never
        happend so far).
        Do not use tags to do analysis, always use the latest version of
        code ('trunk') for this.

------------------------------------------------
  Date         Tag
----------   -----------------------------------
2009.04.19 - heavynu-16x
  latest tag to work with CMSSW_1_6_x

2009.08.24 - heavynu-PATv1
  to work with:
    CMSSW_2_2_9 (last release with PATv1)
    PATv1
    SusyPATCrossCleaning
  
2009.09.24 - heavynu-PATv2_SusyPATCrossCleaning
  to work with:
    CMSSW_2_2_13 (last release of CMSSW in 22x)
    PATv2
    SusyPATCrossCleaning
     (with patches - see readme.txt of this tag)
  
2009.10.03 - heavynu-22x
  to work with:
    CMSSW_2_2_13
    PATv2
    PATv2 integrated cross-cleaning
  
2009.10.09 - heavynu-OCTX
  to work with:
    CMSSW_3_1_4 (version for October Excercise/CSA09 processing)
    PATv2
    PATv2 integrated cross-cleaning

2010.03.31 - heavynu-314
  latest tag to work with CMSSW_3_1_4

2010.06.25 - heavynu-356
  latest tag to work with CMSSW_3_5_6

2010.10.10 - heavynu-36x
  latest tag to work with CMSSW_3_6_x

2011.04.18 - heavynu-38x
  latest tag to work with CMSSW_3_8_x

2011.04.21 - heavynu-41x
  latest tag to work with CMSSW_4_1_x

2015.09.16 - heavynu-53x
  latest tag to work with CMSSW_5_3_x
