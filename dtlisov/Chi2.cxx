// macro for:
//  * Rescale using Chi2
//  * plot signal, background and data histograms
//
// to run:
//  $ root -b -q -l Chi2.cxx+
//
// NOTE: use recent root version
//       fit do not converge in root 5.22 (CMSSW 38x)

#include "TFile.h"
#include "TH1F.h"
#include "TGraph.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "THStack.h"
#include "TLegend.h"
#include "TLatex.h"
#include "tdrstyle.C"
//#include "heavynuStandard.C"
#include "heavynuStandard11.C"
#include <iostream>


double computeChi2(TH1 *h1, TH1 *h2,int ifirst,int ilast)
{
  double chi2=0.;
  for (int ibin=ifirst; ibin<=ilast; ibin++) {
    double bin1  = h1->GetBinContent(ibin);
    double bin2  = h2->GetBinContent(ibin);
    double bin1e = h1->GetBinError  (ibin);
    double bin2e = h2->GetBinError  (ibin);
    chi2 += (bin1-bin2)*(bin1-bin2)/((bin1e*bin1e)+(bin2e*bin2e));
    //cout << bin1 << " " << bin2 << "; ";
  }
 // cout <<chi2<< endl;
  return chi2;
}

void Chi2()
{
  TFile* f = new TFile("fit-input-EMu-Mll.root");
//    TFile* f = new TFile("fit-input-EMu-Mll-11-60-2.root"); 
//  TFile* f = new TFile("fit-input-EMu-Mll-11-60.root");
//    TFile* f = new TFile("fit-input-Mll-MUMU.root");
//	TFile* f = new TFile("fit-input-Mll.root");
//  TFile* f = new TFile("fit-input-MW.root");  
//  TFile* f = new TFile("fit-input-MNuR1.root");   
  TH1F* bkg = (TH1F*) f->Get("Other"); 
  TH1F* sig = (TH1F*) f->Get("TTb");
 // TH1F* sig = (TH1F*) f->Get("ZJ");
  TH1F* dat = (TH1F*) f->Get("Data");
  
 //  TFile* f1 = new TFile("hist-qcdbgtoemu-nomuiso.root"); 
  // TH1F* QCD = (TH1F*) f1->Get("Other"); 
  // bkg->Add(QCD);
  
    
    sig->Sumw2();
	dat->Sumw2();
	bkg->Sumw2();
  
  int NX=sig->GetNbinsX();
  int N=1000;
  float Cont[NX];
  float Error[NX];
  float X[N];
  float Y[N];
  float MIN=10000;
  float MULTIPL=0.0;
//  float RD_Lum = 36.;
  float RD_Lum = 204.;
  
  for (int j=1;j<=NX;++j) {
    Cont[j]=sig->GetBinContent(j);
	Error[j]=sig->GetBinError(j);
   } 
  
  for (int i=0; i<=N; ++i) {
   float mult=i*2./((float) N);
  // float mult=1.;
   for (int j=1;j<=NX;++j) {
    sig->SetBinContent(j,mult*Cont[j]);
	sig->SetBinError(j,sqrt(mult)*Error[j]);
   }   
   sig->Add(bkg);
   X[i]=mult;
   Y[i]=computeChi2(dat, sig, 1,NX);
   cout<< i<< " mult = "<< X[i]<< " Chi2 = "<<Y[i]<< endl;
  if (MIN>Y[i]) {MIN=Y[i];MULTIPL=X[i];}
  }
  
  cout<< "MULT = "<< MULTIPL<< " Chi2 = "<< MIN<<endl;
  
  for (int j=1;j<=NX;++j) {
    sig->SetBinContent(j,MULTIPL*Cont[j]);
	sig->SetBinError(j,sqrt(MULTIPL)*Error[j]);
  }
  
  sig->Add(bkg);
  
  gROOT->SetStyle("Plain");
  
  TCanvas c1("OUTPUT1","OUTPUT1");
  setTDRStyle();
  fixOverlay();
  
  TGraph* Parab=new TGraph( N+1, X, Y);
//  Parab->GetXaxis()->SetTitle("Multiplier for NNLO Z+jets #sigma");
  Parab->GetXaxis()->SetTitle("Multiplier for t#bar{t} #sigma");
  Parab->GetXaxis()->SetLabelFont(42);
  Parab->GetXaxis()->SetTitleFont(42);
  Parab->GetYaxis()->SetTitle("#chi^{2}");
  Parab->GetYaxis()->SetLabelFont(42);
  Parab->GetYaxis()->SetTitleFont(42);
  Parab->SetLineStyle(0);
  Parab->SetLineColor(2);
  Parab->SetLineWidth(4);

  bkg->SetFillColor(4);
  bkg->SetLineStyle(0);
  sig->SetFillColor(5);
  sig->SetLineStyle(0);
  dat->SetMarkerStyle(21);
  dat->SetStats(kFALSE);
    
  TLegend* leg = new TLegend(0.39,0.79,0.94,0.94);
  leg->SetBorderSize(1);
  leg->SetTextFont(62); 
  leg->SetLineColor(1);
  leg->SetLineStyle(1);
  leg->SetLineWidth(1);
  leg->SetFillColor(10);
  leg->SetFillStyle(1001);
//  leg->AddEntry(dat, "Data, Run2010, 36pb^{-1}","PE");
  leg->AddEntry(dat, "Data, Run2011, 204pb^{-1}","PE");
 // leg->AddEntry(sig, "Spring11 t#bar{t} Madgraph","F");
  leg->AddEntry(sig, "Spring11 Z+Jets Alpgen","F");  
  leg->AddEntry(bkg, "Other Background","F");
  
  TCanvas* c = new TCanvas("WR");
//  sig->Draw("HIST");
//  dat->Draw("e1same");
  dat->Draw("e1");
  sig->Draw("HISTsame");
  bkg->Draw("HISTsame");
  dat->Draw("e1same");
  dat->GetYaxis()->SetRangeUser(0.1, 10000);
  c->Modified();
  leg->Draw();
  //label_Lumi(0.73, 0.7, RD_Lum);
  label_Prelim(0.73, 0.75);
  c->Print("Chi_ll.pdf");
  c->Print("Chi_ll.png");
  c->Print("Chi_ll.cxx");
  c->Clear();  
  Parab->Draw("AC");
  TLatex *   tex = new TLatex(0.3,0.8,"multiplier = 1.02+/-0.3"); 
  tex->SetNDC();
  tex->SetLineWidth(2);
  tex->Draw();
  label_Lumi(0.52, 0.7, RD_Lum);
  label_Prelim(0.52, 0.75);
  c->Print("Chi_Mult.pdf");
  c->Print("Chi_Mult.cxx");
  c->Print("Chi_Mult.png");
}
