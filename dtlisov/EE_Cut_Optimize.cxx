// to run script do:
//    root -b -q -l EE_Cut_Optimize.cxx+

#include "TH1.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TLegend.h"
#include <TLorentzVector.h>


#include "../PATDump/src/format.h"
#include "../sampledb.h"
#include <iostream>
#include <fstream>
using namespace std;

void EE_Cut_Optimize()
{
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Inizialization


  unsigned int NofLept = 2;
  unsigned int NofJets = 2;
  float Lep1Pt_cut = 0.;
  float Lep2Pt_cut = 0.;
  float Jet1Pt_cut = 40.;
  float Jet2Pt_cut = 40.;
  float Mll_cut = 200.;
  float MW_cut =  0.;
  int   MW_C = 75;
  float MN1_cut = 0.;
  float MN2_cut = 0.;  
  float Eta1L_cut = 0.;
  float Eta2L_cut = 0.;
  float Eta1J_cut = 2.5;
  float Eta2J_cut = 2.5; 
  float ISO_cut = 1.;
  float Vertex_cut = 0.03;
  float Lum = 36.145/100.;
  float Z_tune = 1.;
  float J_k = 1.0;
  float E_k = 1.0;
  float HEEP_B=0.972;
  float HEEP_E=0.982;
  float A_Wei=1.; 
  
// Samples switcher: true = include samples in analisis
  bool QCD_incl = true;
  bool TTb_incl = true;
  bool ZJ_incl = true;
  bool WJ_incl = true;
  bool ZZ_incl = true;
  bool ZW_incl = true;
  bool WW_incl = true;
  bool TW_incl = true;
 
  FILE* Ptr = fopen("OptimumCuts.txt","w");
  
  int INFORM = 0;
    
  float BG_statw = 0.;  
  float BG_nw = 0.; // number of BG events passed cuts
//  float delta_BG_nw_sq = 0. ; // Squared absolut error for number of total BG events
  

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Reading samples

//============================================================================================================================
// MC SIGNAL


  // finding signal samples in sample.db
  const SampleDB db;
  const SampleDB sa = db.find("MW", 1000)/*.find("MNu", 500)*/.find("type", 1).find("channel", 1).find("AlCa", "START38_V14::All").find("CMSSW", "3_8_7");

  string root = "/moscow31/heavynu/38x/legacy/";
  string full_name;
  
  unsigned int S_e_sample_MW[sa.size()], S_e_sample_MNu[sa.size()];
  float S_nw_Opt[sa.size()], S_eff[sa.size()];
  int   /*S_n_Opt[sa.size()],*/ S_stat[sa.size()];
  float S_statw[sa.size()];
  float S_weight[sa.size()];
  float F[sa.size()];
  float F_max[sa.size()];
  
  for (int S_i = 0; S_i < sa.size(); ++S_i) {
  F_max[S_i] = 0. ;
  }
  
  float Mll_Cut_Opt[sa.size()];
  float MW_Cut_Opt[sa.size()];
  float MN1_Cut_Opt[sa.size()];
  float MN2_Cut_Opt[sa.size()];
  float Lep1Pt_Cut_Opt[sa.size()];
  float Lep2Pt_Cut_Opt[sa.size()];
  float Eta1L_Cut_Opt[sa.size()];
  float Eta2L_Cut_Opt[sa.size()];
  float BG_nw_Opt[sa.size()];
  float BG_eff[sa.size()];
  //int   QCD_n_Opt[sa.size()];
  float QCD_nw_Opt[sa.size()];
  //int   TTb_n_Opt[sa.size()];
  float TTb_nw_Opt[sa.size()];
  //int   ZJ_n_Opt[sa.size()];
  float ZJ_nw_Opt[sa.size()]; 
  float WJ_nw_Opt[sa.size()];
  float ZZ_nw_Opt[sa.size()];
  float ZW_nw_Opt[sa.size()];
  float WW_nw_Opt[sa.size()];
  float TW_nw_Opt[sa.size()];
    
  int S_Pre_n[sa.size()];
  int S_nEvents[sa.size()];
  int S_max_stat = 0;
  
  for (int S_i = 0; S_i < sa.size(); ++S_i) {
   if (S_max_stat < sa[S_i].stat) { S_max_stat = sa[S_i].stat;}
  } 
  
  float S_Lep1Pt[sa.size()][S_max_stat];
  float S_Lep2Pt[sa.size()][S_max_stat];
  float S_Eta1L[sa.size()][S_max_stat];
  float S_Eta2L[sa.size()][ S_max_stat]; 
  float S_Mll[sa.size()][S_max_stat];
  float S_WR[sa.size()][S_max_stat];
  float    S_NuR_l0[sa.size()][S_max_stat];
  float    S_NuR_l1[sa.size()][S_max_stat]; 
  
  for (int S_i = 0; S_i < sa.size(); ++S_i) {
   full_name = root +  sa[S_i].fname;
   TheEvent S_e(full_name.c_str());
   
   if (!S_e) return;
      
   S_e_sample_MW[S_i] = S_e.sample.MW;
   S_e_sample_MNu[S_i] = S_e.sample.MNu;
  
   S_weight[S_i] = S_e.weight*Lum;
   S_stat[S_i] = S_e.sample.stat;
   S_statw[S_i] = S_e.sample.stat*S_weight[S_i];
   S_nEvents[S_i] = S_e.totalEvents();   
   S_Pre_n[S_i] = 0; 
  
  for (int i = 0; i < S_nEvents[S_i]; ++i) {
    S_e.readEvent(i); 
    //cuts on number of leptons and jets    
    if ((S_e.electrons.size() >= NofLept) && (S_e.jets.size() >= NofJets)) {
     //cuts on jets Pt      
     if (((S_e.jets[0].p4).Pt()*J_k>=Jet1Pt_cut) && ((S_e.jets[1].p4).Pt()*J_k>=Jet2Pt_cut))  { 
      //cuts on Eta of jets 
      if ((abs(S_e.jets[0].p4.Eta())<=Eta1J_cut) && (abs(S_e.jets[1].p4.Eta())<=Eta2J_cut))   {    
       // Isolation
       if ((S_e.electrons[0].isoHEEP() <= ISO_cut)&&(S_e.electrons[1].isoHEEP() <= ISO_cut)) {       
        //Cut on vertexes
        if ((abs(S_e.electrons[0].vertex.z()-S_e.electrons[1].vertex.z())<=Vertex_cut) && (abs(S_e.jets[0].vertex.z()-S_e.jets[1].vertex.z())<=Vertex_cut) 
             && (abs(S_e.electrons[0].vertex.z()-S_e.jets[1].vertex.z())<=Vertex_cut) && (abs(S_e.electrons[0].vertex.z()-S_e.jets[1].vertex.z())<=Vertex_cut)) {
          //cuts on electrons invariant mass Mll    
         if ((S_e.electrons[0].p4*E_k  + S_e.electrons[1].p4*E_k  ).M()>=Mll_cut) {
		 
          S_Lep1Pt[S_i][S_Pre_n[S_i]] = (S_e.electrons[0].p4*E_k  ).Pt();
          S_Lep2Pt[S_i][S_Pre_n[S_i]] = (S_e.electrons[1].p4*E_k  ).Pt();
          S_Eta1L[S_i][S_Pre_n[S_i]] = abs(S_e.electrons[0].p4.Eta());
          S_Eta2L[S_i][S_Pre_n[S_i]] = abs(S_e.electrons[1].p4.Eta());         
          S_Mll[S_i][S_Pre_n[S_i]] = (S_e.electrons[0].p4*E_k  + S_e.electrons[1].p4*E_k  ).M();
          S_WR[S_i][S_Pre_n[S_i]] = (S_e.electrons[0].p4*E_k  + S_e.electrons[1].p4*E_k  + S_e.jets[0].p4*J_k + S_e.jets[1].p4*J_k).M();
          S_NuR_l0[S_i][S_Pre_n[S_i]] = (S_e.electrons[0].p4*E_k  + S_e.jets[0].p4*J_k + S_e.jets[1].p4*J_k).M();
          S_NuR_l1[S_i][S_Pre_n[S_i]] = (S_e.electrons[1].p4*E_k  + S_e.jets[0].p4*J_k + S_e.jets[1].p4*J_k).M();
          S_Pre_n[S_i] = S_Pre_n[S_i] + 1;
         }
        }
       }
      }
     }
    }
   }
 }


  

//============================================================================================================================= 
// MC BG QCD FAKE-RATE 

  string QCD_fname = "/moscow31/heavynu/38x/dstmkLQ1.d";
 
  float QCD_e_jets_size = 0; 
  float QCD_e_electrons_size = 0;  
  float QCD_e_weight = 0;
  float pjg[4];
  int QCD_itype = 0;
  int QCD_jemark = 0;
  int QCD_e_electrons_0_charge;
  int QCD_e_electrons_1_charge;
  long int QCD_e_ievent = 0;

  int QCD_Pre_n = 0;

  char str[200] = {" "};
  
  TLorentzVector QCD_e_electrons_0_p4, QCD_e_electrons_1_p4, QCD_e_jets_0_p4, QCD_e_jets_1_p4;
  
  ifstream inFile1(QCD_fname.c_str());
   
  while (!inFile1.eof()) {
        
    inFile1 >> QCD_e_ievent;
    inFile1.getline(str, 200);
    
    inFile1 >> QCD_e_jets_size;
    inFile1 >> QCD_e_weight;
    inFile1 >> QCD_itype;   
    inFile1.getline(str, 200);
        
    // read Jets INFORMrmations   
    inFile1 >> QCD_jemark;
    for (int j = 0; j < 4; j++) inFile1 >> pjg[j];
    QCD_e_jets_0_p4.SetPxPyPzE(pjg[0], pjg[1], pjg[2], pjg[3]);    
    inFile1.getline(str, 200);
    
    inFile1 >> QCD_jemark;
    for (int j = 0; j < 4; j++) inFile1 >> pjg[j];
    QCD_e_jets_1_p4.SetPxPyPzE(pjg[0], pjg[1], pjg[2], pjg[3]);    
    inFile1.getline(str, 200);
    
    // Read Leptons INFORMrmations
    inFile1 >> QCD_e_electrons_size;
    inFile1.getline(str, 200);
    
    inFile1 >> QCD_e_electrons_0_charge;
    for (int j = 0; j < 4; j++) inFile1 >> pjg[j];
    QCD_e_electrons_0_p4.SetPxPyPzE(pjg[0], pjg[1], pjg[2], pjg[3]);    
    inFile1.getline(str, 200);
    
    inFile1 >> QCD_e_electrons_1_charge;
    for (int j = 0; j < 4; j++) inFile1 >> pjg[j];
    QCD_e_electrons_1_p4.SetPxPyPzE(pjg[0], pjg[1], pjg[2], pjg[3]);    
    inFile1.getline(str, 200);
    
    inFile1.getline(str, 200);
    
    
    //cuts on number of leptons and jets    
    if ((QCD_e_electrons_size >= NofLept) && (QCD_e_jets_size >= NofJets)) {
     //cuts on jets Pt      
     if (((QCD_e_jets_0_p4).Pt()>=Jet1Pt_cut) && ((QCD_e_jets_1_p4).Pt()>=Jet2Pt_cut))  { 
      //cuts on Eta of jets 
      if ((abs(QCD_e_jets_0_p4.Eta())<=Eta1J_cut) && (abs(QCD_e_jets_1_p4.Eta())<=Eta2J_cut))   {    
       // Isolation
       //if ((QCD_e.electrons[0].isoHEEP() <= ISO_cut)&&(QCD_e.electrons[0].isoHEEP() <= ISO_cut)) {       
        //Cut on vertexes
       // if ((abs(QCD_e.electrons[0].vertex.z()-QCD_e.electrons[1].vertex.z())<=Vertex_cut) && (abs(QCD_e.jets[0].vertex.z()-QCD_e.jets[1].vertex.z())<=Vertex_cut) 
       //      && (abs(abs(QCD_e.electrons[0].vertex.z()-QCD_e.electrons[1].vertex.z())-abs(QCD_e.jets[0].vertex.z()-QCD_e.jets[1].vertex.z()))<=Vertex_cut)) {
         //cuts on electrons invariant mass Mll    
         if ((QCD_e_electrons_0_p4*E_k + QCD_e_electrons_1_p4*E_k).M()>=Mll_cut) {
          QCD_Pre_n = QCD_Pre_n +1;
         }         
        // }    
      //  }    
      }     
     }
    }
   } 
    
  // Normalization for 36.145 inverse pb  
  float QCD_weight_sample = Lum;
  
  float QCD_weight[QCD_Pre_n];  
  float QCD_Lep1Pt[QCD_Pre_n];
  float QCD_Lep2Pt[QCD_Pre_n];
  float QCD_Eta1L[QCD_Pre_n];
  float QCD_Eta2L[QCD_Pre_n]; 
  float QCD_Mll[QCD_Pre_n];
  float QCD_WR[QCD_Pre_n];
  float    QCD_NuR_l0[QCD_Pre_n];
  float    QCD_NuR_l1[QCD_Pre_n]; 
  
  QCD_Pre_n = 0;
  float QCD_Pre_nw = 0;
  long int QCD_nEvents = 0;
  float QCD_nwEvents = 0.;
  
  ifstream inFile(QCD_fname.c_str());
   
  while (!inFile.eof()) {
  
    QCD_nEvents = QCD_nEvents + 1;
       
    inFile >> QCD_e_ievent;
    inFile.getline(str, 200);
    
    inFile >> QCD_e_jets_size;
    inFile >> QCD_e_weight;
    inFile >> QCD_itype;   
    inFile.getline(str, 200);
    
    QCD_e_weight = QCD_e_weight*QCD_weight_sample;
    QCD_nwEvents = QCD_nwEvents + QCD_e_weight;
    
    // read Jets INFORMrmations   
    inFile >> QCD_jemark;
    for (int j = 0; j < 4; j++) inFile >> pjg[j];
    QCD_e_jets_0_p4.SetPxPyPzE(pjg[0], pjg[1], pjg[2], pjg[3]);    
    inFile.getline(str, 200);
    
    inFile >> QCD_jemark;
    for (int j = 0; j < 4; j++) inFile >> pjg[j];
    QCD_e_jets_1_p4.SetPxPyPzE(pjg[0], pjg[1], pjg[2], pjg[3]);    
    inFile.getline(str, 200);
    
    // Read Leptons INFORMrmations
    inFile >> QCD_e_electrons_size;
    inFile.getline(str, 200);
    
    inFile >> QCD_e_electrons_0_charge;
    for (int j = 0; j < 4; j++) inFile >> pjg[j];
    QCD_e_electrons_0_p4.SetPxPyPzE(pjg[0], pjg[1], pjg[2], pjg[3]);    
    inFile.getline(str, 200);
    
    inFile >> QCD_e_electrons_1_charge;
    for (int j = 0; j < 4; j++) inFile >> pjg[j];
    QCD_e_electrons_1_p4.SetPxPyPzE(pjg[0], pjg[1], pjg[2], pjg[3]);    
    inFile.getline(str, 200);
    
    inFile.getline(str, 200);
    
    
    //cuts on number of leptons and jets    
    if ((QCD_e_electrons_size >= NofLept) && (QCD_e_jets_size >= NofJets)) {
     //cuts on jets Pt      
     if (((QCD_e_jets_0_p4*J_k ).Pt()>=Jet1Pt_cut) && ((QCD_e_jets_1_p4*J_k ).Pt()>=Jet2Pt_cut))  { 
      //cuts on Eta of jets 
      if ((abs(QCD_e_jets_0_p4.Eta())<=Eta1J_cut) && (abs(QCD_e_jets_1_p4.Eta())<=Eta2J_cut))   {    
       // Isolation
       //if ((QCD_e.electrons[0].isoHEEP() <= ISO_cut)&&(QCD_e.electrons[1].isoHEEP() <= ISO_cut)) {       
        //Cut on vertexes
       // if ((abs(QCD_e.electrons[0].vertex.z()-QCD_e.electrons[1].vertex.z())<=Vertex_cut) && (abs(QCD_e.jets[0].vertex.z()-QCD_e.jets[1].vertex.z())<=Vertex_cut) 
        //     && (abs(abs(QCD_e.electrons[0].vertex.z()-QCD_e.electrons[1].vertex.z())-abs(QCD_e.jets[0].vertex.z()-QCD_e.jets[1].vertex.z()))<=Vertex_cut)) {
         //cuts on electrons invariant mass Mll    
         if ((QCD_e_electrons_0_p4*E_k + QCD_e_electrons_1_p4*E_k).M()>=Mll_cut) {
         QCD_Lep1Pt[QCD_Pre_n] = (QCD_e_electrons_0_p4*E_k).Pt();
         QCD_Lep2Pt[QCD_Pre_n] = (QCD_e_electrons_1_p4*E_k).Pt();
         QCD_Eta1L[QCD_Pre_n] = abs(QCD_e_electrons_0_p4.Eta());
         QCD_Eta2L[QCD_Pre_n] = abs(QCD_e_electrons_1_p4.Eta());         
         QCD_Mll[QCD_Pre_n] = (QCD_e_electrons_0_p4*E_k + QCD_e_electrons_1_p4*E_k).M();
         QCD_WR[QCD_Pre_n] = (QCD_e_electrons_0_p4*E_k + QCD_e_electrons_1_p4*E_k + QCD_e_jets_0_p4*J_k + QCD_e_jets_1_p4*J_k ).M();
         QCD_NuR_l0[QCD_Pre_n] = (QCD_e_electrons_0_p4*E_k + QCD_e_jets_0_p4*J_k  + QCD_e_jets_1_p4*J_k ).M();
         QCD_NuR_l1[QCD_Pre_n] = (QCD_e_electrons_1_p4*E_k + QCD_e_jets_0_p4*J_k  + QCD_e_jets_1_p4*J_k ).M();    
         QCD_weight[QCD_Pre_n] = QCD_e_weight;
         QCD_Pre_nw = QCD_Pre_nw +QCD_e_weight;
		 QCD_Pre_n = QCD_Pre_n +1;

         }         
    //    }    
    // }    
      }     
     }
    }
   } 
  
  int   QCD_stat  = QCD_nEvents;
  float QCD_statw = QCD_nwEvents; 
  BG_statw = BG_statw + QCD_statw;
  
  
  
  
//=============================================================================================================================
// MC BG TTbar

  string TTb_fname = "/moscow31/heavynu/38x/TTJets_TuneZ2_7TeV-madgraph-tauola.root";
 
  TheEvent TTb_e(TTb_fname.c_str());
  if (!TTb_e) return;
  
  float TTb_weight = TTb_e.weight*Lum;
  int TTb_stat = TTb_e.sample.stat;
  float TTb_statw = TTb_e.sample.stat*TTb_weight; 
  BG_statw = BG_statw + TTb_statw;
  int TTb_nEvents = TTb_e.totalEvents(); 
  int TTb_Pre_n = 0; 
  
  for (int i = 0; i < TTb_nEvents; ++i) {
    TTb_e.readEvent(i); 
    //cuts on number of leptons and jets    
    if ((TTb_e.electrons.size() >= NofLept) && (TTb_e.jets.size() >= NofJets)) {
     //cuts on jets Pt      
     if (((TTb_e.jets[0].p4*J_k).Pt()>=Jet1Pt_cut) && ((TTb_e.jets[1].p4*J_k).Pt()>=Jet2Pt_cut))  { 
      //cuts on Eta of jets 
      if ((abs(TTb_e.jets[0].p4.Eta())<=Eta1J_cut) && (abs(TTb_e.jets[1].p4.Eta())<=Eta2J_cut))   {    
       // Isolation
       if ((TTb_e.electrons[0].isoHEEP() <= ISO_cut)&&(TTb_e.electrons[1].isoHEEP() <= ISO_cut)) {       
        //Cut on vertexes
        if ((abs(TTb_e.electrons[0].vertex.z()-TTb_e.electrons[1].vertex.z())<=Vertex_cut) && (abs(TTb_e.jets[0].vertex.z()-TTb_e.jets[1].vertex.z())<=Vertex_cut) 
             && (abs(TTb_e.electrons[0].vertex.z()-TTb_e.jets[1].vertex.z())<=Vertex_cut) && (abs(TTb_e.electrons[0].vertex.z()-TTb_e.jets[1].vertex.z())<=Vertex_cut)) {         //cuts on electrons invariant mass Mll    
         if ((TTb_e.electrons[0].p4*E_k  + TTb_e.electrons[1].p4*E_k  ).M()>=Mll_cut) {
          TTb_Pre_n = TTb_Pre_n +1;
         }         
        }    
       }    
      }     
     }
    }
   } 
   
  float TTb_Lep1Pt[TTb_Pre_n];
  float TTb_Lep2Pt[TTb_Pre_n];
  float TTb_Eta1L[TTb_Pre_n];
  float TTb_Eta2L[TTb_Pre_n]; 
  float TTb_Mll[TTb_Pre_n];
  float TTb_WR[TTb_Pre_n];
  float    TTb_NuR_l0[TTb_Pre_n];
  float    TTb_NuR_l1[TTb_Pre_n]; 
  
  TTb_Pre_n = 0;
   
     for (int i = 0; i < TTb_nEvents; ++i) {
    TTb_e.readEvent(i); 
    //cuts on number of leptons and jets    
    if ((TTb_e.electrons.size() >= NofLept) && (TTb_e.jets.size() >= NofJets)) {
     //cuts on jets Pt      
     if (((TTb_e.jets[0].p4*J_k).Pt()>=Jet1Pt_cut) && ((TTb_e.jets[1].p4*J_k).Pt()>=Jet2Pt_cut))  { 
      //cuts on Eta of jets 
      if ((abs(TTb_e.jets[0].p4.Eta())<=Eta1J_cut) && (abs(TTb_e.jets[1].p4.Eta())<=Eta2J_cut))   {    
       // Isolation
       if ((TTb_e.electrons[0].isoHEEP() <= ISO_cut)&&(TTb_e.electrons[1].isoHEEP() <= ISO_cut)) {       
        //Cut on vertexes
        if ((abs(TTb_e.electrons[0].vertex.z()-TTb_e.electrons[1].vertex.z())<=Vertex_cut) && (abs(TTb_e.jets[0].vertex.z()-TTb_e.jets[1].vertex.z())<=Vertex_cut) 
             && (abs(TTb_e.electrons[0].vertex.z()-TTb_e.jets[1].vertex.z())<=Vertex_cut) && (abs(TTb_e.electrons[0].vertex.z()-TTb_e.jets[1].vertex.z())<=Vertex_cut)) {
			 //cuts on electrons invariant mass Mll    
         if ((TTb_e.electrons[0].p4*E_k  + TTb_e.electrons[1].p4*E_k  ).M()>=Mll_cut) {
         TTb_Lep1Pt[TTb_Pre_n] = (TTb_e.electrons[0].p4*E_k  ).Pt();
         TTb_Lep2Pt[TTb_Pre_n] = (TTb_e.electrons[1].p4*E_k  ).Pt();
         TTb_Eta1L[TTb_Pre_n] = abs(TTb_e.electrons[0].p4.Eta());
         TTb_Eta2L[TTb_Pre_n] = abs(TTb_e.electrons[1].p4.Eta());         
         TTb_Mll[TTb_Pre_n] = (TTb_e.electrons[0].p4*E_k  + TTb_e.electrons[1].p4*E_k  ).M();
         TTb_WR[TTb_Pre_n] = (TTb_e.electrons[0].p4*E_k  + TTb_e.electrons[1].p4*E_k  + TTb_e.jets[0].p4*J_k + TTb_e.jets[1].p4*J_k).M();
         TTb_NuR_l0[TTb_Pre_n] = (TTb_e.electrons[0].p4*E_k  + TTb_e.jets[0].p4*J_k + TTb_e.jets[1].p4*J_k).M();
         TTb_NuR_l1[TTb_Pre_n] = (TTb_e.electrons[1].p4*E_k  + TTb_e.jets[0].p4*J_k + TTb_e.jets[1].p4*J_k).M();    
         TTb_Pre_n = TTb_Pre_n +1;     
         }         
        }    
       }    
      }     
     }
    }
   } 
  
  
//=============================================================================================================================
// MC BG Z+JETS

  string ZJ_fname;
  int fc_max = 21;
  int ZJ_nEvents[fc_max];
  //float ZJ_nwEvents[fc_max]; 
  int ZJ_Pre_n_max = 0;
  float ZJ_weight[fc_max];
  int ZJ_stat[fc_max];
  float ZJ_statw[fc_max]; 
  int ZJ_Pre_n[fc_max]; 

for (int fc = 0; fc < fc_max; ++fc) { 
  
   switch(fc)
    {
   case 0:
    ZJ_fname = "/moscow31/heavynu/38x/Z0Jets_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 1:
    ZJ_fname = "/moscow31/heavynu/38x/Z1Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 2:
    ZJ_fname = "/moscow31/heavynu/38x/Z1Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 3:
    ZJ_fname = "/moscow31/heavynu/38x/Z1Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;    
   case 4:
    ZJ_fname = "/moscow31/heavynu/38x/Z1Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 5:
    ZJ_fname = "/moscow31/heavynu/38x/Z2Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 6:
    ZJ_fname = "/moscow31/heavynu/38x/Z2Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 7:
    ZJ_fname = "/moscow31/heavynu/38x/Z2Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 8:
    ZJ_fname = "/moscow31/heavynu/38x/Z2Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;    
   case 9:
    ZJ_fname = "/moscow31/heavynu/38x/Z3Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 10:
    ZJ_fname = "/moscow31/heavynu/38x/Z3Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 11:
    ZJ_fname = "/moscow31/heavynu/38x/Z3Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 12:
    ZJ_fname = "/moscow31/heavynu/38x/Z3Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;    
   case 13:
    ZJ_fname = "/moscow31/heavynu/38x/Z4Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 14:
    ZJ_fname = "/moscow31/heavynu/38x/Z4Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 15:
    ZJ_fname = "/moscow31/heavynu/38x/Z4Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 16:
    ZJ_fname = "/moscow31/heavynu/38x/Z4Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 17:
    ZJ_fname = "/moscow31/heavynu/38x/Z5Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 18:
    ZJ_fname = "/moscow31/heavynu/38x/Z5Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 19:
    ZJ_fname = "/moscow31/heavynu/38x/Z5Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 20:
    ZJ_fname = "/moscow31/heavynu/38x/Z5Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
    }
    
  TheEvent ZJ_e(ZJ_fname.c_str());
  if (!ZJ_e) return;
  
  ZJ_nEvents[fc] = ZJ_e.totalEvents();
  
  ZJ_Pre_n[fc] = 0; 
  
  for (int i = 0; i < ZJ_nEvents[fc]; ++i) {
    ZJ_e.readEvent(i); 
    //cuts on number of leptons and jets    
    if ((ZJ_e.electrons.size() >= NofLept) && (ZJ_e.jets.size() >= NofJets)) {
     //cuts on jets Pt      
     if (((ZJ_e.jets[0].p4*J_k).Pt()>=Jet1Pt_cut) && ((ZJ_e.jets[1].p4*J_k).Pt()>=Jet2Pt_cut))  { 
      //cuts on Eta of jets 
      if ((abs(ZJ_e.jets[0].p4.Eta())<=Eta1J_cut) && (abs(ZJ_e.jets[1].p4.Eta())<=Eta2J_cut))   {    
       // Isolation
       if ((ZJ_e.electrons[0].isoHEEP() <= ISO_cut)&&(ZJ_e.electrons[1].isoHEEP() <= ISO_cut)) {       
        //Cut on vertexes
        if ((abs(ZJ_e.electrons[0].vertex.z()-ZJ_e.electrons[1].vertex.z())<=Vertex_cut) && (abs(ZJ_e.jets[0].vertex.z()-ZJ_e.jets[1].vertex.z())<=Vertex_cut) 
         && (abs(ZJ_e.electrons[0].vertex.z()-ZJ_e.jets[1].vertex.z())<=Vertex_cut) && (abs(ZJ_e.electrons[0].vertex.z()-ZJ_e.jets[1].vertex.z())<=Vertex_cut)) {         //cuts on electrons invariant mass Mll    
         if ((ZJ_e.electrons[0].p4*E_k  + ZJ_e.electrons[1].p4*E_k  ).M()>=Mll_cut) {
          ZJ_Pre_n[fc] = ZJ_Pre_n[fc] +1; 
         }
        }    
       }    
      }     
     }
    }
   } 
  if (ZJ_Pre_n_max < ZJ_Pre_n[fc] ) { ZJ_Pre_n_max = ZJ_Pre_n[fc]; }
  } 
    
  float ZJ_Lep1Pt[20][2000];
  float ZJ_Lep2Pt[20][2000];
  float ZJ_Eta1L[20][2000];
  float ZJ_Eta2L[20][2000];  
  float ZJ_Mll[20][2000];
  float ZJ_WR[20][2000];
  float ZJ_NuR_l0[20][2000];
  float ZJ_NuR_l1[20][2000];
  
  
  for (int fc = 0; fc < fc_max; ++fc) {
  
   switch(fc)
    {
   case 0:
    ZJ_fname = "/moscow31/heavynu/38x/Z0Jets_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 1:
    ZJ_fname = "/moscow31/heavynu/38x/Z1Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 2:
    ZJ_fname = "/moscow31/heavynu/38x/Z1Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 3:
    ZJ_fname = "/moscow31/heavynu/38x/Z1Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;    
   case 4:
    ZJ_fname = "/moscow31/heavynu/38x/Z1Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 5:
    ZJ_fname = "/moscow31/heavynu/38x/Z2Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 6:
    ZJ_fname = "/moscow31/heavynu/38x/Z2Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 7:
    ZJ_fname = "/moscow31/heavynu/38x/Z2Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 8:
    ZJ_fname = "/moscow31/heavynu/38x/Z2Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;    
   case 9:
    ZJ_fname = "/moscow31/heavynu/38x/Z3Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 10:
    ZJ_fname = "/moscow31/heavynu/38x/Z3Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 11:
    ZJ_fname = "/moscow31/heavynu/38x/Z3Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 12:
    ZJ_fname = "/moscow31/heavynu/38x/Z3Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;    
   case 13:
    ZJ_fname = "/moscow31/heavynu/38x/Z4Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 14:
    ZJ_fname = "/moscow31/heavynu/38x/Z4Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 15:
    ZJ_fname = "/moscow31/heavynu/38x/Z4Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 16:
    ZJ_fname = "/moscow31/heavynu/38x/Z4Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 17:
    ZJ_fname = "/moscow31/heavynu/38x/Z5Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 18:
    ZJ_fname = "/moscow31/heavynu/38x/Z5Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 19:
    ZJ_fname = "/moscow31/heavynu/38x/Z5Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 20:
    ZJ_fname = "/moscow31/heavynu/38x/Z5Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
    }
 
 
  TheEvent ZJ_e(ZJ_fname.c_str());
  if (!ZJ_e) return;
  
  ZJ_weight[fc] = ZJ_e.weight*Lum*Z_tune;
  ZJ_stat[fc] = ZJ_e.sample.stat;
  ZJ_statw[fc] = ZJ_e.sample.stat*ZJ_weight[fc]; 
  BG_statw = BG_statw + ZJ_statw[fc];
  ZJ_nEvents[fc] = ZJ_e.totalEvents();
 
  ZJ_Pre_n[fc] = 0; 
  
  for (int i = 0; i < ZJ_nEvents[fc]; ++i) {
    ZJ_e.readEvent(i); 
    //cuts on number of leptons and jets    
    if ((ZJ_e.electrons.size() >= NofLept) && (ZJ_e.jets.size() >= NofJets)) {
     //cuts on jets Pt      
     if (((ZJ_e.jets[0].p4*J_k).Pt()>=Jet1Pt_cut) && ((ZJ_e.jets[1].p4*J_k).Pt()>=Jet2Pt_cut))  { 
      //cuts on Eta of jets 
      if ((abs(ZJ_e.jets[0].p4.Eta())<=Eta1J_cut) && (abs(ZJ_e.jets[1].p4.Eta())<=Eta2J_cut))   {    
       // Isolation
       if ((ZJ_e.electrons[0].isoHEEP() <= ISO_cut)&&(ZJ_e.electrons[1].isoHEEP() <= ISO_cut)) {       
        //Cut on vertexes
        if ((abs(ZJ_e.electrons[0].vertex.z()-ZJ_e.electrons[1].vertex.z())<=Vertex_cut) && (abs(ZJ_e.jets[0].vertex.z()-ZJ_e.jets[1].vertex.z())<=Vertex_cut) 
         && (abs(ZJ_e.electrons[0].vertex.z()-ZJ_e.jets[1].vertex.z())<=Vertex_cut) && (abs(ZJ_e.electrons[0].vertex.z()-ZJ_e.jets[1].vertex.z())<=Vertex_cut)) {         //cuts on electrons invariant mass Mll    
         //cuts on electrons invariant mass Mll    
         if ((ZJ_e.electrons[0].p4*E_k  + ZJ_e.electrons[1].p4*E_k  ).M()>=Mll_cut) {
         ZJ_Lep1Pt[fc][ZJ_Pre_n[fc]] = (ZJ_e.electrons[0].p4*E_k  ).Pt();
         ZJ_Lep2Pt[fc][ZJ_Pre_n[fc]] = (ZJ_e.electrons[1].p4*E_k  ).Pt();
         ZJ_Eta1L[fc][ZJ_Pre_n[fc]] = abs(ZJ_e.electrons[0].p4.Eta());
         ZJ_Eta2L[fc][ZJ_Pre_n[fc]] = abs(ZJ_e.electrons[1].p4.Eta());         
         ZJ_Mll[fc][ZJ_Pre_n[fc]] = (ZJ_e.electrons[0].p4*E_k  + ZJ_e.electrons[1].p4*E_k  ).M();
         ZJ_WR[fc][ZJ_Pre_n[fc]] = (ZJ_e.electrons[0].p4*E_k  + ZJ_e.electrons[1].p4*E_k  + ZJ_e.jets[0].p4*J_k + ZJ_e.jets[1].p4*J_k).M();
         ZJ_NuR_l0[fc][ZJ_Pre_n[fc]] = (ZJ_e.electrons[0].p4*E_k  + ZJ_e.jets[0].p4*J_k + ZJ_e.jets[1].p4*J_k).M();
         ZJ_NuR_l1[fc][ZJ_Pre_n[fc]] = (ZJ_e.electrons[1].p4*E_k  + ZJ_e.jets[0].p4*J_k + ZJ_e.jets[1].p4*J_k).M();
         ZJ_Pre_n[fc] = ZJ_Pre_n[fc] +1;
         }
        }    
       }    
      }     
     }
    }
   } 
  }
 
  

//============================================================================================================================
// MC BG WJ

  string WJ_fname = "/moscow31/heavynu/38x/WJets_7TeV-madgraph-tauola.root";
 
  TheEvent WJ_e(WJ_fname.c_str());
  if (!WJ_e) return;
  
  float WJ_weight = WJ_e.weight*Lum;
  int WJ_stat = WJ_e.sample.stat;
  float WJ_statw = WJ_e.sample.stat*WJ_weight; 
  BG_statw = BG_statw + WJ_statw;
  int WJ_nEvents = WJ_e.totalEvents(); 
  int WJ_Pre_n = 0; 
  
  for (int i = 0; i < WJ_nEvents; ++i) {
    WJ_e.readEvent(i); 
    //cuts on number of leptons and jets    
    if ((WJ_e.electrons.size() >= NofLept) && (WJ_e.jets.size() >= NofJets)) {
     //cuts on jets Pt      
     if (((WJ_e.jets[0].p4*J_k).Pt()>=Jet1Pt_cut) && ((WJ_e.jets[1].p4*J_k).Pt()>=Jet2Pt_cut))  { 
      //cuts on Eta of jets 
      if ((abs(WJ_e.jets[0].p4.Eta())<=Eta1J_cut) && (abs(WJ_e.jets[1].p4.Eta())<=Eta2J_cut))   {    
       // Isolation
       if ((WJ_e.electrons[0].isoHEEP() <= ISO_cut)&&(WJ_e.electrons[1].isoHEEP() <= ISO_cut)) {       
        //Cut on vertexes
        if ((abs(WJ_e.electrons[0].vertex.z()-WJ_e.electrons[1].vertex.z())<=Vertex_cut) && (abs(WJ_e.jets[0].vertex.z()-WJ_e.jets[1].vertex.z())<=Vertex_cut) 
         && (abs(WJ_e.electrons[0].vertex.z()-WJ_e.jets[1].vertex.z())<=Vertex_cut) && (abs(WJ_e.electrons[0].vertex.z()-WJ_e.jets[1].vertex.z())<=Vertex_cut)) {         //cuts on electrons invariant mass Mll    
         //cuts on electrons invariant mass Mll    
         if ((WJ_e.electrons[0].p4*E_k  + WJ_e.electrons[1].p4*E_k  ).M()>=Mll_cut) {
          WJ_Pre_n = WJ_Pre_n +1;
         }         
        }    
       }    
      }     
     }
    }
   } 
   
  float WJ_Lep1Pt[WJ_Pre_n];
  float WJ_Lep2Pt[WJ_Pre_n];
  float WJ_Eta1L[WJ_Pre_n];
  float WJ_Eta2L[WJ_Pre_n]; 
  float WJ_Mll[WJ_Pre_n];
  float WJ_WR[WJ_Pre_n];
  float WJ_NuR_l0[WJ_Pre_n];
  float WJ_NuR_l1[WJ_Pre_n]; 
  
  WJ_Pre_n = 0;
   
     for (int i = 0; i < WJ_nEvents; ++i) {
    WJ_e.readEvent(i); 
    //cuts on number of leptons and jets    
    if ((WJ_e.electrons.size() >= NofLept) && (WJ_e.jets.size() >= NofJets)) {
     //cuts on jets Pt      
     if (((WJ_e.jets[0].p4*J_k).Pt()>=Jet1Pt_cut) && ((WJ_e.jets[1].p4*J_k).Pt()>=Jet2Pt_cut))  { 
      //cuts on Eta of jets 
      if ((abs(WJ_e.jets[0].p4.Eta())<=Eta1J_cut) && (abs(WJ_e.jets[1].p4.Eta())<=Eta2J_cut))   {    
       // Isolation
       if ((WJ_e.electrons[0].isoHEEP() <= ISO_cut)&&(WJ_e.electrons[1].isoHEEP() <= ISO_cut)) {       
        //Cut on vertexes
        if ((abs(WJ_e.electrons[0].vertex.z()-WJ_e.electrons[1].vertex.z())<=Vertex_cut) && (abs(WJ_e.jets[0].vertex.z()-WJ_e.jets[1].vertex.z())<=Vertex_cut) 
         && (abs(WJ_e.electrons[0].vertex.z()-WJ_e.jets[1].vertex.z())<=Vertex_cut) && (abs(WJ_e.electrons[0].vertex.z()-WJ_e.jets[1].vertex.z())<=Vertex_cut)) {         //cuts on electrons invariant mass Mll    
          //cuts on electrons invariant mass Mll    
         if ((WJ_e.electrons[0].p4*E_k  + WJ_e.electrons[1].p4*E_k  ).M()>=Mll_cut) {
         WJ_Lep1Pt[WJ_Pre_n] = (WJ_e.electrons[0].p4*E_k  ).Pt();
         WJ_Lep2Pt[WJ_Pre_n] = (WJ_e.electrons[1].p4*E_k  ).Pt();
         WJ_Eta1L[WJ_Pre_n] = abs(WJ_e.electrons[0].p4.Eta());
         WJ_Eta2L[WJ_Pre_n] = abs(WJ_e.electrons[1].p4.Eta());         
         WJ_Mll[WJ_Pre_n] = (WJ_e.electrons[0].p4*E_k  + WJ_e.electrons[1].p4*E_k  ).M();
         WJ_WR[WJ_Pre_n] = (WJ_e.electrons[0].p4*E_k  + WJ_e.electrons[1].p4*E_k  + WJ_e.jets[0].p4*J_k + WJ_e.jets[1].p4*J_k).M();
         WJ_NuR_l0[WJ_Pre_n] = (WJ_e.electrons[0].p4*E_k  + WJ_e.jets[0].p4*J_k + WJ_e.jets[1].p4*J_k).M();
         WJ_NuR_l1[WJ_Pre_n] = (WJ_e.electrons[1].p4*E_k  + WJ_e.jets[0].p4*J_k + WJ_e.jets[1].p4*J_k).M();    
         WJ_Pre_n = WJ_Pre_n +1;     
         }         
        }    
       }    
      }     
     }
    }
   } 
/* 
 //=============================================================================================================================
// MC BG W+JETS

  string WJ_fname;
  int fc_max_1 = 21;
  int WJ_nEvents[fc_max];
  //float WJ_nwEvents[fc_max]; 
  int WJ_Pre_n_max = 0;
  float WJ_weight[fc_max];
  int WJ_stat[fc_max];
  float WJ_statw[fc_max]; 
  int WJ_Pre_n[fc_max]; 

for (int fc = 0; fc < fc_max_1; ++fc) { 
  
   switch(fc)
    {
   case 0:
    WJ_fname = "/moscow31/heavynu/38x/W0Jets_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 1:
    WJ_fname = "/moscow31/heavynu/38x/W1Jets_ptW-0to100_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 2:
    WJ_fname = "/moscow31/heavynu/38x/W1Jets_ptW-100to300_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 3:
    WJ_fname = "/moscow31/heavynu/38x/W1Jets_ptW-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;    
   case 4:
    WJ_fname = "/moscow31/heavynu/38x/W1Jets_ptW-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 5:
    WJ_fname = "/moscow31/heavynu/38x/W2Jets_ptW-0to100_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 6:
    WJ_fname = "/moscow31/heavynu/38x/W2Jets_ptW-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 7:
    WJ_fname = "/moscow31/heavynu/38x/W2Jets_ptW-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 8:
    WJ_fname = "/moscow31/heavynu/38x/W2Jets_ptW-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;    
   case 9:
    WJ_fname = "/moscow31/heavynu/38x/W3Jets_ptW-0to100_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 10:
    WJ_fname = "/moscow31/heavynu/38x/W3Jets_ptW-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 11:
    WJ_fname = "/moscow31/heavynu/38x/W3Jets_ptW-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 12:
    WJ_fname = "/moscow31/heavynu/38x/W3Jets_ptW-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;    
   case 13:
    WJ_fname = "/moscow31/heavynu/38x/W4Jets_ptW-0to100_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 14:
    WJ_fname = "/moscow31/heavynu/38x/W4Jets_ptW-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 15:
    WJ_fname = "/moscow31/heavynu/38x/W4Jets_ptW-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 16:
    WJ_fname = "/moscow31/heavynu/38x/W4Jets_ptW-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 17:
    WJ_fname = "/moscow31/heavynu/38x/W5Jets_ptW-0to100_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 18:
    WJ_fname = "/moscow31/heavynu/38x/W5Jets_ptW-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 19:
    WJ_fname = "/moscow31/heavynu/38x/W5Jets_ptW-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 20:
    WJ_fname = "/moscow31/heavynu/38x/W5Jets_ptW-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
    }
    
  TheEvent WJ_e(WJ_fname.c_str());
  if (!WJ_e) return;
  
  WJ_nEvents[fc] = WJ_e.totalEvents();
  
  WJ_Pre_n[fc] = 0; 
  
  for (int i = 0; i < WJ_nEvents[fc]; ++i) {
    WJ_e.readEvent(i); 
    //cuts on number of leptons and jets    
    if ((WJ_e.electrons.size() >= NofLept) && (WJ_e.jets.size() >= NofJets)) {
     //cuts on jets Pt      
     if (((WJ_e.jets[0].p4*J_k).Pt()>=Jet1Pt_cut) && ((WJ_e.jets[1].p4*J_k).Pt()>=Jet2Pt_cut))  { 
      //cuts on Eta of jets 
      if ((abs(WJ_e.jets[0].p4.Eta())<=Eta1J_cut) && (abs(WJ_e.jets[1].p4.Eta())<=Eta2J_cut))   {    
       // Isolation
       if ((WJ_e.electrons[0].isoHEEP() <= ISO_cut)&&(WJ_e.electrons[1].isoHEEP() <= ISO_cut)) {       
        //Cut on vertexes
        if ((abs(WJ_e.electrons[0].vertex.z()-WJ_e.electrons[1].vertex.z())<=Vertex_cut) && (abs(WJ_e.jets[0].vertex.z()-WJ_e.jets[1].vertex.z())<=Vertex_cut) 
         && (abs(WJ_e.electrons[0].vertex.z()-WJ_e.jets[1].vertex.z())<=Vertex_cut) && (abs(WJ_e.electrons[0].vertex.z()-WJ_e.jets[1].vertex.z())<=Vertex_cut)) {         //cuts on electrons invariant mass Mll    
         if ((WJ_e.electrons[0].p4*E_k  + WJ_e.electrons[1].p4*E_k  ).M()>=Mll_cut) {
          WJ_Pre_n[fc] = WJ_Pre_n[fc] +1; 
         }
        }    
       }    
      }     
     }
    }
   } 
  if (WJ_Pre_n_max < WJ_Pre_n[fc] ) { WJ_Pre_n_max = WJ_Pre_n[fc]; }
  } 
 cout<< "WJ_Pre_n_max = "   << WJ_Pre_n_max <<endl;
  float WJ_Lep1Pt[20][1000000];
  float WJ_Lep2Pt[20][1000000];
  float WJ_Eta1L[20][1000000];
  float WJ_Eta2L[20][1000000];  
  float WJ_Mll[20][1000000];
  float WJ_WR[20][1000000];
  float WJ_NuR_l0[20][1000000];
  float WJ_NuR_l1[20][1000000];
  
  
  for (int fc = 0; fc < fc_max_1; ++fc) {
  
   switch(fc)
    {
   case 0:
    WJ_fname = "/moscow31/heavynu/38x/W0Jets_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 1:
    WJ_fname = "/moscow31/heavynu/38x/W1Jets_ptW-0to100_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 2:
    WJ_fname = "/moscow31/heavynu/38x/W1Jets_ptW-100to300_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 3:
    WJ_fname = "/moscow31/heavynu/38x/W1Jets_ptW-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;    
   case 4:
    WJ_fname = "/moscow31/heavynu/38x/W1Jets_ptW-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 5:
    WJ_fname = "/moscow31/heavynu/38x/W2Jets_ptW-0to100_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 6:
    WJ_fname = "/moscow31/heavynu/38x/W2Jets_ptW-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 7:
    WJ_fname = "/moscow31/heavynu/38x/W2Jets_ptW-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 8:
    WJ_fname = "/moscow31/heavynu/38x/W2Jets_ptW-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;    
   case 9:
    WJ_fname = "/moscow31/heavynu/38x/W3Jets_ptW-0to100_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 10:
    WJ_fname = "/moscow31/heavynu/38x/W3Jets_ptW-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 11:
    WJ_fname = "/moscow31/heavynu/38x/W3Jets_ptW-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 12:
    WJ_fname = "/moscow31/heavynu/38x/W3Jets_ptW-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;    
   case 13:
    WJ_fname = "/moscow31/heavynu/38x/W4Jets_ptW-0to100_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 14:
    WJ_fname = "/moscow31/heavynu/38x/W4Jets_ptW-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 15:
    WJ_fname = "/moscow31/heavynu/38x/W4Jets_ptW-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 16:
    WJ_fname = "/moscow31/heavynu/38x/W4Jets_ptW-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 17:
    WJ_fname = "/moscow31/heavynu/38x/W5Jets_ptW-0to100_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 18:
    WJ_fname = "/moscow31/heavynu/38x/W5Jets_ptW-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 19:
    WJ_fname = "/moscow31/heavynu/38x/W5Jets_ptW-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 20:
    WJ_fname = "/moscow31/heavynu/38x/W5Jets_ptW-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
    }
 
 
  TheEvent WJ_e(WJ_fname.c_str());
  if (!WJ_e) return;
  
  WJ_weight[fc] = WJ_e.weight*Lum*Z_tune;
  WJ_stat[fc] = WJ_e.sample.stat;
  WJ_statw[fc] = WJ_e.sample.stat*WJ_weight[fc]; 
  BG_statw = BG_statw + WJ_statw[fc];
  WJ_nEvents[fc] = WJ_e.totalEvents();
 
  WJ_Pre_n[fc] = 0; 
  
  for (int i = 0; i < WJ_nEvents[fc]; ++i) {
    WJ_e.readEvent(i); 
    //cuts on number of leptons and jets    
    if ((WJ_e.electrons.size() >= NofLept) && (WJ_e.jets.size() >= NofJets)) {
     //cuts on jets Pt      
     if (((WJ_e.jets[0].p4*J_k).Pt()>=Jet1Pt_cut) && ((WJ_e.jets[1].p4*J_k).Pt()>=Jet2Pt_cut))  { 
      //cuts on Eta of jets 
      if ((abs(WJ_e.jets[0].p4.Eta())<=Eta1J_cut) && (abs(WJ_e.jets[1].p4.Eta())<=Eta2J_cut))   {    
       // Isolation
       if ((WJ_e.electrons[0].isoHEEP() <= ISO_cut)&&(WJ_e.electrons[1].isoHEEP() <= ISO_cut)) {       
        //Cut on vertexes
        if ((abs(WJ_e.electrons[0].vertex.z()-WJ_e.electrons[1].vertex.z())<=Vertex_cut) && (abs(WJ_e.jets[0].vertex.z()-WJ_e.jets[1].vertex.z())<=Vertex_cut) 
         && (abs(WJ_e.electrons[0].vertex.z()-WJ_e.jets[1].vertex.z())<=Vertex_cut) && (abs(WJ_e.electrons[0].vertex.z()-WJ_e.jets[1].vertex.z())<=Vertex_cut)) {         //cuts on electrons invariant mass Mll    
         //cuts on electrons invariant mass Mll    
         if ((WJ_e.electrons[0].p4*E_k  + WJ_e.electrons[1].p4*E_k  ).M()>=Mll_cut) {
         WJ_Lep1Pt[fc][WJ_Pre_n[fc]] = (WJ_e.electrons[0].p4*E_k  ).Pt();
         WJ_Lep2Pt[fc][WJ_Pre_n[fc]] = (WJ_e.electrons[1].p4*E_k  ).Pt();
         WJ_Eta1L[fc][WJ_Pre_n[fc]] = abs(WJ_e.electrons[0].p4.Eta());
         WJ_Eta2L[fc][WJ_Pre_n[fc]] = abs(WJ_e.electrons[1].p4.Eta());         
         WJ_Mll[fc][WJ_Pre_n[fc]] = (WJ_e.electrons[0].p4*E_k  + WJ_e.electrons[1].p4*E_k  ).M();
         WJ_WR[fc][WJ_Pre_n[fc]] = (WJ_e.electrons[0].p4*E_k  + WJ_e.electrons[1].p4*E_k  + WJ_e.jets[0].p4*J_k + WJ_e.jets[1].p4*J_k).M();
         WJ_NuR_l0[fc][WJ_Pre_n[fc]] = (WJ_e.electrons[0].p4*E_k  + WJ_e.jets[0].p4*J_k + WJ_e.jets[1].p4*J_k).M();
         WJ_NuR_l1[fc][WJ_Pre_n[fc]] = (WJ_e.electrons[1].p4*E_k  + WJ_e.jets[0].p4*J_k + WJ_e.jets[1].p4*J_k).M();
         WJ_Pre_n[fc] = WJ_Pre_n[fc] +1;
         }
        }    
       }    
      }     
     }
    }
   } 
  }
 */  
//============================================================================================================================ 
// MC BG ZZ
 
 

  string ZZ_fname = "/moscow31/heavynu/38x/ZZtoAnything_TuneZ2_7TeV-pythia6-tauola.root";
 
  TheEvent ZZ_e(ZZ_fname.c_str());
  if (!ZZ_e) return;
  
  float ZZ_weight = ZZ_e.weight*Lum;
  int ZZ_stat = ZZ_e.sample.stat;
  float ZZ_statw = ZZ_e.sample.stat*ZZ_weight; 
  BG_statw = BG_statw + ZZ_statw;
  int ZZ_nEvents = ZZ_e.totalEvents(); 
  int ZZ_Pre_n = 0; 
  
  for (int i = 0; i < ZZ_nEvents; ++i) {
    ZZ_e.readEvent(i); 
    //cuts on number of leptons and jets    
    if ((ZZ_e.electrons.size() >= NofLept) && (ZZ_e.jets.size() >= NofJets)) {
     //cuts on jets Pt      
     if (((ZZ_e.jets[0].p4*J_k).Pt()>=Jet1Pt_cut) && ((ZZ_e.jets[1].p4*J_k).Pt()>=Jet2Pt_cut))  { 
      //cuts on Eta of jets 
      if ((abs(ZZ_e.jets[0].p4.Eta())<=Eta1J_cut) && (abs(ZZ_e.jets[1].p4.Eta())<=Eta2J_cut))   {    
       // Isolation
       if ((ZZ_e.electrons[0].isoHEEP() <= ISO_cut)&&(ZZ_e.electrons[1].isoHEEP() <= ISO_cut)) {       
        //Cut on vertexes
        if ((abs(ZZ_e.electrons[0].vertex.z()-ZZ_e.electrons[1].vertex.z())<=Vertex_cut) && (abs(ZZ_e.jets[0].vertex.z()-ZZ_e.jets[1].vertex.z())<=Vertex_cut) 
         && (abs(ZZ_e.electrons[0].vertex.z()-ZZ_e.jets[1].vertex.z())<=Vertex_cut) && (abs(ZZ_e.electrons[0].vertex.z()-ZZ_e.jets[1].vertex.z())<=Vertex_cut)) {         //cuts on electrons invariant mass Mll    
         //cuts on electrons invariant mass Mll    
         if ((ZZ_e.electrons[0].p4*E_k  + ZZ_e.electrons[1].p4*E_k  ).M()>=Mll_cut) {
          ZZ_Pre_n = ZZ_Pre_n +1;
         }         
        }    
       }    
      }     
     }
    }
   } 
   
  float ZZ_Lep1Pt[ZZ_Pre_n];
  float ZZ_Lep2Pt[ZZ_Pre_n];
  float ZZ_Eta1L[ZZ_Pre_n];
  float ZZ_Eta2L[ZZ_Pre_n]; 
  float ZZ_Mll[ZZ_Pre_n];
  float ZZ_WR[ZZ_Pre_n];
  float    ZZ_NuR_l0[ZZ_Pre_n];
  float    ZZ_NuR_l1[ZZ_Pre_n]; 
  
  ZZ_Pre_n = 0;
   
     for (int i = 0; i < ZZ_nEvents; ++i) {
    ZZ_e.readEvent(i); 
    //cuts on number of leptons and jets    
    if ((ZZ_e.electrons.size() >= NofLept) && (ZZ_e.jets.size() >= NofJets)) {
     //cuts on jets Pt      
     if (((ZZ_e.jets[0].p4*J_k).Pt()>=Jet1Pt_cut) && ((ZZ_e.jets[1].p4*J_k).Pt()>=Jet2Pt_cut))  { 
      //cuts on Eta of jets 
      if ((abs(ZZ_e.jets[0].p4.Eta())<=Eta1J_cut) && (abs(ZZ_e.jets[1].p4.Eta())<=Eta2J_cut))   {    
       // Isolation
       if ((ZZ_e.electrons[0].isoHEEP() <= ISO_cut)&&(ZZ_e.electrons[1].isoHEEP() <= ISO_cut)) {       
        //Cut on vertexes
        if ((abs(ZZ_e.electrons[0].vertex.z()-ZZ_e.electrons[1].vertex.z())<=Vertex_cut) && (abs(ZZ_e.jets[0].vertex.z()-ZZ_e.jets[1].vertex.z())<=Vertex_cut) 
         && (abs(ZZ_e.electrons[0].vertex.z()-ZZ_e.jets[1].vertex.z())<=Vertex_cut) && (abs(ZZ_e.electrons[0].vertex.z()-ZZ_e.jets[1].vertex.z())<=Vertex_cut)) {         //cuts on electrons invariant mass Mll    
          //cuts on electrons invariant mass Mll    
         if ((ZZ_e.electrons[0].p4*E_k  + ZZ_e.electrons[1].p4*E_k  ).M()>=Mll_cut) {
         ZZ_Lep1Pt[ZZ_Pre_n] = (ZZ_e.electrons[0].p4*E_k  ).Pt();
         ZZ_Lep2Pt[ZZ_Pre_n] = (ZZ_e.electrons[1].p4*E_k  ).Pt();
         ZZ_Eta1L[ZZ_Pre_n] = abs(ZZ_e.electrons[0].p4.Eta());
         ZZ_Eta2L[ZZ_Pre_n] = abs(ZZ_e.electrons[1].p4.Eta());         
         ZZ_Mll[ZZ_Pre_n] = (ZZ_e.electrons[0].p4*E_k  + ZZ_e.electrons[1].p4*E_k  ).M();
         ZZ_WR[ZZ_Pre_n] = (ZZ_e.electrons[0].p4*E_k  + ZZ_e.electrons[1].p4*E_k  + ZZ_e.jets[0].p4*J_k + ZZ_e.jets[1].p4*J_k).M();
         ZZ_NuR_l0[ZZ_Pre_n] = (ZZ_e.electrons[0].p4*E_k  + ZZ_e.jets[0].p4*J_k + ZZ_e.jets[1].p4*J_k).M();
         ZZ_NuR_l1[ZZ_Pre_n] = (ZZ_e.electrons[1].p4*E_k  + ZZ_e.jets[0].p4*J_k + ZZ_e.jets[1].p4*J_k).M();    
         ZZ_Pre_n = ZZ_Pre_n +1;     
         }         
        }    
       }    
      }     
     }
    }
   } 

   

   
//============================================================================================================================ 
// MC BG ZW
 
 

  string ZW_fname = "/moscow31/heavynu/38x/WZtoAnything_TuneZ2_7TeV-pythia6-tauola.root";
 
  TheEvent ZW_e(ZW_fname.c_str());
  if (!ZW_e) return;
  
  float ZW_weight = ZW_e.weight*Lum;
  int ZW_stat = ZW_e.sample.stat;
  float ZW_statw = ZW_e.sample.stat*ZW_weight; 
  BG_statw = BG_statw + ZW_statw;
  int ZW_nEvents = ZW_e.totalEvents(); 
  int ZW_Pre_n = 0; 
  
  for (int i = 0; i < ZW_nEvents; ++i) {
    ZW_e.readEvent(i); 
    //cuts on number of leptons and jets    
    if ((ZW_e.electrons.size() >= NofLept) && (ZW_e.jets.size() >= NofJets)) {
     //cuts on jets Pt      
     if (((ZW_e.jets[0].p4*J_k).Pt()>=Jet1Pt_cut) && ((ZW_e.jets[1].p4*J_k).Pt()>=Jet2Pt_cut))  { 
      //cuts on Eta of jets 
      if ((abs(ZW_e.jets[0].p4.Eta())<=Eta1J_cut) && (abs(ZW_e.jets[1].p4.Eta())<=Eta2J_cut))   {    
       // Isolation
       if ((ZW_e.electrons[0].isoHEEP() <= ISO_cut)&&(ZW_e.electrons[1].isoHEEP() <= ISO_cut)) {       
        //Cut on vertexes
        if ((abs(ZW_e.electrons[0].vertex.z()-ZW_e.electrons[1].vertex.z())<=Vertex_cut) && (abs(ZW_e.jets[0].vertex.z()-ZW_e.jets[1].vertex.z())<=Vertex_cut) 
         && (abs(ZW_e.electrons[0].vertex.z()-ZW_e.jets[1].vertex.z())<=Vertex_cut) && (abs(ZW_e.electrons[0].vertex.z()-ZW_e.jets[1].vertex.z())<=Vertex_cut)) {         //cuts on electrons invariant mass Mll    
         //cuts on electrons invariant mass Mll    
         if ((ZW_e.electrons[0].p4*E_k  + ZW_e.electrons[1].p4*E_k  ).M()>=Mll_cut) {
          ZW_Pre_n = ZW_Pre_n +1;
         }         
        }    
       }    
      }     
     }
    }
   } 
   
  float ZW_Lep1Pt[ZW_Pre_n];
  float ZW_Lep2Pt[ZW_Pre_n];
  float ZW_Eta1L[ZW_Pre_n];
  float ZW_Eta2L[ZW_Pre_n]; 
  float ZW_Mll[ZW_Pre_n];
  float ZW_WR[ZW_Pre_n];
  float    ZW_NuR_l0[ZW_Pre_n];
  float    ZW_NuR_l1[ZW_Pre_n]; 
  
  ZW_Pre_n = 0;
   
     for (int i = 0; i < ZW_nEvents; ++i) {
    ZW_e.readEvent(i); 
    //cuts on number of leptons and jets    
    if ((ZW_e.electrons.size() >= NofLept) && (ZW_e.jets.size() >= NofJets)) {
     //cuts on jets Pt      
     if (((ZW_e.jets[0].p4*J_k).Pt()>=Jet1Pt_cut) && ((ZW_e.jets[1].p4*J_k).Pt()>=Jet2Pt_cut))  { 
      //cuts on Eta of jets 
      if ((abs(ZW_e.jets[0].p4.Eta())<=Eta1J_cut) && (abs(ZW_e.jets[1].p4.Eta())<=Eta2J_cut))   {    
       // Isolation
       if ((ZW_e.electrons[0].isoHEEP() <= ISO_cut)&&(ZW_e.electrons[0].isoHEEP() <= ISO_cut)) {       
        //Cut on vertexes
        if ((abs(ZW_e.electrons[0].vertex.z()-ZW_e.electrons[1].vertex.z())<=Vertex_cut) && (abs(ZW_e.jets[0].vertex.z()-ZW_e.jets[1].vertex.z())<=Vertex_cut) 
         && (abs(ZW_e.electrons[0].vertex.z()-ZW_e.jets[1].vertex.z())<=Vertex_cut) && (abs(ZW_e.electrons[0].vertex.z()-ZW_e.jets[1].vertex.z())<=Vertex_cut)) {         //cuts on electrons invariant mass Mll    
           //cuts on electrons invariant mass Mll    
         if ((ZW_e.electrons[0].p4*E_k  + ZW_e.electrons[1].p4*E_k  ).M()>=Mll_cut) {
         ZW_Lep1Pt[ZW_Pre_n] = (ZW_e.electrons[0].p4*E_k  ).Pt();
         ZW_Lep2Pt[ZW_Pre_n] = (ZW_e.electrons[1].p4*E_k  ).Pt();
         ZW_Eta1L[ZW_Pre_n] = abs(ZW_e.electrons[0].p4.Eta());
         ZW_Eta2L[ZW_Pre_n] = abs(ZW_e.electrons[1].p4.Eta());         
         ZW_Mll[ZW_Pre_n] = (ZW_e.electrons[0].p4*E_k  + ZW_e.electrons[1].p4*E_k  ).M();
         ZW_WR[ZW_Pre_n] = (ZW_e.electrons[0].p4*E_k  + ZW_e.electrons[1].p4*E_k  + ZW_e.jets[0].p4*J_k + ZW_e.jets[1].p4*J_k).M();
         ZW_NuR_l0[ZW_Pre_n] = (ZW_e.electrons[0].p4*E_k  + ZW_e.jets[0].p4*J_k + ZW_e.jets[1].p4*J_k).M();
         ZW_NuR_l1[ZW_Pre_n] = (ZW_e.electrons[1].p4*E_k  + ZW_e.jets[0].p4*J_k + ZW_e.jets[1].p4*J_k).M();    
         ZW_Pre_n = ZW_Pre_n +1;     
         }         
        }    
       }    
      }     
     }
    }
   } 

   

//============================================================================================================================ 
// MC BG WW
 
 

  string WW_fname = "/moscow31/heavynu/38x/WWtoAnything_TuneZ2_7TeV-pythia6-tauola.root";
 
  TheEvent WW_e(WW_fname.c_str());
  if (!WW_e) return;
  
  float WW_weight = WW_e.weight*Lum;
  int WW_stat = WW_e.sample.stat;
  float WW_statw = WW_e.sample.stat*WW_weight; 
  BG_statw = BG_statw + WW_statw;
  int WW_nEvents = WW_e.totalEvents(); 
  int WW_Pre_n = 0; 
  
  for (int i = 0; i < WW_nEvents; ++i) {
    WW_e.readEvent(i); 
    //cuts on number of leptons and jets    
    if ((WW_e.electrons.size() >= NofLept) && (WW_e.jets.size() >= NofJets)) {
     //cuts on jets Pt      
     if (((WW_e.jets[0].p4*J_k).Pt()>=Jet1Pt_cut) && ((WW_e.jets[1].p4*J_k).Pt()>=Jet2Pt_cut))  { 
      //cuts on Eta of jets 
      if ((abs(WW_e.jets[0].p4.Eta())<=Eta1J_cut) && (abs(WW_e.jets[1].p4.Eta())<=Eta2J_cut))   {    
       // Isolation
       if ((WW_e.electrons[0].isoHEEP() <= ISO_cut)&&(WW_e.electrons[1].isoHEEP() <= ISO_cut)) {       
        //Cut on vertexes
        if ((abs(WW_e.electrons[0].vertex.z()-WW_e.electrons[1].vertex.z())<=Vertex_cut) && (abs(WW_e.jets[0].vertex.z()-WW_e.jets[1].vertex.z())<=Vertex_cut) 
         && (abs(WW_e.electrons[0].vertex.z()-WW_e.jets[1].vertex.z())<=Vertex_cut) && (abs(WW_e.electrons[0].vertex.z()-WW_e.jets[1].vertex.z())<=Vertex_cut)) {         //cuts on electrons invariant mass Mll    
          //cuts on electrons invariant mass Mll    
         if ((WW_e.electrons[0].p4*E_k  + WW_e.electrons[1].p4*E_k  ).M()>=Mll_cut) {
          WW_Pre_n = WW_Pre_n +1;
         }         
        }    
       }    
      }     
     }
    }
   } 
   
  float WW_Lep1Pt[WW_Pre_n];
  float WW_Lep2Pt[WW_Pre_n];
  float WW_Eta1L[WW_Pre_n];
  float WW_Eta2L[WW_Pre_n]; 
  float WW_Mll[WW_Pre_n];
  float WW_WR[WW_Pre_n];
  float    WW_NuR_l0[WW_Pre_n];
  float    WW_NuR_l1[WW_Pre_n]; 
  
  WW_Pre_n = 0;
   
     for (int i = 0; i < WW_nEvents; ++i) {
    WW_e.readEvent(i); 
    //cuts on number of leptons and jets    
    if ((WW_e.electrons.size() >= NofLept) && (WW_e.jets.size() >= NofJets)) {
     //cuts on jets Pt      
     if (((WW_e.jets[0].p4*J_k).Pt()>=Jet1Pt_cut) && ((WW_e.jets[1].p4*J_k).Pt()>=Jet2Pt_cut))  { 
      //cuts on Eta of jets 
      if ((abs(WW_e.jets[0].p4.Eta())<=Eta1J_cut) && (abs(WW_e.jets[1].p4.Eta())<=Eta2J_cut))   {    
       // Isolation
       if ((WW_e.electrons[0].isoHEEP() <= ISO_cut)&&(WW_e.electrons[1].isoHEEP() <= ISO_cut)) {       
        //Cut on vertexes
        if ((abs(WW_e.electrons[0].vertex.z()-WW_e.electrons[1].vertex.z())<=Vertex_cut) && (abs(WW_e.jets[0].vertex.z()-WW_e.jets[1].vertex.z())<=Vertex_cut) 
         && (abs(WW_e.electrons[0].vertex.z()-WW_e.jets[1].vertex.z())<=Vertex_cut) && (abs(WW_e.electrons[0].vertex.z()-WW_e.jets[1].vertex.z())<=Vertex_cut)) {         //cuts on electrons invariant mass Mll    
          //cuts on electrons invariant mass Mll    
         if ((WW_e.electrons[0].p4*E_k  + WW_e.electrons[1].p4*E_k  ).M()>=Mll_cut) {
         WW_Lep1Pt[WW_Pre_n] = (WW_e.electrons[0].p4*E_k  ).Pt();
         WW_Lep2Pt[WW_Pre_n] = (WW_e.electrons[1].p4*E_k  ).Pt();
         WW_Eta1L[WW_Pre_n] = abs(WW_e.electrons[0].p4.Eta());
         WW_Eta2L[WW_Pre_n] = abs(WW_e.electrons[1].p4.Eta());         
         WW_Mll[WW_Pre_n] = (WW_e.electrons[0].p4*E_k  + WW_e.electrons[1].p4*E_k  ).M();
         WW_WR[WW_Pre_n] = (WW_e.electrons[0].p4*E_k  + WW_e.electrons[1].p4*E_k  + WW_e.jets[0].p4*J_k + WW_e.jets[1].p4*J_k).M();
         WW_NuR_l0[WW_Pre_n] = (WW_e.electrons[0].p4*E_k  + WW_e.jets[0].p4*J_k + WW_e.jets[1].p4*J_k).M();
         WW_NuR_l1[WW_Pre_n] = (WW_e.electrons[1].p4*E_k  + WW_e.jets[0].p4*J_k + WW_e.jets[1].p4*J_k).M();    
         WW_Pre_n = WW_Pre_n +1;     
         }         
        }    
       }    
      }     
     }
    }
   } 

  

//============================================================================================================================ 
// MC BG TW
 
 

  //string TW_fname = "/moscow31/heavynu/38x/TToBLNu_TuneZ2_tW-channel_7TeV-madgraph.root";
    string TW_fname = "/moscow31/heavynu/38x/TW_dr_7TeV-mcatnlo.root";
  //string TW_fname = "/moscow31/heavynu/38x/TW_dr_7TeV-mcatnlo.root";
 
  TheEvent TW_e(TW_fname.c_str());
  if (!TW_e) return;
  
  float TW_weight = TW_e.weight*Lum;
  int TW_stat = TW_e.sample.stat;
  float TW_statw = TW_e.sample.stat*TW_weight; 
  BG_statw = BG_statw + TW_statw;
  int TW_nEvents = TW_e.totalEvents(); 
  int TW_Pre_n = 0; 
  
  for (int i = 0; i < TW_nEvents; ++i) {
    TW_e.readEvent(i); 
    //cuts on number of leptons and jets    
    if ((TW_e.electrons.size() >= NofLept) && (TW_e.jets.size() >= NofJets)) {
     //cuts on jets Pt      
     if (((TW_e.jets[0].p4*J_k).Pt()>=Jet1Pt_cut) && ((TW_e.jets[1].p4*J_k).Pt()>=Jet2Pt_cut))  { 
      //cuts on Eta of jets 
      if ((abs(TW_e.jets[0].p4.Eta())<=Eta1J_cut) && (abs(TW_e.jets[1].p4.Eta())<=Eta2J_cut))   {    
       // Isolation
       if ((TW_e.electrons[0].isoHEEP() <= ISO_cut)&&(TW_e.electrons[1].isoHEEP() <= ISO_cut)) {       
        //Cut on vertexes
        if ((abs(TW_e.electrons[0].vertex.z()-TW_e.electrons[1].vertex.z())<=Vertex_cut) && (abs(TW_e.jets[0].vertex.z()-TW_e.jets[1].vertex.z())<=Vertex_cut) 
         && (abs(TW_e.electrons[0].vertex.z()-TW_e.jets[1].vertex.z())<=Vertex_cut) && (abs(TW_e.electrons[0].vertex.z()-TW_e.jets[1].vertex.z())<=Vertex_cut)) {         //cuts on electrons invariant mass Mll    
         //cuts on electrons invariant mass Mll    
         if ((TW_e.electrons[0].p4*E_k  + TW_e.electrons[1].p4*E_k  ).M()>=Mll_cut) {
          TW_Pre_n = TW_Pre_n +1;
         }         
        }    
       }    
      }     
     }
    }
   } 
   
  float TW_Lep1Pt[TW_Pre_n];
  float TW_Lep2Pt[TW_Pre_n];
  float TW_Eta1L[TW_Pre_n];
  float TW_Eta2L[TW_Pre_n]; 
  float TW_Mll[TW_Pre_n];
  float TW_WR[TW_Pre_n];
  float    TW_NuR_l0[TW_Pre_n];
  float    TW_NuR_l1[TW_Pre_n]; 
  
  TW_Pre_n = 0;
   
     for (int i = 0; i < TW_nEvents; ++i) {
    TW_e.readEvent(i); 
    //cuts on number of leptons and jets    
    if ((TW_e.electrons.size() >= NofLept) && (TW_e.jets.size() >= NofJets)) {
     //cuts on jets Pt      
     if (((TW_e.jets[0].p4*J_k).Pt()>=Jet1Pt_cut) && ((TW_e.jets[1].p4*J_k).Pt()>=Jet2Pt_cut))  { 
      //cuts on Eta of jets 
      if ((abs(TW_e.jets[0].p4.Eta())<=Eta1J_cut) && (abs(TW_e.jets[1].p4.Eta())<=Eta2J_cut))   {    
       // Isolation
       if ((TW_e.electrons[0].isoHEEP() <= ISO_cut)&&(TW_e.electrons[0].isoHEEP() <= ISO_cut)) {       
        //Cut on vertexes
        if ((abs(TW_e.electrons[0].vertex.z()-TW_e.electrons[1].vertex.z())<=Vertex_cut) && (abs(TW_e.jets[0].vertex.z()-TW_e.jets[1].vertex.z())<=Vertex_cut) 
         && (abs(TW_e.electrons[0].vertex.z()-TW_e.jets[1].vertex.z())<=Vertex_cut) && (abs(TW_e.electrons[0].vertex.z()-TW_e.jets[1].vertex.z())<=Vertex_cut)) {         //cuts on electrons invariant mass Mll    
           //cuts on electrons invariant mass Mll    
         if ((TW_e.electrons[0].p4*E_k  + TW_e.electrons[1].p4*E_k  ).M()>=Mll_cut) {
         TW_Lep1Pt[TW_Pre_n] = (TW_e.electrons[0].p4*E_k  ).Pt();
         TW_Lep2Pt[TW_Pre_n] = (TW_e.electrons[1].p4*E_k  ).Pt();
         TW_Eta1L[TW_Pre_n] = abs(TW_e.electrons[0].p4.Eta());
         TW_Eta2L[TW_Pre_n] = abs(TW_e.electrons[1].p4.Eta());         
         TW_Mll[TW_Pre_n] = (TW_e.electrons[0].p4*E_k  + TW_e.electrons[1].p4*E_k  ).M();
         TW_WR[TW_Pre_n] = (TW_e.electrons[0].p4*E_k  + TW_e.electrons[1].p4*E_k  + TW_e.jets[0].p4*J_k + TW_e.jets[1].p4*J_k).M();
         TW_NuR_l0[TW_Pre_n] = (TW_e.electrons[0].p4*E_k  + TW_e.jets[0].p4*J_k + TW_e.jets[1].p4*J_k).M();
         TW_NuR_l1[TW_Pre_n] = (TW_e.electrons[1].p4*E_k  + TW_e.jets[0].p4*J_k + TW_e.jets[1].p4*J_k).M();    
         TW_Pre_n = TW_Pre_n +1;     
         }         
        }    
       }    
      }     
     }
    }
   } 
  
   
  
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Optimization 
 
  for (int Cut_1LPt = 6; Cut_1LPt < 7; ++Cut_1LPt) {
  Lep1Pt_cut= Cut_1LPt*10;
  
  for (int Cut_2LPt = 2; Cut_2LPt < 3; ++Cut_2LPt) {
  Lep2Pt_cut= Cut_2LPt*10;
 
  if (INFORM == 1) {
  cout << endl;
  cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"<<endl;
  cout << endl;
  cout <<"Lep1Pt = "<<Lep1Pt_cut <<" Lep2Pt = "<< Lep2Pt_cut <<" EtaCut1L = "<< Eta1L_cut <<" EtaCut2L = " << Eta2L_cut <<" MllCut = " << Mll_cut <<" MWCut = " << MW_cut << " MN1Cut = " << MN1_cut << " MN2Cut = " << MN2_cut << endl;
  cout << endl;
  }
  
  for (int Cut_Eta1L = 1; Cut_Eta1L <2; ++Cut_Eta1L) {
  Eta1L_cut= Cut_Eta1L*1.44;
  
  for (int Cut_Eta2L = 0; Cut_Eta2L <1; ++Cut_Eta2L) {
  Eta2L_cut= Cut_Eta2L*0.5;
  
  for (int Cut_LL = 20; Cut_LL < 21; ++Cut_LL) {
  Mll_cut= Cut_LL*10;
  
  for (int Cut_W = MW_C; Cut_W < (MW_C+1); ++Cut_W) {
  MW_cut= Cut_W*10; 
  
  for (int Cut_N1 = 0; Cut_N1 < 1; ++Cut_N1) {
  MN1_cut= Cut_N1*20;
  
  for (int Cut_N2 = 0; Cut_N2 < 1; ++Cut_N2) {
  MN2_cut= Cut_N2*20;
  
  BG_nw = 0.;  

 if (INFORM == 1) {   
  cout << endl;
  cout << "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++"<<endl;
  cout << endl;
  cout <<"Lep1Pt = "<<Lep1Pt_cut <<" Lep2Pt = "<< Lep2Pt_cut <<" EtaCut1L = "<< Eta1L_cut <<" EtaCut2L = " << Eta2L_cut <<" MllCut = " << Mll_cut <<" MWCut = " << MW_cut << " MN1Cut = " << MN1_cut << " MN2Cut = " << MN2_cut << endl;
  cout << endl;
}  
 
  
//============================================================================================================================
// MC SIGNAL 




float S_nw[sa.size()];

 for (int S_i = 0; S_i < sa.size(); ++S_i) {
  S_nw[S_i] = 0;
  for (int i = 0; i < S_Pre_n[S_i]; ++i) {          
   //cuts on electrons Pt
   if ((S_Lep1Pt[S_i][i]>=Lep1Pt_cut) && (S_Lep2Pt[S_i][i]>=Lep2Pt_cut))  { 
    //cuts on Eta  
    //if ((S_Eta1L[S_i][i]<=Eta1L_cut) && (S_Eta2L[S_i][i]<=Eta2L_cut)) {    
      if ((S_Eta1L[S_i][i]<=Eta1L_cut) || (S_Eta2L[S_i][i]<=Eta1L_cut)) {    
     //cuts on electrons invariant mass Mll    
     if (S_Mll[S_i][i]>=Mll_cut) {         
      //cut on  MW              
      if (S_WR[S_i][i]>=MW_cut) {
       //cuts on  Mnu
        if (S_NuR_l0[S_i][i]>=MN1_cut) {
         if (S_NuR_l1[S_i][i]>=MN2_cut) { 
          if (S_Eta1L[S_i][i]<=1.44){A_Wei=HEEP_B;} else {A_Wei=HEEP_E;}
	      if (S_Eta2L[S_i][i]<=1.44){A_Wei=A_Wei*HEEP_B;} else {A_Wei=A_Wei*HEEP_E;}			 
          S_nw[S_i] = S_nw[S_i] + A_Wei;
        }
      }
     }
    }    
   }    
  }     
 }
 
 S_nw[S_i] = S_nw[S_i]*S_weight[S_i];
if (INFORM == 1) { 
 cout << endl;
 cout << "MC SAMPLE, weight = " << S_weight[S_i]<< endl;
 cout << "MW = " << S_e_sample_MW[S_i] << " MNu = "<< S_e_sample_MNu[S_i]<<  endl;
 cout << endl;
 cout << "Stats = " << S_stat[S_i]<< " Stats weight = " << S_statw[S_i]<< endl;
 cout << "Total dumped events = " << S_nEvents[S_i] << " Total weight dumped events = " << S_nEvents[S_i]*S_weight[S_i] << endl;
 cout << endl; 
 cout << "Number of pre-passed events = "<<S_Pre_n[S_i]<<"  weighted =  "<<S_Pre_n[S_i] * S_weight[S_i]<< endl;
 cout << endl; 
 cout << "Number of passed events = "<< S_nw[S_i]<< endl;
 cout << endl;
 }
}
 

 
//============================================================================================================================= 
// MC BG QCD FAKE-RATE 


int QCD_n = 0;
float QCD_nw = 0.0;

if (QCD_incl == true) {

 for (int i = 0; i < QCD_Pre_n; ++i) {          
  //cuts on electrons Pt
  if ((QCD_Lep1Pt[i]>=Lep1Pt_cut) && (QCD_Lep2Pt[i]>=Lep2Pt_cut))  { 
  //cuts on Eta  
  //if ((QCD_Eta1L[i]<=Eta1L_cut) && (QCD_Eta2L[i]<=Eta2L_cut)) {    
  if ((QCD_Eta1L[i]<=Eta1L_cut) || (QCD_Eta2L[i]<=Eta1L_cut)) {      
   //cuts on electrons invariant mass Mll    
   if (QCD_Mll[i]>=Mll_cut) {         
    //cut on  MW              
    if (QCD_WR[i]>=MW_cut) {
     //cuts on  Mnu
      if (QCD_NuR_l0[i]>=MN1_cut) {
       if (QCD_NuR_l1[i]>=MN2_cut) {    
        QCD_n = QCD_n + 1;
        QCD_nw = QCD_nw + QCD_weight[i];
        }
      }
     }
    }    
   }    
  }     
 }
 BG_nw = BG_nw + QCD_nw; // number of total BG events
 // delta_BG_nw_sq = delta_BG_nw_sq + (0.20*0.20 + 1/(QCD_n+1))*QCD_nw*QCD_nw ; // 20% TTb systematic BG error 
 if (INFORM == 1) { 
 cout << endl;
 cout << "MC BG QCD" << endl;
 cout << endl;
 cout << "Stats = " << QCD_stat << " Stats weight = " << QCD_statw << endl;
 cout << "Total dumped events = " << QCD_nEvents << " Total weight dumped events = " << QCD_nwEvents << endl;
 cout << endl; 
 cout << "Number of pre-passed events = "<<QCD_Pre_n<<"  weighted =  "<<QCD_Pre_nw<< endl;
 cout << endl; 
 cout << "Number of passed events = "<<QCD_n<<"  weighted = "<<QCD_nw<< endl;
 cout << endl;
 cout << "BG_statw = "<<BG_statw <<" BG_nw = "<<BG_nw<< endl;
 cout << endl;
 }
}

//============================================================================================================================= 
// MC BG TTbar




float TTb_nw = 0.0;

if (TTb_incl == true) {

 for (int i = 0; i < TTb_Pre_n; ++i) {          
  //cuts on electrons Pt
  if ((TTb_Lep1Pt[i]>=Lep1Pt_cut) && (TTb_Lep2Pt[i]>=Lep2Pt_cut))  { 
  //cuts on Eta  
  //if ((TTb_Eta1L[i]<=Eta1L_cut) && (TTb_Eta2L[i]<=Eta2L_cut)) {
    if ((TTb_Eta1L[i]<=Eta1L_cut) || (TTb_Eta2L[i]<=Eta1L_cut)) {
   //cuts on electrons invariant mass Mll    
   if (TTb_Mll[i]>=Mll_cut) {         
    //cut on  MW              
    if (TTb_WR[i]>=MW_cut) {
     //cuts on  Mnu
      if (TTb_NuR_l0[i]>=MN1_cut) {
       if (TTb_NuR_l1[i]>=MN2_cut) {  
        if (TTb_Eta1L[i]<=1.44){A_Wei=HEEP_B;} else {A_Wei=HEEP_E;}
	    if (TTb_Eta2L[i]<=1.44){A_Wei=A_Wei*HEEP_B;} else {A_Wei=A_Wei*HEEP_E;}			  	   
        TTb_nw = TTb_nw + A_Wei;
        }
      }
     }
    }    
   }    
  }     
 }
 
 TTb_nw = TTb_nw*TTb_weight;
 BG_nw = BG_nw + TTb_nw; // number of total BG events
 // delta_BG_nw_sq = delta_BG_nw_sq + (0.20*0.20 + 1/(TTb_n+1))*TTb_nw*TTb_nw ; // 20% TTb systematic BG error 
 if (INFORM == 1) { 
 cout << endl;
 cout << "MC BG TTbar, weight = " <<TTb_weight<< endl;
 cout << endl;
 cout << "Stats = " << TTb_stat << " Stats weight = " << TTb_statw << endl;
 cout << "Total dumped events = " << TTb_nEvents << " Total weight dumped events = " << TTb_nEvents*TTb_weight << endl;
 cout << endl; 
 cout << "Number of pre-passed events = "<<TTb_Pre_n<<"  weighted =  "<<TTb_Pre_n * TTb_weight<< endl;
 cout << endl; 
 cout << "Number of passed events = "<<TTb_nw<< endl;
 cout << endl;
 cout << "BG_statw = "<<BG_statw <<" BG_nw = "<<BG_nw<< endl;
 cout << endl;
 }
}
 
 
//=============================================================================================================================
// MC BG Z+JETS


float ZJ_nw = 0;  
  
if (ZJ_incl) { 

float ZJ_nw1=0;
 
 for (int fc = 0; fc < fc_max; ++fc) { 
   ZJ_nw1=0;
   for (int i = 0; i < ZJ_Pre_n[fc]; ++i) {          
    //cuts on electrons Pt
    if ((ZJ_Lep1Pt[fc][i]>=Lep1Pt_cut) && (ZJ_Lep2Pt[fc][i]>=Lep2Pt_cut))  { 
     //cuts on Eta  
     //if ((ZJ_Eta1L[fc][i]<=Eta1L_cut) && (ZJ_Eta2L[fc][i]<=Eta2L_cut)) {
     if ((ZJ_Eta1L[fc][i]<=Eta1L_cut) || (ZJ_Eta2L[fc][i]<=Eta1L_cut)) {     
      //cuts on electrons invariant mass Mll    
      if (ZJ_Mll[fc][i]>=Mll_cut) {         
       //cut on  MW              
       if (ZJ_WR[fc][i]>=MW_cut) {
        //cuts on  Mnu
        if (ZJ_NuR_l0[fc][i]>=MN1_cut) {
        if (ZJ_NuR_l1[fc][i]>=MN2_cut) { 
         if (ZJ_Eta1L[fc][i]<=1.44){A_Wei=HEEP_B;} else {A_Wei=HEEP_E;}
	     if (ZJ_Eta2L[fc][i]<=1.44){A_Wei=A_Wei*HEEP_B;} else {A_Wei=A_Wei*HEEP_E;}			  	   		
         ZJ_nw1 = ZJ_nw1 + A_Wei;
         }
        }
     }
    }    
   }    
  }     
 }
 ZJ_nw = ZJ_nw + ZJ_nw1*ZJ_weight[fc];
 if (INFORM == 1) { 
 cout << endl;
 cout << fc <<" MC BG Z+Jets, weight = " <<ZJ_weight[fc]<< endl;
 cout << endl;
 cout << "Stats = " << ZJ_stat[fc] << " Stats weight = " << ZJ_statw[fc] << endl;
 cout << "Total dumped events = " << ZJ_nEvents[fc] << " Total weight dumped events = " << ZJ_nEvents[fc]*ZJ_weight[fc] << endl;
 cout << endl; 
 cout << "Number of pre-passed events = "<<ZJ_Pre_n[fc]<<"  weighted =  "<<ZJ_Pre_n[fc] * ZJ_weight[fc]<< endl;
 cout << endl; 
 cout << "Number of passed events = "<<ZJ_nw1*ZJ_weight[fc]<< endl;
 cout << endl;
 }
} 
 BG_nw = BG_nw + ZJ_nw; // number of total BG events
 if (INFORM == 1) { 
 cout << endl;
 cout << "MC BG Z+Jets, all files "<< endl;
 cout << endl;
 cout << "Number of passed events = "<<ZJ_nw<< endl;
 cout << endl;
 cout << "BG_statw = "<<BG_statw <<" BG_nw = "<<BG_nw<< endl;
 cout << endl;
 }
}
 
  
 
//============================================================================================================================
// MC BG WJ


float WJ_nw = 0.0;
  
 if (WJ_incl == true) {

 for (int i = 0; i < WJ_Pre_n; ++i) {          
  //cuts on electrons Pt
  if ((WJ_Lep1Pt[i]>=Lep1Pt_cut) && (WJ_Lep2Pt[i]>=Lep2Pt_cut))  { 
  //cuts on Eta  
  //if ((WJ_Eta1L[i]<=Eta1L_cut) && (WJ_Eta2L[i]<=Eta2L_cut)) {
    if ((WJ_Eta1L[i]<=Eta1L_cut) || (WJ_Eta2L[i]<=Eta1L_cut)) {
   //cuts on electrons invariant mass Mll    
   if (WJ_Mll[i]>=Mll_cut) {         
    //cut on  MW              
    if (WJ_WR[i]>=MW_cut) {
     //cuts on  Mnu
      if (WJ_NuR_l0[i]>=MN1_cut) {
       if (WJ_NuR_l1[i]>=MN2_cut) { 
        if (WJ_Eta1L[i]<=1.44){A_Wei=HEEP_B;} else {A_Wei=HEEP_E;}
	    if (WJ_Eta2L[i]<=1.44){A_Wei=A_Wei*HEEP_B;} else {A_Wei=A_Wei*HEEP_E;}		   
        WJ_nw = WJ_nw + A_Wei;
        }
      }
     }
    }    
   }    
  }     
 }
 
 WJ_nw = WJ_nw*WJ_weight;
 BG_nw = BG_nw + WJ_nw; // number of total BG events
 // delta_BG_nw_sq = delta_BG_nw_sq + (0.20*0.20 + 1/(WJ_n+1))*WJ_nw*WJ_nw ; // 20% TTb systematic BG error 
 if (INFORM == 1) { 
 cout << endl;
 cout << "MC BG WJ, weight = " <<WJ_weight<< endl;
 cout << endl;
 cout << "Stats = " << WJ_stat << " Stats weight = " << WJ_statw << endl;
 cout << "Total dumped events = " << WJ_nEvents << " Total weight dumped events = " << WJ_nEvents*WJ_weight << endl;
 cout << endl; 
 cout << "Number of pre-passed events = "<<WJ_Pre_n<<"  weighted =  "<<WJ_Pre_n * WJ_weight<< endl;
 cout << endl; 
 cout << "Number of passed events = "<<WJ_nw<< endl;
 cout << endl;
 cout << "BG_statw = "<<BG_statw <<" BG_nw = "<<BG_nw<< endl;
 cout << endl;
 }
}

/*
//=============================================================================================================================
// MC BG W+JETS


float WJ_nw = 0;  
  
if (WJ_incl) { 

float WJ_nw1=0;
 
 for (int fc = 0; fc < fc_max; ++fc) { 
   WJ_nw1=0;
   for (int i = 0; i < WJ_Pre_n[fc]; ++i) {          
    //cuts on electrons Pt
    if ((WJ_Lep1Pt[fc][i]>=Lep1Pt_cut) && (WJ_Lep2Pt[fc][i]>=Lep2Pt_cut))  { 
     //cuts on Eta  
     //if ((WJ_Eta1L[fc][i]<=Eta1L_cut) && (WJ_Eta2L[fc][i]<=Eta2L_cut)) {
     if ((WJ_Eta1L[fc][i]<=Eta1L_cut) || (WJ_Eta2L[fc][i]<=Eta1L_cut)) {     
      //cuts on electrons invariant mass Mll    
      if (WJ_Mll[fc][i]>=Mll_cut) {         
       //cut on  MW              
       if (WJ_WR[fc][i]>=MW_cut) {
        //cuts on  Mnu
        if (WJ_NuR_l0[fc][i]>=MN1_cut) {
        if (WJ_NuR_l1[fc][i]>=MN2_cut) { 
         if (WJ_Eta1L[fc][i]<=1.44){A_Wei=HEEP_B;} else {A_Wei=HEEP_E;}
	     if (WJ_Eta2L[fc][i]<=1.44){A_Wei=A_Wei*HEEP_B;} else {A_Wei=A_Wei*HEEP_E;}			  	   		
         WJ_nw1 = WJ_nw1 + A_Wei;
         }
        }
     }
    }    
   }    
  }     
 }
 WJ_nw = WJ_nw + WJ_nw1*WJ_weight[fc];
 if (INFORM == 1) { 
 cout << endl;
 cout << fc <<" MC BG Z+Jets, weight = " <<WJ_weight[fc]<< endl;
 cout << endl;
 cout << "Stats = " << WJ_stat[fc] << " Stats weight = " << WJ_statw[fc] << endl;
 cout << "Total dumped events = " << WJ_nEvents[fc] << " Total weight dumped events = " << WJ_nEvents[fc]*WJ_weight[fc] << endl;
 cout << endl; 
 cout << "Number of pre-passed events = "<<WJ_Pre_n[fc]<<"  weighted =  "<<WJ_Pre_n[fc] * WJ_weight[fc]<< endl;
 cout << endl; 
 cout << "Number of passed events = "<<WJ_nw1*WJ_weight[fc]<< endl;
 cout << endl;
 }
} 
 BG_nw = BG_nw + WJ_nw; // number of total BG events
 if (INFORM == 1) { 
 cout << endl;
 cout << "MC BG Z+Jets, all files "<< endl;
 cout << endl;
 cout << "Number of passed events = "<<WJ_nw<< endl;
 cout << endl;
 cout << "BG_statw = "<<BG_statw <<" BG_nw = "<<BG_nw<< endl;
 cout << endl;
 }
}
*/
 
//============================================================================================================================ 
// MC BG ZZ


float ZZ_nw = 0.0; 

if (ZZ_incl == true) {

 for (int i = 0; i < ZZ_Pre_n; ++i) {          
  //cuts on electrons Pt
  if ((ZZ_Lep1Pt[i]>=Lep1Pt_cut) && (ZZ_Lep2Pt[i]>=Lep2Pt_cut))  { 
  //cuts on Eta  
  //if ((ZZ_Eta1L[i]<=Eta1L_cut) && (ZZ_Eta2L[i]<=Eta2L_cut)) {
    if ((ZZ_Eta1L[i]<=Eta1L_cut) || (ZZ_Eta2L[i]<=Eta1L_cut)) {  
   //cuts on electrons invariant mass Mll    
   if (ZZ_Mll[i]>=Mll_cut) {         
    //cut on  MW              
    if (ZZ_WR[i]>=MW_cut) {
     //cuts on  Mnu
      if (ZZ_NuR_l0[i]>=MN1_cut) {
       if (ZZ_NuR_l1[i]>=MN2_cut) {  
        if (ZZ_Eta1L[i]<=1.44){A_Wei=HEEP_B;} else {A_Wei=HEEP_E;}
	    if (ZZ_Eta2L[i]<=1.44){A_Wei=A_Wei*HEEP_B;} else {A_Wei=A_Wei*HEEP_E;}		   
        ZZ_nw = ZZ_nw + A_Wei;
        }
      }
     }
    }    
   }    
  }     
 }
 
 ZZ_nw = ZZ_nw*ZZ_weight;
 BG_nw = BG_nw + ZZ_nw; // number of total BG events
 // delta_BG_nw_sq = delta_BG_nw_sq + (0.20*0.20 + 1/(ZZ_n+1))*ZZ_nw*ZZ_nw ; // 20% ZZ systematic BG error 
 if (INFORM == 1) {  
 cout << endl;
 cout << "MC BG ZZ, weight = " <<ZZ_weight<< endl;
 cout << endl;
 cout << "Stats = " << ZZ_stat << " Stats weight = " << ZZ_statw << endl;
 cout << "Total dumped events = " << ZZ_nEvents << " Total weight dumped events = " << ZZ_nEvents*ZZ_weight << endl;
 cout << endl; 
 cout << "Number of pre-passed events = "<<ZZ_Pre_n<<"  weighted =  "<<ZZ_Pre_n * ZZ_weight<< endl;
 cout << endl; 
 cout << "Number of passed events = "<<ZZ_nw<< endl;
 cout << endl;
 cout << "BG_statw = "<<BG_statw <<" BG_nw = "<<BG_nw<< endl;
 cout << endl;
 }
}
 
 
 
 

//============================================================================================================================
// MC BG ZW


float ZW_nw = 0.0;

if (ZW_incl == true) {

 for (int i = 0; i < ZW_Pre_n; ++i) {          
  //cuts on electrons Pt
  if ((ZW_Lep1Pt[i]>=Lep1Pt_cut) && (ZW_Lep2Pt[i]>=Lep2Pt_cut))  { 
  //cuts on Eta  
  //if ((ZW_Eta1L[i]<=Eta1L_cut) && (ZW_Eta2L[i]<=Eta2L_cut)) {
  if ((ZW_Eta1L[i]<=Eta1L_cut) || (ZW_Eta2L[i]<=Eta1L_cut)) {  
   //cuts on electrons invariant mass Mll    
   if (ZW_Mll[i]>=Mll_cut) {         
    //cut on  MW              
    if (ZW_WR[i]>=MW_cut) {
     //cuts on  Mnu
      if (ZW_NuR_l0[i]>=MN1_cut) {
       if (ZW_NuR_l1[i]>=MN2_cut) { 
        if (ZW_Eta1L[i]<=1.44){A_Wei=HEEP_B;} else {A_Wei=HEEP_E;}
	    if (ZW_Eta2L[i]<=1.44){A_Wei=A_Wei*HEEP_B;} else {A_Wei=A_Wei*HEEP_E;}		   
        ZW_nw = ZW_nw + A_Wei;
        }
      }
     }
    }    
   }    
  }     
 }
 
 ZW_nw = ZW_nw*ZW_weight;
 BG_nw = BG_nw + ZW_nw; // number of total BG events
 // delta_BG_nw_sq = delta_BG_nw_sq + (0.20*0.20 + 1/(ZW_n+1))*ZW_nw*ZW_nw ; // 20% ZW systematic BG error 
 if (INFORM == 1) { 
 cout << endl;
 cout << "MC BG ZW, weight = " <<ZW_weight<< endl;
 cout << endl;
 cout << "Stats = " << ZW_stat << " Stats weight = " << ZW_statw << endl;
 cout << "Total dumped events = " << ZW_nEvents << " Total weight dumped events = " << ZW_nEvents*ZW_weight << endl;
 cout << endl; 
 cout << "Number of pre-passed events = "<<ZW_Pre_n<<"  weighted =  "<<ZW_Pre_n * ZW_weight<< endl;
 cout << endl; 
 cout << "Number of passed events = "<<ZW_nw<< endl;
 cout << endl;
 cout << "BG_statw = "<<BG_statw <<" BG_nw = "<<BG_nw<< endl;
 cout << endl;
 }
}
 
 
 
 
 
 
//============================================================================================================================
// MC BG WW


float WW_nw = 0.0;

if (WW_incl == true) {

 for (int i = 0; i < WW_Pre_n; ++i) {          
  //cuts on electrons Pt
  if ((WW_Lep1Pt[i]>=Lep1Pt_cut) && (WW_Lep2Pt[i]>=Lep2Pt_cut))  { 
  //cuts on Eta  
  //if ((WW_Eta1L[i]<=Eta1L_cut) && (WW_Eta2L[i]<=Eta2L_cut)) {
  if ((WW_Eta1L[i]<=Eta1L_cut) || (WW_Eta2L[i]<=Eta1L_cut)) {  
   //cuts on electrons invariant mass Mll    
   if (WW_Mll[i]>=Mll_cut) {         
    //cut on  MW              
    if (WW_WR[i]>=MW_cut) {
     //cuts on  Mnu
      if (WW_NuR_l0[i]>=MN1_cut) {
       if (WW_NuR_l1[i]>=MN2_cut) { 
        if (WW_Eta1L[i]<=1.44){A_Wei=HEEP_B;} else {A_Wei=HEEP_E;}
	    if (WW_Eta2L[i]<=1.44){A_Wei=A_Wei*HEEP_B;} else {A_Wei=A_Wei*HEEP_E;}		   
        WW_nw = WW_nw + A_Wei;
        }
      }
     }
    }    
   }    
  }     
 }
 
 WW_nw = WW_nw*WW_weight;
 BG_nw = BG_nw + WW_nw; // number of total BG events
 // delta_BG_nw_sq = delta_BG_nw_sq + (0.20*0.20 + 1/(WW_n+1))*WW_nw*WW_nw ; // 20% WW systematic BG error 
 if (INFORM == 1) { 
 cout << endl;
 cout << "MC BG WW, weight = " <<WW_weight<< endl;
 cout << endl;
 cout << "Stats = " << WW_stat << " Stats weight = " << WW_statw << endl;
 cout << "Total dumped events = " << WW_nEvents << " Total weight dumped events = " << WW_nEvents*WW_weight << endl;
 cout << endl; 
 cout << "Number of pre-passed events = "<<WW_Pre_n<<"  weighted =  "<<WW_Pre_n * WW_weight<< endl;
 cout << endl; 
 cout << "Number of passed events = "<<WW_nw<< endl;
 cout << endl;
 cout << "BG_statw = "<<BG_statw <<" BG_nw = "<<BG_nw<< endl;
 cout << endl;
 }
}
 
 
 
//============================================================================================================================
// MC BG TW


float TW_nw = 0.0;

if (TW_incl == true) {

 for (int i = 0; i < TW_Pre_n; ++i) {          
  //cuts on electrons Pt
  if ((TW_Lep1Pt[i]>=Lep1Pt_cut) && (TW_Lep2Pt[i]>=Lep2Pt_cut))  { 
  //cuts on Eta  
  //if ((TW_Eta1L[i]<=Eta1L_cut) && (TW_Eta2L[i]<=Eta2L_cut)) {
  if ((TW_Eta1L[i]<=Eta1L_cut) || (TW_Eta2L[i]<=Eta1L_cut)) {  
   //cuts on electrons invariant mass Mll    
   if (TW_Mll[i]>=Mll_cut) {         
    //cut on  MW              
    if (TW_WR[i]>=MW_cut) {
     //cuts on  Mnu
      if (TW_NuR_l0[i]>=MN1_cut) {
       if (TW_NuR_l1[i]>=MN2_cut) {
        if (TW_Eta1L[i]<=1.44){A_Wei=HEEP_B;} else {A_Wei=HEEP_E;}
	    if (TW_Eta2L[i]<=1.44){A_Wei=A_Wei*HEEP_B;} else {A_Wei=A_Wei*HEEP_E;}		   
        TW_nw = TW_nw + A_Wei;
        }
      }
     }
    }    
   }    
  }     
 }
 
 TW_nw = TW_nw*TW_weight;
 BG_nw = BG_nw + TW_nw; // number of total BG events
 // delta_BG_nw_sq = delta_BG_nw_sq + (0.20*0.20 + 1/(TW_n+1))*TW_nw*TW_nw ; // 20% TW systematic BG error 
 if (INFORM == 1) { 
 cout << endl;
 cout << "MC BG TW, weight = " <<TW_weight<< endl;
 cout << endl;
 cout << "Stats = " << TW_stat << " Stats weight = " << TW_statw << endl;
 cout << "Total dumped events = " << TW_nEvents << " Total weight dumped events = " << TW_nEvents*TW_weight << endl;
 cout << endl; 
 cout << "Number of pre-passed events = "<<TW_Pre_n<<"  weighted =  "<<TW_Pre_n * TW_weight<< endl;
 cout << endl; 
 cout << "Number of passed events = "<<TW_nw<< endl;
 cout << endl;
 cout << "BG_statw = "<<BG_statw <<" BG_nw = "<<BG_nw<< endl;
 cout << endl;
 }
}
 

//=========================================================================================================================
// Significance

for (int S_i = 0; S_i < sa.size(); ++S_i) {

F[S_i] = S_nw[S_i]/sqrt(S_nw[S_i] + BG_nw);

if (F[S_i]>F_max[S_i]) {

  F_max[S_i] = F[S_i];
  Mll_Cut_Opt[S_i] = Mll_cut;
  MW_Cut_Opt[S_i] = MW_cut;
  MN1_Cut_Opt[S_i] = MN1_cut;
  MN2_Cut_Opt[S_i] = MN2_cut;
  Lep1Pt_Cut_Opt[S_i] = Lep1Pt_cut;
  Lep2Pt_Cut_Opt[S_i] = Lep2Pt_cut;
  Eta1L_Cut_Opt[S_i] = Eta1L_cut;
  Eta2L_Cut_Opt[S_i] = Eta2L_cut; 
  S_nw_Opt[S_i] = S_nw[S_i];
  S_eff[S_i] = S_nw[S_i]/S_statw[S_i];
  BG_eff[S_i] = BG_nw/BG_statw;
  BG_nw_Opt[S_i] = BG_nw;
  QCD_nw_Opt[S_i] = QCD_nw; 
  TTb_nw_Opt[S_i] = TTb_nw;
  ZJ_nw_Opt[S_i] = ZJ_nw;  
  WJ_nw_Opt[S_i] = WJ_nw;
  ZZ_nw_Opt[S_i] = ZZ_nw;  
  ZW_nw_Opt[S_i] = ZW_nw;
  WW_nw_Opt[S_i] = WW_nw;
  TW_nw_Opt[S_i] = TW_nw;
} 
 
 }
 

 
 } //end MN2 for
 } //end MN1 for
 } //end MW for
 } //end Mll for
 } //end EtaCut2L for
 } //end EtaCut1L for
 } //end Lep2Pt for
 } //end Lep1Pt for
 
  fprintf(Ptr,"Chn MWR MNu  L1Pt   L2Pt     L1Eta  L2Eta    Mll  Mwr_cut MN1_cut MN2_cut | F      S_nw    S_eff     BG_nw     BG_eff    QCD_nw   TTb_nw    ZJ_nw      WJ_nw    ZZ_nw    ZW_nw    WW_nw    TW_nw\n"); 
  for (int S_i = 0; S_i < sa.size(); ++S_i) {
  fprintf(Ptr,"%d %d  %d  %4.1f  %4.1f  %f  %f %4.1f  %4.1f  %4.1f  %4.1f %f  %f  %f  %f  %f  %f  %f  %f  %f  %f  %f  %f %f\n", 1, S_e_sample_MW[S_i], S_e_sample_MNu[S_i], Lep1Pt_Cut_Opt[S_i], Lep2Pt_Cut_Opt[S_i], Eta1L_Cut_Opt[S_i], Eta2L_Cut_Opt[S_i], Mll_Cut_Opt[S_i], MW_Cut_Opt[S_i], MN1_Cut_Opt[S_i], MN2_Cut_Opt[S_i], F_max[S_i], S_nw_Opt[S_i], S_eff[S_i], BG_nw_Opt[S_i], BG_eff[S_i], QCD_nw_Opt[S_i], TTb_nw_Opt[S_i], ZJ_nw_Opt[S_i], WJ_nw_Opt[S_i], ZZ_nw_Opt[S_i], ZW_nw_Opt[S_i], WW_nw_Opt[S_i], TW_nw_Opt[S_i]); 
  } 

  fclose(Ptr); 

  cout << "Optimization finished successfully" << endl;
}
