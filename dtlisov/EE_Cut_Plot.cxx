// to run: root -b -q -l EE_Cut_Plot.cxx+
#include "TH1.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TLegend.h"
#include <TLorentzVector.h>
#include "Exp_lim_cut_opt.cxx"

#include "../PATDump/src/format.h"
#include <iostream>
#include <fstream>
using namespace std;

void EE_Cut_Plot()
{
// set plain style
  gROOT->SetStyle("Plain");
  
  unsigned int NofLept = 2.;
  unsigned int NofJets = 2.;
  float Lep1Pt = 60.;
  float Lep2Pt = 20.;
  float Jet1Pt = 40.;
  float Jet2Pt = 40.;
  float MllCut = 200.;
  float MWCut =  750.;
  float MN1Cut = 0.;
  float MN2Cut = 0.;  
  float EtaCut1L = 1.44;
  float EtaCut2L = EtaCut1L;
  float EtaCut1J = 2.5;
  float EtaCut2J = 2.5; 
  float ISO = 1.;
  float Vertex = 0.03;
  
// Samples switcher: true = include samples in analisis
 // bool RD_incl = true;
  bool S_incl = true;
  bool QCD_incl = true;
  bool TTb_incl = true;
  bool ZJ_incl = true;
  bool WJ_incl = true;
  bool ZZ_incl = true;
  bool ZW_incl = true;
  bool WW_incl = true;
  bool TW_incl = true;
	
  const char* fname;

  //char *str = (char*)alloca(10000);

  float WR; 
  float NuR_l0, NuR_l1; 
  float Lum = 36.145/100.;
  float Z_tune = 1.19; 
  float HEEP_B=0.972;
  float HEEP_E=0.982;
  float A_Wei=1.;  
  float BG_nw = 0.; // number of total BG events
  //float delta_BG_nw_sq = 0. ; // Squared absolut error for number of total BG events
  
  FILE *Ptr;

  Ptr = fopen("OPT_PLOT.txt","w");
     
 // for (int Cut = 0; Cut <= 50; ++Cut) {
 
 // MllCut= Cut*10; 
  
 for (int Cut = 0; Cut <= 24; ++Cut) {
  
 MWCut = Cut*30; 
  
 // for (int Cut = 0; Cut <= 40; ++Cut) {
  
 // EtaCut1L = Cut*0.1;
  
 // EtaCut2L = EtaCut1L;
  
  cout<< "Cut = "<< Cut <<endl;
  
  BG_nw = 0.;
  
  
//============================================================================================================================
// MC SIGNAL 
  
  fname = "/moscow31/heavynu/38x/legacy/7TeV-38x-8e29-V14-WR700_nuRe500.root";
 
  TheEvent S_e(fname);
  if (!S_e) return;
  int S_n = 0;
  float S_nw = 0.;
  float S_Eff = 0.;
 
if (S_incl == true) {
  
  int S_nEvents = 0;

  S_nEvents = S_e.totalEvents();
 
  for (int i = 0; i < S_nEvents; ++i) {
    S_e.readEvent(i);
    
    
      if ((S_e.electrons.size() >= NofLept) && (S_e.jets.size() >= NofJets)) {
	  
	   if (abs((S_e.electrons[0].p4).Eta())<=1.44){A_Wei=HEEP_B;} else {A_Wei=HEEP_E;}
	   if (abs((S_e.electrons[1].p4).Eta())<=1.44){A_Wei=A_Wei*HEEP_B;} else {A_Wei=A_Wei*HEEP_E;}	
	
	//cuts on electrons Pt
	    if (((S_e.electrons[0].p4).Pt()>=Lep1Pt) && ((S_e.electrons[1].p4).Pt()>=Lep2Pt))  { 
    //cuts on jetss Pt      
	      if (((S_e.jets[0].p4).Pt()>=Jet1Pt) && ((S_e.jets[1].p4).Pt()>=Jet2Pt))  { 
		  
	//cuts on electrons invariant mass Mll	
		   if ((S_e.electrons[0].p4 + S_e.electrons[1].p4).M()>=MllCut) {
		  
	//cuts on Eta  
            if (((abs(S_e.electrons[0].p4.Eta())<=EtaCut1L) || (abs(S_e.electrons[1].p4.Eta())<=EtaCut1L)) 
	        && (abs(S_e.jets[0].p4.Eta())<=EtaCut1J) && (abs(S_e.jets[1].p4.Eta())<=EtaCut2J) )        {
			
	// Isolation
	         if ((S_e.electrons[0].isoHEEP() <= ISO)&&(S_e.electrons[0].isoHEEP() <= ISO)) {
		   
	 //Cut on vertexes
              if ((abs(S_e.electrons[0].vertex.z()-S_e.electrons[1].vertex.z())<=Vertex) && (abs(S_e.jets[0].vertex.z()-S_e.jets[1].vertex.z())<=Vertex) 
              && (abs(abs(S_e.electrons[0].vertex.z()-S_e.electrons[1].vertex.z())-abs(S_e.jets[0].vertex.z()-S_e.jets[1].vertex.z()))<=Vertex)) {

       //cut on  MR			  
	           WR = (S_e.electrons[0].p4 + S_e.electrons[1].p4 + S_e.jets[0].p4 + S_e.jets[1].p4).M();			 	  
		        if (WR>=MWCut) {
      
	  //cuts on  Mnu
            NuR_l0 = (S_e.electrons[0].p4 + S_e.jets[0].p4 + S_e.jets[1].p4).M();
            NuR_l1 = (S_e.electrons[1].p4 + S_e.jets[0].p4 + S_e.jets[1].p4).M();				
			
		          if (NuR_l0>=MN1Cut) {
				  if (NuR_l0>=MN2Cut) {
			

							
			S_n = S_n + 1;
					
		}
		}
		}
		}	
		}	
        } 	
        }
	   }
	  }
	 }        
  }
S_nw = S_n*S_e.weight*A_Wei;
S_Eff = S_nw/(S_e.sample.stat*S_e.weight);
} 
 

 
 
 
//============================================================================================================================= 
// MC BG QCD FAKE-RATE 

 


 
  fname = "/moscow31/heavynu/38x/dstmkLQ1.d";

//counters   
  int QCD_n = 0;
  float QCD_nw = 0.;
   
if (QCD_incl == true) {  
  
  ifstream inFile(fname);
 
 
  float QCD_weight_sample = 1.;
  float QCD_e_jets_size = 0; 
  float QCD_e_electrons_size = 0;  
  float QCD_e_weight = 1;
  float pjg[4];
  int QCD_itype = 0;
  int QCD_jemark = 0;
  int QCD_e_electrons_0_charge;
  int QCD_e_electrons_1_charge;
  long int QCD_e_ievent = 0;
  long int QCD_nEvents = 0;
  float QCD_nwEvents = 0.;

  char str[200] = {" "};
  
  TLorentzVector QCD_e_electrons_0_p4, QCD_e_electrons_1_p4, QCD_e_jets_0_p4, QCD_e_jets_1_p4;
 
  while (!inFile.eof()) {
  
    QCD_nEvents = QCD_nEvents + 1;
	
    
	inFile >> QCD_e_ievent;
	inFile.getline(str, 200);
	
	inFile >> QCD_e_jets_size;
    inFile >> QCD_e_weight;
    inFile >> QCD_itype;   
    inFile.getline(str, 200);
	
	QCD_e_weight = QCD_e_weight*QCD_weight_sample;
	QCD_nwEvents = QCD_nwEvents + QCD_e_weight;
	
	// read Jets informations   
	inFile >> QCD_jemark;
	for (int j = 0; j < 4; j++) inFile >> pjg[j];
    QCD_e_jets_0_p4.SetPxPyPzE(pjg[0], pjg[1], pjg[2], pjg[3]);	
	inFile.getline(str, 200);
	
	inFile >> QCD_jemark;
	for (int j = 0; j < 4; j++) inFile >> pjg[j];
	QCD_e_jets_1_p4.SetPxPyPzE(pjg[0], pjg[1], pjg[2], pjg[3]);	
	inFile.getline(str, 200);
	
	// Read Leptons informations
	inFile >> QCD_e_electrons_size;
    inFile.getline(str, 200);
	
	inFile >> QCD_e_electrons_0_charge;
	for (int j = 0; j < 4; j++) inFile >> pjg[j];
	QCD_e_electrons_0_p4.SetPxPyPzE(pjg[0], pjg[1], pjg[2], pjg[3]);	
	inFile.getline(str, 200);
	
	inFile >> QCD_e_electrons_1_charge;
	for (int j = 0; j < 4; j++) inFile >> pjg[j];
	QCD_e_electrons_1_p4.SetPxPyPzE(pjg[0], pjg[1], pjg[2], pjg[3]);	
	inFile.getline(str, 200);
	
    inFile.getline(str, 200);
    

	
	
      
    
      if ((QCD_e_electrons_size >= NofLept) && (QCD_e_jets_size >= NofJets)) {
	
	//cuts on electrons Pt
	    if (((QCD_e_electrons_0_p4).Pt()>=Lep1Pt) && ((QCD_e_electrons_1_p4).Pt()>=Lep2Pt))  { 
    //cuts on jetss Pt      
	      if (((QCD_e_jets_0_p4).Pt()>=Jet1Pt) && ((QCD_e_jets_1_p4).Pt()>=Jet2Pt))  { 
		  
	//cuts on electrons invariant mass Mll>=400.0	
		  if ((QCD_e_electrons_0_p4 + QCD_e_electrons_1_p4).M()>=MllCut) {
		  
	//cuts on Eta  
         if (((abs(QCD_e_electrons_0_p4.Eta())<=EtaCut1L) || (abs(QCD_e_electrons_1_p4.Eta())<=EtaCut1L))
	     && (abs(QCD_e_jets_0_p4.Eta())<=EtaCut1J) && (abs(QCD_e_jets_1_p4.Eta())<=EtaCut2J) )        {
			
	// Isolation
	//       if ((QCD_e_electrons_0_isoHEEP() <= ISO)&&(QCD_e_electrons_0_isoHEEP() <= ISO)) {
		   
	//Cut on vertexes
    //       if ((abs(QCD_e_electrons_0_vertex.z()-QCD_e_electrons_1_vertex.z())<=Vertex) && (abs(QCD_e_jets_0_vertex.z()-QCD_e_jets_1_vertex.z())<=Vertex) 
    //       && (abs(abs(QCD_e_electrons_0_vertex.z()-QCD_e_electrons_1_vertex.z())-abs(QCD_e_jets_0_vertex.z()-QCD_e_jets_1_vertex.z()))<=Vertex)) {
	
	//cut on  MW			  
	           WR = (QCD_e_electrons_0_p4 + QCD_e_electrons_1_p4 + QCD_e_jets_0_p4 + QCD_e_jets_1_p4).M();		 	  
		        if (WR>=MWCut) {
      
	  //cuts on  Mnu
            NuR_l0 = (QCD_e_electrons_0_p4 + QCD_e_jets_0_p4 + QCD_e_jets_1_p4).M();
            NuR_l1 = (QCD_e_electrons_1_p4 + QCD_e_jets_0_p4 + QCD_e_jets_1_p4).M();				
			
		          if (NuR_l0>=MN1Cut) {
				  if (NuR_l0>=MN2Cut) {
      			
			QCD_n = QCD_n + 1;
			QCD_nw = QCD_nw + QCD_e_weight; 
		}
        }		
		}	
//		}	
//      } 	
        }
	   }
	  }
	 }    
    }
  } 
 BG_nw = BG_nw + QCD_nw; // number of total BG events
}





//============================================================================================================================= 
// MC BG TTbar



   
  fname = "/moscow31/heavynu/38x/TTJets_TuneZ2_7TeV-madgraph-tauola.root";
 
  TheEvent TTb_e(fname);
  if (!TTb_e) return;
  int TTb_n = 0; 
  float TTb_nw = 0.;  
  
if (TTb_incl == true) {

  int TTb_nEvents = 0;    

  
  TTb_nEvents = TTb_e.totalEvents();
 
  for (int i = 0; i < TTb_nEvents; ++i) {
    TTb_e.readEvent(i);   
 
    
    // mass distributions
      if ((TTb_e.electrons.size() >= NofLept) && (TTb_e.jets.size() >= NofJets)) {
	   if (abs((TTb_e.electrons[0].p4).Eta()<=1.44)){A_Wei=HEEP_B;} else {A_Wei=HEEP_E;}
	   if (abs((TTb_e.electrons[1].p4).Eta()<=1.44)){A_Wei=A_Wei*HEEP_B;} else {A_Wei=A_Wei*HEEP_E;}
	
	//cuts on electrons Pt>=40
	    if (((TTb_e.electrons[0].p4).Pt()>=Lep1Pt) && ((TTb_e.electrons[1].p4).Pt()>=Lep2Pt))  { 
    //cuts on jetss Pt>=40      
	      if (((TTb_e.jets[0].p4).Pt()>=Jet1Pt) && ((TTb_e.jets[1].p4).Pt()>=Jet2Pt))  { 
		  
	//cuts on electrons invariant mass Mll>=400.0	
		  if ((TTb_e.electrons[0].p4 + TTb_e.electrons[1].p4).M()>=MllCut) {
		  
	//cuts on Eta  
         if (((abs(TTb_e.electrons[0].p4.Eta())<=EtaCut1L) || (abs(TTb_e.electrons[1].p4.Eta())<=EtaCut1L)) 
	     && (abs(TTb_e.jets[0].p4.Eta())<=EtaCut1J) && (abs(TTb_e.jets[1].p4.Eta())<=EtaCut2J) )        {
			
	// Isolation
	       if ((TTb_e.electrons[0].isoHEEP() <= ISO)&&(TTb_e.electrons[0].isoHEEP() <= ISO)) {
		   
	 //Cut on vertexes
            if ((abs(TTb_e.electrons[0].vertex.z()-TTb_e.electrons[1].vertex.z())<=Vertex) && (abs(TTb_e.jets[0].vertex.z()-TTb_e.jets[1].vertex.z())<=Vertex) 
             && (abs(abs(TTb_e.electrons[0].vertex.z()-TTb_e.electrons[1].vertex.z())-abs(TTb_e.jets[0].vertex.z()-TTb_e.jets[1].vertex.z()))<=Vertex)) {
	
     //cut on  MW			  
	        WR = (TTb_e.electrons[0].p4 + TTb_e.electrons[1].p4 + TTb_e.jets[0].p4 + TTb_e.jets[1].p4).M();	 	  
		        if (WR>=MWCut) {
      
	  //cuts on  Mnu
            NuR_l0 = (TTb_e.electrons[0].p4 + TTb_e.jets[0].p4 + TTb_e.jets[1].p4).M();
            NuR_l1 = (TTb_e.electrons[1].p4 + TTb_e.jets[0].p4 + TTb_e.jets[1].p4).M();				
			
		          if (NuR_l0>=MN1Cut) {
				  if (NuR_l0>=MN2Cut) {
      										
			TTb_n = TTb_n + 1;
 		}
		}
		}
		}	
		}	
        } 	
        }
	   }
	  }
	 }  
  }
  
 TTb_nw = TTb_n*TTb_e.weight*A_Wei; 
 BG_nw = BG_nw + TTb_nw; // number of total BG events 
}
 
 
 
 
//=============================================================================================================================
// MC BG Z+JETS

  

  
  int ZJ_n = 0;
  int ZJ_n1 = 0;
  float ZJ_nw = 0;

if (ZJ_incl) { 

  int ZJ_nEvents = 0;
  float ZJ_nwEvents = 0;
  float ZJ_n1Events = 0;    

 
    for (int fc = 0; fc < 21; ++fc) { 
  
   switch(fc)
    {
   case 0:
    fname = "/moscow31/heavynu/38x/Z0Jets_TuneZ2_7TeV-alpgen-tauola.root";
	break;
   case 1:
    fname = "/moscow31/heavynu/38x/Z1Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 2:
    fname = "/moscow31/heavynu/38x/Z1Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 3:
    fname = "/moscow31/heavynu/38x/Z1Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;	
   case 4:
    fname = "/moscow31/heavynu/38x/Z1Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 5:
    fname = "/moscow31/heavynu/38x/Z2Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola.root";
	break;
   case 6:
    fname = "/moscow31/heavynu/38x/Z2Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 7:
    fname = "/moscow31/heavynu/38x/Z2Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 8:
    fname = "/moscow31/heavynu/38x/Z2Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;	
   case 9:
    fname = "/moscow31/heavynu/38x/Z3Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 10:
    fname = "/moscow31/heavynu/38x/Z3Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 11:
    fname = "/moscow31/heavynu/38x/Z3Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 12:
    fname = "/moscow31/heavynu/38x/Z3Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;	
   case 13:
    fname = "/moscow31/heavynu/38x/Z4Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 14:
    fname = "/moscow31/heavynu/38x/Z4Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 15:
    fname = "/moscow31/heavynu/38x/Z4Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 16:
    fname = "/moscow31/heavynu/38x/Z4Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 17:
	fname = "/moscow31/heavynu/38x/Z5Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 18:
    fname = "/moscow31/heavynu/38x/Z5Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 19:
    fname = "/moscow31/heavynu/38x/Z5Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 20:
    fname = "/moscow31/heavynu/38x/Z5Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
    }
 
   TheEvent ZJ_e(fname);
  if (!ZJ_e) return;
  
  ZJ_nEvents = ZJ_e.totalEvents();
  
  for (int i = 0; i < ZJ_nEvents; ++i) {
    ZJ_e.readEvent(i);
    
    
    // mass distributions
      if ((ZJ_e.electrons.size() >= NofLept) && (ZJ_e.jets.size() >= NofJets)) {
	  
	   if (abs((ZJ_e.electrons[0].p4).Eta())<=1.44){A_Wei=HEEP_B;} else {A_Wei=HEEP_E;}
	   if (abs((ZJ_e.electrons[1].p4).Eta())<=1.44){A_Wei=A_Wei*HEEP_B;} else {A_Wei=A_Wei*HEEP_E;}
	
	//cuts on electrons Pt>=40
	    if (((ZJ_e.electrons[0].p4).Pt()>=Lep1Pt) && ((ZJ_e.electrons[1].p4).Pt()>=Lep2Pt))  { 
    //cuts on jetss Pt>=40      
	      if (((ZJ_e.jets[0].p4).Pt()>=Jet1Pt) && ((ZJ_e.jets[1].p4).Pt()>=Jet2Pt))  { 
		  
	//cuts on electrons invariant mass Mll>=400.0	
		  if ((ZJ_e.electrons[0].p4 + ZJ_e.electrons[1].p4).M()>=MllCut) {
		  
	//cuts on Eta  
         if (((abs(ZJ_e.electrons[0].p4.Eta())<=EtaCut1L) || (abs(ZJ_e.electrons[1].p4.Eta())<=EtaCut2L)) 
	     && (abs(ZJ_e.jets[0].p4.Eta())<=EtaCut1J) && (abs(ZJ_e.jets[1].p4.Eta())<=EtaCut2J) )        {
			
	// Isolation
	       if ((ZJ_e.electrons[0].isoHEEP() <= ISO)&&(ZJ_e.electrons[0].isoHEEP() <= ISO)) {
		   
	 //Cut on vertexes
            if ((abs(ZJ_e.electrons[0].vertex.z()-ZJ_e.electrons[1].vertex.z())<=Vertex) && (abs(ZJ_e.jets[0].vertex.z()-ZJ_e.jets[1].vertex.z())<=Vertex) 
             && (abs(abs(ZJ_e.electrons[0].vertex.z()-ZJ_e.electrons[1].vertex.z())-abs(ZJ_e.jets[0].vertex.z()-ZJ_e.jets[1].vertex.z()))<=Vertex)) {
	
	 //cut on  MW			  
	        WR = (ZJ_e.electrons[0].p4 + ZJ_e.electrons[1].p4 + ZJ_e.jets[0].p4 + ZJ_e.jets[1].p4).M(); 	  
		        if (WR>=MWCut) {
      
	 //cuts on  Mnu
            NuR_l0 = (ZJ_e.electrons[0].p4 + ZJ_e.jets[0].p4 + ZJ_e.jets[1].p4).M();
            NuR_l1 = (ZJ_e.electrons[1].p4 + ZJ_e.jets[0].p4 + ZJ_e.jets[1].p4).M();			
			
		          if (NuR_l0>=MN1Cut) {
				  if (NuR_l0>=MN2Cut) {
								
			ZJ_n = ZJ_n + 1;
									
        }
        }	
		}
	    }	
	    }	
        } 	
        }
	   }
	  }
	 }

  }
  
  ZJ_n1Events = ZJ_n1Events + ZJ_nEvents; 
  ZJ_nwEvents = ZJ_nwEvents + ZJ_nEvents*ZJ_e.weight*A_Wei*Z_tune;
  ZJ_n1 = ZJ_n1 + ZJ_n;
  ZJ_nw = ZJ_nw + ZJ_n*ZJ_e.weight*A_Wei*Z_tune;  
  ZJ_nEvents = 0;
  ZJ_n = 0;
  
}
 BG_nw = BG_nw + ZJ_nw; // number of total BG events
}
 
 
 

//============================================================================================================================
// MC BG WJ

  
 
  fname = "/moscow31/heavynu/38x/WJets_7TeV-madgraph-tauola.root";
  
  TheEvent WJ_e(fname);
  if (!WJ_e) return;
  int WJ_n = 0;
  float WJ_nw = 0.;
    
if (WJ_incl == true) {

  int WJ_nEvents = 0;   
  
  WJ_nEvents = WJ_e.totalEvents();
  
  for (int i = 0; i < WJ_nEvents; ++i) {
    WJ_e.readEvent(i);
     
     // mass distributions
      if ((WJ_e.electrons.size() >= NofLept) && (WJ_e.jets.size() >= NofJets)) {
	  
	   if (abs((WJ_e.electrons[0].p4).Eta())<=1.44){A_Wei=HEEP_B;} else {A_Wei=HEEP_E;}
	   if (abs((WJ_e.electrons[1].p4).Eta())<=1.44){A_Wei=A_Wei*HEEP_B;} else {A_Wei=A_Wei*HEEP_E;}
	
	//cuts on electrons Pt>=40
	    if (((WJ_e.electrons[0].p4).Pt()>=Lep1Pt) && ((WJ_e.electrons[1].p4).Pt()>=Lep2Pt))  { 
    //cuts on jetss Pt>=40      
	      if (((WJ_e.jets[0].p4).Pt()>=Jet1Pt) && ((WJ_e.jets[1].p4).Pt()>=Jet2Pt))  { 
		  
	//cuts on electrons invariant mass Mll>=400.0	
		  if ((WJ_e.electrons[0].p4 + WJ_e.electrons[1].p4).M()>=MllCut) {
		  
	//cuts on Eta  
         if (((abs(WJ_e.electrons[0].p4.Eta())<=EtaCut1L) || (abs(WJ_e.electrons[1].p4.Eta())<=EtaCut2L)) 
	     && (abs(WJ_e.jets[0].p4.Eta())<=EtaCut1J) && (abs(WJ_e.jets[1].p4.Eta())<=EtaCut2J) )        {
			
	// Isolation
	       if ((WJ_e.electrons[0].isoHEEP() <= ISO)&&(WJ_e.electrons[0].isoHEEP() <= ISO)) {
		   
	 //Cut on vertexes
            if ((abs(WJ_e.electrons[0].vertex.z()-WJ_e.electrons[1].vertex.z())<=Vertex) && (abs(WJ_e.jets[0].vertex.z()-WJ_e.jets[1].vertex.z())<=Vertex) 
             && (abs(abs(WJ_e.electrons[0].vertex.z()-WJ_e.electrons[1].vertex.z())-abs(WJ_e.jets[0].vertex.z()-WJ_e.jets[1].vertex.z()))<=Vertex)) {
	
	 //cut on  MW			  
	        WR = (WJ_e.electrons[0].p4 + WJ_e.electrons[1].p4 + WJ_e.jets[0].p4 + WJ_e.jets[1].p4).M();	  
		        if (WR>=MWCut) {
      
	 //cuts on  Mnu
            NuR_l0 = (WJ_e.electrons[0].p4 + WJ_e.jets[0].p4 + WJ_e.jets[1].p4).M();
            NuR_l1 = (WJ_e.electrons[1].p4 + WJ_e.jets[0].p4 + WJ_e.jets[1].p4).M();			
			
		          if (NuR_l0>=MN1Cut) {
				  if (NuR_l0>=MN2Cut) {	
							
			WJ_n = WJ_n + 1;
		
		}
        }
        }		
		}	
		}	
        } 	
        }
	   }
	  }
	}    
  }
  
 WJ_nw = WJ_n*WJ_e.weight*A_Wei;
 BG_nw = BG_nw + WJ_nw; // number of total BG events
}


 
/*
//=============================================================================================================================
// MC BG W+JETS

  

  
  int WJ_n = 0;
  int WJ_n1 = 0;
  float WJ_nw = 0;

if (WJ_incl) { 

  int WJ_nEvents = 0;
  float WJ_nwEvents = 0;
  float WJ_n1Events = 0;    

 
    for (int fc = 0; fc < 21; ++fc) { 
  
   switch(fc)
    {
   case 0:
    fname = "/moscow31/heavynu/38x/W0Jets_TuneZ2_7TeV-alpgen-tauola.root";
	break;
   case 1:
    fname = "/moscow31/heavynu/38x/W1Jets_ptW-0to100_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 2:
    fname = "/moscow31/heavynu/38x/W1Jets_ptW-100to300_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 3:
    fname = "/moscow31/heavynu/38x/W1Jets_ptW-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;	
   case 4:
    fname = "/moscow31/heavynu/38x/W1Jets_ptW-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 5:
    fname = "/moscow31/heavynu/38x/W2Jets_ptW-0to100_TuneZ2_7TeV-alpgen-tauola.root";
	break;
   case 6:
    fname = "/moscow31/heavynu/38x/W2Jets_ptW-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 7:
    fname = "/moscow31/heavynu/38x/W2Jets_ptW-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 8:
    fname = "/moscow31/heavynu/38x/W2Jets_ptW-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;	
   case 9:
    fname = "/moscow31/heavynu/38x/W3Jets_ptW-0to100_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 10:
    fname = "/moscow31/heavynu/38x/W3Jets_ptW-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 11:
    fname = "/moscow31/heavynu/38x/W3Jets_ptW-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 12:
    fname = "/moscow31/heavynu/38x/W3Jets_ptW-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;	
   case 13:
    fname = "/moscow31/heavynu/38x/W4Jets_ptW-0to100_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 14:
    fname = "/moscow31/heavynu/38x/W4Jets_ptW-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 15:
    fname = "/moscow31/heavynu/38x/W4Jets_ptW-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 16:
    fname = "/moscow31/heavynu/38x/W4Jets_ptW-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 17:
	fname = "/moscow31/heavynu/38x/W5Jets_ptW-0to100_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 18:
    fname = "/moscow31/heavynu/38x/W5Jets_ptW-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 19:
    fname = "/moscow31/heavynu/38x/W5Jets_ptW-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 20:
    fname = "/moscow31/heavynu/38x/W5Jets_ptW-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
    }
 
   TheEvent WJ_e(fname);
  if (!WJ_e) return;
  
  WJ_nEvents = WJ_e.totalEvents();
  
  for (int i = 0; i < WJ_nEvents; ++i) {
    WJ_e.readEvent(i);
    
    
    // mass distributions
      if ((WJ_e.electrons.size() >= NofLept) && (WJ_e.jets.size() >= NofJets)) {
	  
	   if (abs((WJ_e.electrons[0].p4).Eta())<=1.44){A_Wei=HEEP_B;} else {A_Wei=HEEP_E;}
	   if (abs((WJ_e.electrons[1].p4).Eta())<=1.44){A_Wei=A_Wei*HEEP_B;} else {A_Wei=A_Wei*HEEP_E;}
	
	//cuts on electrons Pt>=40
	    if (((WJ_e.electrons[0].p4).Pt()>=Lep1Pt) && ((WJ_e.electrons[1].p4).Pt()>=Lep2Pt))  { 
    //cuts on jetss Pt>=40      
	      if (((WJ_e.jets[0].p4).Pt()>=Jet1Pt) && ((WJ_e.jets[1].p4).Pt()>=Jet2Pt))  { 
		  
	//cuts on electrons invariant mass Mll>=400.0	
		  if ((WJ_e.electrons[0].p4 + WJ_e.electrons[1].p4).M()>=MllCut) {
		  
	//cuts on Eta  
         if (((abs(WJ_e.electrons[0].p4.Eta())<=EtaCut1L) || (abs(WJ_e.electrons[1].p4.Eta())<=EtaCut2L)) 
	     && (abs(WJ_e.jets[0].p4.Eta())<=EtaCut1J) && (abs(WJ_e.jets[1].p4.Eta())<=EtaCut2J) )        {
			
	// Isolation
	       if ((WJ_e.electrons[0].isoHEEP() <= ISO)&&(WJ_e.electrons[0].isoHEEP() <= ISO)) {
		   
	 //Cut on vertexes
            if ((abs(WJ_e.electrons[0].vertex.z()-WJ_e.electrons[1].vertex.z())<=Vertex) && (abs(WJ_e.jets[0].vertex.z()-WJ_e.jets[1].vertex.z())<=Vertex) 
             && (abs(abs(WJ_e.electrons[0].vertex.z()-WJ_e.electrons[1].vertex.z())-abs(WJ_e.jets[0].vertex.z()-WJ_e.jets[1].vertex.z()))<=Vertex)) {
	
	 //cut on  MW			  
	        WR = (WJ_e.electrons[0].p4 + WJ_e.electrons[1].p4 + WJ_e.jets[0].p4 + WJ_e.jets[1].p4).M(); 	  
		        if (WR>=MWCut) {
      
	 //cuts on  Mnu
            NuR_l0 = (WJ_e.electrons[0].p4 + WJ_e.jets[0].p4 + WJ_e.jets[1].p4).M();
            NuR_l1 = (WJ_e.electrons[1].p4 + WJ_e.jets[0].p4 + WJ_e.jets[1].p4).M();			
			
		          if (NuR_l0>=MN1Cut) {
				  if (NuR_l0>=MN2Cut) {
								
			WJ_n = WJ_n + 1;
									
        }
        }	
		}
	    }	
	    }	
        } 	
        }
	   }
	  }
	 }

  }
  
  WJ_n1Events = WJ_n1Events + WJ_nEvents; 
  WJ_nwEvents = WJ_nwEvents + WJ_nEvents*WJ_e.weight*A_Wei*Z_tune;
  WJ_n1 = WJ_n1 + WJ_n;
  WJ_nw = WJ_nw + WJ_n*WJ_e.weight*A_Wei*Z_tune;  
  WJ_nEvents = 0;
  WJ_n = 0;
  
}
 BG_nw = BG_nw + WJ_nw; // number of total BG events
}

*/

 
//============================================================================================================================ 
// MC BG ZZ
 
 
  fname = "/moscow31/heavynu/38x/ZZtoAnything_TuneZ2_7TeV-pythia6-tauola.root";
  
  TheEvent ZZ_e(fname);
  if (!ZZ_e) return;
  int ZZ_n = 0;
  float ZZ_nw = 0.;
  
if (ZZ_incl == true) { 


  int ZZ_nEvents = 0;  
  
  ZZ_nEvents = ZZ_e.totalEvents();
 
  for (int i = 0; i < ZZ_nEvents; ++i) {
    ZZ_e.readEvent(i);
       
    
      if ((ZZ_e.electrons.size() >= NofLept) && (ZZ_e.jets.size() >= NofJets)) {
	  
	   if (abs((ZZ_e.electrons[0].p4).Eta())<=1.44){A_Wei=HEEP_B;} else {A_Wei=HEEP_E;}
	   if (abs((ZZ_e.electrons[1].p4).Eta())<=1.44){A_Wei=A_Wei*HEEP_B;} else {A_Wei=A_Wei*HEEP_E;}
	
	//cuts on electrons Pt
	    if (((ZZ_e.electrons[0].p4).Pt()>=Lep1Pt) && ((ZZ_e.electrons[1].p4).Pt()>=Lep2Pt))  { 
    //cuts on jetss Pt      
	      if (((ZZ_e.jets[0].p4).Pt()>=Jet1Pt) && ((ZZ_e.jets[1].p4).Pt()>=Jet2Pt))  { 
		  
	//cuts on electrons invariant mass Mll	
		  if ((ZZ_e.electrons[0].p4 + ZZ_e.electrons[1].p4).M()>=MllCut) {
		  
	//cuts on Eta  
         if (((abs(ZZ_e.electrons[0].p4.Eta())<=EtaCut1L) || (abs(ZZ_e.electrons[1].p4.Eta())<=EtaCut2L)) 
	     && (abs(ZZ_e.jets[0].p4.Eta())<=EtaCut1J) && (abs(ZZ_e.jets[1].p4.Eta())<=EtaCut2J) )        {
			
	// Isolation
	       if ((ZZ_e.electrons[0].isoHEEP() <= ISO)&&(ZZ_e.electrons[0].isoHEEP() <= ISO)) {
		   
	 //Cut on vertexes
            if ((abs(ZZ_e.electrons[0].vertex.z()-ZZ_e.electrons[1].vertex.z())<=Vertex) && (abs(ZZ_e.jets[0].vertex.z()-ZZ_e.jets[1].vertex.z())<=Vertex) 
             && (abs(abs(ZZ_e.electrons[0].vertex.z()-ZZ_e.electrons[1].vertex.z())-abs(ZZ_e.jets[0].vertex.z()-ZZ_e.jets[1].vertex.z()))<=Vertex)) {

	 //cut on  MW			  
	        WR = (ZZ_e.electrons[0].p4 + ZZ_e.electrons[1].p4 + ZZ_e.jets[0].p4 + ZZ_e.jets[1].p4).M();  
		        if (WR>=MWCut) {
      
	 //cuts on  Mnu
            NuR_l0 = (ZZ_e.electrons[0].p4 + ZZ_e.jets[0].p4 + ZZ_e.jets[1].p4).M();
            NuR_l1 = (ZZ_e.electrons[1].p4 + ZZ_e.jets[0].p4 + ZZ_e.jets[1].p4).M();			
			
		          if (NuR_l0>=MN1Cut) {
				  if (NuR_l0>=MN2Cut) {	

							
			ZZ_n = ZZ_n + 1;
			
		}
        }
        }		
		}	
		}	
        } 	
        }
	   }
	  }
	}  
	    
  }
  
 ZZ_nw = ZZ_n*ZZ_e.weight*A_Wei;
 BG_nw = BG_nw + ZZ_nw; // number of total BG events
 
}
 
 
 
 
//============================================================================================================================
// MC BG ZW



  fname = "/moscow31/heavynu/38x/WZtoAnything_TuneZ2_7TeV-pythia6-tauola.root";
  
  TheEvent ZW_e(fname);
  if (!ZW_e) return;
  int ZW_n = 0;
  float ZW_nw = 0.;
  
if (ZW_incl == true) {


  int ZW_nEvents = 0; 
  
  ZW_nEvents = ZW_e.totalEvents();
 
  for (int i = 0; i < ZW_nEvents; ++i) {
    ZW_e.readEvent(i);
    

 
    
     // mass distributions
      if ((ZW_e.electrons.size() >= NofLept) && (ZW_e.jets.size() >= NofJets)) {
	  
	   if (abs((ZW_e.electrons[0].p4).Eta())<=1.44){A_Wei=HEEP_B;} else {A_Wei=HEEP_E;}
	   if (abs((ZW_e.electrons[1].p4).Eta())<=1.44){A_Wei=A_Wei*HEEP_B;} else {A_Wei=A_Wei*HEEP_E;}
	
	//cuts on electrons Pt
	    if (((ZW_e.electrons[0].p4).Pt()>=Lep1Pt) && ((ZW_e.electrons[1].p4).Pt()>=Lep2Pt))  { 
    //cuts on jetss Pt      
	      if (((ZW_e.jets[0].p4).Pt()>=Jet1Pt) && ((ZW_e.jets[1].p4).Pt()>=Jet2Pt))  { 
		  
	//cuts on electrons invariant mass Mll	
		  if ((ZW_e.electrons[0].p4 + ZW_e.electrons[1].p4).M()>=MllCut) {
		  
	//cuts on Eta  
         if (((abs(ZW_e.electrons[0].p4.Eta())<=EtaCut1L) || (abs(ZW_e.electrons[1].p4.Eta())<=EtaCut2L))
	     && (abs(ZW_e.jets[0].p4.Eta())<=EtaCut1J) && (abs(ZW_e.jets[1].p4.Eta())<=EtaCut2J) )        {
			
	// Isolation
	       if ((ZW_e.electrons[0].isoHEEP() <= ISO)&&(ZW_e.electrons[0].isoHEEP() <= ISO)) {
		   
	 //Cut on vertexes
            if ((abs(ZW_e.electrons[0].vertex.z()-ZW_e.electrons[1].vertex.z())<=Vertex) && (abs(ZW_e.jets[0].vertex.z()-ZW_e.jets[1].vertex.z())<=Vertex) 
             && (abs(abs(ZW_e.electrons[0].vertex.z()-ZW_e.electrons[1].vertex.z())-abs(ZW_e.jets[0].vertex.z()-ZW_e.jets[1].vertex.z()))<=Vertex)) {

	 //cut on  MW			  
	        WR = (ZW_e.electrons[0].p4 + ZW_e.electrons[1].p4 + ZW_e.jets[0].p4 + ZW_e.jets[1].p4).M(); 
		        if (WR>=MWCut) {
      
	 //cuts on  Mnu
            NuR_l0 = (ZW_e.electrons[0].p4 + ZW_e.jets[0].p4 + ZW_e.jets[1].p4).M();
            NuR_l1 = (ZW_e.electrons[1].p4 + ZW_e.jets[0].p4 + ZW_e.jets[1].p4).M();			
			
		          if (NuR_l0>=MN1Cut) {
				  if (NuR_l0>=MN2Cut) {				 
							
			ZW_n = ZW_n + 1;
		
        }
        }
        }		
		}	
		}	
        } 	
        }
	   }
	  }
	}  
	    
  }
  
 ZW_nw = ZW_n*ZW_e.weight*A_Wei;
 BG_nw = BG_nw + ZW_nw; // number of total BG events
}
 
 
 
 
 
//============================================================================================================================
// MC BG WW
  


  
  
  fname = "/moscow31/heavynu/38x/WWtoAnything_TuneZ2_7TeV-pythia6-tauola.root";
  
  TheEvent WW_e(fname);
  if (!WW_e) return;
  int WW_n = 0;
  float WW_nw = 0.;
  
if (WW_incl == true) {

  int WW_nEvents = 0;  
  
  WW_nEvents = WW_e.totalEvents();
 
  for (int i = 0; i < WW_nEvents; ++i) {
    WW_e.readEvent(i);
    

      if ((WW_e.electrons.size() >= NofLept) && (WW_e.jets.size() >= NofJets)) {
	  
	   if (abs((WW_e.electrons[0].p4).Eta())<=1.44){A_Wei=HEEP_B;} else {A_Wei=HEEP_E;}
	   if (abs((WW_e.electrons[1].p4).Eta())<=1.44){A_Wei=A_Wei*HEEP_B;} else {A_Wei=A_Wei*HEEP_E;}
	
	//cuts on electrons Pt
	    if (((WW_e.electrons[0].p4).Pt()>=Lep1Pt) && ((WW_e.electrons[1].p4).Pt()>=Lep2Pt))  { 
    //cuts on jetss Pt      
	      if (((WW_e.jets[0].p4).Pt()>=Jet1Pt) && ((WW_e.jets[1].p4).Pt()>=Jet2Pt))  { 
		  
	//cuts on electrons invariant mass Mll	
		  if ((WW_e.electrons[0].p4 + WW_e.electrons[1].p4).M()>=MllCut) {
		  
	//cuts on Eta  
         if (((abs(WW_e.electrons[0].p4.Eta())<=EtaCut1L) || (abs(WW_e.electrons[1].p4.Eta())<=EtaCut2L)) 
	     && (abs(WW_e.jets[0].p4.Eta())<=EtaCut1J) && (abs(WW_e.jets[1].p4.Eta())<=EtaCut2J) )        {
			
	// Isolation
	       if ((WW_e.electrons[0].isoHEEP() <= ISO)&&(WW_e.electrons[0].isoHEEP() <= ISO)) {
		   
	 //Cut on vertexes
            if ((abs(WW_e.electrons[0].vertex.z()-WW_e.electrons[1].vertex.z())<=Vertex) && (abs(WW_e.jets[0].vertex.z()-WW_e.jets[1].vertex.z())<=Vertex) 
             && (abs(abs(WW_e.electrons[0].vertex.z()-WW_e.electrons[1].vertex.z())-abs(WW_e.jets[0].vertex.z()-WW_e.jets[1].vertex.z()))<=Vertex)) {
	
	 //cut on  MW			  
	        WR = (WW_e.electrons[0].p4 + WW_e.electrons[1].p4 + WW_e.jets[0].p4 + WW_e.jets[1].p4).M();
		        if (WR>=MWCut) {
      
	 //cuts on  Mnu
            NuR_l0 = (WW_e.electrons[0].p4 + WW_e.jets[0].p4 + WW_e.jets[1].p4).M();
            NuR_l1 = (WW_e.electrons[1].p4 + WW_e.jets[0].p4 + WW_e.jets[1].p4).M();			
			
		          if (NuR_l0>=MN1Cut) {
				  if (NuR_l0>=MN2Cut) {			
								
			WW_n = WW_n + 1;
			
		}
	    }	
		}
		}	
		}	
        } 	
        }
	   }
	  }
	}  
	    
  }
 WW_nw = WW_n*WW_e.weight*A_Wei;
 BG_nw = BG_nw + WW_nw; // number of total BG events

 
}


//============================================================================================================================
// MC BG TW
  


  
  
  //fname = "/moscow31/heavynu/38x/TW_dr_7TeV-mcatnlo.root";
  fname = "/moscow31/heavynu/38x/TToBLNu_TuneZ2_tW-channel_7TeV-madgraph.root";
  
  TheEvent TW_e(fname);
  if (!TW_e) return;
  int TW_n = 0;
  float TW_nw = 0.;
  
if (TW_incl == true) {

  int TW_nEvents = 0;  
  
  TW_nEvents = TW_e.totalEvents();
 
  for (int i = 0; i < TW_nEvents; ++i) {
    TW_e.readEvent(i);
    

      if ((TW_e.electrons.size() >= NofLept) && (TW_e.jets.size() >= NofJets)) {
	  
	   if (abs((TW_e.electrons[0].p4).Eta())<=1.44){A_Wei=HEEP_B;} else {A_Wei=HEEP_E;}
	   if (abs((TW_e.electrons[1].p4).Eta())<=1.44){A_Wei=A_Wei*HEEP_B;} else {A_Wei=A_Wei*HEEP_E;}
	
	//cuts on electrons Pt
	    if (((TW_e.electrons[0].p4).Pt()>=Lep1Pt) && ((TW_e.electrons[1].p4).Pt()>=Lep2Pt))  { 
    //cuts on jetss Pt      
	      if (((TW_e.jets[0].p4).Pt()>=Jet1Pt) && ((TW_e.jets[1].p4).Pt()>=Jet2Pt))  { 
		  
	//cuts on electrons invariant mass Mll	
		  if ((TW_e.electrons[0].p4 + TW_e.electrons[1].p4).M()>=MllCut) {
		  
	//cuts on Eta  
         if (((abs(TW_e.electrons[0].p4.Eta())<=EtaCut1L) || (abs(TW_e.electrons[1].p4.Eta())<=EtaCut2L)) 
	     && (abs(TW_e.jets[0].p4.Eta())<=EtaCut1J) && (abs(TW_e.jets[1].p4.Eta())<=EtaCut2J) )        {
			
	// Isolation
	       if ((TW_e.electrons[0].isoHEEP() <= ISO)&&(TW_e.electrons[0].isoHEEP() <= ISO)) {
		   
	 //Cut on vertexes
            if ((abs(TW_e.electrons[0].vertex.z()-TW_e.electrons[1].vertex.z())<=Vertex) && (abs(TW_e.jets[0].vertex.z()-TW_e.jets[1].vertex.z())<=Vertex) 
             && (abs(abs(TW_e.electrons[0].vertex.z()-TW_e.electrons[1].vertex.z())-abs(TW_e.jets[0].vertex.z()-TW_e.jets[1].vertex.z()))<=Vertex)) {
	
	 //cut on  MW			  
	        WR = (TW_e.electrons[0].p4 + TW_e.electrons[1].p4 + TW_e.jets[0].p4 + TW_e.jets[1].p4).M();
		        if (WR>=MWCut) {
      
	 //cuts on  Mnu
            NuR_l0 = (TW_e.electrons[0].p4 + TW_e.jets[0].p4 + TW_e.jets[1].p4).M();
            NuR_l1 = (TW_e.electrons[1].p4 + TW_e.jets[0].p4 + TW_e.jets[1].p4).M();			
			
		          if (NuR_l0>=MN1Cut) {
				  if (NuR_l0>=MN2Cut) {			
								
			TW_n = TW_n + 1;
			
		}
	    }	
		}
		}	
		}	
        } 	
        }
	   }
	  }
	}  
	    
  }
 TW_nw = TW_n*TW_e.weight*A_Wei;
 BG_nw = BG_nw + TW_nw; // number of total BG events

 
}


//=========================================================================================================================
// Sensitivity

float F_L= Exp_lim_cut_opt(S_Eff, Lum*100., BG_nw*Lum );
 
float F = S_nw*Lum/sqrt(S_nw*Lum + BG_nw*Lum);

float F1 = S_nw*Lum/sqrt(BG_nw*Lum);

float F2 =sqrt(S_nw*Lum + BG_nw*Lum) - sqrt(BG_nw*Lum);

//cout << "S_e.weight = " << S_e.weight << "S_n = " << S_n << "S_nw = " << S_nw << "S_nw*Lum = "<<S_nw*Lum <<"S_Eff = " << S_Eff <<endl ;
//cout << "TTb_e.weight = " << TTb_e.weight << "TTb_n = " << TTb_n << "TTb_nw = " << TTb_nw << "TTb_nw*Lum = "<<TTb_nw*Lum << endl ; 
//cout << "ZJ_n1 = " << ZJ_n1 << "ZJ_nw = " << ZJ_nw << "ZJ_nw*Lum = "<<ZJ_nw*Lum << endl ;  
//cout << MllCut <<" "<< F << endl;
cout << MWCut <<" "<< F_L << endl;//EtaCut1L
    

fprintf(Ptr,"%f   %f %f %f %f   %d %f   %d %f   %d %f   %d %f   %d %f     %f\n",MWCut, F_L, F, F1, F2, S_n, S_nw*Lum, QCD_n, QCD_nw*Lum, TTb_n, TTb_nw*Lum, ZJ_n1, ZJ_nw*Lum, WJ_n+ZW_n+ZZ_n+WW_n, (WJ_nw+ZW_nw+ZZ_nw+WW_nw)*Lum, BG_nw*Lum );

 }
 
  fclose(Ptr);   
}

