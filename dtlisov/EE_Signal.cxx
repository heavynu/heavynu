//To run this script on pcrfcms14.cern.ch machine: 
//root -b -q -l EE_Signal.cxx+
#include "TH1.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TROOT.h"
#include <TLorentzVector.h>
#include <TMath.h>
#include <TLatex.h>
#include "TStyle.h"
#include "TLegend.h"
#include "TText.h"
#include "TAxis.h"
#include "../../PhysicsTools/Utilities/interface/LumiReweightingStandAlone.h"
//#include "../../PhysicsTools/Utilities/interface/Lumi3DReWeighting.h"

#include "tdrstyle.C"
//#include "heavynuStandard.C"
#include "heavynuStandard11.C"

#include "../PATDump/src/format.h"
#include "../sampledb.h"
#include <iostream>
#include <fstream>
using namespace std;

vector<double> generate_flat10_weights(TH1D* data_npu_estimated){
   // see SimGeneral/MixingModule/python/mix_E7TeV_FlatDist10_2011EarlyData_inTimeOnly_cfi.py; copy and paste from there:
// for spring11 and summer11, if out of time is not important:
//   const double npu_probs[25] = {0.0698146584, 0.0698146584, 0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,
//          0.0630151648,0.0526654164,0.0402754482,0.0292988928,0.0194384503,0.0122016783,0.007207042,0.004003637,0.0020278322,
//          0.0010739954,0.0004595759,0.0002229748,0.0001028162,4.58337152809607E-05 /* <-- 24 */};
// for summer11 PU_S4, obtained by averaging:
   const double npu_probs[25] = {0.104109, 0.0703573, 0.0698445, 0.0698254, 0.0697054, 0.0697907, 0.0696751, 0.0694486, 0.0680332,
                                 0.0651044, 0.0598036, 0.0527395, 0.0439513, 0.0352202, 0.0266714, 0.019411, 0.0133974, 0.00898536,
                                 0.0057516, 0.00351493, 0.00212087, 0.00122891, 0.00070592, 0.000384744, 0.000219377};
   vector<double> result(25);
   double s = 0.0;
   for(int npu=0; npu<25; ++npu){
       double npu_estimated = data_npu_estimated->GetBinContent(data_npu_estimated->GetXaxis()->FindBin(npu));                              
       result[npu] = npu_estimated / npu_probs[npu];
       s += npu_estimated;
   }
   // normalize weights such that the total sum of weights over thw whole sample is 1.0, i.e., sum_i  result[i] * npu_probs[i] should be 1.0 (!)
   for(int npu=0; npu<25; ++npu){
       result[npu] /= s;
   }
   return result;
}


void EE_Signal()
{

  // activate storage of sum of squares of errors
  // to calculate correctly errors in histograms
  // with non-unit entries
  TH1::SetDefaultSumw2(kTRUE);
 
  TFile* f_A1 = new TFile("PileUp/2011A_pileup.root");
  TFile* f_B1 = new TFile("PileUp/2011B_pileup.root");
  TH1D* histo_A1 = (TH1D*) f_A1->Get("pileup");
  TH1D* histo_B1 = (TH1D*) f_B1->Get("pileup");
  
  /*
  TFile* f_A = new TFile("PileUp/2011A_pileupTruth.root");
  TFile* f_B = new TFile("PileUp/2011B_pileupTruth.root");
  TH1D* histo_A = (TH1D*) f_A->Get("pileup");
  TH1D* histo_B = (TH1D*) f_B->Get("pileup");
     
  Double_t PoissonOneXDist[35] = {
    1.45346E-01,
    6.42802E-02,
    6.95255E-02,
    6.96747E-02,
    6.92955E-02,
    6.84997E-02,
    6.69528E-02,
    6.45515E-02,
    6.09865E-02,
    5.63323E-02,
    5.07322E-02,
    4.44681E-02,
    3.79205E-02,
    3.15131E-02,
    2.54220E-02,
    2.00184E-02,
    1.53776E-02,
    1.15387E-02,
    8.47608E-03,
    6.08715E-03,
    4.28255E-03,
    2.97185E-03,
    2.01918E-03,
    1.34490E-03,
    8.81587E-04,
    5.69954E-04,
    3.61493E-04,
    2.28692E-04,
    1.40791E-04,
    8.44606E-05,
    5.10204E-05,
    3.07802E-05,
    1.81401E-05,
    1.00201E-05,
    5.80004E-06
  };
  
  std::vector< float > DataPileUp_A ;
  std::vector< float > DataPileUp_B ;
  std::vector< float > MCPileUp ;
  for( int i=0; i<35; ++i) {
      DataPileUp_A.push_back(histo_A->GetBinContent(i+1));
      DataPileUp_B.push_back(histo_B->GetBinContent(i+1));
      MCPileUp.push_back(PoissonOneXDist[i]);
    }
    
  reweight::LumiReWeighting LumiWeights_A = reweight::LumiReWeighting(MCPileUp, DataPileUp_A); 
  LumiWeights_A.weight3D_init("Weight3D_2011A.root");   
  reweight::LumiReWeighting LumiWeights_B = reweight::LumiReWeighting(MCPileUp, DataPileUp_B);  
  LumiWeights_B.weight3D_init("Weight3D_2011B.root"); 
  Double_t Corr[35] = { 
    0.,
    0.887809,
    0.889615,
    0.902631,
    0.884461,
    0.88004,
    0.865174,
    0.870073,
    0.873842,
    0.8978,
    0.92261,
    0.988201,
    1.05329,
    1.13407,
    1.2587,
    1.39528,
    1.55154,
    1.77817,
    2.02186,
    2.43142,
    2.83008,
    3.47422,
    4.6109,
    5.55045,

    5.88748,
    7.18155,
    10.9544,
    12.0279,
    13.5533,
    11.3418,
    28.946,
    14.0245,
    35.8912,
    9.13846,
    10.4667
    };
*/  
// Cuts for event selection
  unsigned int NofLept = 2;
  unsigned int NofJets = 2;
  float Lep1Pt = 60.;
  float Lep2Pt = 40.;
  float JetPt = 40.;
  float EtaCut1L = 1.44; //1.44
  float EtaCut2L = EtaCut1L;
  float EtaCutJ = 2.5;
  float ISO = 1.;
  int HeepId = 0;
//  float Vertex = 100.03;
  float MllCut = 200.0;
  float MllCut1 = 1200000.0;
  float MW_Cut = 600.0; 
 
// M_W histogramm parameters  
  int M_Delta_M_W = 12;
  double M_min_M_W = 600;
  double M_max_M_W = 3000; 
  
 float WR; 
  float Mll, MNu1, MNu2; 
  float HEEP_A_B=0.996;
  float HEEP_A_E=0.857;
  float HEEP_B_B=0.977;
  float HEEP_B_E=0.854;
  float A_Wei=1.;
  float B_Wei=1.;
  
  char *str = (char*)alloca(10000);
  char *str1 = (char*)alloca(10000);
  
    // finding signal samples in sample.db
  const SampleDB db;
  const SampleDB sa = db./*find("MW", 2000).find("MNu", 1000).*/find("type", 1).find("channel", 1).find("AlCa", "START42_V15B::All")/*.find("CMSSW", "4_2_8_patch7")*/;

  string root = "/moscow31/heavynu/42x-signal-central/";

  TH1F* hMW_A[100];  
  TH1F* hMW_B[100];

  for (int i = 0; i <= 100; ++i) { 
    sprintf(str,"MW_A_%d", i);
    sprintf(str1,"MW_A; M(eejj) ,GeV;Events/%5.1f GeV", (M_max_M_W-M_min_M_W)/M_Delta_M_W );
    hMW_A[i] = new TH1F(str,str1, M_Delta_M_W, M_min_M_W, M_max_M_W); 
    sprintf(str,"MW_B_%d", i);
    sprintf(str1,"MW_B; M(eejj), GeV;Events/%5.1f GeV", (M_max_M_W-M_min_M_W)/M_Delta_M_W );
    hMW_B[i] = new TH1F(str,str1, M_Delta_M_W, M_min_M_W, M_max_M_W);  
  }

  string fname[100];
  float Lum[100];
  int n[100];
  int nEvents[100];
  float Weight_A, Weight_B;
  TLorentzVector EPT0_A, EPT1_A, EPT0_B, EPT1_B;
  TLorentzVector JPT0_A, JPT1_A, JPT0_B, JPT1_B;
  
  for (int S_i = 0; S_i < sa.size(); ++S_i) {
     fname[S_i+2] = root +  sa[S_i].fname;
	 cout<<fname[S_i+2]<< endl;
  }  
  fname[0] = "/moscow41/heavynu/Data_2011/DElectron_2011A.root";
  fname[1] = "/moscow41/heavynu/Data_2011/DElectron_B_v1.root";
  cout<<fname[0]<< endl;
  cout<<fname[1]<< endl;
  for (int j = 0; j < sa.size()+2; ++j) {
      float Sum_A = 0.;
      float Sum_B = 0.;
      float Sum_A_n = 0.;
      float Sum_B_n = 0.;
      double np_A = 0.;
      double np_B = 0.;
      TheEvent e(fname[j]);
	  //if (j>=2) {TheEvent e(fname[j].c_str());}
      if (!e) return;
      Lum[j] = e.sample.lumi; //Luminocity in pb^-1
      n[j] = 0;
      nEvents[j] = e.totalEvents();
      for (int i = 0; i < nEvents[j]; ++i) {
          e.readEvent(i);
	  Weight_A = 1.;
	  Weight_B = 1.;
	  if (j>=2){  
              /*
	      Weight_A = Lum[0]/100.* e.weight * e.Gen_weight * LumiWeights_A.weight3D(e.num_interactions(-1),e.num_interactions(0),e.num_interactions(1));   
		 // cout<<"A "<< LumiWeights_A.weight3D(e.num_interactions(-1),e.num_interactions(0),e.num_interactions(1)) << "     "<< Weight_A<< endl;
	      Weight_B = Lum[1]/100.* e.weight * e.Gen_weight * LumiWeights_B.weight3D(e.num_interactions(-1),e.num_interactions(0),e.num_interactions(1));
	      if(e.vertices.size()<35){
	         Weight_B = Weight_B * Corr[e.vertices.size()];
	      }
	      //cout<<"B "<< LumiWeights_B.weight3D(e.num_interactions(-1),e.num_interactions(0),e.num_interactions(1)) << "     "<< Weight_B<< endl;
	      */
	      
	      if(e.pileupIT()<25){
                // Weight_A = Lum[0]/100.* e.weight * e.Gen_weight *  generate_flat10_weights(histo_A1)[e.pileupIT()];
		// Weight_B = Lum[1]/100.* e.weight * e.Gen_weight *  generate_flat10_weights(histo_B1)[e.pileupIT()];
                 Weight_A =  e.Gen_weight *  generate_flat10_weights(histo_A1)[e.pileupIT()];
		 Weight_B =  e.Gen_weight *  generate_flat10_weights(histo_B1)[e.pileupIT()];		
		
              }
              else{ //should not happen as we have a weight for all simulated ZJ_e.pileup.size() multiplicities!
                 Weight_A = 0.;
	         Weight_B = 0.;
              }	
	      
	  }
	  
	  for (unsigned int cur=0;cur<e.electrons.size();cur++) {
	       if (!((e.electrons[cur].ID <= HeepId)&&(e.electrons[cur].isoHEEP() <= ISO))) {e.electrons.erase(e.electrons.begin()+cur);}
	  }
	  for (unsigned int cur=0;cur<e.jets.size();cur++) {
	       if (!((e.jets[cur].ID >= 1)&&(abs((e.jets[cur].p4).Eta())<=EtaCutJ))) {e.jets.erase(e.jets.begin()+cur);}
	  }
	  
	  Sum_A+= Weight_A;
	  Sum_A_n+= e.Gen_weight;
          Sum_B+= Weight_B;
	  Sum_B_n+=  e.Gen_weight;
	  
          if ((e.electrons.size() >= NofLept) && (e.jets.size() >= NofJets)) {
	      if (j>=2){
	           if (abs((e.electrons[0].p4).Eta())<=1.44){
		       A_Wei=HEEP_A_B; B_Wei=HEEP_B_B;
		       EPT0_A.SetPxPyPzE(e.electrons[0].p4.Px()*91./91.3, e.electrons[0].p4.Py()*91./91.3, e.electrons[0].p4.Pz()*91./91.3,e.electrons[0].p4.E()*91./91.3);
		       EPT0_B.SetPxPyPzE(e.electrons[0].p4.Px()*91./91.8, e.electrons[0].p4.Py()*91./91.8, e.electrons[0].p4.Pz()*91./91.8,e.electrons[0].p4.E()*91./91.8);
		   } else {
		       A_Wei=HEEP_A_E; B_Wei=HEEP_B_E; 
		       EPT0_A.SetPxPyPzE(e.electrons[0].p4.Px()*92.45/92., e.electrons[0].p4.Py()*92.45/92., e.electrons[0].p4.Pz()*92.45/92., e.electrons[0].p4.E()*92.45/92.);  
		       EPT0_B.SetPxPyPzE(e.electrons[0].p4.Px()*93.3/92., e.electrons[0].p4.Py()*93.3/92., e.electrons[0].p4.Pz()*93.3/92., e.electrons[0].p4.E()*93.3/92.);
		   }
	           if (abs((e.electrons[1].p4).Eta())<=1.44){
		       A_Wei=A_Wei*HEEP_A_B; B_Wei=B_Wei*HEEP_B_B;
		       EPT1_A.SetPxPyPzE(e.electrons[1].p4.Px()*91./91.3, e.electrons[1].p4.Py()*91./91.3, e.electrons[1].p4.Pz()*91./91.3, e.electrons[1].p4.E()*91./91.3);
		       EPT1_B.SetPxPyPzE(e.electrons[1].p4.Px()*91./91.8, e.electrons[1].p4.Py()*91./91.8, e.electrons[1].p4.Pz()*91./91.8, e.electrons[1].p4.E()*91./91.8);
		   } else {
		       A_Wei=A_Wei*HEEP_A_E;
		       B_Wei=B_Wei*HEEP_B_E; 
		       EPT1_A.SetPxPyPzE(e.electrons[1].p4.Px()*92.45/92., e.electrons[1].p4.Py()*92.45/92., e.electrons[1].p4.Pz()*92.45/92., e.electrons[1].p4.E()*92.45/92.); 
		       EPT1_B.SetPxPyPzE(e.electrons[1].p4.Px()*93.3/92., e.electrons[1].p4.Py()*93.3/92., e.electrons[1].p4.Pz()*93.3/92., e.electrons[1].p4.E()*93.3/92.);
		   }  
	               Weight_A = Weight_A * A_Wei;
	               Weight_B = Weight_B * B_Wei;
	           if ((abs((e.electrons[0].p4).Eta())>=2.1)&&(e.electrons[0].p4.Pt()<=45.)&&(e.electrons[1].p4.Pt()>=40.)){
	               Weight_A=Weight_A*(0.08*(e.electrons[0].p4.Pt()-40.)+0.6); 
		       Weight_B=Weight_B*(0.08*(e.electrons[0].p4.Pt()-40.)+0.6);} 
	           if ((abs((e.electrons[1].p4).Eta())>=2.1)&&(e.electrons[1].p4.Pt()<=45.)&&(e.electrons[1].p4.Pt()>=40.)){
	               Weight_A=Weight_A*(0.08*(e.electrons[1].p4.Pt()-40.)+0.6); 
		       Weight_B=Weight_B*(0.08*(e.electrons[1].p4.Pt()-40.)+0.6);} 	          
	       } else  {
	        //   Weight_A = Weight_A/Prescale;
	        //   Weight_B = Weight_B/Prescale;
		   EPT0_A.SetPxPyPzE(e.electrons[0].p4.Px(), e.electrons[0].p4.Py(), e.electrons[0].p4.Pz(), e.electrons[0].p4.E()); 
		   EPT0_B.SetPxPyPzE(e.electrons[0].p4.Px(), e.electrons[0].p4.Py(), e.electrons[0].p4.Pz(), e.electrons[0].p4.E());
		   EPT1_A.SetPxPyPzE(e.electrons[1].p4.Px(), e.electrons[1].p4.Py(), e.electrons[1].p4.Pz(), e.electrons[1].p4.E()); 
		   EPT1_B.SetPxPyPzE(e.electrons[1].p4.Px(), e.electrons[1].p4.Py(), e.electrons[1].p4.Pz(), e.electrons[1].p4.E());
	       }
               
	       	   JPT0_A.SetPxPyPzE(e.jets[0].p4.Px(), e.jets[0].p4.Py(), e.jets[0].p4.Pz(), e.jets[0].p4.E()); 
		   JPT0_B.SetPxPyPzE(e.jets[0].p4.Px(), e.jets[0].p4.Py(), e.jets[0].p4.Pz(), e.jets[0].p4.E());
		   JPT1_A.SetPxPyPzE(e.jets[1].p4.Px(), e.jets[1].p4.Py(), e.jets[1].p4.Pz(), e.jets[1].p4.E()); 
		   JPT1_B.SetPxPyPzE(e.jets[1].p4.Px(), e.jets[1].p4.Py(), e.jets[1].p4.Pz(), e.jets[1].p4.E());
	      
	       if ((e.jets[0].p4.Pt()>=JetPt)&&(e.jets[0].ID>=1)&&(abs((e.jets[0].p4).Eta())<=EtaCutJ)&&
		   (e.jets[1].p4.Pt()>=JetPt)&&(e.jets[1].ID>=1)&&(abs((e.jets[1].p4).Eta())<=EtaCutJ)) {

	          if ((EPT0_A.Pt()>=Lep1Pt)&&(EPT1_A.Pt()>=Lep2Pt)&& 
	             ((EPT0_A + EPT1_A).M()>=MllCut)&&((EPT0_A + EPT1_A).M()<=MllCut1)&&
		     ((abs(e.electrons[0].p4.Eta())<=EtaCut1L)||(abs(e.electrons[1].p4.Eta())<=EtaCut2L))&&
	             (e.electrons[0].ID <= HeepId)&&(e.electrons[1].ID <= HeepId)&&
		     (e.electrons[0].isoHEEP() <= ISO)&&(e.electrons[1].isoHEEP() <= ISO)&&
	             ((EPT0_A + EPT1_A + JPT0_A + JPT1_A).M()>=MW_Cut)) {

		        WR = (EPT0_A + EPT1_A + JPT0_A + JPT1_A).M(); 
			hMW_A[j]->Fill(WR,Weight_A);
                        if (j>=2) np_A+=e.Gen_weight * e.Gen_weight *  generate_flat10_weights(histo_A1)[e.pileupIT()];  
   
		   }
		   
		   if ((EPT0_B.Pt()>=Lep1Pt)&&(EPT1_B.Pt()>=Lep2Pt)&& 
	              ((EPT0_B + EPT1_B).M()>=MllCut)&&((EPT0_B + EPT1_B).M()<=MllCut1)&&
		      ((abs(e.electrons[0].p4.Eta())<=EtaCut1L)||(abs(e.electrons[1].p4.Eta())<=EtaCut2L))&&
	              (e.electrons[0].ID <= HeepId)&&(e.electrons[1].ID <= HeepId)&&
		      (e.electrons[0].isoHEEP() <= ISO)&&(e.electrons[1].isoHEEP() <= ISO)&&
	              ((EPT0_B + EPT1_B + JPT0_B + JPT1_B).M()>=MW_Cut)) {  
		      
		       	WR = (EPT0_B + EPT1_B + JPT0_B + JPT1_B).M(); 
			hMW_B[j]->Fill(WR,Weight_B);
			if (j>=2) np_B+=e.Gen_weight * e.Gen_weight *  generate_flat10_weights(histo_B1)[e.pileupIT()];
	           }       
               }
	  }
      }
  if (j>=2){   
	//hMW_A[j]->Scale(np_A/(e.sample.stat*hMW_A[j]->Integral()));
	//hMW_B[j]->Scale(np_B/(e.sample.stat*hMW_B[j]->Integral()));  
	cout << j<< "  " << e.sample.stat<<"  "<< Sum_A <<"   "<< Sum_A_n << endl;
	hMW_A[j]->Scale(1./e.sample.stat);
	hMW_B[j]->Scale(1./e.sample.stat); 
  
//============================================================================================================================
// Histos plotting and saving

  // ============================================
  // save to .root file M_WR histograms with:
  //  1. MC: summ of all backgrounds
  //  2. MC: signal
  //  3. DATA:
  // {
//	TTb_hWR.Sumw2();
//	ZJ_hWR.Sumw2();
//	TW_hWR.Sumw2();
//	RD_hWR.Sumw2();
    sprintf(str1,"MW%d-MN%d.root", e.sample.MW, e.sample.MNu); 
    TFile f(str1, "RECREATE");
    hMW_A[j]->Clone("Signal_A")->Write();
    hMW_B[j]->Clone("Signal_B")->Write();
   // hMW_A[j]->Add(hMW_B[j]);
   // hMW_A[j]->Scale(0.5);
    hMW_A[j]->Clone("Signal")->Write();
    hMW_A[0]->Clone("Data_A")->Write();
    hMW_B[1]->Clone("Data_B")->Write();
    f.Close();
  // }
  // ============================================
  }
  else {
  cout<< j <<"   "<< hMW_A[j]->Integral()<<"    "<<hMW_B[j]->Integral() <<endl;
  }
  
 }
 
}

