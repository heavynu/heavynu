// to run script do:
//   $ root -b -q -l EE_Sys_calc.cxx+

#include <iostream>
#include <fstream>
using namespace std;

void EE_Sys_calc()
{
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Inizialization

  
  int INFORM = 1;
    
  int sa_size = 300;
  
  float JES[300][11] = {0};
  float JES1[300][11] = {0};
  float JES2[300][11] = {0};

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
  
  
  FILE *Ptr;
  Ptr = fopen("Systematics.txt","w");
  

  int SA = 0;
  int CUT_Chn = 0;	
  int CUT_MWR[300] = {0};
  int CUT_MNu[300] = {0};
  
  int CUT_Chn1 = 0;	
  int CUT_MWR1 = 0;
  int CUT_MNu1 = 0;
  int CUT_Chn2 = 0;	
  int CUT_MWR2 = 0;
  int CUT_MNu2 = 0;
  
  float AUX;
 
  string fname = "/afs/cern.ch/user/d/dtlisov/CMSSW_3_8_7/src/heavynu/OptimumCuts_E.txt";
  string fname1 = "/afs/cern.ch/user/d/dtlisov/CMSSW_3_8_7/src/heavynu/JES1_E.txt";
  string fname2 = "/afs/cern.ch/user/d/dtlisov/CMSSW_3_8_7/src/heavynu/JES2_E.txt";
  
  char str[500] = {" "};
  ifstream inFile(fname.c_str());
  inFile.getline(str, 500);
  
  ifstream inFile1(fname1.c_str());
  inFile1.getline(str, 500);  
  
  ifstream inFile2(fname2.c_str());
  inFile2.getline(str, 500); 

  while (!inFile.eof()) {
   inFile >> CUT_Chn;
   inFile >> CUT_MWR[SA];
   inFile >> CUT_MNu[SA];
   
   inFile1 >> CUT_Chn1;
   inFile1 >> CUT_MWR1;
   inFile1 >> CUT_MNu1;
   
   inFile2 >> CUT_Chn2;
   inFile2 >> CUT_MWR2;
   inFile2 >> CUT_MNu2;

   if (INFORM == 1) { 
    cout << "Chn = "<< CUT_Chn <<" MWR = " << CUT_MWR[SA] << " MNu = "<< CUT_MNu[SA] <<endl;
   }
	    inFile >> AUX;
        inFile >> AUX;
		inFile >> AUX;
        inFile >> AUX;	
	    inFile >> AUX;
        inFile >> AUX;
        inFile >> AUX;
        inFile >> AUX;
		inFile >> AUX;
		inFile >> AUX;
		inFile >> JES[SA][1];
        inFile >> JES[SA][2];
		cout << "eff = "<< JES[SA][1] <<" BG = " << JES[SA][2] <<endl;
        inFile >> AUX;
		inFile >> AUX;
		inFile >> AUX;
        inFile >> JES[SA][3];
		inFile >> AUX;
		inFile >> JES[SA][4]; 
		inFile >> AUX;
		inFile >> JES[SA][5];
		inFile >> JES[SA][6];
		inFile >> JES[SA][7];
		inFile >> JES[SA][8];
		inFile >> JES[SA][9];
		
        inFile.getline(str, 500);

	    inFile1 >> AUX;
        inFile1 >> AUX;
		inFile1 >> AUX;
        inFile1 >> AUX;	
	    inFile1 >> AUX;
        inFile1 >> AUX;
        inFile1 >> AUX;
        inFile1 >> AUX;
		inFile1 >> AUX;
		inFile1 >> AUX;
		inFile1 >> JES1[SA][1];
        inFile1 >> JES1[SA][2];
cout << "eff = "<< JES1[SA][1] <<" BG = " << JES1[SA][2] <<endl;		
        inFile1 >> AUX;
		inFile1 >> AUX;
		inFile1 >> AUX;
        inFile1 >> JES1[SA][3];
		inFile1 >> AUX;
		inFile1 >> JES1[SA][4]; 
		inFile1 >> AUX;
		inFile1 >> JES1[SA][5];
		inFile1 >> JES1[SA][6];
		inFile1 >> JES1[SA][7];
		inFile1 >> JES1[SA][8];
		inFile1 >> JES1[SA][9];
        inFile1.getline(str, 500);
	    inFile2 >> AUX;
        inFile2 >> AUX;
		inFile2 >> AUX;
        inFile2 >> AUX;	
	    inFile2 >> AUX;
        inFile2 >> AUX;
        inFile2 >> AUX;
        inFile2 >> AUX;
		inFile2 >> AUX;
		inFile2 >> AUX;
		inFile2 >> JES2[SA][1];
        inFile2 >> JES2[SA][2];
        inFile2 >> AUX;
		inFile2 >> AUX;
		inFile2 >> AUX;
        inFile2 >> JES2[SA][3];
		inFile2 >> AUX;
		inFile2 >> JES2[SA][4]; 
		inFile2 >> AUX;
		inFile2 >> JES2[SA][5];
		inFile2 >> JES2[SA][6];
		inFile2 >> JES2[SA][7];
		inFile2 >> JES2[SA][8];
		inFile2 >> JES2[SA][9];
		inFile2.getline(str, 500);			
	
        SA=SA+1;		
  }
 
  fprintf(Ptr,"Chn MWR MNu  S     S   BG   BG   QCD+ QCD-  TTB+ TTB-  ZJ+   ZJ-   Other+ Other- \n"); 
  for (int S_i = 0; S_i < sa_size; ++S_i) {
  
//   cout <<"MWR = " << CUT_MWR[S_i]<< " MNu = " << CUT_MNu[S_i]  <<endl; 
  
   for(int i=1; i<=5; ++i) {
  
//   cout <<"i= " << i << " JES1[S_i][i] = "<< JES1[S_i][i] <<endl;
  
   JES1[S_i][i]= (JES1[S_i][i]-JES[S_i][i])/JES[S_i][i];
      
//   cout << "JES1[S_i][i] = "<< JES1[S_i][i] <<endl;

   JES2[S_i][i]= (JES[S_i][i]-JES2[S_i][i])/JES[S_i][i];   
 }
    
   for(int i=7; i<10; ++i) {
   JES[S_i][6]= JES[S_i][6]+JES[S_i][i];
   JES1[S_i][6]= JES1[S_i][6]+JES1[S_i][i];
   JES2[S_i][6]= JES2[S_i][6]+JES2[S_i][i];   
 }
  
   JES1[S_i][6]= (JES1[S_i][6]-JES[S_i][6])/JES[S_i][6];
   JES2[S_i][6]= (JES[S_i][6]-JES2[S_i][6])/JES[S_i][6]; 
   
   JES1[S_i][1]= sqrt(JES1[S_i][1]*JES1[S_i][1]*10000 + 55)/100 ;
   JES2[S_i][1]= sqrt(JES2[S_i][1]*JES2[S_i][1]*10000 + 55)/100 ;
   
   JES1[S_i][2]= 0.5 ;
   JES2[S_i][2]= 0.5 ;
   
   
   
  
  fprintf(Ptr,"%d %d %d %f %f %f %f %f %f %f %f %f %f %f %f \n", 1, CUT_MWR[S_i], CUT_MNu[S_i], JES1[S_i][1], JES2[S_i][1], JES1[S_i][2], JES2[S_i][2], JES1[S_i][3], JES2[S_i][3], JES1[S_i][4], JES2[S_i][4], JES1[S_i][5], JES2[S_i][5], JES1[S_i][6], JES2[S_i][6]); 
  } 

  fclose(Ptr); 
  
  cout << "Sysematics done successfully" << endl;
}
