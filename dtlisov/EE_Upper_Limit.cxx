//root -b -q -l EE_Upper_Limit.cxx+
#include "TMath.h"
#include "TF1.h"
#include "TGraph.h"
#include "TGraphErrors.h"
#include "TMultiGraph.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TStyle.h"
#include "TLegend.h"
#include "TText.h"
#include "TAxis.h"

#include "tdrstyle.C"
#include "heavynuStandard.C"



#include "../PATDump/src/format.h"
#include "../sampledb.h"
#include <iostream>
#include <fstream>
using namespace std;

void EE_Upper_Limit()
{
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Inizialization


  unsigned int NofLept = 2.;
  unsigned int NofJets = 2.;
  float Jet1Pt_cut = 40.;
  float Jet2Pt_cut = 40.;  
  float Eta1J_cut = 2.5;
  float Eta2J_cut = 2.5; 
  float ISO_cut = 1.;
  float Vertex_cut = 0.03;
  
  FILE *Ptr;
  Ptr = fopen("Data_CS_Up_Lim.txt","w");
  
  int INFORM = 1;
      

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Reading CUTS

  // finding signal samples in sample.db
  const SampleDB db;
  const SampleDB sa = db.find("MW", 1000)/*.find("MNu", 500)*/.find("type", 1).find("channel", 1).find("AlCa", "START38_V14::All").find("CMSSW", "3_8_7");

  float S_CS[sa.size()];
  float S_CS_Error[sa.size()];
  float S_Eff[sa.size()]; // Signal efficiency
  float delta_S_Eff[sa.size()]; // Error for Signal efficiency
  float BG_nw[sa.size()]; // number of BG events passed cuts
  float delta_BG_nw[sa.size()]; // Relative error for number of total BG events
  int   RD_n[sa.size()];//number of data passed cuts;
  float X[sa.size()];
  float X_ERR[sa.size()];
  
  //cuts
  float Mll_Cut[sa.size()];
  float MW_Cut[sa.size()];
  float MN1_Cut[sa.size()];
  float MN2_Cut[sa.size()];
  float Lep1Pt_Cut[sa.size()];
  float Lep2Pt_Cut[sa.size()];
  float Eta1L_Cut[sa.size()];
  float Eta2L_Cut[sa.size()];
  
  int CUT_Chn = 0;	
  int CUT_MWR = 0;
  int CUT_MNu = 0;
  float AUX;

  for (int S_i = 0; S_i < sa.size(); ++S_i) {
   S_CS[S_i] = sa[S_i].CS;
   S_CS_Error[S_i] = 0.10;
   RD_n[S_i] = 0;   
  }
 
  string CUT_fname = "/afs/cern.ch/user/d/dtlisov/CMSSW_3_8_7/src/heavynu/examples/OptimumCuts_E.txt";
  //string CUT_fname = "/afs/cern.ch/user/d/dtlisov/CMSSW_3_8_7/src/heavynu/examples/OptimumCuts_Mu.txt";
  char str[500] = {" "};
  ifstream inFile(CUT_fname.c_str()); 
  inFile.getline(str, 500);
  
  
  string CUT_fname1 = "/afs/cern.ch/user/d/dtlisov/CMSSW_3_8_7/src/heavynu/examples/Systematics_ALL_E.txt";
  ifstream inFile1(CUT_fname1.c_str()); 
  inFile1.getline(str, 500);
  
  
  
  
  while (!inFile.eof()) {
   inFile >> CUT_Chn;
   inFile >> CUT_MWR;
   inFile >> CUT_MNu;

   if (INFORM == 1) { 
 //   cout << "Chn = "<< CUT_Chn <<" MWR = " << CUT_MWR << " MNu = "<< CUT_MNu <<endl;
   }
	for (int S_i = 0; S_i < sa.size(); ++S_i) {      
	  if((sa[S_i].MW == CUT_MWR) && (sa[S_i].MNu == CUT_MNu)) {	  
	    inFile >> Lep1Pt_Cut[S_i];
        inFile >> Lep2Pt_Cut[S_i];
		inFile >> Eta1L_Cut[S_i];
        inFile >> Eta2L_Cut[S_i];	
	    inFile >> Mll_Cut[S_i];
        inFile >> MW_Cut[S_i];
        inFile >> MN1_Cut[S_i];
        inFile >> MN2_Cut[S_i];
		inFile >> AUX;
		inFile >> AUX;
		inFile >> S_Eff[S_i];
        inFile >> BG_nw[S_i];		
        		
	  }	  
	}
	inFile.getline(str, 500);
	
  }
  
    while (!inFile1.eof()) {
   inFile1 >> CUT_Chn;
   inFile1 >> CUT_MWR;
   inFile1 >> CUT_MNu;

   if (INFORM == 1) { 
//    cout << "Chn1 = "<< CUT_Chn <<" MWR1 = " << CUT_MWR << " MNu1 = "<< CUT_MNu <<endl;
   }
	for (int S_i = 0; S_i < sa.size(); ++S_i) {      
	  if((sa[S_i].MW == CUT_MWR) && (sa[S_i].MNu == CUT_MNu)) {	  
		inFile1 >> delta_S_Eff[S_i];
		inFile1 >> AUX;
		inFile1 >> delta_BG_nw[S_i];
	  }	  
	}
	inFile1.getline(str, 500);
	
  }

  if (INFORM == 1) {   
   for (int S_i = 0; S_i < sa.size(); ++S_i) {
    cout << sa[S_i].MW << "  " << sa[S_i].MNu << " BG_nw[S_i]= " <<S_Eff[S_i]  << " BG_nw[S_i]= " << BG_nw[S_i]<< " delta_BG_nw[S_i]= " <<delta_S_Eff[S_i]  << " delta_BG_nw[S_i]= " << delta_BG_nw[S_i] <<endl;
    cout << endl;
   }
  } 
  
  
  
  
  
  
  
 //+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// Reading DATA 
  
 
 /*
 
 // string RD_fname = "/moscow31/heavynu/data/2010A_EG_2010B_Electron_Nov4_ReReco.root"; 
  string RD_fname = "/moscow31/heavynu/data/Electron_Run2010A_Run2010B_Dec22ReReco.root";    

  TheEvent RD_e(RD_fname.c_str());
  if (!RD_e) return;
  
  float RD_Lum = RD_e.sample.lumi; //Luminocity in pb^-1
  float delta_Lum = 0.11 ;// Error for Lumi
  float RD_WR = 0.;
  float RD_NuR_l0 = 0.;
  float RD_NuR_l1 = 0.;
  float K1 = 0.0;
  float K2 = 0.0;
  
  int RD_nEvents = RD_e.totalEvents();
  
for (int S_i = 0; S_i < 1; ++S_i) { 

  for (int i = 0; i < RD_nEvents; ++i) {
   RD_e.readEvent(i);
   // Cuts on number of leptons and jets in event
   if ((RD_e.electrons.size() >= NofLept) && (RD_e.jets.size() >= NofJets)) {
	// correction of Z mass
	//if ((abs(RD_e.electrons[0].p4.Eta())<=1.5)) {K1 = 90.2/89.6;} else {K1=90.2/85.9;}
	//if ((abs(RD_e.electrons[1].p4.Eta())<=1.5)) {K2 = 90.2/89.6;} else {K2=90.2/85.9;}
	//cuts on electrons Pt
	if (((K1*RD_e.electrons[0].p4).Pt()>=Lep1Pt_Cut[S_i]) && ((K2*RD_e.electrons[1].p4).Pt()>=Lep2Pt_Cut[S_i]))  { 
	 //cuts on jets Pt      
	 if (((RD_e.jets[0].p4).Pt()>=Jet1Pt_cut) && ((RD_e.jets[1].p4).Pt()>=Jet2Pt_cut))  { 
	  //cuts on Eta  
      if (((abs(K1*RD_e.electrons[0].p4.Eta())<=Eta1L_Cut[S_i]) || (abs(K2*RD_e.electrons[1].p4.Eta())<=Eta1L_Cut[S_i])) 
	  && (abs(RD_e.jets[0].p4.Eta())<=Eta1J_cut) && (abs(RD_e.jets[1].p4.Eta())<=Eta2J_cut) )        {
	   // Isolation cut
	   if ((RD_e.electrons[0].isoHEEP() <= ISO_cut)&&(RD_e.electrons[0].isoHEEP() <= ISO_cut)) {
		//Cut on vertexes
        if ((abs(RD_e.electrons[0].vertex.z()-RD_e.electrons[1].vertex.z())<=Vertex_cut) && (abs(RD_e.jets[0].vertex.z()-RD_e.jets[1].vertex.z())<=Vertex_cut) 
        && (abs(abs(RD_e.electrons[0].vertex.z()-RD_e.electrons[1].vertex.z())-abs(RD_e.jets[0].vertex.z()-RD_e.jets[1].vertex.z()))<=Vertex_cut)) {
	     //cuts on electrons invariant mass Mll	
	     if ((K1*RD_e.electrons[0].p4 + K2*RD_e.electrons[1].p4).M()>=Mll_Cut[S_i]) {	
	      //cut on  MW	
          RD_WR = (K1*RD_e.electrons[0].p4 + K2*RD_e.electrons[1].p4 + RD_e.jets[0].p4 + RD_e.jets[1].p4).M();		  
          if (RD_WR>=MW_Cut[S_i]) {
           //cuts on  Mnu
		   RD_NuR_l0 = (K1*RD_e.electrons[0].p4 + RD_e.jets[0].p4 + RD_e.jets[1].p4).M();
           RD_NuR_l1 = (K2*RD_e.electrons[1].p4 + RD_e.jets[0].p4 + RD_e.jets[1].p4).M();
	        if (RD_NuR_l0>=MN1_Cut[S_i]) {
	        if (RD_NuR_l1>=MN2_Cut[S_i]) {	
	        RD_n[S_i] = RD_n[S_i] + 1;	
		    }	
		   }	
          } 	
         }
	    }
	   }
	  }    
     }
	}
   }
   

  }

  if (INFORM == 1) {   
   cout << endl; 
   cout << "REAL DATA" <<" S_i = "<< S_i<<  endl;
   cout << endl;
   cout << "Total events = " << RD_nEvents << endl;
   cout << endl; 
   cout << "Number of passed events = " <<RD_n[S_i]<< endl;
   cout << endl;
  }
}

*/

float RD_Lum = 35.165; //Luminocity in pb^-1
float delta_Lum = 0.11 ;// Error for Lumi

/*
  for (int S_i = 1; S_i < sa.size(); ++S_i) {
   RD_n[S_i] = RD_n[0];
  }
*/ 
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//Upper limit calculation 

  char *str1 = (char*)alloca(10000);
 



float L_sb = 0.0; 
float L_sb_shtrih = 0.0;
float L_sb_shtrih_shtrih = 0.0;
float L_sb_shtrih_shtrih_shtrih = 0.0;
float Log_L_sb = 0.0;
float Log_L_sb_shtrih = 0.0;
float Log_L_sb_shtrih_shtrih = 0.0;
float Log_L_sb_shtrih_shtrih_shtrih = 0.0;
float sigma = 0.0;
float Log_sigma = 0.0;
float BG = 0.0;
float Log_BG = 0.0;
float Eff = 0.0;
float Lum = 0.0;




float Log_Int = 0.;
float Log_Int_N = 0.;
float s = 0.0;
float Log_N = 100.;
int Log_MAX_INT = 1000;

float x[6], y[6];
float CS_DATA_Up_Lim[sa.size()];
float ex_sigma_mean[sa.size()];
float ex_sigma_sigma[sa.size()];
float ex_sigma_sigma2[sa.size()];
float Log_x[6], Log_y[6];
//float Log_CS_DATA_Up_Lim[sa.size()];
float Log_ex_sigma_mean[sa.size()];
float Log_ex_sigma_sigma[sa.size()];
float Log_ex_sigma_sigma2[sa.size()];

float ex_nf;


gROOT->SetStyle("Plain");

TCanvas c("Expected CS distribution");
c.Print("Limit_Distr.ps[");

//experimental

for (int S_i = 0; S_i < sa.size(); ++S_i) {

float Log_bg_min = log(BG_nw[S_i]*(1-delta_BG_nw[S_i]));
float Log_bg_max = log(BG_nw[S_i]*(1+delta_BG_nw[S_i]));
float Log_bg_D = Log_bg_max-Log_bg_min; 
 
// LOGNORMAL
 
Log_sigma = 0.0;
Log_Int_N = 0.0;
for ( int i_s = 0; i_s < Log_MAX_INT; ++i_s) {
 Log_BG = 0.;
 Log_L_sb = 0.;
 for ( int i_s1 = 0; i_s1 < 51; ++i_s1) {
  Log_L_sb_shtrih = 0.0;
  for ( int i_Eff = 0; i_Eff < 20; ++i_Eff) {
   Log_L_sb_shtrih_shtrih = 0.0;
   for ( int i_Lum = 25; i_Lum < 46; ++i_Lum) {
    Log_L_sb_shtrih_shtrih_shtrih = exp(Log_BG)*TMath::Poisson(RD_n[S_i], Log_sigma*Eff*Lum + exp(Log_BG))*TMath::Gaus(Eff, S_Eff[S_i], delta_S_Eff[S_i]*S_Eff[S_i],kTRUE)*TMath::Gaus(Lum,RD_Lum,delta_Lum*RD_Lum,kTRUE)*TMath::Gaus(Log_BG,log(BG_nw[S_i]),delta_BG_nw[S_i]*BG_nw[S_i],kTRUE);
    Log_L_sb_shtrih_shtrih = Log_L_sb_shtrih_shtrih + Log_L_sb_shtrih_shtrih_shtrih; 
    Lum = (float) i_Lum/1.;
   }
   Log_L_sb_shtrih = Log_L_sb_shtrih + Log_L_sb_shtrih_shtrih; 
   Eff = (float) i_Eff/20.;
  }
  Log_BG = Log_bg_min + Log_bg_D*i_s1/50.;
  Log_L_sb = Log_L_sb + Log_L_sb_shtrih; 
 }
 Log_Int_N = Log_Int_N + Log_L_sb; 
 Log_sigma = Log_sigma + 1/Log_N ;
}

  if (INFORM == 1) {     
    cout << sa[S_i].MW << "  " << sa[S_i].MNu <<endl;
	cout << "Log_Int_N = " << Log_Int_N << " Log_Sigma = " << Log_sigma <<endl;	
    cout << endl;  
  }   
 
Log_sigma = 0.0;
Log_Int = 0.0;
while (Log_Int < 0.95) {
 Log_BG = 0.;
 Log_L_sb = 0.;
 for ( int i_s1 = 0; i_s1 < 51; ++i_s1) {
  Log_L_sb_shtrih = 0.0;
  for ( int i_Eff = 0; i_Eff < 20; ++i_Eff) {
   Log_L_sb_shtrih_shtrih = 0.0;
   for ( int i_Lum = 25; i_Lum < 46; ++i_Lum) {
    Log_L_sb_shtrih_shtrih_shtrih = exp(Log_BG)*TMath::Poisson(RD_n[S_i], Log_sigma*Eff*Lum + exp(Log_BG))*TMath::Gaus(Eff,S_Eff[S_i],delta_S_Eff[S_i]*S_Eff[S_i],kTRUE)*TMath::Gaus(Lum,RD_Lum,delta_Lum*RD_Lum,kTRUE)*TMath::Gaus(Log_BG,log(BG_nw[S_i]),delta_BG_nw[S_i]*BG_nw[S_i],kTRUE);
    Log_L_sb_shtrih_shtrih = Log_L_sb_shtrih_shtrih + Log_L_sb_shtrih_shtrih_shtrih; 
    Lum = (float) i_Lum/1.;
   }
   Log_L_sb_shtrih = Log_L_sb_shtrih + Log_L_sb_shtrih_shtrih; 
   Eff = (float) i_Eff/20.;
  }
  Log_BG = Log_bg_min + Log_bg_D*i_s1/50.;
  Log_L_sb = Log_L_sb + Log_L_sb_shtrih; 
 }
 Log_Int = Log_Int + Log_L_sb/Log_Int_N; 
 Log_sigma = Log_sigma + 1/Log_N ;
}

CS_DATA_Up_Lim[S_i] = Log_sigma; 
//fprintf(Ptr,"%d %d %f\n",sa[S_i].MW,sa[S_i].MNu,CS_DATA_Up_Lim[S_i] );

  if (INFORM == 1) {     
    cout << sa[S_i].MW << "  " << sa[S_i].MNu <<endl;
    cout << "CL = " << Log_Int <<" RD_n = "<< RD_n[S_i] <<" LOGNORMAL  Byesian upper signal CS limit = " << CS_DATA_Up_Lim[S_i] <<endl;
    cout << endl;  
  }   

//expected

ex_sigma_mean[S_i] = 0.0;



for (int ex_n = 0; ex_n <= 5; ++ex_n) {

ex_nf = ex_n;

// LOGNORMAL

Log_sigma = 0.0;
Log_Int_N = 0.0;
for ( int i_s = 0; i_s < Log_MAX_INT; ++i_s) {
 Log_BG = 0.;
 Log_L_sb = 0.;
 for ( int i_s1 = 0; i_s1 < 51; ++i_s1) {
  Log_L_sb_shtrih = 0.0;
  for ( int i_Eff = 0; i_Eff < 20; ++i_Eff) {
   Log_L_sb_shtrih_shtrih = 0.0;
   for ( int i_Lum = 25; i_Lum < 46; ++i_Lum) {
    Log_L_sb_shtrih_shtrih_shtrih = exp(Log_BG)*TMath::Poisson(ex_n, Log_sigma*Eff*Lum + exp(Log_BG))*TMath::Gaus(Eff, S_Eff[S_i], delta_S_Eff[S_i]*S_Eff[S_i],kTRUE)*TMath::Gaus(Lum,RD_Lum,delta_Lum*RD_Lum,kTRUE)*TMath::Gaus(Log_BG,log(BG_nw[S_i]),delta_BG_nw[S_i]*BG_nw[S_i],kTRUE);
    Log_L_sb_shtrih_shtrih = Log_L_sb_shtrih_shtrih + Log_L_sb_shtrih_shtrih_shtrih; 
    Lum = (float) i_Lum/1.;
   }
   Log_L_sb_shtrih = Log_L_sb_shtrih + Log_L_sb_shtrih_shtrih; 
   Eff = (float) i_Eff/20.;
  }
  Log_BG = Log_bg_min + Log_bg_D*i_s1/50.;
  Log_L_sb = Log_L_sb + Log_L_sb_shtrih; 
 }
 Log_Int_N = Log_Int_N + Log_L_sb; 
 Log_sigma = Log_sigma + 1/Log_N ;
}
  if (INFORM == 1) {     
    cout << sa[S_i].MW << "  " << sa[S_i].MNu <<endl;
	cout << "Log_Int_N = " << Log_Int_N << " Log_Sigma = " << Log_sigma <<endl;	
    cout << endl;  
  }  
 
Log_sigma = 0.0;
Log_Int = 0.0;
while (Log_Int < 0.95) {
 Log_BG = 0.;
 Log_L_sb = 0.;
 for ( int i_s1 = 0; i_s1 < 51; ++i_s1) {
  Log_L_sb_shtrih = 0.0;
  for ( int i_Eff = 0; i_Eff < 20; ++i_Eff) {
   Log_L_sb_shtrih_shtrih = 0.0;
   for ( int i_Lum = 25; i_Lum < 46; ++i_Lum) {
    Log_L_sb_shtrih_shtrih_shtrih = exp(Log_BG)*TMath::Poisson(ex_n, Log_sigma*Eff*Lum + exp(Log_BG))*TMath::Gaus(Eff,S_Eff[S_i],delta_S_Eff[S_i]*S_Eff[S_i],kTRUE)*TMath::Gaus(Lum,RD_Lum,delta_Lum*RD_Lum,kTRUE)*TMath::Gaus(Log_BG,log(BG_nw[S_i]),delta_BG_nw[S_i]*BG_nw[S_i],kTRUE);
    Log_L_sb_shtrih_shtrih = Log_L_sb_shtrih_shtrih + Log_L_sb_shtrih_shtrih_shtrih; 
    Lum = (float) i_Lum/1.;
   }
   Log_L_sb_shtrih = Log_L_sb_shtrih + Log_L_sb_shtrih_shtrih; 
   Eff = (float) i_Eff/20.;
  }
  Log_BG = Log_bg_min + Log_bg_D*i_s1/50.;
  Log_L_sb = Log_L_sb + Log_L_sb_shtrih; 
 }
 Log_Int = Log_Int + Log_L_sb/Log_Int_N; 
 Log_sigma = Log_sigma + 1/Log_N ;
}

x[ex_n] = Log_sigma;
y[ex_n] = TMath::Poisson(ex_n, BG_nw[S_i]);
ex_sigma_mean[S_i] = ex_sigma_mean[S_i] + Log_sigma*y[ex_n];

  if (INFORM == 1) {     
    cout << sa[S_i].MW << "  " << sa[S_i].MNu <<endl;
    cout <<"ex_n = "<< ex_nf <<" CL = " << Log_Int <<"  LOG Byesian upper number of signal events limit = " << x[ex_n] <<" prob = "<<y[ex_n]<<endl;
    cout << endl;  
  } 



  
}


TGraph h_ex_s( 6, x, y);
h_ex_s.SetMarkerStyle(21);
h_ex_s.SetMarkerSize(3);
h_ex_s.SetMarkerColor(1); 
sprintf(str1,"Expected CS distribution for M_{W} = %d GeV, M_{Nu} = %d GeV", sa[S_i].MW, sa[S_i].MNu);
h_ex_s.SetTitle(str1);
h_ex_s.Fit("gaus");
TF1* g = h_ex_s.GetFunction("gaus");
ex_sigma_sigma[S_i] = g->GetParameter(2);
h_ex_s.Draw("AP"); c.Print("Limit_Distr.ps");

//ex_sigma_sigma[S_i] = 0.11;


  if (INFORM == 1) {     
    cout << sa[S_i].MW << "  " << sa[S_i].MNu <<endl;
    cout << "  Expected upper CS limit = " << ex_sigma_mean[S_i] <<endl;
	cout << "  Expected delta = " << ex_sigma_sigma[S_i] <<endl;
    cout << endl;  
  } 

  
//fprintf(Ptr,"%d %d %f %f %f %f %f \n",sa[S_i].MW,sa[S_i].MNu,CS_DATA_Up_Lim[S_i], ex_sigma_mean[S_i], ex_sigma_sigma[S_i],S_CS[S_i], S_CS_Error[S_i]*S_CS[S_i]);  
  
fprintf(Ptr,"%d %d %f  \n",sa[S_i].MW,sa[S_i].MNu,CS_DATA_Up_Lim[S_i]);  
 
}

c.Print("Limit_Distr.ps]");

setTDRStyle();
fixOverlay();

TCanvas c1("CS upper limits","CS upper limits");


//c1.SetMargin(0.15, 0.05, 0.15, 0.05);
//c1.SetTitleOffset("Y", 0.1);


TLegend leg(0.5, 0.65, 0.94, 0.94);
leg.SetTextFont(42);
leg.SetTextSize(0.034);

//c1.Print("CS_Upper_Limits.ps[");



TMultiGraph mg;// = new TMultiGraph();



  for (int S_i = 0; S_i < sa.size(); ++S_i) {
   S_CS_Error[S_i] = S_CS_Error[S_i]*S_CS[S_i];
   X[S_i] = sa[S_i].MNu;
   X_ERR[S_i] = 0.0;
   ex_sigma_sigma2[S_i]=ex_sigma_sigma[S_i]*2.;
  
   
  }
 

   
TGraphErrors h_EX_UP_LIM_CS2( sa.size(), X, ex_sigma_mean, X_ERR, ex_sigma_sigma2);

   h_EX_UP_LIM_CS2.SetFillColor(kYellow);
 //  h_EX_UP_LIM_CS2.SetLineColor(kYellow); 
//   mg.Add(&h_EX_UP_LIM_CS2,"3");
   
TGraphErrors h_EX_UP_LIM_CS1( sa.size(), X, ex_sigma_mean, X_ERR, ex_sigma_sigma);

   h_EX_UP_LIM_CS1.SetFillColor(kYellow); 
//   h_EX_UP_LIM_CS1.SetLineColor(kBlue);

   mg.Add(&h_EX_UP_LIM_CS1,"3");
   
TGraphErrors h_S_CS( sa.size(), X, S_CS, X_ERR, S_CS_Error);

   h_S_CS.SetFillColor(kGreen);
//   h_S_CS.SetLineColor(kGreen);
   mg.Add(&h_S_CS,"3");
   
TGraph h_EX_UP_LIM_CS( sa.size(), X, ex_sigma_mean); 

   h_EX_UP_LIM_CS.SetLineWidth(3);
//   h_EX_UP_LIM_CS.SetLineColor(kRed); 
   h_EX_UP_LIM_CS.SetLineStyle(2);
//   h_EX_UP_LIM_CS.SetFillColor(kWhite);   
   mg.Add(&h_EX_UP_LIM_CS,"L");
   
   
TGraph h_S_CS1( sa.size(), X, S_CS);
   h_S_CS1.SetLineWidth(3);
   h_S_CS1.SetLineColor(kRed);
//   h_S_CS1.SetLineStyle(4);
//   h_S_CS1.SetFillColor(kWhite);   
   mg.Add(&h_S_CS1,"C");  
     
TGraph h_DATA_UP_LIM_CS( sa.size(), X, CS_DATA_Up_Lim); 
  
   h_DATA_UP_LIM_CS.SetMarkerStyle(21);
   h_DATA_UP_LIM_CS.SetMarkerSize(2);
   h_DATA_UP_LIM_CS.SetMarkerColor(1); 
   h_DATA_UP_LIM_CS.SetLineWidth(3);
   h_DATA_UP_LIM_CS.SetLineColor(1);
   h_DATA_UP_LIM_CS.SetLineStyle(1);
     
   mg.Add(&h_DATA_UP_LIM_CS,"LP"); 
   
   sprintf(str1,"Byesian #sigma upper limit; M_{N}, GeV; #sigma(pp #rightarrow W_{R})#times Br(W_{R} #rightarrow eejj), pb", sa[1].MW);
  // sprintf(str1,"Byesian #sigma upper limit; M_{N}, GeV; #sigma(pp #rightarrow W_{R})#times Br(W_{R} #rightarrow #mu#mujj), pb", sa[1].MW);
 
   
   mg.SetTitle(str1);
   //mg->SetMinimum(0);
   //mg->SetMaximum(1);
  /* 
   // watermark
   TText* mark = new TText(0.5, 0.5, "CMS preliminary");
   mark->SetNDC(kTRUE);
   mark->SetTextColor(17);
   mark->SetTextAlign(22); // centered
   mark->SetTextSize(0.10);
   mark->SetTextAngle(45);
   mark->Draw();
   */

   mg.Draw("A"); 
   mg.GetXaxis()->SetLimits(0, sa[1].MW);
   mg.GetYaxis()->SetRangeUser(0, 1.); 
  
   c1.Modified();

   
   label_Lumi(0.72, 0.55, 36.1);
   label_Prelim(0.72, 0.61);
   

  
   sprintf(str1,"M_{W} = %d GeV, L = 36 pb^{-1}", sa[1].MW);
   leg.SetHeader(str1);
   //leg.SetHeader("Muon-Muon channel");
   leg.AddEntry(&h_DATA_UP_LIM_CS, "95% C.L. Observed Limit", "LP");
   leg.AddEntry(&h_EX_UP_LIM_CS, "95% C.L. Expected Limit", "L");
   leg.AddEntry(&h_EX_UP_LIM_CS1,"#pm1#sigma Expected Limit", "F");
   //leg.AddEntry(&h_EX_UP_LIM_CS2,"2#sigma for expected upper limit", "F");
   leg.AddEntry(&h_S_CS1,"NNLO Signal Xsection", "L");
   leg.AddEntry(&h_S_CS,"#pm1#sigma PDF&scale unc.", "F");
   leg.Draw();
   //c1.Print("CS_Upper_Limits.ps"); 
   
   c1.Print("CS_Upper_Limits.pdf");
   c1.Print("CS_Upper_Limits.png");
   c1.Print("CS_Upper_Limits.cxx");
   
//c1.Print("CS_Upper_Limits.ps]");


  fclose(Ptr);   
}

