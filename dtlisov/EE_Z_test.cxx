//To run this script on pcrfcms14.cern.ch machine: 
//root -b -q -l EE_Z_test.cxx+
#include "TH1.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TROOT.h"
#include <TLorentzVector.h>
#include <TMath.h>
#include <TRandom3.h>
#include <TLatex.h>
#include "TStyle.h"
#include "TLegend.h"
#include "TText.h"
#include "TAxis.h"
#include "TGraph.h"
#include "../../PhysicsTools/Utilities/interface/LumiReweightingStandAlone.h"

#include "tdrstyle.C"
//#include "heavynuStandard.C"
#include "heavynuStandard11.C"

#include "../PATDump/src/format.h"
#include <iostream>
#include <fstream>
using namespace std;

vector<double> generate_flat10_weights(TH1D* data_npu_estimated){
   // see SimGeneral/MixingModule/python/mix_E7TeV_FlatDist10_2011EarlyData_inTimeOnly_cfi.py; copy and paste from there:
// for spring11 and summer11, if out of time is not important:
//   const double npu_probs[25] = {0.0698146584, 0.0698146584, 0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,
//          0.0630151648,0.0526654164,0.0402754482,0.0292988928,0.0194384503,0.0122016783,0.007207042,0.004003637,0.0020278322,
//          0.0010739954,0.0004595759,0.0002229748,0.0001028162,4.58337152809607E-05 /* <-- 24 */};
// for summer11 PU_S4, obtained by averaging:
   const double npu_probs[25] = {0.104109, 0.0703573, 0.0698445, 0.0698254, 0.0697054, 0.0697907, 0.0696751, 0.0694486, 0.0680332,
                                 0.0651044, 0.0598036, 0.0527395, 0.0439513, 0.0352202, 0.0266714, 0.019411, 0.0133974, 0.00898536,
                                 0.0057516, 0.00351493, 0.00212087, 0.00122891, 0.00070592, 0.000384744, 0.000219377};
   vector<double> result(25);
   double s = 0.0;
   for(int npu=0; npu<25; ++npu){
       double npu_estimated = data_npu_estimated->GetBinContent(data_npu_estimated->GetXaxis()->FindBin(npu));                              
       result[npu] = npu_estimated / npu_probs[npu];
       s += npu_estimated;
   }
   // normalize weights such that the total sum of weights over thw whole sample is 1.0, i.e., sum_i  result[i] * npu_probs[i] should be 1.0 (!)
   for(int npu=0; npu<25; ++npu){
       result[npu] /= s;
   }
   return result;
}

void EE_Z_test()
{
// set plain style
  gROOT->SetStyle("Plain");
  TCanvas c1("OUTPUT1","OUTPUT1");
  setTDRStyle();
  fixOverlay(); 
 
  TH1::SetDefaultSumw2(kTRUE);
  
//  TFile* f_A1 = new TFile("PileUp/2011A_pileup.root");
//  TFile* f_B1 = new TFile("PileUp/2011B_pileup.root");
  TFile* f_A1 = new TFile("PileUp/PURunA.root");
  TFile* f_B1 = new TFile("PileUp/PURunB.root"); 
  TH1D* histo_A1 = (TH1D*) f_A1->Get("pileup");
  TH1D* histo_B1 = (TH1D*) f_B1->Get("pileup");
 
 /* 
  TFile* f_A = new TFile("PileUp/2011A_pileupTruth.root");
  TFile* f_B = new TFile("PileUp/2011B_pileupTruth.root");
  TH1D* histo_A = (TH1D*) f_A->Get("pileup");
  TH1D* histo_B = (TH1D*) f_B->Get("pileup");
     
  Double_t PoissonOneXDist[35] = {
    1.45346E-01,
    6.42802E-02,
    6.95255E-02,
    6.96747E-02,
    6.92955E-02,
    6.84997E-02,
    6.69528E-02,
    6.45515E-02,
    6.09865E-02,
    5.63323E-02,
    5.07322E-02,
    4.44681E-02,
    3.79205E-02,
    3.15131E-02,
    2.54220E-02,
    2.00184E-02,
    1.53776E-02,
    1.15387E-02,
    8.47608E-03,
    6.08715E-03,
    4.28255E-03,
    2.97185E-03,
    2.01918E-03,
    1.34490E-03,
    8.81587E-04,
    5.69954E-04,
    3.61493E-04,
    2.28692E-04,
    1.40791E-04,
    8.44606E-05,
    5.10204E-05,
    3.07802E-05,
    1.81401E-05,
    1.00201E-05,
    5.80004E-06
  };
  
  std::vector< float > DataPileUp_A ;
  std::vector< float > DataPileUp_B ;
  std::vector< float > MCPileUp ;
  for( int i=0; i<35; ++i) {
      DataPileUp_A.push_back(histo_A->GetBinContent(i+1));
      DataPileUp_B.push_back(histo_B->GetBinContent(i+1));
      MCPileUp.push_back(PoissonOneXDist[i]);
    }
    
  reweight::LumiReWeighting LumiWeights_A = reweight::LumiReWeighting(MCPileUp, DataPileUp_A); 
  LumiWeights_A.weight3D_init("Weight3D_2011A.root");   
  reweight::LumiReWeighting LumiWeights_B = reweight::LumiReWeighting(MCPileUp, DataPileUp_B);  
  LumiWeights_B.weight3D_init("Weight3D_2011B.root"); 
  Double_t Corr[35] = { 
    0.,
    0.887809,
    0.889615,
    0.902631,
    0.884461,
    0.88004,
    0.865174,
    0.870073,
    0.873842,
    0.8978,
    0.92261,
    0.988201,
    1.05329,
    1.13407,
    1.2587,
    1.39528,
    1.55154,
    1.77817,
    2.02186,
    2.43142,
    2.83008,
    3.47422,
    4.6109,
    5.55045,
    5.88748,
    7.18155,
    10.9544,
    12.0279,
    13.5533,
    11.3418,
    28.946,
    14.0245,
    35.8912,
    9.13846,
    10.4667
    };

*/
// Cuts for event selection
  unsigned int NofLept = 2;
  unsigned int NofJets = 0;
  float Lep1Pt = 40.; 
  float Lep2Pt = 40.;
  float JetPt = 40.;
  float EtaCut1L = 1.44; //1.44
  float EtaCut2L = EtaCut1L;
  int HeepId = 0;
  float ISO = 1.;
  float Vertex = 100.03;
  float MllCut =  70.0;
  float MllCut1 = 110.0;

// Mll histogramm parameters  
  int M_Delta_Mll = 40;
  double M_min_Mll = 70;
  double M_max_Mll = 110;
  TRandom3 r;

  float Mll; 
  float HEEP_A_B=0.983;//0.996;//0.973;//0.972; 0.98;
  float HEEP_A_E=0.997;//0.987;//0.857;//0.963;//0.982; 0.949
  float HEEP_B_B=0.949;//0.977;//0.973;//0.972; 0.98;
  float HEEP_B_E=0.981;//0.854;//0.963;//0.982; 0.949  
  float A_Wei=1.;
  float B_Wei=1.;
  float Z_tune = 1.0;
  float proc_A_B = 0.016;
  float proc_B_B = 0.016;
  float proc_A_E = 0.05;
  float proc_B_E = 0.035;

  char *str = (char*)alloca(10000);
  char *str1 = (char*)alloca(10000);

  TH1F* hMll_A[13];  
  TH1F* hMll_B[13]; 
  TH1F* hPU_A[13];
  TH1F* hPU_B[13];
  
  for (int i = 0; i <= 12; ++i) {  
    sprintf(str,"Mll_A_%d", i);
    sprintf(str1,"Mll_A; M(ee), GeV;Events/%5.1f GeV", (M_max_Mll-M_min_Mll)/M_Delta_Mll );
    hMll_A[i] = new TH1F(str,str1, M_Delta_Mll, M_min_Mll, M_max_Mll); 
    sprintf(str,"Mll_B_%d", i);
    sprintf(str1,"Mll_B; M(ee), GeV;Events/%5.1f GeV", (M_max_Mll-M_min_Mll)/M_Delta_Mll );
    hMll_B[i] = new TH1F(str,str1, M_Delta_Mll, M_min_Mll, M_max_Mll);   
     
    sprintf(str,"PU_A_%d", i);
    hPU_A[i] = new TH1F(str, "PU_A; Primary vertex number ; Events", 35, 0, 35);
    sprintf(str,"PU_B_%d", i);
    hPU_B[i] = new TH1F(str, "PU_B; Primary vertex number ; Events", 35, 0, 35);
  }
  
  const char* fname[13];
  float Lum[13] = {0};
  int n[13] = {0};
  int nEvents[13] = {0};
  float Weight_A, Weight_B;
  TLorentzVector EPT0_A, EPT1_A, EPT0_B, EPT1_B;


//  fname[0] = "/moscow41/heavynu/Data_2011/Electron_2011A.root";
//  fname[1] = "/moscow41/heavynu/Data_2011/Electron_B_v1.root";
  fname[0] = "/moscow41/heavynu/Data_2011/DElectron_2011A.root";
  fname[1] = "/moscow41/heavynu/Data_2011/DElectron_B_v1.root";    
  fname[2] = "/moscow31/heavynu/42x-bg/DYToLL_M-50_Sherpa_Summer11-PU_S4_START42_V11-v1_AODSIM.root";
  fname[3] = "/moscow31/heavynu/42x-bg/TTTo2L2Nu2B_7TeV-powheg-pythia6_Summer11-PU_S4_START42_V11-v1_AODSIM.root";
  fname[4] = "/moscow31/heavynu/42x-bg/WJetsToLNu_TuneZ2_7TeV-madgraph-tauola_Summer11-PU_S4_START42_V11-v1_AODSIM.root";
  fname[5] = "/moscow31/heavynu/42x-bg/ZZ_TuneZ2_7TeV_pythia6_tauola_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept.root";
  fname[6] = "/moscow31/heavynu/42x-bg/WZ_TuneZ2_7TeV_pythia6_tauola_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept.root";
  fname[7] = "/moscow31/heavynu/42x-bg/WW_TuneZ2_7TeV_pythia6_tauola_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept.root";
  fname[8] = "/moscow31/heavynu/42x-bg/T_TuneZ2_tW-channel-DR_7TeV-powheg-tauola_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept_1jet.root";
  fname[9] = "/moscow31/heavynu/42x-bg/Tbar_TuneZ2_tW-channel-DR_7TeV-powheg-tauola_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept_1jet.root";
  fname[11] = "/moscow31/heavynu/QCDDEA.d";
  fname[12] = "/moscow31/heavynu/QCDDEB.d";



  for (int j = 0; j <= 9; ++j) {
      TheEvent e(fname[j]);
      if (!e) return;
      Lum[j] = e.sample.lumi; //Luminocity in pb^-1
      n[j] = 0;
      nEvents[j] = e.totalEvents();
      int n_loss=0;
      float Prescale;
      int I_T;
      string Name;
      for (int i = 0; i < nEvents[j]; ++i) {
          e.readEvent(i);
	  int flag = 0; 
	  Weight_A = 1.;
	  Weight_B = 1.;
	  vector<Particle>::iterator cur;
          for (cur=e.electrons.begin();cur<e.electrons.end();cur++) {
	       if (!((cur->ID <= HeepId)&&(cur->isoHEEP() <= ISO)))  {e.electrons.erase(cur);} 
	  }
	  if (j>=2){  
	      /*
              Weight_A = Lum[0]/100.* e.weight * e.Gen_weight * LumiWeights_A.weight3D(e.num_interactions(-1),e.num_interactions(0),e.num_interactions(1));
	      //cout<<"A "<< LumiWeights_A.weight3D(e.num_interactions(-1),e.num_interactions(0),e.num_interactions(1)) << "     "<< Weight_A<< endl;
	      Weight_B = Lum[1]/100.* e.weight * e.Gen_weight * LumiWeights_B.weight3D(e.num_interactions(-1),e.num_interactions(0),e.num_interactions(1));
	      if(e.vertices.size()<35){
	         Weight_B = Weight_B * Corr[e.vertices.size()];
	      }
	      */
	      flag = 1;
	      //cout<<"B "<< LumiWeights_B.weight3D(e.num_interactions(-1),e.num_interactions(0),e.num_interactions(1)) << "     "<< Weight_B<< endl;
	      
	      if(e.pileupIT()<25){
                 Weight_A = Lum[0]/100.* e.weight * e.Gen_weight *  generate_flat10_weights(histo_A1)[e.pileupIT()];
		 Weight_B = Lum[1]/100.* e.weight * e.Gen_weight *  generate_flat10_weights(histo_B1)[e.pileupIT()];
              }
              else{ //should not happen as we have a weight for all simulated ZJ_e.pileup.size() multiplicities!
                 Weight_A = 0.;
	         Weight_B = 0.;
              }	
	      
	  }
	  else {
	        Prescale = 1000000;
	        const vector<int>& HLTBits = *e.hltBits;
                const HLTtable& hlt = e.HLT();
		for (size_t i_t = 0; i_t < HLTBits.size(); ++i_t) {
		     string Trigger = "";
	             if ((HLTBits[i_t]==1)&&
		         (i_t<hlt.names.size())) {            
		         //cout << i_t << " \t" << HLTBits[i_t]<< " \t" << hlt.names[i_t] << endl;
		         for (unsigned int h_s = 0; h_s < (hlt.names[i_t].length()-1); ++h_s) {
		              Trigger += hlt.names[i_t][h_s];
		         } 
	             }				 
		     /*  if ((Trigger=="HLT_DoubleEle33_CaloIdL_v")||
		           (Trigger=="HLT_DoubleEle33_v")||
		           (Trigger=="HLT_DoublePhoton33_v")||
		           (Trigger=="HLT_DoubleEle33_CaloIdT_v")||
			   (Trigger=="HLT_DoubleEle33_CaloIdL_CaloIsoT_v")){//||
			   (Trigger=="HLT_Ele32_CaloIdT_CaloIsoT_TrkIdT_TrkIsoT_SC17_v")||
			   (Trigger=="HLT_Ele32_CaloIdT_CaloIsoT_TrkIdT_TrkIsoT_Ele17_v")||
			   (Trigger=="HLT_Ele32_CaloIdL_CaloIsoVL_SC17_v")){ 
		     */    
			   flag=1;
		     //    if (Prescale>hlt.prescales[i_t]) {Prescale=hlt.prescales[i_t]; I_T=i_t; Name=hlt.names[I_T];}							
                     //}						   
	        }
		if ((flag==0)&&(e.electrons.size() >= NofLept) && (e.jets.size() >= NofJets)&&(e.electrons[0].p4.Pt()>=Lep1Pt)&&(e.electrons[1].p4.Pt()>=Lep2Pt)&& 
	          ((e.electrons[0].p4 + e.electrons[1].p4).M()>=MllCut)&&((e.electrons[0].p4 + e.electrons[1].p4).M()<=MllCut1)&&
		  ((abs(e.electrons[0].p4.Eta())<=EtaCut1L)&&(abs(e.electrons[1].p4.Eta())<=EtaCut2L))&&
	           (e.electrons[0].ID <= HeepId)&&(e.electrons[1].ID <= HeepId)&&
		   (e.electrons[0].isoHEEP() <= ISO)&&(e.electrons[1].isoHEEP() <= ISO)) {
		    cout<< "Event "<<i<<" Run "<< e.irun<<" Event No "<< e.ievent<<endl;
		    n_loss+=1;
		    for (size_t i_t = 0; i_t < HLTBits.size(); ++i_t) {
		         if ((HLTBits[i_t]==1)&&
		             (i_t<hlt.names.size())) { 
		              cout <<n_loss <<"  " << i_t << " " << hlt.names[i_t] << " \t"<< hlt.prescales[i_t]<<" \t"<< flag <<endl;
			 }     
		    }	 
		}
          }
          if ((flag == 1)&&(e.electrons.size() >= NofLept) && (e.jets.size() >= NofJets)) {
	       //cout<< "Bit " << I_T<<"    Trigger "<<Name <<"    Prescale "<< Prescale<<endl;
	       /*
	       if (j>=2){
	           if (abs((e.electrons[0].p4).Eta())<=1.44){
		       A_Wei=HEEP_A_B; B_Wei=HEEP_B_B;
		       EPT0_A.SetPxPyPzE(e.electrons[0].p4.Px()*91./91.4, e.electrons[0].p4.Py()*91./91.4, e.electrons[0].p4.Pz()*91./91.4,e.electrons[0].p4.E()*91./91.4);
		       EPT0_B.SetPxPyPzE(e.electrons[0].p4.Px()*91.1/92., e.electrons[0].p4.Py()*91.1/92., e.electrons[0].p4.Pz()*91.1/92.,e.electrons[0].p4.E()*91.1/92.);
		       EPT0_A *= r.Gaus(EPT0_A.E(),proc_A_B*EPT0_A.E())/EPT0_A.E(); 
		       EPT0_B *= r.Gaus(EPT0_B.E(),proc_B_B*EPT0_B.E())/EPT0_B.E();
		   } else {
		       A_Wei=HEEP_A_E; B_Wei=HEEP_B_E; 
		       EPT0_A.SetPxPyPzE(e.electrons[0].p4.Px()*92./92., e.electrons[0].p4.Py()*92./92., e.electrons[0].p4.Pz()*92./92., e.electrons[0].p4.E()*92./92.);  
		       EPT0_B.SetPxPyPzE(e.electrons[0].p4.Px()*93.2/92., e.electrons[0].p4.Py()*93.2/92., e.electrons[0].p4.Pz()*93.2/92., e.electrons[0].p4.E()*93.2/92.);
		       EPT0_A *= r.Gaus(EPT0_A.E(),proc_A_E*EPT0_A.E())/EPT0_A.E(); 
		       EPT0_B *= r.Gaus(EPT0_B.E(),proc_B_E*EPT0_B.E())/EPT0_B.E();
		   }
	           if (abs((e.electrons[1].p4).Eta())<=1.44){
		       A_Wei=A_Wei*HEEP_A_B; B_Wei=B_Wei*HEEP_B_B;
		       EPT1_A.SetPxPyPzE(e.electrons[1].p4.Px()*91./91.4, e.electrons[1].p4.Py()*91./91.4, e.electrons[1].p4.Pz()*91./91.4, e.electrons[1].p4.E()*91./91.4);
		       EPT1_B.SetPxPyPzE(e.electrons[1].p4.Px()*91.1/92., e.electrons[1].p4.Py()*91.1/92., e.electrons[1].p4.Pz()*91.1/92., e.electrons[1].p4.E()*91.1/92.);
		       EPT1_A *= r.Gaus(EPT1_A.E(),proc_A_B*EPT1_A.E())/EPT1_A.E();
		       EPT1_B *= r.Gaus(EPT1_B.E(),proc_B_B*EPT1_B.E())/EPT1_B.E();
		   } else {	
		       A_Wei=A_Wei*HEEP_A_E;
		       B_Wei=B_Wei*HEEP_B_E; 
		       EPT1_A.SetPxPyPzE(e.electrons[1].p4.Px()*92./92., e.electrons[1].p4.Py()*92./92., e.electrons[1].p4.Pz()*92./92., e.electrons[1].p4.E()*92./92.); 
		       EPT1_B.SetPxPyPzE(e.electrons[1].p4.Px()*93.2/92., e.electrons[1].p4.Py()*93.2/92., e.electrons[1].p4.Pz()*93.2/92., e.electrons[1].p4.E()*93.2/92.);
		       EPT1_A *= r.Gaus(EPT1_A.E(),proc_A_E*EPT1_A.E())/EPT1_A.E();
		       EPT1_B *= r.Gaus(EPT1_B.E(),proc_B_E*EPT1_B.E())/EPT1_B.E();
		   }  
	               Weight_A = Weight_A * A_Wei;
	               Weight_B = Weight_B * B_Wei;
	           
		   
		   if ((abs((e.electrons[0].p4).Eta())>=2.1)&&(e.electrons[0].p4.Pt()<=45.)&&(e.electrons[1].p4.Pt()>=40.)){
	              // Weight_A=Weight_A*(0.08*(e.electrons[0].p4.Pt()-40.)+0.6); 
		       Weight_B=Weight_B*(0.08*(e.electrons[0].p4.Pt()-40.)+0.6);} 
	           
		   if ((abs((e.electrons[1].p4).Eta())>=2.1)&&(e.electrons[1].p4.Pt()<=45.)&&(e.electrons[1].p4.Pt()>=40.)){
	              // Weight_A=Weight_A*(0.08*(e.electrons[1].p4.Pt()-40.)+0.6); 
		       Weight_B=Weight_B*(0.08*(e.electrons[1].p4.Pt()-40.)+0.6);} 	          
	       
	       } else  {
	        //   Weight_A = Weight_A/Prescale;
	        //   Weight_B = Weight_B/Prescale;
		   EPT0_A.SetPxPyPzE(e.electrons[0].p4.Px(), e.electrons[0].p4.Py(), e.electrons[0].p4.Pz(), e.electrons[0].p4.E()); 
		   EPT0_B.SetPxPyPzE(e.electrons[0].p4.Px(), e.electrons[0].p4.Py(), e.electrons[0].p4.Pz(), e.electrons[0].p4.E());
		   EPT1_A.SetPxPyPzE(e.electrons[1].p4.Px(), e.electrons[1].p4.Py(), e.electrons[1].p4.Pz(), e.electrons[1].p4.E()); 
		   EPT1_B.SetPxPyPzE(e.electrons[1].p4.Px(), e.electrons[1].p4.Py(), e.electrons[1].p4.Pz(), e.electrons[1].p4.E());  
	       }
	       
	       */
	       if (j>=2){
	           if (abs((e.electrons[0].p4).Eta())<=1.44){
		       A_Wei=HEEP_A_B; B_Wei=HEEP_B_B;
		   } else {
		       A_Wei=HEEP_A_E; B_Wei=HEEP_B_E; 
		   }
	           if (abs((e.electrons[1].p4).Eta())<=1.44){
		       A_Wei=A_Wei*HEEP_A_B; B_Wei=B_Wei*HEEP_B_B;
		   } else {	
		       A_Wei=A_Wei*HEEP_A_E;
		       B_Wei=B_Wei*HEEP_B_E; 
		   }  
	               Weight_A = Weight_A * A_Wei;
	               Weight_B = Weight_B * B_Wei;
	           
		   
		   if ((abs((e.electrons[0].p4).Eta())>=2.1)&&(e.electrons[0].p4.Pt()<=45.)&&(e.electrons[1].p4.Pt()>=40.)){
	              // Weight_A=Weight_A*(0.08*(e.electrons[0].p4.Pt()-40.)+0.6); 
		       Weight_B=Weight_B*(0.08*(e.electrons[0].p4.Pt()-40.)+0.6);} 
	           
		   if ((abs((e.electrons[1].p4).Eta())>=2.1)&&(e.electrons[1].p4.Pt()<=45.)&&(e.electrons[1].p4.Pt()>=40.)){
	              // Weight_A=Weight_A*(0.08*(e.electrons[1].p4.Pt()-40.)+0.6); 
		       Weight_B=Weight_B*(0.08*(e.electrons[1].p4.Pt()-40.)+0.6);} 	          
	       } 
	           EPT0_A.SetPxPyPzE(e.electrons[0].p4.Px(), e.electrons[0].p4.Py(), e.electrons[0].p4.Pz(), e.electrons[0].p4.E()); 
		   EPT0_B.SetPxPyPzE(e.electrons[0].p4.Px(), e.electrons[0].p4.Py(), e.electrons[0].p4.Pz(), e.electrons[0].p4.E());
		   EPT1_A.SetPxPyPzE(e.electrons[1].p4.Px(), e.electrons[1].p4.Py(), e.electrons[1].p4.Pz(), e.electrons[1].p4.E()); 
		   EPT1_B.SetPxPyPzE(e.electrons[1].p4.Px(), e.electrons[1].p4.Py(), e.electrons[1].p4.Pz(), e.electrons[1].p4.E());  
	       
	              
	       if ((EPT0_A.Pt()>=Lep1Pt)&&(EPT1_A.Pt()>=Lep2Pt)&& 
	          ((EPT0_A + EPT1_A).M()>=MllCut)&&((EPT0_A + EPT1_A).M()<=MllCut1)&&
		  ((abs(e.electrons[0].p4.Eta())>=EtaCut1L)&&(abs(e.electrons[1].p4.Eta())>=EtaCut2L))&&
	          (e.electrons[0].ID <= HeepId)&&(e.electrons[1].ID <= HeepId)&&
		  (e.electrons[0].isoHEEP() <= ISO)&&(e.electrons[1].isoHEEP() <= ISO)) {
		  // cout<< "Bit " << I_T<<"    Trigger "<<Name <<"    Prescale "<< Prescale<<endl;
		   int flag_jet = 1;
		   for (unsigned int k = 0; k < NofJets; ++k)	{     
	                if ((flag_jet == 1)&&(e.jets[k].p4.Pt()>=JetPt)&&
			    (e.jets[k].ID>=1)) { flag_jet = 1;} else { flag_jet = 0; Break;}  
		   }
		   if (flag_jet == 1) {
		       Mll = (EPT0_A + EPT1_A).M();
		       hMll_A[j]->Fill(Mll,Weight_A);
		       hPU_A[j]->Fill(e.vertices.size(),Weight_A);
		   //    cout<< "Event "<<i<<" Run "<< e.irun<<" Lumi  "<< e.ilumi <<" Event No "<< e.ievent<<endl;    
		   }     
               }
	       
	       if ((EPT0_B.Pt()>=Lep1Pt)&&(EPT1_B.Pt()>=Lep2Pt)&& 
	          ((EPT0_B + EPT1_B).M()>=MllCut)&&((EPT0_B + EPT1_B).M()<=MllCut1)&&
		  ((abs(e.electrons[0].p4.Eta())>=EtaCut1L)&&(abs(e.electrons[1].p4.Eta())>=EtaCut2L))&&
	           (e.electrons[0].ID <= HeepId)&&(e.electrons[1].ID <= HeepId)&&
		   (e.electrons[0].isoHEEP() <= ISO)&&(e.electrons[1].isoHEEP() <= ISO)) {
		  // cout<< "Bit " << I_T<<"    Trigger "<<Name <<"    Prescale "<< Prescale<<endl;
		   int flag_jet = 1;
		   for (unsigned int k = 0; k < NofJets; ++k)	{     
	                if ((flag_jet == 1)&&(e.jets[k].p4.Pt()>=JetPt)&&
			    (e.jets[k].ID>=1)) { flag_jet = 1;} else { flag_jet = 0; Break;}  
		   }
		   if (flag_jet == 1) {
		       Mll = (EPT0_B + EPT1_B).M();
		       hMll_B[j]->Fill(Mll,Weight_B);
	               hPU_B[j]->Fill(e.vertices.size(),Weight_B);
		   //    cout<< "Event "<<i<<" Run "<< e.irun<<" Lumi  "<< e.ilumi <<" Event No "<< e.ievent<<endl;    
		   }     
               }
	       
	  }
      }
  }
//============================================================================================================================  
// MC BG QCD FAKE-RATE 
  Lum[11]=Lum[0];    
  Lum[12]=Lum[1];
  
  for (int k = 11; k <= 12; ++k) {     
	 ifstream inFile(fname[k]);  
     // Normalization for Data sample lumi  
     float QCD_weight_sample = Lum[k]/100.;
     float QCD_e_jets_size = 0; 
     float QCD_e_electrons_size = 0;  
     float QCD_e_weight= 1.;
     float pjg[4];
     int QCD_itype = 0;
     int QCD_jemark = 0;
     int QCD_e_electrons_0_charge;
     int QCD_e_electrons_1_charge;
     long int QCD_e_ievent = 0;
	 int n_ev=0;

     char lin[200] = {" "};
 
     TLorentzVector QCD_e_electrons_0_p4, QCD_e_electrons_1_p4, QCD_e_jets_0_p4, QCD_e_jets_1_p4;
 
     while (!inFile.eof()) {
  
	     inFile >> QCD_e_ievent;
	     inFile.getline(lin, 200);
	
	     inFile >> QCD_e_jets_size;
         inFile >> QCD_e_weight;
         inFile >> QCD_itype;   
         inFile.getline(lin, 200);
	
	     QCD_e_weight = QCD_e_weight*QCD_weight_sample;
	
	     // read Jets informations 
		  
         inFile >> QCD_jemark;
	     for (int j = 0; j < 4; j++) inFile >> pjg[j];
         QCD_e_jets_0_p4.SetPxPyPzE(pjg[0], pjg[1], pjg[2], pjg[3]);	
 	     inFile.getline(lin, 200);
	
	     inFile >> QCD_jemark;
	     for (int j = 0; j < 4; j++) inFile >> pjg[j];
	     QCD_e_jets_1_p4.SetPxPyPzE(pjg[0], pjg[1], pjg[2], pjg[3]);	
	     inFile.getline(lin, 200);
		 
		 for (int jn = 0; jn < (QCD_e_jets_size-2); jn++) {
		     inFile.getline(lin, 200);
		 }
	
	     // Read Leptons informations
	     inFile >> QCD_e_electrons_size;
          inFile.getline(lin, 200);
	
	     inFile >> QCD_e_electrons_0_charge;
	     for (int j = 0; j < 4; j++) inFile >> pjg[j];
	     QCD_e_electrons_0_p4.SetPxPyPzE(pjg[0], pjg[1], pjg[2], pjg[3]);	
	     inFile.getline(lin, 200);
	
	     inFile >> QCD_e_electrons_1_charge;
	     for (int j = 0; j < 4; j++) inFile >> pjg[j];
	     QCD_e_electrons_1_p4.SetPxPyPzE(pjg[0], pjg[1], pjg[2], pjg[3]);	
	     inFile.getline(lin, 200);
	
         inFile.getline(lin, 200);
		 
         if ((QCD_e_electrons_size >= NofLept) && (QCD_e_jets_size >= NofJets)&&
	     (QCD_e_electrons_0_p4.Pt()>=Lep1Pt) && (QCD_e_electrons_1_p4.Pt()>=Lep2Pt) &&
	     (QCD_e_jets_0_p4.Pt()>=JetPt) && (QCD_e_jets_1_p4.Pt()>=JetPt)&&
	     ((QCD_e_electrons_0_p4 + QCD_e_electrons_1_p4).M()>=MllCut)&&((QCD_e_electrons_0_p4 + QCD_e_electrons_1_p4).M()<=MllCut1)&&
	     ((abs(QCD_e_electrons_0_p4.Eta())>=EtaCut1L) && (abs(QCD_e_electrons_1_p4.Eta())>=EtaCut2L))
	     ) {  
			 
	      Mll = (QCD_e_electrons_0_p4 + QCD_e_electrons_1_p4).M();			  
	      hMll_A[k]->Fill(Mll,QCD_e_weight);
              hMll_B[k]->Fill(Mll,QCD_e_weight);
//	      hPU_A[k]->Fill(e.vertices.size(),QCD_e_weight);
//            hPU_B[k]->Fill(e.vertices.size(),QCD_e_weight);			  
         }
     }
  }	

cout <<" QCD_A = "<< hMll_A[11]->Integral() << " QCD_B = "<< hMll_B[12]->Integral() << endl; 
  //=============================================================================================================================  
  hMll_A[9]->Add(hMll_A[11]);
  hMll_B[9]->Add(hMll_B[12]);
  hPU_A[9]->Add(hPU_A[11]);
  hPU_B[9]->Add(hPU_B[12]);

  for (int i = 8; i >= 2; --i) {
       hMll_A[i]->Add(hMll_A[i+1]);
       hMll_B[i]->Add(hMll_B[i+1]);
       hPU_A[i]->Add(hPU_A[i+1]);
       hPU_B[i]->Add(hPU_B[i+1]);
  } 


 TFile* f_out = new TFile("Z-Peak.root","recreate");
  for (int i = 0; i <= 2; ++i) { 
       hMll_A[i]->Write(); 
       hMll_B[i]->Write();
       cout<<i<<" hMll_A "<< hMll_A[i]->Integral()<<" hMll_B "<< hMll_B[i]->Integral()<<endl;
       hPU_A[i]->Write();
       hPU_B[i]->Write();
       cout<<i<<" hPU_A "<< hPU_A[i]->Integral()<<" hPU_B "<< hPU_B[i]->Integral()<<endl;
  }


// Histos plotting and saving

  gStyle->SetOptStat("");
  gStyle->SetOptLogy(1);

  TCanvas c("Z-Peak","Z-Peak");
  
  hMll_A[0]->SetLineColor(1);
  hMll_A[0]->SetLineWidth(2); 

  hMll_A[2]->SetLineColor(3);
  hMll_A[2]->SetLineWidth(5);
  
  hPU_A[0]->SetLineColor(1);
  hPU_A[0]->SetLineWidth(2); 

  hPU_A[2]->SetLineColor(3);
  hPU_A[2]->SetLineWidth(5);
/*
  hMll_A[0]->Fit("gaus"); 
   
  TF1* fitf = new TF1("fitf", "[0] + [1]*exp(-([2] - x)*([2] - x)/(2*[3]*[3]))", 80, 100);
  fitf->SetNpx(500);
  fitf->SetParameters(500, 10000, 90, 5);

  hMll_A[0]->Fit(fitf, "IMN");
   double A = fitf->GetParameter(1); // area under gaussian
   double B = fitf->GetParameter(2); // area under gaussian
   double C = fitf->GetParameter(3); // area under gaussian
  
  cout << "M_Z = "<< B << endl;
  cout << "Width_Z = "<< C <<endl;
  
  //hMll_A[2]->Fit("gaus");
    
  TF1* fitf1 = new TF1("fitf1", "[0] + [1]*exp(-([2] - x)*([2] - x)/(2*[3]*[3]))", 80, 100);
  fitf1->SetNpx(500);
  fitf1->SetParameters(500, 10000, 90, 5);
  hMll_A[2]->Fit(fitf1, "IMN");
   double A1 = fitf1->GetParameter(1); // area under gaussian
   double B1 = fitf1->GetParameter(2); // area under gaussian
   double C1 = fitf1->GetParameter(3); // area under gaussian
  
  cout << "MC M_Z = "<< B1 << endl;
  cout << "MC Width_Z = "<< C1 <<endl;

*/
   hMll_A[2]->Draw("e1");  hMll_A[0]->Draw("e1same");   
   hMll_A[2]->GetYaxis()->SetRangeUser(10, 100000);
  c.Modified();

/*  
  // draw fit to background
  fitf->SetLineColor(kBlack);
  fitf->SetLineStyle(7);
  fitf->SetLineWidth(3);
  fitf->SetParameter(1, 0);
  fitf->DrawCopy("same");
  
  
  // draw fit to background + signal
  fitf->SetLineColor(kBlack);
  fitf->SetLineStyle(1);
  fitf->SetLineWidth(3);
  fitf->SetParameter(1, A);
  fitf->Draw("same");
  
  // draw fit to background
  fitf1->SetLineColor(kGreen);
  fitf1->SetLineStyle(7);
  fitf1->SetLineWidth(3);
  fitf1->SetParameter(1, 0);
  fitf1->DrawCopy("same");
 
  // draw fit to background + signal
  fitf1->SetLineColor(kGreen);
  fitf1->SetLineStyle(1);
  fitf1->SetLineWidth(3);
  fitf1->SetParameter(1, A1);
  fitf1->Draw("same");
*/  
   TLegend leg(0.64, 0.79, 0.94, 0.94);
  leg.SetTextFont(42);
  leg.SetTextSize(0.034);
   
  sprintf(str,"Ele-Ele channel"); 
  leg.SetHeader(str);
  sprintf(str,"2011A: %5.0f", hMll_A[0]->Integral()); 
  leg.AddEntry(hMll_A[0], str, "EL");
  sprintf(str,"MC: %5.0f", hMll_A[2]->Integral());
  leg.AddEntry(hMll_A[2], str,"EL");
  
  leg.Draw();
  label_Lumi(0.38, 0.85, Lum[0]);
  label_Prelim(0.38, 0.9);
  c.Print("Z-Peak-A.pdf");
  c.Print("Z-Peak-A.png");
  c.Print("Z-Peak-A.cxx");
  
  hPU_A[2]->Draw("e1");  hPU_A[0]->Draw("e1same");  
  leg.Draw();
  label_Lumi(0.38, 0.35, Lum[0]);
  label_Prelim(0.38, 0.4);
  c.Print("Z-Peak-A-PU.pdf");
  c.Print("Z-Peak-A-PU.png");
  c.Print("Z-Peak-A-PU.cxx");

  
  hMll_B[1]->SetLineColor(1);
  hMll_B[1]->SetLineWidth(2); 

  hMll_B[2]->SetLineColor(3);
  hMll_B[2]->SetLineWidth(5);
  
  hPU_B[1]->SetLineColor(1);
  hPU_B[1]->SetLineWidth(2); 

  hPU_B[2]->SetLineColor(3);
  hPU_B[2]->SetLineWidth(5);

  //hMll_B[1]->Fit("gaus"); 
/*  
  fitf = new TF1("fitf", "[0] + [1]*exp(-([2] - x)*([2] - x)/(2*[3]*[3]))", 86, 96);
  fitf->SetNpx(500);
  fitf->SetParameters(500, 10000, 90, 5);

  hMll_B[1]->Fit(fitf, "IMN");
  A = fitf->GetParameter(1); // area under gaussian
  B = fitf->GetParameter(2); // area under gaussian
  C = fitf->GetParameter(3); // area under gaussian
  
  cout << "M_Z = "<< B << endl;
  cout << "Width_Z = "<< C <<endl;
  
 
  //hMll_B[2]->Fit("gaus");
  
  fitf1 = new TF1("fitf1", "[0] + [1]*exp(-([2] - x)*([2] - x)/(2*[3]*[3]))", 86, 96);
  fitf1->SetNpx(500);
  fitf1->SetParameters(500, 10000, 90, 5);
  hMll_B[2]->Fit(fitf1, "IMN");
  A1 = fitf1->GetParameter(1); // area under gaussian
  B1 = fitf1->GetParameter(2); // area under gaussian
  C1 = fitf1->GetParameter(3); // area under gaussian
  
  cout << "MC M_Z = "<< B1 << endl;
  cout << "MC Width_Z = "<< C1 <<endl;
 */ 
   hMll_B[2]->Draw("e1");  hMll_B[1]->Draw("e1same"); 
   hMll_B[2]->GetYaxis()->SetRangeUser(10, 100000);
   c.Modified(); 

  /*
  // draw fit to background
  fitf->SetLineColor(kBlack);
  fitf->SetLineStyle(7);
  fitf->SetLineWidth(3);
  fitf->SetParameter(1, 0);
  fitf->DrawCopy("same");
  
  
  // draw fit to background + signal
  fitf->SetLineColor(kBlack);
  fitf->SetLineStyle(1);
  fitf->SetLineWidth(3);
  fitf->SetParameter(1, A);
  fitf->Draw("same");
  
  // draw fit to background
  fitf1->SetLineColor(kGreen);
  fitf1->SetLineStyle(7);
  fitf1->SetLineWidth(3);
  fitf1->SetParameter(1, 0);
  fitf1->DrawCopy("same");
 
  // draw fit to background + signal
  fitf1->SetLineColor(kGreen);
  fitf1->SetLineStyle(1);
  fitf1->SetLineWidth(3);
  fitf1->SetParameter(1, A1);
  fitf1->Draw("same");
 */ 
  TLegend leg1(0.64, 0.79, 0.94, 0.94);
  //TLegend leg1(0.17, 0.52, 0.57, 0.94);
  leg1.SetTextFont(42);
  leg1.SetTextSize(0.034);
   
  sprintf(str,"Ele-Ele channel"); 
  leg1.SetHeader(str);
  sprintf(str,"2011B: %5.0f", hMll_B[1]->Integral()); 
  leg1.AddEntry(hMll_B[1], str, "EL");
  sprintf(str,"MC: %5.0f", hMll_B[2]->Integral());
  leg1.AddEntry(hMll_B[2], str,"EL");
  
  leg1.Draw();
  label_Lumi(0.38, 0.85, Lum[1]);
  label_Prelim(0.38, 0.9);
  c.Print("Z-Peak-B.pdf");
  c.Print("Z-Peak-B.png");
  c.Print("Z-Peak-B.cxx");
  
  hPU_B[2]->Draw("e1");  hPU_B[1]->Draw("e1same");  
  leg1.Draw();
  label_Lumi(0.38, 0.35, Lum[1]);
  label_Prelim(0.38, 0.4);
  c.Print("Z-Peak-B-PU.pdf");
  c.Print("Z-Peak-B-PU.png");
  c.Print("Z-Peak-B-PU.cxx");
  
  //=====================================
  
  TH1* hnew_D = (TH1*) hPU_B[1]->Clone();
  TH1* hnew_MC = (TH1*) hPU_B[2]->Clone();
  hnew_D -> Scale((hnew_MC->Integral())/(hnew_D->Integral()));
  
  hnew_D->Divide(hnew_MC);
  
  hPU_B[2]->Multiply(hnew_D);
  cout<<" hPU_B_D "<< hPU_B[1]->Integral()<<" hPU_B_MC "<< hPU_B[2]->Integral()<<endl;
  
  for (int k = 1; k <= 35; ++k) {
  cout<< k<<"    "<< hnew_D->GetBinContent(k) << endl;
  }
  
  hPU_B[1]->SetLineColor(1);
  hPU_B[1]->SetLineWidth(2); 

  hPU_B[2]->SetLineColor(3);
  hPU_B[2]->SetLineWidth(5);
  
  hPU_B[2]->Draw("e1");  hPU_B[1]->Draw("e1same");  
  c.Print("Z-Peak-B-PU2.pdf"); 
  
  //======================================
 
  
  hMll_A[0]->Add(hMll_B[1]);
  hMll_A[2]->Add(hMll_B[2]);
  
  hMll_A[0]->SetLineColor(1);
  hMll_A[0]->SetLineWidth(2); 

  hMll_A[2]->SetLineColor(3);
  hMll_A[2]->SetLineWidth(5);
  
  hPU_A[0]->Add(hPU_B[1]);
  hPU_A[2]->Add(hPU_B[2]);
  
  hPU_A[0]->SetLineColor(1);
  hPU_A[0]->SetLineWidth(2); 

  hPU_A[2]->SetLineColor(3);
  hPU_A[2]->SetLineWidth(5);


/*
  //hMll_A[0]->Fit("gaus"); 
    
  fitf = new TF1("fitf", "[0] + [1]*exp(-([2] - x)*([2] - x)/(2*[3]*[3]))", 86, 96);
  fitf->SetNpx(500);
  fitf->SetParameters(500, 10000, 90, 5);

  hMll_A[0]->Fit(fitf, "IMN");
  A = fitf->GetParameter(1); // area under gaussian
  B = fitf->GetParameter(2); // area under gaussian
  C = fitf->GetParameter(3); // area under gaussian
  
  cout << "M_Z = "<< B << endl;
  cout << "Width_Z = "<< C <<endl;
  
 
  //hMll_A[2]->Fit("gaus");
  
  
  fitf1 = new TF1("fitf1", "[0] + [1]*exp(-([2] - x)*([2] - x)/(2*[3]*[3]))", 86, 96);
  fitf1->SetNpx(500);
  fitf1->SetParameters(500, 10000, 90, 5);
  hMll_A[2]->Fit(fitf1, "IMN");
  A1 = fitf1->GetParameter(1); // area under gaussian
  B1 = fitf1->GetParameter(2); // area under gaussian
  C1 = fitf1->GetParameter(3); // area under gaussian
  
  cout << "MC M_Z = "<< B1 << endl;
  cout << "MC Width_Z = "<< C1 <<endl;
 */ 
   hMll_A[2]->Draw("e1");  hMll_A[0]->Draw("e1same");  
/*
  
  // draw fit to background
  fitf->SetLineColor(kBlack);
  fitf->SetLineStyle(7);
  fitf->SetLineWidth(3);
  fitf->SetParameter(1, 0);
  fitf->DrawCopy("same");
  
  
  // draw fit to background + signal
  fitf->SetLineColor(kBlack);
  fitf->SetLineStyle(1);
  fitf->SetLineWidth(3);
  fitf->SetParameter(1, A);
  fitf->Draw("same");
  
  // draw fit to background
  fitf1->SetLineColor(kRed);
  fitf1->SetLineStyle(7);
  fitf1->SetLineWidth(3);
  fitf1->SetParameter(1, 0);
  fitf1->DrawCopy("same");
 
  // draw fit to background + signal
  fitf1->SetLineColor(kRed);
  fitf1->SetLineStyle(1);
  fitf1->SetLineWidth(3);
  fitf1->SetParameter(1, A1);
  fitf1->Draw("same");
*/
  TLegend leg2(0.64, 0.79, 0.94, 0.94);
//TLegend leg2(0.17, 0.52, 0.57, 0.94);
  leg2.SetTextFont(42);
  leg2.SetTextSize(0.034);
   
  sprintf(str,"Ele-Ele channel"); 
  leg2.SetHeader(str);
  sprintf(str,"2011A+B: %5.0f", hMll_A[0]->Integral()); 
  leg2.AddEntry(hMll_A[0], str, "EL");
  sprintf(str,"MC: %5.0f", hMll_A[2]->Integral());
  leg2.AddEntry(hMll_A[2], str,"EL");
  
  leg2.Draw();
  label_Lumi(0.38, 0.85, Lum[0]+Lum[1]);
  label_Prelim(0.38, 0.9);
  c.Print("Z-Peak-AB.pdf");
  
  hPU_A[2]->Draw("e1");  hPU_A[0]->Draw("e1same"); 
  leg2.Draw();
  label_Lumi(0.38, 0.35, Lum[0]+Lum[1]);
  label_Prelim(0.38, 0.4);
  c.Print("Z-Peak-AB-PU.pdf");
}
