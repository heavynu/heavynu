//To run this script on pcrfcms14.cern.ch machine: 
//root -b -q -l EMu_Analis.cxx+
#include "TH1.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TROOT.h"
#include <TRandom3.h>
#include <TLorentzVector.h>
#include <TMath.h>
#include <TLatex.h>
#include "TStyle.h"
#include "TLegend.h"
#include "TText.h"
#include "TAxis.h"
#include "../../PhysicsTools/Utilities/interface/LumiReweightingStandAlone.h"
//#include "../../PhysicsTools/Utilities/interface/Lumi3DReWeighting.h"

#include "tdrstyle.C"
//#include "heavynuStandard.C"
#include "heavynuStandard11.C"

#include "../PATDump/src/format.h"
#include <iostream>
#include <fstream>
using namespace std;

vector<double> generate_flat10_weights(TH1D* data_npu_estimated){
   // see SimGeneral/MixingModule/python/mix_E7TeV_FlatDist10_2011EarlyData_inTimeOnly_cfi.py; copy and paste from there:
// for spring11 and summer11, if out of time is not important:
//   const double npu_probs[25] = {0.0698146584, 0.0698146584, 0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,
//          0.0630151648,0.0526654164,0.0402754482,0.0292988928,0.0194384503,0.0122016783,0.007207042,0.004003637,0.0020278322,
//          0.0010739954,0.0004595759,0.0002229748,0.0001028162,4.58337152809607E-05 /* <-- 24 */};
// for summer11 PU_S4, obtained by averaging:
   const double npu_probs[25] = {0.104109, 0.0703573, 0.0698445, 0.0698254, 0.0697054, 0.0697907, 0.0696751, 0.0694486, 0.0680332,
                                 0.0651044, 0.0598036, 0.0527395, 0.0439513, 0.0352202, 0.0266714, 0.019411, 0.0133974, 0.00898536,
                                 0.0057516, 0.00351493, 0.00212087, 0.00122891, 0.00070592, 0.000384744, 0.000219377};
   vector<double> result(25);
   double s = 0.0;
   for(int npu=0; npu<25; ++npu){
       double npu_estimated = data_npu_estimated->GetBinContent(data_npu_estimated->GetXaxis()->FindBin(npu));                              
       result[npu] = npu_estimated / npu_probs[npu];
       s += npu_estimated;
   }
   // normalize weights such that the total sum of weights over thw whole sample is 1.0, i.e., sum_i  result[i] * npu_probs[i] should be 1.0 (!)
   for(int npu=0; npu<25; ++npu){
       result[npu] /= s;
   }
   return result;
}

void EMu_Analis()
{
// set plain style
  gROOT->SetStyle("Plain");
  TCanvas c1("OUTPUT1","OUTPUT1");
  setTDRStyle();
  fixOverlay(); 

  // activate storage of sum of squares of errors
  // to calculate correctly errors in histograms
  // with non-unit entries
  TH1::SetDefaultSumw2(kTRUE);
  
  TFile* f_A1 = new TFile("PileUp/2011A_pileup.root");
  TFile* f_B1 = new TFile("PileUp/2011B_pileup.root");
  TH1D* histo_A1 = (TH1D*) f_A1->Get("pileup");
  TH1D* histo_B1 = (TH1D*) f_B1->Get("pileup");

// Cuts for event selection
  unsigned int NofLept = 2;
  unsigned int NofJets = 2;
  float Lep1Pt = 60.;
  float Lep2Pt = 40.;
  float JetPt = 40.;
  float EtaCut1L = 1.44; //1.44
  float EtaCut2L = 2.5;
  float EtaCutJ = 2.5;
  float ISO = 1.;
  int HeepID = 0;
  float PhiWidthCut = 0.06;
  float MuonID = 3;
//  float Vertex = 100.03;
  float MllCut = 200.0; //150
  float MllCut1 = 1200000000000.0;
  float MW_Cut = 0.0;
  float MW_Cut1 = 65000000000000.0; 
 
// M_W histogramm parameters  
  int M_Delta_M_W =  25;//25;10
  double M_min_M_W = 0;// 0; 250
  double M_max_M_W = 2500;//2500;650
 
// M_Nu histogramm parameters  
  int M_Delta_M_Nu = 20;
  double M_min_M_Nu = 0;
  double M_max_M_Nu = 2000;  
 
// Mll histogramm parameters  
  int M_Delta_Mll =  20;//17;
  double M_min_Mll = 0;//60;
  double M_max_Mll = 2000;//400
  
 // MET histogramm parameters  
//  int M_Delta_MET = 20;
//  int M_min_MET = 0;
//  int M_max_MET = 200;

  TRandom3 r;
  float WR; 
  float Mll, MNu1, MNu2; 
  float HEEP_A_B=0.983;
  float HEEP_A_E=0.997;
  float HEEP_B_B=0.949;
  float HEEP_B_E=0.981;
  float proc_A_B = 0.016;
  float proc_B_B = 0.016;
  float proc_A_E = 0.05;
  float proc_B_E = 0.035;
  float A_Wei=1.;
  float B_Wei=1.;
  float Tune_A[13];
  float Tune_B[13];
  for (int i = 0; i <= 12; ++i) { 
      Tune_A[i]= 1.;
      Tune_B[i]= 1.;
  }
  Tune_A[3]= 1.103;// //TTb
  Tune_A[2]= 1.541;//1.541; //Z+J
  Tune_B[3]= 0.97;// //TTb
  Tune_B[2]= 1.534;//1.534; //Z+J
  float QCD_tune = 1.;
  
  char *str = (char*)alloca(10000);
  char *str1 = (char*)alloca(10000);

  TH1F* hMW_A[13];  
  TH1F* hMW_B[13];
  TH1F* hMNu1_A[13];  
  TH1F* hMNu1_B[13]; 
  TH1F* hMNu2_A[13];  
  TH1F* hMNu2_B[13];   
  TH1F* hMll_A[13];  
  TH1F* hMll_B[13]; 
  TH1F* hPU_A[13];
  TH1F* hPU_B[13];
  
  for (int i = 0; i <= 12; ++i) { 
    sprintf(str,"MW_A_%d", i);
    sprintf(str1,"MW_A; M(e#mujj) ,GeV;Events/%5.1f GeV", (M_max_M_W-M_min_M_W)/M_Delta_M_W );
    hMW_A[i] = new TH1F(str,str1, M_Delta_M_W, M_min_M_W, M_max_M_W); 
    sprintf(str,"MW_B_%d", i);
    sprintf(str1,"MW_B; M(e#mujj), GeV;Events/%5.1f GeV", (M_max_M_W-M_min_M_W)/M_Delta_M_W );
    hMW_B[i] = new TH1F(str,str1, M_Delta_M_W, M_min_M_W, M_max_M_W);  
  
    sprintf(str,"MNu1_A_%d", i);
    sprintf(str1,"MNu1_A; M(ejj), GeV;Events/%5.1f GeV", (M_max_M_Nu-M_min_M_Nu)/M_Delta_M_Nu);
    hMNu1_A[i] = new TH1F(str,str1, M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu); 
    sprintf(str,"MNu1_B_%d", i);
    sprintf(str1,"MNu1_B; M(ejj), GeV;Events/%5.1f GeV", (M_max_M_Nu-M_min_M_Nu)/M_Delta_M_Nu);
    hMNu1_B[i] = new TH1F(str,str1, M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu);
	
    sprintf(str,"MNu2_A_%d", i);
    sprintf(str1,"MNu2_A; M(#mujj), GeV;Events/%5.1f GeV", (M_max_M_Nu-M_min_M_Nu)/M_Delta_M_Nu);
    hMNu2_A[i] = new TH1F(str,str1, M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu); 
    sprintf(str,"MNu2_B_%d", i);
    sprintf(str1,"MNu2_B; M(#mujj), GeV;Events/%5.1f GeV", (M_max_M_Nu-M_min_M_Nu)/M_Delta_M_Nu);
    hMNu2_B[i] = new TH1F(str,str1, M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu);	
	
    sprintf(str,"Mll_A_%d", i);
    sprintf(str1,"Mll_A; M(e#mu), GeV;Events/%5.1f GeV", (M_max_Mll-M_min_Mll)/M_Delta_Mll );
    hMll_A[i] = new TH1F(str,str1, M_Delta_Mll, M_min_Mll, M_max_Mll); 
    sprintf(str,"Mll_B_%d", i);
    sprintf(str1,"Mll_B; M(e#mu), GeV;Events/%5.1f GeV", (M_max_Mll-M_min_Mll)/M_Delta_Mll );
    hMll_B[i] = new TH1F(str,str1, M_Delta_Mll, M_min_Mll, M_max_Mll);   
	
    sprintf(str,"PU_A_%d", i);
    hPU_A[i] = new TH1F(str, "PU_A; Primary vertex number ; Events", 35, 0, 35);
    sprintf(str,"PU_B_%d", i);
    hPU_B[i] = new TH1F(str, "PU_B; Primary vertex number ; Events", 35, 0, 35);
  }
  
  const char* fname[13];
  float Lum[13] = {0};
  int n[13] = {0};
  float Sum_A[13] = {0};
  float Sum_B[13] = {0};
  int nEvents[13] = {0};
  float Weight_A, Weight_B;
  TLorentzVector EPT0_A, EPT1_A, EPT0_B, EPT1_B;
  TLorentzVector JPT0_A, JPT1_A, JPT0_B, JPT1_B;

//  fname[0] = "/moscow41/heavynu/Data_2011/Electron_2011A.root";
//  fname[1] = "/moscow41/heavynu/Data_2011/Electron_B_v1.root";
  fname[0] = "/moscow41/heavynu/Data_2011/Muon_2011A.root";
  fname[1] = "/moscow41/heavynu/Data_2011/Muon_B_v1.root"; 
  fname[2] = "/moscow31/heavynu/42x-bg/DYToLL_M-50_Sherpa_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept_1jet.root";
  fname[3] = "/moscow31/heavynu/42x-bg/TTTo2L2Nu2B_7TeV-powheg-pythia6_Summer11-PU_S4_START42_V11-v1_AODSIM.root";
  fname[4] = "/moscow31/heavynu/42x-bg/WJetsToLNu_TuneZ2_7TeV-madgraph-tauola_Summer11-PU_S4_START42_V11-v1_AODSIM.root";
  fname[5] = "/moscow31/heavynu/42x-bg/ZZ_TuneZ2_7TeV_pythia6_tauola_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept.root";
  fname[6] = "/moscow31/heavynu/42x-bg/WZ_TuneZ2_7TeV_pythia6_tauola_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept.root";
  fname[7] = "/moscow31/heavynu/42x-bg/WW_TuneZ2_7TeV_pythia6_tauola_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept.root";
  fname[8] = "/moscow31/heavynu/42x-bg/T_TuneZ2_tW-channel-DR_7TeV-powheg-tauola_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept_1jet.root";
  fname[9] = "/moscow31/heavynu/42x-bg/Tbar_TuneZ2_tW-channel-DR_7TeV-powheg-tauola_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept_1jet.root";
  fname[10] = "/moscow31/heavynu/42x-signal-central/WRToNuLeptonToLLJJ_MW-1800_MNu-1000_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root";
  fname[11] = "/moscow31/heavynu/QCDDEA.d";
  fname[12] = "/moscow31/heavynu/QCDDEB.d";


  for (int j = 0; j <= 10; ++j) {
      TheEvent e(fname[j]);
      if (!e) return;
      Lum[j] = e.sample.lumi; //Luminocity in pb^-1
//      Lum[0] = 2173.;
//      Lum[1] = 2511;
      n[j] = 0;
      nEvents[j] = e.totalEvents();
      cout << "j = "<< j <<" totalEvents() = " << e.totalEvents()<<endl;
      for (int i = 0; i < nEvents[j]; ++i) {
          e.readEvent(i);
	  Weight_A = 1.;
	  Weight_B = 1.;
	  if (j>=2){  
              /*
	      Weight_A = Tune_A[j]*Lum[0]/100.* e.weight * e.Gen_weight * LumiWeights_A.weight3D(e.num_interactions(-1),e.num_interactions(0),e.num_interactions(1));   
		 // cout<<"A "<< LumiWeights_A.weight3D(e.num_interactions(-1),e.num_interactions(0),e.num_interactions(1)) << "     "<< Weight_A<< endl;
	      Weight_B = Tune_B[j]*Lum[1]/100.* e.weight * e.Gen_weight * LumiWeights_B.weight3D(e.num_interactions(-1),e.num_interactions(0),e.num_interactions(1));
	      if(e.vertices.size()<35){
	         Weight_B = Weight_B * Corr[e.vertices.size()];
	      }
	      //cout<<"B "<< LumiWeights_B.weight3D(e.num_interactions(-1),e.num_interactions(0),e.num_interactions(1)) << "     "<< Weight_B<< endl;
	      */
		  
	      if(e.pileupIT()<25){
                 Weight_A = Tune_A[j]*(Lum[0]/100.)*e.weight*e.Gen_weight*generate_flat10_weights(histo_A1)[e.pileupIT()];
	         Weight_B = Tune_B[j]*(Lum[1]/100.)*e.weight*e.Gen_weight*generate_flat10_weights(histo_B1)[e.pileupIT()];
		// cout<< Tune_A[j]<<"   "<< Lum[0]/100.<<"   "<< e.weight<<"   "<<e.Gen_weight*generate_flat10_weights(histo_A1)[e.pileupIT()]<< endl;
              }
              else{ //should not happen as we have a weight for all simulated ZJ_e.pileup.size() multiplicities!
                 Weight_A = 0.;
	         Weight_B = 0.;
              }	
	      
		//  Weight_A = Tune_A[j]* Lum[0]/100.* e.weight * e.Gen_weight;
		//  Weight_B = Tune_B[j]* Lum[1]/100.* e.weight * e.Gen_weight;
	  }
	  
//	  for (unsigned int cur=0;cur<e.electrons.size();cur++) {
//	       if (!((e.electrons[cur].ID <= HeepID)&&(e.electrons[cur].ScPhiWidth<=PhiWidthCut)&&(e.electrons[cur].isoHEEP32() <= ISO))) {e.electrons.erase(e.electrons.begin()+cur);}
//	  }
          
	  vector<Particle>::iterator current;
	  for (current=e.electrons.begin();current<e.electrons.end();current++) {
	  //     if (!((current->ID <= HeepID)&&(abs(current->ScPhiWidth)<=PhiWidthCut)&&(current->isoHEEP32() <= ISO)))  {e.electrons.erase(current);} 
	        if (!((current->ID <= HeepID)&&(current->isoHEEP() <= ISO)))  {e.electrons.erase(current);} 
	  }
	  for (unsigned int cur=0;cur<e.muons.size();cur++) {
	       if (!(e.muons[cur].ID >= MuonID)) {e.muons.erase(e.muons.begin()+cur);}
	  }
	  for (unsigned int cur=0;cur<e.jets.size();cur++) {
	       if (!((e.jets[cur].ID >= 1)&&(abs((e.jets[cur].p4).Eta())<=EtaCutJ))) {e.jets.erase(e.jets.begin()+cur);}
	  }	  
 
          if ((e.electrons.size() >= 1) && (e.muons.size() >= 1)  && (e.jets.size() >= NofJets)) {
	      
	      if (j>=2){
                   if (abs((e.electrons[0].p4).Eta())<=1.44){
		       A_Wei=HEEP_A_B; B_Wei=HEEP_B_B;
		       EPT0_A.SetPxPyPzE(e.electrons[0].p4.Px()*91./91.4, e.electrons[0].p4.Py()*91./91.4, e.electrons[0].p4.Pz()*91./91.4,e.electrons[0].p4.E()*91./91.4);
		       EPT0_B.SetPxPyPzE(e.electrons[0].p4.Px()*91.1/92., e.electrons[0].p4.Py()*91.1/92., e.electrons[0].p4.Pz()*91.1/92.,e.electrons[0].p4.E()*91.1/92.);
		       EPT0_A *= r.Gaus(EPT0_A.E(),proc_A_B*EPT0_A.E())/EPT0_A.E(); 
		       EPT0_B *= r.Gaus(EPT0_B.E(),proc_B_B*EPT0_B.E())/EPT0_B.E();
		   } else {
		       A_Wei=HEEP_A_E; B_Wei=HEEP_B_E; 
		       EPT0_A.SetPxPyPzE(e.electrons[0].p4.Px()*92./92., e.electrons[0].p4.Py()*92./92., e.electrons[0].p4.Pz()*92./92., e.electrons[0].p4.E()*92./92.);  
		       EPT0_B.SetPxPyPzE(e.electrons[0].p4.Px()*93.2/92., e.electrons[0].p4.Py()*93.2/92., e.electrons[0].p4.Pz()*93.2/92., e.electrons[0].p4.E()*93.2/92.);
		       EPT0_A *= r.Gaus(EPT0_A.E(),proc_A_E*EPT0_A.E())/EPT0_A.E(); 
		       EPT0_B *= r.Gaus(EPT0_B.E(),proc_B_E*EPT0_B.E())/EPT0_B.E();
		   }
		   
		       A_Wei=A_Wei*0.85;
		       B_Wei=B_Wei*0.85;
      		       EPT1_A.SetPxPyPzE(e.muons[0].p4.Px(), e.muons[0].p4.Py(), e.muons[0].p4.Pz(), e.muons[0].p4.E()); 
    		       EPT1_B.SetPxPyPzE(e.muons[0].p4.Px(), e.muons[0].p4.Py(), e.muons[0].p4.Pz(), e.muons[0].p4.E());
	  
                  Weight_A = Weight_A * A_Wei;
	           Weight_B = Weight_B * B_Wei;
				/*  
	           if ((abs((e.electrons[0].p4).Eta())>=2.1)&&(e.electrons[0].p4.Pt()<=45.)&&(e.electrons[0].p4.Pt()>=40.)){
	               Weight_A=Weight_A*(0.08*(e.electrons[0].p4.Pt()-40.)+0.6); 
		       Weight_B=Weight_B*(0.08*(e.electrons[0].p4.Pt()-40.)+0.6);}
				*/   
	       } else  {
	        //   Weight_A = Weight_A/Prescale;
	        //   Weight_B = Weight_B/Prescale;
		       EPT0_A.SetPxPyPzE(e.electrons[0].p4.Px(), e.electrons[0].p4.Py(), e.electrons[0].p4.Pz(), e.electrons[0].p4.E()); 
		       EPT0_B.SetPxPyPzE(e.electrons[0].p4.Px(), e.electrons[0].p4.Py(), e.electrons[0].p4.Pz(), e.electrons[0].p4.E());
		       EPT1_A.SetPxPyPzE(e.muons[0].p4.Px(), e.muons[0].p4.Py(), e.muons[0].p4.Pz(), e.muons[0].p4.E()); 
		       EPT1_B.SetPxPyPzE(e.muons[0].p4.Px(), e.muons[0].p4.Py(), e.muons[0].p4.Pz(), e.muons[0].p4.E());
	       }
               
	           JPT0_A.SetPxPyPzE(e.jets[0].p4.Px(), e.jets[0].p4.Py(), e.jets[0].p4.Pz(), e.jets[0].p4.E()); 
		   JPT0_B.SetPxPyPzE(e.jets[0].p4.Px(), e.jets[0].p4.Py(), e.jets[0].p4.Pz(), e.jets[0].p4.E());
		   JPT1_A.SetPxPyPzE(e.jets[1].p4.Px(), e.jets[1].p4.Py(), e.jets[1].p4.Pz(), e.jets[1].p4.E()); 
		   JPT1_B.SetPxPyPzE(e.jets[1].p4.Px(), e.jets[1].p4.Py(), e.jets[1].p4.Pz(), e.jets[1].p4.E());
		   
		   
	       if ((e.jets[0].p4.Pt()>=JetPt)&&(e.jets[0].ID>=1)&&(abs((e.jets[0].p4).Eta())<=EtaCutJ)&&
		      (e.jets[1].p4.Pt()>=JetPt)&&(e.jets[1].ID>=1)&&(abs((e.jets[1].p4).Eta())<=EtaCutJ)) {
	       
	          if ((((EPT0_A.Pt()>=Lep1Pt)&&(EPT1_A.Pt()>=Lep2Pt))||((EPT0_A.Pt()>=Lep2Pt)&&(EPT1_A.Pt()>=Lep1Pt)))&& 
	             ((EPT0_A + EPT1_A).M()>=MllCut)&&((EPT0_A + EPT1_A).M()<=MllCut1)&&
		     ((abs(e.electrons[0].p4.Eta())<=EtaCut1L)&&(abs(e.muons[0].p4.Eta())<=EtaCut2L))&&
		  //   (e.electrons[0].ID <= HeepID)&&(abs(e.electrons[0].ScPhiWidth)<=PhiWidthCut)&&(e.electrons[0].isoHEEP32() <= ISO)&&
		     (e.electrons[0].ID <= HeepID)&&(e.electrons[0].isoHEEP() <= ISO)&&
		     (e.muons[0].ID >= MuonID)&&
	             ((EPT0_A + EPT1_A + JPT0_A + JPT1_A).M()>MW_Cut)&&
		         ((EPT0_A + EPT1_A + JPT0_A + JPT1_A).M()<MW_Cut1)) {
		             WR = (EPT0_A + EPT1_A + JPT0_A + JPT1_A).M(); 
			         MNu1 = (EPT0_A + JPT0_A + JPT1_A).M(); 
			         MNu2 = (EPT1_A + JPT0_A + JPT1_A).M();
		             Mll = (EPT0_A + EPT1_A).M();
			         hMW_A[j]->Fill(WR,Weight_A);
			         hMNu1_A[j]->Fill(MNu1,Weight_A);	
			         hMNu2_A[j]->Fill(MNu2,Weight_A);				   
		             hMll_A[j]->Fill(Mll,Weight_A);
		             hPU_A[j]->Fill(e.vertices.size(),Weight_A);
			         Sum_A[j]+=Weight_A;
			         //cout<< e.jets[0].JECunc << "   " << e.jets[1].JECunc<< endl;
                        //cout<< "Event "<<i<<" Run "<< e.irun<<" Lumi  "<< e.ilumi <<" Event No "<< e.ievent<<"  Weight_A "<<Weight_A<<"  Weight_B  "<< Weight_B <<endl;
			         //cout<<e.irun<<" "<<e.ievent<<" "<<Weight_A<<endl;   
		            }
		   if ((((EPT0_B.Pt()>=Lep1Pt)&&(EPT1_B.Pt()>=Lep2Pt))||((EPT0_B.Pt()>=Lep2Pt)&&(EPT1_B.Pt()>=Lep1Pt)))&& 
	              ((EPT0_B + EPT1_B).M()>=MllCut)&&((EPT0_B + EPT1_B).M()<=MllCut1)&&
		      ((abs(e.electrons[0].p4.Eta())<=EtaCut1L)&&(abs(e.muons[0].p4.Eta())<=EtaCut2L))&&
		    //  (e.electrons[0].ID <= HeepID)&&(abs(e.electrons[0].ScPhiWidth)<=PhiWidthCut)&&(e.electrons[0].isoHEEP32() <= ISO)&&
		      (e.electrons[0].ID <= HeepID)&&(e.electrons[0].isoHEEP() <= ISO)&&
		      (e.muons[0].ID >= MuonID)&&
	              ((EPT0_B + EPT1_B + JPT0_B + JPT1_B).M()>MW_Cut)&&
		      ((EPT0_B + EPT1_B + JPT0_B + JPT1_B).M()<MW_Cut1)) {  
		       	WR = (EPT0_B + EPT1_B + JPT0_B + JPT1_B).M(); 
		     	MNu1 = (EPT0_B + JPT0_B + JPT1_B).M(); 
		    	MNu2 = (EPT1_B + JPT0_B + JPT1_B).M();
		        Mll = (EPT0_B + EPT1_B).M();
		    	hMW_B[j]->Fill(WR,Weight_B);
		    	hMNu1_B[j]->Fill(MNu1,Weight_B);	
		    	hMNu2_B[j]->Fill(MNu2,Weight_B);				   
		        hMll_B[j]->Fill(Mll,Weight_B);
	            hPU_B[j]->Fill(e.vertices.size(),Weight_B); 
		    	Sum_B[j]+=Weight_B;
	           }
		        
            }
	  }
      }
  }
  
  cout<<"Data = "<<Sum_A[0]+Sum_B[1]<<endl;
  cout<<"TTb= "<<Sum_A[3]+Sum_B[3]<<endl;
  cout<<"DY= "<<Sum_A[2]+Sum_B[2]<<endl;
  
  //=============================================================================================================================
  hMW_A[9]->Add(hMW_A[11]);
  hMW_B[9]->Add(hMW_B[12]); 
  hMNu1_A[9]->Add(hMNu1_A[11]);
  hMNu1_B[9]->Add(hMNu1_B[12]);
  hMNu2_A[9]->Add(hMNu2_A[11]);
  hMNu2_B[9]->Add(hMNu2_B[12]);	   
  hMll_A[9]->Add(hMll_A[11]);
  hMll_B[9]->Add(hMll_B[12]);
  
 
 
   for (int i = 8; i >= 4; --i) {
       hMW_A[i]->Add(hMW_A[i+1]);
       hMW_B[i]->Add(hMW_B[i+1]); 
       hMNu1_A[i]->Add(hMNu1_A[i+1]);
       hMNu1_B[i]->Add(hMNu1_B[i+1]);
       hMNu2_A[i]->Add(hMNu2_A[i+1]);
       hMNu2_B[i]->Add(hMNu2_B[i+1]);	   
       hMll_A[i]->Add(hMll_A[i+1]);
       hMll_B[i]->Add(hMll_B[i+1]);
       hPU_A[i]->Add(hPU_A[i+1]);
       hPU_B[i]->Add(hPU_B[i+1]);
  } 

  cout <<" Other = "<<hMW_A[4]->Integral()+ hMW_B[4]->Integral() - (hMW_A[11]->Integral()+ hMW_B[12]->Integral())<<endl;

  
//=========================================================================================================================
 
      TH1F* hMW_A_2 = (TH1F*) hMW_A[2]->Clone();
      TH1F* hMW_B_2 = (TH1F*) hMW_B[2]->Clone();
      TH1F* hMNu1_A_2 = (TH1F*) hMNu1_A[2]->Clone();
      TH1F* hMNu1_B_2 = (TH1F*) hMNu1_B[2]->Clone();
      TH1F* hMll_A_2 = (TH1F*) hMll_A[2]->Clone();
      TH1F* hMll_B_2 = (TH1F*) hMll_B[2]->Clone();
      TH1F* hMNu2_A_2 = (TH1F*) hMNu2_A[2]->Clone();
      TH1F* hMNu2_B_2 = (TH1F*) hMNu2_B[2]->Clone();
      TH1F* hPU_A_2 = (TH1F*) hPU_A[2]->Clone();
      TH1F* hPU_B_2 = (TH1F*) hPU_B[2]->Clone();
      
      TH1F* hMW_A_3 = (TH1F*) hMW_A[3]->Clone();
      TH1F* hMW_B_3 = (TH1F*) hMW_B[3]->Clone();
      TH1F* hMNu1_A_3 = (TH1F*) hMNu1_A[3]->Clone();
      TH1F* hMNu1_B_3 = (TH1F*) hMNu1_B[3]->Clone();
      TH1F* hMll_A_3 = (TH1F*) hMll_A[3]->Clone();
      TH1F* hMll_B_3 = (TH1F*) hMll_B[3]->Clone();
      TH1F* hMNu2_A_3 = (TH1F*) hMNu2_A[3]->Clone();
      TH1F* hMNu2_B_3 = (TH1F*) hMNu2_B[3]->Clone();
      TH1F* hPU_A_3 = (TH1F*) hPU_A[3]->Clone();
      TH1F* hPU_B_3 = (TH1F*) hPU_B[3]->Clone();

   for (int i = 3; i >= 2; --i) {  
       hMW_A[i]->Add(hMW_A[i+1]);
       hMW_B[i]->Add(hMW_B[i+1]); 
       hMNu1_A[i]->Add(hMNu1_A[i+1]);
       hMNu1_B[i]->Add(hMNu1_B[i+1]);
       hMNu2_A[i]->Add(hMNu2_A[i+1]);
       hMNu2_B[i]->Add(hMNu2_B[i+1]);	   
       hMll_A[i]->Add(hMll_A[i+1]);
       hMll_B[i]->Add(hMll_B[i+1]);
       hPU_A[i]->Add(hPU_A[i+1]);
       hPU_B[i]->Add(hPU_B[i+1]);
  } 

       hMW_A_2->Add(hMW_A[4]);
       hMW_B_2->Add(hMW_B[4]); 
       hMNu1_A_2->Add(hMNu1_A[4]);
       hMNu1_B_2->Add(hMNu1_B[4]);
       hMNu2_A_2->Add(hMNu2_A[4]);
       hMNu2_B_2->Add(hMNu2_B[4]);	   
       hMll_A_2->Add(hMll_A[4]);
       hMll_B_2->Add(hMll_B[4]);
       hPU_A_2->Add(hPU_A[4]);
       hPU_B_2->Add(hPU_B[4]);

       hMW_A_3->Add(hMW_A_2);
       hMW_B_3->Add(hMW_B_2); 
       hMNu1_A_3->Add(hMNu1_A_2);
       hMNu1_B_3->Add(hMNu1_B_2);
       hMNu2_A_3->Add(hMNu2_A_2);
       hMNu2_B_3->Add(hMNu2_B_2);	   
       hMll_A_3->Add(hMll_A_2);
       hMll_B_3->Add(hMll_B_2);
       hPU_A_3->Add(hPU_A_2);
       hPU_B_3->Add(hPU_B_2);



//=========================================================================================================================
 
   
// Histos plotting and saving
 
  gStyle->SetOptStat("");
  gStyle->SetOptLogy(1);
  TCanvas c("OUTPUT","OUTPUT");
  
  hMW_A[10]->Add(hMW_B[10]);
  hMll_A[10]->Add(hMll_B[10]);
  hMNu1_A[10]->Add(hMNu1_B[10]);
  hMNu2_A[10]->Add(hMNu2_B[10]);
  
  
  hMW_A[0]->SetLineColor(1);
  hMW_A[0]->SetLineWidth(2); 

  hMW_A_2->SetLineColor(5);
  hMW_A_2->SetFillColor(5);
  
  hMW_A_3->SetLineColor(4);
  hMW_A_3->SetFillColor(4); 
  
  hMW_A[4]->SetLineColor(3);
  hMW_A[4]->SetFillColor(3);
  
  hMW_A[10]->SetLineColor(2);
  hMW_A[10]->SetLineWidth(3);
  
  hMW_A[0]->Draw("e1"); hMW_A_3->Draw("HISTsame"); hMW_A_2->Draw("HISTsame"); hMW_A[4]->Draw("HISTsame"); hMW_A[0]->Draw("e1same"); 

  hMW_A[0]->GetYaxis()->SetRangeUser(0.1, 1000);
  c.Modified();
  
  TLegend leg_A_W(0.64, 0.74, 0.94, 0.94);
  leg_A_W.SetTextFont(42);
  leg_A_W.SetTextSize(0.034);
  sprintf(str1,"Data:%4.0f", hMW_A[0]->Integral()); 
  leg_A_W.AddEntry(hMW_A[0], str1, "EL");
  sprintf(str1,"t#bar{t}:%4.0f", (hMW_A_3->Integral()) - (hMW_A_2->Integral()));
  leg_A_W.AddEntry(hMW_A_3, str1,"F");
  sprintf(str1,"Z+jets:%4.0f", (hMW_A_2->Integral()) - (hMW_A[4]->Integral()));
  leg_A_W.AddEntry(hMW_A_2, str1,"F");
  sprintf(str1,"Other:%4.0f", (hMW_A[4]->Integral()));
  leg_A_W.AddEntry(hMW_A[4], str1,"F");
  leg_A_W.Draw(); 
  label_Lumi(0.38, 0.85, Lum[0]);
  label_Prelim(0.38, 0.9);
  
  c.Print("M_W_A_EMu.pdf");
  c.Print("M_W_A_EMu.png");
  c.Print("M_W_A_EMu.cxx");
  //=====================================================================
 
  hMW_B[1]->SetLineColor(1);
  hMW_B[1]->SetLineWidth(2); 

  hMW_B_2->SetLineColor(5);
  hMW_B_2->SetFillColor(5);
  
  hMW_B_3->SetLineColor(4);
  hMW_B_3->SetFillColor(4); 
  
  hMW_B[4]->SetLineColor(3);
  hMW_B[4]->SetFillColor(3);
 
  hMW_B[10]->SetLineColor(2);
  hMW_B[10]->SetLineWidth(3);
 
  hMW_B[1]->Draw("e1"); hMW_B_3->Draw("HISTsame"); hMW_B_2->Draw("HISTsame"); hMW_B[4]->Draw("HISTsame");  hMW_B[1]->Draw("e1same"); 

  hMW_B[1]->GetYaxis()->SetRangeUser(0.1, 1000);
  c.Modified();
  
  TLegend leg_B_W(0.64, 0.74, 0.94, 0.94);
  leg_B_W.SetTextFont(42);
  leg_B_W.SetTextSize(0.034);
  sprintf(str1,"Data:%4.0f", hMW_B[1]->Integral()); 
  leg_B_W.AddEntry(hMW_B[1], str1,"EL");
  sprintf(str1,"t#bar{t}:%4.0f", (hMW_B_3->Integral()) - (hMW_B_2->Integral()));
  leg_B_W.AddEntry(hMW_B_3, str1, "F");
  sprintf(str1,"Z+jets:%4.0f", (hMW_B_2->Integral()) - (hMW_B[4]->Integral()));
  leg_B_W.AddEntry(hMW_B_2, str1,"F");
  sprintf(str1,"Other:%4.0f", (hMW_B[4]->Integral()));
  leg_B_W.AddEntry(hMW_B[4], str1,"F");
  leg_B_W.Draw(); 
  
  label_Lumi(0.38, 0.85, Lum[1]);
  label_Prelim(0.38, 0.9);
  
  c.Print("M_W_B_EMu.pdf");
  c.Print("M_W_B_EMu.png");
  c.Print("M_W_B_EMu.cxx");
  
  //===================================================================== 
  
  hMW_A[0]->Add(hMW_B[1]);
  hMW_A[2]->Add(hMW_B[2]);
  hMW_A[3]->Add(hMW_B[3]);
  hMW_A_2->Add(hMW_B_2);
  hMW_A_3->Add(hMW_B_3);  
  hMW_A[4]->Add(hMW_B[4]); 
  
  hMW_A[0]->SetLineColor(1);
  hMW_A[0]->SetLineWidth(2); 

  hMW_A_2->SetLineColor(5);
  hMW_A_2->SetFillColor(5);
  
  hMW_A_3->SetLineColor(4);
  hMW_A_3->SetFillColor(4); 
  
  hMW_A[4]->SetLineColor(3);
  hMW_A[4]->SetFillColor(3);
  
  hMW_A[10]->SetLineColor(2);
  hMW_A[10]->SetLineWidth(3);
 
  hMW_A[0]->Draw("e1"); hMW_A_3->Draw("HISTsame"); hMW_A_2->Draw("HISTsame"); hMW_A[4]->Draw("HISTsame"); hMW_A[10]->Draw("HISTsame");  hMW_A[0]->Draw("e1same"); 

  hMW_A[0]->GetYaxis()->SetRangeUser(0.01, 1000);
  c.Modified();
 
  TLegend leg(0.64, 0.74, 0.94, 0.94);
//TLegend leg(0.17, 0.52, 0.57, 0.94);
  leg.SetTextFont(42);
  leg.SetTextSize(0.034);
   
//  sprintf(str1,"Electron-Electron channel"); 
//  leg.SetHeader(str1);
  sprintf(str1,"Data:%4.0f", hMW_A[0]->Integral()); 
  leg.AddEntry(hMW_A[0], str1, "EL");
  sprintf(str1,"t#bar{t}:%4.0f", (hMW_A_3->Integral()) - (hMW_A_2->Integral()));
  leg.AddEntry(hMW_A_3, str1,"F");
  sprintf(str1,"Z+jets:%4.0f", (hMW_A_2->Integral()) - (hMW_A[4]->Integral()));
  leg.AddEntry(hMW_A_2, str1,"F");
  sprintf(str1,"Other:%4.0f", (hMW_A[4]->Integral()));
  leg.AddEntry(hMW_A[4], str1,"F");
  sprintf(str1,"Signal:%4.0f",(hMW_A[10]->Integral())); 
  leg.AddEntry(hMW_A[10], str1,"L");
  leg.Draw();
   
  label_Lumi(0.38, 0.85, Lum[0]+Lum[1]);
  label_Prelim(0.38, 0.9);
 
  c.Print("M_W_EMu.pdf");
  c.Print("M_W_EMu.png");
  c.Print("M_W_EMu.cxx");
  
  //=====================================================================
  
  
  hMll_A[0]->SetLineColor(1);
  hMll_A[0]->SetLineWidth(2); 

  hMll_A[2]->SetLineColor(5);
  hMll_A[2]->SetFillColor(5);
  
  hMll_A[3]->SetLineColor(4);
  hMll_A[3]->SetFillColor(4); 
  
  hMll_A[4]->SetLineColor(3);
  hMll_A[4]->SetFillColor(3);
  
  hMll_A[10]->SetLineColor(2);
  hMll_A[10]->SetLineWidth(3);
  
  hMll_A[0]->Draw("e1"); hMll_A[2]->Draw("HISTsame"); hMll_A[3]->Draw("HISTsame"); hMll_A[4]->Draw("HISTsame"); hMll_A[0]->Draw("e1same"); 

  hMll_A[0]->GetYaxis()->SetRangeUser(0.1, 10000);
  c.Modified();
  
  TLegend leg_A(0.64, 0.74, 0.94, 0.94);
  leg_A.SetTextFont(42);
  leg_A.SetTextSize(0.034);
  sprintf(str1,"Data:%4.0f", hMll_A[0]->Integral()); 
  leg_A.AddEntry(hMll_A[0], str1, "EL");
  sprintf(str1,"Z+jets:%4.0f", (hMll_A[2]->Integral()) - (hMll_A[3]->Integral()));
  leg_A.AddEntry(hMll_A[2], str1,"F");
  sprintf(str1,"t#bar{t}:%4.0f", (hMll_A[3]->Integral()) - (hMll_A[4]->Integral()));
  leg_A.AddEntry(hMll_A[3], str1,"F");
  sprintf(str1,"Other:%4.0f", (hMll_A[4]->Integral()));
  leg_A.AddEntry(hMll_A[4], str1,"F");
  leg_A.Draw(); 
  label_Lumi(0.38, 0.85, Lum[0]);
  label_Prelim(0.38, 0.9);
  
  c.Print("M_ll_A_EMu.pdf");
  c.Print("M_ll_A_EMu.png");
  c.Print("M_ll_A_EMu.cxx");
  //=====================================================================
 
  hMll_B[1]->SetLineColor(1);
  hMll_B[1]->SetLineWidth(2); 

  hMll_B[2]->SetLineColor(5);
  hMll_B[2]->SetFillColor(5);
  
  hMll_B[3]->SetLineColor(4);
  hMll_B[3]->SetFillColor(4); 
  
  hMll_B[4]->SetLineColor(3);
  hMll_B[4]->SetFillColor(3);
 
  hMll_B[10]->SetLineColor(2);
  hMll_B[10]->SetLineWidth(3);
 
  hMll_B[1]->Draw("e1"); hMll_B[2]->Draw("HISTsame"); hMll_B[3]->Draw("HISTsame"); hMll_B[4]->Draw("HISTsame");  hMll_B[1]->Draw("e1same"); 

  hMll_B[1]->GetYaxis()->SetRangeUser(0.1, 10000);
  c.Modified();
  
  TLegend leg_B(0.64, 0.74, 0.94, 0.94);
  leg_B.SetTextFont(42);
  leg_B.SetTextSize(0.034);
  sprintf(str1,"Data:%4.0f", hMll_B[1]->Integral()); 
  leg_B.AddEntry(hMll_B[1], str1, "EL");
  sprintf(str1,"Z+jets:%4.0f", (hMll_B[2]->Integral()) - (hMll_B[3]->Integral()));
  leg_B.AddEntry(hMll_B[2], str1,"F");
  sprintf(str1,"t#bar{t}:%4.0f", (hMll_B[3]->Integral()) - (hMll_B[4]->Integral()));
  leg_B.AddEntry(hMll_B[3], str1,"F");
  sprintf(str1,"Other:%4.0f", (hMll_B[4]->Integral()));
  leg_B.AddEntry(hMll_B[4], str1,"F");
  leg_B.Draw(); 
  label_Lumi(0.38, 0.85, Lum[1]);
  label_Prelim(0.38, 0.9);
  
  c.Print("M_ll_B_EMu.pdf");
  c.Print("M_ll_B_EMu.png");
  c.Print("M_ll_B_EMu.cxx");
  //=====================================================================
  cout << "A "<< (hMll_A[0]->Integral())<< "  "<< (-hMll_A[4]->Integral()-hMll_A[2]->Integral())<<"  " <<(hMll_A[3]->Integral())<<endl;
  cout << "B "<< (hMll_B[1]->Integral()-hMll_B[4]->Integral())<<"   "<< (-hMll_B[2]->Integral())<< "  "<<(hMll_B[3]->Integral())<<endl;
  hMll_A[0]->Add(hMll_B[1]);
  hMll_A[2]->Add(hMll_B[2]);
  hMll_A[3]->Add(hMll_B[3]);
  hMll_A_2->Add(hMll_B_2);
  hMll_A_3->Add(hMll_B_3);
  hMll_A[4]->Add(hMll_B[4]);  
  cout << "A+B "<< (hMll_A[0]->Integral()-hMll_A[4]->Integral()-hMll_A[2]->Integral())/(hMll_A[3]->Integral())<<endl;
  
  
  hMll_A[0]->SetLineColor(1);
  hMll_A[0]->SetLineWidth(2); 

  hMll_A_2->SetLineColor(5);
  hMll_A_2->SetFillColor(5);
  
  hMll_A_3->SetLineColor(4);
  hMll_A_3->SetFillColor(4); 
  
  hMll_A[4]->SetLineColor(3);
  hMll_A[4]->SetFillColor(3);
  
  hMll_A[10]->SetLineColor(2);
  hMll_A[10]->SetLineWidth(3);
  
  hMll_A[0]->Draw("e1"); hMll_A_3->Draw("HISTsame"); hMll_A_2->Draw("HISTsame"); hMll_A[4]->Draw("HISTsame"); hMll_A[10]->Draw("HISTsame"); hMll_A[0]->Draw("e1same"); 

  hMll_A[0]->GetYaxis()->SetRangeUser(0.01, 1000);
  c.Modified();
  
  leg.Draw(); 
  label_Lumi(0.38, 0.85, Lum[0]+Lum[1]);
  label_Prelim(0.38, 0.9);
  
  c.Print("M_ll_EMu.pdf");
  c.Print("M_ll_EMu.png");
  c.Print("M_ll_EMu.cxx");
  
  //=====================================================================
  hMNu1_A[0]->Add(hMNu1_B[1]);
  hMNu1_A[2]->Add(hMNu1_B[2]);
  hMNu1_A[3]->Add(hMNu1_B[3]);
  hMNu1_A[4]->Add(hMNu1_B[4]);  
  hMNu1_A_2->Add(hMNu1_B_2);
  hMNu1_A_3->Add(hMNu1_B_3);
  
  hMNu1_A[0]->SetLineColor(1);
  hMNu1_A[0]->SetLineWidth(2); 

  hMNu1_A_2->SetLineColor(5);
  hMNu1_A_2->SetFillColor(5);
  
  hMNu1_A_3->SetLineColor(4);
  hMNu1_A_3->SetFillColor(4); 
  
  hMNu1_A[4]->SetLineColor(3);
  hMNu1_A[4]->SetFillColor(3);
  
  hMNu1_A[10]->SetLineColor(2);
  hMNu1_A[10]->SetLineWidth(3); 
 
  hMNu1_A[0]->Draw("e1"); hMNu1_A_3->Draw("HISTsame"); hMNu1_A_2->Draw("HISTsame"); hMNu1_A[4]->Draw("HISTsame"); hMNu1_A[10]->Draw("HISTsame"); hMNu1_A[0]->Draw("e1same"); 

  hMNu1_A[0]->GetYaxis()->SetRangeUser(0.01, 1000);
  c.Modified();
  
  leg.Draw(); 
  label_Lumi(0.38, 0.85, Lum[0]+Lum[1]);
  label_Prelim(0.38, 0.9);
  
  c.Print("M_Nu1_EMu.pdf");
  c.Print("M_Nu1_EMu.png");
  c.Print("M_Nu1_EMu.cxx");  
  
  //=====================================================================
  
  hMNu2_A[0]->Add(hMNu2_B[1]);
  hMNu2_A[2]->Add(hMNu2_B[2]);
  hMNu2_A[3]->Add(hMNu2_B[3]);
  hMNu2_A[4]->Add(hMNu2_B[4]); 
  
  hMNu2_A_2->Add(hMNu2_B_2);
  hMNu2_A_3->Add(hMNu2_B_3); 
  
  hMNu2_A[0]->SetLineColor(1);
  hMNu2_A[0]->SetLineWidth(2); 

  hMNu2_A_2->SetLineColor(5);
  hMNu2_A_2->SetFillColor(5);
  
  hMNu2_A_3->SetLineColor(4);
  hMNu2_A_3->SetFillColor(4); 
  
  hMNu2_A[4]->SetLineColor(3);
  hMNu2_A[4]->SetFillColor(3);
  
  hMNu2_A[10]->SetLineColor(2);
  hMNu2_A[10]->SetLineWidth(3);
 
  hMNu2_A[0]->Draw("e1"); hMNu2_A_3->Draw("HISTsame"); hMNu2_A_2->Draw("HISTsame"); hMNu2_A[4]->Draw("HISTsame"); hMNu2_A[10]->Draw("HISTsame");  hMNu2_A[0]->Draw("e1same"); 

  hMNu2_A[0]->GetYaxis()->SetRangeUser(0.01, 1000);
  c.Modified();
  
  leg.Draw(); 
  label_Lumi(0.38, 0.85, Lum[0]+Lum[1]);
  label_Prelim(0.38, 0.9);
  
  c.Print("M_Nu2_EMu.pdf");
  c.Print("M_Nu2_EMu.png");
  c.Print("M_Nu2_EMu.cxx"); 
  
    //=====================================================================
  
  hPU_A[0]->Add(hPU_B[1]);
  hPU_A[2]->Add(hPU_B[2]);
  hPU_A[3]->Add(hPU_B[3]);
  hPU_A[4]->Add(hPU_B[4]); 
  
  hPU_A[2]->Add(hPU_B_2);
  hPU_A[3]->Add(hPU_B_3); 
  
  hPU_A[0]->SetLineColor(1);
  hPU_A[0]->SetLineWidth(2); 

  hPU_A_2->SetLineColor(4);
  hPU_A_2->SetFillColor(4);
  
  hPU_A_3->SetLineColor(5);
  hPU_A_3->SetFillColor(5); 
  
  hPU_A[4]->SetLineColor(3);
  hPU_A[4]->SetFillColor(3);
  
  hPU_A[10]->SetLineColor(2);
  hPU_A[10]->SetLineWidth(3);
 
  hPU_A[0]->Draw("e1"); hPU_A_3->Draw("HISTsame"); hPU_A_2->Draw("HISTsame"); hPU_A[4]->Draw("HISTsame"); hPU_A[10]->Draw("HISTsame");  hPU_A[0]->Draw("e1same"); 

  hPU_A[0]->GetYaxis()->SetRangeUser(0.01, 1000);
  c.Modified();
  
  leg.Draw(); 
  label_Lumi(0.38, 0.85, Lum[0]+Lum[1]);
  label_Prelim(0.38, 0.9);
  
  c.Print("PU_EMu.pdf");
  c.Print("PU_EMu.png");
  c.Print("PU_EMu.cxx");   
  
 }


int main ( int , char ** )
{
EMu_Analis();
return 0;
}
