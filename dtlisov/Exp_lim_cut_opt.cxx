//root -b -q -l 'Exp_lim_cut_opt.cxx+ (0.5, 36, 1.)'
#include "TMath.h"
#include <iostream>
#include <fstream>
using namespace std;

float Exp_lim_cut_opt(float Eff, float Lum, float BG)
{

float x[21], y[21];
float ex_sigma_mean=0.;
float L_sb = 0.; 
float Int = 0.;
float Int_N = 0.;
float sigma = 0.0;
float N = 1000.;
int MAX_INT = 10000;

int INFORM = 1;


for (int ex_n = 0; ex_n <= 20; ++ex_n) {

sigma = 0.0;
Int_N = 0.0;
for ( int i_s = 0; i_s < MAX_INT; ++i_s) {
 L_sb= TMath::Poisson(ex_n, sigma*Eff*Lum + BG);
 Int_N = Int_N + L_sb; 
 sigma = sigma + 1/N ;
}
  if (INFORM == 1) {     
	cout << "Int_N = " << Int_N << " Sigma = " << sigma <<endl;	
    cout << endl;  
  }  
 
sigma = 0.0;
Int = 0.0;
while (Int < 0.95) {
 L_sb= TMath::Poisson(ex_n, sigma*Eff*Lum + BG);
 Int = Int + L_sb/Int_N; 
 sigma = sigma + 1/N ;
}

x[ex_n] = sigma;
y[ex_n] = TMath::Poisson(ex_n, BG);
ex_sigma_mean = ex_sigma_mean + sigma*y[ex_n];

  if (INFORM == 1) {     
    cout <<"ex_n = "<< ex_n <<" CL = " << Int <<" Byesian upper number of signal events limit = " << x[ex_n] <<" prob = "<<y[ex_n]<<endl;
    cout << endl;  
  }
}  

return ex_sigma_mean;

}