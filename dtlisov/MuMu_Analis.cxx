//To run this script on pcrfcms14.cern.ch machine: 
//root -b -q -l MuMu_Analis-MUMU.cxx+
#include "TH1.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TROOT.h"
#include <TLorentzVector.h>
#include <TMath.h>
#include <TLatex.h>
#include "TStyle.h"
#include "TLegend.h"
#include "TText.h"
#include "TAxis.h"

#include "tdrstyle.C"
//#include "heavynuStandard.C"
#include "heavynuStandard11.C"

#include "../PATDump/src/format.h"
#include <iostream>
#include <fstream>
using namespace std;

vector<double> generate_flat10_weights(TH1D* data_npu_estimated){
    // see SimGeneral/MixingModule/python/mix_E7TeV_FlatDist10_2011EarlyData_inTimeOnly_cfi.py; copy and paste from there:
    const double npu_probs[25] = {0.0698146584, 0.0698146584, 0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,
           0.0630151648,0.0526654164,0.0402754482,0.0292988928,0.0194384503,0.0122016783,0.007207042,0.004003637,0.0020278322,
           0.0010739954,0.0004595759,0.0002229748,0.0001028162,4.58337152809607E-05 /* <-- 24 */};
    vector<double> result(25);
    double s = 0.0;
    for(int npu=0; npu<25; ++npu){
        double npu_estimated = data_npu_estimated->GetBinContent(data_npu_estimated->GetXaxis()->FindBin(npu));                              
        result[npu] = npu_estimated / npu_probs[npu];
        s += npu_estimated;
    }
    // normalize weights such that the total sum of weights over thw whole sample is 1.0, i.e., sum_i  result[i] * npu_probs[i] should be 1.0 (!)
    for(int npu=0; npu<25; ++npu){
        result[npu] /= s;
    }
    return result;
}

void MuMu_Analis()
{
// set plain style
  gROOT->SetStyle("Plain");
  
    TCanvas c1("OUTPUT1","OUTPUT1");
 
  setTDRStyle();
  fixOverlay(); 

  
  // activate storage of sum of squares of errors
  // to calculate correctly errors in histograms
  // with non-unit entries
  TH1::SetDefaultSumw2(kTRUE);
  
  TFile* f = new TFile("204pb_10May_Rereco.root");
  //TFile* f = new TFile("36pb_21April_Rereco.root");
  TH1D* histo = (TH1D*) f->Get("pileup");
  
// Cuts for event selection
  unsigned int NofLept = 2;
  unsigned int NofJets = 2;
  float Lep1Pt = 60.;
  float Lep2Pt = 30.;
  float Jet1Pt = 40.;
  float Jet2Pt = 40.;
  float EtaCut1L = 1.44; //1.44
  float EtaCut2L = EtaCut1L;
  float EtaCut1J = 2.5;
  float EtaCut2J = 2.5; 
  float ISO = 1.;
  float Vertex = 10.03;
  float MllCut = 70.0;
  float MllCut1 = 110.0;
  float MW_Cut = 0.0; 
 
// M_W histogramm parameters  
  int M_Delta_M_W = 40;
  int M_min_M_W = 0;
  int M_max_M_W = 2000; 
 
// M_Nu histogramm parameters  
  int M_Delta_M_Nu = 30;
  int M_min_M_Nu = 0;
  int M_max_M_Nu = 1500;  
 
// Mll histogramm parameters  
  int M_Delta_Mll = 10;
  int M_min_Mll = 70;
  int M_max_Mll = 110;
  
 // MET histogramm parameters  
  int M_Delta_MET = 20;
  int M_min_MET = 0;
  int M_max_MET = 200;
  
// Samples switcher: true = include samples in analisis
  bool RD_incl = true;
  bool S_incl = true;
  bool QCD_incl = true;
  bool TTb_incl = true;
  bool ZJ_incl = true;
  bool WJ_incl = true;
  bool ZZ_incl = true;
  bool ZW_incl = true;
  bool WW_incl = true;
  bool TW_incl = true;

  const char* fname;
  char *str1 = (char*)alloca(10000);

  float WR, tWR, lWR, eWR, pWR; 
  float NuR_l0, NuR_l1, Mll, Mjj, Mmet; 
  float K1 = 1.;
  float K2 = 1.;
  float K1_Z1 = 1.;
  float K1_Z2 = 1.;
  float HEEP_B=1.;
  float HEEP_E=1.;
  float A_Wei=1.;
  float Z_tune = 1.0;
  //float Z_tune = 1.0;
  float QCD_tune = 1.0;
  //float QCD_tune = 1.;
  float TTb_tune = 1.11;
  //float TTb_tune = 1.0;
  float ZZ_tune = 1.;
  float ZW_tune = 1.;
  float WW_tune = 1.;
  float TW_tune = 1.;
  float S_tune = 1.;
  float WJ_tune = 1.;
  float BG_nw = 0.; // number of total BG events
  float delta_BG_nw_sq = 0. ; // Squared absolut error for number of total BG events

// ================================================================================================== 

// REAL DATA 

  // histograms
	
  TH1F RD_hWR(  "WR","Z+jets ;M(#mu#mujj), GeV;Events/50 GeV", M_Delta_M_W, M_min_M_W, M_max_M_W);
  TH1F RD_hNuR0("RD_NuR0", "#nu_{R} reconstructed mass using first electron;M(#mu_{1}jj), GeV;Events/100 GeV", M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu);
  TH1F RD_hNuR1("RD_NuR1", "#nu_{R} reconstructed mass using second electron;M(#mu_{2}jj), GeV;Events/100 GeV", M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu);
  TH1F RD_hMll("RD_Mll", "Z+jets; M(#mu#mu), GeV;Events/4 GeV", M_Delta_Mll, M_min_Mll, M_max_Mll);
  TH1F RD_hMll1("RD_Mll1", "Lepton-lepton invariant mass;Reconstructed Dielectron mass, GeV;Events/50 GeV", 20, 100, 1000);
  TH1F RD_hMjj("RD_Mjj", "Jet-jet pair invariant mass;mass, GeV;Events/100 GeV", M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu);
  TH1F RD_hMmets("RD_Mmets", "Miss E_{T} ; Reconstructed miss E_{T}, GeV;Events/10 GeV", M_Delta_MET, M_min_MET, M_max_MET);
  TH1F RD_hMmets1("RD_Mmets1", "Miss Et ; Et, GeV;Events/30 GeV", 30, 100, 1000);
  TH1F RD_PtWR(  "RD_PtWR", "W_{R} Pt; Pt, GeV; Events/6GeV", 30, 0, 180);
  TH1F RD_PlWR(  "RD_PlWR", "W_{R} Pl;Pl, GeV;Events/50 GeV", 60, -1500, 1500);
  TH1F RD_EtaWR( "RD_EtaWR", "W_{R} #eta; Eta; Events", 10, -6, 6);
  TH1F RD_PhiWR( "RD_PhiWR", "W_{R} #phi; Phi; Events", 10, -3.1415, 3.1415); 
  TH1F RD_hbDiscr_j1( "RD_bDiscrJ1", "First jet b Discriminant; bDiscr; Events", 15, 0, 1.5); 
  TH1F RD_hbDiscr_j2( "RD_bDiscrJ2", "Second jet b Discriminant; bDiscr; Events", 15, 0, 1.5); 
  TH1F RD_hbNum( "RD_hbNum", "Number of b jets ; Number of b jets; Events", 3, 0, 3); 
  TH1F RD_hQNum( "RD_hQNum", "Product of lepton charges ; Product of lepton charges; Events", 2, -1, 1); 
  TH1F RD_hElMult("RD_ElMult", "Electrons multiplicity", 10, 0, 10);
       RD_hElMult.GetXaxis()->SetNdivisions(110);
  TH1F RD_hJetMult("RD_JetMult", "Jets multiplicity", 20, 0, 20);
       RD_hJetMult.GetXaxis()->SetNdivisions(210);
	   
  TH1F RD_dVz( "RD_dVz", "delta Vz; #delta V_{z} between e_{1}e_{2}, cm; Events", 20, 0, 0.1); 
  TH1F RD_dVz1( "RD_dVz1", "delta Vz;#delta V_{z} between j_{1}j_{2}, cm; Events", 20, 0, 0.1);
  TH1F RD_dVz2( "RD_dVz2", "delta Vz;#delta V_{z} between e_{1}j_{1}, cm; Events", 20, 0, 0.1);  
  TH1F RD_dVz3( "RD_dVz3", "delta Vz;#delta V_{z} between e_{2}j_{2}, cm; Events", 20, 0, 0.1); 
  TH1F RD_dVz4( "RD_dVz4", "delta Vz;#delta V_{z} between e_{1}j_{2}, cm; Events", 20, 0, 0.1);  
  TH1F RD_dVz5( "RD_dVz5", "delta Vz;#delta V_{z} between e_{2}j_{1}, cm; Events", 20, 0, 0.1);  

  TH1F RD_PU( "RD_PU", "PU; Primary vertex number ; Events", 25, 0, 25); 
  TH1F RD_hZPt("RD_ZPt", "Z+jets; Z P_{t}, GeV/c;Events/20 GeV/c", 10, 0, 200);  
  
	   
     fname = "/moscow41/heavynu/Data/SingleMu10MayRereco_204pb.root";
   
  // fname = "/moscow41/heavynu/Data/Mu21AprilRereco_36pb.root";

  TheEvent RD_e(fname);
  if (!RD_e) return;
  
  float RD_Lum = RD_e.sample.lumi; //Luminocity in pb^-1
 
  
//counters 
  int RD_n = 0;
  
if (RD_incl == true) {

//counters 
  int RD_nB1 = 0; 
  int RD_nB2 = 0;  
  int RD_nB3 = 0;  
  int RD_nEvents = 0; 
  int RD_nQ = 0;
  int RD_nQ1 = 0;
  int RD_nQ3 = 0;
  int RD_nQ4 = 0;
  
  RD_nEvents = RD_e.totalEvents();
 
  for (int i = 0; i < RD_nEvents; ++i) {
    RD_e.readEvent(i);
  
    // Cuts on number of leptons and jets in event
      if ((RD_e.muons.size() >= NofLept) && (RD_e.jets.size() >= NofJets)) {
	  
	// correction of Z mass
	//  if ((abs(RD_e.muons[0].p4.Eta())<=1.5)) {K1 = 90.2/89.6;} else {K1=90.2/85.9;}
	//  if ((abs(RD_e.muons[1].p4.Eta())<=1.5)) {K2 = 90.2/89.6;} else {K2=90.2/85.9;}
	
	//cuts on electrons Pt
	    if (((K1*RD_e.muons[0].p4).Pt()>=Lep1Pt) && ((K2*RD_e.muons[1].p4).Pt()>=Lep2Pt))  { 
		
    //cuts on jets Pt      
	      if (((RD_e.jets[0].p4).Pt()>=Jet1Pt) && ((RD_e.jets[1].p4).Pt()>=Jet2Pt))  { 
		  
	//cuts on electrons invariant mass Mll	
		  if (((K1*RD_e.muons[0].p4 + K2*RD_e.muons[1].p4).M()>=MllCut)&&((K1*RD_e.muons[0].p4 + K2*RD_e.muons[1].p4).M()<=MllCut1)) {
//		  if (!(((K1*RD_e.muons[0].p4 + K2*RD_e.muons[1].p4).M()>=MllCut)&&((K1*RD_e.muons[0].p4 + K2*RD_e.muons[1].p4).M()<=MllCut1))) {
		  
	//cuts on Eta  
         if (((abs(K1*RD_e.muons[0].p4.Eta())<=EtaCut1L) || (abs(K2*RD_e.muons[1].p4.Eta())<=EtaCut2L)) 
	     && (abs(RD_e.jets[0].p4.Eta())<=EtaCut1J) && (abs(RD_e.jets[1].p4.Eta())<=EtaCut2J) )        {
			
	// Isolation cut
	       if ((RD_e.muons[0].isoHEEP() <= ISO)&&(RD_e.muons[1].isoHEEP() <= ISO)) {
		
	 //Cut on vertexes
            if ((abs(RD_e.muons[0].vertex.z()-RD_e.muons[1].vertex.z())<=Vertex) && (abs(RD_e.jets[0].vertex.z()-RD_e.jets[1].vertex.z())<=Vertex) 
             && ((abs(RD_e.muons[0].vertex.z()-RD_e.jets[1].vertex.z())<=Vertex) && (abs(RD_e.jets[0].vertex.z()-RD_e.muons[1].vertex.z())<=Vertex))) {
	
	// Filling the histos
            WR = (K1*RD_e.muons[0].p4 + K2*RD_e.muons[1].p4 + RD_e.jets[0].p4 + RD_e.jets[1].p4).M();
			if (WR>=MW_Cut) {
            RD_hWR.Fill(WR, RD_e.weight);
	  
	        tWR = (K1*RD_e.muons[0].p4 + K2*RD_e.muons[1].p4 + RD_e.jets[0].p4 + RD_e.jets[1].p4).Pt();
            RD_PtWR.Fill(tWR, RD_e.weight);
	  
	        lWR = (K1*RD_e.muons[0].p4 + K2*RD_e.muons[1].p4 + RD_e.jets[0].p4 + RD_e.jets[1].p4).Pz();
            RD_PlWR.Fill(lWR, RD_e.weight);
	  
	        eWR = (K1*RD_e.muons[0].p4 + K2*RD_e.muons[1].p4 + RD_e.jets[0].p4 + RD_e.jets[1].p4).Eta();
            RD_EtaWR.Fill(eWR, RD_e.weight);
	  
	        pWR = (K1*RD_e.muons[0].p4 + K2*RD_e.muons[1].p4 + RD_e.jets[0].p4 + RD_e.jets[1].p4).Phi();
            RD_PhiWR.Fill(pWR, RD_e.weight);
	  
            NuR_l0 = (K1*RD_e.muons[0].p4 + RD_e.jets[0].p4 + RD_e.jets[1].p4).M();
            NuR_l1 = (K2*RD_e.muons[1].p4 + RD_e.jets[0].p4 + RD_e.jets[1].p4).M();
      
	        RD_hNuR0.Fill(NuR_l0, RD_e.weight);
	        RD_hNuR1.Fill(NuR_l1, RD_e.weight);
	        
            Mll = (K1*RD_e.muons[0].p4 + K2*RD_e.muons[1].p4).M();
            RD_hMll.Fill(Mll, RD_e.weight);
            RD_hMll1.Fill(Mll, RD_e.weight);
	  
            Mjj = (RD_e.jets[0].p4 + RD_e.jets[1].p4).M();
            RD_hMjj.Fill(Mjj, RD_e.weight);
			
			Mmet = (RD_e.mets[0].p4).Pt();
            RD_hMmets.Fill(Mmet, RD_e.weight);
			RD_hMmets1.Fill(Mmet, RD_e.weight);
			
			RD_hbDiscr_j1.Fill(RD_e.jets[0].bDiscr, RD_e.weight);
			RD_hbDiscr_j2.Fill(RD_e.jets[1].bDiscr, RD_e.weight);
			
			
				
			RD_n = RD_n + 1;
						
			if ((RD_e.jets[0].bDiscr >= 0.669) && (RD_e.jets[1].bDiscr >= 0.669)) { //  10% = 0.215      1% = 0.459    0.1% = 0.669
			RD_nB2 = RD_nB2 + 1;
			RD_hbNum.Fill(2., RD_e.weight);
			}
			if (((RD_e.jets[0].bDiscr >= 0.669) && (RD_e.jets[1].bDiscr <= 0.669)) || ((RD_e.jets[0].bDiscr <= 0.669) && (RD_e.jets[1].bDiscr >= 0.669)) ) { //  10% = 0.215      1% = 0.459    0.1% = 0.669
			RD_nB1 = RD_nB1 + 1;
			RD_hbNum.Fill(1., RD_e.weight);
			}
			if ((RD_e.jets[0].bDiscr <= 0.669) && (RD_e.jets[1].bDiscr <= 0.669)) { 
			RD_nB3 = RD_nB3 + 1;
			RD_hbNum.Fill(0., RD_e.weight);
			}
			
			if (RD_e.muons[0].charge*RD_e.muons[1].charge == -1) { 
			RD_nQ = RD_nQ +1; 
			RD_hQNum.Fill(-0.5, RD_e.weight);}
			else {RD_nQ1 = RD_nQ1 +1;
			RD_hQNum.Fill(0.5, RD_e.weight);}
			
			if ((RD_e.muons[0].charge==-1)&&(RD_e.muons[1].charge == -1)) { 
			RD_nQ3 = RD_nQ3 +1;}
			
			if ((RD_e.muons[0].charge==1)&&(RD_e.muons[1].charge == 1)) { 
			RD_nQ4 = RD_nQ4 +1;}
						
			RD_dVz.Fill((abs(RD_e.muons[0].vertex.z()-RD_e.muons[1].vertex.z())), RD_e.weight);
			RD_dVz1.Fill(abs(RD_e.jets[0].vertex.z()-RD_e.jets[1].vertex.z()), RD_e.weight);
			RD_dVz2.Fill(abs(RD_e.jets[0].vertex.z()-RD_e.muons[0].vertex.z()), RD_e.weight);
			RD_dVz3.Fill(abs(RD_e.jets[1].vertex.z()-RD_e.muons[1].vertex.z()), RD_e.weight);
			RD_dVz4.Fill(abs(RD_e.jets[1].vertex.z()-RD_e.muons[0].vertex.z()), RD_e.weight);
			RD_dVz5.Fill(abs(RD_e.jets[0].vertex.z()-RD_e.muons[1].vertex.z()), RD_e.weight);
			
			RD_PU.Fill(RD_e.vertices.size(), RD_e.weight);
			RD_hZPt.Fill((K1*RD_e.muons[0].p4 + K2*RD_e.muons[1].p4).Pt(), RD_e.weight);
				
/*			
			cout<<"New event!" << RD_n <<" PtL1 = " <<(K1*RD_e.muons[0].p4).Pt()<<" PtL2 = "<<(K2*RD_e.muons[1].p4).Pt() << " Mll = "<<(K1*RD_e.muons[0].p4 + K2*RD_e.muons[1].p4).M() << endl;
			cout<<" PtJ1 = "<<(RD_e.jets[0].p4).Pt()<<" bDiscr J1 = "<< RD_e.jets[0].bDiscr <<" PtJ2 = "<<(RD_e.jets[1].p4).Pt()<<" bDiscr J2 = "<< RD_e.jets[1].bDiscr <<endl;
			cout<<"EtaL1 = "<<(K1*RD_e.muons[0].p4).Eta()<<" PhiL1 = "<<(K1*RD_e.muons[0].p4).Phi()<<" EtaL2 = "<<(K2*RD_e.muons[1].p4).Eta()<<" PhiL2 = "<<(K2*RD_e.muons[1].p4).Phi() << endl;
			cout<<"EtaJ1 = "<<(RD_e.jets[0].p4).Eta()<<" PhiJ1 = "<<(RD_e.jets[0].p4).Phi()<<" EtaJ2 = "<<(RD_e.jets[1].p4).Eta()<<" PhiJ2 = "<<(RD_e.jets[1].p4).Phi() << endl;			
			
	        cout<<"vzL1 = "<<(RD_e.muons[0].vertex).z()<<" VzL2 = "<<(RD_e.muons[1].vertex).z()<<" VzJ1 = "<<(RD_e.jets[0].vertex).z()<<" VzJ2 = "<<(RD_e.jets[1].vertex).z() << endl;
		
	    	cout<<" L1: Iso = " << RD_e.muons[0].isoHEEP() << endl; 
					
			cout<<" L1: charge = " <<RD_e.muons[0].charge << endl; 
	
	        cout<<" L1: Iso = " << RD_e.muons[1].isoHEEP() << endl; 
			
			cout<<" L2: charge = " <<RD_e.muons[1].charge << endl; 
									
			cout<<"MWR = "<< WR <<" MNuR = "<< NuR_l1 <<" Mjj = "<< Mjj << endl;
			
			cout<<"PtWR = "<< tWR <<" PlWR = "<< lWR << endl;
			
			cout <<"RUN = "<< RD_e.irun << " LUMI = " << RD_e.ilumi << " EVENT = "<< RD_e.ievent<< endl;
		    
			cout << "Number of events with 0 b-jet = " << RD_nB3 << endl; 
			cout << "Number of events with 1 b-jet = " << RD_nB1 << endl;
			cout << "Number of events with 2 b-jet = " << RD_nB2 << endl;	
            cout << endl;	
            cout << "Number of events with same charges of lepton = " << RD_nQ1 << endl;
			cout << "Number of events with opposite charges of lepton = " << RD_nQ << endl; 
*/        
		}
		}	
		}	
        } 	
        }
	   }
	  }
	 }    
   // Multiplicity
   RD_hElMult.Fill(RD_e.muons.size());
   RD_hJetMult.Fill(RD_e.jets.size());	    

  }
  
 cout << "REAL DATA" << endl;
 cout << endl;
 cout << "Total events = " << RD_nEvents << endl;
 cout << "Total weight events = " << RD_nEvents*RD_e.weight << endl;
 cout << endl; 
 cout << "Number of passed events = " <<RD_n<<"   "<<RD_n*RD_e.weight<< endl;
 cout << endl;
 cout << "Number of events with 0 b-jet = " << RD_nB3<<"   " <<RD_nB3*RD_e.weight << endl; 
 cout << "Number of events with 1 b-jet = " << RD_nB1<<"   " <<RD_nB1*RD_e.weight << endl;
 cout << "Number of events with 2 b-jet = " << RD_nB2<<"   " <<RD_nB2*RD_e.weight << endl;	
 cout << endl;	
 cout << "Number of events with same charges of lepton = " << RD_nQ1<<"   " <<RD_nQ1*RD_e.weight << endl;
 cout << "Number of events with opposite charges of lepton = " << RD_nQ <<"   " <<RD_nQ*RD_e.weight << endl; 
 cout << "Number of events with -1 charges of lepton = " << RD_nQ3 <<"   " <<RD_nQ3*RD_e.weight << endl; 
 cout << "Number of events with +1 charges of lepton = " << RD_nQ4 <<"   " <<RD_nQ4*RD_e.weight << endl; 
 cout << endl;
 
 
} 
 
//============================================================================================================================
 
   
// MC SIGNAL 
  
  // histograms
  

  TH1F S_hWR(  "S_WR", "W_{R} reconstructed mass;mass, GeV/c^{2};events/50 GeV", M_Delta_M_W, M_min_M_W, M_max_M_W);
  TH1F S_hNuR0("S_NuR0", "nu_{R} reconstructed mass using first E electron;mass, GeV/c^{2};events/50 GeV", M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu);
  TH1F S_hNuR1("S_NuR1", "nu_{R} reconstructed mass using second E electron;mass, GeV/c^{2};events/50 GeV", M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu); 
  TH1F S_hMll("S_Mll", "lepton-lepton pair invariant mass;mass, GeV/c^{2};events/2 GeV", M_Delta_Mll, M_min_Mll, M_max_Mll);
  TH1F S_hMll1("S_Mll1", "lepton-lepton pair invariant mass;mass, GeV/c^{2};events/40 GeV", 20, 100, 1000);
  TH1F S_hMjj("S_Mjj", "jet-jet pair invariant mass;mass, GeV/c^{2};events/50 GeV", M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu);
  TH1F S_hMmets("S_Mmets", "Miss Et; Et, GeV/c^{2};events/10 GeV", M_Delta_MET, M_min_MET, M_max_MET);
  TH1F S_hMmets1("S_Mmets1", "Miss Et; Et, GeV/c^{2};events/30 GeV", 30, 100, 1000); 
  TH1F S_PtWR(  "S_PtWR", "W_{R} Pt; Pt, GeV/c^{2}; events/6GeV", 30, 0, 180);
  TH1F S_PlWR(  "S_PlWR", "W_{R} Pl;Pl, GeV/c^{2};events/50 GeV", 60, -1500, 1500);
  TH1F S_EtaWR( "S_EtaWR", "W_{R} Eta; Eta; events", 10, -6, 6);
  TH1F S_PhiWR( "S_PhiWR", "W_{R} Phi; Phi; events", 10, -3.1415, 3.1415); 
  TH1F S_hbDiscr_j1( "S_bDiscrJ1", "first jet b Discriminant; bDiscr; events", 15, 0, 1.5); 
  TH1F S_hbDiscr_j2( "S_bDiscrJ2", "second jet b Discriminant; bDiscr; events", 15, 0, 1.5); 
  TH1F S_hbNum( "S_hbNum", "Number of b jets ; bNum; events", 3, 0, 3); 
  TH1F S_hQNum( "S_hQNum", "Mult of leptons charges ; e_1*e_2; events", 2, -1, 1); 
  TH1F S_hElMult("S_ElMult", "electrons multiplicity", 10, 0, 10);
       S_hElMult.GetXaxis()->SetNdivisions(110);
  TH1F S_hJetMult("S_JetMult", "jets multiplicity", 20, 0, 20);
       S_hJetMult.GetXaxis()->SetNdivisions(210);   
  TH1F S_hElRes("S_ElRes", "electron energy reconstruction resolution", 100, -0.3, 0.3);
  
  TH1F S_dVz( "S_dVz", "delta Vz; #delta V_{z} between ee, cm; Events", 20, 0, 0.1); 
  TH1F S_dVz1( "S_dVz1", "delta Vz;#delta V_{z} between jj, cm; Events", 20, 0, 0.1);
  TH1F S_dVz2( "S_dVz2", "delta Vz;#delta V_{z} between e_{1}j_{1}, cm; Events", 20, 0, 0.1);  
  TH1F S_dVz3( "S_dVz3", "delta Vz;#delta V_{z} between e_{2}j_{2}, cm; Events", 20, 0, 0.1); 
  TH1F S_dVz4( "S_dVz4", "delta Vz;#delta V_{z} between e_{2}j_{1}, cm; Events", 20, 0, 0.1);  
  TH1F S_dVz5( "S_dVz5", "delta Vz;#delta V_{z} between e_{1}j_{2}, cm; Events", 20, 0, 0.1); 
  
  TH1F S_PU( "S_PU", "PU; Primary vertex number ; Events", 25, 0, 25); 
  TH1F S_PU1( "S_PU1", "PU1; Primary vertex number ; Events", 25, 0, 25); 
  TH1F S_hZPt("S_ZPt", "Z+jets; Z Pt, GeV/c;Events/20 GeV/c", 10, 0, 200);
    	
  fname = "/moscow41/heavynu/41x-signal/41x-7TeV-START311_V2-WR1100_nuRe400.root";


 
  TheEvent S_e(fname);
  if (!S_e) return;
  int S_n = 0; 
  float S_nw = 0.;  
  
if (S_incl == true) {

  int S_nB1 = 0; 
  float S_nwB1 = 0;
  int S_nB2 = 0;
  float S_nwB2 = 0;  
  int S_nB3 = 0;
  float S_nwB3 = 0;
  int S_nEvents = 0; 
  float S_nwEvents = 0;  
  int S_nQ = 0;
  float S_nwQ = 0;
  int S_nQ1 = 0;
  float S_nwQ1 = 0;
  int S_nQ3 = 0;
  float S_nwQ3 = 0;
  int S_nQ4 = 0;
  float S_nwQ4 = 0;
  float S_weight =0;
  

  S_nEvents = S_e.totalEvents();
 
  for (int i = 0; i < S_nEvents; ++i) {
    S_e.readEvent(i);  
     
      if(S_e.pileup.size()<25){
        S_weight = S_e.weight *  generate_flat10_weights(histo)[S_e.pileup.size()];
        }
        else{ //should not happen as we have a weight for all simulated S_e.pileup.size() multiplicities!
        S_weight = 0.;
        }
	  
		S_nwEvents = S_nwEvents + S_tune*S_weight*RD_Lum/100.;  
   
    // electron energy resolution
    for (unsigned int j = 0; j < S_e.muons.size(); ++j) {
      const Particle& reco = S_e.muons[j];
      if (reco.mcId >= 0) {
        const Particle& mc = S_e.gens[reco.mcId];
        const float r = (reco.p4.E() - mc.p4.E())/mc.p4.E();
        S_hElRes.Fill(r, S_tune*S_weight*A_Wei*RD_Lum/100.);
      }
    }
    
    // mass distributions
      if ((S_e.muons.size() >= NofLept) && (S_e.jets.size() >= NofJets)) {
	  
    //  correction of Z mass and ID efficiency
	   if ((abs(S_e.muons[0].p4.Eta())<=1.44)) {K1_Z1 = 1.;} else {K1_Z1=1.;}
	   if ((abs(K1_Z2*S_e.muons[1].p4.Eta())<=1.44)) {K1_Z2 = 1.;} else {K1_Z2=1.;}
	  
	   if (abs((K1_Z1*S_e.muons[0].p4).Eta())<=1.44){A_Wei=HEEP_B;} else {A_Wei=HEEP_E;}
	   if (abs((K1_Z2*S_e.muons[1].p4).Eta())<=1.44){A_Wei=A_Wei*HEEP_B;} else {A_Wei=A_Wei*HEEP_E;}
	
	//cuts on electrons Pt>=40
	    if (((K1_Z1*S_e.muons[0].p4).Pt()>=Lep1Pt) && ((K1_Z2*S_e.muons[1].p4).Pt()>=Lep2Pt))  { 
    //cuts on jetss Pt>=40      
	      if (((S_e.jets[0].p4).Pt()>=Jet1Pt) && ((S_e.jets[1].p4).Pt()>=Jet2Pt))  { 
		  
	//cuts on electrons invariant mass Mll>=400.0	
	  if (((K1_Z1*S_e.muons[0].p4 + K1_Z2*S_e.muons[1].p4).M()>=MllCut)&& ((K1_Z1*S_e.muons[0].p4 + K1_Z2*S_e.muons[1].p4).M()<=MllCut1)) {
//	  if (!(((K1_Z1*S_e.muons[0].p4 + K1_Z2*S_e.muons[1].p4).M()>=MllCut)&& ((K1_Z1*S_e.muons[0].p4 + K1_Z2*S_e.muons[1].p4).M()<=MllCut1))) {
			  
	//cuts on Eta  
         if (((abs(K1_Z1*S_e.muons[0].p4.Eta())<=EtaCut1L) || (abs(K1_Z2*S_e.muons[1].p4.Eta())<=EtaCut2L)) 
	     && (abs(S_e.jets[0].p4.Eta())<=EtaCut1J) && (abs(S_e.jets[1].p4.Eta())<=EtaCut2J) )        {
			
	// Isolation
	       if ((S_e.muons[0].isoHEEP() <= ISO)&&(S_e.muons[1].isoHEEP() <= ISO)) {
		   
	 //Cut on vertexes
             if ((abs(S_e.muons[0].vertex.z()-S_e.muons[1].vertex.z())<=Vertex) && (abs(S_e.jets[0].vertex.z()-S_e.jets[1].vertex.z())<=Vertex) 
             && ((abs(S_e.muons[0].vertex.z()-S_e.jets[1].vertex.z())<=Vertex) && (abs(S_e.jets[0].vertex.z()-S_e.muons[1].vertex.z())<=Vertex))) {
			 
	        WR = (K1_Z1*S_e.muons[0].p4 + K1_Z2*S_e.muons[1].p4 + S_e.jets[0].p4 + S_e.jets[1].p4).M();
			if (WR>=MW_Cut) {
            S_hWR.Fill(WR, S_tune*S_weight*A_Wei*RD_Lum/100.);
	  
	        tWR = (K1_Z1*S_e.muons[0].p4 + K1_Z2*S_e.muons[1].p4 + S_e.jets[0].p4 + S_e.jets[1].p4).Pt();
            S_PtWR.Fill(tWR, S_tune*S_weight*A_Wei*RD_Lum/100.);
	  
	        lWR = (K1_Z1*S_e.muons[0].p4 + K1_Z2*S_e.muons[1].p4 + S_e.jets[0].p4 + S_e.jets[1].p4).Pz();
            S_PlWR.Fill(lWR, S_tune*S_weight*A_Wei*RD_Lum/100.);
	  
	        eWR = (K1_Z1*S_e.muons[0].p4 + K1_Z2*S_e.muons[1].p4 + S_e.jets[0].p4 + S_e.jets[1].p4).Eta();
            S_EtaWR.Fill(eWR, S_tune*S_weight*A_Wei*RD_Lum/100.);
	  
	        pWR = (K1_Z1*S_e.muons[0].p4 + K1_Z2*S_e.muons[1].p4 + S_e.jets[0].p4 + S_e.jets[1].p4).Phi();
            S_PhiWR.Fill(pWR, S_tune*S_weight*A_Wei*RD_Lum/100.);
	  
            NuR_l0 = (K1_Z1*S_e.muons[0].p4 + S_e.jets[0].p4 + S_e.jets[1].p4).M();
            NuR_l1 = (K1_Z2*S_e.muons[1].p4 + S_e.jets[0].p4 + S_e.jets[1].p4).M();
      
	        S_hNuR0.Fill(NuR_l0, S_tune*S_weight*A_Wei*RD_Lum/100.);
	        S_hNuR1.Fill(NuR_l1, S_tune*S_weight*A_Wei*RD_Lum/100.);
	        
            Mll = (K1_Z1*S_e.muons[0].p4 + K1_Z2*S_e.muons[1].p4).M();
            S_hMll.Fill(Mll, S_tune*S_weight*A_Wei*RD_Lum/100.);
			S_hMll1.Fill(Mll, S_tune*S_weight*A_Wei*RD_Lum/100.);
      
            Mjj = (S_e.jets[0].p4 + S_e.jets[1].p4).M();
            S_hMjj.Fill(Mjj, S_tune*S_weight*A_Wei*RD_Lum/100.);
			
			Mmet = (S_e.mets[0].p4).Pt();
            S_hMmets.Fill(Mmet, S_tune*S_weight*A_Wei*RD_Lum/100.);
		    S_hMmets1.Fill(Mmet, S_tune*S_weight*A_Wei*RD_Lum/100.);
			
			S_hbDiscr_j1.Fill(S_e.jets[0].bDiscr, S_tune*S_weight*A_Wei*RD_Lum/100.);
			S_hbDiscr_j2.Fill(S_e.jets[1].bDiscr, S_tune*S_weight*A_Wei*RD_Lum/100.);
							
			S_n = S_n + 1;
			S_nw = S_nw + S_tune*S_weight*A_Wei*RD_Lum/100.; 
						
			if ((S_e.jets[0].bDiscr >= 0.669) && (S_e.jets[1].bDiscr >= 0.669)) { //  10% = 0.215      1% = 0.459    0.1% = 0.669
			S_nwB2 = S_nwB2 + S_tune*S_weight*A_Wei*RD_Lum/100.; 
			S_nB2 = S_nB2 + 1;
			S_hbNum.Fill(2., S_tune*S_weight*A_Wei*RD_Lum/100.);
			}
			if (((S_e.jets[0].bDiscr >= 0.669) && (S_e.jets[1].bDiscr <= 0.669)) || ((S_e.jets[0].bDiscr <= 0.669) && (S_e.jets[1].bDiscr >= 0.669)) ) { //  10% = 0.215      1% = 0.459    0.1% = 0.669
			S_nwB1 = S_nwB1 + S_tune*S_weight*A_Wei*RD_Lum/100.; 
			S_nB1 = S_nB1 + 1;
			S_hbNum.Fill(1., S_tune*S_weight*A_Wei*RD_Lum/100.);
			}
			if ((S_e.jets[0].bDiscr <= 0.669) && (S_e.jets[1].bDiscr <= 0.669)) {
            S_nwB3 = S_nwB3 + S_tune*S_weight*A_Wei*RD_Lum/100.; 			
			S_nB3 = S_nB3 + 1;
			S_hbNum.Fill(0., S_tune*S_weight*A_Wei*RD_Lum/100.);
			}
			
			if (S_e.muons[0].charge*S_e.muons[1].charge == -1) {
			S_nwQ = S_nwQ + S_tune*S_weight*A_Wei*RD_Lum/100.; 
			S_nQ = S_nQ +1; 
			S_hQNum.Fill(-0.5, S_tune*S_weight*A_Wei*RD_Lum/100.);}
			else {
			S_nwQ1 = S_nwQ1 + S_tune*S_weight*A_Wei*RD_Lum/100.;
			S_nQ1 = S_nQ1 +1;
			S_hQNum.Fill(0.5, S_tune*S_weight*A_Wei*RD_Lum/100.);}
			
			
			if ((S_e.muons[0].charge==-1)&&(S_e.muons[1].charge == -1)) {
			S_nwQ3 = S_nwQ3 + S_tune*S_weight*A_Wei*RD_Lum/100.;
			S_nQ3 = S_nQ3 +1;}
			
			if ((S_e.muons[0].charge==1)&&(S_e.muons[1].charge == 1)) {
            S_nwQ4 = S_nwQ4 + S_tune*S_weight*A_Wei*RD_Lum/100.;			
			S_nQ4 = S_nQ4 +1;}
			
			
			S_dVz.Fill((abs(S_e.muons[0].vertex.z()-S_e.muons[1].vertex.z())), S_tune*S_weight*A_Wei*RD_Lum/100.);
			S_dVz1.Fill(abs(S_e.jets[0].vertex.z()-S_e.jets[1].vertex.z()), S_tune*S_weight*A_Wei*RD_Lum/100.);
			S_dVz2.Fill(abs(S_e.jets[0].vertex.z()-S_e.muons[0].vertex.z()), S_tune*S_weight*A_Wei*RD_Lum/100.);
			S_dVz3.Fill(abs(S_e.jets[1].vertex.z()-S_e.muons[1].vertex.z()), S_tune*S_weight*A_Wei*RD_Lum/100.);
			S_dVz4.Fill(abs(S_e.jets[1].vertex.z()-S_e.muons[0].vertex.z()), S_tune*S_weight*A_Wei*RD_Lum/100.);
			S_dVz5.Fill(abs(S_e.jets[0].vertex.z()-S_e.muons[1].vertex.z()), S_tune*S_weight*A_Wei*RD_Lum/100.);
	
			S_PU.Fill(S_e.vertices.size(), S_tune*S_weight*A_Wei*RD_Lum/100.);
			S_PU1.Fill(S_e.pileup.size(), S_tune*S_weight*A_Wei*RD_Lum/100.);
			S_hZPt.Fill((K1_Z1*S_e.muons[0].p4 + K1_Z2*S_e.muons[1].p4).Pt(), S_tune*S_weight*A_Wei*RD_Lum/100.);
		
		
		/*	
			cout<<"New event!" << n <<" PtL1 = " <<(K1_Z1*S_e.muons[0].p4).Pt()<<" PtL2 = "<<(K1_Z2*S_e.muons[1].p4).Pt() << " Mll = "<<(K1_Z1*S_e.muons[0].p4 + K1_Z2*S_e.muons[1].p4).M() << endl;
			cout<<" PtJ1 = "<<(S_e.jets[0].p4).Pt()<<" bDiscr J1 = "<< S_e.jets[0].bDiscr <<" PtJ2 = "<<(S_e.jets[1].p4).Pt()<<" bDiscr J2 = "<< S_e.jets[1].bDiscr <<endl;
			cout<<"EtaL1 = "<<(K1_Z1*S_e.muons[0].p4).Eta()<<" PhiL1 = "<<(K1_Z1*S_e.muons[0].p4).Phi()<<" EtaL2 = "<<(K1_Z2*S_e.muons[1].p4).Eta()<<" PhiL2 = "<<(K1_Z2*S_e.muons[1].p4).Phi() << endl;
			cout<<"EtaJ1 = "<<(S_e.jets[0].p4).Eta()<<" PhiJ1 = "<<(S_e.jets[0].p4).Phi()<<" EtaJ2 = "<<(S_e.jets[1].p4).Eta()<<" PhiJ2 = "<<(S_e.jets[1].p4).Phi() << endl;			
			
	        cout<<"vzL1 = "<<(S_e.muons[0].vertex).z()<<" VzL2 = "<<(S_e.muons[1].vertex).z()<<" VzJ1 = "<<(S_e.jets[0].vertex).z()<<" VzJ2 = "<<(S_e.jets[1].vertex).z() << endl;
		
	    	cout<<" L1: ecalIso = " <<S_e.muons[0].ecalIso <<" hcalIso = "<<S_e.muons[0].hcalIso <<" hcal1Iso = "<<S_e.muons[0].hcal1Iso << " trackIso = "<< S_e.muons[0].trackIso << endl; 
					
			cout<<" L1: charge = " <<S_e.muons[0].charge << endl; 
	
	       cout<<" L1: ecalIso = " <<S_e.muons[1].ecalIso <<" hcalIso = "<<S_e.muons[1].hcalIso <<" hcal1Iso = "<<S_e.muons[1].hcal1Iso << " trackIso = "<< S_e.muons[1].trackIso << endl; 
			
			cout<<" L2: charge = " <<S_e.muons[1].charge << endl;
							
			cout<<"MWR = "<< WR <<" MNuR = "<< NuR_l1 <<" Mjj = "<< Mjj << endl;
			cout <<"RUN = "<< S_e.irun << " LUMI = " << S_e.ilumi << " EVENT = "<< S_e.ievent<< endl;
		    
			
        */  
		}
		}	
		}	
       } 	
        }
	   }
	  }
	 }  
	// Multiplicity
    S_hElMult.Fill(S_e.muons.size(),S_tune*S_weight*RD_Lum/100.);
    S_hJetMult.Fill(S_e.jets.size(),S_tune*S_weight*RD_Lum/100.);	
	    

  } 
 BG_nw = BG_nw + S_nw; // number of total BG events
 delta_BG_nw_sq = delta_BG_nw_sq + (0.20*0.20 + 1/(S_n+1))*S_nw*S_nw ; // 20% TTb systematic BG error 

 cout << "MC SIGNAL" << endl;
 cout << endl;
 cout << "Total events = " << S_nEvents << endl;
 cout << "Total weight events = " << S_nwEvents << endl;
 cout << endl; 
 cout << "Number of passed events = "<<S_n<<"   "<<S_nw<< endl;
 cout << endl;
 cout << "Number of events with 0 b-jet = " << S_nB3<<"   " <<S_nwB3 << endl; 
 cout << "Number of events with 1 b-jet = " << S_nB1<<"   " <<S_nwB1 << endl;
 cout << "Number of events with 2 b-jet = " << S_nB2<<"   " <<S_nwB2 << endl;	
 cout << endl;	
 cout << "Number of events with same charges of lepton = " << S_nQ1<<"   " <<S_nwQ1 << endl;
 cout << "Number of events with opposite charges of lepton = " << S_nQ <<"   " <<S_nwQ << endl; 
 cout << endl;
 cout << "Number of events with e-e- = " << S_nQ3<<"   " <<S_nwQ3 << endl;
 cout << "Number of events with e+e+ = " << S_nQ4 <<"   " <<S_nwQ4 << endl; 
 cout << endl;
 
}
 
//============================================================================================================================
 

  
// MC BG QCD FAKE-RATE 


  
  TH1F QCD_hWR(  "QCD_WR", "W_{R} reconstructed mass;mass, GeV/c^{2};events/50 GeV", M_Delta_M_W, M_min_M_W, M_max_M_W);
  TH1F QCD_hNuR0("QCD_NuR0", "nu_{R} reconstructed mass using first E electron;mass, GeV/c^{2};events/50 GeV", M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu);
  TH1F QCD_hNuR1("QCD_NuR1", "nu_{R} reconstructed mass using second E electron;mass, GeV/c^{2};events/50 GeV", M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu);
  TH1F QCD_hMll("QCD_Mll", "lepton-lepton pair invariant mass;mass, GeV/c^{2};events/2 GeV", M_Delta_Mll, M_min_Mll, M_max_Mll);
  TH1F QCD_hMll1("QCD_Mll1", "lepton-lepton pair invariant mass;mass, GeV/c^{2};events/40 GeV", 20, 100, 1000);
  TH1F QCD_hMjj("QCD_Mjj", "jet-jet pair invariant mass;mass, GeV/c^{2};events/50 GeV", M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu);
 // TH1F QCD_hMmets("QCD_Mmets", "Miss Et; Et, GeV/c^{2};events/10 GeV", M_Delta_MET, M_min_MET, M_max_MET);
 // TH1F QCD_hMmets1("QCD_Mmets1", "Miss Et; Et, GeV/c^{2};events/30 GeV", 30, 100, 1000); 
  TH1F QCD_PtWR(  "QCD_PtWR", "W_{R} Pt; Pt, GeV/c^{2}; events/6GeV", 30, 0, 180);
  TH1F QCD_PlWR(  "QCD_PlWR", "W_{R} Pl;Pl, GeV/c^{2};events/50 GeV", 60, -1500, 1500);
  TH1F QCD_EtaWR( "QCD_EtaWR", "W_{R} Eta; Eta; events", 10, -6, 6);
  TH1F QCD_PhiWR( "QCD_PhiWR", "W_{R} Phi; Phi; events", 10, -3.1415, 3.1415); 
 // TH1F QCD_hbDiscr_j1( "QCD_bDiscrJ1", "first jet b Discriminant; bDiscr; events", 15, 0, 1.5); 
 // TH1F QCD_hbDiscr_j2( "QCD_bDiscrJ2", "second jet b Discriminant; bDiscr; events", 15, 0, 1.5); 
 // TH1F QCD_hbNum( "QCD_hbNum", "Number of b jets ; bNum; events", 3, 0, 3); 
  TH1F QCD_hQNum( "QCD_hQNum", "Mult of leptons charges ; e_1*e_2; events", 2, -1, 1); 
  TH1F QCD_hElMult("QCD_ElMult", "electrons multiplicity", 10, 0, 10);
       QCD_hElMult.GetXaxis()->SetNdivisions(110);
  TH1F QCD_hJetMult("QCD_JetMult", "jets multiplicity", 20, 0, 20);
       QCD_hJetMult.GetXaxis()->SetNdivisions(210);   
//  TH1F QCD_hElRes("QCD_ElRes", "electron energy reconstruction resolution", 100, -0.3, 0.3);

  TH1F QCD_dVz( "QCD_dVz", "delta Vz; #delta V_{z} between ee, cm; Events", 20, 0, 0.1); 
  TH1F QCD_dVz1( "QCD_dVz1", "delta Vz;#delta V_{z} between jj, cm; Events", 20, 0, 0.1);
  TH1F QCD_dVz2( "QCD_dVz2", "delta Vz;#delta V_{z} between e_{1}j_{1}, cm; Events", 20, 0, 0.1);  
  TH1F QCD_dVz3( "QCD_dVz3", "delta Vz;#delta V_{z} between e_{2}j_{2}, cm; Events", 20, 0, 0.1); 
  TH1F QCD_dVz4( "QCD_dVz4", "delta Vz;#delta V_{z} between e_{2}j_{1}, cm; Events", 20, 0, 0.1);  
  TH1F QCD_dVz5( "QCD_dVz5", "delta Vz;#delta V_{z} between e_{1}j_{2}, cm; Events", 20, 0, 0.1); 
  
  fname = "/moscow31/heavynu/38x/dstmkLQ1.d";

//counters   
  int QCD_n = 0;
  float QCD_nw = 0.;
   
if (QCD_incl == true) {  
  
  ifstream inFile(fname);

//counters 
//  int QCD_nB1 = 0; 
//  int QCD_nB2 = 0;  
//  int QCD_nB3 = 0;
  int QCD_nQ = 0;
  float QCD_nwQ = 0.;
  int QCD_nQ1 = 0;
  float QCD_nwQ1 = 0.;  
  
  // Normalization for Data sample lumi  
  float QCD_weight_sample = QCD_tune*RD_Lum/100.;
  float QCD_e_jets_size = 0; 
  float QCD_e_electrons_size = 0;  
  float QCD_e_weight= 1.;
  float pjg[4];
  int QCD_itype = 0;
  int QCD_jemark = 0;
  int QCD_e_electrons_0_charge;
  int QCD_e_electrons_1_charge;
  long int QCD_e_ievent = 0;
  long int QCD_nEvents = 0;
  float QCD_nwEvents = 0.;

  char str[200] = {" "};
  
  TLorentzVector QCD_e_electrons_0_p4, QCD_e_electrons_1_p4, QCD_e_jets_0_p4, QCD_e_jets_1_p4;
 
  while (!inFile.eof()) {
  
    QCD_nEvents = QCD_nEvents + 1;
	
    
	inFile >> QCD_e_ievent;
	inFile.getline(str, 200);
	
	inFile >> QCD_e_jets_size;
    inFile >> QCD_e_weight;
    inFile >> QCD_itype;   
    inFile.getline(str, 200);
	
	QCD_e_weight = QCD_e_weight*QCD_weight_sample;
	QCD_nwEvents = QCD_nwEvents + QCD_e_weight;
	
	// read Jets informations   
	inFile >> QCD_jemark;
	for (int j = 0; j < 4; j++) inFile >> pjg[j];
    QCD_e_jets_0_p4.SetPxPyPzE(pjg[0], pjg[1], pjg[2], pjg[3]);	
	inFile.getline(str, 200);
	
	inFile >> QCD_jemark;
	for (int j = 0; j < 4; j++) inFile >> pjg[j];
	QCD_e_jets_1_p4.SetPxPyPzE(pjg[0], pjg[1], pjg[2], pjg[3]);	
	inFile.getline(str, 200);
	
	// Read Leptons informations
	inFile >> QCD_e_electrons_size;
    inFile.getline(str, 200);
	
	inFile >> QCD_e_electrons_0_charge;
	for (int j = 0; j < 4; j++) inFile >> pjg[j];
	QCD_e_electrons_0_p4.SetPxPyPzE(pjg[0], pjg[1], pjg[2], pjg[3]);	
	inFile.getline(str, 200);
	
	inFile >> QCD_e_electrons_1_charge;
	for (int j = 0; j < 4; j++) inFile >> pjg[j];
	QCD_e_electrons_1_p4.SetPxPyPzE(pjg[0], pjg[1], pjg[2], pjg[3]);	
	inFile.getline(str, 200);
	
    inFile.getline(str, 200);
    

	
	
      
    
      if ((QCD_e_electrons_size >= NofLept) && (QCD_e_jets_size >= NofJets)) {
	  
	  if ((QCD_e_electrons_0_p4).Eta()<=1.44){A_Wei=HEEP_B;} else {A_Wei=HEEP_E;}
	  if ((QCD_e_electrons_1_p4).Eta()<=1.44){A_Wei=A_Wei*HEEP_B;} else {A_Wei=A_Wei*HEEP_E;}
	
	//cuts on electrons Pt
	    if (((QCD_e_electrons_0_p4).Pt()>=Lep1Pt) && ((QCD_e_electrons_1_p4).Pt()>=Lep2Pt))  { 
    //cuts on jetss Pt      
	      if (((QCD_e_jets_0_p4).Pt()>=Jet1Pt) && ((QCD_e_jets_1_p4).Pt()>=Jet2Pt))  { 
		  
	//cuts on electrons invariant mass Mll>=400.0	
		  if (((QCD_e_electrons_0_p4 + QCD_e_electrons_1_p4).M()>=MllCut)&&((QCD_e_electrons_0_p4 + QCD_e_electrons_1_p4).M()<=MllCut1)) {
//		  if (!(((QCD_e_electrons_0_p4 + QCD_e_electrons_1_p4).M()>=MllCut)&&((QCD_e_electrons_0_p4 + QCD_e_electrons_1_p4).M()<=MllCut1))) {
		  
	//cuts on Eta  
         if (((abs(QCD_e_electrons_0_p4.Eta())<=EtaCut1L) || (abs(QCD_e_electrons_1_p4.Eta())<=EtaCut2L)) 
	     && (abs(QCD_e_jets_0_p4.Eta())<=EtaCut1J) && (abs(QCD_e_jets_1_p4.Eta())<=EtaCut2J) )        {
			
	// Isolation
	//       if ((QCD_e_electrons_0_isoHEEP() <= ISO)&&(QCD_e_electrons_0_isoHEEP() <= ISO)) {
		   
	//Cut on vertexes
    //       if ((abs(QCD_e_electrons_0_vertex.z()-QCD_e_electrons_1_vertex.z())<=Vertex) && (abs(QCD_e_jets_0_vertex.z()-QCD_e_jets_1_vertex.z())<=Vertex) 
    //       && (abs(abs(QCD_e_electrons_0_vertex.z()-QCD_e_electrons_1_vertex.z())-abs(QCD_e_jets_0_vertex.z()-QCD_e_jets_1_vertex.z()))<=Vertex)) {
	
	        WR = (QCD_e_electrons_0_p4 + QCD_e_electrons_1_p4 + QCD_e_jets_0_p4 + QCD_e_jets_1_p4).M();
			if (WR>=MW_Cut) {
            QCD_hWR.Fill(WR, QCD_e_weight);
	  
	        tWR = (QCD_e_electrons_0_p4 + QCD_e_electrons_1_p4 + QCD_e_jets_0_p4 + QCD_e_jets_1_p4).Pt();
            QCD_PtWR.Fill(tWR, QCD_e_weight);
	  
	        lWR = (QCD_e_electrons_0_p4 + QCD_e_electrons_1_p4 + QCD_e_jets_0_p4 + QCD_e_jets_1_p4).Pz();
            QCD_PlWR.Fill(lWR, QCD_e_weight);
	  
	        eWR = (QCD_e_electrons_0_p4 + QCD_e_electrons_1_p4 + QCD_e_jets_0_p4 + QCD_e_jets_1_p4).Eta();
            QCD_EtaWR.Fill(eWR, QCD_e_weight);
	  
	        pWR = (QCD_e_electrons_0_p4 + QCD_e_electrons_1_p4 + QCD_e_jets_0_p4 + QCD_e_jets_1_p4).Phi();
            QCD_PhiWR.Fill(pWR, QCD_e_weight);
	  
            NuR_l0 = (QCD_e_electrons_0_p4 + QCD_e_jets_0_p4 + QCD_e_jets_1_p4).M();
            NuR_l1 = (QCD_e_electrons_1_p4 + QCD_e_jets_0_p4 + QCD_e_jets_1_p4).M();
      
	        QCD_hNuR0.Fill(NuR_l0, QCD_e_weight);
	        QCD_hNuR1.Fill(NuR_l1, QCD_e_weight);
	        
            Mll = (QCD_e_electrons_0_p4 + QCD_e_electrons_1_p4).M();
            QCD_hMll.Fill(Mll, QCD_e_weight);
			QCD_hMll1.Fill(Mll, QCD_e_weight);
      
            Mjj = (QCD_e_jets_0_p4 + QCD_e_jets_1_p4).M();
            QCD_hMjj.Fill(Mjj, QCD_e_weight);
			
//			Mmet = (QCD_e_mets_0_p4).Pt();
//            QCD_hMmets.Fill(Mmet, QCD_e_weight);
//			QCD_hMmets1.Fill(Mmet, QCD_e_weight);
			
//			QCD_hbDiscr_j1.Fill(QCD_e_jets_0_bDiscr, QCD_e_weight);
//			QCD_hbDiscr_j2.Fill(QCD_e_jets_1_bDiscr, QCD_e_weight);

			
			QCD_n = QCD_n + 1;
			QCD_nw = QCD_nw + QCD_e_weight; 


/*			
			if ((QCD_e_jets_0_bDiscr >= 0.669) && (QCD_e_jets_1_bDiscr >= 0.669)) { //  10% = 0.215      1% = 0.459    0.1% = 0.669
			QCD_nB2 = QCD_nB2 + 1;
			QCD_hbNum.Fill(2., QCD_e_weight);
			}
			if (((QCD_e_jets_0_bDiscr >= 0.669) && (QCD_e_jets_1_bDiscr <= 0.669)) || ((QCD_e_jets_0_bDiscr <= 0.669) && (QCD_e_jets_1_bDiscr >= 0.669)) ) { //  10% = 0.215      1% = 0.459    0.1% = 0.669
			QCD_nB1 = QCD_nB1 + 1;
			QCD_hbNum.Fill(1., QCD_e_weight);
			}
			if ((QCD_e_jets_0_bDiscr <= 0.669) && (QCD_e_jets_1_bDiscr <= 0.669)) { 
			QCD_nB3 = QCD_nB3 + 1;
			QCD_hbNum.Fill(0., QCD_e_weight);
			}
*/			
			if (QCD_e_electrons_0_charge*QCD_e_electrons_1_charge == -1) { 
			QCD_nQ = QCD_nQ +1;
			QCD_nwQ = QCD_nwQ + QCD_e_weight;
			QCD_hQNum.Fill(-0.5, QCD_e_weight);}
			else {QCD_nQ1 = QCD_nQ1 +1;
			QCD_nwQ1 = QCD_nwQ1 + QCD_e_weight;
			QCD_hQNum.Fill(0.5, QCD_e_weight);}
			
			QCD_dVz.Fill(0., QCD_e_weight);
			QCD_dVz1.Fill(0., QCD_e_weight);
			QCD_dVz2.Fill(0., QCD_e_weight);
			QCD_dVz3.Fill(0., QCD_e_weight);
			QCD_dVz4.Fill(0., QCD_e_weight);
			QCD_dVz5.Fill(0., QCD_e_weight);
			

			
/*			
			cout<<"New event!"<< QCD_n << " "<<QCD_e_weight<<" "<< QCD_nw<<" PtL1 = " <<(QCD_e_electrons_0_p4).Pt()<<" PtL2 = "<<(QCD_e_electrons_1_p4).Pt() << " Mll = "<<(QCD_e_electrons_0_p4 + QCD_e_electrons_1_p4).M() << endl;
			cout<<" PtJ1 = "<<(QCD_e_jets_0_p4).Pt()<<" PtJ2 = "<<(QCD_e_jets_1_p4).Pt()<<endl;
			cout<<"EtaL1 = "<<(QCD_e_electrons_0_p4).Eta()<<" PhiL1 = "<<(QCD_e_electrons_0_p4).Phi()<<" EtaL2 = "<<(QCD_e_electrons_1_p4).Eta()<<" PhiL2 = "<<(QCD_e_electrons_1_p4).Phi() << endl;
			cout<<"EtaJ1 = "<<(QCD_e_jets_0_p4).Eta()<<" PhiJ1 = "<<(QCD_e_jets_0_p4).Phi()<<" EtaJ2 = "<<(QCD_e_jets_1_p4).Eta()<<" PhiJ2 = "<<(QCD_e_jets_1_p4).Phi() << endl;			
			
			cout<<" L1: charge = " <<QCD_e_electrons_0_charge << endl; 
			
			cout<<" L2: charge = " <<QCD_e_electrons_1_charge << endl;
							
			cout<<"MWR = "<< WR <<" MNuR = "<< NuR_l1 <<" Mjj = "<< Mjj << endl;
			cout <<" EVENT = "<< QCD_e_ievent<< endl;
*/		
        }        			
		}	
		}	
        } 	
        }
	   }
//	  }
//	 }    
	// Multiplicity
    QCD_hElMult.Fill(QCD_e_electrons_size);
    QCD_hJetMult.Fill(QCD_e_jets_size);
  } 
 BG_nw = BG_nw + QCD_nw; // number of total BG events
 delta_BG_nw_sq = delta_BG_nw_sq + (0.50*0.50 + 1/(QCD_n+1))*QCD_nw*QCD_nw ; // 50% QCD systematic BG error 
 
 QCD_hQNum.SetBinContent(1, QCD_nw/2.);
 QCD_hQNum.SetBinContent(2, QCD_nw/2.);

 cout << "MC BG QCD SAMPLE" << endl;
 cout << endl;
 cout << "Total events = " << QCD_nEvents << endl;
 cout << "Total weight events = " << QCD_nwEvents<<" "<< endl;
 cout << endl; 
 cout << "Number of passed events = "<<QCD_n<<"   "<<QCD_nw<< endl;
 cout << endl;
 cout << "Number of events with same charges of lepton = " << QCD_nQ1<<"   " <<QCD_nwQ1 << endl;
 cout << "Number of events with opposite charges of lepton = " << QCD_nQ <<"   " <<QCD_nwQ << endl; 
 cout << endl;
}

//=============================================================================================================================
   
  
// MC BG TTbar

  
  TH1F TTb_hWR(  "TTb_WR", "W_{R} reconstructed mass;mass, GeV/c^{2};events/50 GeV", M_Delta_M_W, M_min_M_W, M_max_M_W);
  TH1F TTb_hNuR0("TTb_NuR0", "nu_{R} reconstructed mass using first E electron;mass, GeV/c^{2};events/50 GeV", M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu);
  TH1F TTb_hNuR1("TTb_NuR1", "nu_{R} reconstructed mass using second E electron;mass, GeV/c^{2};events/50 GeV", M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu); 
  TH1F TTb_hMll("TTb_Mll", "lepton-lepton pair invariant mass;mass, GeV/c^{2};events/2 GeV", M_Delta_Mll, M_min_Mll, M_max_Mll);
  TH1F TTb_hMll1("TTb_Mll1", "lepton-lepton pair invariant mass;mass, GeV/c^{2};events/40 GeV", 20, 100, 1000);
  TH1F TTb_hMjj("TTb_Mjj", "jet-jet pair invariant mass;mass, GeV/c^{2};events/50 GeV", M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu);
  TH1F TTb_hMmets("TTb_Mmets", "Miss Et; Et, GeV/c^{2};events/10 GeV", M_Delta_MET, M_min_MET, M_max_MET);
  TH1F TTb_hMmets1("TTb_Mmets1", "Miss Et; Et, GeV/c^{2};events/30 GeV", 30, 100, 1000); 
  TH1F TTb_PtWR(  "TTb_PtWR", "W_{R} Pt; Pt, GeV/c^{2}; events/6GeV", 30, 0, 180);
  TH1F TTb_PlWR(  "TTb_PlWR", "W_{R} Pl;Pl, GeV/c^{2};events/50 GeV", 60, -1500, 1500);
  TH1F TTb_EtaWR( "TTb_EtaWR", "W_{R} Eta; Eta; events", 10, -6, 6);
  TH1F TTb_PhiWR( "TTb_PhiWR", "W_{R} Phi; Phi; events", 10, -3.1415, 3.1415); 
  TH1F TTb_hbDiscr_j1( "TTb_bDiscrJ1", "first jet b Discriminant; bDiscr; events", 15, 0, 1.5); 
  TH1F TTb_hbDiscr_j2( "TTb_bDiscrJ2", "second jet b Discriminant; bDiscr; events", 15, 0, 1.5); 
  TH1F TTb_hbNum( "TTb_hbNum", "Number of b jets ; bNum; events", 3, 0, 3); 
  TH1F TTb_hQNum( "TTb_hQNum", "Mult of leptons charges ; e_1*e_2; events", 2, -1, 1); 
  TH1F TTb_hElMult("TTb_ElMult", "electrons multiplicity", 10, 0, 10);
       TTb_hElMult.GetXaxis()->SetNdivisions(110);
  TH1F TTb_hJetMult("TTb_JetMult", "jets multiplicity", 20, 0, 20);
       TTb_hJetMult.GetXaxis()->SetNdivisions(210);   
  TH1F TTb_hElRes("TTb_ElRes", "electron energy reconstruction resolution", 100, -0.3, 0.3);
  
  TH1F TTb_dVz( "TTb_dVz", "delta Vz; #delta V_{z} between ee, cm; Events", 20, 0, 0.1); 
  TH1F TTb_dVz1( "TTb_dVz1", "delta Vz;#delta V_{z} between jj, cm; Events", 20, 0, 0.1);
  TH1F TTb_dVz2( "TTb_dVz2", "delta Vz;#delta V_{z} between e_{1}j_{1}, cm; Events", 20, 0, 0.1);  
  TH1F TTb_dVz3( "TTb_dVz3", "delta Vz;#delta V_{z} between e_{2}j_{2}, cm; Events", 20, 0, 0.1); 
  TH1F TTb_dVz4( "TTb_dVz4", "delta Vz;#delta V_{z} between e_{2}j_{1}, cm; Events", 20, 0, 0.1);  
  TH1F TTb_dVz5( "TTb_dVz5", "delta Vz;#delta V_{z} between e_{1}j_{2}, cm; Events", 20, 0, 0.1); 
  
  TH1F TTb_PU( "TTb_PU", "PU; Primary vertex number ; Events", 25, 0, 25); 
  TH1F TTb_PU1( "TTb_PU1", "PU1; Primary vertex number ; Events", 25, 0, 25); 
  TH1F TTb_hZPt("TTb_ZPt", "Z+jets; Z Pt, GeV/c;Events/20 GeV/c", 10, 0, 200);
    	
  fname = "/moscow41/heavynu/41x-background/TTJets_TuneZ2_7TeV-madgraph-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root";


 
  TheEvent TTb_e(fname);
  if (!TTb_e) return;
  int TTb_n = 0; 
  float TTb_nw = 0.;  
  
if (TTb_incl == true) {

  int TTb_nB1 = 0; 
  float TTb_nwB1 = 0;
  int TTb_nB2 = 0;
  float TTb_nwB2 = 0;  
  int TTb_nB3 = 0;
  float TTb_nwB3 = 0;
  int TTb_nEvents = 0; 
  float TTb_nwEvents = 0;  
  int TTb_nQ = 0;
  float TTb_nwQ = 0;
  int TTb_nQ1 = 0;
  float TTb_nwQ1 = 0;
  int TTb_nQ3 = 0;
  float TTb_nwQ3 = 0;
  int TTb_nQ4 = 0;
  float TTb_nwQ4 = 0;
  float TTb_weight =0;
  

  TTb_nEvents = TTb_e.totalEvents();
 
  for (int i = 0; i < TTb_nEvents; ++i) {
    TTb_e.readEvent(i);  

      if(TTb_e.pileup.size()<25){
        TTb_weight = TTb_e.weight *  generate_flat10_weights(histo)[TTb_e.pileup.size()];
        }
        else{ //should not happen as we have a weight for all simulated TTb_e.pileup.size() multiplicities!
        TTb_weight = 0.;
        }

		TTb_nwEvents = TTb_nwEvents + TTb_tune*TTb_weight*RD_Lum/100.;  
   
    // electron energy resolution
    for (unsigned int j = 0; j < TTb_e.muons.size(); ++j) {
      const Particle& reco = TTb_e.muons[j];
      if (reco.mcId >= 0) {
        const Particle& mc = TTb_e.gens[reco.mcId];
        const float r = (reco.p4.E() - mc.p4.E())/mc.p4.E();
        TTb_hElRes.Fill(r, TTb_tune*TTb_weight*RD_Lum/100.);
      }
    }
    
    // mass distributions
      if ((TTb_e.muons.size() >= NofLept) && (TTb_e.jets.size() >= NofJets)) {
	  
    //  correction of Z mass and ID efficiency
	   if ((abs(TTb_e.muons[0].p4.Eta())<=1.44)) {K1_Z1 = 1.;} else {K1_Z1=1.;}
	   if ((abs(K1_Z2*TTb_e.muons[1].p4.Eta())<=1.44)) {K1_Z2 = 1.;} else {K1_Z2=1.;}
	  
	   if (abs((K1_Z1*TTb_e.muons[0].p4).Eta())<=1.44){A_Wei=HEEP_B;} else {A_Wei=HEEP_E;}
	   if (abs((K1_Z2*TTb_e.muons[1].p4).Eta())<=1.44){A_Wei=A_Wei*HEEP_B;} else {A_Wei=A_Wei*HEEP_E;}
	
	//cuts on electrons Pt>=40
	    if (((K1_Z1*TTb_e.muons[0].p4).Pt()>=Lep1Pt) && ((K1_Z2*TTb_e.muons[1].p4).Pt()>=Lep2Pt))  { 
    //cuts on jetss Pt>=40      
	      if (((TTb_e.jets[0].p4).Pt()>=Jet1Pt) && ((TTb_e.jets[1].p4).Pt()>=Jet2Pt))  { 
		  
	//cuts on electrons invariant mass Mll>=400.0	
	  if (((K1_Z1*TTb_e.muons[0].p4 + K1_Z2*TTb_e.muons[1].p4).M()>=MllCut)&& ((K1_Z1*TTb_e.muons[0].p4 + K1_Z2*TTb_e.muons[1].p4).M()<=MllCut1)) {
//	  if (!(((K1_Z1*TTb_e.muons[0].p4 + K1_Z2*TTb_e.muons[1].p4).M()>=MllCut)&& ((K1_Z1*TTb_e.muons[0].p4 + K1_Z2*TTb_e.muons[1].p4).M()<=MllCut1))) {
			  
	//cuts on Eta  
         if (((abs(K1_Z1*TTb_e.muons[0].p4.Eta())<=EtaCut1L) || (abs(K1_Z2*TTb_e.muons[1].p4.Eta())<=EtaCut2L)) 
	     && (abs(TTb_e.jets[0].p4.Eta())<=EtaCut1J) && (abs(TTb_e.jets[1].p4.Eta())<=EtaCut2J) )        {
			
	// Isolation
	       if ((TTb_e.muons[0].isoHEEP() <= ISO)&&(TTb_e.muons[1].isoHEEP() <= ISO)) {
		   
	 //Cut on vertexes
             if ((abs(TTb_e.muons[0].vertex.z()-TTb_e.muons[1].vertex.z())<=Vertex) && (abs(TTb_e.jets[0].vertex.z()-TTb_e.jets[1].vertex.z())<=Vertex) 
             && ((abs(TTb_e.muons[0].vertex.z()-TTb_e.jets[1].vertex.z())<=Vertex) && (abs(TTb_e.jets[0].vertex.z()-TTb_e.muons[1].vertex.z())<=Vertex))) {
			 
	        WR = (K1_Z1*TTb_e.muons[0].p4 + K1_Z2*TTb_e.muons[1].p4 + TTb_e.jets[0].p4 + TTb_e.jets[1].p4).M();
			if (WR>=MW_Cut) {
            TTb_hWR.Fill(WR, TTb_tune*TTb_weight*A_Wei*RD_Lum/100.);
	  
	        tWR = (K1_Z1*TTb_e.muons[0].p4 + K1_Z2*TTb_e.muons[1].p4 + TTb_e.jets[0].p4 + TTb_e.jets[1].p4).Pt();
            TTb_PtWR.Fill(tWR, TTb_tune*TTb_weight*A_Wei*RD_Lum/100.);
	  
	        lWR = (K1_Z1*TTb_e.muons[0].p4 + K1_Z2*TTb_e.muons[1].p4 + TTb_e.jets[0].p4 + TTb_e.jets[1].p4).Pz();
            TTb_PlWR.Fill(lWR, TTb_tune*TTb_weight*A_Wei*RD_Lum/100.);
	  
	        eWR = (K1_Z1*TTb_e.muons[0].p4 + K1_Z2*TTb_e.muons[1].p4 + TTb_e.jets[0].p4 + TTb_e.jets[1].p4).Eta();
            TTb_EtaWR.Fill(eWR, TTb_tune*TTb_weight*A_Wei*RD_Lum/100.);
	  
	        pWR = (K1_Z1*TTb_e.muons[0].p4 + K1_Z2*TTb_e.muons[1].p4 + TTb_e.jets[0].p4 + TTb_e.jets[1].p4).Phi();
            TTb_PhiWR.Fill(pWR, TTb_tune*TTb_weight*A_Wei*RD_Lum/100.);
	  
            NuR_l0 = (K1_Z1*TTb_e.muons[0].p4 + TTb_e.jets[0].p4 + TTb_e.jets[1].p4).M();
            NuR_l1 = (K1_Z2*TTb_e.muons[1].p4 + TTb_e.jets[0].p4 + TTb_e.jets[1].p4).M();
      
	        TTb_hNuR0.Fill(NuR_l0, TTb_tune*TTb_weight*A_Wei*RD_Lum/100.);
	        TTb_hNuR1.Fill(NuR_l1, TTb_tune*TTb_weight*A_Wei*RD_Lum/100.);
	        
            Mll = (K1_Z1*TTb_e.muons[0].p4 + K1_Z2*TTb_e.muons[1].p4).M();
            TTb_hMll.Fill(Mll, TTb_tune*TTb_weight*A_Wei*RD_Lum/100.);
			TTb_hMll1.Fill(Mll, TTb_tune*TTb_weight*A_Wei*RD_Lum/100.);
      
            Mjj = (TTb_e.jets[0].p4 + TTb_e.jets[1].p4).M();
            TTb_hMjj.Fill(Mjj, TTb_tune*TTb_weight*A_Wei*RD_Lum/100.);
			
			Mmet = (TTb_e.mets[0].p4).Pt();
            TTb_hMmets.Fill(Mmet, TTb_tune*TTb_weight*A_Wei*RD_Lum/100.);
		    TTb_hMmets1.Fill(Mmet, TTb_tune*TTb_weight*A_Wei*RD_Lum/100.);
			
			TTb_hbDiscr_j1.Fill(TTb_e.jets[0].bDiscr, TTb_tune*TTb_weight*A_Wei*RD_Lum/100.);
			TTb_hbDiscr_j2.Fill(TTb_e.jets[1].bDiscr, TTb_tune*TTb_weight*A_Wei*RD_Lum/100.);
							
			TTb_n = TTb_n + 1;
			TTb_nw = TTb_nw + TTb_tune*TTb_weight*A_Wei*RD_Lum/100.; 
						
			if ((TTb_e.jets[0].bDiscr >= 0.669) && (TTb_e.jets[1].bDiscr >= 0.669)) { //  10% = 0.215      1% = 0.459    0.1% = 0.669
			TTb_nwB2 = TTb_nwB2 + TTb_tune*TTb_weight*A_Wei*RD_Lum/100.; 
			TTb_nB2 = TTb_nB2 + 1;
			TTb_hbNum.Fill(2., TTb_tune*TTb_weight*A_Wei*RD_Lum/100.);
			}
			if (((TTb_e.jets[0].bDiscr >= 0.669) && (TTb_e.jets[1].bDiscr <= 0.669)) || ((TTb_e.jets[0].bDiscr <= 0.669) && (TTb_e.jets[1].bDiscr >= 0.669)) ) { //  10% = 0.215      1% = 0.459    0.1% = 0.669
			TTb_nwB1 = TTb_nwB1 + TTb_tune*TTb_weight*A_Wei*RD_Lum/100.; 
			TTb_nB1 = TTb_nB1 + 1;
			TTb_hbNum.Fill(1., TTb_tune*TTb_weight*A_Wei*RD_Lum/100.);
			}
			if ((TTb_e.jets[0].bDiscr <= 0.669) && (TTb_e.jets[1].bDiscr <= 0.669)) {
            TTb_nwB3 = TTb_nwB3 + TTb_tune*TTb_weight*A_Wei*RD_Lum/100.; 			
			TTb_nB3 = TTb_nB3 + 1;
			TTb_hbNum.Fill(0., TTb_tune*TTb_weight*A_Wei*RD_Lum/100.);
			}
			
			if (TTb_e.muons[0].charge*TTb_e.muons[1].charge == -1) {
			TTb_nwQ = TTb_nwQ + TTb_tune*TTb_weight*A_Wei*RD_Lum/100.; 
			TTb_nQ = TTb_nQ +1; 
			TTb_hQNum.Fill(-0.5, TTb_tune*TTb_weight*A_Wei*RD_Lum/100.);}
			else {
			TTb_nwQ1 = TTb_nwQ1 + TTb_tune*TTb_weight*A_Wei*RD_Lum/100.;
			TTb_nQ1 = TTb_nQ1 +1;
			TTb_hQNum.Fill(0.5, TTb_tune*TTb_weight*A_Wei*RD_Lum/100.);}
			
			
			if ((TTb_e.muons[0].charge==-1)&&(TTb_e.muons[1].charge == -1)) {
			TTb_nwQ3 = TTb_nwQ3 + TTb_tune*TTb_weight*A_Wei*RD_Lum/100.;
			TTb_nQ3 = TTb_nQ3 +1;}
			
			if ((TTb_e.muons[0].charge==1)&&(TTb_e.muons[1].charge == 1)) {
            TTb_nwQ4 = TTb_nwQ4 + TTb_tune*TTb_weight*A_Wei*RD_Lum/100.;			
			TTb_nQ4 = TTb_nQ4 +1;}
			
			
			TTb_dVz.Fill((abs(TTb_e.muons[0].vertex.z()-TTb_e.muons[1].vertex.z())), TTb_tune*TTb_weight*A_Wei*RD_Lum/100.);
			TTb_dVz1.Fill(abs(TTb_e.jets[0].vertex.z()-TTb_e.jets[1].vertex.z()), TTb_tune*TTb_weight*A_Wei*RD_Lum/100.);
			TTb_dVz2.Fill(abs(TTb_e.jets[0].vertex.z()-TTb_e.muons[0].vertex.z()), TTb_tune*TTb_weight*A_Wei*RD_Lum/100.);
			TTb_dVz3.Fill(abs(TTb_e.jets[1].vertex.z()-TTb_e.muons[1].vertex.z()), TTb_tune*TTb_weight*A_Wei*RD_Lum/100.);
			TTb_dVz4.Fill(abs(TTb_e.jets[1].vertex.z()-TTb_e.muons[0].vertex.z()), TTb_tune*TTb_weight*A_Wei*RD_Lum/100.);
			TTb_dVz5.Fill(abs(TTb_e.jets[0].vertex.z()-TTb_e.muons[1].vertex.z()), TTb_tune*TTb_weight*A_Wei*RD_Lum/100.);
	
			TTb_PU.Fill(TTb_e.vertices.size(), TTb_tune*TTb_weight*A_Wei*RD_Lum/100.);
			TTb_PU1.Fill(TTb_e.pileup.size(), TTb_tune*TTb_weight*A_Wei*RD_Lum/100.);
			TTb_hZPt.Fill((K1_Z1*TTb_e.muons[0].p4 + K1_Z2*TTb_e.muons[1].p4).Pt(), TTb_tune*TTb_weight*A_Wei*RD_Lum/100.);
		
		
		/*	
			cout<<"New event!" << n <<" PtL1 = " <<(K1_Z1*TTb_e.muons[0].p4).Pt()<<" PtL2 = "<<(K1_Z2*TTb_e.muons[1].p4).Pt() << " Mll = "<<(K1_Z1*TTb_e.muons[0].p4 + K1_Z2*TTb_e.muons[1].p4).M() << endl;
			cout<<" PtJ1 = "<<(TTb_e.jets[0].p4).Pt()<<" bDiscr J1 = "<< TTb_e.jets[0].bDiscr <<" PtJ2 = "<<(TTb_e.jets[1].p4).Pt()<<" bDiscr J2 = "<< TTb_e.jets[1].bDiscr <<endl;
			cout<<"EtaL1 = "<<(K1_Z1*TTb_e.muons[0].p4).Eta()<<" PhiL1 = "<<(K1_Z1*TTb_e.muons[0].p4).Phi()<<" EtaL2 = "<<(K1_Z2*TTb_e.muons[1].p4).Eta()<<" PhiL2 = "<<(K1_Z2*TTb_e.muons[1].p4).Phi() << endl;
			cout<<"EtaJ1 = "<<(TTb_e.jets[0].p4).Eta()<<" PhiJ1 = "<<(TTb_e.jets[0].p4).Phi()<<" EtaJ2 = "<<(TTb_e.jets[1].p4).Eta()<<" PhiJ2 = "<<(TTb_e.jets[1].p4).Phi() << endl;			
			
	        cout<<"vzL1 = "<<(TTb_e.muons[0].vertex).z()<<" VzL2 = "<<(TTb_e.muons[1].vertex).z()<<" VzJ1 = "<<(TTb_e.jets[0].vertex).z()<<" VzJ2 = "<<(TTb_e.jets[1].vertex).z() << endl;
		
	    	cout<<" L1: ecalIso = " <<TTb_e.muons[0].ecalIso <<" hcalIso = "<<TTb_e.muons[0].hcalIso <<" hcal1Iso = "<<TTb_e.muons[0].hcal1Iso << " trackIso = "<< TTb_e.muons[0].trackIso << endl; 
					
			cout<<" L1: charge = " <<TTb_e.muons[0].charge << endl; 
	
	       cout<<" L1: ecalIso = " <<TTb_e.muons[1].ecalIso <<" hcalIso = "<<TTb_e.muons[1].hcalIso <<" hcal1Iso = "<<TTb_e.muons[1].hcal1Iso << " trackIso = "<< TTb_e.muons[1].trackIso << endl; 
			
			cout<<" L2: charge = " <<TTb_e.muons[1].charge << endl;
							
			cout<<"MWR = "<< WR <<" MNuR = "<< NuR_l1 <<" Mjj = "<< Mjj << endl;
			cout <<"RUN = "<< TTb_e.irun << " LUMI = " << TTb_e.ilumi << " EVENT = "<< TTb_e.ievent<< endl;
		    
			
        */  
		}
		}	
		}	
       } 	
        }
	   }
	  }
	 }  
	// Multiplicity
    TTb_hElMult.Fill(TTb_e.muons.size(),TTb_tune*TTb_weight*RD_Lum/100.);
    TTb_hJetMult.Fill(TTb_e.jets.size(),TTb_tune*TTb_weight*RD_Lum/100.);	
	    

  } 
 BG_nw = BG_nw + TTb_nw; // number of total BG events
 delta_BG_nw_sq = delta_BG_nw_sq + (0.20*0.20 + 1/(TTb_n+1))*TTb_nw*TTb_nw ; // 20% TTb systematic BG error 

 cout << "MC BG TTbar" << endl;
 cout << endl;
 cout << "Total events = " << TTb_nEvents << endl;
 cout << "Total weight events = " << TTb_nwEvents << endl;
 cout << endl; 
 cout << "Number of passed events = "<<TTb_n<<"   "<<TTb_nw<< endl;
 cout << endl;
 cout << "Number of events with 0 b-jet = " << TTb_nB3<<"   " <<TTb_nwB3 << endl; 
 cout << "Number of events with 1 b-jet = " << TTb_nB1<<"   " <<TTb_nwB1 << endl;
 cout << "Number of events with 2 b-jet = " << TTb_nB2<<"   " <<TTb_nwB2 << endl;	
 cout << endl;	
 cout << "Number of events with same charges of lepton = " << TTb_nQ1<<"   " <<TTb_nwQ1 << endl;
 cout << "Number of events with opposite charges of lepton = " << TTb_nQ <<"   " <<TTb_nwQ << endl; 
 cout << endl;
 cout << "Number of events with e-e- = " << TTb_nQ3<<"   " <<TTb_nwQ3 << endl;
 cout << "Number of events with e+e+ = " << TTb_nQ4 <<"   " <<TTb_nwQ4 << endl; 
 cout << endl;
 
}
 
//=============================================================================================================================
 
  
// MC BG Z+JETS

  
  TH1F ZJ_hWR(  "ZJ_WR", "W_{R} reconstructed mass;mass, GeV/c^{2};events/100 GeV", M_Delta_M_W, M_min_M_W, M_max_M_W);
  TH1F ZJ_hNuR0("ZJ_NuR0", "nu_{R} reconstructed mass using first E electron;mass, GeV/c^{2};events/100 GeV", M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu);
  TH1F ZJ_hNuR1("ZJ_NuR1", "nu_{R} reconstructed mass using second E electron;mass, GeV/c^{2};events/100 GeV", M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu); 
  TH1F ZJ_hMll("ZJ_Mll", "lepton-lepton pair invariant mass;mass, GeV/c^{2};events/2 GeV", M_Delta_Mll, M_min_Mll, M_max_Mll);
  TH1F ZJ_hMll1("ZJ_Mll1", "lepton-lepton pair invariant mass;mass, GeV/c^{2};events/40 GeV", 20, 100, 1000);
  TH1F ZJ_hMjj("ZJ_Mjj", "jet-jet pair invariant mass;mass, GeV/c^{2};events/50 GeV", M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu);
  TH1F ZJ_hMmets("ZJ_Mmets", "Miss Et; Et, GeV/c^{2};events/10 GeV", M_Delta_MET, M_min_MET, M_max_MET);
  TH1F ZJ_hMmets1("ZJ_Mmets1", "Miss Et; Et, GeV/c^{2};events/30 GeV", 30, 100, 1000); 
  TH1F ZJ_PtWR(  "ZJ_PtWR", "W_{R} Pt; Pt, GeV/c^{2}; events/6GeV", 30, 0, 180);
  TH1F ZJ_PlWR(  "ZJ_PlWR", "W_{R} Pl;Pl, GeV/c^{2};events/50 GeV", 60, -1500, 1500);
  TH1F ZJ_EtaWR( "ZJ_EtaWR", "W_{R} Eta; Eta; events", 10, -6, 6);
  TH1F ZJ_PhiWR( "ZJ_PhiWR", "W_{R} Phi; Phi; events", 10, -3.1415, 3.1415); 
  TH1F ZJ_hbDiscr_j1( "ZJ_bDiscrJ1", "first jet b Discriminant; bDiscr; events", 15, 0, 1.5); 
  TH1F ZJ_hbDiscr_j2( "ZJ_bDiscrJ2", "second jet b Discriminant; bDiscr; events", 15, 0, 1.5); 
  TH1F ZJ_hbNum( "ZJ_hbNum", "Number of b jets ; bNum; events", 3, 0, 3); 
  TH1F ZJ_hQNum( "ZJ_hQNum", "Mult of leptons charges ; e_1*e_2; events", 2, -1, 1);  
  TH1F ZJ_hElMult("ZJ_ElMult", "electrons multiplicity", 10, 0, 10);
       ZJ_hElMult.GetXaxis()->SetNdivisions(110);
  TH1F ZJ_hJetMult("ZJ_JetMult", "jets multiplicity", 20, 0, 20);
       ZJ_hJetMult.GetXaxis()->SetNdivisions(210);	   
  TH1F ZJ_hElRes("ZJ_ElRes", "electron energy reconstruction resolution", 100, -0.3, 0.3);
  
  TH1F ZJ_dVz( "ZJ_dVz", "delta Vz; #delta V_{z} between ee, cm; Events", 20, 0, 0.1); 
  TH1F ZJ_dVz1( "ZJ_dVz1", "delta Vz;#delta V_{z} between jj, cm; Events", 20, 0, 0.1);
  TH1F ZJ_dVz2( "ZJ_dVz2", "delta Vz;#delta V_{z} between e_{1}j_{1}, cm; Events", 20, 0, 0.1);  
  TH1F ZJ_dVz3( "ZJ_dVz3", "delta Vz;#delta V_{z} between e_{2}j_{2}, cm; Events", 20, 0, 0.1); 
  TH1F ZJ_dVz4( "ZJ_dVz4", "delta Vz;#delta V_{z} between e_{2}j_{1}, cm; Events", 20, 0, 0.1);  
  TH1F ZJ_dVz5( "ZJ_dVz5", "delta Vz;#delta V_{z} between e_{1}j_{2}, cm; Events", 20, 0, 0.1); 
  
  TH1F ZJ_PU( "ZJ_PU", "PU; Primary vertex number ; Events", 25, 0, 25); 
  TH1F ZJ_PU1( "ZJ_PU1", "PU1; Primary vertex number ; Events", 25, 0, 25); 
  TH1F ZJ_hZPt("ZJ_ZPt", "Z+jets; Z Pt, GeV/c;Events/20 GeV/c", 10, 0, 200);
  
  int ZJ_n = 0;
  float ZJ_nw = 0;

if (ZJ_incl) { 

  int ZJ_nB1 = 0; 
  float ZJ_nwB1 = 0; 
  int ZJ_nB2 = 0;  
  float ZJ_nwB2 = 0;
  int ZJ_nB3 = 0;
  float ZJ_nwB3 = 0;
  int ZJ_nEvents = 0;
  float ZJ_nwEvents = 0;   
  int ZJ_nQ = 0;
  float ZJ_nwQ = 0;
  int ZJ_nQ1 = 0;
  float ZJ_nwQ1 = 0; 
  int ZJ_nQ3 = 0;
  float ZJ_nwQ3 = 0; 
  int ZJ_nQ4 = 0;
  float ZJ_nwQ4 = 0; 
  float ZJ_weight =0;
 
    for (int fc = 0; fc < 21; ++fc) { 
/*  
   switch(fc)
    {
   case 0:
    fname = "/moscow31/heavynu/38x/Z0Jets_TuneZ2_7TeV-alpgen-tauola.root";
	break;
   case 1:
    fname = "/moscow31/heavynu/38x/Z1Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 2:
    fname = "/moscow31/heavynu/38x/Z1Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 3:
    fname = "/moscow31/heavynu/38x/Z1Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;	
   case 4:
    fname = "/moscow31/heavynu/38x/Z1Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 5:
    fname = "/moscow31/heavynu/38x/Z2Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola.root";
	break;
   case 6:
    fname = "/moscow31/heavynu/38x/Z2Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 7:
    fname = "/moscow31/heavynu/38x/Z2Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 8:
    fname = "/moscow31/heavynu/38x/Z2Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;	
   case 9:
    fname = "/moscow31/heavynu/38x/Z3Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 10:
    fname = "/moscow31/heavynu/38x/Z3Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 11:
    fname = "/moscow31/heavynu/38x/Z3Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 12:
    fname = "/moscow31/heavynu/38x/Z3Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;	
   case 13:
    fname = "/moscow31/heavynu/38x/Z4Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 14:
    fname = "/moscow31/heavynu/38x/Z4Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 15:
    fname = "/moscow31/heavynu/38x/Z4Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 16:
    fname = "/moscow31/heavynu/38x/Z4Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 17:
	fname = "/moscow31/heavynu/38x/Z5Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 18:
    fname = "/moscow31/heavynu/38x/Z5Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 19:
    fname = "/moscow31/heavynu/38x/Z5Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 20:
    fname = "/moscow31/heavynu/38x/Z5Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
    }
	
*/

switch(fc)
    {
   case 0:
    fname = "/moscow41/heavynu/41x-background/Z0Jets_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root";
	break;
   case 1:
    fname = "/moscow41/heavynu/41x-background/Z1Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root";
    break;
   case 2:
    fname = "/moscow41/heavynu/41x-background/Z1Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root"; 
    break;
   case 3:
    fname = "/moscow41/heavynu/41x-background/Z1Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root"; 
    break;	
   case 4:
    fname = "/moscow41/heavynu/41x-background/Z1Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root"; 
    break;
   case 5:
    fname = "/moscow41/heavynu/41x-background/Z2Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root";
	break;
   case 6:
    fname = "/moscow41/heavynu/41x-background/Z2Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root";
    break;
   case 7:
    fname = "/moscow41/heavynu/41x-background/Z2Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root"; 
    break;
   case 8:
    fname = "/moscow41/heavynu/41x-background/Z2Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root"; 
    break;	
   case 9:
    fname = "/moscow41/heavynu/41x-background/Z3Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root"; 
    break;
   case 10:
    fname = "/moscow41/heavynu/41x-background/Z3Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root";
    break;
   case 11:
    fname = "/moscow41/heavynu/41x-background/Z3Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root"; 
    break;
   case 12:
    fname = "/moscow41/heavynu/41x-background/Z3Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root"; 
    break;	
   case 13:
    fname = "/moscow41/heavynu/41x-background/Z4Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root"; 
    break;
   case 14:
    fname = "/moscow41/heavynu/41x-background/Z4Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root";
    break;
   case 15:
    fname = "/moscow41/heavynu/41x-background/Z4Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root"; 
    break;
   case 16:
    fname = "/moscow41/heavynu/41x-background/Z4Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root"; 
    break;
   case 17:
	fname = "/moscow41/heavynu/41x-background/Z5Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root"; 
    break;
   case 18:
    fname = "/moscow41/heavynu/41x-background/Z5Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root";
    break;
   case 19:
    fname = "/moscow41/heavynu/41x-background/Z5Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root"; 
    break;
   case 20:
    fname = "/moscow41/heavynu/41x-background/Z5Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root"; 
    break;
    }

/*
      switch(fc)
    {
   case 0:
    fname = "/moscow41/heavynu/42x-background/ZJetToEE_Pt-30to50_TuneZ2_7TeV_pythia6_Summer11-PU_S3_START42_V11-v2_AODSIM.root";
	break;
   case 1:
    fname = "/moscow41/heavynu/42x-background/ZJetToEE_Pt-50to80_TuneZ2_7TeV_pythia6_Summer11-PU_S3_START42_V11-v2_AODSIM.root";
    break;
   case 2:
    fname = "/moscow41/heavynu/42x-background/ZJetToEE_Pt-80to120_TuneZ2_7TeV_pythia6_Summer11-PU_S3_START42_V11-v2_AODSIM.root"; 
    break;
   case 3:
    fname = "/moscow41/heavynu/42x-background/ZJetToEE_Pt-120to170_TuneZ2_7TeV_pythia6_Summer11-PU_S3_START42_V11-v2_AODSIM.root"; 
    break;	
   case 4:
    fname = "/moscow41/heavynu/42x-background/ZJetToEE_Pt-170to230_TuneZ2_7TeV_pythia6_Summer11-PU_S3_START42_V11-v2_AODSIM.root"; 
    break;
   case 5:
    fname = "/moscow41/heavynu/42x-background/ZJetToEE_Pt-230to300_TuneZ2_7TeV_pythia6_Summer11-PU_S3_START42_V11-v2_AODSIM.root";
    break;
    }
*/
   TheEvent ZJ_e(fname);
  if (!ZJ_e) return;
  
  ZJ_nEvents =ZJ_nEvents + ZJ_e.totalEvents();
  
  for (int i = 0; i < ZJ_e.totalEvents(); ++i) {
    ZJ_e.readEvent(i);
	
	if(ZJ_e.pileup.size()<25){
    ZJ_weight = ZJ_e.weight *  generate_flat10_weights(histo)[ZJ_e.pileup.size()];
    }
    else{ //should not happen as we have a weight for all simulated ZJ_e.pileup.size() multiplicities!
    ZJ_weight = 0.;
    }	
    
	ZJ_nwEvents = ZJ_nwEvents + Z_tune*ZJ_weight*A_Wei*RD_Lum/100.;
	

    
    // electron energy resolution
    for (unsigned int j = 0; j < ZJ_e.muons.size(); ++j) {
      const Particle& reco = ZJ_e.muons[j];
      if (reco.mcId >= 0) {
        const Particle& mc = ZJ_e.gens[reco.mcId];
        const float r = (reco.p4.E() - mc.p4.E())/mc.p4.E();
        ZJ_hElRes.Fill(r);
      }
    }
    

      if ((ZJ_e.muons.size() >= NofLept) && (ZJ_e.jets.size() >= NofJets)) {
	  
	   if (abs((ZJ_e.muons[0].p4).Eta())<=1.44){A_Wei=HEEP_B;} else {A_Wei=HEEP_E;}
	   if (abs((ZJ_e.muons[1].p4).Eta())<=1.44){A_Wei=A_Wei*HEEP_B;} else {A_Wei=A_Wei*HEEP_E;}
	   
    //  correction of Z mass and ID efficiency
	   if ((abs(ZJ_e.muons[0].p4.Eta())<=1.44)) {K1_Z1 = 1.;} else {K1_Z1=1.;}
	   if ((abs(ZJ_e.muons[1].p4.Eta())<=1.44)) {K1_Z2 = 1.;} else {K1_Z2=1.;}
	
	//cuts on electrons Pt>=40
	    if (((K1_Z1*K1_Z1*ZJ_e.muons[0].p4).Pt()>=Lep1Pt) && ((K1_Z2*ZJ_e.muons[1].p4).Pt()>=Lep2Pt))  { 
    //cuts on jetss Pt>=40      
	      if (((ZJ_e.jets[0].p4).Pt()>=Jet1Pt) && ((ZJ_e.jets[1].p4).Pt()>=Jet2Pt))  { 
		  
	//cuts on electrons invariant mass Mll>=400.0	
		  if (((K1_Z1*ZJ_e.muons[0].p4 + K1_Z2*ZJ_e.muons[1].p4).M()>=MllCut)&&((K1_Z1*ZJ_e.muons[0].p4 + K1_Z2*ZJ_e.muons[1].p4).M()<=MllCut1)) {
//		  if (!(((K1_Z1*ZJ_e.muons[0].p4 + K1_Z2*ZJ_e.muons[1].p4).M()>=MllCut)&&((K1_Z1*ZJ_e.muons[0].p4 + K1_Z2*ZJ_e.muons[1].p4).M()<=MllCut1))) {
			  
	//cuts on Eta  
         if (((abs(K1_Z1*ZJ_e.muons[0].p4.Eta())<=EtaCut1L) || (abs(K1_Z2*ZJ_e.muons[1].p4.Eta())<=EtaCut2L)) 
	     && (abs(ZJ_e.jets[0].p4.Eta())<=EtaCut1J) && (abs(ZJ_e.jets[1].p4.Eta())<=EtaCut2J) )        {
			
	// Isolation
	       if ((ZJ_e.muons[0].isoHEEP() <= ISO)&&(ZJ_e.muons[1].isoHEEP() <= ISO)) {
		   
	 //Cut on vertexes
            if ((abs(ZJ_e.muons[0].vertex.z()-ZJ_e.muons[1].vertex.z())<=Vertex) && (abs(ZJ_e.jets[0].vertex.z()-ZJ_e.jets[1].vertex.z())<=Vertex) 
             && ((abs(ZJ_e.muons[0].vertex.z()-ZJ_e.jets[1].vertex.z())<=Vertex) && (abs(ZJ_e.jets[0].vertex.z()-ZJ_e.muons[1].vertex.z())<=Vertex))) {
						     
	        WR = (K1_Z1*ZJ_e.muons[0].p4 + K1_Z2*ZJ_e.muons[1].p4 + ZJ_e.jets[0].p4 + ZJ_e.jets[1].p4).M();
			if (WR>=MW_Cut) {
            ZJ_hWR.Fill(WR, Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);
	  
	        tWR = (K1_Z1*ZJ_e.muons[0].p4 + K1_Z2*ZJ_e.muons[1].p4 + ZJ_e.jets[0].p4 + ZJ_e.jets[1].p4).Pt();
            ZJ_PtWR.Fill(tWR, Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);
	  
	        lWR = (K1_Z1*ZJ_e.muons[0].p4 + K1_Z2*ZJ_e.muons[1].p4 + ZJ_e.jets[0].p4 + ZJ_e.jets[1].p4).Pz();
            ZJ_PlWR.Fill(lWR, Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);
	  
	        eWR = (K1_Z1*ZJ_e.muons[0].p4 + K1_Z2*ZJ_e.muons[1].p4 + ZJ_e.jets[0].p4 + ZJ_e.jets[1].p4).Eta();
            ZJ_EtaWR.Fill(eWR, Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);
	  
	        pWR = (K1_Z1*ZJ_e.muons[0].p4 + K1_Z2*ZJ_e.muons[1].p4 + ZJ_e.jets[0].p4 + ZJ_e.jets[1].p4).Phi();
            ZJ_PhiWR.Fill(pWR, Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);
	  
            NuR_l0 = (K1_Z1*ZJ_e.muons[0].p4 + ZJ_e.jets[0].p4 + ZJ_e.jets[1].p4).M();
            NuR_l1 = (K1_Z2*ZJ_e.muons[1].p4 + ZJ_e.jets[0].p4 + ZJ_e.jets[1].p4).M();
      
	        ZJ_hNuR0.Fill(NuR_l0, Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);
	        ZJ_hNuR1.Fill(NuR_l1, Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);
	        
            Mll = (K1_Z1*ZJ_e.muons[0].p4 + K1_Z2*ZJ_e.muons[1].p4).M();
            ZJ_hMll.Fill(Mll, Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);
			ZJ_hMll1.Fill(Mll, Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);
      
            Mjj = (ZJ_e.jets[0].p4 + ZJ_e.jets[1].p4).M();
            ZJ_hMjj.Fill(Mjj, Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);
			
			Mmet = (ZJ_e.mets[0].p4).Pt();
            ZJ_hMmets.Fill(Mmet, Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);
			ZJ_hMmets1.Fill(Mmet, Z_tune*ZJ_weight*A_Wei*RD_Lum/100.); 
			
			ZJ_hbDiscr_j1.Fill(ZJ_e.jets[0].bDiscr, Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);
			ZJ_hbDiscr_j2.Fill(ZJ_e.jets[1].bDiscr, Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);
							
			ZJ_n = ZJ_n + 1;
			
			ZJ_nw = ZJ_nw + Z_tune*ZJ_weight*A_Wei*RD_Lum/100.;
			  
			if ((ZJ_e.jets[0].bDiscr >= 0.669) && (ZJ_e.jets[1].bDiscr >= 0.669)) { //  10% = 0.215      1% = 0.459    0.1% = 0.669
			ZJ_nB2 = ZJ_nB2 + 1;
			ZJ_nwB2 = ZJ_nwB2 + Z_tune*ZJ_weight*A_Wei*RD_Lum/100.; 
			ZJ_hbNum.Fill(2., Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);
			}
			if (((ZJ_e.jets[0].bDiscr >= 0.669) && (ZJ_e.jets[1].bDiscr <= 0.669)) || ((ZJ_e.jets[0].bDiscr <= 0.669) && (ZJ_e.jets[1].bDiscr >= 0.669)) ) { //  10% = 0.215      1% = 0.459    0.1% = 0.669
			ZJ_nB1 = ZJ_nB1 + 1;
			ZJ_nwB1 = ZJ_nwB1 + Z_tune*ZJ_weight*A_Wei*RD_Lum/100.;
			ZJ_hbNum.Fill(1., Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);
			}
			if ((ZJ_e.jets[0].bDiscr <= 0.669) && (ZJ_e.jets[1].bDiscr <= 0.669)) { 
			ZJ_nB3 = ZJ_nB3 + 1;
			ZJ_nwB3 = ZJ_nwB3 + Z_tune*ZJ_weight*A_Wei*RD_Lum/100.;
			ZJ_hbNum.Fill(0., Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);
			}
			
			if (ZJ_e.muons[0].charge*ZJ_e.muons[1].charge == -1) {
            ZJ_nQ = ZJ_nQ+1;			
			ZJ_nwQ = ZJ_nwQ + Z_tune*ZJ_weight*A_Wei*RD_Lum/100.; 
			ZJ_hQNum.Fill(-0.5, Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);}
			else {
			ZJ_nQ1 = ZJ_nQ1+1;
			ZJ_nwQ1 = ZJ_nwQ1 + Z_tune*ZJ_weight*A_Wei*RD_Lum/100.;
			ZJ_hQNum.Fill(0.5, Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);}
			
			if ((ZJ_e.muons[0].charge==-1)&&(ZJ_e.muons[1].charge == -1)) {
            ZJ_nQ3 = ZJ_nQ3+1;			
			ZJ_nwQ3 = ZJ_nwQ3 + Z_tune*ZJ_weight*A_Wei*RD_Lum/100.;}
			
			if ((ZJ_e.muons[0].charge==1)&&(ZJ_e.muons[1].charge == 1)) {
            ZJ_nQ4 = ZJ_nQ4+1;			
			ZJ_nwQ4 = ZJ_nwQ4 + Z_tune*ZJ_weight*A_Wei*RD_Lum/100.;}
			
			
			ZJ_dVz.Fill((abs(ZJ_e.muons[0].vertex.z()-ZJ_e.muons[1].vertex.z())), Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);
			ZJ_dVz1.Fill(abs(ZJ_e.jets[0].vertex.z()-ZJ_e.jets[1].vertex.z()), Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);
			ZJ_dVz2.Fill(abs(ZJ_e.jets[0].vertex.z()-ZJ_e.muons[0].vertex.z()), Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);
			ZJ_dVz3.Fill(abs(ZJ_e.jets[1].vertex.z()-ZJ_e.muons[1].vertex.z()), Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);
			ZJ_dVz4.Fill(abs(ZJ_e.jets[1].vertex.z()-ZJ_e.muons[0].vertex.z()), Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);
			ZJ_dVz5.Fill(abs(ZJ_e.jets[0].vertex.z()-ZJ_e.muons[1].vertex.z()), Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);
			
			ZJ_PU.Fill(ZJ_e.vertices.size(), Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);
			ZJ_PU1.Fill(ZJ_e.pileup.size(), Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);
			ZJ_hZPt.Fill((K1_Z1*ZJ_e.muons[0].p4 + K1_Z2*ZJ_e.muons[1].p4).Pt(), Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);
			
			
		/*	
			cout<<"New event!" << n <<" PtL1 = " <<(K1_Z1*ZJ_e.muons[0].p4).Pt()<<" PtL2 = "<<(K1_Z2*ZJ_e.muons[1].p4).Pt() << " Mll = "<<(K1_Z1*ZJ_e.muons[0].p4 + K1_Z2*ZJ_e.muons[1].p4).M() << endl;
			cout<<" PtJ1 = "<<(ZJ_e.jets[0].p4).Pt()<<" bDiscr J1 = "<< ZJ_e.jets[0].bDiscr <<" PtJ2 = "<<(ZJ_e.jets[1].p4).Pt()<<" bDiscr J2 = "<< ZJ_e.jets[1].bDiscr <<endl;
			cout<<"EtaL1 = "<<(K1_Z1*ZJ_e.muons[0].p4).Eta()<<" PhiL1 = "<<(K1_Z1*ZJ_e.muons[0].p4).Phi()<<" EtaL2 = "<<(K1_Z2*ZJ_e.muons[1].p4).Eta()<<" PhiL2 = "<<(K1_Z2*ZJ_e.muons[1].p4).Phi() << endl;
			cout<<"EtaJ1 = "<<(ZJ_e.jets[0].p4).Eta()<<" PhiJ1 = "<<(ZJ_e.jets[0].p4).Phi()<<" EtaJ2 = "<<(ZJ_e.jets[1].p4).Eta()<<" PhiJ2 = "<<(ZJ_e.jets[1].p4).Phi() << endl;			
			
	        cout<<"vzL1 = "<<(ZJ_e.muons[0].vertex).z()<<" VzL2 = "<<(ZJ_e.muons[1].vertex).z()<<" VzJ1 = "<<(ZJ_e.jets[0].vertex).z()<<" VzJ2 = "<<(ZJ_e.jets[1].vertex).z() << endl;
		
	    	cout<<" L1: ecalIso = " <<ZJ_e.muons[0].ecalIso <<" hcalIso = "<<ZJ_e.muons[0].hcalIso <<" hcal1Iso = "<<ZJ_e.muons[0].hcal1Iso << " trackIso = "<< ZJ_e.muons[0].trackIso << endl; 
					
			cout<<" L1: charge = " <<ZJ_e.muons[0].charge << endl; 
	
	       cout<<" L1: ecalIso = " <<ZJ_e.muons[1].ecalIso <<" hcalIso = "<<ZJ_e.muons[1].hcalIso <<" hcal1Iso = "<<ZJ_e.muons[1].hcal1Iso << " trackIso = "<< ZJ_e.muons[1].trackIso << endl; 
			
			cout<<" L2: charge = " <<ZJ_e.muons[1].charge << endl;
							
			cout<<"MWR = "<< WR <<" MNuR = "<< NuR_l1 <<" Mjj = "<< Mjj << endl;
			cout <<"RUN = "<< ZJ_e.irun << " LUMI = " << ZJ_e.ilumi << " EVENT = "<< ZJ_e.ievent<< endl;
		    
			
        */  	
		}
	    }	
	    }	
        } 	
        }
	   }
	  }
	 }

    // Multiplicity
    ZJ_hElMult.Fill(ZJ_e.muons.size());
    ZJ_hJetMult.Fill(ZJ_e.jets.size());    
	    

  }  
}
 cout << "MC BG Z+JETS" << endl;
 cout << endl;
 cout << "Total events = " << ZJ_nEvents << endl;
 cout << "Total weight events = " << ZJ_nwEvents << endl;
 cout << endl; 
 cout << "Number of passed events = "<<ZJ_n<<"   "<<ZJ_nw<< endl;
 cout << endl;
 cout << "Number of events with 0 b-jet = " << ZJ_nB3<<"   " <<ZJ_nwB3 << endl; 
 cout << "Number of events with 1 b-jet = " << ZJ_nB1<<"   " <<ZJ_nwB1 << endl;
 cout << "Number of events with 2 b-jet = " << ZJ_nB2<<"   " <<ZJ_nwB2 << endl;	
 cout << endl;	
 cout << "Number of events with same charges of lepton = " << ZJ_nQ1<<"   " <<ZJ_nwQ1 << endl;
 cout << "Number of events with opposite charges of lepton = " << ZJ_nQ <<"   " <<ZJ_nwQ << endl; 
 cout << "Number of events with -1 charges of lepton = " << ZJ_nQ3 <<"   " <<ZJ_nwQ3 << endl; 
 cout << "Number of events with +1 charges of lepton = " << ZJ_nQ4 <<"   " <<ZJ_nwQ4 << endl; 
 cout << endl;

 BG_nw = BG_nw + ZJ_nw; // number of total BG events 
 
}

  
//============================================================================================================================
 
  
// MC BG WJ


  TH1F WJ_hWR(  "WJ_WR", "W_{R} reconstructed mass;mass, GeV/c^{2};events/50 GeV", M_Delta_M_W, M_min_M_W, M_max_M_W);
  TH1F WJ_hNuR0("WJ_NuR0", "nu_{R} reconstructed mass using first E electron;mass, GeV/c^{2};events/50 GeV", M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu);
  TH1F WJ_hNuR1("WJ_NuR1", "nu_{R} reconstructed mass using second E electron;mass, GeV/c^{2};events/50 GeV", M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu); 
  TH1F WJ_hMll("WJ_Mll", "lepton-lepton pair invariant mass;mass, GeV/c^{2};events/2 GeV", M_Delta_Mll, M_min_Mll, M_max_Mll);
  TH1F WJ_hMll1("WJ_Mll1", "lepton-lepton pair invariant mass;mass, GeV/c^{2};events/40 GeV", 20, 100, 1000);
  TH1F WJ_hMjj("WJ_Mjj", "jet-jet pair invariant mass;mass, GeV/c^{2};events/50 GeV", M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu);
  TH1F WJ_hMmets("WJ_Mmets", "Miss Et; Et, GeV/c^{2};events/10 GeV", M_Delta_MET, M_min_MET, M_max_MET);
  TH1F WJ_hMmets1("WJ_Mmets1", "Miss Et; Et, GeV/c^{2};events/30 GeV", 30, 100, 1000); 
  TH1F WJ_PtWR(  "WJ_PtWR", "W_{R} Pt; Pt, GeV/c^{2}; events/6GeV", 30, 0, 180);
  TH1F WJ_PlWR(  "WJ_PlWR", "W_{R} Pl;Pl, GeV/c^{2};events/50 GeV", 60, -1500, 1500);
  TH1F WJ_EtaWR( "WJ_EtaWR", "W_{R} Eta; Eta; events", 10, -6, 6);
  TH1F WJ_PhiWR( "WJ_PhiWR", "W_{R} Phi; Phi; events", 10, -3.1415, 3.1415); 
  TH1F WJ_hbDiscr_j1( "WJ_bDiscrJ1", "first jet b Discriminant; bDiscr; events", 15, 0, 1.5); 
  TH1F WJ_hbDiscr_j2( "WJ_bDiscrJ2", "second jet b Discriminant; bDiscr; events", 15, 0, 1.5); 
  TH1F WJ_hbNum( "WJ_hbNum", "Number of b jets ; bNum; events", 3, 0, 3); 
  TH1F WJ_hQNum( "WJ_hQNum", "Mult of leptons charges ; e_1*e_2; events", 2, -1, 1); 
  TH1F WJ_hElMult("WJ_ElMult", "electrons multiplicity", 10, 0, 10);
       WJ_hElMult.GetXaxis()->SetNdivisions(110);
  TH1F WJ_hJetMult("WJ_JetMult", "jets multiplicity", 20, 0, 20);
       WJ_hJetMult.GetXaxis()->SetNdivisions(210);   
  TH1F WJ_hElRes("WJ_ElRes", "electron energy reconstruction resolution", 100, -0.3, 0.3);
  
  TH1F WJ_dVz( "WJ_dVz", "delta Vz; #delta V_{z} between ee, cm; Events", 20, 0, 0.1); 
  TH1F WJ_dVz1( "WJ_dVz1", "delta Vz;#delta V_{z} between jj, cm; Events", 20, 0, 0.1);
  TH1F WJ_dVz2( "WJ_dVz2", "delta Vz;#delta V_{z} between e_{1}j_{1}, cm; Events", 20, 0, 0.1);  
  TH1F WJ_dVz3( "WJ_dVz3", "delta Vz;#delta V_{z} between e_{2}j_{2}, cm; Events", 20, 0, 0.1); 
  TH1F WJ_dVz4( "WJ_dVz4", "delta Vz;#delta V_{z} between e_{2}j_{1}, cm; Events", 20, 0, 0.1);  
  TH1F WJ_dVz5( "WJ_dVz5", "delta Vz;#delta V_{z} between e_{1}j_{2}, cm; Events", 20, 0, 0.1); 
  
  TH1F WJ_PU( "WJ_PU", "PU; Primary vertex number ; Events", 25, 0, 25); 
  TH1F WJ_PU1( "WJ_PU1", "PU1; Primary vertex number ; Events", 25, 0, 25); 
  TH1F WJ_hZPt("WJ_ZPt", "Z+jets; Z Pt, GeV/c;Events/20 GeV/c", 10, 0, 200);
    	
  //fname = "/moscow41/heavynu/41x-background/.root";

fname = "/moscow31/heavynu/38x/WJetsToLNu_TuneZ2_7TeV-madgraph-tauola.root";
 
  TheEvent WJ_e(fname);
  if (!WJ_e) return;
  int WJ_n = 0; 
  float WJ_nw = 0.;  
  
if (WJ_incl == true) {

  int WJ_nB1 = 0; 
  float WJ_nwB1 = 0;
  int WJ_nB2 = 0;
  float WJ_nwB2 = 0;  
  int WJ_nB3 = 0;
  float WJ_nwB3 = 0;
  int WJ_nEvents = 0; 
  float WJ_nwEvents = 0;  
  int WJ_nQ = 0;
  float WJ_nwQ = 0;
  int WJ_nQ1 = 0;
  float WJ_nwQ1 = 0;
  int WJ_nQ3 = 0;
  float WJ_nwQ3 = 0;
  int WJ_nQ4 = 0;
  float WJ_nwQ4 = 0;
  float WJ_weight =0;
  

  WJ_nEvents = WJ_e.totalEvents();
 
  for (int i = 0; i < WJ_nEvents; ++i) {
    WJ_e.readEvent(i);  
/*
      if(WJ_e.pileup.size()<25){
        WJ_weight = WJ_e.weight *  generate_flat10_weights(histo)[WJ_e.pileup.size()];
        }
        else{ //should not happen as we have a weight for all simulated WJ_e.pileup.size() multiplicities!
        WJ_weight = 0.;
        }
*/
        WJ_weight = WJ_e.weight;

		WJ_nwEvents = WJ_nwEvents + WJ_tune*WJ_weight*RD_Lum/100.;  
   
    // electron energy resolution
    for (unsigned int j = 0; j < WJ_e.muons.size(); ++j) {
      const Particle& reco = WJ_e.muons[j];
      if (reco.mcId >= 0) {
        const Particle& mc = WJ_e.gens[reco.mcId];
        const float r = (reco.p4.E() - mc.p4.E())/mc.p4.E();
        WJ_hElRes.Fill(r, WJ_tune*WJ_weight*A_Wei*RD_Lum/100.);
      }
    }
    
    // mass distributions
      if ((WJ_e.muons.size() >= NofLept) && (WJ_e.jets.size() >= NofJets)) {
	  
    //  correction of Z mass and ID efficiency
	   if ((abs(WJ_e.muons[0].p4.Eta())<=1.44)) {K1_Z1 = 1.;} else {K1_Z1=1.;}
	   if ((abs(K1_Z2*WJ_e.muons[1].p4.Eta())<=1.44)) {K1_Z2 = 1.;} else {K1_Z2=1.;}
	  
	   if (abs((K1_Z1*WJ_e.muons[0].p4).Eta())<=1.44){A_Wei=HEEP_B;} else {A_Wei=HEEP_E;}
	   if (abs((K1_Z2*WJ_e.muons[1].p4).Eta())<=1.44){A_Wei=A_Wei*HEEP_B;} else {A_Wei=A_Wei*HEEP_E;}
	
	//cuts on electrons Pt>=40
	    if (((K1_Z1*WJ_e.muons[0].p4).Pt()>=Lep1Pt) && ((K1_Z2*WJ_e.muons[1].p4).Pt()>=Lep2Pt))  { 
    //cuts on jetss Pt>=40      
	      if (((WJ_e.jets[0].p4).Pt()>=Jet1Pt) && ((WJ_e.jets[1].p4).Pt()>=Jet2Pt))  { 
		  
	//cuts on electrons invariant mass Mll>=400.0	
	  if (((K1_Z1*WJ_e.muons[0].p4 + K1_Z2*WJ_e.muons[1].p4).M()>=MllCut)&& ((K1_Z1*WJ_e.muons[0].p4 + K1_Z2*WJ_e.muons[1].p4).M()<=MllCut1)) {
//	  if (!(((K1_Z1*WJ_e.muons[0].p4 + K1_Z2*WJ_e.muons[1].p4).M()>=MllCut)&& ((K1_Z1*WJ_e.muons[0].p4 + K1_Z2*WJ_e.muons[1].p4).M()<=MllCut1))) {
			  
	//cuts on Eta  
         if (((abs(K1_Z1*WJ_e.muons[0].p4.Eta())<=EtaCut1L) || (abs(K1_Z2*WJ_e.muons[1].p4.Eta())<=EtaCut2L)) 
	     && (abs(WJ_e.jets[0].p4.Eta())<=EtaCut1J) && (abs(WJ_e.jets[1].p4.Eta())<=EtaCut2J) )        {
			
	// Isolation
	       if ((WJ_e.muons[0].isoHEEP() <= ISO)&&(WJ_e.muons[1].isoHEEP() <= ISO)) {
		   
	 //Cut on vertexes
             if ((abs(WJ_e.muons[0].vertex.z()-WJ_e.muons[1].vertex.z())<=Vertex) && (abs(WJ_e.jets[0].vertex.z()-WJ_e.jets[1].vertex.z())<=Vertex) 
             && ((abs(WJ_e.muons[0].vertex.z()-WJ_e.jets[1].vertex.z())<=Vertex) && (abs(WJ_e.jets[0].vertex.z()-WJ_e.muons[1].vertex.z())<=Vertex))) {
			 
	        WR = (K1_Z1*WJ_e.muons[0].p4 + K1_Z2*WJ_e.muons[1].p4 + WJ_e.jets[0].p4 + WJ_e.jets[1].p4).M();
			if (WR>=MW_Cut) {
            WJ_hWR.Fill(WR, WJ_tune*WJ_weight*A_Wei*RD_Lum/100.);
	  
	        tWR = (K1_Z1*WJ_e.muons[0].p4 + K1_Z2*WJ_e.muons[1].p4 + WJ_e.jets[0].p4 + WJ_e.jets[1].p4).Pt();
            WJ_PtWR.Fill(tWR, WJ_tune*WJ_weight*A_Wei*RD_Lum/100.);
	  
	        lWR = (K1_Z1*WJ_e.muons[0].p4 + K1_Z2*WJ_e.muons[1].p4 + WJ_e.jets[0].p4 + WJ_e.jets[1].p4).Pz();
            WJ_PlWR.Fill(lWR, WJ_tune*WJ_weight*A_Wei*RD_Lum/100.);
	  
	        eWR = (K1_Z1*WJ_e.muons[0].p4 + K1_Z2*WJ_e.muons[1].p4 + WJ_e.jets[0].p4 + WJ_e.jets[1].p4).Eta();
            WJ_EtaWR.Fill(eWR, WJ_tune*WJ_weight*A_Wei*RD_Lum/100.);
	  
	        pWR = (K1_Z1*WJ_e.muons[0].p4 + K1_Z2*WJ_e.muons[1].p4 + WJ_e.jets[0].p4 + WJ_e.jets[1].p4).Phi();
            WJ_PhiWR.Fill(pWR, WJ_tune*WJ_weight*A_Wei*RD_Lum/100.);
	  
            NuR_l0 = (K1_Z1*WJ_e.muons[0].p4 + WJ_e.jets[0].p4 + WJ_e.jets[1].p4).M();
            NuR_l1 = (K1_Z2*WJ_e.muons[1].p4 + WJ_e.jets[0].p4 + WJ_e.jets[1].p4).M();
      
	        WJ_hNuR0.Fill(NuR_l0, WJ_tune*WJ_weight*A_Wei*RD_Lum/100.);
	        WJ_hNuR1.Fill(NuR_l1, WJ_tune*WJ_weight*A_Wei*RD_Lum/100.);
	        
            Mll = (K1_Z1*WJ_e.muons[0].p4 + K1_Z2*WJ_e.muons[1].p4).M();
            WJ_hMll.Fill(Mll, WJ_tune*WJ_weight*A_Wei*RD_Lum/100.);
			WJ_hMll1.Fill(Mll, WJ_tune*WJ_weight*A_Wei*RD_Lum/100.);
      
            Mjj = (WJ_e.jets[0].p4 + WJ_e.jets[1].p4).M();
            WJ_hMjj.Fill(Mjj, WJ_tune*WJ_weight*A_Wei*RD_Lum/100.);
			
			Mmet = (WJ_e.mets[0].p4).Pt();
            WJ_hMmets.Fill(Mmet, WJ_tune*WJ_weight*A_Wei*RD_Lum/100.);
		    WJ_hMmets1.Fill(Mmet, WJ_tune*WJ_weight*A_Wei*RD_Lum/100.);
			
			WJ_hbDiscr_j1.Fill(WJ_e.jets[0].bDiscr, WJ_tune*WJ_weight*A_Wei*RD_Lum/100.);
			WJ_hbDiscr_j2.Fill(WJ_e.jets[1].bDiscr, WJ_tune*WJ_weight*A_Wei*RD_Lum/100.);
							
			WJ_n = WJ_n + 1;
			WJ_nw = WJ_nw + WJ_tune*WJ_weight*A_Wei*RD_Lum/100.; 
						
			if ((WJ_e.jets[0].bDiscr >= 0.669) && (WJ_e.jets[1].bDiscr >= 0.669)) { //  10% = 0.215      1% = 0.459    0.1% = 0.669
			WJ_nwB2 = WJ_nwB2 + WJ_tune*WJ_weight*A_Wei*RD_Lum/100.; 
			WJ_nB2 = WJ_nB2 + 1;
			WJ_hbNum.Fill(2., WJ_tune*WJ_weight*A_Wei*RD_Lum/100.);
			}
			if (((WJ_e.jets[0].bDiscr >= 0.669) && (WJ_e.jets[1].bDiscr <= 0.669)) || ((WJ_e.jets[0].bDiscr <= 0.669) && (WJ_e.jets[1].bDiscr >= 0.669)) ) { //  10% = 0.215      1% = 0.459    0.1% = 0.669
			WJ_nwB1 = WJ_nwB1 + WJ_tune*WJ_weight*A_Wei*RD_Lum/100.; 
			WJ_nB1 = WJ_nB1 + 1;
			WJ_hbNum.Fill(1., WJ_tune*WJ_weight*A_Wei*RD_Lum/100.);
			}
			if ((WJ_e.jets[0].bDiscr <= 0.669) && (WJ_e.jets[1].bDiscr <= 0.669)) {
            WJ_nwB3 = WJ_nwB3 + WJ_tune*WJ_weight*A_Wei*RD_Lum/100.; 			
			WJ_nB3 = WJ_nB3 + 1;
			WJ_hbNum.Fill(0., WJ_tune*WJ_weight*A_Wei*RD_Lum/100.);
			}
			
			if (WJ_e.muons[0].charge*WJ_e.muons[1].charge == -1) {
			WJ_nwQ = WJ_nwQ + WJ_tune*WJ_weight*A_Wei*RD_Lum/100.; 
			WJ_nQ = WJ_nQ +1; 
			WJ_hQNum.Fill(-0.5, WJ_tune*WJ_weight*A_Wei*RD_Lum/100.);}
			else {
			WJ_nwQ1 = WJ_nwQ1 + WJ_tune*WJ_weight*A_Wei*RD_Lum/100.;
			WJ_nQ1 = WJ_nQ1 +1;
			WJ_hQNum.Fill(0.5, WJ_tune*WJ_weight*A_Wei*RD_Lum/100.);}
			
			
			if ((WJ_e.muons[0].charge==-1)&&(WJ_e.muons[1].charge == -1)) {
			WJ_nwQ3 = WJ_nwQ3 + WJ_tune*WJ_weight*A_Wei*RD_Lum/100.;
			WJ_nQ3 = WJ_nQ3 +1;}
			
			if ((WJ_e.muons[0].charge==1)&&(WJ_e.muons[1].charge == 1)) {
            WJ_nwQ4 = WJ_nwQ4 + WJ_tune*WJ_weight*A_Wei*RD_Lum/100.;			
			WJ_nQ4 = WJ_nQ4 +1;}
			
			
			WJ_dVz.Fill((abs(WJ_e.muons[0].vertex.z()-WJ_e.muons[1].vertex.z())), WJ_tune*WJ_weight*A_Wei*RD_Lum/100.);
			WJ_dVz1.Fill(abs(WJ_e.jets[0].vertex.z()-WJ_e.jets[1].vertex.z()), WJ_tune*WJ_weight*A_Wei*RD_Lum/100.);
			WJ_dVz2.Fill(abs(WJ_e.jets[0].vertex.z()-WJ_e.muons[0].vertex.z()), WJ_tune*WJ_weight*A_Wei*RD_Lum/100.);
			WJ_dVz3.Fill(abs(WJ_e.jets[1].vertex.z()-WJ_e.muons[1].vertex.z()), WJ_tune*WJ_weight*A_Wei*RD_Lum/100.);
			WJ_dVz4.Fill(abs(WJ_e.jets[1].vertex.z()-WJ_e.muons[0].vertex.z()), WJ_tune*WJ_weight*A_Wei*RD_Lum/100.);
			WJ_dVz5.Fill(abs(WJ_e.jets[0].vertex.z()-WJ_e.muons[1].vertex.z()), WJ_tune*WJ_weight*A_Wei*RD_Lum/100.);
	
			WJ_PU.Fill(WJ_e.vertices.size(), WJ_tune*WJ_weight*A_Wei*RD_Lum/100.);
			WJ_PU1.Fill(WJ_e.pileup.size(), WJ_tune*WJ_weight*A_Wei*RD_Lum/100.);
			WJ_hZPt.Fill((K1_Z1*WJ_e.muons[0].p4 + K1_Z2*WJ_e.muons[1].p4).Pt(), WJ_tune*WJ_weight*A_Wei*RD_Lum/100.);
		
		
		/*	
			cout<<"New event!" << n <<" PtL1 = " <<(K1_Z1*WJ_e.muons[0].p4).Pt()<<" PtL2 = "<<(K1_Z2*WJ_e.muons[1].p4).Pt() << " Mll = "<<(K1_Z1*WJ_e.muons[0].p4 + K1_Z2*WJ_e.muons[1].p4).M() << endl;
			cout<<" PtJ1 = "<<(WJ_e.jets[0].p4).Pt()<<" bDiscr J1 = "<< WJ_e.jets[0].bDiscr <<" PtJ2 = "<<(WJ_e.jets[1].p4).Pt()<<" bDiscr J2 = "<< WJ_e.jets[1].bDiscr <<endl;
			cout<<"EtaL1 = "<<(K1_Z1*WJ_e.muons[0].p4).Eta()<<" PhiL1 = "<<(K1_Z1*WJ_e.muons[0].p4).Phi()<<" EtaL2 = "<<(K1_Z2*WJ_e.muons[1].p4).Eta()<<" PhiL2 = "<<(K1_Z2*WJ_e.muons[1].p4).Phi() << endl;
			cout<<"EtaJ1 = "<<(WJ_e.jets[0].p4).Eta()<<" PhiJ1 = "<<(WJ_e.jets[0].p4).Phi()<<" EtaJ2 = "<<(WJ_e.jets[1].p4).Eta()<<" PhiJ2 = "<<(WJ_e.jets[1].p4).Phi() << endl;			
			
	        cout<<"vzL1 = "<<(WJ_e.muons[0].vertex).z()<<" VzL2 = "<<(WJ_e.muons[1].vertex).z()<<" VzJ1 = "<<(WJ_e.jets[0].vertex).z()<<" VzJ2 = "<<(WJ_e.jets[1].vertex).z() << endl;
		
	    	cout<<" L1: ecalIso = " <<WJ_e.muons[0].ecalIso <<" hcalIso = "<<WJ_e.muons[0].hcalIso <<" hcal1Iso = "<<WJ_e.muons[0].hcal1Iso << " trackIso = "<< WJ_e.muons[0].trackIso << endl; 
					
			cout<<" L1: charge = " <<WJ_e.muons[0].charge << endl; 
	
	       cout<<" L1: ecalIso = " <<WJ_e.muons[1].ecalIso <<" hcalIso = "<<WJ_e.muons[1].hcalIso <<" hcal1Iso = "<<WJ_e.muons[1].hcal1Iso << " trackIso = "<< WJ_e.muons[1].trackIso << endl; 
			
			cout<<" L2: charge = " <<WJ_e.muons[1].charge << endl;
							
			cout<<"MWR = "<< WR <<" MNuR = "<< NuR_l1 <<" Mjj = "<< Mjj << endl;
			cout <<"RUN = "<< WJ_e.irun << " LUMI = " << WJ_e.ilumi << " EVENT = "<< WJ_e.ievent<< endl;
		    
			
        */  
		}
		}	
		}	
       } 	
        }
	   }
	  }
	 }  
	// Multiplicity
    WJ_hElMult.Fill(WJ_e.muons.size(),WJ_tune*WJ_weight*RD_Lum/100.);
    WJ_hJetMult.Fill(WJ_e.jets.size(),WJ_tune*WJ_weight*RD_Lum/100.);	
	    

  } 
 BG_nw = BG_nw + WJ_nw; // number of total BG events
 delta_BG_nw_sq = delta_BG_nw_sq + (0.20*0.20 + 1/(WJ_n+1))*WJ_nw*WJ_nw ; // 20% TTb systematic BG error 

 cout << "MC BG WJ" << endl;
 cout << endl;
 cout << "Total events = " << WJ_nEvents << endl;
 cout << "Total weight events = " << WJ_nwEvents << endl;
 cout << endl; 
 cout << "Number of passed events = "<<WJ_n<<"   "<<WJ_nw<< endl;
 cout << endl;
 cout << "Number of events with 0 b-jet = " << WJ_nB3<<"   " <<WJ_nwB3 << endl; 
 cout << "Number of events with 1 b-jet = " << WJ_nB1<<"   " <<WJ_nwB1 << endl;
 cout << "Number of events with 2 b-jet = " << WJ_nB2<<"   " <<WJ_nwB2 << endl;	
 cout << endl;	
 cout << "Number of events with same charges of lepton = " << WJ_nQ1<<"   " <<WJ_nwQ1 << endl;
 cout << "Number of events with opposite charges of lepton = " << WJ_nQ <<"   " <<WJ_nwQ << endl; 
 cout << endl;
 cout << "Number of events with e-e- = " << WJ_nQ3<<"   " <<WJ_nwQ3 << endl;
 cout << "Number of events with e+e+ = " << WJ_nQ4 <<"   " <<WJ_nwQ4 << endl; 
 cout << endl;
 
}

  
//============================================================================================================================
 
  
// MC BG ZZ
 
 
  TH1F ZZ_hWR(  "ZZ_WR", "W_{R} reconstructed mass;mass, GeV/c^{2};events/50 GeV", M_Delta_M_W, M_min_M_W, M_max_M_W);
  TH1F ZZ_hNuR0("ZZ_NuR0", "nu_{R} reconstructed mass using first E electron;mass, GeV/c^{2};events/50 GeV", M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu);
  TH1F ZZ_hNuR1("ZZ_NuR1", "nu_{R} reconstructed mass using second E electron;mass, GeV/c^{2};events/50 GeV", M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu); 
  TH1F ZZ_hMll("ZZ_Mll", "lepton-lepton pair invariant mass;mass, GeV/c^{2};events/2 GeV", M_Delta_Mll, M_min_Mll, M_max_Mll);
  TH1F ZZ_hMll1("ZZ_Mll1", "lepton-lepton pair invariant mass;mass, GeV/c^{2};events/40 GeV", 20, 100, 1000);
  TH1F ZZ_hMjj("ZZ_Mjj", "jet-jet pair invariant mass;mass, GeV/c^{2};events/50 GeV", M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu);
  TH1F ZZ_hMmets("ZZ_Mmets", "Miss Et; Et, GeV/c^{2};events/10 GeV", M_Delta_MET, M_min_MET, M_max_MET);
  TH1F ZZ_hMmets1("ZZ_Mmets1", "Miss Et; Et, GeV/c^{2};events/30 GeV", 30, 100, 1000); 
  TH1F ZZ_PtWR(  "ZZ_PtWR", "W_{R} Pt; Pt, GeV/c^{2}; events/6GeV", 30, 0, 180);
  TH1F ZZ_PlWR(  "ZZ_PlWR", "W_{R} Pl;Pl, GeV/c^{2};events/50 GeV", 60, -1500, 1500);
  TH1F ZZ_EtaWR( "ZZ_EtaWR", "W_{R} Eta; Eta; events", 10, -6, 6);
  TH1F ZZ_PhiWR( "ZZ_PhiWR", "W_{R} Phi; Phi; events", 10, -3.1415, 3.1415); 
  TH1F ZZ_hbDiscr_j1( "ZZ_bDiscrJ1", "first jet b Discriminant; bDiscr; events", 15, 0, 1.5); 
  TH1F ZZ_hbDiscr_j2( "ZZ_bDiscrJ2", "second jet b Discriminant; bDiscr; events", 15, 0, 1.5); 
  TH1F ZZ_hbNum( "ZZ_hbNum", "Number of b jets ; bNum; events", 3, 0, 3); 
  TH1F ZZ_hQNum( "ZZ_hQNum", "Mult of leptons charges ; e_1*e_2; events", 2, -1, 1); 
  TH1F ZZ_hElMult("ZZ_ElMult", "electrons multiplicity", 10, 0, 10);
       ZZ_hElMult.GetXaxis()->SetNdivisions(110);
  TH1F ZZ_hJetMult("ZZ_JetMult", "jets multiplicity", 20, 0, 20);
       ZZ_hJetMult.GetXaxis()->SetNdivisions(210);   
  TH1F ZZ_hElRes("ZZ_ElRes", "electron energy reconstruction resolution", 100, -0.3, 0.3);
  
  TH1F ZZ_dVz( "ZZ_dVz", "delta Vz; #delta V_{z} between ee, cm; Events", 20, 0, 0.1); 
  TH1F ZZ_dVz1( "ZZ_dVz1", "delta Vz;#delta V_{z} between jj, cm; Events", 20, 0, 0.1);
  TH1F ZZ_dVz2( "ZZ_dVz2", "delta Vz;#delta V_{z} between e_{1}j_{1}, cm; Events", 20, 0, 0.1);  
  TH1F ZZ_dVz3( "ZZ_dVz3", "delta Vz;#delta V_{z} between e_{2}j_{2}, cm; Events", 20, 0, 0.1); 
  TH1F ZZ_dVz4( "ZZ_dVz4", "delta Vz;#delta V_{z} between e_{2}j_{1}, cm; Events", 20, 0, 0.1);  
  TH1F ZZ_dVz5( "ZZ_dVz5", "delta Vz;#delta V_{z} between e_{1}j_{2}, cm; Events", 20, 0, 0.1); 
  
  TH1F ZZ_PU( "ZZ_PU", "PU; Primary vertex number ; Events", 25, 0, 25); 
  TH1F ZZ_PU1( "ZZ_PU1", "PU1; Primary vertex number ; Events", 25, 0, 25); 
  TH1F ZZ_hZPt("ZZ_ZPt", "Z+jets; Z Pt, GeV/c;Events/20 GeV/c", 10, 0, 200);
    	
  fname = "/moscow41/heavynu/41x-background/ZZtoAnything_TuneZ2_7TeV-pythia6-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root";


 
  TheEvent ZZ_e(fname);
  if (!ZZ_e) return;
  int ZZ_n = 0; 
  float ZZ_nw = 0.;  
  
if (ZZ_incl == true) {

  int ZZ_nB1 = 0; 
  float ZZ_nwB1 = 0;
  int ZZ_nB2 = 0;
  float ZZ_nwB2 = 0;  
  int ZZ_nB3 = 0;
  float ZZ_nwB3 = 0;
  int ZZ_nEvents = 0; 
  float ZZ_nwEvents = 0;  
  int ZZ_nQ = 0;
  float ZZ_nwQ = 0;
  int ZZ_nQ1 = 0;
  float ZZ_nwQ1 = 0;
  int ZZ_nQ3 = 0;
  float ZZ_nwQ3 = 0;
  int ZZ_nQ4 = 0;
  float ZZ_nwQ4 = 0;
  float ZZ_weight =0;
  

  ZZ_nEvents = ZZ_e.totalEvents();
 
  for (int i = 0; i < ZZ_nEvents; ++i) {
    ZZ_e.readEvent(i);  

      if(ZZ_e.pileup.size()<25){
        ZZ_weight = ZZ_e.weight *  generate_flat10_weights(histo)[ZZ_e.pileup.size()];
        }
        else{ //should not happen as we have a weight for all simulated ZZ_e.pileup.size() multiplicities!
        ZZ_weight = 0.;
        }

		ZZ_nwEvents = ZZ_nwEvents + ZZ_tune*ZZ_weight*RD_Lum/100.;  
   
    // electron energy resolution
    for (unsigned int j = 0; j < ZZ_e.muons.size(); ++j) {
      const Particle& reco = ZZ_e.muons[j];
      if (reco.mcId >= 0) {
        const Particle& mc = ZZ_e.gens[reco.mcId];
        const float r = (reco.p4.E() - mc.p4.E())/mc.p4.E();
        ZZ_hElRes.Fill(r, ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.);
      }
    }
    
    // mass distributions
      if ((ZZ_e.muons.size() >= NofLept) && (ZZ_e.jets.size() >= NofJets)) {
	  
    //  correction of Z mass and ID efficiency
	   if ((abs(ZZ_e.muons[0].p4.Eta())<=1.44)) {K1_Z1 = 1.;} else {K1_Z1=1.;}
	   if ((abs(K1_Z2*ZZ_e.muons[1].p4.Eta())<=1.44)) {K1_Z2 = 1.;} else {K1_Z2=1.;}
	  
	   if (abs((K1_Z1*ZZ_e.muons[0].p4).Eta())<=1.44){A_Wei=HEEP_B;} else {A_Wei=HEEP_E;}
	   if (abs((K1_Z2*ZZ_e.muons[1].p4).Eta())<=1.44){A_Wei=A_Wei*HEEP_B;} else {A_Wei=A_Wei*HEEP_E;}
	
	//cuts on electrons Pt>=40
	    if (((K1_Z1*ZZ_e.muons[0].p4).Pt()>=Lep1Pt) && ((K1_Z2*ZZ_e.muons[1].p4).Pt()>=Lep2Pt))  { 
    //cuts on jetss Pt>=40      
	      if (((ZZ_e.jets[0].p4).Pt()>=Jet1Pt) && ((ZZ_e.jets[1].p4).Pt()>=Jet2Pt))  { 
		  
	//cuts on electrons invariant mass Mll>=400.0	
	  if (((K1_Z1*ZZ_e.muons[0].p4 + K1_Z2*ZZ_e.muons[1].p4).M()>=MllCut)&& ((K1_Z1*ZZ_e.muons[0].p4 + K1_Z2*ZZ_e.muons[1].p4).M()<=MllCut1)) {
//	  if (!(((K1_Z1*ZZ_e.muons[0].p4 + K1_Z2*ZZ_e.muons[1].p4).M()>=MllCut)&& ((K1_Z1*ZZ_e.muons[0].p4 + K1_Z2*ZZ_e.muons[1].p4).M()<=MllCut1))) {
			  
	//cuts on Eta  
         if (((abs(K1_Z1*ZZ_e.muons[0].p4.Eta())<=EtaCut1L) || (abs(K1_Z2*ZZ_e.muons[1].p4.Eta())<=EtaCut2L)) 
	     && (abs(ZZ_e.jets[0].p4.Eta())<=EtaCut1J) && (abs(ZZ_e.jets[1].p4.Eta())<=EtaCut2J) )        {
			
	// Isolation
	       if ((ZZ_e.muons[0].isoHEEP() <= ISO)&&(ZZ_e.muons[1].isoHEEP() <= ISO)) {
		   
	 //Cut on vertexes
             if ((abs(ZZ_e.muons[0].vertex.z()-ZZ_e.muons[1].vertex.z())<=Vertex) && (abs(ZZ_e.jets[0].vertex.z()-ZZ_e.jets[1].vertex.z())<=Vertex) 
             && ((abs(ZZ_e.muons[0].vertex.z()-ZZ_e.jets[1].vertex.z())<=Vertex) && (abs(ZZ_e.jets[0].vertex.z()-ZZ_e.muons[1].vertex.z())<=Vertex))) {
			 
	        WR = (K1_Z1*ZZ_e.muons[0].p4 + K1_Z2*ZZ_e.muons[1].p4 + ZZ_e.jets[0].p4 + ZZ_e.jets[1].p4).M();
			if (WR>=MW_Cut) {
            ZZ_hWR.Fill(WR, ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.);
	  
	        tWR = (K1_Z1*ZZ_e.muons[0].p4 + K1_Z2*ZZ_e.muons[1].p4 + ZZ_e.jets[0].p4 + ZZ_e.jets[1].p4).Pt();
            ZZ_PtWR.Fill(tWR, ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.);
	  
	        lWR = (K1_Z1*ZZ_e.muons[0].p4 + K1_Z2*ZZ_e.muons[1].p4 + ZZ_e.jets[0].p4 + ZZ_e.jets[1].p4).Pz();
            ZZ_PlWR.Fill(lWR, ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.);
	  
	        eWR = (K1_Z1*ZZ_e.muons[0].p4 + K1_Z2*ZZ_e.muons[1].p4 + ZZ_e.jets[0].p4 + ZZ_e.jets[1].p4).Eta();
            ZZ_EtaWR.Fill(eWR, ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.);
	  
	        pWR = (K1_Z1*ZZ_e.muons[0].p4 + K1_Z2*ZZ_e.muons[1].p4 + ZZ_e.jets[0].p4 + ZZ_e.jets[1].p4).Phi();
            ZZ_PhiWR.Fill(pWR, ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.);
	  
            NuR_l0 = (K1_Z1*ZZ_e.muons[0].p4 + ZZ_e.jets[0].p4 + ZZ_e.jets[1].p4).M();
            NuR_l1 = (K1_Z2*ZZ_e.muons[1].p4 + ZZ_e.jets[0].p4 + ZZ_e.jets[1].p4).M();
      
	        ZZ_hNuR0.Fill(NuR_l0, ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.);
	        ZZ_hNuR1.Fill(NuR_l1, ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.);
	        
            Mll = (K1_Z1*ZZ_e.muons[0].p4 + K1_Z2*ZZ_e.muons[1].p4).M();
            ZZ_hMll.Fill(Mll, ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.);
			ZZ_hMll1.Fill(Mll, ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.);
      
            Mjj = (ZZ_e.jets[0].p4 + ZZ_e.jets[1].p4).M();
            ZZ_hMjj.Fill(Mjj, ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.);
			
			Mmet = (ZZ_e.mets[0].p4).Pt();
            ZZ_hMmets.Fill(Mmet, ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.);
		    ZZ_hMmets1.Fill(Mmet, ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.);
			
			ZZ_hbDiscr_j1.Fill(ZZ_e.jets[0].bDiscr, ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.);
			ZZ_hbDiscr_j2.Fill(ZZ_e.jets[1].bDiscr, ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.);
							
			ZZ_n = ZZ_n + 1;
			ZZ_nw = ZZ_nw + ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.; 
						
			if ((ZZ_e.jets[0].bDiscr >= 0.669) && (ZZ_e.jets[1].bDiscr >= 0.669)) { //  10% = 0.215      1% = 0.459    0.1% = 0.669
			ZZ_nwB2 = ZZ_nwB2 + ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.; 
			ZZ_nB2 = ZZ_nB2 + 1;
			ZZ_hbNum.Fill(2., ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.);
			}
			if (((ZZ_e.jets[0].bDiscr >= 0.669) && (ZZ_e.jets[1].bDiscr <= 0.669)) || ((ZZ_e.jets[0].bDiscr <= 0.669) && (ZZ_e.jets[1].bDiscr >= 0.669)) ) { //  10% = 0.215      1% = 0.459    0.1% = 0.669
			ZZ_nwB1 = ZZ_nwB1 + ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.; 
			ZZ_nB1 = ZZ_nB1 + 1;
			ZZ_hbNum.Fill(1., ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.);
			}
			if ((ZZ_e.jets[0].bDiscr <= 0.669) && (ZZ_e.jets[1].bDiscr <= 0.669)) {
            ZZ_nwB3 = ZZ_nwB3 + ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.; 			
			ZZ_nB3 = ZZ_nB3 + 1;
			ZZ_hbNum.Fill(0., ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.);
			}
			
			if (ZZ_e.muons[0].charge*ZZ_e.muons[1].charge == -1) {
			ZZ_nwQ = ZZ_nwQ + ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.; 
			ZZ_nQ = ZZ_nQ +1; 
			ZZ_hQNum.Fill(-0.5, ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.);}
			else {
			ZZ_nwQ1 = ZZ_nwQ1 + ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.;
			ZZ_nQ1 = ZZ_nQ1 +1;
			ZZ_hQNum.Fill(0.5, ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.);}
			
			
			if ((ZZ_e.muons[0].charge==-1)&&(ZZ_e.muons[1].charge == -1)) {
			ZZ_nwQ3 = ZZ_nwQ3 + ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.;
			ZZ_nQ3 = ZZ_nQ3 +1;}
			
			if ((ZZ_e.muons[0].charge==1)&&(ZZ_e.muons[1].charge == 1)) {
            ZZ_nwQ4 = ZZ_nwQ4 + ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.;			
			ZZ_nQ4 = ZZ_nQ4 +1;}
			
			
			ZZ_dVz.Fill((abs(ZZ_e.muons[0].vertex.z()-ZZ_e.muons[1].vertex.z())), ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.);
			ZZ_dVz1.Fill(abs(ZZ_e.jets[0].vertex.z()-ZZ_e.jets[1].vertex.z()), ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.);
			ZZ_dVz2.Fill(abs(ZZ_e.jets[0].vertex.z()-ZZ_e.muons[0].vertex.z()), ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.);
			ZZ_dVz3.Fill(abs(ZZ_e.jets[1].vertex.z()-ZZ_e.muons[1].vertex.z()), ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.);
			ZZ_dVz4.Fill(abs(ZZ_e.jets[1].vertex.z()-ZZ_e.muons[0].vertex.z()), ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.);
			ZZ_dVz5.Fill(abs(ZZ_e.jets[0].vertex.z()-ZZ_e.muons[1].vertex.z()), ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.);
	
			ZZ_PU.Fill(ZZ_e.vertices.size(), ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.);
			ZZ_PU1.Fill(ZZ_e.pileup.size(), ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.);
			ZZ_hZPt.Fill((K1_Z1*ZZ_e.muons[0].p4 + K1_Z2*ZZ_e.muons[1].p4).Pt(), ZZ_tune*ZZ_weight*A_Wei*RD_Lum/100.);
		
		
		/*	
			cout<<"New event!" << n <<" PtL1 = " <<(K1_Z1*ZZ_e.muons[0].p4).Pt()<<" PtL2 = "<<(K1_Z2*ZZ_e.muons[1].p4).Pt() << " Mll = "<<(K1_Z1*ZZ_e.muons[0].p4 + K1_Z2*ZZ_e.muons[1].p4).M() << endl;
			cout<<" PtJ1 = "<<(ZZ_e.jets[0].p4).Pt()<<" bDiscr J1 = "<< ZZ_e.jets[0].bDiscr <<" PtJ2 = "<<(ZZ_e.jets[1].p4).Pt()<<" bDiscr J2 = "<< ZZ_e.jets[1].bDiscr <<endl;
			cout<<"EtaL1 = "<<(K1_Z1*ZZ_e.muons[0].p4).Eta()<<" PhiL1 = "<<(K1_Z1*ZZ_e.muons[0].p4).Phi()<<" EtaL2 = "<<(K1_Z2*ZZ_e.muons[1].p4).Eta()<<" PhiL2 = "<<(K1_Z2*ZZ_e.muons[1].p4).Phi() << endl;
			cout<<"EtaJ1 = "<<(ZZ_e.jets[0].p4).Eta()<<" PhiJ1 = "<<(ZZ_e.jets[0].p4).Phi()<<" EtaJ2 = "<<(ZZ_e.jets[1].p4).Eta()<<" PhiJ2 = "<<(ZZ_e.jets[1].p4).Phi() << endl;			
			
	        cout<<"vzL1 = "<<(ZZ_e.muons[0].vertex).z()<<" VzL2 = "<<(ZZ_e.muons[1].vertex).z()<<" VzJ1 = "<<(ZZ_e.jets[0].vertex).z()<<" VzJ2 = "<<(ZZ_e.jets[1].vertex).z() << endl;
		
	    	cout<<" L1: ecalIso = " <<ZZ_e.muons[0].ecalIso <<" hcalIso = "<<ZZ_e.muons[0].hcalIso <<" hcal1Iso = "<<ZZ_e.muons[0].hcal1Iso << " trackIso = "<< ZZ_e.muons[0].trackIso << endl; 
					
			cout<<" L1: charge = " <<ZZ_e.muons[0].charge << endl; 
	
	       cout<<" L1: ecalIso = " <<ZZ_e.muons[1].ecalIso <<" hcalIso = "<<ZZ_e.muons[1].hcalIso <<" hcal1Iso = "<<ZZ_e.muons[1].hcal1Iso << " trackIso = "<< ZZ_e.muons[1].trackIso << endl; 
			
			cout<<" L2: charge = " <<ZZ_e.muons[1].charge << endl;
							
			cout<<"MWR = "<< WR <<" MNuR = "<< NuR_l1 <<" Mjj = "<< Mjj << endl;
			cout <<"RUN = "<< ZZ_e.irun << " LUMI = " << ZZ_e.ilumi << " EVENT = "<< ZZ_e.ievent<< endl;
		    
			
        */  
		}
		}	
		}	
       } 	
        }
	   }
	  }
	 }  
	// Multiplicity
    ZZ_hElMult.Fill(ZZ_e.muons.size(),ZZ_tune*ZZ_weight*RD_Lum/100.);
    ZZ_hJetMult.Fill(ZZ_e.jets.size(),ZZ_tune*ZZ_weight*RD_Lum/100.);	
	    

  } 
 BG_nw = BG_nw + ZZ_nw; // number of total BG events
 delta_BG_nw_sq = delta_BG_nw_sq + (0.20*0.20 + 1/(ZZ_n+1))*ZZ_nw*ZZ_nw ; // 20% TTb systematic BG error 

 cout << "MC BG ZZ" << endl;
 cout << endl;
 cout << "Total events = " << ZZ_nEvents << endl;
 cout << "Total weight events = " << ZZ_nwEvents << endl;
 cout << endl; 
 cout << "Number of passed events = "<<ZZ_n<<"   "<<ZZ_nw<< endl;
 cout << endl;
 cout << "Number of events with 0 b-jet = " << ZZ_nB3<<"   " <<ZZ_nwB3 << endl; 
 cout << "Number of events with 1 b-jet = " << ZZ_nB1<<"   " <<ZZ_nwB1 << endl;
 cout << "Number of events with 2 b-jet = " << ZZ_nB2<<"   " <<ZZ_nwB2 << endl;	
 cout << endl;	
 cout << "Number of events with same charges of lepton = " << ZZ_nQ1<<"   " <<ZZ_nwQ1 << endl;
 cout << "Number of events with opposite charges of lepton = " << ZZ_nQ <<"   " <<ZZ_nwQ << endl; 
 cout << endl;
 cout << "Number of events with e-e- = " << ZZ_nQ3<<"   " <<ZZ_nwQ3 << endl;
 cout << "Number of events with e+e+ = " << ZZ_nQ4 <<"   " <<ZZ_nwQ4 << endl; 
 cout << endl;
 
}
 
 
//============================================================================================================================
 


  
// MC BG ZW

   TH1F ZW_hWR(  "ZW_WR", "W_{R} reconstructed mass;mass, GeV/c^{2};events/50 GeV", M_Delta_M_W, M_min_M_W, M_max_M_W);
  TH1F ZW_hNuR0("ZW_NuR0", "nu_{R} reconstructed mass using first E electron;mass, GeV/c^{2};events/50 GeV", M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu);
  TH1F ZW_hNuR1("ZW_NuR1", "nu_{R} reconstructed mass using second E electron;mass, GeV/c^{2};events/50 GeV", M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu); 
  TH1F ZW_hMll("ZW_Mll", "lepton-lepton pair invariant mass;mass, GeV/c^{2};events/2 GeV", M_Delta_Mll, M_min_Mll, M_max_Mll);
  TH1F ZW_hMll1("ZW_Mll1", "lepton-lepton pair invariant mass;mass, GeV/c^{2};events/40 GeV", 20, 100, 1000);
  TH1F ZW_hMjj("ZW_Mjj", "jet-jet pair invariant mass;mass, GeV/c^{2};events/50 GeV", M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu);
  TH1F ZW_hMmets("ZW_Mmets", "Miss Et; Et, GeV/c^{2};events/10 GeV", M_Delta_MET, M_min_MET, M_max_MET);
  TH1F ZW_hMmets1("ZW_Mmets1", "Miss Et; Et, GeV/c^{2};events/30 GeV", 30, 100, 1000); 
  TH1F ZW_PtWR(  "ZW_PtWR", "W_{R} Pt; Pt, GeV/c^{2}; events/6GeV", 30, 0, 180);
  TH1F ZW_PlWR(  "ZW_PlWR", "W_{R} Pl;Pl, GeV/c^{2};events/50 GeV", 60, -1500, 1500);
  TH1F ZW_EtaWR( "ZW_EtaWR", "W_{R} Eta; Eta; events", 10, -6, 6);
  TH1F ZW_PhiWR( "ZW_PhiWR", "W_{R} Phi; Phi; events", 10, -3.1415, 3.1415); 
  TH1F ZW_hbDiscr_j1( "ZW_bDiscrJ1", "first jet b Discriminant; bDiscr; events", 15, 0, 1.5); 
  TH1F ZW_hbDiscr_j2( "ZW_bDiscrJ2", "second jet b Discriminant; bDiscr; events", 15, 0, 1.5); 
  TH1F ZW_hbNum( "ZW_hbNum", "Number of b jets ; bNum; events", 3, 0, 3); 
  TH1F ZW_hQNum( "ZW_hQNum", "Mult of leptons charges ; e_1*e_2; events", 2, -1, 1); 
  TH1F ZW_hElMult("ZW_ElMult", "electrons multiplicity", 10, 0, 10);
       ZW_hElMult.GetXaxis()->SetNdivisions(110);
  TH1F ZW_hJetMult("ZW_JetMult", "jets multiplicity", 20, 0, 20);
       ZW_hJetMult.GetXaxis()->SetNdivisions(210);   
  TH1F ZW_hElRes("ZW_ElRes", "electron energy reconstruction resolution", 100, -0.3, 0.3);
  
  TH1F ZW_dVz( "ZW_dVz", "delta Vz; #delta V_{z} between ee, cm; Events", 20, 0, 0.1); 
  TH1F ZW_dVz1( "ZW_dVz1", "delta Vz;#delta V_{z} between jj, cm; Events", 20, 0, 0.1);
  TH1F ZW_dVz2( "ZW_dVz2", "delta Vz;#delta V_{z} between e_{1}j_{1}, cm; Events", 20, 0, 0.1);  
  TH1F ZW_dVz3( "ZW_dVz3", "delta Vz;#delta V_{z} between e_{2}j_{2}, cm; Events", 20, 0, 0.1); 
  TH1F ZW_dVz4( "ZW_dVz4", "delta Vz;#delta V_{z} between e_{2}j_{1}, cm; Events", 20, 0, 0.1);  
  TH1F ZW_dVz5( "ZW_dVz5", "delta Vz;#delta V_{z} between e_{1}j_{2}, cm; Events", 20, 0, 0.1); 
  
  TH1F ZW_PU( "ZW_PU", "PU; Primary vertex number ; Events", 25, 0, 25); 
  TH1F ZW_PU1( "ZW_PU1", "PU1; Primary vertex number ; Events", 25, 0, 25); 
  TH1F ZW_hZPt("ZW_ZPt", "Z+jets; Z Pt, GeV/c;Events/20 GeV/c", 10, 0, 200);
    	
  fname = "/moscow41/heavynu/41x-background/WZtoAnything_TuneZ2_7TeV-pythia6-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root";


 
  TheEvent ZW_e(fname);
  if (!ZW_e) return;
  int ZW_n = 0; 
  float ZW_nw = 0.;  
  
if (ZW_incl == true) {

  int ZW_nB1 = 0; 
  float ZW_nwB1 = 0;
  int ZW_nB2 = 0;
  float ZW_nwB2 = 0;  
  int ZW_nB3 = 0;
  float ZW_nwB3 = 0;
  int ZW_nEvents = 0; 
  float ZW_nwEvents = 0;  
  int ZW_nQ = 0;
  float ZW_nwQ = 0;
  int ZW_nQ1 = 0;
  float ZW_nwQ1 = 0;
  int ZW_nQ3 = 0;
  float ZW_nwQ3 = 0;
  int ZW_nQ4 = 0;
  float ZW_nwQ4 = 0;
  float ZW_weight =0;
  

  ZW_nEvents = ZW_e.totalEvents();
 
  for (int i = 0; i < ZW_nEvents; ++i) {
    ZW_e.readEvent(i);  

      if(ZW_e.pileup.size()<25){
        ZW_weight = ZW_e.weight *  generate_flat10_weights(histo)[ZW_e.pileup.size()];
        }
        else{ //should not happen as we have a weight for all simulated ZW_e.pileup.size() multiplicities!
        ZW_weight = 0.;
        }

		ZW_nwEvents = ZW_nwEvents + ZW_tune*ZW_weight*RD_Lum/100.;  
   
    // electron energy resolution
    for (unsigned int j = 0; j < ZW_e.muons.size(); ++j) {
      const Particle& reco = ZW_e.muons[j];
      if (reco.mcId >= 0) {
        const Particle& mc = ZW_e.gens[reco.mcId];
        const float r = (reco.p4.E() - mc.p4.E())/mc.p4.E();
        ZW_hElRes.Fill(r, ZW_tune*ZW_weight*A_Wei*RD_Lum/100.);
      }
    }
    
    // mass distributions
      if ((ZW_e.muons.size() >= NofLept) && (ZW_e.jets.size() >= NofJets)) {
	  
    //  correction of Z mass and ID efficiency
	   if ((abs(ZW_e.muons[0].p4.Eta())<=1.44)) {K1_Z1 = 1.;} else {K1_Z1=1.;}
	   if ((abs(K1_Z2*ZW_e.muons[1].p4.Eta())<=1.44)) {K1_Z2 = 1.;} else {K1_Z2=1.;}
	  
	   if (abs((K1_Z1*ZW_e.muons[0].p4).Eta())<=1.44){A_Wei=HEEP_B;} else {A_Wei=HEEP_E;}
	   if (abs((K1_Z2*ZW_e.muons[1].p4).Eta())<=1.44){A_Wei=A_Wei*HEEP_B;} else {A_Wei=A_Wei*HEEP_E;}
	
	//cuts on electrons Pt>=40
	    if (((K1_Z1*ZW_e.muons[0].p4).Pt()>=Lep1Pt) && ((K1_Z2*ZW_e.muons[1].p4).Pt()>=Lep2Pt))  { 
    //cuts on jetss Pt>=40      
	      if (((ZW_e.jets[0].p4).Pt()>=Jet1Pt) && ((ZW_e.jets[1].p4).Pt()>=Jet2Pt))  { 
		  
	//cuts on electrons invariant mass Mll>=400.0	
	  if (((K1_Z1*ZW_e.muons[0].p4 + K1_Z2*ZW_e.muons[1].p4).M()>=MllCut)&& ((K1_Z1*ZW_e.muons[0].p4 + K1_Z2*ZW_e.muons[1].p4).M()<=MllCut1)) {
//	  if (!(((K1_Z1*ZW_e.muons[0].p4 + K1_Z2*ZW_e.muons[1].p4).M()>=MllCut)&& ((K1_Z1*ZW_e.muons[0].p4 + K1_Z2*ZW_e.muons[1].p4).M()<=MllCut1))) {
			  
	//cuts on Eta  
         if (((abs(K1_Z1*ZW_e.muons[0].p4.Eta())<=EtaCut1L) || (abs(K1_Z2*ZW_e.muons[1].p4.Eta())<=EtaCut2L)) 
	     && (abs(ZW_e.jets[0].p4.Eta())<=EtaCut1J) && (abs(ZW_e.jets[1].p4.Eta())<=EtaCut2J) )        {
			
	// Isolation
	       if ((ZW_e.muons[0].isoHEEP() <= ISO)&&(ZW_e.muons[1].isoHEEP() <= ISO)) {
		   
	 //Cut on vertexes
             if ((abs(ZW_e.muons[0].vertex.z()-ZW_e.muons[1].vertex.z())<=Vertex) && (abs(ZW_e.jets[0].vertex.z()-ZW_e.jets[1].vertex.z())<=Vertex) 
             && ((abs(ZW_e.muons[0].vertex.z()-ZW_e.jets[1].vertex.z())<=Vertex) && (abs(ZW_e.jets[0].vertex.z()-ZW_e.muons[1].vertex.z())<=Vertex))) {
			 
	        WR = (K1_Z1*ZW_e.muons[0].p4 + K1_Z2*ZW_e.muons[1].p4 + ZW_e.jets[0].p4 + ZW_e.jets[1].p4).M();
			if (WR>=MW_Cut) {
            ZW_hWR.Fill(WR, ZW_tune*ZW_weight*A_Wei*RD_Lum/100.);
	  
	        tWR = (K1_Z1*ZW_e.muons[0].p4 + K1_Z2*ZW_e.muons[1].p4 + ZW_e.jets[0].p4 + ZW_e.jets[1].p4).Pt();
            ZW_PtWR.Fill(tWR, ZW_tune*ZW_weight*A_Wei*RD_Lum/100.);
	  
	        lWR = (K1_Z1*ZW_e.muons[0].p4 + K1_Z2*ZW_e.muons[1].p4 + ZW_e.jets[0].p4 + ZW_e.jets[1].p4).Pz();
            ZW_PlWR.Fill(lWR, ZW_tune*ZW_weight*A_Wei*RD_Lum/100.);
	  
	        eWR = (K1_Z1*ZW_e.muons[0].p4 + K1_Z2*ZW_e.muons[1].p4 + ZW_e.jets[0].p4 + ZW_e.jets[1].p4).Eta();
            ZW_EtaWR.Fill(eWR, ZW_tune*ZW_weight*A_Wei*RD_Lum/100.);
	  
	        pWR = (K1_Z1*ZW_e.muons[0].p4 + K1_Z2*ZW_e.muons[1].p4 + ZW_e.jets[0].p4 + ZW_e.jets[1].p4).Phi();
            ZW_PhiWR.Fill(pWR, ZW_tune*ZW_weight*A_Wei*RD_Lum/100.);
	  
            NuR_l0 = (K1_Z1*ZW_e.muons[0].p4 + ZW_e.jets[0].p4 + ZW_e.jets[1].p4).M();
            NuR_l1 = (K1_Z2*ZW_e.muons[1].p4 + ZW_e.jets[0].p4 + ZW_e.jets[1].p4).M();
      
	        ZW_hNuR0.Fill(NuR_l0, ZW_tune*ZW_weight*A_Wei*RD_Lum/100.);
	        ZW_hNuR1.Fill(NuR_l1, ZW_tune*ZW_weight*A_Wei*RD_Lum/100.);
	        
            Mll = (K1_Z1*ZW_e.muons[0].p4 + K1_Z2*ZW_e.muons[1].p4).M();
            ZW_hMll.Fill(Mll, ZW_tune*ZW_weight*A_Wei*RD_Lum/100.);
			ZW_hMll1.Fill(Mll, ZW_tune*ZW_weight*A_Wei*RD_Lum/100.);
      
            Mjj = (ZW_e.jets[0].p4 + ZW_e.jets[1].p4).M();
            ZW_hMjj.Fill(Mjj, ZW_tune*ZW_weight*A_Wei*RD_Lum/100.);
			
			Mmet = (ZW_e.mets[0].p4).Pt();
            ZW_hMmets.Fill(Mmet, ZW_tune*ZW_weight*A_Wei*RD_Lum/100.);
		    ZW_hMmets1.Fill(Mmet, ZW_tune*ZW_weight*A_Wei*RD_Lum/100.);
			
			ZW_hbDiscr_j1.Fill(ZW_e.jets[0].bDiscr, ZW_tune*ZW_weight*A_Wei*RD_Lum/100.);
			ZW_hbDiscr_j2.Fill(ZW_e.jets[1].bDiscr, ZW_tune*ZW_weight*A_Wei*RD_Lum/100.);
							
			ZW_n = ZW_n + 1;
			ZW_nw = ZW_nw + ZW_tune*ZW_weight*A_Wei*RD_Lum/100.; 
						
			if ((ZW_e.jets[0].bDiscr >= 0.669) && (ZW_e.jets[1].bDiscr >= 0.669)) { //  10% = 0.215      1% = 0.459    0.1% = 0.669
			ZW_nwB2 = ZW_nwB2 + ZW_tune*ZW_weight*A_Wei*RD_Lum/100.; 
			ZW_nB2 = ZW_nB2 + 1;
			ZW_hbNum.Fill(2., ZW_tune*ZW_weight*A_Wei*RD_Lum/100.);
			}
			if (((ZW_e.jets[0].bDiscr >= 0.669) && (ZW_e.jets[1].bDiscr <= 0.669)) || ((ZW_e.jets[0].bDiscr <= 0.669) && (ZW_e.jets[1].bDiscr >= 0.669)) ) { //  10% = 0.215      1% = 0.459    0.1% = 0.669
			ZW_nwB1 = ZW_nwB1 + ZW_tune*ZW_weight*A_Wei*RD_Lum/100.; 
			ZW_nB1 = ZW_nB1 + 1;
			ZW_hbNum.Fill(1., ZW_tune*ZW_weight*A_Wei*RD_Lum/100.);
			}
			if ((ZW_e.jets[0].bDiscr <= 0.669) && (ZW_e.jets[1].bDiscr <= 0.669)) {
            ZW_nwB3 = ZW_nwB3 + ZW_tune*ZW_weight*A_Wei*RD_Lum/100.; 			
			ZW_nB3 = ZW_nB3 + 1;
			ZW_hbNum.Fill(0., ZW_tune*ZW_weight*A_Wei*RD_Lum/100.);
			}
			
			if (ZW_e.muons[0].charge*ZW_e.muons[1].charge == -1) {
			ZW_nwQ = ZW_nwQ + ZW_tune*ZW_weight*A_Wei*RD_Lum/100.; 
			ZW_nQ = ZW_nQ +1; 
			ZW_hQNum.Fill(-0.5, ZW_tune*ZW_weight*A_Wei*RD_Lum/100.);}
			else {
			ZW_nwQ1 = ZW_nwQ1 + ZW_tune*ZW_weight*A_Wei*RD_Lum/100.;
			ZW_nQ1 = ZW_nQ1 +1;
			ZW_hQNum.Fill(0.5, ZW_tune*ZW_weight*A_Wei*RD_Lum/100.);}
			
			
			if ((ZW_e.muons[0].charge==-1)&&(ZW_e.muons[1].charge == -1)) {
			ZW_nwQ3 = ZW_nwQ3 + ZW_tune*ZW_weight*A_Wei*RD_Lum/100.;
			ZW_nQ3 = ZW_nQ3 +1;}
			
			if ((ZW_e.muons[0].charge==1)&&(ZW_e.muons[1].charge == 1)) {
            ZW_nwQ4 = ZW_nwQ4 + ZW_tune*ZW_weight*A_Wei*RD_Lum/100.;			
			ZW_nQ4 = ZW_nQ4 +1;}
			
			
			ZW_dVz.Fill((abs(ZW_e.muons[0].vertex.z()-ZW_e.muons[1].vertex.z())), ZW_tune*ZW_weight*A_Wei*RD_Lum/100.);
			ZW_dVz1.Fill(abs(ZW_e.jets[0].vertex.z()-ZW_e.jets[1].vertex.z()), ZW_tune*ZW_weight*A_Wei*RD_Lum/100.);
			ZW_dVz2.Fill(abs(ZW_e.jets[0].vertex.z()-ZW_e.muons[0].vertex.z()), ZW_tune*ZW_weight*A_Wei*RD_Lum/100.);
			ZW_dVz3.Fill(abs(ZW_e.jets[1].vertex.z()-ZW_e.muons[1].vertex.z()), ZW_tune*ZW_weight*A_Wei*RD_Lum/100.);
			ZW_dVz4.Fill(abs(ZW_e.jets[1].vertex.z()-ZW_e.muons[0].vertex.z()), ZW_tune*ZW_weight*A_Wei*RD_Lum/100.);
			ZW_dVz5.Fill(abs(ZW_e.jets[0].vertex.z()-ZW_e.muons[1].vertex.z()), ZW_tune*ZW_weight*A_Wei*RD_Lum/100.);
	
			ZW_PU.Fill(ZW_e.vertices.size(), ZW_tune*ZW_weight*A_Wei*RD_Lum/100.);
			ZW_PU1.Fill(ZW_e.pileup.size(), ZW_tune*ZW_weight*A_Wei*RD_Lum/100.);
			ZW_hZPt.Fill((K1_Z1*ZW_e.muons[0].p4 + K1_Z2*ZW_e.muons[1].p4).Pt(), ZW_tune*ZW_weight*A_Wei*RD_Lum/100.);
		
		
		/*	
			cout<<"New event!" << n <<" PtL1 = " <<(K1_Z1*ZW_e.muons[0].p4).Pt()<<" PtL2 = "<<(K1_Z2*ZW_e.muons[1].p4).Pt() << " Mll = "<<(K1_Z1*ZW_e.muons[0].p4 + K1_Z2*ZW_e.muons[1].p4).M() << endl;
			cout<<" PtJ1 = "<<(ZW_e.jets[0].p4).Pt()<<" bDiscr J1 = "<< ZW_e.jets[0].bDiscr <<" PtJ2 = "<<(ZW_e.jets[1].p4).Pt()<<" bDiscr J2 = "<< ZW_e.jets[1].bDiscr <<endl;
			cout<<"EtaL1 = "<<(K1_Z1*ZW_e.muons[0].p4).Eta()<<" PhiL1 = "<<(K1_Z1*ZW_e.muons[0].p4).Phi()<<" EtaL2 = "<<(K1_Z2*ZW_e.muons[1].p4).Eta()<<" PhiL2 = "<<(K1_Z2*ZW_e.muons[1].p4).Phi() << endl;
			cout<<"EtaJ1 = "<<(ZW_e.jets[0].p4).Eta()<<" PhiJ1 = "<<(ZW_e.jets[0].p4).Phi()<<" EtaJ2 = "<<(ZW_e.jets[1].p4).Eta()<<" PhiJ2 = "<<(ZW_e.jets[1].p4).Phi() << endl;			
			
	        cout<<"vzL1 = "<<(ZW_e.muons[0].vertex).z()<<" VzL2 = "<<(ZW_e.muons[1].vertex).z()<<" VzJ1 = "<<(ZW_e.jets[0].vertex).z()<<" VzJ2 = "<<(ZW_e.jets[1].vertex).z() << endl;
		
	    	cout<<" L1: ecalIso = " <<ZW_e.muons[0].ecalIso <<" hcalIso = "<<ZW_e.muons[0].hcalIso <<" hcal1Iso = "<<ZW_e.muons[0].hcal1Iso << " trackIso = "<< ZW_e.muons[0].trackIso << endl; 
					
			cout<<" L1: charge = " <<ZW_e.muons[0].charge << endl; 
	
	       cout<<" L1: ecalIso = " <<ZW_e.muons[1].ecalIso <<" hcalIso = "<<ZW_e.muons[1].hcalIso <<" hcal1Iso = "<<ZW_e.muons[1].hcal1Iso << " trackIso = "<< ZW_e.muons[1].trackIso << endl; 
			
			cout<<" L2: charge = " <<ZW_e.muons[1].charge << endl;
							
			cout<<"MWR = "<< WR <<" MNuR = "<< NuR_l1 <<" Mjj = "<< Mjj << endl;
			cout <<"RUN = "<< ZW_e.irun << " LUMI = " << ZW_e.ilumi << " EVENT = "<< ZW_e.ievent<< endl;
		    
			
        */  
		}
		}	
		}	
       } 	
        }
	   }
	  }
	 }  
	// Multiplicity
    ZW_hElMult.Fill(ZW_e.muons.size(),ZW_tune*ZW_weight*RD_Lum/100.);
    ZW_hJetMult.Fill(ZW_e.jets.size(),ZW_tune*ZW_weight*RD_Lum/100.);	
	    

  } 
 BG_nw = BG_nw + ZW_nw; // number of total BG events
 delta_BG_nw_sq = delta_BG_nw_sq + (0.20*0.20 + 1/(ZW_n+1))*ZW_nw*ZW_nw ; // 20% TTb systematic BG error 

 cout << "MC BG ZW" << endl;
 cout << endl;
 cout << "Total events = " << ZW_nEvents << endl;
 cout << "Total weight events = " << ZW_nwEvents << endl;
 cout << endl; 
 cout << "Number of passed events = "<<ZW_n<<"   "<<ZW_nw<< endl;
 cout << endl;
 cout << "Number of events with 0 b-jet = " << ZW_nB3<<"   " <<ZW_nwB3 << endl; 
 cout << "Number of events with 1 b-jet = " << ZW_nB1<<"   " <<ZW_nwB1 << endl;
 cout << "Number of events with 2 b-jet = " << ZW_nB2<<"   " <<ZW_nwB2 << endl;	
 cout << endl;	
 cout << "Number of events with same charges of lepton = " << ZW_nQ1<<"   " <<ZW_nwQ1 << endl;
 cout << "Number of events with opposite charges of lepton = " << ZW_nQ <<"   " <<ZW_nwQ << endl; 
 cout << endl;
 cout << "Number of events with e-e- = " << ZW_nQ3<<"   " <<ZW_nwQ3 << endl;
 cout << "Number of events with e+e+ = " << ZW_nQ4 <<"   " <<ZW_nwQ4 << endl; 
 cout << endl;
 
}
  
//============================================================================================================================
 
 
// MC BG WW
  
   TH1F WW_hWR(  "WW_WR", "W_{R} reconstructed mass;mass, GeV/c^{2};events/50 GeV", M_Delta_M_W, M_min_M_W, M_max_M_W);
  TH1F WW_hNuR0("WW_NuR0", "nu_{R} reconstructed mass using first E electron;mass, GeV/c^{2};events/50 GeV", M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu);
  TH1F WW_hNuR1("WW_NuR1", "nu_{R} reconstructed mass using second E electron;mass, GeV/c^{2};events/50 GeV", M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu); 
  TH1F WW_hMll("WW_Mll", "lepton-lepton pair invariant mass;mass, GeV/c^{2};events/2 GeV", M_Delta_Mll, M_min_Mll, M_max_Mll);
  TH1F WW_hMll1("WW_Mll1", "lepton-lepton pair invariant mass;mass, GeV/c^{2};events/40 GeV", 20, 100, 1000);
  TH1F WW_hMjj("WW_Mjj", "jet-jet pair invariant mass;mass, GeV/c^{2};events/50 GeV", M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu);
  TH1F WW_hMmets("WW_Mmets", "Miss Et; Et, GeV/c^{2};events/10 GeV", M_Delta_MET, M_min_MET, M_max_MET);
  TH1F WW_hMmets1("WW_Mmets1", "Miss Et; Et, GeV/c^{2};events/30 GeV", 30, 100, 1000); 
  TH1F WW_PtWR(  "WW_PtWR", "W_{R} Pt; Pt, GeV/c^{2}; events/6GeV", 30, 0, 180);
  TH1F WW_PlWR(  "WW_PlWR", "W_{R} Pl;Pl, GeV/c^{2};events/50 GeV", 60, -1500, 1500);
  TH1F WW_EtaWR( "WW_EtaWR", "W_{R} Eta; Eta; events", 10, -6, 6);
  TH1F WW_PhiWR( "WW_PhiWR", "W_{R} Phi; Phi; events", 10, -3.1415, 3.1415); 
  TH1F WW_hbDiscr_j1( "WW_bDiscrJ1", "first jet b Discriminant; bDiscr; events", 15, 0, 1.5); 
  TH1F WW_hbDiscr_j2( "WW_bDiscrJ2", "second jet b Discriminant; bDiscr; events", 15, 0, 1.5); 
  TH1F WW_hbNum( "WW_hbNum", "Number of b jets ; bNum; events", 3, 0, 3); 
  TH1F WW_hQNum( "WW_hQNum", "Mult of leptons charges ; e_1*e_2; events", 2, -1, 1); 
  TH1F WW_hElMult("WW_ElMult", "electrons multiplicity", 10, 0, 10);
       WW_hElMult.GetXaxis()->SetNdivisions(110);
  TH1F WW_hJetMult("WW_JetMult", "jets multiplicity", 20, 0, 20);
       WW_hJetMult.GetXaxis()->SetNdivisions(210);   
  TH1F WW_hElRes("WW_ElRes", "electron energy reconstruction resolution", 100, -0.3, 0.3);
  
  TH1F WW_dVz( "WW_dVz", "delta Vz; #delta V_{z} between ee, cm; Events", 20, 0, 0.1); 
  TH1F WW_dVz1( "WW_dVz1", "delta Vz;#delta V_{z} between jj, cm; Events", 20, 0, 0.1);
  TH1F WW_dVz2( "WW_dVz2", "delta Vz;#delta V_{z} between e_{1}j_{1}, cm; Events", 20, 0, 0.1);  
  TH1F WW_dVz3( "WW_dVz3", "delta Vz;#delta V_{z} between e_{2}j_{2}, cm; Events", 20, 0, 0.1); 
  TH1F WW_dVz4( "WW_dVz4", "delta Vz;#delta V_{z} between e_{2}j_{1}, cm; Events", 20, 0, 0.1);  
  TH1F WW_dVz5( "WW_dVz5", "delta Vz;#delta V_{z} between e_{1}j_{2}, cm; Events", 20, 0, 0.1); 
  
  TH1F WW_PU( "WW_PU", "PU; Primary vertex number ; Events", 25, 0, 25); 
  TH1F WW_PU1( "WW_PU1", "PU1; Primary vertex number ; Events", 25, 0, 25); 
  TH1F WW_hZPt("WW_ZPt", "Z+jets; Z Pt, GeV/c;Events/20 GeV/c", 10, 0, 200);
    	
  fname = "/moscow41/heavynu/41x-background/WWtoAnything_TuneZ2_7TeV-pythia6-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root";


 
  TheEvent WW_e(fname);
  if (!WW_e) return;
  int WW_n = 0; 
  float WW_nw = 0.;  
  
if (WW_incl == true) {

  int WW_nB1 = 0; 
  float WW_nwB1 = 0;
  int WW_nB2 = 0;
  float WW_nwB2 = 0;  
  int WW_nB3 = 0;
  float WW_nwB3 = 0;
  int WW_nEvents = 0; 
  float WW_nwEvents = 0;  
  int WW_nQ = 0;
  float WW_nwQ = 0;
  int WW_nQ1 = 0;
  float WW_nwQ1 = 0;
  int WW_nQ3 = 0;
  float WW_nwQ3 = 0;
  int WW_nQ4 = 0;
  float WW_nwQ4 = 0;
  float WW_weight =0;
  

  WW_nEvents = WW_e.totalEvents();
 
  for (int i = 0; i < WW_nEvents; ++i) {
    WW_e.readEvent(i);  

      if(WW_e.pileup.size()<25){
        WW_weight = WW_e.weight *  generate_flat10_weights(histo)[WW_e.pileup.size()];
        }
        else{ //should not happen as we have a weight for all simulated WW_e.pileup.size() multiplicities!
        WW_weight = 0.;
        }

		WW_nwEvents = WW_nwEvents + WW_tune*WW_weight*RD_Lum/100.;  
   
    // electron energy resolution
    for (unsigned int j = 0; j < WW_e.muons.size(); ++j) {
      const Particle& reco = WW_e.muons[j];
      if (reco.mcId >= 0) {
        const Particle& mc = WW_e.gens[reco.mcId];
        const float r = (reco.p4.E() - mc.p4.E())/mc.p4.E();
        WW_hElRes.Fill(r, WW_tune*WW_weight*A_Wei*RD_Lum/100.);
      }
    }
    
    // mass distributions
      if ((WW_e.muons.size() >= NofLept) && (WW_e.jets.size() >= NofJets)) {
	  
    //  correction of Z mass and ID efficiency
	   if ((abs(WW_e.muons[0].p4.Eta())<=1.44)) {K1_Z1 = 1.;} else {K1_Z1=1.;}
	   if ((abs(K1_Z2*WW_e.muons[1].p4.Eta())<=1.44)) {K1_Z2 = 1.;} else {K1_Z2=1.;}
	  
	   if (abs((K1_Z1*WW_e.muons[0].p4).Eta())<=1.44){A_Wei=HEEP_B;} else {A_Wei=HEEP_E;}
	   if (abs((K1_Z2*WW_e.muons[1].p4).Eta())<=1.44){A_Wei=A_Wei*HEEP_B;} else {A_Wei=A_Wei*HEEP_E;}
	
	//cuts on electrons Pt>=40
	    if (((K1_Z1*WW_e.muons[0].p4).Pt()>=Lep1Pt) && ((K1_Z2*WW_e.muons[1].p4).Pt()>=Lep2Pt))  { 
    //cuts on jetss Pt>=40      
	      if (((WW_e.jets[0].p4).Pt()>=Jet1Pt) && ((WW_e.jets[1].p4).Pt()>=Jet2Pt))  { 
		  
	//cuts on electrons invariant mass Mll>=400.0	
	  if (((K1_Z1*WW_e.muons[0].p4 + K1_Z2*WW_e.muons[1].p4).M()>=MllCut)&& ((K1_Z1*WW_e.muons[0].p4 + K1_Z2*WW_e.muons[1].p4).M()<=MllCut1)) {
//	  if (!(((K1_Z1*WW_e.muons[0].p4 + K1_Z2*WW_e.muons[1].p4).M()>=MllCut)&& ((K1_Z1*WW_e.muons[0].p4 + K1_Z2*WW_e.muons[1].p4).M()<=MllCut1))) {
			  
	//cuts on Eta  
         if (((abs(K1_Z1*WW_e.muons[0].p4.Eta())<=EtaCut1L) || (abs(K1_Z2*WW_e.muons[1].p4.Eta())<=EtaCut2L)) 
	     && (abs(WW_e.jets[0].p4.Eta())<=EtaCut1J) && (abs(WW_e.jets[1].p4.Eta())<=EtaCut2J) )        {
			
	// Isolation
	       if ((WW_e.muons[0].isoHEEP() <= ISO)&&(WW_e.muons[1].isoHEEP() <= ISO)) {
		   
	 //Cut on vertexes
             if ((abs(WW_e.muons[0].vertex.z()-WW_e.muons[1].vertex.z())<=Vertex) && (abs(WW_e.jets[0].vertex.z()-WW_e.jets[1].vertex.z())<=Vertex) 
             && ((abs(WW_e.muons[0].vertex.z()-WW_e.jets[1].vertex.z())<=Vertex) && (abs(WW_e.jets[0].vertex.z()-WW_e.muons[1].vertex.z())<=Vertex))) {
			 
	        WR = (K1_Z1*WW_e.muons[0].p4 + K1_Z2*WW_e.muons[1].p4 + WW_e.jets[0].p4 + WW_e.jets[1].p4).M();
			if (WR>=MW_Cut) {
            WW_hWR.Fill(WR, WW_tune*WW_weight*A_Wei*RD_Lum/100.);
	  
	        tWR = (K1_Z1*WW_e.muons[0].p4 + K1_Z2*WW_e.muons[1].p4 + WW_e.jets[0].p4 + WW_e.jets[1].p4).Pt();
            WW_PtWR.Fill(tWR, WW_tune*WW_weight*A_Wei*RD_Lum/100.);
	  
	        lWR = (K1_Z1*WW_e.muons[0].p4 + K1_Z2*WW_e.muons[1].p4 + WW_e.jets[0].p4 + WW_e.jets[1].p4).Pz();
            WW_PlWR.Fill(lWR, WW_tune*WW_weight*A_Wei*RD_Lum/100.);
	  
	        eWR = (K1_Z1*WW_e.muons[0].p4 + K1_Z2*WW_e.muons[1].p4 + WW_e.jets[0].p4 + WW_e.jets[1].p4).Eta();
            WW_EtaWR.Fill(eWR, WW_tune*WW_weight*A_Wei*RD_Lum/100.);
	  
	        pWR = (K1_Z1*WW_e.muons[0].p4 + K1_Z2*WW_e.muons[1].p4 + WW_e.jets[0].p4 + WW_e.jets[1].p4).Phi();
            WW_PhiWR.Fill(pWR, WW_tune*WW_weight*A_Wei*RD_Lum/100.);
	  
            NuR_l0 = (K1_Z1*WW_e.muons[0].p4 + WW_e.jets[0].p4 + WW_e.jets[1].p4).M();
            NuR_l1 = (K1_Z2*WW_e.muons[1].p4 + WW_e.jets[0].p4 + WW_e.jets[1].p4).M();
      
	        WW_hNuR0.Fill(NuR_l0, WW_tune*WW_weight*A_Wei*RD_Lum/100.);
	        WW_hNuR1.Fill(NuR_l1, WW_tune*WW_weight*A_Wei*RD_Lum/100.);
	        
            Mll = (K1_Z1*WW_e.muons[0].p4 + K1_Z2*WW_e.muons[1].p4).M();
            WW_hMll.Fill(Mll, WW_tune*WW_weight*A_Wei*RD_Lum/100.);
			WW_hMll1.Fill(Mll, WW_tune*WW_weight*A_Wei*RD_Lum/100.);
      
            Mjj = (WW_e.jets[0].p4 + WW_e.jets[1].p4).M();
            WW_hMjj.Fill(Mjj, WW_tune*WW_weight*A_Wei*RD_Lum/100.);
			
			Mmet = (WW_e.mets[0].p4).Pt();
            WW_hMmets.Fill(Mmet, WW_tune*WW_weight*A_Wei*RD_Lum/100.);
		    WW_hMmets1.Fill(Mmet, WW_tune*WW_weight*A_Wei*RD_Lum/100.);
			
			WW_hbDiscr_j1.Fill(WW_e.jets[0].bDiscr, WW_tune*WW_weight*A_Wei*RD_Lum/100.);
			WW_hbDiscr_j2.Fill(WW_e.jets[1].bDiscr, WW_tune*WW_weight*A_Wei*RD_Lum/100.);
							
			WW_n = WW_n + 1;
			WW_nw = WW_nw + WW_tune*WW_weight*A_Wei*RD_Lum/100.; 
						
			if ((WW_e.jets[0].bDiscr >= 0.669) && (WW_e.jets[1].bDiscr >= 0.669)) { //  10% = 0.215      1% = 0.459    0.1% = 0.669
			WW_nwB2 = WW_nwB2 + WW_tune*WW_weight*A_Wei*RD_Lum/100.; 
			WW_nB2 = WW_nB2 + 1;
			WW_hbNum.Fill(2., WW_tune*WW_weight*A_Wei*RD_Lum/100.);
			}
			if (((WW_e.jets[0].bDiscr >= 0.669) && (WW_e.jets[1].bDiscr <= 0.669)) || ((WW_e.jets[0].bDiscr <= 0.669) && (WW_e.jets[1].bDiscr >= 0.669)) ) { //  10% = 0.215      1% = 0.459    0.1% = 0.669
			WW_nwB1 = WW_nwB1 + WW_tune*WW_weight*A_Wei*RD_Lum/100.; 
			WW_nB1 = WW_nB1 + 1;
			WW_hbNum.Fill(1., WW_tune*WW_weight*A_Wei*RD_Lum/100.);
			}
			if ((WW_e.jets[0].bDiscr <= 0.669) && (WW_e.jets[1].bDiscr <= 0.669)) {
            WW_nwB3 = WW_nwB3 + WW_tune*WW_weight*A_Wei*RD_Lum/100.; 			
			WW_nB3 = WW_nB3 + 1;
			WW_hbNum.Fill(0., WW_tune*WW_weight*A_Wei*RD_Lum/100.);
			}
			
			if (WW_e.muons[0].charge*WW_e.muons[1].charge == -1) {
			WW_nwQ = WW_nwQ + WW_tune*WW_weight*A_Wei*RD_Lum/100.; 
			WW_nQ = WW_nQ +1; 
			WW_hQNum.Fill(-0.5, WW_tune*WW_weight*A_Wei*RD_Lum/100.);}
			else {
			WW_nwQ1 = WW_nwQ1 + WW_tune*WW_weight*A_Wei*RD_Lum/100.;
			WW_nQ1 = WW_nQ1 +1;
			WW_hQNum.Fill(0.5, WW_tune*WW_weight*A_Wei*RD_Lum/100.);}
			
			
			if ((WW_e.muons[0].charge==-1)&&(WW_e.muons[1].charge == -1)) {
			WW_nwQ3 = WW_nwQ3 + WW_tune*WW_weight*A_Wei*RD_Lum/100.;
			WW_nQ3 = WW_nQ3 +1;}
			
			if ((WW_e.muons[0].charge==1)&&(WW_e.muons[1].charge == 1)) {
            WW_nwQ4 = WW_nwQ4 + WW_tune*WW_weight*A_Wei*RD_Lum/100.;			
			WW_nQ4 = WW_nQ4 +1;}
			
			
			WW_dVz.Fill((abs(WW_e.muons[0].vertex.z()-WW_e.muons[1].vertex.z())), WW_tune*WW_weight*A_Wei*RD_Lum/100.);
			WW_dVz1.Fill(abs(WW_e.jets[0].vertex.z()-WW_e.jets[1].vertex.z()), WW_tune*WW_weight*A_Wei*RD_Lum/100.);
			WW_dVz2.Fill(abs(WW_e.jets[0].vertex.z()-WW_e.muons[0].vertex.z()), WW_tune*WW_weight*A_Wei*RD_Lum/100.);
			WW_dVz3.Fill(abs(WW_e.jets[1].vertex.z()-WW_e.muons[1].vertex.z()), WW_tune*WW_weight*A_Wei*RD_Lum/100.);
			WW_dVz4.Fill(abs(WW_e.jets[1].vertex.z()-WW_e.muons[0].vertex.z()), WW_tune*WW_weight*A_Wei*RD_Lum/100.);
			WW_dVz5.Fill(abs(WW_e.jets[0].vertex.z()-WW_e.muons[1].vertex.z()), WW_tune*WW_weight*A_Wei*RD_Lum/100.);
	
			WW_PU.Fill(WW_e.vertices.size(), WW_tune*WW_weight*A_Wei*RD_Lum/100.);
			WW_PU1.Fill(WW_e.pileup.size(), WW_tune*WW_weight*A_Wei*RD_Lum/100.);
			WW_hZPt.Fill((K1_Z1*WW_e.muons[0].p4 + K1_Z2*WW_e.muons[1].p4).Pt(), WW_tune*WW_weight*A_Wei*RD_Lum/100.);
		
		
		/*	
			cout<<"New event!" << n <<" PtL1 = " <<(K1_Z1*WW_e.muons[0].p4).Pt()<<" PtL2 = "<<(K1_Z2*WW_e.muons[1].p4).Pt() << " Mll = "<<(K1_Z1*WW_e.muons[0].p4 + K1_Z2*WW_e.muons[1].p4).M() << endl;
			cout<<" PtJ1 = "<<(WW_e.jets[0].p4).Pt()<<" bDiscr J1 = "<< WW_e.jets[0].bDiscr <<" PtJ2 = "<<(WW_e.jets[1].p4).Pt()<<" bDiscr J2 = "<< WW_e.jets[1].bDiscr <<endl;
			cout<<"EtaL1 = "<<(K1_Z1*WW_e.muons[0].p4).Eta()<<" PhiL1 = "<<(K1_Z1*WW_e.muons[0].p4).Phi()<<" EtaL2 = "<<(K1_Z2*WW_e.muons[1].p4).Eta()<<" PhiL2 = "<<(K1_Z2*WW_e.muons[1].p4).Phi() << endl;
			cout<<"EtaJ1 = "<<(WW_e.jets[0].p4).Eta()<<" PhiJ1 = "<<(WW_e.jets[0].p4).Phi()<<" EtaJ2 = "<<(WW_e.jets[1].p4).Eta()<<" PhiJ2 = "<<(WW_e.jets[1].p4).Phi() << endl;			
			
	        cout<<"vzL1 = "<<(WW_e.muons[0].vertex).z()<<" VzL2 = "<<(WW_e.muons[1].vertex).z()<<" VzJ1 = "<<(WW_e.jets[0].vertex).z()<<" VzJ2 = "<<(WW_e.jets[1].vertex).z() << endl;
		
	    	cout<<" L1: ecalIso = " <<WW_e.muons[0].ecalIso <<" hcalIso = "<<WW_e.muons[0].hcalIso <<" hcal1Iso = "<<WW_e.muons[0].hcal1Iso << " trackIso = "<< WW_e.muons[0].trackIso << endl; 
					
			cout<<" L1: charge = " <<WW_e.muons[0].charge << endl; 
	
	       cout<<" L1: ecalIso = " <<WW_e.muons[1].ecalIso <<" hcalIso = "<<WW_e.muons[1].hcalIso <<" hcal1Iso = "<<WW_e.muons[1].hcal1Iso << " trackIso = "<< WW_e.muons[1].trackIso << endl; 
			
			cout<<" L2: charge = " <<WW_e.muons[1].charge << endl;
							
			cout<<"MWR = "<< WR <<" MNuR = "<< NuR_l1 <<" Mjj = "<< Mjj << endl;
			cout <<"RUN = "<< WW_e.irun << " LUMI = " << WW_e.ilumi << " EVENT = "<< WW_e.ievent<< endl;
		    
			
        */  
		}
		}	
		}	
       } 	
        }
	   }
	  }
	 }  
	// Multiplicity
    WW_hElMult.Fill(WW_e.muons.size(),WW_tune*WW_weight*RD_Lum/100.);
    WW_hJetMult.Fill(WW_e.jets.size(),WW_tune*WW_weight*RD_Lum/100.);	
	    

  } 
 BG_nw = BG_nw + WW_nw; // number of total BG events
 delta_BG_nw_sq = delta_BG_nw_sq + (0.20*0.20 + 1/(WW_n+1))*WW_nw*WW_nw ; // 20% TTb systematic BG error 

 cout << "MC BG WW" << endl;
 cout << endl;
 cout << "Total events = " << WW_nEvents << endl;
 cout << "Total weight events = " << WW_nwEvents << endl;
 cout << endl; 
 cout << "Number of passed events = "<<WW_n<<"   "<<WW_nw<< endl;
 cout << endl;
 cout << "Number of events with 0 b-jet = " << WW_nB3<<"   " <<WW_nwB3 << endl; 
 cout << "Number of events with 1 b-jet = " << WW_nB1<<"   " <<WW_nwB1 << endl;
 cout << "Number of events with 2 b-jet = " << WW_nB2<<"   " <<WW_nwB2 << endl;	
 cout << endl;	
 cout << "Number of events with same charges of lepton = " << WW_nQ1<<"   " <<WW_nwQ1 << endl;
 cout << "Number of events with opposite charges of lepton = " << WW_nQ <<"   " <<WW_nwQ << endl; 
 cout << endl;
 cout << "Number of events with e-e- = " << WW_nQ3<<"   " <<WW_nwQ3 << endl;
 cout << "Number of events with e+e+ = " << WW_nQ4 <<"   " <<WW_nwQ4 << endl; 
 cout << endl;
 
}
 

//============================================================================================================================
 
 
// MC BG TW
  
 
  TH1F TW_hWR(  "TW_WR", "W_{R} reconstructed mass;mass, GeV/c^{2};events/50 GeV", M_Delta_M_W, M_min_M_W, M_max_M_W);
  TH1F TW_hNuR0("TW_NuR0", "nu_{R} reconstructed mass using first E electron;mass, GeV/c^{2};events/50 GeV", M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu);
  TH1F TW_hNuR1("TW_NuR1", "nu_{R} reconstructed mass using second E electron;mass, GeV/c^{2};events/50 GeV", M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu); 
  TH1F TW_hMll("TW_Mll", "lepton-lepton pair invariant mass;mass, GeV/c^{2};events/2 GeV", M_Delta_Mll, M_min_Mll, M_max_Mll);
  TH1F TW_hMll1("TW_Mll1", "lepton-lepton pair invariant mass;mass, GeV/c^{2};events/40 GeV", 20, 100, 1000);
  TH1F TW_hMjj("TW_Mjj", "jet-jet pair invariant mass;mass, GeV/c^{2};events/50 GeV", M_Delta_M_Nu, M_min_M_Nu, M_max_M_Nu);
  TH1F TW_hMmets("TW_Mmets", "Miss Et; Et, GeV/c^{2};events/10 GeV", M_Delta_MET, M_min_MET, M_max_MET);
  TH1F TW_hMmets1("TW_Mmets1", "Miss Et; Et, GeV/c^{2};events/30 GeV", 30, 100, 1000); 
  TH1F TW_PtWR(  "TW_PtWR", "W_{R} Pt; Pt, GeV/c^{2}; events/6GeV", 30, 0, 180);
  TH1F TW_PlWR(  "TW_PlWR", "W_{R} Pl;Pl, GeV/c^{2};events/50 GeV", 60, -1500, 1500);
  TH1F TW_EtaWR( "TW_EtaWR", "W_{R} Eta; Eta; events", 10, -6, 6);
  TH1F TW_PhiWR( "TW_PhiWR", "W_{R} Phi; Phi; events", 10, -3.1415, 3.1415); 
  TH1F TW_hbDiscr_j1( "TW_bDiscrJ1", "first jet b Discriminant; bDiscr; events", 15, 0, 1.5); 
  TH1F TW_hbDiscr_j2( "TW_bDiscrJ2", "second jet b Discriminant; bDiscr; events", 15, 0, 1.5); 
  TH1F TW_hbNum( "TW_hbNum", "Number of b jets ; bNum; events", 3, 0, 3); 
  TH1F TW_hQNum( "TW_hQNum", "Mult of leptons charges ; e_1*e_2; events", 2, -1, 1); 
  TH1F TW_hElMult("TW_ElMult", "electrons multiplicity", 10, 0, 10);
       TW_hElMult.GetXaxis()->SetNdivisions(110);
  TH1F TW_hJetMult("TW_JetMult", "jets multiplicity", 20, 0, 20);
       TW_hJetMult.GetXaxis()->SetNdivisions(210);   
  TH1F TW_hElRes("TW_ElRes", "electron energy reconstruction resolution", 100, -0.3, 0.3);
  
  TH1F TW_dVz( "TW_dVz", "delta Vz; #delta V_{z} between ee, cm; Events", 20, 0, 0.1); 
  TH1F TW_dVz1( "TW_dVz1", "delta Vz;#delta V_{z} between jj, cm; Events", 20, 0, 0.1);
  TH1F TW_dVz2( "TW_dVz2", "delta Vz;#delta V_{z} between e_{1}j_{1}, cm; Events", 20, 0, 0.1);  
  TH1F TW_dVz3( "TW_dVz3", "delta Vz;#delta V_{z} between e_{2}j_{2}, cm; Events", 20, 0, 0.1); 
  TH1F TW_dVz4( "TW_dVz4", "delta Vz;#delta V_{z} between e_{2}j_{1}, cm; Events", 20, 0, 0.1);  
  TH1F TW_dVz5( "TW_dVz5", "delta Vz;#delta V_{z} between e_{1}j_{2}, cm; Events", 20, 0, 0.1); 
  
  TH1F TW_PU( "TW_PU", "PU; Primary vertex number ; Events", 25, 0, 25); 
  TH1F TW_PU1( "TW_PU1", "PU1; Primary vertex number ; Events", 25, 0, 25); 
  TH1F TW_hZPt("TW_ZPt", "Z+jets; Z Pt, GeV/c;Events/20 GeV/c", 10, 0, 200);
    	
  fname = "/moscow41/heavynu/41x-background/TToBLNu_TuneZ2_tW-channel_7TeV-madgraph_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root";

  TheEvent TW_e(fname);
  if (!TW_e) return;
  int TW_n = 0; 
  float TW_nw = 0.;  
  
if (TW_incl == true) {

  int TW_nB1 = 0; 
  float TW_nwB1 = 0;
  int TW_nB2 = 0;
  float TW_nwB2 = 0;  
  int TW_nB3 = 0;
  float TW_nwB3 = 0;
  int TW_nEvents = 0; 
  float TW_nwEvents = 0;  
  int TW_nQ = 0;
  float TW_nwQ = 0;
  int TW_nQ1 = 0;
  float TW_nwQ1 = 0;
  int TW_nQ3 = 0;
  float TW_nwQ3 = 0;
  int TW_nQ4 = 0;
  float TW_nwQ4 = 0;
  float TW_weight =0;
  

  TW_nEvents = TW_e.totalEvents();
 
  for (int i = 0; i < TW_nEvents; ++i) {
    TW_e.readEvent(i);  

      if(TW_e.pileup.size()<25){
        TW_weight = TW_e.weight *  generate_flat10_weights(histo)[TW_e.pileup.size()];
        }
        else{ //should not happen as we have a weight for all simulated TW_e.pileup.size() multiplicities!
        TW_weight = 0.;
        }

		TW_nwEvents = TW_nwEvents + TW_tune*TW_weight*RD_Lum/100.;  
   
    // electron energy resolution
    for (unsigned int j = 0; j < TW_e.muons.size(); ++j) {
      const Particle& reco = TW_e.muons[j];
      if (reco.mcId >= 0) {
        const Particle& mc = TW_e.gens[reco.mcId];
        const float r = (reco.p4.E() - mc.p4.E())/mc.p4.E();
        TW_hElRes.Fill(r, TW_tune*TW_weight*A_Wei*RD_Lum/100.);
      }
    }
    
    // mass distributions
      if ((TW_e.muons.size() >= NofLept) && (TW_e.jets.size() >= NofJets)) {
	  
    //  correction of Z mass and ID efficiency
	   if ((abs(TW_e.muons[0].p4.Eta())<=1.44)) {K1_Z1 = 1.;} else {K1_Z1=1.;}
	   if ((abs(K1_Z2*TW_e.muons[1].p4.Eta())<=1.44)) {K1_Z2 = 1.;} else {K1_Z2=1.;}
	  
	   if (abs((K1_Z1*TW_e.muons[0].p4).Eta())<=1.44){A_Wei=HEEP_B;} else {A_Wei=HEEP_E;}
	   if (abs((K1_Z2*TW_e.muons[1].p4).Eta())<=1.44){A_Wei=A_Wei*HEEP_B;} else {A_Wei=A_Wei*HEEP_E;}
	
	//cuts on electrons Pt>=40
	    if (((K1_Z1*TW_e.muons[0].p4).Pt()>=Lep1Pt) && ((K1_Z2*TW_e.muons[1].p4).Pt()>=Lep2Pt))  { 
    //cuts on jetss Pt>=40      
	      if (((TW_e.jets[0].p4).Pt()>=Jet1Pt) && ((TW_e.jets[1].p4).Pt()>=Jet2Pt))  { 
		  
	//cuts on electrons invariant mass Mll>=400.0	
	  if (((K1_Z1*TW_e.muons[0].p4 + K1_Z2*TW_e.muons[1].p4).M()>=MllCut)&& ((K1_Z1*TW_e.muons[0].p4 + K1_Z2*TW_e.muons[1].p4).M()<=MllCut1)) {
//	  if (!(((K1_Z1*TW_e.muons[0].p4 + K1_Z2*TW_e.muons[1].p4).M()>=MllCut)&& ((K1_Z1*TW_e.muons[0].p4 + K1_Z2*TW_e.muons[1].p4).M()<=MllCut1))) {
			  
	//cuts on Eta  
         if (((abs(K1_Z1*TW_e.muons[0].p4.Eta())<=EtaCut1L) || (abs(K1_Z2*TW_e.muons[1].p4.Eta())<=EtaCut2L)) 
	     && (abs(TW_e.jets[0].p4.Eta())<=EtaCut1J) && (abs(TW_e.jets[1].p4.Eta())<=EtaCut2J) )        {
			
	// Isolation
	       if ((TW_e.muons[0].isoHEEP() <= ISO)&&(TW_e.muons[1].isoHEEP() <= ISO)) {
		   
	 //Cut on vertexes
             if ((abs(TW_e.muons[0].vertex.z()-TW_e.muons[1].vertex.z())<=Vertex) && (abs(TW_e.jets[0].vertex.z()-TW_e.jets[1].vertex.z())<=Vertex) 
             && ((abs(TW_e.muons[0].vertex.z()-TW_e.jets[1].vertex.z())<=Vertex) && (abs(TW_e.jets[0].vertex.z()-TW_e.muons[1].vertex.z())<=Vertex))) {
			 
	        WR = (K1_Z1*TW_e.muons[0].p4 + K1_Z2*TW_e.muons[1].p4 + TW_e.jets[0].p4 + TW_e.jets[1].p4).M();
			if (WR>=MW_Cut) {
            TW_hWR.Fill(WR, TW_tune*TW_weight*A_Wei*RD_Lum/100.);
	  
	        tWR = (K1_Z1*TW_e.muons[0].p4 + K1_Z2*TW_e.muons[1].p4 + TW_e.jets[0].p4 + TW_e.jets[1].p4).Pt();
            TW_PtWR.Fill(tWR, TW_tune*TW_weight*A_Wei*RD_Lum/100.);
	  
	        lWR = (K1_Z1*TW_e.muons[0].p4 + K1_Z2*TW_e.muons[1].p4 + TW_e.jets[0].p4 + TW_e.jets[1].p4).Pz();
            TW_PlWR.Fill(lWR, TW_tune*TW_weight*A_Wei*RD_Lum/100.);
	  
	        eWR = (K1_Z1*TW_e.muons[0].p4 + K1_Z2*TW_e.muons[1].p4 + TW_e.jets[0].p4 + TW_e.jets[1].p4).Eta();
            TW_EtaWR.Fill(eWR, TW_tune*TW_weight*A_Wei*RD_Lum/100.);
	  
	        pWR = (K1_Z1*TW_e.muons[0].p4 + K1_Z2*TW_e.muons[1].p4 + TW_e.jets[0].p4 + TW_e.jets[1].p4).Phi();
            TW_PhiWR.Fill(pWR, TW_tune*TW_weight*A_Wei*RD_Lum/100.);
	  
            NuR_l0 = (K1_Z1*TW_e.muons[0].p4 + TW_e.jets[0].p4 + TW_e.jets[1].p4).M();
            NuR_l1 = (K1_Z2*TW_e.muons[1].p4 + TW_e.jets[0].p4 + TW_e.jets[1].p4).M();
      
	        TW_hNuR0.Fill(NuR_l0, TW_tune*TW_weight*A_Wei*RD_Lum/100.);
	        TW_hNuR1.Fill(NuR_l1, TW_tune*TW_weight*A_Wei*RD_Lum/100.);
	        
            Mll = (K1_Z1*TW_e.muons[0].p4 + K1_Z2*TW_e.muons[1].p4).M();
            TW_hMll.Fill(Mll, TW_tune*TW_weight*A_Wei*RD_Lum/100.);
			TW_hMll1.Fill(Mll, TW_tune*TW_weight*A_Wei*RD_Lum/100.);
      
            Mjj = (TW_e.jets[0].p4 + TW_e.jets[1].p4).M();
            TW_hMjj.Fill(Mjj, TW_tune*TW_weight*A_Wei*RD_Lum/100.);
			
			Mmet = (TW_e.mets[0].p4).Pt();
            TW_hMmets.Fill(Mmet, TW_tune*TW_weight*A_Wei*RD_Lum/100.);
		    TW_hMmets1.Fill(Mmet, TW_tune*TW_weight*A_Wei*RD_Lum/100.);
			
			TW_hbDiscr_j1.Fill(TW_e.jets[0].bDiscr, TW_tune*TW_weight*A_Wei*RD_Lum/100.);
			TW_hbDiscr_j2.Fill(TW_e.jets[1].bDiscr, TW_tune*TW_weight*A_Wei*RD_Lum/100.);
							
			TW_n = TW_n + 1;
			TW_nw = TW_nw + TW_tune*TW_weight*A_Wei*RD_Lum/100.; 
						
			if ((TW_e.jets[0].bDiscr >= 0.669) && (TW_e.jets[1].bDiscr >= 0.669)) { //  10% = 0.215      1% = 0.459    0.1% = 0.669
			TW_nwB2 = TW_nwB2 + TW_tune*TW_weight*A_Wei*RD_Lum/100.; 
			TW_nB2 = TW_nB2 + 1;
			TW_hbNum.Fill(2., TW_tune*TW_weight*A_Wei*RD_Lum/100.);
			}
			if (((TW_e.jets[0].bDiscr >= 0.669) && (TW_e.jets[1].bDiscr <= 0.669)) || ((TW_e.jets[0].bDiscr <= 0.669) && (TW_e.jets[1].bDiscr >= 0.669)) ) { //  10% = 0.215      1% = 0.459    0.1% = 0.669
			TW_nwB1 = TW_nwB1 + TW_tune*TW_weight*A_Wei*RD_Lum/100.; 
			TW_nB1 = TW_nB1 + 1;
			TW_hbNum.Fill(1., TW_tune*TW_weight*A_Wei*RD_Lum/100.);
			}
			if ((TW_e.jets[0].bDiscr <= 0.669) && (TW_e.jets[1].bDiscr <= 0.669)) {
            TW_nwB3 = TW_nwB3 + TW_tune*TW_weight*A_Wei*RD_Lum/100.; 			
			TW_nB3 = TW_nB3 + 1;
			TW_hbNum.Fill(0., TW_tune*TW_weight*A_Wei*RD_Lum/100.);
			}
			
			if (TW_e.muons[0].charge*TW_e.muons[1].charge == -1) {
			TW_nwQ = TW_nwQ + TW_tune*TW_weight*A_Wei*RD_Lum/100.; 
			TW_nQ = TW_nQ +1; 
			TW_hQNum.Fill(-0.5, TW_tune*TW_weight*A_Wei*RD_Lum/100.);}
			else {
			TW_nwQ1 = TW_nwQ1 + TW_tune*TW_weight*A_Wei*RD_Lum/100.;
			TW_nQ1 = TW_nQ1 +1;
			TW_hQNum.Fill(0.5, TW_tune*TW_weight*A_Wei*RD_Lum/100.);}
			
			
			if ((TW_e.muons[0].charge==-1)&&(TW_e.muons[1].charge == -1)) {
			TW_nwQ3 = TW_nwQ3 + TW_tune*TW_weight*A_Wei*RD_Lum/100.;
			TW_nQ3 = TW_nQ3 +1;}
			
			if ((TW_e.muons[0].charge==1)&&(TW_e.muons[1].charge == 1)) {
            TW_nwQ4 = TW_nwQ4 + TW_tune*TW_weight*A_Wei*RD_Lum/100.;			
			TW_nQ4 = TW_nQ4 +1;}
			
			
			TW_dVz.Fill((abs(TW_e.muons[0].vertex.z()-TW_e.muons[1].vertex.z())), TW_tune*TW_weight*A_Wei*RD_Lum/100.);
			TW_dVz1.Fill(abs(TW_e.jets[0].vertex.z()-TW_e.jets[1].vertex.z()), TW_tune*TW_weight*A_Wei*RD_Lum/100.);
			TW_dVz2.Fill(abs(TW_e.jets[0].vertex.z()-TW_e.muons[0].vertex.z()), TW_tune*TW_weight*A_Wei*RD_Lum/100.);
			TW_dVz3.Fill(abs(TW_e.jets[1].vertex.z()-TW_e.muons[1].vertex.z()), TW_tune*TW_weight*A_Wei*RD_Lum/100.);
			TW_dVz4.Fill(abs(TW_e.jets[1].vertex.z()-TW_e.muons[0].vertex.z()), TW_tune*TW_weight*A_Wei*RD_Lum/100.);
			TW_dVz5.Fill(abs(TW_e.jets[0].vertex.z()-TW_e.muons[1].vertex.z()), TW_tune*TW_weight*A_Wei*RD_Lum/100.);
	
			TW_PU.Fill(TW_e.vertices.size(), TW_tune*TW_weight*A_Wei*RD_Lum/100.);
			TW_PU1.Fill(TW_e.pileup.size(), TW_tune*TW_weight*A_Wei*RD_Lum/100.);
			TW_hZPt.Fill((K1_Z1*TW_e.muons[0].p4 + K1_Z2*TW_e.muons[1].p4).Pt(), TW_tune*TW_weight*A_Wei*RD_Lum/100.);
		
		
		/*	
			cout<<"New event!" << n <<" PtL1 = " <<(K1_Z1*TW_e.muons[0].p4).Pt()<<" PtL2 = "<<(K1_Z2*TW_e.muons[1].p4).Pt() << " Mll = "<<(K1_Z1*TW_e.muons[0].p4 + K1_Z2*TW_e.muons[1].p4).M() << endl;
			cout<<" PtJ1 = "<<(TW_e.jets[0].p4).Pt()<<" bDiscr J1 = "<< TW_e.jets[0].bDiscr <<" PtJ2 = "<<(TW_e.jets[1].p4).Pt()<<" bDiscr J2 = "<< TW_e.jets[1].bDiscr <<endl;
			cout<<"EtaL1 = "<<(K1_Z1*TW_e.muons[0].p4).Eta()<<" PhiL1 = "<<(K1_Z1*TW_e.muons[0].p4).Phi()<<" EtaL2 = "<<(K1_Z2*TW_e.muons[1].p4).Eta()<<" PhiL2 = "<<(K1_Z2*TW_e.muons[1].p4).Phi() << endl;
			cout<<"EtaJ1 = "<<(TW_e.jets[0].p4).Eta()<<" PhiJ1 = "<<(TW_e.jets[0].p4).Phi()<<" EtaJ2 = "<<(TW_e.jets[1].p4).Eta()<<" PhiJ2 = "<<(TW_e.jets[1].p4).Phi() << endl;			
			
	        cout<<"vzL1 = "<<(TW_e.muons[0].vertex).z()<<" VzL2 = "<<(TW_e.muons[1].vertex).z()<<" VzJ1 = "<<(TW_e.jets[0].vertex).z()<<" VzJ2 = "<<(TW_e.jets[1].vertex).z() << endl;
		
	    	cout<<" L1: ecalIso = " <<TW_e.muons[0].ecalIso <<" hcalIso = "<<TW_e.muons[0].hcalIso <<" hcal1Iso = "<<TW_e.muons[0].hcal1Iso << " trackIso = "<< TW_e.muons[0].trackIso << endl; 
					
			cout<<" L1: charge = " <<TW_e.muons[0].charge << endl; 
	
	       cout<<" L1: ecalIso = " <<TW_e.muons[1].ecalIso <<" hcalIso = "<<TW_e.muons[1].hcalIso <<" hcal1Iso = "<<TW_e.muons[1].hcal1Iso << " trackIso = "<< TW_e.muons[1].trackIso << endl; 
			
			cout<<" L2: charge = " <<TW_e.muons[1].charge << endl;
							
			cout<<"MWR = "<< WR <<" MNuR = "<< NuR_l1 <<" Mjj = "<< Mjj << endl;
			cout <<"RUN = "<< TW_e.irun << " LUMI = " << TW_e.ilumi << " EVENT = "<< TW_e.ievent<< endl;
		    
			
        */  
		}
		}	
		}	
       } 	
        }
	   }
	  }
	 }  
	// Multiplicity
    TW_hElMult.Fill(TW_e.muons.size(),TW_tune*TW_weight*RD_Lum/100.);
    TW_hJetMult.Fill(TW_e.jets.size(),TW_tune*TW_weight*RD_Lum/100.);	
	    

  } 
 BG_nw = BG_nw + TW_nw; // number of total BG events
 delta_BG_nw_sq = delta_BG_nw_sq + (0.20*0.20 + 1/(TW_n+1))*TW_nw*TW_nw ; // 20% TTb systematic BG error 

 cout << "MC BG TW" << endl;
 cout << endl;
 cout << "Total events = " << TW_nEvents << endl;
 cout << "Total weight events = " << TW_nwEvents << endl;
 cout << endl; 
 cout << "Number of passed events = "<<TW_n<<"   "<<TW_nw<< endl;
 cout << endl;
 cout << "Number of events with 0 b-jet = " << TW_nB3<<"   " <<TW_nwB3 << endl; 
 cout << "Number of events with 1 b-jet = " << TW_nB1<<"   " <<TW_nwB1 << endl;
 cout << "Number of events with 2 b-jet = " << TW_nB2<<"   " <<TW_nwB2 << endl;	
 cout << endl;	
 cout << "Number of events with same charges of lepton = " << TW_nQ1<<"   " <<TW_nwQ1 << endl;
 cout << "Number of events with opposite charges of lepton = " << TW_nQ <<"   " <<TW_nwQ << endl; 
 cout << endl;
 cout << "Number of events with e-e- = " << TW_nQ3<<"   " <<TW_nwQ3 << endl;
 cout << "Number of events with e+e+ = " << TW_nQ4 <<"   " <<TW_nwQ4 << endl; 
 cout << endl;
 
}
 

//=========================================================================================================================

// Histos plotting and saving

 
 
 
//  gStyle->SetOptStat("");
  

 
  TCanvas c("OUTPUT","OUTPUT");


  
//  c.SetMargin(0.15, 0.05, 0.15, 0.05);
   
  c.Print("OUTPUT-MUMU.ps[");
  
  //gStyle->SetErrorX(1);
   
 
  WW_hWR.Add(&WJ_hWR);
  TW_hWR.Add(&WW_hWR);
  QCD_hWR.Add(&TW_hWR);
  ZZ_hWR.Add(&QCD_hWR);
  ZW_hWR.Add(&ZZ_hWR);
  TTb_hWR.Add(&ZW_hWR);
  ZJ_hWR.Add(&TTb_hWR);

  
  
 /* 
  TTb_hWR.Add(&ZJ_hWR);
  QCD_hWR.Add(&TTb_hWR);
  //QCD_hWR.Add(&ZJ_hWR);
  WJ_hWR.Add(&QCD_hWR);
  ZW_hWR.Add(&WJ_hWR);
  ZZ_hWR.Add(&ZW_hWR);
  WW_hWR.Add(&ZZ_hWR);
  TW_hWR.Add(&WW_hWR);
  */
 /*
  // ============================================
  // save to .root file M_WR histograms with:
  //  1. MC: summ of all backgrounds
  //  2. MC: signal
  //  3. DATA:
  //{
  //	TTb_hWR.Sumw2();
//	ZJ_hWR.Sumw2();
//	TW_hWR.Sumw2();
//	RD_hWR.Sumw2();
    TFile f3("fit-input-MW.root", "RECREATE");
    TW_hWR.Clone("Other")->Write();
  //   S_hWR.Clone("Signal")->Write();
  //  ZJ_hWR.Clone("ZJ")->Write();
    TTb_hWR.Clone("TTb")->Write();
    RD_hWR.Clone("Data")->Write();
    f3.Close();
  // }
  // ============================================
 */ 
  S_hWR.Add(&ZJ_hWR);

  
  RD_hWR.SetLineColor(1);
  RD_hWR.SetLineWidth(5); 
  
  S_hWR.SetLineStyle(1);
  S_hWR.SetLineColor(1);
  //S_hWR.SetFillStyle(0);
  S_hWR.SetFillColor(2);
  S_hWR.SetLineWidth(2);
  
  ZJ_hWR.SetLineStyle(1);
  ZJ_hWR.SetLineColor(1);
  //ZJ_hWR.SetFillStyle(4);
  ZJ_hWR.SetFillColor(3);
  ZJ_hWR.SetLineWidth(2);
  
  TTb_hWR.SetLineStyle(1);
  TTb_hWR.SetLineColor(1);
  //TTb_hWR.SetFillStyle(5);
  TTb_hWR.SetFillColor(4);
  TTb_hWR.SetLineWidth(2);
  
  ZW_hWR.SetLineStyle(1);
  ZW_hWR.SetLineColor(1);
  //ZW_hWR.SetFillStyle(6);
  ZW_hWR.SetFillColor(5);
  ZW_hWR.SetLineWidth(2);
  
  ZZ_hWR.SetLineStyle(1);
  ZZ_hWR.SetLineColor(1);
  //ZW_hWR.SetFillStyle(6);
  ZZ_hWR.SetFillColor(6);
  ZZ_hWR.SetLineWidth(2);
  
  WW_hWR.SetLineStyle(1);
  WW_hWR.SetLineColor(1);
  //WW_hWR.SetFillStyle(6);
  WW_hWR.SetFillColor(7);
  WW_hWR.SetLineWidth(2);
  

  WJ_hWR.SetLineStyle(1);
  WJ_hWR.SetLineColor(1);
  WJ_hWR.SetFillColor(1);
  WJ_hWR.SetLineWidth(2);  
  
  QCD_hWR.SetLineStyle(1);
  QCD_hWR.SetLineColor(1);
  QCD_hWR.SetFillColor(11);
  QCD_hWR.SetLineWidth(2); 
  
  TW_hWR.SetLineStyle(1);
  TW_hWR.SetLineColor(1);
  TW_hWR.SetFillColor(12);
  TW_hWR.SetLineWidth(2);
  
  
 RD_hWR.Draw("e1"); S_hWR.Draw("HISTsame"); ZJ_hWR.Draw("HISTsame"); TTb_hWR.Draw("HISTsame");  ZW_hWR.Draw("HISTsame"); ZZ_hWR.Draw("HISTsame"); QCD_hWR.Draw("HISTsame"); TW_hWR.Draw("HISTsame"); WW_hWR.Draw("HISTsame");  WJ_hWR.Draw("HISTsame");   RD_hWR.Draw("e1same"); 

 
 RD_hWR.GetYaxis()->SetRangeUser(0.01, 1000);
 c.Modified();
 
TLegend leg(0.58, 0.59, 0.94, 0.94);
//TLegend leg(0.17, 0.52, 0.57, 0.94);
leg.SetTextFont(42);
leg.SetTextSize(0.034);
   
  sprintf(str1,"Muon-Muon channel"); 
  leg.SetHeader(str1);
// sprintf(str1,"Data Run2010: %d", RD_n); 
  sprintf(str1,"Data Run2011: %d", RD_n); 
  leg.AddEntry(&RD_hWR, str1, "EL");
  sprintf(str1,"Signal: %5.2f",S_nw); 
  leg.AddEntry(&S_hWR, str1,"F");
  sprintf(str1,"Z+jets BG: %5.2f", ZJ_nw);
  leg.AddEntry(&ZJ_hWR, str1,"F");
  sprintf(str1,"t#bar{t} BG: %5.2f", TTb_nw);
  leg.AddEntry(&TTb_hWR, str1,"F");
  sprintf(str1,"Z+W BG: %5.2f", ZW_nw);
  leg.AddEntry(&ZW_hWR, str1,"F");
  sprintf(str1,"Z+Z BG: %5.2f", ZZ_nw);
  leg.AddEntry(&ZZ_hWR, str1,"F");
  sprintf(str1,"QCD BG: %5.2f", QCD_nw);
  leg.AddEntry(&QCD_hWR, str1,"F");
  sprintf(str1,"tW BG: %5.2f", TW_nw);
  leg.AddEntry(&TW_hWR, str1,"F");
  sprintf(str1,"W+W BG: %5.2f", WW_nw);
  leg.AddEntry(&WW_hWR, str1,"F");
  sprintf(str1,"W+jets BG: %5.2f", WJ_nw);
  leg.AddEntry(&WJ_hWR, str1,"F");
  leg.Draw();
   
  label_Lumi(0.38, 0.85, RD_Lum);
  label_Prelim(0.38, 0.9);
 
  c.Print("OUTPUT-MUMU.ps");
  c.Print("M_W-MUMU.pdf");
  c.Print("M_W-MUMU.png");
  c.Print("M_W-MUMU.cxx");
  
  WW_hNuR1.Add(&WJ_hNuR1);
  TW_hNuR1.Add(&WW_hNuR1);
  QCD_hNuR1.Add(&TW_hNuR1);
  ZZ_hNuR1.Add(&QCD_hNuR1);
  ZW_hNuR1.Add(&ZZ_hNuR1);
  TTb_hNuR1.Add(&ZW_hNuR1);
  ZJ_hNuR1.Add(&TTb_hNuR1);

  
  
  /*
  TTb_hNuR1.Add(&ZJ_hNuR1);
  QCD_hNuR1.Add(&TTb_hNuR1);
  WJ_hNuR1.Add(&QCD_hNuR1);
  ZW_hNuR1.Add(&WJ_hNuR1);
  ZZ_hNuR1.Add(&ZW_hNuR1);
  WW_hNuR1.Add(&ZZ_hNuR1);
  TW_hNuR1.Add(&WW_hNuR1);
 */
/* 
  // ============================================
  // save to .root file M_NuR1 histograms with:
  //  1. MC: summ of all backgrounds
  //  2. MC: signal
  //  3. DATA:
//  {
    TFile f1("fit-input-MNuR1.root", "RECREATE");
    TW_hNuR1.Clone("Other")->Write();
   // S_hNuR1.Clone("Signal")->Write();
    ZJ_hNuR1.Clone("ZJ")->Write();
    RD_hNuR1.Clone("Data")->Write();
    f1.Close();
 // }
  // ============================================
 */
    S_hNuR1.Add(&ZJ_hNuR1);

  
  RD_hNuR1.SetLineColor(1);
  RD_hNuR1.SetLineWidth(5); 
  
  S_hNuR1.SetLineStyle(1);
  S_hNuR1.SetLineColor(1);
  //S_hNuR1.SetFillStyle(0);
  S_hNuR1.SetFillColor(2);
  S_hNuR1.SetLineWidth(2);
  
  ZJ_hNuR1.SetLineStyle(1);
  ZJ_hNuR1.SetLineColor(1);
  //ZJ_hNuR1.SetFillStyle(4);
  ZJ_hNuR1.SetFillColor(3);
  ZJ_hNuR1.SetLineWidth(2);
  
  TTb_hNuR1.SetLineStyle(1);
  TTb_hNuR1.SetLineColor(1);
  //TTb_hNuR1.SetFillStyle(5);
  TTb_hNuR1.SetFillColor(4);
  TTb_hNuR1.SetLineWidth(2);
  
  ZW_hNuR1.SetLineStyle(1);
  ZW_hNuR1.SetLineColor(1);
  //ZW_hNuR1.SetFillStyle(6);
  ZW_hNuR1.SetFillColor(5);
  ZW_hNuR1.SetLineWidth(2);
  
  ZZ_hNuR1.SetLineStyle(1);
  ZZ_hNuR1.SetLineColor(1);
  //ZW_hNuR1.SetFillStyle(6);
  ZZ_hNuR1.SetFillColor(6);
  ZZ_hNuR1.SetLineWidth(2);
  
  WW_hNuR1.SetLineStyle(1);
  WW_hNuR1.SetLineColor(1);
  //WW_hNuR1.SetFillStyle(6);
  WW_hNuR1.SetFillColor(7);
  WW_hNuR1.SetLineWidth(2);
  

  WJ_hNuR1.SetLineStyle(1);
  WJ_hNuR1.SetLineColor(1);
  WJ_hNuR1.SetFillColor(1);
  WJ_hNuR1.SetLineWidth(2);  
  
  QCD_hNuR1.SetLineStyle(1);
  QCD_hNuR1.SetLineColor(1);
  QCD_hNuR1.SetFillColor(11);
  QCD_hNuR1.SetLineWidth(2); 
  
  TW_hNuR1.SetLineStyle(1);
  TW_hNuR1.SetLineColor(1);
  TW_hNuR1.SetFillColor(12);
  TW_hNuR1.SetLineWidth(2);
  
  
 RD_hNuR1.Draw("e1"); S_hNuR1.Draw("HISTsame"); ZJ_hNuR1.Draw("HISTsame"); TTb_hNuR1.Draw("HISTsame");  ZW_hNuR1.Draw("HISTsame"); ZZ_hNuR1.Draw("HISTsame"); QCD_hNuR1.Draw("HISTsame"); TW_hNuR1.Draw("HISTsame"); WW_hNuR1.Draw("HISTsame");  WJ_hNuR1.Draw("HISTsame");  RD_hNuR1.Draw("e1same"); 

 RD_hNuR1.GetYaxis()->SetRangeUser(0.01, 1000);
  c.Modified();
 
   leg.Draw();
  label_Lumi(0.38, 0.85, RD_Lum);
  label_Prelim(0.38, 0.9);
  
  
   c.Print("OUTPUT-MUMU.ps");
   c.Print("M_Nu1-MUMU.pdf");
   c.Print("M_Nu1-MUMU.png");
   c.Print("M_Nu1-MUMU.cxx");

  WW_hMll.Add(&WJ_hMll);
  TW_hMll.Add(&WW_hMll);
  QCD_hMll.Add(&TW_hMll);
  ZZ_hMll.Add(&QCD_hMll);
  ZW_hMll.Add(&ZZ_hMll);
  TTb_hMll.Add(&ZW_hMll);
  // ============================================
  // save to .root file M_l1 histograms with:
  //  1. MC: summ of all backgrounds
  //  2. MC: signal
  //  3. DATA:
  //{
  // ZJ_hMll.Sumw2();
  // TW_hMll.Sumw2();
 //	RD_hMll.Sumw2();
    TFile f2("fit-input-Mll-MUMU.root", "RECREATE");
    TTb_hMll.Clone("Other")->Write();
   // S_hMll.Clone("Signal")->Write();
    ZJ_hMll.Clone("ZJ")->Write();
    RD_hMll.Clone("Data")->Write();
    f2.Close();
  //}
  // ============================================
  ZJ_hMll.Add(&TTb_hMll); 
  S_hMll.Add(&ZJ_hMll);

  
  RD_hMll.SetLineColor(1);
  RD_hMll.SetLineWidth(5); 
  
  S_hMll.SetLineStyle(1);
  S_hMll.SetLineColor(1);
  //S_hMll.SetFillStyle(0);
  S_hMll.SetFillColor(2);
  S_hMll.SetLineWidth(2);
  
  ZJ_hMll.SetLineStyle(1);
  ZJ_hMll.SetLineColor(1);
  //ZJ_hMll.SetFillStyle(4);
  ZJ_hMll.SetFillColor(3);
  ZJ_hMll.SetLineWidth(2);
  
  TTb_hMll.SetLineStyle(1);
  TTb_hMll.SetLineColor(1);
  //TTb_hMll.SetFillStyle(5);
  TTb_hMll.SetFillColor(4);
  TTb_hMll.SetLineWidth(2);
  
  ZW_hMll.SetLineStyle(1);
  ZW_hMll.SetLineColor(1);
  //ZW_hMll.SetFillStyle(6);
  ZW_hMll.SetFillColor(5);
  ZW_hMll.SetLineWidth(2);
  
  ZZ_hMll.SetLineStyle(1);
  ZZ_hMll.SetLineColor(1);
  //ZW_hMll.SetFillStyle(6);
  ZZ_hMll.SetFillColor(6);
  ZZ_hMll.SetLineWidth(2);
  
  WW_hMll.SetLineStyle(1);
  WW_hMll.SetLineColor(1);
  //WW_hMll.SetFillStyle(6);
  WW_hMll.SetFillColor(7);
  WW_hMll.SetLineWidth(2);
  

  WJ_hMll.SetLineStyle(1);
  WJ_hMll.SetLineColor(1);
  WJ_hMll.SetFillColor(1);
  WJ_hMll.SetLineWidth(2);  
  
  QCD_hMll.SetLineStyle(1);
  QCD_hMll.SetLineColor(1);
  QCD_hMll.SetFillColor(11);
  QCD_hMll.SetLineWidth(2); 
  
  TW_hMll.SetLineStyle(1);
  TW_hMll.SetLineColor(1);
  TW_hMll.SetFillColor(12);
  TW_hMll.SetLineWidth(2);
 
  
 // RD_hMll.Fit("gaus"); 
  /*
  TF1* fitf = new TF1("fitf", "[0]*exp(-[1]*x) + [2]*exp(-([3] - x)*([3] - x)/(2*[4]*[4]))", 0, 200);
  fitf->SetNpx(500);
  fitf->SetParameters(10, 0.01, 10, 90, 10);

  RD_hMll.Fit(fitf, "IMN");
  const double A = fitf->GetParameter(2); // area under gaussian
  const double B = fitf->GetParameter(3); // area under gaussian
  const double C = fitf->GetParameter(4); // area under gaussian
  
 // cout << "M_Z = "<< B << endl;
 // cout << "Width_Z = "<< C <<endl;
  
  //ZJ_hMll.Fit("gaus");
  
  TF1* fitf1 = new TF1("fitf1", "[0]*exp(-[1]*x) + [2]*exp(-([3] - x)*([3] - x)/(2*[4]*[4]))", 0, 200);
  fitf1->SetNpx(500);
  fitf1->SetParameters(10, 0.01, 10, 90, 10);
  ZJ_hMll.Fit(fitf1, "IMN");
  const double A1 = fitf1->GetParameter(2); // area under gaussian
  const double B1 = fitf1->GetParameter(3); // area under gaussian
  const double C1 = fitf1->GetParameter(4); // area under gaussian
  
  cout << "MC M_Z = "<< B1 << endl;
  cout << "MC Width_Z = "<< C1 <<endl;
 */ 

 
 RD_hMll.Draw("e1"); S_hMll.Draw("HISTsame"); ZJ_hMll.Draw("HISTsame"); TTb_hMll.Draw("HISTsame");  ZW_hMll.Draw("HISTsame"); ZZ_hMll.Draw("HISTsame"); QCD_hMll.Draw("HISTsame"); TW_hMll.Draw("HISTsame"); WW_hMll.Draw("HISTsame");  WJ_hMll.Draw("HISTsame"); RD_hMll.Draw("e1same"); 

  RD_hMll.GetYaxis()->SetRangeUser(0.1, 100000);
//  RD_hMll.GetYaxis()->SetRangeUser(5, 10000);
  
 // RD_hMll.GetYaxis()->SetLimitsUser(-50, 1000);
  c.Modified();
 
 
 /*
  // draw fit to background
  fitf->SetLineColor(kBlack);
  fitf->SetLineStyle(7);
  fitf->SetLineWidth(3);
  fitf->SetParameter(2, 0);
  //fitf->DrawCopy("same");
  
  // draw fit to background + signal
  fitf->SetLineColor(kBlack);
  fitf->SetLineStyle(1);
  fitf->SetLineWidth(3);
  fitf->SetParameter(2, A);
  //fitf->Draw("HISTsame");
  
  // draw fit to background
  fitf1->SetLineColor(kGreen);
  fitf1->SetLineStyle(7);
  fitf1->SetLineWidth(3);
  fitf1->SetParameter(2, 0);
  //fitf1->DrawCopy("same");
  
  // draw fit to background + signal
  fitf1->SetLineColor(kGreen);
  fitf1->SetLineStyle(1);
  fitf1->SetLineWidth(3);
  fitf1->SetParameter(2, A1);
  //fitf1->Draw("HISTsame");
*/
  leg.Draw();
  label_Lumi(0.38, 0.85, RD_Lum);
  label_Prelim(0.38, 0.9);
  c.Print("OUTPUT-MUMU.ps");
  
  c.Print("M_ll-MUMU.pdf");
  c.Print("M_ll-MUMU.png");   
  c.Print("M_ll-MUMU.cxx");   
  
  
  TTb_hMll1.Add(&ZJ_hMll1);
  QCD_hMll1.Add(&TTb_hMll1);
  WJ_hMll1.Add(&QCD_hMll1);
  ZW_hMll1.Add(&WJ_hMll1);
  ZZ_hMll1.Add(&ZW_hMll1);
  WW_hMll1.Add(&ZZ_hMll1);
  TW_hMll1.Add(&WW_hMll1);
  S_hMll1.Add(&TW_hMll1);
  
  RD_hMll1.SetLineColor(1);
  RD_hMll1.SetLineWidth(5); 
  
  S_hMll1.SetLineStyle(1);
  S_hMll1.SetLineColor(2);
  //S_hMll1.SetFillStyle(0);
  //S_hMll1.SetFillColor(kRed);
  S_hMll1.SetLineWidth(5);
  
  ZJ_hMll1.SetLineStyle(2);
  ZJ_hMll1.SetLineColor(3);
  //ZJ_hMll1.SetFillStyle(4);
  //ZJ_hMll1.SetFillColor(kGreen);
  ZJ_hMll1.SetLineWidth(5);
  
  TTb_hMll1.SetLineStyle(3);
  TTb_hMll1.SetLineColor(4);
  //TTb_hMll1.SetFillStyle(5);
  //TTb_hMll1.SetFillColor(kYellow);
  TTb_hMll1.SetLineWidth(5);
  
  ZW_hMll1.SetLineStyle(4);
  ZW_hMll1.SetLineColor(5);
  //ZW_hMll1.SetFillStyle(6);
  //ZW_hMll1.SetFillColor(kBlue);
  ZW_hMll1.SetLineWidth(5);
  
  ZZ_hMll1.SetLineStyle(5);
  ZZ_hMll1.SetLineColor(6);
  ZZ_hMll1.SetLineWidth(5);
  
  WW_hMll1.SetLineStyle(6);
  WW_hMll1.SetLineColor(7);
  WW_hMll1.SetLineWidth(5);
  
  WJ_hMll1.SetLineStyle(7);
  WJ_hMll1.SetLineColor(8);
  WJ_hMll1.SetLineWidth(5);
  
  QCD_hMll1.SetLineStyle(8);
  QCD_hMll1.SetLineColor(11);
  QCD_hMll1.SetLineWidth(5);
  
  TW_hMll1.SetLineStyle(9);
  TW_hMll1.SetLineColor(12);
  TW_hMll1.SetLineWidth(5);
 
  RD_hMll1.Draw("e1"); S_hMll1.Draw("HISTsame"); TW_hMll1.Draw("HISTsame"); WW_hMll1.Draw("HISTsame"); ZZ_hMll1.Draw("HISTsame"); ZW_hMll1.Draw("HISTsame"); WJ_hMll1.Draw("HISTsame"); QCD_hMll1.Draw("HISTsame"); TTb_hMll1.Draw("HISTsame"); ZJ_hMll1.Draw("HISTsame"); RD_hMll1.Draw("e1same");leg.Draw(); /*latex.DrawLatex(0.2,3.5,str); latex.DrawLatex(0.2,3.3,str1);*/ c.Print("OUTPUT-MUMU.ps");
  //S_hMll1.Draw(); TW_hMll1.Draw("HISTsame"); WW_hMll1.Draw("HISTsame"); ZZ_hMll1.Draw("HISTsame"); ZW_hMll1.Draw("HISTsame"); WJ_hMll1.Draw("HISTsame"); QCD_hMll1.Draw("HISTsame"); TTb_hMll1.Draw("HISTsame"); ZJ_hMll1.Draw("HISTsame"); RD_hMll1.Draw("e1same");leg.Draw(); latex.DrawLatex(125,13.5,str); latex.DrawLatex(125,13.0,str1); c.Print("OUTPUT-MUMU.ps");
  
  label_Lumi(0.38, 0.85, RD_Lum);
  label_Prelim(0.38, 0.9);
 
 // c.Print("M_ll1-MUMU.pdf");
 // c.Print("M_ll1-MUMU.png");
 // c.Print("M_ll1-MUMU.cxx");

  
  TTb_PtWR.Add(&ZJ_PtWR);
  QCD_PtWR.Add(&TTb_PtWR);
  WJ_PtWR.Add(&QCD_PtWR);
  ZW_PtWR.Add(&WJ_PtWR);
  ZZ_PtWR.Add(&ZW_PtWR);
  WW_PtWR.Add(&ZZ_PtWR);
  S_PtWR.Add(&WW_PtWR);
  
  
  RD_PtWR.SetLineColor(1);
  RD_PtWR.SetLineWidth(5); 
  
  S_PtWR.SetLineStyle(1);
  S_PtWR.SetLineColor(2);
  //S_PtWR.SetFillStyle(0);
  //S_PtWR.SetFillColor(kRed);
  S_PtWR.SetLineWidth(5);
  
  ZJ_PtWR.SetLineStyle(2);
  ZJ_PtWR.SetLineColor(3);
  //ZJ_PtWR.SetFillStyle(4);
  //ZJ_PtWR.SetFillColor(kGreen);
  ZJ_PtWR.SetLineWidth(5);
  
  TTb_PtWR.SetLineStyle(3);
  TTb_PtWR.SetLineColor(4);
  //TTb_PtWR.SetFillStyle(5);
  //TTb_PtWR.SetFillColor(kYellow);
  TTb_PtWR.SetLineWidth(5);
  
  ZW_PtWR.SetLineStyle(4);
  ZW_PtWR.SetLineColor(5);
  //ZW_PtWR.SetFillStyle(6);
  //ZW_PtWR.SetFillColor(kBlue);
  ZW_PtWR.SetLineWidth(5);
  
  ZZ_PtWR.SetLineStyle(5);
  ZZ_PtWR.SetLineColor(6);
  ZZ_PtWR.SetLineWidth(5);
  
  WW_PtWR.SetLineStyle(6);
  WW_PtWR.SetLineColor(7);
  WW_PtWR.SetLineWidth(5);
  
  WJ_PtWR.SetLineStyle(7);
  WJ_PtWR.SetLineColor(8);
  WJ_PtWR.SetLineWidth(5);  
  
  QCD_PtWR.SetLineStyle(8);
  QCD_PtWR.SetLineColor(11);
  QCD_PtWR.SetLineWidth(5); 
  
  RD_PtWR.Draw("e1"); S_PtWR.Draw("HISTsame"); WW_PtWR.Draw("HISTsame"); ZZ_PtWR.Draw("HISTsame"); ZW_PtWR.Draw("HISTsame"); WJ_PtWR.Draw("HISTsame"); QCD_PtWR.Draw("HISTsame"); TTb_PtWR.Draw("HISTsame"); ZJ_PtWR.Draw("HISTsame"); RD_PtWR.Draw("e1same"); leg.Draw(); c.Print("OUTPUT-MUMU.ps");
  
  
 

   

  TTb_PlWR.Add(&ZJ_PlWR);
  QCD_PlWR.Add(&TTb_PlWR);
  WJ_PlWR.Add(&QCD_PlWR);
  ZW_PlWR.Add(&WJ_PlWR);
  ZZ_PlWR.Add(&ZW_PlWR);
  WW_PlWR.Add(&ZZ_PlWR);
  S_PlWR.Add(&WW_PlWR);
  
  RD_PlWR.SetLineColor(1);
  RD_PlWR.SetLineWidth(5); 
  
  S_PlWR.SetLineStyle(1);
  S_PlWR.SetLineColor(2);
  //S_PlWR.SetFillStyle(0);
  //S_PlWR.SetFillColor(kRed);
  S_PlWR.SetLineWidth(5);
  
  ZJ_PlWR.SetLineStyle(2);
  ZJ_PlWR.SetLineColor(3);
  //ZJ_PlWR.SetFillStyle(4);
  //ZJ_PlWR.SetFillColor(kGreen);
  ZJ_PlWR.SetLineWidth(5);
  
  TTb_PlWR.SetLineStyle(3);
  TTb_PlWR.SetLineColor(4);
  //TTb_PlWR.SetFillStyle(5);
  //TTb_PlWR.SetFillColor(kYellow);
  TTb_PlWR.SetLineWidth(5);
  
  ZW_PlWR.SetLineStyle(4);
  ZW_PlWR.SetLineColor(5);
  //ZW_PlWR.SetFillStyle(6);
  //ZW_PlWR.SetFillColor(kBlue);
  ZW_PlWR.SetLineWidth(5);
  
  ZZ_PlWR.SetLineStyle(5);
  ZZ_PlWR.SetLineColor(6);
  ZZ_PlWR.SetLineWidth(5);
  
  WW_PlWR.SetLineStyle(6);
  WW_PlWR.SetLineColor(7);
  WW_PlWR.SetLineWidth(5);
  
  WJ_PlWR.SetLineStyle(7);
  WJ_PlWR.SetLineColor(8);
  WJ_PlWR.SetLineWidth(5);  
  
  QCD_PlWR.SetLineStyle(8);
  QCD_PlWR.SetLineColor(11);
  QCD_PlWR.SetLineWidth(5); 
  
  RD_PlWR.Draw("e1"); S_PlWR.Draw("HISTsame"); WW_PlWR.Draw("HISTsame"); ZZ_PlWR.Draw("HISTsame"); ZW_PlWR.Draw("HISTsame"); WJ_PlWR.Draw("HISTsame"); QCD_PlWR.Draw("HISTsame"); TTb_PlWR.Draw("HISTsame"); ZJ_PlWR.Draw("HISTsame"); RD_PlWR.Draw("e1same"); leg.Draw();/* latex.DrawLatex(1.5,3.5,str); latex.DrawLatex(1.6,3.3,str1)*/;c.Print("OUTPUT-MUMU.ps");
  
  
  
  

  
  RD_EtaWR.SetMinimum(0);
  
  TTb_EtaWR.Add(&ZJ_EtaWR);
  QCD_EtaWR.Add(&TTb_EtaWR);
  WJ_EtaWR.Add(&QCD_EtaWR);
  ZW_EtaWR.Add(&WJ_EtaWR);
  ZZ_EtaWR.Add(&ZW_EtaWR);
  WW_EtaWR.Add(&ZZ_EtaWR);
  S_EtaWR.Add(&WW_EtaWR);
  
  RD_EtaWR.SetLineColor(1);
  RD_EtaWR.SetLineWidth(5); 
  
  S_EtaWR.SetLineStyle(1);
  S_EtaWR.SetLineColor(2);
  //S_EtaWR.SetFillStyle(0);
  //S_EtaWR.SetFillColor(kRed);
  S_EtaWR.SetLineWidth(5);
  
  ZJ_EtaWR.SetLineStyle(2);
  ZJ_EtaWR.SetLineColor(3);
  //ZJ_EtaWR.SetFillStyle(4);
  //ZJ_EtaWR.SetFillColor(kGreen);
  ZJ_EtaWR.SetLineWidth(5);
  
  TTb_EtaWR.SetLineStyle(3);
  TTb_EtaWR.SetLineColor(4);
  //TTb_EtaWR.SetFillStyle(5);
  //TTb_EtaWR.SetFillColor(kYellow);
  TTb_EtaWR.SetLineWidth(5);
  
  ZW_EtaWR.SetLineStyle(4);
  ZW_EtaWR.SetLineColor(5);
  //ZW_EtaWR.SetFillStyle(6);
  //ZW_EtaWR.SetFillColor(kBlue);
  ZW_EtaWR.SetLineWidth(5);
  
  ZZ_EtaWR.SetLineStyle(5);
  ZZ_EtaWR.SetLineColor(6);
  ZZ_EtaWR.SetLineWidth(5);
  
  WW_EtaWR.SetLineStyle(6);
  WW_EtaWR.SetLineColor(7);
  WW_EtaWR.SetLineWidth(5);
  
  WJ_EtaWR.SetLineStyle(7);
  WJ_EtaWR.SetLineColor(8);
  WJ_EtaWR.SetLineWidth(5);  
  
  QCD_EtaWR.SetLineStyle(8);
  QCD_EtaWR.SetLineColor(11);
  QCD_EtaWR.SetLineWidth(5); 
  
  RD_EtaWR.Draw("e1"); S_EtaWR.Draw("HISTsame"); WW_EtaWR.Draw("HISTsame"); ZZ_EtaWR.Draw("HISTsame"); ZW_EtaWR.Draw("HISTsame"); WJ_EtaWR.Draw("HISTsame"); QCD_EtaWR.Draw("HISTsame"); TTb_EtaWR.Draw("HISTsame"); ZJ_EtaWR.Draw("HISTsame"); RD_EtaWR.Draw("e1same"); leg.Draw();  c.Print("OUTPUT-MUMU.ps");
  
  //S_EtaWR.Draw(); WW_EtaWR.Draw("HISTsame"); ZZ_EtaWR.Draw("HISTsame"); ZW_EtaWR.Draw("HISTsame"); WJ_EtaWR.Draw("HISTsame"); QCD_EtaWR.Draw("HISTsame"); TTb_EtaWR.Draw("HISTsame"); ZJ_EtaWR.Draw("HISTsame"); RD_EtaWR.Draw("e1same"); leg.Draw();/* latex.DrawLatex(1.5,3.5,str); latex.DrawLatex(1.6,3.3,str1);*/ c.Print("OUTPUT-MUMU.ps");
  

  RD_PhiWR.SetMinimum(0);
  
  TTb_PhiWR.Add(&ZJ_PhiWR);
  QCD_PhiWR.Add(&TTb_PhiWR);
  WJ_PhiWR.Add(&QCD_PhiWR);
  ZW_PhiWR.Add(&WJ_PhiWR);
  ZZ_PhiWR.Add(&ZW_PhiWR);
  WW_PhiWR.Add(&ZZ_PhiWR);
  S_PhiWR.Add(&WW_PhiWR);
  
  RD_PhiWR.SetLineColor(1);
  RD_PhiWR.SetLineWidth(5); 
  
  S_PhiWR.SetLineStyle(1);
  S_PhiWR.SetLineColor(2);
  //S_PhiWR.SetFillStyle(0);
  //S_PhiWR.SetFillColor(kRed);
  S_PhiWR.SetLineWidth(5);
  
  ZJ_PhiWR.SetLineStyle(2);
  ZJ_PhiWR.SetLineColor(3);
  //ZJ_PhiWR.SetFillStyle(4);
  //ZJ_PhiWR.SetFillColor(kGreen);
  ZJ_PhiWR.SetLineWidth(5);
  
  TTb_PhiWR.SetLineStyle(3);
  TTb_PhiWR.SetLineColor(4);
  //TTb_PhiWR.SetFillStyle(5);
  //TTb_PhiWR.SetFillColor(kYellow);
  TTb_PhiWR.SetLineWidth(5);
  
  ZW_PhiWR.SetLineStyle(4);
  ZW_PhiWR.SetLineColor(5);
  //ZW_PhiWR.SetFillStyle(6);
  //ZW_PhiWR.SetFillColor(kBlue);
  ZW_PhiWR.SetLineWidth(5);
  
  ZZ_PhiWR.SetLineStyle(5);
  ZZ_PhiWR.SetLineColor(6);
  ZZ_PhiWR.SetLineWidth(5);
  
  WW_PhiWR.SetLineStyle(6);
  WW_PhiWR.SetLineColor(7);
  WW_PhiWR.SetLineWidth(5);
  
  WJ_PhiWR.SetLineStyle(7);
  WJ_PhiWR.SetLineColor(8);
  WJ_PhiWR.SetLineWidth(5);  
  
  QCD_PhiWR.SetLineStyle(8);
  QCD_PhiWR.SetLineColor(11);
  QCD_PhiWR.SetLineWidth(5); 
  
  RD_PhiWR.Draw("e1"); S_PhiWR.Draw("HISTsame"); WW_PhiWR.Draw("HISTsame"); ZZ_PhiWR.Draw("HISTsame"); ZW_PhiWR.Draw("HISTsame"); WJ_PhiWR.Draw("HISTsame"); QCD_PhiWR.Draw("HISTsame"); TTb_PhiWR.Draw("HISTsame"); ZJ_PhiWR.Draw("HISTsame"); RD_PhiWR.Draw("e1same"); leg.Draw();/* latex.DrawLatex(1.5,3.5,str); latex.DrawLatex(1.6,3.3,str1);*/ c.Print("OUTPUT-MUMU.ps");
  
  
  
  
  
  TTb_hNuR0.Add(&ZJ_hNuR0);
  QCD_hNuR0.Add(&TTb_hNuR0);
  WJ_hNuR0.Add(&QCD_hNuR0);
  ZW_hNuR0.Add(&WJ_hNuR0);
  ZZ_hNuR0.Add(&ZW_hNuR0);
  WW_hNuR0.Add(&ZZ_hNuR0);
  TW_hNuR0.Add(&WW_hNuR0);
  S_hNuR0.Add(&TW_hNuR0);
  
  RD_hNuR0.SetLineColor(1);
  RD_hNuR0.SetLineWidth(5); 
  
  S_hNuR0.SetLineStyle(1);
  S_hNuR0.SetLineColor(2);
  //S_hNuR0.SetFillStyle(0);
  //S_hNuR0.SetFillColor(kRed);
  S_hNuR0.SetLineWidth(5);
  
  ZJ_hNuR0.SetLineStyle(2);
  ZJ_hNuR0.SetLineColor(3);
  //ZJ_hNuR0.SetFillStyle(4);
  //ZJ_hNuR0.SetFillColor(kGreen);
  ZJ_hNuR0.SetLineWidth(5);
  
  TTb_hNuR0.SetLineStyle(3);
  TTb_hNuR0.SetLineColor(4);
  //TTb_hNuR0.SetFillStyle(5);
  //TTb_hNuR0.SetFillColor(kYellow);
  TTb_hNuR0.SetLineWidth(5);
  
  ZW_hNuR0.SetLineStyle(4);
  ZW_hNuR0.SetLineColor(5);
  //ZW_hNuR0.SetFillStyle(6);
  //ZW_hNuR0.SetFillColor(kBlue);
  ZW_hNuR0.SetLineWidth(5);
  
  ZZ_hNuR0.SetLineStyle(5);
  ZZ_hNuR0.SetLineColor(6);
  ZZ_hNuR0.SetLineWidth(5);
  
  WW_hNuR0.SetLineStyle(6);
  WW_hNuR0.SetLineColor(7);
  WW_hNuR0.SetLineWidth(5);
  
  WJ_hNuR0.SetLineStyle(7);
  WJ_hNuR0.SetLineColor(8);
  WJ_hNuR0.SetLineWidth(5);  
  
  QCD_hNuR0.SetLineStyle(8);
  QCD_hNuR0.SetLineColor(11);
  QCD_hNuR0.SetLineWidth(5); 
 
  TW_hNuR0.SetLineStyle(9);
  TW_hNuR0.SetLineColor(12);
  TW_hNuR0.SetLineWidth(5);
  
  RD_hNuR0.Draw("e1"); S_hNuR0.Draw("HISTsame"); TW_hNuR0.Draw("HISTsame"); WW_hNuR0.Draw("HISTsame"); ZZ_hNuR0.Draw("HISTsame"); ZW_hNuR0.Draw("HISTsame"); WJ_hNuR0.Draw("HISTsame"); QCD_hNuR0.Draw("HISTsame"); TTb_hNuR0.Draw("HISTsame"); ZJ_hNuR0.Draw("HISTsame"); RD_hNuR0.Draw("e1same"); leg.Draw(); c.Print("OUTPUT-MUMU.ps");
  
 // S_hNuR0.Draw(); TW_hNuR0.Draw("HISTsame"); WW_hNuR0.Draw("HISTsame"); ZZ_hNuR0.Draw("HISTsame"); ZW_hNuR0.Draw("HISTsame"); WJ_hNuR0.Draw("HISTsame"); QCD_hNuR0.Draw("HISTsame"); TTb_hNuR0.Draw("HISTsame"); ZJ_hNuR0.Draw("HISTsame"); RD_hNuR0.Draw("e1same"); leg.Draw(); c.Print("OUTPUT-MUMU.ps");
 
  label_Lumi(0.38, 0.85, RD_Lum);
  label_Prelim(0.38, 0.9);
 
//  c.Print("M_Nu0-MUMU.pdf");
//  c.Print("M_Nu0-MUMU.png");
//  c.Print("M_Nu0-MUMU.cxx");

   
  TTb_hMjj.Add(&ZJ_hMjj);
  QCD_hMjj.Add(&TTb_hMjj);
  WJ_hMjj.Add(&QCD_hMjj);
  ZW_hMjj.Add(&WJ_hMjj);
  ZZ_hMjj.Add(&ZW_hMjj);
  WW_hMjj.Add(&ZZ_hMjj);
  S_hMjj.Add(&WW_hMjj); 
    
  RD_hMjj.SetLineColor(1);
  RD_hMjj.SetLineWidth(5); 
  
  S_hMjj.SetLineStyle(1);
  S_hMjj.SetLineColor(2);
  //S_hMjj.SetFillStyle(0);
  //S_hMjj.SetFillColor(kRed);
  S_hMjj.SetLineWidth(5);
  
  ZJ_hMjj.SetLineStyle(2);
  ZJ_hMjj.SetLineColor(3);
  //ZJ_hMjj.SetFillStyle(4);
  //ZJ_hMjj.SetFillColor(kGreen);
  ZJ_hMjj.SetLineWidth(5);
  
  TTb_hMjj.SetLineStyle(3);
  TTb_hMjj.SetLineColor(4);
  //TTb_hMjj.SetFillStyle(5);
  //TTb_hMjj.SetFillColor(kYellow);
  TTb_hMjj.SetLineWidth(5);
  
  ZW_hMjj.SetLineStyle(4);
  ZW_hMjj.SetLineColor(5);
  //ZW_hMjj.SetFillStyle(6);
  //ZW_hMjj.SetFillColor(kBlue);
  ZW_hMjj.SetLineWidth(5);
  
  ZZ_hMjj.SetLineStyle(5);
  ZZ_hMjj.SetLineColor(6);
  ZZ_hMjj.SetLineWidth(5);
  
  WW_hMjj.SetLineStyle(6);
  WW_hMjj.SetLineColor(7);
  WW_hMjj.SetLineWidth(5);
  
  WJ_hMjj.SetLineStyle(7);
  WJ_hMjj.SetLineColor(8);
  WJ_hMjj.SetLineWidth(5);  
  
  QCD_hMjj.SetLineStyle(8);
  QCD_hMjj.SetLineColor(11);
  QCD_hMjj.SetLineWidth(5); 
  
   RD_hMjj.Draw("e1"); S_hMjj.Draw("HISTsame"); WW_hMjj.Draw("HISTsame"); ZZ_hMjj.Draw("HISTsame"); ZW_hMjj.Draw("HISTsame"); WJ_hMjj.Draw("HISTsame"); QCD_hMjj.Draw("HISTsame"); TTb_hMjj.Draw("HISTsame"); ZJ_hMjj.Draw("HISTsame"); RD_hMjj.Draw("e1same"); leg.Draw(); c.Print("OUTPUT-MUMU.ps");
  
  //  S_hMjj.Draw(""); WW_hMjj.Draw("HISTsame"); ZZ_hMjj.Draw("HISTsame"); ZW_hMjj.Draw("HISTsame"); WJ_hMjj.Draw("HISTsame"); QCD_hMjj.Draw("HISTsame"); TTb_hMjj.Draw("HISTsame"); ZJ_hMjj.Draw("HISTsame"); RD_hMjj.Draw("e1same"); leg.Draw(); latex.DrawLatex(25,12,str); latex.DrawLatex(25,11.5,str1);c.Print("OUTPUT-MUMU.ps");
  
  
  
 


  

  
  WW_hMmets.Add(&WJ_hMmets);
  TW_hMmets.Add(&WW_hMmets);
//  QCD_hMmets.Add(&TW_hMmets);
  ZZ_hMmets.Add(&TW_hMmets);
  ZW_hMmets.Add(&ZZ_hMmets);
  TTb_hMmets.Add(&ZW_hMmets);
  ZJ_hMmets.Add(&TTb_hMmets);
  S_hMmets.Add(&ZJ_hMmets);

  
  RD_hMmets.SetLineColor(1);
  RD_hMmets.SetLineWidth(5); 
  
  S_hMmets.SetLineStyle(1);
  S_hMmets.SetLineColor(1);
  //S_hMmets.SetFillStyle(0);
  S_hMmets.SetFillColor(2);
  S_hMmets.SetLineWidth(2);
  
  ZJ_hMmets.SetLineStyle(1);
  ZJ_hMmets.SetLineColor(1);
  //ZJ_hMmets.SetFillStyle(4);
  ZJ_hMmets.SetFillColor(3);
  ZJ_hMmets.SetLineWidth(2);
  
  TTb_hMmets.SetLineStyle(1);
  TTb_hMmets.SetLineColor(1);
  //TTb_hMmets.SetFillStyle(5);
  TTb_hMmets.SetFillColor(4);
  TTb_hMmets.SetLineWidth(2);
  
  ZW_hMmets.SetLineStyle(1);
  ZW_hMmets.SetLineColor(1);
  //ZW_hMmets.SetFillStyle(6);
  ZW_hMmets.SetFillColor(5);
  ZW_hMmets.SetLineWidth(2);
  
  ZZ_hMmets.SetLineStyle(1);
  ZZ_hMmets.SetLineColor(1);
  //ZW_hMmets.SetFillStyle(6);
  ZZ_hMmets.SetFillColor(6);
  ZZ_hMmets.SetLineWidth(2);
  
  WW_hMmets.SetLineStyle(1);
  WW_hMmets.SetLineColor(1);
  //WW_hMmets.SetFillStyle(6);
  WW_hMmets.SetFillColor(7);
  WW_hMmets.SetLineWidth(2);
  

  WJ_hMmets.SetLineStyle(1);
  WJ_hMmets.SetLineColor(1);
  WJ_hMmets.SetFillColor(1);
  WJ_hMmets.SetLineWidth(2);  
/*  
  QCD_hMmets.SetLineStyle(1);
  QCD_hMmets.SetLineColor(1);
  QCD_hMmets.SetFillColor(11);
  QCD_hMmets.SetLineWidth(2); 
 */ 
  TW_hMmets.SetLineStyle(1);
  TW_hMmets.SetLineColor(1);
  TW_hMmets.SetFillColor(12);
  TW_hMmets.SetLineWidth(2);
  
  
 RD_hMmets.Draw("e"); S_hMmets.Draw("HISTsame"); ZJ_hMmets.Draw("HISTsame"); TTb_hMmets.Draw("HISTsame");  ZW_hMmets.Draw("HISTsame"); ZZ_hMmets.Draw("HISTsame"); /*QCD_hMmets.Draw("HISTsame");*/ TW_hMmets.Draw("HISTsame"); WW_hMmets.Draw("HISTsame");  WJ_hMmets.Draw("HISTsame");   RD_hMmets.Draw("e1same"); 

 
 RD_hMmets.GetYaxis()->SetRangeUser(0.01, 1000);
// RD_hMmets.GetXaxis()->SetLimits(-10, 210);
 c.Modified();

   leg.Draw();
  label_Lumi(0.38, 0.85, RD_Lum);
  label_Prelim(0.38, 0.9);
   
  c.Print("MET-MUMU.pdf");
  c.Print("MET-MUMU.png");
  c.Print("MET-MUMU.cxx");

  
  TTb_hMmets1.Add(&ZJ_hMmets1);
  WJ_hMmets1.Add(&TTb_hMmets1);
  ZW_hMmets1.Add(&WJ_hMmets1);
  ZZ_hMmets1.Add(&ZW_hMmets1);
  WW_hMmets1.Add(&ZZ_hMmets1);
  S_hMmets1.Add(&WW_hMmets1); 
  
  RD_hMmets1.SetLineColor(1);
  RD_hMmets1.SetLineWidth(5); 
  
  S_hMmets1.SetLineStyle(1);
  S_hMmets1.SetLineColor(2);
  //S_hMmets1.SetFillStyle(0);
  //S_hMmets1.SetFillColor(kRed);
  S_hMmets1.SetLineWidth(5);
  
  ZJ_hMmets1.SetLineStyle(2);
  ZJ_hMmets1.SetLineColor(3);
  //ZJ_hMmets1.SetFillStyle(4);
  //ZJ_hMmets1.SetFillColor(kGreen);
  ZJ_hMmets1.SetLineWidth(5);
  
  TTb_hMmets1.SetLineStyle(3);
  TTb_hMmets1.SetLineColor(4);
  //TTb_hMmets1.SetFillStyle(5);
  //TTb_hMmets1.SetFillColor(kYellow);
  TTb_hMmets1.SetLineWidth(5);
  
  ZW_hMmets1.SetLineStyle(4);
  ZW_hMmets1.SetLineColor(5);
  //ZW_hMmets1.SetFillStyle(6);
  //ZW_hMmets1.SetFillColor(kBlue);
  ZW_hMmets1.SetLineWidth(5);
  
  ZZ_hMmets1.SetLineStyle(5);
  ZZ_hMmets1.SetLineColor(6);
  ZZ_hMmets1.SetLineWidth(5);
  
  WW_hMmets1.SetLineStyle(6);
  WW_hMmets1.SetLineColor(7);
  WW_hMmets1.SetLineWidth(5);
 
  WJ_hMmets1.SetLineStyle(7);
  WJ_hMmets1.SetLineColor(8);
  WJ_hMmets1.SetLineWidth(5);  
  
  RD_hMmets1.Draw("e1"); S_hMmets1.Draw("HISTsame"); WW_hMmets1.Draw("HISTsame"); ZZ_hMmets1.Draw("HISTsame"); ZW_hMmets1.Draw("HISTsame"); WJ_hMmets1.Draw("HISTsame"); TTb_hMmets1.Draw("HISTsame"); ZJ_hMmets1.Draw("HISTsame"); RD_hMmets1.Draw("e1same"); leg.Draw();  c.Print("OUTPUT-MUMU.ps");
  
 //  S_hMmets1.Draw(""); WW_hMmets1.Draw("HISTsame"); ZZ_hMmets1.Draw("HISTsame"); ZW_hMmets1.Draw("HISTsame"); WJ_hMmets1.Draw("HISTsame"); TTb_hMmets1.Draw("HISTsame"); ZJ_hMmets1.Draw("HISTsame"); RD_hMmets1.Draw("e1same"); leg.Draw(); latex.DrawLatex(1.5,3.5,str); latex.DrawLatex(1.6,3.3,str1); c.Print("OUTPUT-MUMU.ps");
  

  
  TTb_hbDiscr_j1.Add(&ZJ_hbDiscr_j1);
  WJ_hbDiscr_j1.Add(&TTb_hbDiscr_j1);
  ZW_hbDiscr_j1.Add(&WJ_hbDiscr_j1);
  ZZ_hbDiscr_j1.Add(&ZW_hbDiscr_j1);
  WW_hbDiscr_j1.Add(&ZZ_hbDiscr_j1);
  S_hbDiscr_j1.Add(&WW_hbDiscr_j1); 
 
  RD_hbDiscr_j1.SetLineColor(1);
  RD_hbDiscr_j1.SetLineWidth(5); 
  
  S_hbDiscr_j1.SetLineStyle(1);
  S_hbDiscr_j1.SetLineColor(2);
  //S_hbDiscr_j1.SetFillStyle(0);
  //S_hbDiscr_j1.SetFillColor(kRed);
  S_hbDiscr_j1.SetLineWidth(5);
  
  ZJ_hbDiscr_j1.SetLineStyle(2);
  ZJ_hbDiscr_j1.SetLineColor(3);
  //ZJ_hbDiscr_j1.SetFillStyle(4);
  //ZJ_hbDiscr_j1.SetFillColor(kGreen);
  ZJ_hbDiscr_j1.SetLineWidth(5);
  
  TTb_hbDiscr_j1.SetLineStyle(3);
  TTb_hbDiscr_j1.SetLineColor(4);
  //TTb_hbDiscr_j1.SetFillStyle(5);
  //TTb_hbDiscr_j1.SetFillColor(kYellow);
  TTb_hbDiscr_j1.SetLineWidth(5);
  
  ZW_hbDiscr_j1.SetLineStyle(4);
  ZW_hbDiscr_j1.SetLineColor(5);
  //ZW_hbDiscr_j1.SetFillStyle(6);
  //ZW_hbDiscr_j1.SetFillColor(kBlue);
  ZW_hbDiscr_j1.SetLineWidth(5);
  
  ZZ_hbDiscr_j1.SetLineStyle(5);
  ZZ_hbDiscr_j1.SetLineColor(6);
  ZZ_hbDiscr_j1.SetLineWidth(5);
  
  WW_hbDiscr_j1.SetLineStyle(6);
  WW_hbDiscr_j1.SetLineColor(7);
  WW_hbDiscr_j1.SetLineWidth(5);
 
  WJ_hbDiscr_j1.SetLineStyle(7);
  WJ_hbDiscr_j1.SetLineColor(8);
  WJ_hbDiscr_j1.SetLineWidth(5);  
  
  
  
 // RD_hbDiscr_j1.Draw("e1"); S_hbDiscr_j1.Draw("HISTsame"); WW_hbDiscr_j1.Draw("HISTsame"); ZZ_hbDiscr_j1.Draw("HISTsame"); ZW_hbDiscr_j1.Draw("HISTsame"); WJ_hbDiscr_j1.Draw("HISTsame"); TTb_hbDiscr_j1.Draw("HISTsame"); ZJ_hbDiscr_j1.Draw("HISTsame"); RD_hbDiscr_j1.Draw("e1same"); leg.Draw();/* latex.DrawLatex(1.5,3.5,str); latex.DrawLatex(1.6,3.3,str1);*/ c.Print("OUTPUT-MUMU.ps");
  
   S_hbDiscr_j1.Draw(""); WW_hbDiscr_j1.Draw("HISTsame"); ZZ_hbDiscr_j1.Draw("HISTsame"); ZW_hbDiscr_j1.Draw("HISTsame"); WJ_hbDiscr_j1.Draw("HISTsame"); TTb_hbDiscr_j1.Draw("HISTsame"); ZJ_hbDiscr_j1.Draw("HISTsame"); RD_hbDiscr_j1.Draw("e1same"); leg.Draw(); /*latex.DrawLatex(1.5,3.5,str); latex.DrawLatex(1.6,3.3,str1);*/ c.Print("OUTPUT-MUMU.ps");
  
 
  
  TTb_hbDiscr_j2.Add(&ZJ_hbDiscr_j2);
  WJ_hbDiscr_j2.Add(&TTb_hbDiscr_j2);
  ZW_hbDiscr_j2.Add(&WJ_hbDiscr_j2);
  ZZ_hbDiscr_j2.Add(&ZW_hbDiscr_j2);
  WW_hbDiscr_j2.Add(&ZZ_hbDiscr_j2);
  S_hbDiscr_j2.Add(&WW_hbDiscr_j2); 
  
  RD_hbDiscr_j2.SetLineColor(1);
  RD_hbDiscr_j2.SetLineWidth(5); 
  
  S_hbDiscr_j2.SetLineStyle(1);
  S_hbDiscr_j2.SetLineColor(2);
  //S_hbDiscr_j2.SetFillStyle(0);
  //S_hbDiscr_j2.SetFillColor(kRed);
  S_hbDiscr_j2.SetLineWidth(5);
  
  ZJ_hbDiscr_j2.SetLineStyle(2);
  ZJ_hbDiscr_j2.SetLineColor(3);
  //ZJ_hbDiscr_j2.SetFillStyle(4);
  //ZJ_hbDiscr_j2.SetFillColor(kGreen);
  ZJ_hbDiscr_j2.SetLineWidth(5);
  
  TTb_hbDiscr_j2.SetLineStyle(3);
  TTb_hbDiscr_j2.SetLineColor(4);
  //TTb_hbDiscr_j2.SetFillStyle(5);
  //TTb_hbDiscr_j2.SetFillColor(kYellow);
  TTb_hbDiscr_j2.SetLineWidth(5);
  
  ZW_hbDiscr_j2.SetLineStyle(4);
  ZW_hbDiscr_j2.SetLineColor(5);
  //ZW_hbDiscr_j2.SetFillStyle(6);
  //ZW_hbDiscr_j2.SetFillColor(kBlue);
  ZW_hbDiscr_j2.SetLineWidth(5);
  
  ZZ_hbDiscr_j2.SetLineStyle(5);
  ZZ_hbDiscr_j2.SetLineColor(6);
  ZZ_hbDiscr_j2.SetLineWidth(5);
  
  WW_hbDiscr_j2.SetLineStyle(6);
  WW_hbDiscr_j2.SetLineColor(7);
  WW_hbDiscr_j2.SetLineWidth(5);
 
  WJ_hbDiscr_j2.SetLineStyle(7);
  WJ_hbDiscr_j2.SetLineColor(8);
  WJ_hbDiscr_j2.SetLineWidth(5);  
  
 // RD_hbDiscr_j2.Draw("e1"); S_hbDiscr_j2.Draw("HISTsame"); WW_hbDiscr_j2.Draw("HISTsame"); ZZ_hbDiscr_j2.Draw("HISTsame"); ZW_hbDiscr_j2.Draw("HISTsame"); WJ_hbDiscr_j2.Draw("HISTsame"); TTb_hbDiscr_j2.Draw("HISTsame"); ZJ_hbDiscr_j2.Draw("HISTsame"); RD_hbDiscr_j2.Draw("e1same"); leg.Draw();/*latex.DrawLatex(1.5,3.5,str); latex.DrawLatex(1.6,3.3,str1);*/ c.Print("OUTPUT-MUMU.ps");
  
   S_hbDiscr_j2.Draw(""); WW_hbDiscr_j2.Draw("HISTsame"); ZZ_hbDiscr_j2.Draw("HISTsame"); ZW_hbDiscr_j2.Draw("HISTsame"); WJ_hbDiscr_j2.Draw("HISTsame"); TTb_hbDiscr_j2.Draw("HISTsame"); ZJ_hbDiscr_j2.Draw("HISTsame"); RD_hbDiscr_j2.Draw("e1same");/* leg.Draw();latex.DrawLatex(1.5,3.5,str); latex.DrawLatex(1.6,3.3,str1);*/ c.Print("OUTPUT-MUMU.ps");
  
  
  
  
  WW_hbNum.Add(&WJ_hbNum);  
  TW_hbNum.Add(&WW_hbNum);
//  QCD_hbNum.Add(&TW_hbNum);
  ZZ_hbNum.Add(&TW_hbNum);
  ZW_hbNum.Add(&ZZ_hbNum);
  TTb_hbNum.Add(&ZW_hbNum);
  ZJ_hbNum.Add(&TTb_hbNum);
  S_hbNum.Add(&ZJ_hbNum);

  
  RD_hbNum.SetLineColor(1);
  RD_hbNum.SetLineWidth(5); 
  
  S_hbNum.SetLineStyle(1);
  S_hbNum.SetLineColor(1);
  //S_hbNum.SetFillStyle(0);
  S_hbNum.SetFillColor(2);
  S_hbNum.SetLineWidth(2);
  
  ZJ_hbNum.SetLineStyle(1);
  ZJ_hbNum.SetLineColor(1);
  //ZJ_hbNum.SetFillStyle(4);
  ZJ_hbNum.SetFillColor(3);
  ZJ_hbNum.SetLineWidth(2);
  
  TTb_hbNum.SetLineStyle(1);
  TTb_hbNum.SetLineColor(1);
  //TTb_hbNum.SetFillStyle(5);
  TTb_hbNum.SetFillColor(4);
  TTb_hbNum.SetLineWidth(2);
  
  ZW_hbNum.SetLineStyle(1);
  ZW_hbNum.SetLineColor(1);
  //ZW_hbNum.SetFillStyle(6);
  ZW_hbNum.SetFillColor(5);
  ZW_hbNum.SetLineWidth(2);
  
  ZZ_hbNum.SetLineStyle(1);
  ZZ_hbNum.SetLineColor(1);
  //ZW_hbNum.SetFillStyle(6);
  ZZ_hbNum.SetFillColor(6);
  ZZ_hbNum.SetLineWidth(2);
  
  WW_hbNum.SetLineStyle(1);
  WW_hbNum.SetLineColor(1);
  //WW_hbNum.SetFillStyle(6);
  WW_hbNum.SetFillColor(7);
  WW_hbNum.SetLineWidth(2);
  

  WJ_hbNum.SetLineStyle(1);
  WJ_hbNum.SetLineColor(1);
  WJ_hbNum.SetFillColor(1);
  WJ_hbNum.SetLineWidth(2);  
/*  
  QCD_hbNum.SetLineStyle(1);
  QCD_hbNum.SetLineColor(1);
  QCD_hbNum.SetFillColor(11);
  QCD_hbNum.SetLineWidth(2); 
*/  
  TW_hbNum.SetLineStyle(1);
  TW_hbNum.SetLineColor(1);
  TW_hbNum.SetFillColor(12);
  TW_hbNum.SetLineWidth(2);
  
 RD_hbNum.Draw("e"); S_hbNum.Draw("HISTsame"); ZJ_hbNum.Draw("HISTsame"); TTb_hbNum.Draw("HISTsame");  ZW_hbNum.Draw("HISTsame"); ZZ_hbNum.Draw("HISTsame");/* QCD_hbNum.Draw("HISTsame");*/ TW_hbNum.Draw("HISTsame"); WW_hbNum.Draw("HISTsame");  WJ_hbNum.Draw("HISTsame");   RD_hbNum.Draw("e1same"); 
 
 RD_hbNum.GetYaxis()->SetRangeUser(0.01, 10000);
// RD_hbNum.GetXaxis()->SetLimits(-0.2, 3.2);
 c.Modified();
    leg.Draw();
  label_Lumi(0.38, 0.85, RD_Lum);
  label_Prelim(0.38, 0.9);
  
  c.Print("Num_b-MUMU.pdf");
  c.Print("Num_b-MUMU.png");
  c.Print("Num_b-MUMU.cxx");

  
 
 // RD_hQNum.SetMaximum(5)
 
  WW_hQNum.Add(&WJ_hQNum);  
  TW_hQNum.Add(&WW_hQNum);
  QCD_hQNum.Add(&TW_hQNum);
  ZZ_hQNum.Add(&QCD_hQNum);
  ZW_hQNum.Add(&ZZ_hQNum);
  TTb_hQNum.Add(&ZW_hQNum);
  ZJ_hQNum.Add(&TTb_hQNum);
  S_hQNum.Add(&ZJ_hQNum);

  
  RD_hQNum.SetLineColor(1);
  RD_hQNum.SetLineWidth(5); 
  
  S_hQNum.SetLineStyle(1);
  S_hQNum.SetLineColor(1);
  //S_hQNum.SetFillStyle(0);
  S_hQNum.SetFillColor(2);
  S_hQNum.SetLineWidth(2);
  
  ZJ_hQNum.SetLineStyle(1);
  ZJ_hQNum.SetLineColor(1);
  //ZJ_hQNum.SetFillStyle(4);
  ZJ_hQNum.SetFillColor(3);
  ZJ_hQNum.SetLineWidth(2);
  
  TTb_hQNum.SetLineStyle(1);
  TTb_hQNum.SetLineColor(1);
  //TTb_hQNum.SetFillStyle(5);
  TTb_hQNum.SetFillColor(4);
  TTb_hQNum.SetLineWidth(2);
  
  ZW_hQNum.SetLineStyle(1);
  ZW_hQNum.SetLineColor(1);
  //ZW_hQNum.SetFillStyle(6);
  ZW_hQNum.SetFillColor(5);
  ZW_hQNum.SetLineWidth(2);
  
  ZZ_hQNum.SetLineStyle(1);
  ZZ_hQNum.SetLineColor(1);
  //ZW_hQNum.SetFillStyle(6);
  ZZ_hQNum.SetFillColor(6);
  ZZ_hQNum.SetLineWidth(2);
  
  WW_hQNum.SetLineStyle(1);
  WW_hQNum.SetLineColor(1);
  //WW_hQNum.SetFillStyle(6);
  WW_hQNum.SetFillColor(7);
  WW_hQNum.SetLineWidth(2);
  

  WJ_hQNum.SetLineStyle(1);
  WJ_hQNum.SetLineColor(1);
  WJ_hQNum.SetFillColor(1);
  WJ_hQNum.SetLineWidth(2);  
  
  QCD_hQNum.SetLineStyle(1);
  QCD_hQNum.SetLineColor(1);
  QCD_hQNum.SetFillColor(11);
  QCD_hQNum.SetLineWidth(2); 
  
  TW_hQNum.SetLineStyle(1);
  TW_hQNum.SetLineColor(1);
  TW_hQNum.SetFillColor(12);
  TW_hQNum.SetLineWidth(2);
  
 RD_hQNum.Draw("e"); S_hQNum.Draw("HISTsame"); ZJ_hQNum.Draw("HISTsame"); TTb_hQNum.Draw("HISTsame");  ZW_hQNum.Draw("HISTsame"); ZZ_hQNum.Draw("HISTsame"); QCD_hQNum.Draw("HISTsame"); TW_hQNum.Draw("HISTsame"); WW_hQNum.Draw("HISTsame");  WJ_hQNum.Draw("HISTsame");   RD_hQNum.Draw("e1same"); 
 
 RD_hQNum.GetYaxis()->SetRangeUser(0.01, 10000);
 //RD_hQNum.GetXaxis()->SetLimits(-1.1, 1.1);
 c.Modified();
 
    leg.Draw(); 
  label_Lumi(0.38, 0.85, RD_Lum);
  label_Prelim(0.38, 0.9);
  
  
  c.Print("OUTPUT-MUMU.ps");
  c.Print("Num_q-MUMU.pdf");
  c.Print("Num_q-MUMU.png");
  c.Print("Num_q-MUMU.cxx");
  
  TTb_hElMult.Add(&ZJ_hElMult);
  WJ_hElMult.Add(&TTb_hElMult);
  ZW_hElMult.Add(&WJ_hElMult);
  ZZ_hElMult.Add(&ZW_hElMult);
  WW_hElMult.Add(&ZZ_hElMult);
  S_hElMult.Add(&WW_hElMult);
  
  RD_hElMult.SetLineColor(1);
  RD_hElMult.SetLineWidth(5); 
  
  S_hElMult.SetLineStyle(1);
  S_hElMult.SetLineColor(2);
  //S_hElMult.SetFillStyle(0);
  //S_hElMult.SetFillColor(kRed);
  S_hElMult.SetLineWidth(5);
  
  ZJ_hElMult.SetLineStyle(2);
  ZJ_hElMult.SetLineColor(3);
  //ZJ_hElMult.SetFillStyle(4);
  //ZJ_hElMult.SetFillColor(kGreen);
  ZJ_hElMult.SetLineWidth(5);
  
  TTb_hElMult.SetLineStyle(3);
  TTb_hElMult.SetLineColor(4);
  //TTb_hElMult.SetFillStyle(5);
  //TTb_hElMult.SetFillColor(kYellow);
  TTb_hElMult.SetLineWidth(5);
  
  ZW_hElMult.SetLineStyle(4);
  ZW_hElMult.SetLineColor(5);
  //ZW_hElMult.SetFillStyle(6);
  //ZW_hElMult.SetFillColor(kBlue);
  ZW_hElMult.SetLineWidth(5);
 
  ZZ_hElMult.SetLineStyle(5);
  ZZ_hElMult.SetLineColor(6);
  ZZ_hElMult.SetLineWidth(5);
  
  WW_hElMult.SetLineStyle(6);
  WW_hElMult.SetLineColor(7);
  WW_hElMult.SetLineWidth(5);
 
  WJ_hElMult.SetLineStyle(7);
  WJ_hElMult.SetLineColor(8);
  WJ_hElMult.SetLineWidth(5);  
  
  RD_hElMult.Draw("e1"); S_hElMult.Draw("HISTsame"); WW_hElMult.Draw("HISTsame"); ZZ_hElMult.Draw("HISTsame"); ZW_hElMult.Draw("HISTsame"); WJ_hElMult.Draw("HISTsame"); TTb_hElMult.Draw("HISTsame"); ZJ_hElMult.Draw("HISTsame"); RD_hElMult.Draw("e1same"); leg.Draw(); c.Print("OUTPUT-MUMU.ps");
  
 //  S_hElMult.Draw(""); WW_hElMult.Draw("HISTsame"); ZZ_hElMult.Draw("HISTsame"); ZW_hElMult.Draw("HISTsame"); WJ_hElMult.Draw("HISTsame"); TTb_hElMult.Draw("HISTsame"); ZJ_hElMult.Draw("HISTsame"); RD_hElMult.Draw("e1same"); leg.Draw(); c.Print("OUTPUT-MUMU.ps");
  
	
  TTb_hJetMult.Add(&ZJ_hJetMult);
  WJ_hJetMult.Add(&TTb_hJetMult);
  ZW_hJetMult.Add(&WJ_hJetMult);
  ZZ_hJetMult.Add(&ZW_hJetMult);
  WW_hJetMult.Add(&ZZ_hJetMult);
  S_hJetMult.Add(&WW_hJetMult); 
  
  RD_hJetMult.SetLineColor(1);
  RD_hJetMult.SetLineWidth(5); 
  
  S_hJetMult.SetLineStyle(1);
  S_hJetMult.SetLineColor(2);
  //S_hJetMult.SetFillStyle(0);
  //S_hJetMult.SetFillColor(kRed);
  S_hJetMult.SetLineWidth(5);
  
  ZJ_hJetMult.SetLineStyle(2);
  ZJ_hJetMult.SetLineColor(3);
  //ZJ_hJetMult.SetFillStyle(4);
  //ZJ_hJetMult.SetFillColor(kGreen);
  ZJ_hJetMult.SetLineWidth(5);
  
  TTb_hJetMult.SetLineStyle(3);
  TTb_hJetMult.SetLineColor(4);
  //TTb_hJetMult.SetFillStyle(5);
  //TTb_hJetMult.SetFillColor(kYellow);
  TTb_hJetMult.SetLineWidth(5);
  
  ZW_hJetMult.SetLineStyle(4);
  ZW_hJetMult.SetLineColor(5);
  //ZW_hJetMult.SetFillStyle(6);
  //ZW_hJetMult.SetFillColor(kBlue);
  ZW_hJetMult.SetLineWidth(5);
 
  ZZ_hJetMult.SetLineStyle(5);
  ZZ_hJetMult.SetLineColor(6);
  ZZ_hJetMult.SetLineWidth(5);
  
  WW_hJetMult.SetLineStyle(6);
  WW_hJetMult.SetLineColor(7);
  WW_hJetMult.SetLineWidth(5);
 
  WJ_hJetMult.SetLineStyle(7);
  WJ_hJetMult.SetLineColor(8);
  WJ_hJetMult.SetLineWidth(5);  
  
  RD_hJetMult.Draw("e1"); S_hJetMult.Draw("HISTsame"); WW_hJetMult.Draw("HISTsame"); ZZ_hJetMult.Draw("HISTsame"); ZW_hJetMult.Draw("HISTsame"); WJ_hJetMult.Draw("HISTsame"); TTb_hJetMult.Draw("HISTsame"); ZJ_hJetMult.Draw("HISTsame"); RD_hJetMult.Draw("e1same"); leg.Draw(); c.Print("OUTPUT-MUMU.ps");
  
 //  S_hJetMult.Draw(""); WW_hJetMult.Draw("HISTsame"); ZZ_hJetMult.Draw("HISTsame"); ZW_hJetMult.Draw("HISTsame"); WJ_hJetMult.Draw("HISTsame"); TTb_hJetMult.Draw("HISTsame"); ZJ_hJetMult.Draw("HISTsame"); RD_hJetMult.Draw("e1same"); leg.Draw(); c.Print("OUTPUT-MUMU.ps");
  
 
 
  TTb_hElRes.Add(&ZJ_hElRes);
  WJ_hElRes.Add(&TTb_hElRes);
  ZW_hElRes.Add(&WJ_hElRes);
  ZZ_hElRes.Add(&ZW_hElRes);
  WW_hElRes.Add(&ZZ_hElRes);
  S_hElRes.Add(&WW_hElRes); 	 
  
  S_hElRes.SetLineStyle(1);
  S_hElRes.SetLineColor(2);
  //S_hElRes.SetFillStyle(0);
  //S_hElRes.SetFillColor(kRed);
  S_hElRes.SetLineWidth(5);
  
  ZJ_hElRes.SetLineStyle(2);
  ZJ_hElRes.SetLineColor(3);
  //ZJ_hElRes.SetFillStyle(4);
  //ZJ_hElRes.SetFillColor(kGreen);
  ZJ_hElRes.SetLineWidth(5);
  
  TTb_hElRes.SetLineStyle(3);
  TTb_hElRes.SetLineColor(4);
  //TTb_hElRes.SetFillStyle(5);
  //TTb_hElRes.SetFillColor(kYellow);
  TTb_hElRes.SetLineWidth(5);
  
  ZW_hElRes.SetLineStyle(4);
  ZW_hElRes.SetLineColor(5);
  //ZW_hElRes.SetFillStyle(6);
  //ZW_hElRes.SetFillColor(kBlue);
  ZW_hElRes.SetLineWidth(5);
   
  ZZ_hElRes.SetLineStyle(5);
  ZZ_hElRes.SetLineColor(6);
  ZZ_hElRes.SetLineWidth(5);
  
  WW_hElRes.SetLineStyle(6);
  WW_hElRes.SetLineColor(7);
  WW_hElRes.SetLineWidth(5);
 
  WJ_hElRes.SetLineStyle(7);
  WJ_hElRes.SetLineColor(8);
  WJ_hElRes.SetLineWidth(5); 

   S_hElRes.Fit("gaus");  
   
   S_hElRes.Draw(""); WW_hElRes.Draw("HISTsame"); ZZ_hElRes.Draw("HISTsame"); ZW_hElRes.Draw("HISTsame"); WJ_hElRes.Draw("HISTsame"); TTb_hElRes.Draw("HISTsame"); ZJ_hElRes.Draw("HISTsame"); leg.Draw(); c.Print("OUTPUT-MUMU.ps");
  

  
  
   RD_dVz.SetLineWidth(5); 
   RD_dVz1.SetLineWidth(5);   
   RD_dVz2.SetLineWidth(5); 
   RD_dVz3.SetLineWidth(5);   
   RD_dVz4.SetLineWidth(5); 
   RD_dVz5.SetLineWidth(5);  
   
   
   
  WW_dVz.Add(&WJ_dVz);  
  TW_dVz.Add(&WW_dVz);
  QCD_dVz.Add(&TW_dVz);
  ZZ_dVz.Add(&QCD_dVz);
  ZW_dVz.Add(&ZZ_dVz);
  TTb_dVz.Add(&ZW_dVz);
  ZJ_dVz.Add(&TTb_dVz);
  S_dVz.Add(&ZJ_dVz);
  
    WW_dVz1.Add(&WJ_dVz1);  
  TW_dVz1.Add(&WW_dVz1);
  QCD_dVz1.Add(&TW_dVz1);
  ZZ_dVz1.Add(&QCD_dVz1);
  ZW_dVz1.Add(&ZZ_dVz1);
  TTb_dVz1.Add(&ZW_dVz1);
  ZJ_dVz1.Add(&TTb_dVz1);
  S_dVz1.Add(&ZJ_dVz1);
  
    WW_dVz2.Add(&WJ_dVz2);  
  TW_dVz2.Add(&WW_dVz2);
  QCD_dVz2.Add(&TW_dVz2);
  ZZ_dVz2.Add(&QCD_dVz2);
  ZW_dVz2.Add(&ZZ_dVz2);
  TTb_dVz2.Add(&ZW_dVz2);
  ZJ_dVz2.Add(&TTb_dVz2);
  S_dVz2.Add(&ZJ_dVz2);
  
    WW_dVz3.Add(&WJ_dVz3);  
  TW_dVz3.Add(&WW_dVz3);
  QCD_dVz3.Add(&TW_dVz3);
  ZZ_dVz3.Add(&QCD_dVz3);
  ZW_dVz3.Add(&ZZ_dVz3);
  TTb_dVz3.Add(&ZW_dVz3);
  ZJ_dVz3.Add(&TTb_dVz3);
  S_dVz3.Add(&ZJ_dVz3);
  
    WW_dVz4.Add(&WJ_dVz4);  
  TW_dVz4.Add(&WW_dVz4);
  QCD_dVz4.Add(&TW_dVz4);
  ZZ_dVz4.Add(&QCD_dVz4);
  ZW_dVz4.Add(&ZZ_dVz4);
  TTb_dVz4.Add(&ZW_dVz4);
  ZJ_dVz4.Add(&TTb_dVz4);
  S_dVz4.Add(&ZJ_dVz4);
  
    WW_dVz5.Add(&WJ_dVz5);  
  TW_dVz5.Add(&WW_dVz5);
  QCD_dVz5.Add(&TW_dVz5);
  ZZ_dVz5.Add(&QCD_dVz5);
  ZW_dVz5.Add(&ZZ_dVz5);
  TTb_dVz5.Add(&ZW_dVz5);
  ZJ_dVz5.Add(&TTb_dVz5);
  S_dVz5.Add(&ZJ_dVz5);
     
  S_dVz.SetLineStyle(1);
  S_dVz.SetLineColor(1);
  //S_dVz.SetFillStyle(0);
  S_dVz.SetFillColor(2);
  S_dVz.SetLineWidth(2);
  
  ZJ_dVz.SetLineStyle(1);
  ZJ_dVz.SetLineColor(1);
  //ZJ_dVz.SetFillStyle(4);
  ZJ_dVz.SetFillColor(3);
  ZJ_dVz.SetLineWidth(2);
  
  TTb_dVz.SetLineStyle(1);
  TTb_dVz.SetLineColor(1);
  //TTb_dVz.SetFillStyle(5);
  TTb_dVz.SetFillColor(4);
  TTb_dVz.SetLineWidth(2);
  
  ZW_dVz.SetLineStyle(1);
  ZW_dVz.SetLineColor(1);
  //ZW_dVz.SetFillStyle(6);
  ZW_dVz.SetFillColor(5);
  ZW_dVz.SetLineWidth(2);
  
  ZZ_dVz.SetLineStyle(1);
  ZZ_dVz.SetLineColor(1);
  //ZW_dVz.SetFillStyle(6);
  ZZ_dVz.SetFillColor(6);
  ZZ_dVz.SetLineWidth(2);
  
  WW_dVz.SetLineStyle(1);
  WW_dVz.SetLineColor(1);
  //WW_dVz.SetFillStyle(6);
  WW_dVz.SetFillColor(7);
  WW_dVz.SetLineWidth(2);
  

  WJ_dVz.SetLineStyle(1);
  WJ_dVz.SetLineColor(1);
  WJ_dVz.SetFillColor(1);
  WJ_dVz.SetLineWidth(2);  
  
  QCD_dVz.SetLineStyle(1);
  QCD_dVz.SetLineColor(1);
  QCD_dVz.SetFillColor(11);
  QCD_dVz.SetLineWidth(2); 
  
  TW_dVz.SetLineStyle(1);
  TW_dVz.SetLineColor(1);
  TW_dVz.SetFillColor(12);
  TW_dVz.SetLineWidth(2);
  
   S_dVz1.SetLineStyle(1);
  S_dVz1.SetLineColor(1);
  //S_dVz1.SetFillStyle(0);
  S_dVz1.SetFillColor(2);
  S_dVz1.SetLineWidth(2);
  
  ZJ_dVz1.SetLineStyle(1);
  ZJ_dVz1.SetLineColor(1);
  //ZJ_dVz1.SetFillStyle(4);
  ZJ_dVz1.SetFillColor(3);
  ZJ_dVz1.SetLineWidth(2);
  
  TTb_dVz1.SetLineStyle(1);
  TTb_dVz1.SetLineColor(1);
  //TTb_dVz1.SetFillStyle(5);
  TTb_dVz1.SetFillColor(4);
  TTb_dVz1.SetLineWidth(2);
  
  ZW_dVz1.SetLineStyle(1);
  ZW_dVz1.SetLineColor(1);
  //ZW_dVz1.SetFillStyle(6);
  ZW_dVz1.SetFillColor(5);
  ZW_dVz1.SetLineWidth(2);
  
  ZZ_dVz1.SetLineStyle(1);
  ZZ_dVz1.SetLineColor(1);
  //ZW_dVz1.SetFillStyle(6);
  ZZ_dVz1.SetFillColor(6);
  ZZ_dVz1.SetLineWidth(2);
  
  WW_dVz1.SetLineStyle(1);
  WW_dVz1.SetLineColor(1);
  //WW_dVz1.SetFillStyle(6);
  WW_dVz1.SetFillColor(7);
  WW_dVz1.SetLineWidth(2);
  

  WJ_dVz1.SetLineStyle(1);
  WJ_dVz1.SetLineColor(1);
  WJ_dVz1.SetFillColor(1);
  WJ_dVz1.SetLineWidth(2);  
  
  QCD_dVz1.SetLineStyle(1);
  QCD_dVz1.SetLineColor(1);
  QCD_dVz1.SetFillColor(11);
  QCD_dVz1.SetLineWidth(2); 
  
  TW_dVz1.SetLineStyle(1);
  TW_dVz1.SetLineColor(1);
  TW_dVz1.SetFillColor(12);
  TW_dVz1.SetLineWidth(2);
  
   S_dVz2.SetLineStyle(1);
  S_dVz2.SetLineColor(1);
  //S_dVz2.SetFillStyle(0);
  S_dVz2.SetFillColor(2);
  S_dVz2.SetLineWidth(2);
  
  ZJ_dVz2.SetLineStyle(1);
  ZJ_dVz2.SetLineColor(1);
  //ZJ_dVz2.SetFillStyle(4);
  ZJ_dVz2.SetFillColor(3);
  ZJ_dVz2.SetLineWidth(2);
  
  TTb_dVz2.SetLineStyle(1);
  TTb_dVz2.SetLineColor(1);
  //TTb_dVz2.SetFillStyle(5);
  TTb_dVz2.SetFillColor(4);
  TTb_dVz2.SetLineWidth(2);
  
  ZW_dVz2.SetLineStyle(1);
  ZW_dVz2.SetLineColor(1);
  //ZW_dVz2.SetFillStyle(6);
  ZW_dVz2.SetFillColor(5);
  ZW_dVz2.SetLineWidth(2);
  
  ZZ_dVz2.SetLineStyle(1);
  ZZ_dVz2.SetLineColor(1);
  //ZW_dVz2.SetFillStyle(6);
  ZZ_dVz2.SetFillColor(6);
  ZZ_dVz2.SetLineWidth(2);
  
  WW_dVz2.SetLineStyle(1);
  WW_dVz2.SetLineColor(1);
  //WW_dVz2.SetFillStyle(6);
  WW_dVz2.SetFillColor(7);
  WW_dVz2.SetLineWidth(2);
  

  WJ_dVz2.SetLineStyle(1);
  WJ_dVz2.SetLineColor(1);
  WJ_dVz2.SetFillColor(1);
  WJ_dVz2.SetLineWidth(2);  
  
  QCD_dVz2.SetLineStyle(1);
  QCD_dVz2.SetLineColor(1);
  QCD_dVz2.SetFillColor(11);
  QCD_dVz2.SetLineWidth(2); 
  
  TW_dVz2.SetLineStyle(1);
  TW_dVz2.SetLineColor(1);
  TW_dVz2.SetFillColor(12);
  TW_dVz2.SetLineWidth(2);
  
   S_dVz3.SetLineStyle(1);
  S_dVz3.SetLineColor(1);
  //S_dVz3.SetFillStyle(0);
  S_dVz3.SetFillColor(2);
  S_dVz3.SetLineWidth(2);
  
  ZJ_dVz3.SetLineStyle(1);
  ZJ_dVz3.SetLineColor(1);
  //ZJ_dVz3.SetFillStyle(4);
  ZJ_dVz3.SetFillColor(3);
  ZJ_dVz3.SetLineWidth(2);
  
  TTb_dVz3.SetLineStyle(1);
  TTb_dVz3.SetLineColor(1);
  //TTb_dVz3.SetFillStyle(5);
  TTb_dVz3.SetFillColor(4);
  TTb_dVz3.SetLineWidth(2);
  
  ZW_dVz3.SetLineStyle(1);
  ZW_dVz3.SetLineColor(1);
  //ZW_dVz3.SetFillStyle(6);
  ZW_dVz3.SetFillColor(5);
  ZW_dVz3.SetLineWidth(2);
  
  ZZ_dVz3.SetLineStyle(1);
  ZZ_dVz3.SetLineColor(1);
  //ZW_dVz3.SetFillStyle(6);
  ZZ_dVz3.SetFillColor(6);
  ZZ_dVz3.SetLineWidth(2);
  
  WW_dVz3.SetLineStyle(1);
  WW_dVz3.SetLineColor(1);
  //WW_dVz3.SetFillStyle(6);
  WW_dVz3.SetFillColor(7);
  WW_dVz3.SetLineWidth(2);
  

  WJ_dVz3.SetLineStyle(1);
  WJ_dVz3.SetLineColor(1);
  WJ_dVz3.SetFillColor(1);
  WJ_dVz3.SetLineWidth(2);  
  
  QCD_dVz3.SetLineStyle(1);
  QCD_dVz3.SetLineColor(1);
  QCD_dVz3.SetFillColor(11);
  QCD_dVz3.SetLineWidth(2); 
  
  TW_dVz3.SetLineStyle(1);
  TW_dVz3.SetLineColor(1);
  TW_dVz3.SetFillColor(12);
  TW_dVz3.SetLineWidth(2);
  
   S_dVz4.SetLineStyle(1);
  S_dVz4.SetLineColor(1);
  //S_dVz4.SetFillStyle(0);
  S_dVz4.SetFillColor(2);
  S_dVz4.SetLineWidth(2);
  
  ZJ_dVz4.SetLineStyle(1);
  ZJ_dVz4.SetLineColor(1);
  //ZJ_dVz4.SetFillStyle(4);
  ZJ_dVz4.SetFillColor(3);
  ZJ_dVz4.SetLineWidth(2);
  
  TTb_dVz4.SetLineStyle(1);
  TTb_dVz4.SetLineColor(1);
  //TTb_dVz4.SetFillStyle(5);
  TTb_dVz4.SetFillColor(4);
  TTb_dVz4.SetLineWidth(2);
  
  ZW_dVz4.SetLineStyle(1);
  ZW_dVz4.SetLineColor(1);
  //ZW_dVz4.SetFillStyle(6);
  ZW_dVz4.SetFillColor(5);
  ZW_dVz4.SetLineWidth(2);
  
  ZZ_dVz4.SetLineStyle(1);
  ZZ_dVz4.SetLineColor(1);
  //ZW_dVz4.SetFillStyle(6);
  ZZ_dVz4.SetFillColor(6);
  ZZ_dVz4.SetLineWidth(2);
  
  WW_dVz4.SetLineStyle(1);
  WW_dVz4.SetLineColor(1);
  //WW_dVz4.SetFillStyle(6);
  WW_dVz4.SetFillColor(7);
  WW_dVz4.SetLineWidth(2);
  

  WJ_dVz4.SetLineStyle(1);
  WJ_dVz4.SetLineColor(1);
  WJ_dVz4.SetFillColor(1);
  WJ_dVz4.SetLineWidth(2);  
  
  QCD_dVz4.SetLineStyle(1);
  QCD_dVz4.SetLineColor(1);
  QCD_dVz4.SetFillColor(11);
  QCD_dVz4.SetLineWidth(2); 
  
  TW_dVz4.SetLineStyle(1);
  TW_dVz4.SetLineColor(1);
  TW_dVz4.SetFillColor(12);
  TW_dVz4.SetLineWidth(2);
  
   S_dVz5.SetLineStyle(1);
  S_dVz5.SetLineColor(1);
  //S_dVz5.SetFillStyle(0);
  S_dVz5.SetFillColor(2);
  S_dVz5.SetLineWidth(2);
  
  ZJ_dVz5.SetLineStyle(1);
  ZJ_dVz5.SetLineColor(1);
  //ZJ_dVz5.SetFillStyle(4);
  ZJ_dVz5.SetFillColor(3);
  ZJ_dVz5.SetLineWidth(2);
  
  TTb_dVz5.SetLineStyle(1);
  TTb_dVz5.SetLineColor(1);
  //TTb_dVz5.SetFillStyle(5);
  TTb_dVz5.SetFillColor(4);
  TTb_dVz5.SetLineWidth(2);
  
  ZW_dVz5.SetLineStyle(1);
  ZW_dVz5.SetLineColor(1);
  //ZW_dVz5.SetFillStyle(6);
  ZW_dVz5.SetFillColor(5);
  ZW_dVz5.SetLineWidth(2);
  
  ZZ_dVz5.SetLineStyle(1);
  ZZ_dVz5.SetLineColor(1);
  //ZW_dVz5.SetFillStyle(6);
  ZZ_dVz5.SetFillColor(6);
  ZZ_dVz5.SetLineWidth(2);
  
  WW_dVz5.SetLineStyle(1);
  WW_dVz5.SetLineColor(1);
  //WW_dVz5.SetFillStyle(6);
  WW_dVz5.SetFillColor(7);
  WW_dVz5.SetLineWidth(2);
  

  WJ_dVz5.SetLineStyle(1);
  WJ_dVz5.SetLineColor(1);
  WJ_dVz5.SetFillColor(1);
  WJ_dVz5.SetLineWidth(2);  
  
  QCD_dVz5.SetLineStyle(1);
  QCD_dVz5.SetLineColor(1);
  QCD_dVz5.SetFillColor(11);
  QCD_dVz5.SetLineWidth(2); 
  
  TW_dVz5.SetLineStyle(1);
  TW_dVz5.SetLineColor(1);
  TW_dVz5.SetFillColor(12);
  TW_dVz5.SetLineWidth(2);
 
/*
 RD_dVz.Draw("e"); S_dVz.Draw("HISTsame"); ZJ_dVz.Draw("HISTsame"); TTb_dVz.Draw("HISTsame");  ZW_dVz.Draw("HISTsame"); ZZ_dVz.Draw("HISTsame"); QCD_dVz.Draw("HISTsame"); TW_dVz.Draw("HISTsame"); WW_dVz.Draw("HISTsame");  WJ_dVz.Draw("HISTsame");   RD_dVz.Draw("e1same"); 
 
 RD_dVz.GetYaxis()->SetRangeUser(0.01, 1000);
 c.Modified();
leg.Draw();
 label_Lumi(0.38, 0.85, RD_Lum);
  label_Prelim(0.38, 0.9);
  c.Print("OUTPUT-MUMU.ps");
  c.Print("V_z-MUMU.pdf");
  c.Print("V_z-MUMU.png");
  c.Print("V_z-MUMU.cxx");   
 
 RD_dVz1.Draw("e"); S_dVz1.Draw("HISTsame"); ZJ_dVz1.Draw("HISTsame"); TTb_dVz1.Draw("HISTsame");  ZW_dVz1.Draw("HISTsame"); ZZ_dVz1.Draw("HISTsame"); QCD_dVz1.Draw("HISTsame"); TW_dVz1.Draw("HISTsame"); WW_dVz1.Draw("HISTsame");  WJ_dVz1.Draw("HISTsame");   RD_dVz1.Draw("e1same"); 
 
 RD_dVz1.GetYaxis()->SetRangeUser(0.01, 1000);
 c.Modified();
 leg.Draw();
 
  label_Lumi(0.38, 0.85, RD_Lum);
  label_Prelim(0.38, 0.9);
  c.Print("OUTPUT-MUMU.ps");
  c.Print("V_z1-MUMU.pdf");
  c.Print("V_z1-MUMU.png");
  c.Print("V_z1-MUMU.cxx");  
 
 RD_dVz2.Draw("e"); S_dVz2.Draw("HISTsame"); ZJ_dVz2.Draw("HISTsame"); TTb_dVz2.Draw("HISTsame");  ZW_dVz2.Draw("HISTsame"); ZZ_dVz2.Draw("HISTsame"); QCD_dVz2.Draw("HISTsame"); TW_dVz2.Draw("HISTsame"); WW_dVz2.Draw("HISTsame");  WJ_dVz2.Draw("HISTsame");   RD_dVz2.Draw("e1same"); 
 
 RD_dVz2.GetYaxis()->SetRangeUser(0.01, 1000);
 c.Modified();
leg.Draw();
 label_Lumi(0.38, 0.85, RD_Lum);
  label_Prelim(0.38, 0.9);
  c.Print("OUTPUT-MUMU.ps");
  c.Print("V_z2-MUMU.pdf");
  c.Print("V_z2-MUMU.png");
  c.Print("V_z2-MUMU.cxx");  
 
 RD_dVz3.Draw("e"); S_dVz3.Draw("HISTsame"); ZJ_dVz3.Draw("HISTsame"); TTb_dVz3.Draw("HISTsame");  ZW_dVz3.Draw("HISTsame"); ZZ_dVz3.Draw("HISTsame"); QCD_dVz3.Draw("HISTsame"); TW_dVz3.Draw("HISTsame"); WW_dVz3.Draw("HISTsame");  WJ_dVz3.Draw("HISTsame");   RD_dVz3.Draw("e1same"); 
 
 RD_dVz3.GetYaxis()->SetRangeUser(0.01, 1000);
 c.Modified();
 leg.Draw();
  label_Lumi(0.38, 0.85, RD_Lum);
  label_Prelim(0.38, 0.9);
  c.Print("OUTPUT-MUMU.ps");
  c.Print("V_z3-MUMU.pdf");
  c.Print("V_z3-MUMU.png");
  c.Print("V_z3-MUMU.cxx");  
 
 RD_dVz4.Draw("e"); S_dVz4.Draw("HISTsame"); ZJ_dVz4.Draw("HISTsame"); TTb_dVz4.Draw("HISTsame");  ZW_dVz4.Draw("HISTsame"); ZZ_dVz4.Draw("HISTsame"); QCD_dVz4.Draw("HISTsame"); TW_dVz4.Draw("HISTsame"); WW_dVz4.Draw("HISTsame");  WJ_dVz4.Draw("HISTsame");   RD_dVz4.Draw("e1same"); 
 
 RD_dVz4.GetYaxis()->SetRangeUser(0.01, 1000);
 c.Modified();
leg.Draw();
 label_Lumi(0.38, 0.85, RD_Lum);
  label_Prelim(0.38, 0.9);
  c.Print("OUTPUT-MUMU.ps");
  c.Print("V_z4-MUMU.pdf");
  c.Print("V_z4-MUMU.png");
  c.Print("V_z4-MUMU.cxx");  
 
 RD_dVz5.Draw("e"); S_dVz5.Draw("HISTsame"); ZJ_dVz5.Draw("HISTsame"); TTb_dVz5.Draw("HISTsame");  ZW_dVz5.Draw("HISTsame"); ZZ_dVz5.Draw("HISTsame"); QCD_dVz5.Draw("HISTsame"); TW_dVz5.Draw("HISTsame"); WW_dVz5.Draw("HISTsame");  WJ_dVz5.Draw("HISTsame");   RD_dVz5.Draw("e1same"); 
 
 RD_dVz5.GetYaxis()->SetRangeUser(0.01, 1000);
 c.Modified();
leg.Draw();
 label_Lumi(0.38, 0.85, RD_Lum);
  label_Prelim(0.38, 0.9);
  c.Print("OUTPUT-MUMU.ps");
  c.Print("V_z5-MUMU.pdf");
  c.Print("V_z5-MUMU.png");
  c.Print("V_z5-MUMU.cxx");  
 */   

  WW_PU.Add(&WJ_PU);  
  TW_PU.Add(&WW_PU);
  ZZ_PU.Add(&TW_PU);
  ZW_PU.Add(&ZZ_PU);
  TTb_PU.Add(&ZW_PU);
  ZJ_PU.Add(&TTb_PU);
  S_PU.Add(&ZJ_PU);
 
 
  RD_PU.SetLineWidth(5); 
  RD_PU.SetLineStyle(1);
  RD_PU.SetLineColor(1);
  
   S_PU.SetLineStyle(1);
  S_PU.SetLineColor(1);
  //S_PU.SetFillStyle(0);
  S_PU.SetFillColor(2);
  S_PU.SetLineWidth(2);
  
  ZJ_PU.SetLineStyle(1);
  ZJ_PU.SetLineColor(1);
  //ZJ_PU.SetFillStyle(4);
  ZJ_PU.SetFillColor(3);
  ZJ_PU.SetLineWidth(2);
  
  TTb_PU.SetLineStyle(1);
  TTb_PU.SetLineColor(1);
  //TTb_PU.SetFillStyle(5);
  TTb_PU.SetFillColor(4);
  TTb_PU.SetLineWidth(2);
  
  ZW_PU.SetLineStyle(1);
  ZW_PU.SetLineColor(1);
  //ZW_PU.SetFillStyle(6);
  ZW_PU.SetFillColor(5);
  ZW_PU.SetLineWidth(2);
  
  ZZ_PU.SetLineStyle(1);
  ZZ_PU.SetLineColor(1);
  //ZW_PU.SetFillStyle(6);
  ZZ_PU.SetFillColor(6);
  ZZ_PU.SetLineWidth(2);
  
  WW_PU.SetLineStyle(1);
  WW_PU.SetLineColor(1);
  //WW_PU.SetFillStyle(6);
  WW_PU.SetFillColor(7);
  WW_PU.SetLineWidth(2);
  

  WJ_PU.SetLineStyle(1);
  WJ_PU.SetLineColor(1);
  WJ_PU.SetFillColor(1);
  WJ_PU.SetLineWidth(2);  

  TW_PU.SetLineStyle(1);
  TW_PU.SetLineColor(1);
  TW_PU.SetFillColor(12);
  TW_PU.SetLineWidth(2);
  
   
 RD_PU.Draw("e"); S_PU.Draw("HISTsame"); ZJ_PU.Draw("HISTsame"); TTb_PU.Draw("HISTsame");  ZW_PU.Draw("HISTsame"); ZZ_PU.Draw("HISTsame"); TW_PU.Draw("HISTsame"); WW_PU.Draw("HISTsame");  WJ_PU.Draw("HISTsame");   RD_PU.Draw("e1same"); 
 
 RD_PU.GetYaxis()->SetRangeUser(0.01, 1000);
  c.Modified();
  
  leg.Draw();
  label_Lumi(0.38, 0.85, RD_Lum);
  label_Prelim(0.38, 0.9);
  c.Print("OUTPUT-MUMU.ps");
  c.Print("PU-MUMU.pdf");
  c.Print("PU-MUMU.png");
  c.Print("PU-MUMU.cxx"); 
  
  
  
  WW_hZPt.Add(&WJ_hZPt);  
  TW_hZPt.Add(&WW_hZPt);
  ZZ_hZPt.Add(&TW_hZPt);
  ZW_hZPt.Add(&ZZ_hZPt);
  TTb_hZPt.Add(&ZW_hZPt);
  ZJ_hZPt.Add(&TTb_hZPt);
  S_hZPt.Add(&ZJ_hZPt);
 
 
  RD_hZPt.SetLineWidth(5); 
  RD_hZPt.SetLineStyle(1);
  RD_hZPt.SetLineColor(1);
  
   S_hZPt.SetLineStyle(1);
  S_hZPt.SetLineColor(1);
  //S_hZPt.SetFillStyle(0);
  S_hZPt.SetFillColor(2);
  S_hZPt.SetLineWidth(2);
  
  ZJ_hZPt.SetLineStyle(1);
  ZJ_hZPt.SetLineColor(1);
  //ZJ_hZPt.SetFillStyle(4);
  ZJ_hZPt.SetFillColor(3);
  ZJ_hZPt.SetLineWidth(2);
  
  TTb_hZPt.SetLineStyle(1);
  TTb_hZPt.SetLineColor(1);
  //TTb_hZPt.SetFillStyle(5);
  TTb_hZPt.SetFillColor(4);
  TTb_hZPt.SetLineWidth(2);
  
  ZW_hZPt.SetLineStyle(1);
  ZW_hZPt.SetLineColor(1);
  //ZW_hZPt.SetFillStyle(6);
  ZW_hZPt.SetFillColor(5);
  ZW_hZPt.SetLineWidth(2);
  
  ZZ_hZPt.SetLineStyle(1);
  ZZ_hZPt.SetLineColor(1);
  //ZW_hZPt.SetFillStyle(6);
  ZZ_hZPt.SetFillColor(6);
  ZZ_hZPt.SetLineWidth(2);
  
  WW_hZPt.SetLineStyle(1);
  WW_hZPt.SetLineColor(1);
  //WW_hZPt.SetFillStyle(6);
  WW_hZPt.SetFillColor(7);
  WW_hZPt.SetLineWidth(2);
  

  WJ_hZPt.SetLineStyle(1);
  WJ_hZPt.SetLineColor(1);
  WJ_hZPt.SetFillColor(1);
  WJ_hZPt.SetLineWidth(2);  
    
  TW_hZPt.SetLineStyle(1);
  TW_hZPt.SetLineColor(1);
  TW_hZPt.SetFillColor(12);
  TW_hZPt.SetLineWidth(2);
  
   
 RD_hZPt.Draw("e"); S_hZPt.Draw("HISTsame"); ZJ_hZPt.Draw("HISTsame"); TTb_hZPt.Draw("HISTsame");  ZW_hZPt.Draw("HISTsame"); ZZ_hZPt.Draw("HISTsame"); TW_hZPt.Draw("HISTsame"); WW_hZPt.Draw("HISTsame");  WJ_hZPt.Draw("HISTsame");   RD_hZPt.Draw("e1same"); 

 RD_hZPt.GetYaxis()->SetRangeUser(0.1, 1000);
 c.Modified();
 leg.Draw();
 label_Lumi(0.38, 0.85, RD_Lum);
  label_Prelim(0.38, 0.9);
  c.Print("OUTPUT-MUMU.ps");
  c.Print("ZPt-MUMU.pdf");
  c.Print("ZPt-MUMU.png");
  c.Print("ZPt-MUMU.cxx");    
 
   c.Print("OUTPUT-MUMU.ps]");
 }
