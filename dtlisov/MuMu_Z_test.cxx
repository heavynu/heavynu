//To run this script on pcrfcms14.cern.ch machine: 
//root -b -q -l MuMu_Z_test.cxx+
#include "TH1.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TROOT.h"
#include <TLorentzVector.h>
#include <TMath.h>
#include <TLatex.h>
#include "TStyle.h"
#include "TLegend.h"
#include "TText.h"
#include "TAxis.h"

#include "tdrstyle.C"
//#include "heavynuStandard.C"
#include "heavynuStandard11.C"

#include "../PATDump/src/format.h"
#include <iostream>
#include <fstream>
using namespace std;


vector<double> generate_flat10_weights(TH1D* data_npu_estimated){
    // see SimGeneral/MixingModule/python/mix_E7TeV_FlatDist10_2011EarlyData_inTimeOnly_cfi.py; copy and paste from there:
    const double npu_probs[25] = {0.0698146584, 0.0698146584, 0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,
           0.0630151648,0.0526654164,0.0402754482,0.0292988928,0.0194384503,0.0122016783,0.007207042,0.004003637,0.0020278322,
           0.0010739954,0.0004595759,0.0002229748,0.0001028162,4.58337152809607E-05 /* <-- 24 */};
    vector<double> result(25);
    double s = 0.0;
    for(int npu=0; npu<25; ++npu){
        double npu_estimated = data_npu_estimated->GetBinContent(data_npu_estimated->GetXaxis()->FindBin(npu));                              
        result[npu] = npu_estimated / npu_probs[npu];
        s += npu_estimated;
    }
    // normalize weights such that the total sum of weights over thw whole sample is 1.0, i.e., sum_i  result[i] * npu_probs[i] should be 1.0 (!)
    for(int npu=0; npu<25; ++npu){
        result[npu] /= s;
    }
    return result;
}


void MuMu_Z_test()
{
// set plain style
  gROOT->SetStyle("Plain");
  TCanvas c1("OUTPUT1","OUTPUT1");
  setTDRStyle();
  fixOverlay(); 
 
  // activate storage of sum of squares of errors
  // to calculate correctly errors in histograms
  // with non-unit entries
  TH1::SetDefaultSumw2(kTRUE);
  
  TFile* f = new TFile("204pb_10May_Rereco.root");
 // TFile* f = new TFile("36pb_21April_Rereco.root");
  TH1D* histo = (TH1D*) f->Get("pileup");
  
// Cuts for event selection
  unsigned int NofLept = 2;
  unsigned int NofJets = 1;
  float Lep1Pt = 30.;
  float Lep2Pt = 30.;
  float Jet1Pt = 40.;
  float EtaCut1L = 2.1; //1.44
  float EtaCut2L = EtaCut1L;
  float ISO = 0.1;
  float Vertex = 10.03;
  float MllCut = 70.0;
  float MllCut1 = 110.0;

// Mll histogramm parameters  
  int M_Delta_Mll = 10;
  int M_min_Mll = 70;
  int M_max_Mll = 110;
  
// MET histogramm parameters  
  int M_Delta_MET = 20;
  int M_min_MET = 0;
  int M_max_MET = 200;

  
// Samples switcher: true = include samples in analisis
  bool RD_incl = true;
  bool ZJ_incl = true;

  const char* fname;
  char *str = (char*)alloca(10000);

  float Mll, Mmet; 
  float K1 = 1.;
  float K2 = 1.;
  float HEEP_B=1.;//0.972;
  float HEEP_E=1.;//0.982;
  float A_Wei=1.;//0.97*0.97
  float Z_tune = 1.0;
  //float Z_tune = 1.0;
  float BG_nw = 0.; // number of total BG events
  float delta_BG_nw_sq = 0. ; // Squared absolut error for number of total BG events

// ================================================================================================== 

// REAL DATA 

  // histograms
	
  TH1F RD_hMll("RD_Mll", "Z+jets; M(#mu#mu), GeV;Events/4 GeV", M_Delta_Mll, M_min_Mll, M_max_Mll);
  TH1F RD_hMmets("RD_Mmets", "Miss E_{T} ; Reconstructed miss E_{T}, GeV;Events/10 GeV", M_Delta_MET, M_min_MET, M_max_MET);
  TH1F RD_hQNum( "RD_hQNum", "Product of lepton charges ; Product of lepton charges; Events", 2, -1, 1);    
  TH1F RD_dVz( "RD_dVz", "delta Vz; #delta V_{z} between e_{1}e_{2}, cm; Events", 20, 0, 0.1); 
  TH1F RD_PU( "RD_PU", "PU; Primary vertex number ; Events", 25, 0, 25);
  TH1F RD_hZPt("RD_ZPt", "Z+jets; Z Pt, GeV/c;Events/20 GeV/c", 10, 0, 200);
  
  
       fname = "/moscow41/heavynu/Data/SingleMu10MayRereco_204pb.root";
	// fname = "/moscow41/heavynu/Data/SingleElectron10MayRereco_204pb.root";
	// fname = "/moscow31/heavynu/data/Mu-Run2010B-Apr21ReReco-v1-0jets.root";
	  
	 

  TheEvent RD_e(fname);
  if (!RD_e) return;
  
  float RD_Lum = RD_e.sample.lumi; //Luminocity in pb^-1
  float delta_Lum = 0.11 ;// Error for Lumi
 
  
//counters 
  int RD_n = 0;
  
if (RD_incl == true) {

//counters 
  int RD_nEvents = 0; 
  int RD_nQ = 0;
  int RD_nQ1 = 0;
  int RD_nQ3 = 0;
  int RD_nQ4 = 0;
  
  RD_nEvents = RD_e.totalEvents();
 
  for (int i = 0; i < RD_nEvents; ++i) {
    RD_e.readEvent(i);
  
    // Cuts on number of leptons and jets in event
      if ((RD_e.muons.size() >= NofLept) && (RD_e.jets.size() >= NofJets)) {
	  
	// correction of Z mass
	//  if ((abs(RD_e.muons[0].p4.Eta())<=1.5)) {K1 = 90.2/89.6;} else {K1=90.2/85.9;}
	//  if ((abs(RD_e.muons[1].p4.Eta())<=1.5)) {K2 = 90.2/89.6;} else {K2=90.2/85.9;}
	
	//cuts on electrons Pt
	    if (((K1*RD_e.muons[0].p4).Pt()>=Lep1Pt) && ((K2*RD_e.muons[1].p4).Pt()>=Lep2Pt))  { 
		
	//cuts on jets Pt      
	 if (RD_e.jets[0].p4.Pt()>=Jet1Pt)  { 
	// if (RD_e.jets[1].p4.Pt()>=Jet1Pt)  {
	// if (RD_e.jets[2].p4.Pt()>=Jet1Pt)  {
	// if (RD_e.jets[3].p4.Pt()>=Jet1Pt)  {
	// if (RD_e.jets[4].p4.Pt()>=Jet1Pt)  {
		
		  
	//cuts on electrons invariant mass Mll	
		  if (((K1*RD_e.muons[0].p4 + K2*RD_e.muons[1].p4).M()>=MllCut)&&((K1*RD_e.muons[0].p4 + K2*RD_e.muons[1].p4).M()<=MllCut1)) {
//		  if (!(((K1*RD_e.muons[0].p4 + K2*RD_e.muons[1].p4).M()>=MllCut)&&((K1*RD_e.muons[0].p4 + K2*RD_e.muons[1].p4).M()<=MllCut1))) {
		  
	//cuts on Eta  
         if ((abs(K1*RD_e.muons[0].p4.Eta())<=EtaCut1L) && (abs(K2*RD_e.muons[1].p4.Eta())<=EtaCut2L)) {
	
	// isolation cut
		 if (((RD_e.muons[0].trackIso/RD_e.muons[0].p4.Pt()) <= ISO)&&((RD_e.muons[1].trackIso/RD_e.muons[1].p4.Pt()) <= ISO)) {
	

	//Cut on vertexes
            if (abs(RD_e.muons[0].vertex.z()-RD_e.muons[1].vertex.z())<=Vertex) {
	
	// Filling the histos

            Mll = (K1*RD_e.muons[0].p4 + K2*RD_e.muons[1].p4).M();
            RD_hMll.Fill(Mll, RD_e.weight);
			
			Mmet = (RD_e.mets[0].p4).Pt();
            RD_hMmets.Fill(Mmet, RD_e.weight);

			RD_n = RD_n + 1;			
			
			if (RD_e.muons[0].charge*RD_e.muons[1].charge == -1) { 
			RD_nQ = RD_nQ +1; 
			RD_hQNum.Fill(-0.5, RD_e.weight);}
			else {RD_nQ1 = RD_nQ1 +1;
			RD_hQNum.Fill(0.5, RD_e.weight);}
			
			if ((RD_e.muons[0].charge==-1)&&(RD_e.muons[1].charge == -1)) { 
			RD_nQ3 = RD_nQ3 +1;}
			
			if ((RD_e.muons[0].charge==1)&&(RD_e.muons[1].charge == 1)) { 
			RD_nQ4 = RD_nQ4 +1;}
						
			RD_dVz.Fill((abs(RD_e.muons[0].vertex.z()-RD_e.muons[1].vertex.z())), RD_e.weight);
			
			RD_PU.Fill(RD_e.vertices.size(), RD_e.weight);
			
			RD_hZPt.Fill((K1*RD_e.muons[0].p4 + K2*RD_e.muons[1].p4).Pt(), RD_e.weight);
		
/*			
			cout<<"New event!" << RD_n <<" PtL1 = " <<(K1*RD_e.muons[0].p4).Pt()<<" PtL2 = "<<(K2*RD_e.muons[1].p4).Pt() << " Mll = "<<(K1*RD_e.muons[0].p4 + K2*RD_e.muons[1].p4).M() << endl;
			cout<<" PtJ1 = "<<(RD_e.jets[0].p4).Pt()<<" bDiscr J1 = "<< RD_e.jets[0].bDiscr <<" PtJ2 = "<<(RD_e.jets[1].p4).Pt()<<" bDiscr J2 = "<< RD_e.jets[1].bDiscr <<endl;
			cout<<"EtaL1 = "<<(K1*RD_e.muons[0].p4).Eta()<<" PhiL1 = "<<(K1*RD_e.muons[0].p4).Phi()<<" EtaL2 = "<<(K2*RD_e.muons[1].p4).Eta()<<" PhiL2 = "<<(K2*RD_e.muons[1].p4).Phi() << endl;
			cout<<"EtaJ1 = "<<(RD_e.jets[0].p4).Eta()<<" PhiJ1 = "<<(RD_e.jets[0].p4).Phi()<<" EtaJ2 = "<<(RD_e.jets[1].p4).Eta()<<" PhiJ2 = "<<(RD_e.jets[1].p4).Phi() << endl;			
			
	        cout<<"vzL1 = "<<(RD_e.muons[0].vertex).z()<<" VzL2 = "<<(RD_e.muons[1].vertex).z()<<" VzJ1 = "<<(RD_e.jets[0].vertex).z()<<" VzJ2 = "<<(RD_e.jets[1].vertex).z() << endl;
		
	    	cout<<" L1: Iso = " << RD_e.muons[0].isoHEEP() << endl; 
					
			cout<<" L1: charge = " <<RD_e.muons[0].charge << endl; 
	
	        cout<<" L1: Iso = " << RD_e.muons[1].isoHEEP() << endl; 
			
			cout<<" L2: charge = " <<RD_e.muons[1].charge << endl; 
									
			cout<<"MWR = "<< WR <<" MNuR = "<< NuR_l1 <<" Mjj = "<< Mjj << endl;
			
			cout<<"PtWR = "<< tWR <<" PlWR = "<< lWR << endl;
			
			cout <<"RUN = "<< RD_e.irun << " LUMI = " << RD_e.ilumi << " EVENT = "<< RD_e.ievent<< endl;
		    
			cout << "Number of events with 0 b-jet = " << RD_nB3 << endl; 
			cout << "Number of events with 1 b-jet = " << RD_nB1 << endl;
			cout << "Number of events with 2 b-jet = " << RD_nB2 << endl;	
            cout << endl;	
            cout << "Number of events with same charges of lepton = " << RD_nQ1 << endl;
			cout << "Number of events with opposite charges of lepton = " << RD_nQ << endl; 
*/        
		}
		}
        }
        }
		
	//	}
    //  } 		
    //  }        
	//	}
		}
	
        } 	
        }   
		
  }
  
 cout << "REAL DATA" << endl;
 cout << endl;
 cout << "Total events = " << RD_nEvents << endl;
 cout << "Total weight events = " << RD_nEvents*RD_e.weight << endl;
 cout << endl; 
 cout << "Number of passed events = " <<RD_n<<"   "<<RD_n*RD_e.weight<< endl;
 cout << endl;	
 cout << "Number of events with same charges of lepton = " << RD_nQ1<<"   " <<RD_nQ1*RD_e.weight << endl;
 cout << "Number of events with opposite charges of lepton = " << RD_nQ <<"   " <<RD_nQ*RD_e.weight << endl; 
 cout << "Number of events with -1 charges of lepton = " << RD_nQ3 <<"   " <<RD_nQ3*RD_e.weight << endl; 
 cout << "Number of events with +1 charges of lepton = " << RD_nQ4 <<"   " <<RD_nQ4*RD_e.weight << endl; 
 cout << endl;
 
 
} 

  
// MC BG Z+JETS

  TH1F ZJ_hMll("ZJ_Mll", "lepton-lepton pair invariant mass;mass, GeV/c^{2};events/2 GeV", M_Delta_Mll, M_min_Mll, M_max_Mll);
  TH1F ZJ_hMmets("ZJ_Mmets", "Miss Et; Et, GeV/c^{2};events/10 GeV", M_Delta_MET, M_min_MET, M_max_MET); 
  TH1F ZJ_hQNum( "ZJ_hQNum", "Mult of leptons charges ; e_1*e_2; events", 2, -1, 1);  
  TH1F ZJ_dVz( "ZJ_dVz", "delta Vz; #delta V_{z} between ee, cm; Events", 20, 0, 0.1); 
  TH1F ZJ_PU( "RD_PU", "PU; Primary vertex number ; Events", 25, 0, 25); 
  TH1F ZJ_PU1( "RD_PU1", "PU1; Primary vertex number ; Events", 25, 0, 25); 
  TH1F ZJ_hZPt("ZJ_ZPt", "Z+jets; Z Pt, GeV/c;Events/20 GeV/c", 10, 0, 200);
  
  int ZJ_n = 0;
  int ZJ_n1 = 0;
  float ZJ_nw = 0;

if (ZJ_incl) { 

  int ZJ_nEvents = 0;
  float ZJ_nwEvents = 0;   
  int ZJ_nQ = 0;
  float ZJ_nwQ = 0;
  int ZJ_nQ1 = 0;
  float ZJ_nwQ1 = 0; 
  int ZJ_nQ3 = 0;
  float ZJ_nwQ3 = 0; 
  int ZJ_nQ4 = 0;
  float ZJ_nwQ4 = 0; 
  float ZJ_weight =0;
 
    for (int fc = 0; fc < 21; ++fc) { 
  /*
   switch(fc)
    {
   case 0:
    fname = "/moscow31/heavynu/38x/Z0Jets_TuneZ2_7TeV-alpgen-tauola.root";
	break;
   case 1:
    fname = "/moscow31/heavynu/38x/Z1Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 2:
    fname = "/moscow31/heavynu/38x/Z1Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 3:
    fname = "/moscow31/heavynu/38x/Z1Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;	
   case 4:
    fname = "/moscow31/heavynu/38x/Z1Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 5:
    fname = "/moscow31/heavynu/38x/Z2Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola.root";
	break;
   case 6:
    fname = "/moscow31/heavynu/38x/Z2Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 7:
    fname = "/moscow31/heavynu/38x/Z2Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 8:
    fname = "/moscow31/heavynu/38x/Z2Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;	
   case 9:
    fname = "/moscow31/heavynu/38x/Z3Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 10:
    fname = "/moscow31/heavynu/38x/Z3Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 11:
    fname = "/moscow31/heavynu/38x/Z3Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 12:
    fname = "/moscow31/heavynu/38x/Z3Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;	
   case 13:
    fname = "/moscow31/heavynu/38x/Z4Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 14:
    fname = "/moscow31/heavynu/38x/Z4Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 15:
    fname = "/moscow31/heavynu/38x/Z4Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 16:
    fname = "/moscow31/heavynu/38x/Z4Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 17:
	fname = "/moscow31/heavynu/38x/Z5Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 18:
    fname = "/moscow31/heavynu/38x/Z5Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola.root";
    break;
   case 19:
    fname = "/moscow31/heavynu/38x/Z5Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
   case 20:
    fname = "/moscow31/heavynu/38x/Z5Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola.root"; 
    break;
    }
 */
 
 switch(fc)
    {
   case 0:
    fname = "/moscow41/heavynu/41x-background/Z0Jets_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root";
	break;
   case 1:
    fname = "/moscow41/heavynu/41x-background/Z1Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root";
    break;
   case 2:
    fname = "/moscow41/heavynu/41x-background/Z1Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root"; 
    break;
   case 3:
    fname = "/moscow41/heavynu/41x-background/Z1Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root"; 
    break;	
   case 4:
    fname = "/moscow41/heavynu/41x-background/Z1Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root"; 
    break;
   case 5:
    fname = "/moscow41/heavynu/41x-background/Z2Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root";
	break;
   case 6:
    fname = "/moscow41/heavynu/41x-background/Z2Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root";
    break;
   case 7:
    fname = "/moscow41/heavynu/41x-background/Z2Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root"; 
    break;
   case 8:
    fname = "/moscow41/heavynu/41x-background/Z2Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root"; 
    break;	
   case 9:
    fname = "/moscow41/heavynu/41x-background/Z3Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root"; 
    break;
   case 10:
    fname = "/moscow41/heavynu/41x-background/Z3Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root";
    break;
   case 11:
    fname = "/moscow41/heavynu/41x-background/Z3Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root"; 
    break;
   case 12:
    fname = "/moscow41/heavynu/41x-background/Z3Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root"; 
    break;	
   case 13:
    fname = "/moscow41/heavynu/41x-background/Z4Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root"; 
    break;
   case 14:
    fname = "/moscow41/heavynu/41x-background/Z4Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root";
    break;
   case 15:
    fname = "/moscow41/heavynu/41x-background/Z4Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root"; 
    break;
   case 16:
    fname = "/moscow41/heavynu/41x-background/Z4Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root"; 
    break;
   case 17:
	fname = "/moscow41/heavynu/41x-background/Z5Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root"; 
    break;
   case 18:
    fname = "/moscow41/heavynu/41x-background/Z5Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root";
    break;
   case 19:
    fname = "/moscow41/heavynu/41x-background/Z5Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root"; 
    break;
   case 20:
    fname = "/moscow41/heavynu/41x-background/Z5Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root"; 
    break;
    }
	
	
   TheEvent ZJ_e(fname);
  if (!ZJ_e) return;
  
  ZJ_nEvents =ZJ_nEvents + ZJ_e.totalEvents();
  
  for (int i = 0; i < ZJ_e.totalEvents(); ++i) {
    ZJ_e.readEvent(i);
	
	if(ZJ_e.pileup.size()<25){
       ZJ_weight = ZJ_e.weight *  generate_flat10_weights(histo)[ZJ_e.pileup.size()];
    }
    else{ //should not happen as we have a weight for all simulated ZJ_e.pileup.size() multiplicities!
       ZJ_weight = 0.;
    }
	
	
	//ZJ_weight = ZJ_e.weight;
	
	ZJ_nwEvents = ZJ_nwEvents + Z_tune*ZJ_weight*A_Wei*RD_Lum/100.;
    
      if ((ZJ_e.muons.size() >= NofLept) && (ZJ_e.jets.size() >= NofJets)) {
	  
	 //  if (abs((ZJ_e.muons[0].p4).Eta())<=1.44){A_Wei=HEEP_B;} else {A_Wei=HEEP_E;}
	 //  if (abs((ZJ_e.muons[1].p4).Eta())<=1.44){A_Wei=A_Wei*HEEP_B;} else {A_Wei=A_Wei*HEEP_E;}
	
	//cuts on electrons Pt>=40
	    if (((ZJ_e.muons[0].p4).Pt()>=Lep1Pt) && ((ZJ_e.muons[1].p4).Pt()>=Lep2Pt))  { 
		
	//cuts on jets Pt      
	      if (ZJ_e.jets[0].p4.Pt()>=Jet1Pt)  {
       //   if (ZJ_e.jets[1].p4.Pt()>=Jet1Pt)  {
       //   if (ZJ_e.jets[2].p4.Pt()>=Jet1Pt)  {
       //   if (ZJ_e.jets[3].p4.Pt()>=Jet1Pt)  {
       //   if (ZJ_e.jets[4].p4.Pt()>=Jet1Pt)  {		
		  
	//cuts on electrons invariant mass Mll>=400.0	
		  if (((ZJ_e.muons[0].p4 + ZJ_e.muons[1].p4).M()>=MllCut)&&((ZJ_e.muons[0].p4 + ZJ_e.muons[1].p4).M()<=MllCut1)) {
//		  if (!(((ZJ_e.muons[0].p4 + ZJ_e.muons[1].p4).M()>=MllCut)&&((ZJ_e.muons[0].p4 + ZJ_e.muons[1].p4).M()<=MllCut1))) {
			  
	//cuts on Eta  
         if ((abs(ZJ_e.muons[0].p4.Eta())<=EtaCut1L) && (abs(ZJ_e.muons[1].p4.Eta())<=EtaCut2L))  {
	
	
	// isolation cut
		 if (((ZJ_e.muons[0].trackIso/ZJ_e.muons[0].p4.Pt()) <= ISO)&&((ZJ_e.muons[1].trackIso/ZJ_e.muons[1].p4.Pt()) <= ISO)) {
		

	
	 //Cut on vertexes
            if (abs(ZJ_e.muons[0].vertex.z()-ZJ_e.muons[1].vertex.z())<=Vertex) {
		
            Mll = (ZJ_e.muons[0].p4 + ZJ_e.muons[1].p4).M();
            ZJ_hMll.Fill(Mll, Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);
      
			Mmet = (ZJ_e.mets[0].p4).Pt();
            ZJ_hMmets.Fill(Mmet, Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);

			ZJ_n = ZJ_n + 1;
			
			ZJ_nw = ZJ_nw + Z_tune*ZJ_weight*A_Wei*RD_Lum/100.;				
			
			if (ZJ_e.muons[0].charge*ZJ_e.muons[1].charge == -1) { 
			ZJ_nQ = ZJ_nQ+1;
			ZJ_nwQ = ZJ_nwQ + Z_tune*ZJ_weight*A_Wei*RD_Lum/100.;; 
			ZJ_hQNum.Fill(-0.5, Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);}
			else {
			ZJ_nQ1 = ZJ_nQ1+1;
			ZJ_nwQ1 = ZJ_nwQ1 + Z_tune*ZJ_weight*A_Wei*RD_Lum/100.;
			ZJ_hQNum.Fill(0.5, Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);}
			
			if ((ZJ_e.muons[0].charge==-1)&&(ZJ_e.muons[1].charge == -1)) { 
			ZJ_nQ3 = ZJ_nQ3+1;	
			ZJ_nwQ3 = ZJ_nwQ3 + Z_tune*ZJ_weight*A_Wei*RD_Lum/100.;}
			
			if ((ZJ_e.muons[0].charge==1)&&(ZJ_e.muons[1].charge == 1)) { 
			ZJ_nQ4 = ZJ_nQ4+1;	
			ZJ_nwQ4 = ZJ_nwQ4 + Z_tune*ZJ_weight*A_Wei*RD_Lum/100.;}
			
			
			ZJ_dVz.Fill((abs(ZJ_e.muons[0].vertex.z()-ZJ_e.muons[1].vertex.z())), Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);	

			ZJ_PU.Fill(ZJ_e.vertices.size(), Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);
			ZJ_PU1.Fill(ZJ_e.pileup.size(), Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);
			ZJ_hZPt.Fill((ZJ_e.muons[0].p4 + ZJ_e.muons[1].p4).Pt(), Z_tune*ZJ_weight*A_Wei*RD_Lum/100.);
			
		/*	
			cout<<"New event!" << n <<" PtL1 = " <<(ZJ_e.muons[0].p4).Pt()<<" PtL2 = "<<(ZJ_e.muons[1].p4).Pt() << " Mll = "<<(ZJ_e.muons[0].p4 + ZJ_e.muons[1].p4).M() << endl;
			cout<<" PtJ1 = "<<(ZJ_e.jets[0].p4).Pt()<<" bDiscr J1 = "<< ZJ_e.jets[0].bDiscr <<" PtJ2 = "<<(ZJ_e.jets[1].p4).Pt()<<" bDiscr J2 = "<< ZJ_e.jets[1].bDiscr <<endl;
			cout<<"EtaL1 = "<<(ZJ_e.muons[0].p4).Eta()<<" PhiL1 = "<<(ZJ_e.muons[0].p4).Phi()<<" EtaL2 = "<<(ZJ_e.muons[1].p4).Eta()<<" PhiL2 = "<<(ZJ_e.muons[1].p4).Phi() << endl;
			cout<<"EtaJ1 = "<<(ZJ_e.jets[0].p4).Eta()<<" PhiJ1 = "<<(ZJ_e.jets[0].p4).Phi()<<" EtaJ2 = "<<(ZJ_e.jets[1].p4).Eta()<<" PhiJ2 = "<<(ZJ_e.jets[1].p4).Phi() << endl;			
			
	        cout<<"vzL1 = "<<(ZJ_e.muons[0].vertex).z()<<" VzL2 = "<<(ZJ_e.muons[1].vertex).z()<<" VzJ1 = "<<(ZJ_e.jets[0].vertex).z()<<" VzJ2 = "<<(ZJ_e.jets[1].vertex).z() << endl;
		
	    	cout<<" L1: ecalIso = " <<ZJ_e.muons[0].ecalIso <<" hcalIso = "<<ZJ_e.muons[0].hcalIso <<" hcal1Iso = "<<ZJ_e.muons[0].hcal1Iso << " trackIso = "<< ZJ_e.muons[0].trackIso << endl; 
					
			cout<<" L1: charge = " <<ZJ_e.muons[0].charge << endl; 
	
	       cout<<" L1: ecalIso = " <<ZJ_e.muons[1].ecalIso <<" hcalIso = "<<ZJ_e.muons[1].hcalIso <<" hcal1Iso = "<<ZJ_e.muons[1].hcal1Iso << " trackIso = "<< ZJ_e.muons[1].trackIso << endl; 
			
			cout<<" L2: charge = " <<ZJ_e.muons[1].charge << endl;
							
			cout<<"MWR = "<< WR <<" MNuR = "<< NuR_l1 <<" Mjj = "<< Mjj << endl;
			cout <<"RUN = "<< ZJ_e.irun << " LUMI = " << ZJ_e.ilumi << " EVENT = "<< ZJ_e.ievent<< endl;
		    
			
        */  	
		}  
	    }
        }		
        } 
		
     //   }
	 //   }
     //   }
     //   }
        }
		
		}
	   }

  }
}
 cout << "MC BG Z+JETS" << endl;
 cout << endl;
 cout << "Total events = " << ZJ_nEvents << endl;
 cout << "Total weight events = " << ZJ_nwEvents << endl;
 cout << endl; 
 cout << "Number of passed events = "<<ZJ_n<<"   "<<ZJ_nw<< endl;
 cout << endl;
 cout << "Number of events with same charges of lepton = " << ZJ_nQ1<<"   " <<ZJ_nwQ1 << endl;
 cout << "Number of events with opposite charges of lepton = " << ZJ_nQ <<"   " <<ZJ_nwQ << endl; 
 cout << "Number of events with -1 charges of lepton = " << ZJ_nQ3 <<"   " <<ZJ_nwQ3 << endl; 
 cout << "Number of events with +1 charges of lepton = " << ZJ_nQ4 <<"   " <<ZJ_nwQ4 << endl; 
 cout << endl;
}


//=========================================================================================================================

// Histos plotting and saving

  TCanvas c("OUTPUT","OUTPUT");

  c.Print("OUTPUT_mu.ps[");
   


   

  // ============================================
  // save to .root file M_l1 histograms with:
  //  1. MC: summ of all backgrounds
  //  2. MC: signal
  //  3. DATA:
  //{
  // ZJ_hMll.Sumw2();
  
 //	RD_hMll.Sumw2();
    TFile f2("fit-input-Mll_mu.root", "RECREATE");
    ZJ_hMll.Clone("ZJ")->Write();
    RD_hMll.Clone("Data")->Write();
    f2.Close();
  //}
  // ============================================
  

  RD_hMll.SetLineColor(1);
  RD_hMll.SetLineWidth(5); 

  ZJ_hMll.SetLineStyle(1);
  ZJ_hMll.SetLineColor(1);
  //ZJ_hMll.SetFillStyle(4);
  ZJ_hMll.SetFillColor(3);
  ZJ_hMll.SetLineWidth(2);

 //RD_hMll.Fit("gaus"); 
  
  TF1* fitf = new TF1("fitf", "[0] + [1]*exp(-([2] - x)*([2] - x)/(2*[3]*[3]))", 86, 96);
  fitf->SetNpx(500);
  fitf->SetParameters(500, 10000, 90, 5);

  RD_hMll.Fit(fitf, "IMN");
  const double A = fitf->GetParameter(1); // area under gaussian
  const double B = fitf->GetParameter(2); // area under gaussian
  const double C = fitf->GetParameter(3); // area under gaussian
  
  cout << "M_Z = "<< B << endl;
  cout << "Width_Z = "<< C <<endl;
  
 //ZJ_hMll.Fit("gaus");
  
  TF1* fitf1 = new TF1("fitf1", "[0] + [1]*exp(-([2] - x)*([2] - x)/(2*[3]*[3]))", 86, 96);
  fitf1->SetNpx(500);
  fitf1->SetParameters(500, 10000, 90, 5);
  ZJ_hMll.Fit(fitf1, "IMN");
  const double A1 = fitf1->GetParameter(1); // area under gaussian
  const double B1 = fitf1->GetParameter(2); // area under gaussian
  const double C1 = fitf1->GetParameter(3); // area under gaussian
  
  cout << "MC M_Z = "<< B1 << endl;
  cout << "MC Width_Z = "<< C1 <<endl;
  

 
 RD_hMll.Draw("e");  ZJ_hMll.Draw("HISTsame"); RD_hMll.Draw("e1same"); 

 // RD_hMll.GetYaxis()->SetRangeUser(0.1, 1000);
 // RD_hMll.GetYaxis()->SetLimitsUser(-50, 1000);
  c.Modified();
  /*
  // draw fit to background
  fitf->SetLineColor(kBlack);
  fitf->SetLineStyle(7);
  fitf->SetLineWidth(3);
  fitf->SetParameter(1, 0);
  fitf->DrawCopy("same");
  */
  // draw fit to background + signal
  fitf->SetLineColor(kBlack);
  fitf->SetLineStyle(1);
  fitf->SetLineWidth(3);
  fitf->SetParameter(1, A);
  fitf->Draw("same");
  /*
  // draw fit to background
  fitf1->SetLineColor(kRed);
  fitf1->SetLineStyle(7);
  fitf1->SetLineWidth(3);
  fitf1->SetParameter(1, 0);
  fitf1->DrawCopy("same");
 */ 
  // draw fit to background + signal
  fitf1->SetLineColor(kRed);
  fitf1->SetLineStyle(1);
  fitf1->SetLineWidth(3);
  fitf1->SetParameter(1, A1);
  fitf1->Draw("same");

   TLegend leg(0.58, 0.79, 0.94, 0.94);
//TLegend leg(0.17, 0.52, 0.57, 0.94);
leg.SetTextFont(42);
leg.SetTextSize(0.034);
   
  sprintf(str,"Muon-Muon channel"); 
  leg.SetHeader(str);
//  sprintf(str,"Run2010A: %d", RD_n); 
  sprintf(str,"Run2011: %d", RD_n); 
  leg.AddEntry(&RD_hMll, str, "EL");
  sprintf(str,"Z+j MC: %5.2f", ZJ_nw);
  leg.AddEntry(&ZJ_hMll, str,"F");
  
  leg.Draw();
  label_Lumi(0.38, 0.85, RD_Lum);
  label_Prelim(0.38, 0.9);
  c.Print("OUTPUT.ps");
  
  c.Print("M_ll_mu.pdf");
  c.Print("M_ll_mu.png");   
  c.Print("M_ll_mu.cxx");   
  
  

  
  RD_hMmets.SetLineColor(1);
  RD_hMmets.SetLineWidth(5); 

  
  ZJ_hMmets.SetLineStyle(1);
  ZJ_hMmets.SetLineColor(1);
  //ZJ_hMmets.SetFillStyle(4);
  ZJ_hMmets.SetFillColor(3);
  ZJ_hMmets.SetLineWidth(2);
  
  
 RD_hMmets.Draw("e"); ZJ_hMmets.Draw("HISTsame"); RD_hMmets.Draw("e1same"); 

 
// RD_hMmets.GetYaxis()->SetRangeUser(0.01, 1000);
// RD_hMmets.GetXaxis()->SetLimits(-10, 210);
 c.Modified();

   leg.Draw();
  label_Lumi(0.38, 0.85, RD_Lum);
  label_Prelim(0.38, 0.9);
   
  c.Print("MET_mu.pdf");
  c.Print("MET_mu.png");
  c.Print("MET_mu.cxx");

 
 // RD_hQNum.SetMaximum(5)

  RD_hQNum.SetLineColor(1);
  RD_hQNum.SetLineWidth(5); 

  ZJ_hQNum.SetLineStyle(1);
  ZJ_hQNum.SetLineColor(1);
  //ZJ_hQNum.SetFillStyle(4);
  ZJ_hQNum.SetFillColor(3);
  ZJ_hQNum.SetLineWidth(2);
  
  
 RD_hQNum.Draw("e");  ZJ_hQNum.Draw("HISTsame");RD_hQNum.Draw("e1same"); 
 
// RD_hQNum.GetYaxis()->SetRangeUser(0.01, 10000);
 //RD_hQNum.GetXaxis()->SetLimits(-1.1, 1.1);
 c.Modified();
 
    leg.Draw(); 
  label_Lumi(0.38, 0.85, RD_Lum);
  label_Prelim(0.38, 0.9);
  
  
  c.Print("OUTPUT.ps");
  c.Print("Num_q_mu.pdf");
  c.Print("Num_q_mu.png");
  c.Print("Num_q_mu.cxx");
  
  
   RD_dVz.SetLineWidth(5); 

  
  ZJ_dVz.SetLineStyle(1);
  ZJ_dVz.SetLineColor(1);
  //ZJ_dVz.SetFillStyle(4);
  ZJ_dVz.SetFillColor(3);
  ZJ_dVz.SetLineWidth(2);

 RD_dVz.Draw("e"); ZJ_dVz.Draw("HISTsame"); RD_dVz.Draw("e1same"); 
 
// RD_dVz.GetYaxis()->SetRangeUser(0.01, 1000);
 c.Modified();
leg.Draw();
 label_Lumi(0.38, 0.85, RD_Lum);
  label_Prelim(0.38, 0.9);
  c.Print("OUTPUT.ps");
  c.Print("V_z_mu.pdf");
  c.Print("V_z_mu.png");
  c.Print("V_z_mu.cxx");   
  
  
  RD_PU.SetLineWidth(5); 
  RD_PU.SetLineStyle(1);
  RD_PU.SetLineColor(1);
  
  ZJ_PU1.SetLineWidth(5); 
  ZJ_PU1.SetLineStyle(2);
  ZJ_PU1.SetLineColor(2);
  
  
  ZJ_PU.SetLineStyle(1);
  ZJ_PU.SetLineColor(1);
  //ZJ_PU.SetFillStyle(4);
  ZJ_PU.SetFillColor(3);
  ZJ_PU.SetLineWidth(2);

  ZJ_PU.Draw("HIST");ZJ_PU1.Draw("HISTsame"); RD_PU.Draw("E1same");
  
 //ZJ_PU.GetYaxis()->SetRangeUser(0.01, 100000);
  c.Modified();
  
  TLegend leg1(0.58, 0.79, 0.94, 0.94);
 //TLegend leg1(0.17, 0.52, 0.57, 0.94);
  leg1.SetTextFont(42);
  leg1.SetTextSize(0.034);
    sprintf(str,"Muon-Muon channel"); 
  leg1.SetHeader(str);
//  sprintf(str,"Run2010A: %d", RD_n); 
  sprintf(str,"Run2011: %d", RD_n); 
  leg1.AddEntry(&RD_PU, str, "EL");
  sprintf(str,"Z+j MC: %5.2f", ZJ_nw);
  leg1.AddEntry(&ZJ_PU, str,"F");
  sprintf(str,"Z+j RC: %5.2f", ZJ_nw);
  leg1.AddEntry(&ZJ_PU1, str,"L");
  
  leg1.Draw();
  label_Lumi(0.38, 0.85, RD_Lum);
  label_Prelim(0.38, 0.9);
  c.Print("OUTPUT_mu.ps");
  c.Print("PU_mu.pdf");
  c.Print("PU_mu.png");
  c.Print("PU_mu.cxx");  
  
  
  
  
  RD_hZPt.SetLineWidth(5); 
  
  ZJ_hZPt.SetLineStyle(1);
  ZJ_hZPt.SetLineColor(1);
  //ZJ_hZPt.SetFillStyle(4);
  ZJ_hZPt.SetFillColor(3);
  ZJ_hZPt.SetLineWidth(2);

 RD_hZPt.Draw("e"); ZJ_hZPt.Draw("HISTsame"); RD_hZPt.Draw("e1same"); 
 
 //RD_hZPt.GetYaxis()->SetRangeUser(0.01, 10000);
 c.Modified();
 leg.Draw();
 label_Lumi(0.38, 0.85, RD_Lum);
  label_Prelim(0.38, 0.9);
  c.Print("OUTPUT_mu.ps");
  c.Print("ZPt_mu.pdf");
  c.Print("ZPt_mu.png");
  c.Print("ZPt_mu.cxx");  
 
   c.Print("OUTPUT_mu.ps]");
}
