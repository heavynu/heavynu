// This is a small program to make histo


// Headers:
//
// c++
#include <iostream>

// root
#include "TH1.h"
#include "TStyle.h"
#include "TFile.h"
#include "TROOT.h"
#include "TLegend.h"
#include "TCanvas.h"
using namespace std;


int main(int argc, char* argv[]) {

  TH1::SetDefaultSumw2();

  char *str = (char*)alloca(10000);
  char *str1 = (char*)alloca(10000);

  gROOT->SetStyle("Plain");
  gStyle->SetOptStat("");
  gStyle->SetOptLogy(1);

//---electrons------------------------------

  double k_e[5]= {1., 0.6265, 0.6409, 0.5459, 2.4354};

  TFile* f_e = new TFile("zboson_advanced_full.root");
  TH1F* hMZ_0[5];  
  hMZ_0[0] = (TH1F*) f_e->Get("hMZ_0");
  hMZ_0[1] = (TH1F*) f_e->Get("hMZ_1");
  hMZ_0[2] = (TH1F*) f_e->Get("hMZ_2");
  hMZ_0[3] = (TH1F*) f_e->Get("hMZ_3");
  hMZ_0[4] = (TH1F*) f_e->Get("hMZ_4");
  
  TH1F* hMZ_e[5];
  for (int FileNumber = 0; FileNumber < 5; ++FileNumber) {
       sprintf(str,"hMZ_e_%d", FileNumber);
       hMZ_e[FileNumber] = new TH1F(str, "M_{ee} distribution;M_{ee}, GeV/c^{2};#events", 33, 80, 100); //histogram mass z boson declaration
       for (int j = 0; j < hMZ_0[FileNumber]->GetNbinsX(); ++j) 
          if ((hMZ_0[FileNumber]->GetBinCenter(j)>80)&&(hMZ_0[FileNumber]->GetBinCenter(j)<100)) hMZ_e[FileNumber]->SetBinContent((j-33),hMZ_0[FileNumber]->GetBinContent(j));
  }

  TH1F* hMZ1_e[5];  
  hMZ1_e[0] = (TH1F*) f_e->Get("hMZ1_0");
  hMZ1_e[1] = (TH1F*) f_e->Get("hMZ1_1");
  hMZ1_e[2] = (TH1F*) f_e->Get("hMZ1_2");
  hMZ1_e[3] = (TH1F*) f_e->Get("hMZ1_3");
  hMZ1_e[4] = (TH1F*) f_e->Get("hMZ1_4");


  TH1F* hMZ2_e[5];  
  hMZ2_e[0] = (TH1F*) f_e->Get("hMZ2_G_0");
  hMZ2_e[1] = (TH1F*) f_e->Get("hMZ2_G_1");
  hMZ2_e[2] = (TH1F*) f_e->Get("hMZ2_G_2");
  hMZ2_e[3] = (TH1F*) f_e->Get("hMZ2_G_3");
  hMZ2_e[4] = (TH1F*) f_e->Get("hMZ2_G_4");

  TH1F* hMZ3_e[5];  
  hMZ3_e[0] = (TH1F*) f_e->Get("hMZ3_G_0");
  hMZ3_e[1] = (TH1F*) f_e->Get("hMZ3_G_1");
  hMZ3_e[2] = (TH1F*) f_e->Get("hMZ3_G_2");
  hMZ3_e[3] = (TH1F*) f_e->Get("hMZ3_G_3");
  hMZ3_e[4] = (TH1F*) f_e->Get("hMZ3_G_4");

  TH1F* hPU_e[5];
  hPU_e[0] = (TH1F*) f_e->Get("hPU_0");
  hPU_e[1] = (TH1F*) f_e->Get("hPU_1");
  hPU_e[2] = (TH1F*) f_e->Get("hPU_2");
  hPU_e[3] = (TH1F*) f_e->Get("hPU_3");
  hPU_e[4] = (TH1F*) f_e->Get("hPU_4");

  TH1F* hPU_G_e[5];
  hPU_G_e[0] = (TH1F*) f_e->Get("hPU_G_0");
  hPU_G_e[1] = (TH1F*) f_e->Get("hPU_G_1");
  hPU_G_e[2] = (TH1F*) f_e->Get("hPU_G_2");
  hPU_G_e[3] = (TH1F*) f_e->Get("hPU_G_3");
  hPU_G_e[4] = (TH1F*) f_e->Get("hPU_G_4");

  TH1F* hTruePU_e[5];
  hTruePU_e[0] = (TH1F*) f_e->Get("hTrueNumInter_0");
  hTruePU_e[1] = (TH1F*) f_e->Get("hTrueNumInter_1");
  hTruePU_e[2] = (TH1F*) f_e->Get("hTrueNumInter_2");
  hTruePU_e[3] = (TH1F*) f_e->Get("hTrueNumInter_3");
  hTruePU_e[4] = (TH1F*) f_e->Get("hTrueNumInter_4");

  TH1F* hTruePU_G_e[5];
  hTruePU_G_e[0] = (TH1F*) f_e->Get("hTrueNumInterWeighted_0");
  hTruePU_G_e[1] = (TH1F*) f_e->Get("hTrueNumInterWeighted_1");
  hTruePU_G_e[2] = (TH1F*) f_e->Get("hTrueNumInterWeighted_2");
  hTruePU_G_e[3] = (TH1F*) f_e->Get("hTrueNumInterWeighted_3");
  hTruePU_G_e[4] = (TH1F*) f_e->Get("hTrueNumInterWeighted_4");
  
  TH1F* histo_MC_e = (TH1F*) f_e->Get("MC_pileup");
  TH1F* histo_DA_e = (TH1F*) f_e->Get("pileup");


  for (int i = 1; i < 5; ++i) {
     hMZ_e[i]->Scale(k_e[i]);
     hMZ1_e[i]->Scale(k_e[i]);
     hMZ2_e[i]->Scale(k_e[i]);
     hMZ3_e[i]->Scale(k_e[i]);
     hPU_e[i]->Scale(k_e[i]);
     hPU_G_e[i]->Scale(k_e[i]);
     hTruePU_e[i]->Scale(k_e[i]);
     hTruePU_G_e[i]->Scale(k_e[i]);
  }


  TCanvas c_e("ZtoEE", "ZtoEE", 800, 800);

  TLegend leg_A(0.65, 0.75, 0.95, 0.95);
  leg_A.SetTextFont(3242);
  leg_A.SetTextSize(0.02);
  sprintf(str1,"Data:%4.0f", hMZ_e[0]->Integral()); 
  leg_A.AddEntry(hMZ_e[0], str1, "EP");
  sprintf(str1,"Z+jets amcatnloFXFX:%4.0f", hMZ_e[1]->Integral());
  leg_A.AddEntry(hMZ_e[1], str1,"L");
  sprintf(str1,"Z->ee amcatnloFXFX:%4.0f", hMZ_e[2]->Integral());
  leg_A.AddEntry(hMZ_e[2], str1,"L");
  sprintf(str1,"Z+jets madgraphMLM:%4.0f", hMZ_e[3]->Integral());
  leg_A.AddEntry(hMZ_e[3], str1,"L");
  sprintf(str1,"Z->ee powheg:%4.0f", hMZ_e[4]->Integral());
  leg_A.AddEntry(hMZ_e[4], str1,"L");


  TLegend leg_A1(0.6, 0.70, 0.90, 0.90);
  leg_A1.SetTextFont(3242);
  leg_A1.SetTextSize(0.02);
  sprintf(str1,"Data:%4.0f", hMZ1_e[0]->Integral()); 
  leg_A1.AddEntry(hMZ1_e[0], str1, "EP");
  sprintf(str1,"Z+jets amcatnloFXFX:%4.0f", hMZ1_e[1]->Integral());
  leg_A1.AddEntry(hMZ1_e[1], str1,"L");
  sprintf(str1,"Z->ee amcatnloFXFX:%4.0f", hMZ1_e[2]->Integral());
  leg_A1.AddEntry(hMZ1_e[2], str1,"L");
  sprintf(str1,"Z+jets madgraphMLM:%4.0f", hMZ1_e[3]->Integral());
  leg_A1.AddEntry(hMZ1_e[3], str1,"L");
  sprintf(str1,"Z->ee powheg:%4.0f", hMZ1_e[4]->Integral());
  leg_A1.AddEntry(hMZ1_e[4], str1,"L");


  TLegend leg_A2(0.6, 0.70, 0.90, 0.90);
  leg_A2.SetTextFont(3242);
  leg_A2.SetTextSize(0.02);
  sprintf(str1,"Data:%4.0f", hMZ2_e[0]->Integral()); 
  leg_A2.AddEntry(hMZ1_e[0], str1, "EP");
  sprintf(str1,"Z+jets amcatnloFXFX:%4.0f", hMZ2_e[1]->Integral());
  leg_A2.AddEntry(hMZ1_e[1], str1,"L");
  sprintf(str1,"Z->ee amcatnloFXFX:%4.0f", hMZ2_e[2]->Integral());
  leg_A2.AddEntry(hMZ2_e[2], str1,"L");
  sprintf(str1,"Z+jets madgraphMLM:%4.0f", hMZ2_e[3]->Integral());
  leg_A2.AddEntry(hMZ2_e[3], str1,"L");
  sprintf(str1,"Z->ee powheg:%4.0f", hMZ2_e[4]->Integral());
  leg_A2.AddEntry(hMZ2_e[4], str1,"L");


  TLegend leg_A3(0.6, 0.70, 0.90, 0.90);
  leg_A3.SetTextFont(3242);
  leg_A3.SetTextSize(0.02);
  sprintf(str1,"Data:%4.0f", hMZ3_e[0]->Integral()); 
  leg_A3.AddEntry(hMZ3_e[0], str1, "EP");
  sprintf(str1,"Z+jets amcatnloFXFX:%4.0f", hMZ3_e[1]->Integral());
  leg_A3.AddEntry(hMZ3_e[1], str1,"L");
  sprintf(str1,"Z->ee amcatnloFXFX:%4.0f", hMZ3_e[2]->Integral());
  leg_A3.AddEntry(hMZ3_e[2], str1,"L");
  sprintf(str1,"Z+jets madgraphMLM:%4.0f", hMZ3_e[3]->Integral());
  leg_A3.AddEntry(hMZ3_e[3], str1,"L");
  sprintf(str1,"Z->ee powheg:%4.0f", hMZ3_e[4]->Integral());
  leg_A3.AddEntry(hMZ3_e[4], str1,"L");


  gStyle->SetOptLogy(1);

 c_e.Print(".pdf[");

    hMZ_e[0]->SetTitle("");
    hMZ_e[0]->SetMarkerStyle(21);
    hMZ_e[0]->SetMarkerColor(1);
    hMZ_e[0]->Draw("PE1");
for (int i = 1; i < 5; ++i) {
    hMZ_e[i]->SetLineStyle(1);
    hMZ_e[i]->SetLineColor(1+i);
    hMZ_e[i]->Draw("HISTsame");
}
// save picture to .pdf file
    leg_A.Draw();
    c_e.Print(".pdf");


    hMZ1_e[0]->SetTitle("");
    hMZ1_e[0]->SetMarkerStyle(21);
    hMZ1_e[0]->SetMarkerColor(1);
    hMZ1_e[0]->Draw("E1");
for (int i = 1; i < 5; ++i) {
    hMZ1_e[i]->SetLineStyle(1);
    hMZ1_e[i]->SetLineColor(1+i);
    hMZ1_e[i]->Draw("HISTsame");
}
// save picture to .pdf file
    leg_A1.Draw();
    c_e.Print(".pdf");


    hMZ2_e[0]->SetTitle("");
    hMZ2_e[0]->SetMarkerStyle(21);
    hMZ2_e[0]->SetMarkerColor(1);
    hMZ2_e[0]->Draw("E1");
for (int i = 1; i < 5; ++i) {
    hMZ2_e[i]->SetLineStyle(1);
    hMZ2_e[i]->SetLineColor(1+i);
    hMZ2_e[i]->Draw("HISTsame");
}
// save picture to .pdf file
    leg_A2.Draw();
    c_e.Print(".pdf");


    hMZ3_e[0]->SetTitle("");
    hMZ3_e[0]->SetMarkerStyle(21);
    hMZ3_e[0]->SetMarkerColor(1);
    hMZ3_e[0]->Draw("E1");
for (int i = 1; i < 5; ++i) {
    hMZ3_e[i]->SetLineStyle(1);
    hMZ3_e[i]->SetLineColor(1+i);
    hMZ3_e[i]->Draw("HISTsame");
}
// save picture to .pdf file
    leg_A3.Draw();
    c_e.Print(".pdf");

  TLegend leg_A4(0.70, 0.75, 0.95, 0.95);
  leg_A4.SetTextFont(3242);
  leg_A4.SetTextSize(0.02);
  leg_A4.AddEntry(hMZ_e[0], "Data", "EP");
  leg_A4.AddEntry(hMZ_e[1], "Z+jets amcatnloFXFX","L");
  leg_A4.AddEntry(hMZ_e[2], "Z->ee amcatnloFXFX","L");
  leg_A4.AddEntry(hMZ_e[3], "Z+jets madgraphMLM","L");
  leg_A4.AddEntry(hMZ_e[4], "Z->ee powheg","L");

    hPU_e[0]->SetTitle("");
    hPU_e[0]->SetMarkerStyle(21);
    hPU_e[0]->SetMarkerColor(1);
    hPU_e[0]->DrawNormalized("E1");
for (int i = 1; i < 5; ++i) {
    hPU_e[i]->SetLineStyle(1);
    hPU_e[i]->SetLineColor(1+i);
    hPU_e[i]->DrawNormalized("HISTsame");
}

// save picture to .pdf file
    leg_A4.Draw();
    c_e.Print(".pdf");

    hPU_G_e[0]->SetTitle("");
    hPU_G_e[0]->SetMarkerStyle(21);
    hPU_G_e[0]->SetMarkerColor(1);
    hPU_G_e[0]->DrawNormalized("E1");
for (int i = 1; i < 5; ++i) {
    hPU_G_e[i]->SetLineStyle(1);
    hPU_G_e[i]->SetLineColor(1+i);
    hPU_G_e[i]->DrawNormalized("HISTsame");
}

// save picture to .pdf file
    leg_A4.Draw();
    c_e.Print(".pdf");


    histo_DA_e->SetTitle("");
    histo_DA_e->SetMarkerStyle(21);
    histo_DA_e->SetMarkerColor(1);
    histo_DA_e->DrawNormalized("E1");
    histo_MC_e->DrawNormalized("same");

    leg_A4.AddEntry(histo_MC_e, "True MC","L");

for (int i = 1; i < 5; ++i) {
    hTruePU_e[i]->SetLineStyle(1);
    hTruePU_e[i]->SetLineColor(1+i);
    hTruePU_e[i]->DrawNormalized("HISTsame");
}

// save picture to .pdf file
    leg_A4.Draw();
    c_e.Print(".pdf");

    histo_DA_e->SetTitle("");
    histo_DA_e->SetMarkerStyle(21);
    histo_DA_e->SetMarkerColor(1);
    histo_DA_e->DrawNormalized("E1");
    histo_MC_e->DrawNormalized("same");

for (int i = 1; i < 5; ++i) {
    hTruePU_G_e[i]->SetLineStyle(1);
    hTruePU_G_e[i]->SetLineColor(1+i);
    hTruePU_G_e[i]->DrawNormalized("HISTsame");
}

// save picture to .pdf file
    leg_A4.Draw();
    c_e.Print(".pdf");

 c_e.Print(".pdf]");

//----------electrons-----------------------------------------------------------------

//---muon------------------------------

  double k_m[5]= {1., 0.8977, 0.7777, 1., 1.};
  TFile* f_m = new TFile("zboson_advanced_MuMu_full.root");
  TH1F* hMZ_m[5];  
  hMZ_m[0] = (TH1F*) f_m->Get("hMZ_0");
  hMZ_m[1] = (TH1F*) f_m->Get("hMZ_1");
  hMZ_m[2] = (TH1F*) f_m->Get("hMZ_2");


  TH1F* hMZ1_m[5];  
  hMZ1_m[0] = (TH1F*) f_m->Get("hMZ1_0");
  hMZ1_m[1] = (TH1F*) f_m->Get("hMZ1_1");
  hMZ1_m[2] = (TH1F*) f_m->Get("hMZ1_2");


  TH1F* hMZ2_m[5];  
  hMZ2_m[0] = (TH1F*) f_m->Get("hMZ2_0");
  hMZ2_m[1] = (TH1F*) f_m->Get("hMZ2_1");
  hMZ2_m[2] = (TH1F*) f_m->Get("hMZ2_2");


  TH1F* hMZ3_m[5];  
  hMZ3_m[0] = (TH1F*) f_m->Get("hMZ3_0");
  hMZ3_m[1] = (TH1F*) f_m->Get("hMZ3_1");
  hMZ3_m[2] = (TH1F*) f_m->Get("hMZ3_2");

  TH1F* hPU_m[5];
  hPU_m[0] = (TH1F*) f_m->Get("hPU_0");
  hPU_m[1] = (TH1F*) f_m->Get("hPU_1");
  hPU_m[2] = (TH1F*) f_m->Get("hPU_2");
  hPU_m[3] = (TH1F*) f_m->Get("hPU_3");
  hPU_m[4] = (TH1F*) f_m->Get("hPU_4");

  TH1F* hTruePU_m[5];
  hTruePU_m[0] = (TH1F*) f_m->Get("hTrueNumInterWeighted_0");
  hTruePU_m[1] = (TH1F*) f_m->Get("hTrueNumInterWeighted_1");
  hTruePU_m[2] = (TH1F*) f_m->Get("hTrueNumInterWeighted_2");
  hTruePU_m[3] = (TH1F*) f_m->Get("hTrueNumInterWeighted_3");
  hTruePU_m[4] = (TH1F*) f_m->Get("hTrueNumInterWeighted_4");

  TH1F* histo_MC_m = (TH1F*) f_m->Get("MC_pileup");
  TH1F* histo_DA_m = (TH1F*) f_m->Get("pileup");


  for (int i = 1; i < 3; ++i) {
     hMZ_m[i]->Scale(k_m[i]);
     hMZ1_m[i]->Scale(k_m[i]);
     hMZ2_m[i]->Scale(k_m[i]);
     hMZ3_m[i]->Scale(k_m[i]);
     hPU_m[i]->Scale(k_m[i]);
     hTruePU_m[i]->Scale(k_m[i]);
  }


  TCanvas c_m("ZtoMuMu", "ZtoMuMu", 800, 800);

  TLegend leg_B(0.58, 0.80, 0.90, 0.90);
  leg_B.SetTextFont(3242);
  leg_B.SetTextSize(0.02);
  sprintf(str1,"Data:%4.0f", hMZ_m[0]->Integral()); 
  leg_B.AddEntry(hMZ_m[0], str1, "EP");
  sprintf(str1,"Z+jets amcatnloFXFX:%4.0f", hMZ_m[1]->Integral());
  leg_B.AddEntry(hMZ_m[1], str1,"L");
  sprintf(str1,"Z+jets madgraphMLM:%4.0f", hMZ_m[2]->Integral());
  leg_B.AddEntry(hMZ_m[2], str1,"L");


  TLegend leg_B1(0.6, 0.80, 0.90, 0.90);
  leg_B1.SetTextFont(3242);
  leg_B1.SetTextSize(0.02);
  sprintf(str1,"Data:%4.0f", hMZ1_m[0]->Integral()); 
  leg_B1.AddEntry(hMZ1_m[0], str1, "EP");
  sprintf(str1,"Z+jets amcatnloFXFX:%4.0f", hMZ1_m[1]->Integral());
  leg_B1.AddEntry(hMZ1_m[1], str1,"L");
  sprintf(str1,"Z+jets madgraphMLM:%4.0f", hMZ1_m[2]->Integral());
  leg_B1.AddEntry(hMZ1_m[2], str1,"L");


  TLegend leg_B2(0.6, 0.80, 0.90, 0.90);
  leg_B2.SetTextFont(3242);
  leg_B2.SetTextSize(0.02);
  sprintf(str1,"Data:%4.0f", hMZ2_m[0]->Integral()); 
  leg_B2.AddEntry(hMZ1_m[0], str1, "EP");
  sprintf(str1,"Z+jets amcatnloFXFX:%4.0f", hMZ2_m[1]->Integral());
  leg_B2.AddEntry(hMZ1_m[1], str1,"L");
  sprintf(str1,"Z+jets madgraphMLM:%4.0f", hMZ2_m[2]->Integral());
  leg_B2.AddEntry(hMZ2_m[2], str1,"L");


  TLegend leg_B3(0.6, 0.80, 0.90, 0.90);
  leg_B3.SetTextFont(3242);
  leg_B3.SetTextSize(0.02);
  sprintf(str1,"Data:%4.0f", hMZ3_m[0]->Integral()); 
  leg_B3.AddEntry(hMZ3_m[0], str1, "EP");
  sprintf(str1,"Z+jets amcatnloFXFX:%4.0f", hMZ3_m[1]->Integral());
  leg_B3.AddEntry(hMZ3_m[1], str1,"L");
  sprintf(str1,"Z+jets madgraphMLM:%4.0f", hMZ3_m[2]->Integral());
  leg_B3.AddEntry(hMZ3_m[2], str1,"L");


  gStyle->SetOptLogy(1);

 c_m.Print(".pdf[");

    hMZ_m[0]->SetTitle("");
    hMZ_m[0]->SetMarkerStyle(21);
    hMZ_m[0]->SetMarkerColor(1);
    hMZ_m[0]->Draw("E1");
for (int i = 1; i < 3; ++i) {
    hMZ_m[i]->SetLineStyle(1);
    hMZ_m[i]->SetLineColor(1+i);
    hMZ_m[i]->Draw("HISTsame");
}
// save picture to .pdf file
    leg_B.Draw();
    c_m.Print(".pdf");


    hMZ1_m[0]->SetTitle("");
    hMZ1_m[0]->SetMarkerStyle(21);
    hMZ1_m[0]->SetMarkerColor(1);
    hMZ1_m[0]->Draw("E1");
for (int i = 1; i < 3; ++i) {
    hMZ1_m[i]->SetLineStyle(1);
    hMZ1_m[i]->SetLineColor(1+i);
    hMZ1_m[i]->Draw("HISTsame");
}
// save picture to .pdf file
    leg_B1.Draw();
    c_m.Print(".pdf");


    hMZ2_m[0]->SetTitle("");
    hMZ2_m[0]->SetMarkerStyle(21);
    hMZ2_m[0]->SetMarkerColor(1);
    hMZ2_m[0]->Draw("E1");
for (int i = 1; i < 3; ++i) {
    hMZ2_m[i]->SetLineStyle(1);
    hMZ2_m[i]->SetLineColor(1+i);
    hMZ2_m[i]->Draw("HISTsame");
}
// save picture to .pdf file
    leg_B2.Draw();
    c_m.Print(".pdf");

    hMZ3_m[0]->SetTitle("");
    hMZ3_m[0]->SetMarkerStyle(21);
    hMZ3_m[0]->SetMarkerColor(1);
    hMZ3_m[0]->Draw("E1");
for (int i = 1; i < 3; ++i) {
    hMZ3_m[i]->SetLineStyle(1);
    hMZ3_m[i]->SetLineColor(1+i);
    hMZ3_m[i]->Draw("HISTsame");
}
// save picture to .pdf file
    leg_B3.Draw();
    c_m.Print(".pdf");


  TLegend leg_B4(0.65, 0.80, 0.90, 0.90);
  leg_B4.SetTextFont(3242);
  leg_B4.SetTextSize(0.02);
  leg_B4.AddEntry(hMZ_e[0], "Data", "EP");
  leg_B4.AddEntry(hMZ_e[1], "Z+jets amcatnloFXFX","L");
  leg_B4.AddEntry(hMZ_e[3], "Z+jets madgraphMLM","L");


    hPU_m[0]->SetTitle("");
    hPU_m[0]->SetMarkerStyle(21);
    hPU_m[0]->SetMarkerColor(1);
    hPU_m[0]->DrawNormalized("E1");
for (int i = 1; i < 3; ++i) {
    hPU_m[i]->SetLineStyle(1);
    hPU_m[i]->SetLineColor(1+i);
    hPU_m[i]->DrawNormalized("HISTsame");
}

// save picture to .pdf file
    leg_B4.Draw();
    c_m.Print(".pdf");

    histo_DA_m->SetTitle("");
    histo_DA_m->SetMarkerStyle(21);
    histo_DA_m->SetMarkerColor(1);
    histo_DA_m->DrawNormalized("E1");
    histo_MC_m->DrawNormalized("same");

    leg_B4.AddEntry(histo_MC_m, "True MC","L");

for (int i = 1; i < 3; ++i) {
    hTruePU_m[i]->SetLineStyle(1);
    hTruePU_m[i]->SetLineColor(1+i);
    hTruePU_m[i]->DrawNormalized("HISTsame");
}

// save picture to .pdf file
    leg_B4.Draw();
    c_m.Print(".pdf");

    c_m.Print(".pdf]");

//----------muon-----------------------------------------------------------------

//----------tau-----------------------------------------------------------------------

  double k_t[5]= {1., 1., 1., 1., 1.};
  TFile* f_t = new TFile("zboson_advanced_TauTau_full.root");
  TH1F* hMZ_t[5];  
  hMZ_t[0] = (TH1F*) f_t->Get("hMZ_0");
  hMZ_t[1] = (TH1F*) f_t->Get("hMZ_1");
  hMZ_t[2] = (TH1F*) f_t->Get("hMZ_2");


  TH1F* hMZ1_t[5];  
  hMZ1_t[0] = (TH1F*) f_t->Get("hMZ1_0");
  hMZ1_t[1] = (TH1F*) f_t->Get("hMZ1_1");
  hMZ1_t[2] = (TH1F*) f_t->Get("hMZ1_2");


  TH1F* hMZ2_t[5];  
  hMZ2_t[0] = (TH1F*) f_t->Get("hMZ2_0");
  hMZ2_t[1] = (TH1F*) f_t->Get("hMZ2_1");
  hMZ2_t[2] = (TH1F*) f_t->Get("hMZ2_2");


  TH1F* hMZ3_t[5];  
  hMZ3_t[0] = (TH1F*) f_t->Get("hMZ3_0");
  hMZ3_t[1] = (TH1F*) f_t->Get("hMZ3_1");
  hMZ3_t[2] = (TH1F*) f_t->Get("hMZ3_2");


  for (int i = 1; i < 3; ++i) {
     hMZ_t[i]->Scale(k_t[i]);
     hMZ1_t[i]->Scale(k_t[i]);
     hMZ2_t[i]->Scale(k_t[i]);
     hMZ3_t[i]->Scale(k_t[i]);
  }


  TCanvas c_t("ZtoTauTau", "ZtoTauTau", 800, 800);

  TLegend leg_C(0.6, 0.80, 0.90, 0.90);
  leg_C.SetTextFont(3242);
  leg_C.SetTextSize(0.02);
  sprintf(str1,"Data:%4.0f", hMZ_t[0]->Integral()); 
  leg_C.AddEntry(hMZ_t[0], str1, "EP");
  sprintf(str1,"Z+jets amcatnloFXFX:%4.0f", hMZ_t[1]->Integral());
  leg_C.AddEntry(hMZ_t[1], str1,"L");
  sprintf(str1,"Z+jets madgraphMLM:%4.0f", hMZ_t[2]->Integral());
  leg_C.AddEntry(hMZ_t[2], str1,"L");


  TLegend leg_C1(0.6, 0.80, 0.90, 0.90);
  leg_C1.SetTextFont(3242);
  leg_C1.SetTextSize(0.02);
  sprintf(str1,"Data:%4.0f", hMZ1_t[0]->Integral()); 
  leg_C1.AddEntry(hMZ1_t[0], str1, "EP");
  sprintf(str1,"Z+jets amcatnloFXFX:%4.0f", hMZ1_t[1]->Integral());
  leg_C1.AddEntry(hMZ1_t[1], str1,"L");
  sprintf(str1,"Z+jets madgraphMLM:%4.0f", hMZ1_t[2]->Integral());
  leg_C1.AddEntry(hMZ1_t[2], str1,"L");


  TLegend leg_C2(0.6, 0.80, 0.90, 0.90);
  leg_C2.SetTextFont(3242);
  leg_C2.SetTextSize(0.02);
  sprintf(str1,"Data:%4.0f", hMZ2_t[0]->Integral()); 
  leg_C2.AddEntry(hMZ1_t[0], str1, "EP");
  sprintf(str1,"Z+jets amcatnloFXFX:%4.0f", hMZ2_t[1]->Integral());
  leg_C2.AddEntry(hMZ1_t[1], str1,"L");
  sprintf(str1,"Z+jets madgraphMLM:%4.0f", hMZ2_t[2]->Integral());
  leg_C2.AddEntry(hMZ2_t[2], str1,"L");


  TLegend leg_C3(0.6, 0.80, 0.90, 0.90);
  leg_C3.SetTextFont(3242);
  leg_C3.SetTextSize(0.02);
  sprintf(str1,"Data:%4.0f", hMZ3_t[0]->Integral()); 
  leg_C3.AddEntry(hMZ3_t[0], str1, "EP");
  sprintf(str1,"Z+jets amcatnloFXFX:%4.0f", hMZ3_t[1]->Integral());
  leg_C3.AddEntry(hMZ3_t[1], str1,"L");
  sprintf(str1,"Z+jets madgraphMLM:%4.0f", hMZ3_t[2]->Integral());
  leg_C3.AddEntry(hMZ3_t[2], str1,"L");


  gStyle->SetOptLogy(1);

 c_t.Print(".pdf[");

    hMZ_t[0]->SetTitle("");
    hMZ_t[0]->SetMarkerStyle(21);
    hMZ_t[0]->SetMarkerColor(1);
    hMZ_t[0]->Draw("E1");
for (int i = 1; i < 3; ++i) {
    hMZ_t[i]->SetLineStyle(1);
    hMZ_t[i]->SetLineColor(1+i);
    hMZ_t[i]->Draw("HISTsame");
}
// save picture to .pdf file
    leg_C.Draw();
    c_t.Print(".pdf");


    hMZ1_t[0]->SetTitle("");
    hMZ1_t[0]->SetMarkerStyle(21);
    hMZ1_t[0]->SetMarkerColor(1);
    hMZ1_t[0]->Draw("E1");
for (int i = 1; i < 3; ++i) {
    hMZ1_t[i]->SetLineStyle(1);
    hMZ1_t[i]->SetLineColor(1+i);
    hMZ1_t[i]->Draw("HISTsame");
}
// save picture to .pdf file
    leg_C1.Draw();
    c_t.Print(".pdf");


    hMZ2_t[0]->SetTitle("");
    hMZ2_t[0]->SetMarkerStyle(21);
    hMZ2_t[0]->SetMarkerColor(1);
    hMZ2_t[0]->Draw("E1");
for (int i = 1; i < 3; ++i) {
    hMZ2_t[i]->SetLineStyle(1);
    hMZ2_t[i]->SetLineColor(1+i);
    hMZ2_t[i]->Draw("HISTsame");
}
// save picture to .pdf file
    leg_C2.Draw();
    c_t.Print(".pdf");


    hMZ3_t[0]->SetTitle("");
    hMZ3_t[0]->SetMarkerStyle(21);
    hMZ3_t[0]->SetMarkerColor(1);
    hMZ3_t[0]->Draw("E1");
for (int i = 1; i < 3; ++i) {
    hMZ3_t[i]->SetLineStyle(1);
    hMZ3_t[i]->SetLineColor(1+i);
    hMZ3_t[i]->Draw("HISTsame");
}
// save picture to .pdf file
    leg_C3.Draw();
    c_t.Print(".pdf");

    c_t.Print(".pdf]");

//----------tau-----------------------------------------------------------------------
     
 return 0;
}
