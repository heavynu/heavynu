//To run this script on pcrfcms14.cern.ch machine: 
//root -b -q -l Trig_Eff.cxx+
#include "TH1.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TROOT.h"
#include <TLorentzVector.h>
#include <TMath.h>
#include <TLatex.h>
#include "TStyle.h"
#include "TLegend.h"
#include "TText.h"
#include "TAxis.h"
#include "TGraph.h"
#include "../../PhysicsTools/Utilities/interface/LumiReweightingStandAlone.h"
//#include "../../PhysicsTools/Utilities/interface/Lumi3DReWeighting.h"

#include "tdrstyle.C"
//#include "heavynuStandard.C"
#include "heavynuStandard11.C"

#include "../PATDump/src/format.h"
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

void Trig_Eff() {
// set plain style
  gROOT->SetStyle("Plain");
  TCanvas c1("OUTPUT1","OUTPUT1");
  setTDRStyle();
  fixOverlay(); 
 
  TH1::SetDefaultSumw2(kTRUE);
 
// Cuts for event selection
  unsigned int NofLept = 2;
  unsigned int NofJets = 0;
  float Lep1Pt = 40.; 
  float Lep2Pt = 40.;
  float Jet1Pt = 40.;
  int HeepId1 = 0;
  int HeepId2 = 0;
  float ISO1 = 1.;
  float ISO2 = 1.;
  float EtaCutE_min = 2.0; //1.44
  float EtaCutE_max = 3.0;//2.5
  float EtaCutE2_min = 0.; //1.44
  float EtaCutE2_max = 2.0;//2.5
  float EtaCutJ = 2.5; 
  float MllCut =  80.0;
  float MllCut1 = 100.0;
  

// Mll histogramm parameters  
  float M_Delta_Mll = 60;
  float M_min_Mll = 80;
  float M_max_Mll = 100;
  
// Pt histogramm parameters  
  int M_Delta_Pt = 40;
  float M_min_Pt = 0;
  float M_max_Pt = 400;
  
  float Mll; 

  char *str = (char*)alloca(10000);
  char *str1 = (char*)alloca(10000);

  TH1F* hMll_A[10][3];
  TH1F* hMll_B[10][3];
  TH1F* hEPt_A[10][3];  
  TH1F* hEPt_B[10][3]; 
  TH1F* hSCPt_A[10][3];
  TH1F* hSCPt_B[10][3];
  TH1F* hEffPt_A[10][3];
  TH1F* hEffPt_B[10][3];
  TH1F* hEEta_A[10][3];  
  TH1F* hEEta_B[10][3];  
  TH1F* hSCEta_A[10][3];
  TH1F* hSCEta_B[10][3];
  TH1F* hEffEta_A[10][3];
  TH1F* hEffEta_B[10][3];
  TH1F* hEPU_A[10][3]; 
  TH1F* hEPU_B[10][3]; 
  TH1F* hSCPU_A[10][3];
  TH1F* hSCPU_B[10][3];
  TH1F* hEffPU_A[10][3];  
  TH1F* hEffPU_B[10][3];
  TH1F* hPU_A[10];
  TH1F* hPU_B[10];
  Double_t xbins[11] = {0,10,20,30,40,50,60,80,100,200,400};
  
  
  for (int i = 0; i <= 9; ++i) {
       for (int j = 0; j <= 2; ++j) {   
            sprintf(str,"Mll_A_%d_%d", i,j);
            sprintf(str1,"Mll_A; M(ee), GeV;Events/%f GeV", (M_max_Mll-M_min_Mll)/M_Delta_Mll );
            hMll_A[i][j] = new TH1F(str,str1, M_Delta_Mll, M_min_Mll, M_max_Mll);
			sprintf(str,"Mll_B_%d_%d", i,j);
            sprintf(str1,"Mll_B; M(ee), GeV;Events/%f GeV", (M_max_Mll-M_min_Mll)/M_Delta_Mll );
            hMll_B[i][j] = new TH1F(str,str1, M_Delta_Mll, M_min_Mll, M_max_Mll);
            sprintf(str,"SCPt_A_%d_%d", i, j);
	        sprintf(str1,"SCPt_A; P_{t}, GeV;Efficiency");  
            hSCPt_A[i][j] = new TH1F(str,str1, M_Delta_Pt, M_min_Pt, M_max_Pt);
			sprintf(str,"SCPt_B_%d_%d", i, j);
	        sprintf(str1,"SCPt_B; P_{t}, GeV;Efficiency");  
            hSCPt_B[i][j] = new TH1F(str,str1, M_Delta_Pt, M_min_Pt, M_max_Pt);
            sprintf(str,"EPt_A_%d_%d", i, j); 
	        sprintf(str1,"EPt_A; P_{t}, GeV;Efficiency" );  
            hEPt_A[i][j] = new TH1F(str,str1, M_Delta_Pt, M_min_Pt, M_max_Pt);
			sprintf(str,"EPt_B_%d_%d", i, j); 
	        sprintf(str1,"EPt_B; P_{t}, GeV;Efficiency" );  
            hEPt_B[i][j] = new TH1F(str,str1, M_Delta_Pt, M_min_Pt, M_max_Pt);
	        sprintf(str,"SCEta_A_%d_%d", i, j);   
            hSCEta_A[i][j] = new TH1F(str,"SCEta_A; #eta; Efficiency", 15, 0, 3);
			sprintf(str,"SCEta_B_%d_%d", i, j);   
            hSCEta_B[i][j] = new TH1F(str,"SCEta_B; #eta; Efficiency", 15, 0, 3);
            sprintf(str,"EEta_A_%d_%d", i, j);
            hEEta_A[i][j] = new TH1F(str,"EEta_A; #eta; Efficiency", 15, 0, 3); 
			sprintf(str,"EEta_B_%d_%d", i, j);
            hEEta_B[i][j] = new TH1F(str,"EEta_B; #eta; Efficiency", 15, 0, 3); 
	        sprintf(str,"SCPU_A_%d_%d", i, j);   
            hSCPU_A[i][j] = new TH1F(str,"SCPU_A; Primary vertex number;Efficiency", 25, 0, 25);
			sprintf(str,"SCPU_B_%d_%d", i, j);   
            hSCPU_B[i][j] = new TH1F(str,"SCPU_B; Primary vertex number;Efficiency", 25, 0, 25);
            sprintf(str,"EPU_A_%d_%d", i, j);
            hEPU_A[i][j] = new TH1F(str,"EPU_A; Primary vertex number;Efficiency", 25, 0, 25); 
            sprintf(str,"EPU_B_%d_%d", i, j);
            hEPU_B[i][j] = new TH1F(str,"EPU_B; Primary vertex number;Efficiency", 25, 0, 25);  			
       }
    sprintf(str,"PU_A_%d", i);
    hPU_A[i] = new TH1F(str, "PU_A; Primary vertex number ; Events", 25, 0, 25);
	sprintf(str,"PU_B_%d", i);
    hPU_B[i] = new TH1F(str, "PU_B; Primary vertex number ; Events", 25, 0, 25);
  }
  const char* fname[10];
  float Lum[10] = {0};
  int n[10] = {0};
  int nEvents[10] = {0};
 
  fname[0] = "/moscow41/heavynu/Data_2011/DFoton_2011A.root";
  fname[1] = "/moscow41/heavynu/Data_2011/DFoton_B_v1.root"; 
//  fname[0] = "/moscow41/heavynu/Data_2011/DElectron_2011A.root";
//  fname[1] = "/moscow41/heavynu/Data_2011/DElectron_B_v1.root";   
  

  for (int j = 0; j <= 1; ++j) {
      TheEvent e(fname[j]);
      if (!e) return;
      Lum[j] = e.sample.lumi; //Luminocity in pb^-1
      n[j] = 0;
      nEvents[j] = e.totalEvents();
      for (int i = 0; i < nEvents[j]; ++i) {
          e.readEvent(i);
      //    cout<< "Event "<<i<<" Run "<< e.irun<<" Event No "<< e.ievent<<endl;
	  int flag = 1;
	  float Prescale = 1.;
	  
	  const vector<int>& HLTBits = *e.hltBits;
	  const HLTtable& hlt = e.HLT();
	 
	  for (size_t i_t = 0; i_t < HLTBits.size()-1; ++i_t) {
	        string Trigger = "";
	        if ((HLTBits[i_t]==1)&&
		       (i_t<hlt.names.size())) {
	           //cout << i_t << " \t" << HLTBits[i_t]<< " \t" << hlt.names[i_t] << " \t"<< hlt.prescales[i_t]<< endl;
		       //if ((Prescale>hlt.prescales[i_t])&&(hlt.prescales[i_t]>0)) {Prescale=hlt.prescales[i_t];}
		       for (unsigned int h_s = 0; h_s < (hlt.names[i_t].length()-1); ++h_s) {
		            Trigger += hlt.names[i_t][h_s];
		       }
	       }
	       /*
		   if (Trigger=="HLT_DoublePhoton33_v"){ 
	        flag=1;
			Prescale=Prescale=hlt.prescales[i_t];
                } 	
		 */
            				
	  }
	  
	 // cout<< "Prescale = "<< Prescale<< endl;
	  
	  for (unsigned int cur=0;cur<e.electrons.size();cur++) {
	       if (!((e.electrons[cur].ID <= HeepId1)&&(e.electrons[cur].isoHEEP() <= ISO1))) {e.electrons.erase(e.electrons.begin()+cur);}
	  }
	  for (unsigned int cur=0;cur<e.jets.size();cur++) {
	       if (!((e.jets[cur].ID >= 1)&&(abs((e.jets[cur].p4).Eta())<=EtaCutJ))) {e.jets.erase(e.jets.begin()+cur);}
	  } 
	  
	  for (unsigned int k=0;k<e.electrons.size();k++) { 
	      for (unsigned int l=k+1;l<e.electrons.size();l++) {
	  
	          if ((flag==1)&&(e.electrons.size() >= NofLept)&&
	              (e.electrons[k].ID <= HeepId1)&&(e.electrons[l].ID <= HeepId2)&&
	              (e.electrons[k].isoHEEP() <= ISO1)&&(e.electrons[l].isoHEEP() <= ISO2)&&
	              (abs(e.electrons[k].p4.Eta()) >= EtaCutE_min)&&(abs(e.electrons[k].p4.Eta()) <= EtaCutE_max)&&
	              (abs(e.electrons[l].p4.Eta()) >= EtaCutE2_min)&&(abs(e.electrons[l].p4.Eta()) <= EtaCutE2_max)&&
	              (e.electrons[k].p4.Pt() >=  Lep1Pt)&&(e.electrons[l].p4.Pt() >=  Lep2Pt)&&
	              ((e.electrons[k].p4 + e.electrons[l].p4).M()>=MllCut)&&((e.electrons[k].p4 + e.electrons[l].p4).M()<=MllCut1))  {
		    //      (e.jets.size()>=NofJets)&&(e.jets[0].p4.Pt()>=Jet1Pt)) {
		    //      (e.jets.size()>=NofJets)&&(e.jets[0].p4.Pt()>=Jet1Pt)&&(e.jets[1].p4.Pt()>=Jet1Pt)) {
		      hSCPt_A[j][0]->Fill(e.electrons[k].p4.Pt(), 1/Prescale);
		      hSCEta_A[j][0]->Fill(e.electrons[k].p4.Eta(), 1/Prescale);
		      hSCPt_A[j][1]->Fill(e.electrons[l].p4.Pt(), 1/Prescale);
		      hSCEta_A[j][1]->Fill(e.electrons[l].p4.Eta(), 1/Prescale);
		      hSCPU_A[j][0]->Fill(e.vertices.size(), 1/Prescale);
		      int flag_ele = 0;
		      for (size_t i_t = 0; i_t < HLTBits.size(); ++i_t) {
	                   string Trigger = "";
	                   if ((HLTBits[i_t]==1)&&
		               (i_t<hlt.names.size())) {
	                       //cout << i_t << " \t" << HLTBits[i_t]<< " \t" << hlt.names[i_t] << " \t"<< hlt.prescales[i_t]<< endl;
		                for (unsigned int h_s = 0; h_s < (hlt.names[i_t].length()-1); ++h_s) {
		                Trigger += hlt.names[i_t][h_s];
		                } 
		           } 	 
	                   if ((Trigger=="HLT_DoubleEle33_CaloIdL_v")||
		               (Trigger=="HLT_DoubleEle33_v")||
		               (Trigger=="HLT_DoublePhoton33_v")||
		               (Trigger=="HLT_DoubleEle33_CaloIdT_v")||
			           (Trigger=="HLT_DoubleEle33_CaloIdL_CaloIsoT_v")  ||
			           (Trigger=="HLT_Ele32_CaloIdT_CaloIsoT_TrkIdT_TrkIsoT_SC17_v")||
			           (Trigger=="HLT_Ele32_CaloIdT_CaloIsoT_TrkIdT_TrkIsoT_Ele17_v")||
			           (Trigger=="HLT_Ele32_CaloIdL_CaloIsoVL_SC17_v")){  
		                flag_ele=1;
                           }						   
	              } 
		      if (flag_ele == 1) {
		          hEPt_A[j][0]->Fill(e.electrons[k].p4.Pt(), 1/Prescale); 
		          hEEta_A[j][0]->Fill(e.electrons[k].p4.Eta(), 1/Prescale); 
			      hEPt_A[j][1]->Fill(e.electrons[l].p4.Pt(), 1/Prescale); 
		          hEEta_A[j][1]->Fill(e.electrons[l].p4.Eta(), 1/Prescale); 
		          hEPU_A[j][0]->Fill(e.vertices.size(), 1/Prescale);
		     } 
	         }
	      }
	  }
      }  
  }
  
  		    
  for (int i = 0; i <= 1; ++i) {
       for (int j = 0; j <= 2; ++j) { 
       TH1* hnew_A = hEPt_A[i][j]->Rebin(10,"hnew",xbins);
       TH1* hnew1_A = hSCPt_A[i][j]->Rebin(10,"hnew1",xbins);
       hnew_A -> Divide(hnew1_A);
       hEffPt_A[i][j] = (TH1F*) hnew_A->Clone();
       TH1* hnew_B = hEPt_B[i][j]->Rebin(10,"hnew",xbins);
       TH1* hnew1_B = hSCPt_B[i][j]->Rebin(10,"hnew1",xbins);
       hnew_B -> Divide(hnew1_B);
       hEffPt_B[i][j] = (TH1F*) hnew_B->Clone();	   
       TH1F* hnew2_A = (TH1F*) hEEta_A[i][j]->Clone();
       TH1F* hnew3_A = (TH1F*) hSCEta_A[i][j]->Clone();
       hnew2_A -> Divide(hnew3_A);
       hEffEta_A[i][j] = (TH1F*) hnew2_A->Clone();   
       TH1F* hnew2_B = (TH1F*) hEEta_B[i][j]->Clone();
       TH1F* hnew3_B = (TH1F*) hSCEta_B[i][j]->Clone();
       hnew2_B -> Divide(hnew3_B);
       hEffEta_B[i][j] = (TH1F*) hnew2_B->Clone();  	   
       TH1F* hnew4_A = (TH1F*) hEPU_A[i][j]->Clone();
       TH1F* hnew5_A = (TH1F*) hSCPU_A[i][j]->Clone(); 
       hnew4_A -> Divide(hnew5_A); 
       hEffPU_A[i][j] = (TH1F*) hnew4_A->Clone();   
	   TH1F* hnew4_B = (TH1F*) hEPU_B[i][j]->Clone();
       TH1F* hnew5_B = (TH1F*) hSCPU_B[i][j]->Clone(); 
       hnew4_B -> Divide(hnew5_B); 
       hEffPU_B[i][j] = (TH1F*) hnew4_B->Clone(); 
       }
  }
  
    TFile f1("Trigeff.root", "RECREATE");
    hEffPt_A[0][0]->Clone("Eff_Pt_A")->Write();
    hEffPt_A[1][0]->Clone("Eff_Pt_B")->Write();
    hEffEta_A[0][0]->Clone("Eff_Eta_A")->Write();
    hEffEta_A[1][0]->Clone("Eff_Eta_B")->Write();
    hEffPU_A[0][0]->Clone("Eff_PU_A")->Write();
    hEffPU_A[1][0]->Clone("Eff_PU_B")->Write();    
    f1.Close();
  
   TCanvas c("Trigout","Trigout");
   c.Print("Trigout.pdf[");
 
    
     hEffPt_A[0][0]->SetLineColor(1);
     hEffPt_A[0][0]->SetLineWidth(3); 
  
     hEffPt_A[1][0]->SetLineColor(2);
     hEffPt_A[1][0]->SetLineWidth(3); 
	 
     hEffPt_A[0][0]->Draw("E1");	 
     hEffPt_A[1][0]->Draw("sameE1");

     hEffPt_A[0][0]->GetYaxis()->SetRangeUser(0.8, 1.2);
     c.Modified();
  
     c.Print("Trigout.pdf");
     
     hEffPt_A[0][1]->SetLineColor(1);
     hEffPt_A[0][1]->SetLineWidth(3); 
  
     hEffPt_A[1][1]->SetLineColor(2);
     hEffPt_A[1][1]->SetLineWidth(3); 
	 
     hEffPt_A[0][1]->Draw("E1");	 
     hEffPt_A[1][1]->Draw("sameE1");

     hEffPt_A[0][1]->GetYaxis()->SetRangeUser(0.8, 1.2);
     c.Modified();
  
     c.Print("Trigout.pdf");
     
     hEffEta_A[0][0]->SetLineColor(1);
     hEffEta_A[0][0]->SetLineWidth(3); 
  
     hEffEta_A[1][0]->SetLineColor(2);
     hEffEta_A[1][0]->SetLineWidth(3); 

     hEffEta_A[0][0]->Draw("E1"); 
     hEffEta_A[1][0]->Draw("sameE1");

     hEffEta_A[0][0]->GetYaxis()->SetRangeUser(0.9, 1.1);
     c.Modified();
  
     c.Print("Trigout.pdf");
     
     hEffEta_A[0][1]->SetLineColor(1);
     hEffEta_A[0][1]->SetLineWidth(3); 
  
     hEffEta_A[1][1]->SetLineColor(2);
     hEffEta_A[1][1]->SetLineWidth(3); 

     hEffEta_A[0][1]->Draw("E1"); 
     hEffEta_A[1][1]->Draw("sameE1");

     hEffEta_A[0][1]->GetYaxis()->SetRangeUser(0.9, 1.1);
     c.Modified();
  
     c.Print("Trigout.pdf");
     
     hEffPU_A[0][0]->SetLineColor(1);
     hEffPU_A[0][0]->SetLineWidth(3); 
  
     hEffPU_A[1][0]->SetLineColor(2);
     hEffPU_A[1][0]->SetLineWidth(3); 
     
     hEffPU_A[0][0]->Draw("E1"); 
     hEffPU_A[1][0]->Draw("sameE1");

     hEffPU_A[0][0]->GetYaxis()->SetRangeUser(0.8, 1.2);
     c.Modified();
     c.Print("Trigout.pdf");
     
     
     cout<<0<<"A, Data, 1 electron    "<< hEPt_A[0][0]->Integral()<<"    "<< hSCPt_A[0][0]->Integral()<< "    " << (hEPt_A[0][0]->Integral())/(hSCPt_A[0][0]->Integral())	  
     <<sqrt(1./(hEPt_A[0][0]->Integral())+1./(hSCPt_A[0][0]->Integral()))<<endl;
 
     cout<<0<<"A, Data  2 electron   "<< hEPt_A[0][1]->Integral()<<"    "<< hSCPt_A[0][1]->Integral()<< "    " << (hEPt_A[0][1]->Integral())/(hSCPt_A[0][1]->Integral())	  
     <<sqrt(1./(hEPt_A[0][1]->Integral())+1./(hSCPt_A[0][1]->Integral()))<<endl;
     
     cout<<1<<"B, Data, 1 electron    "<< hEPt_A[1][0]->Integral()<<"    "<< hSCPt_A[1][0]->Integral()<< "    " << (hEPt_A[1][0]->Integral())/(hSCPt_A[1][0]->Integral())	  
     <<sqrt(1./(hEPt_A[1][0]->Integral())+1./(hSCPt_A[1][0]->Integral()))<<endl;
 
     cout<<1<<"B, Data  2 electron   "<< hEPt_A[1][1]->Integral()<<"    "<< hSCPt_A[1][1]->Integral()<< "    " << (hEPt_A[1][1]->Integral())/(hSCPt_A[1][1]->Integral())	  
     <<sqrt(1./(hEPt_A[1][1]->Integral())+1./(hSCPt_A[1][1]->Integral()))<<endl;
 
      
    c.Print("Trigout.pdf]");
  
  

}
