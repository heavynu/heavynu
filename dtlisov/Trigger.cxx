//To run this script on pcrfcms14.cern.ch machine: 
//root -b -q -l Trigger.cxx+
#include "TH1.h"
#include "TF1.h"
#include "TCanvas.h"
#include "TROOT.h"
#include <TLorentzVector.h>
#include <TMath.h>
#include <TLatex.h>
#include "TStyle.h"
#include "TLegend.h"
#include "TText.h"
#include "TAxis.h"
#include "TGraph.h"
#include "../../PhysicsTools/Utilities/interface/LumiReweightingStandAlone.h"
//#include "../../PhysicsTools/Utilities/interface/Lumi3DReWeighting.h"

#include "tdrstyle.C"
//#include "heavynuStandard.C"
#include "heavynuStandard11.C"

#include "../PATDump/src/format.h"
#include <iostream>
#include <fstream>
#include <string>
using namespace std;

vector<double> generate_flat10_weights(TH1D* data_npu_estimated){
    // see SimGeneral/MixingModule/python/mix_E7TeV_FlatDist10_2011EarlyData_inTimeOnly_cfi.py; copy and paste from there:
    const double npu_probs[25] = {    
    0.104109,
    0.0703573,
    0.0698445,
    0.0698254,
    0.0697054,
    0.0697907,
    0.0696751,
    0.0694486,
    0.0680332,
    0.0651044,
    0.0598036,
    0.0527395,
    0.0439513,
    0.0352202,
    0.0266714,
    0.019411,
    0.0133974,
    0.00898536,
    0.0057516,
    0.00351493,
    0.00212087,
    0.00122891,
    0.00070592,
    0.000384744,
    0.000219377};
    vector<double> result(25);
    double s = 0.0;
    for(int npu=0; npu<25; ++npu){
        double npu_estimated = data_npu_estimated->GetBinContent(data_npu_estimated->GetXaxis()->FindBin(npu));                              
        result[npu] = npu_estimated / npu_probs[npu];
        s += npu_estimated;
    }
    // normalize weights such that the total sum of weights over thw whole sample is 1.0, i.e., sum_i  result[i] * npu_probs[i] should be 1.0 (!)
    for(int npu=0; npu<25; ++npu){
        result[npu] /= s;
    }
    return result;
}

void Trigger()
{
// set plain style
  gROOT->SetStyle("Plain");
  TCanvas c1("OUTPUT1","OUTPUT1");
  setTDRStyle();
  fixOverlay(); 
 
  TH1::SetDefaultSumw2(kTRUE);
  
  TFile* f_A1 = new TFile("PileUp/2011A_pileup.root");
  TFile* f_B1 = new TFile("PileUp/2011B_pileup.root");
  TH1D* histo_A1 = (TH1D*) f_A1->Get("pileup");
  TH1D* histo_B1 = (TH1D*) f_B1->Get("pileup");
  
  TFile* f_A = new TFile("PileUp/2011A_pileupTruth.root");
  TFile* f_B = new TFile("PileUp/2011B_pileupTruth.root");
  TH1D* histo_A = (TH1D*) f_A->Get("pileup");
  TH1D* histo_B = (TH1D*) f_B->Get("pileup");
     
  Double_t PoissonOneXDist[35] = {
    1.45346E-01,
    6.42802E-02,
    6.95255E-02,
    6.96747E-02,
    6.92955E-02,
    6.84997E-02,
    6.69528E-02,
    6.45515E-02,
    6.09865E-02,
    5.63323E-02,
    5.07322E-02,
    4.44681E-02,
    3.79205E-02,
    3.15131E-02,
    2.54220E-02,
    2.00184E-02,
    1.53776E-02,
    1.15387E-02,
    8.47608E-03,
    6.08715E-03,
    4.28255E-03,
    2.97185E-03,
    2.01918E-03,
    1.34490E-03,
    8.81587E-04,
    5.69954E-04,
    3.61493E-04,
    2.28692E-04,
    1.40791E-04,
    8.44606E-05,
    5.10204E-05,
    3.07802E-05,
    1.81401E-05,
    1.00201E-05,
    5.80004E-06
  };
  
  std::vector< float > DataPileUp_A ;
  std::vector< float > DataPileUp_B ;
  std::vector< float > MCPileUp ;
  for( int i=0; i<35; ++i) {
      DataPileUp_A.push_back(histo_A->GetBinContent(i+1));
      DataPileUp_B.push_back(histo_B->GetBinContent(i+1));
      MCPileUp.push_back(PoissonOneXDist[i]);
    }
    
  reweight::LumiReWeighting LumiWeights_A = reweight::LumiReWeighting(MCPileUp, DataPileUp_A); 
  LumiWeights_A.weight3D_init("Weight3D_2011A.root");   
  reweight::LumiReWeighting LumiWeights_B = reweight::LumiReWeighting(MCPileUp, DataPileUp_B);  
  LumiWeights_B.weight3D_init("Weight3D_2011B.root"); 
  Double_t Corr[35] = { 
    0.,
    0.887809,
    0.889615,
    0.902631,
    0.884461,
    0.88004,
    0.865174,
    0.870073,
    0.873842,
    0.8978,
    0.92261,
    0.988201,
    1.05329,
    1.13407,
    1.2587,
    1.39528,
    1.55154,
    1.77817,
    2.02186,
    2.43142,
    2.83008,
    3.47422,
    4.6109,
    5.55045,
    5.88748,
    7.18155,
    10.9544,
    12.0279,
    13.5533,
    11.3418,
    28.946,
    14.0245,
    35.8912,
    9.13846,
    10.4667
    };

// Cuts for event selection
//  unsigned int NofLept = 2;
  unsigned int NofJets = 0;
  float Lep1Pt = 40.; 
  float Lep2Pt = 40.;
  float Jet1Pt = 40.;
  int HeepId1 = 0;
  int HeepId2 = 0;
  float ISO1 = 1.;
  float ISO2 = 1.;
  float EtaCutE_min = 0; //1.44
  float EtaCutE_max = 2.5;//2.5
  float EtaCutE2_min = 0.; //1.44
  float EtaCutE2_max = 1.44;//2.5  
  float MllCut =  80.0;
  float MllCut1 = 100.0;

// Mll histogramm parameters  
  float M_Delta_Mll = 60;
  float M_min_Mll = 80;
  float M_max_Mll = 110;
  
// Pt histogramm parameters  
  int M_Delta_Pt = 40;
  float M_min_Pt = 0;
  float M_max_Pt = 400;

  float Mll; 

  char *str = (char*)alloca(10000);
  char *str1 = (char*)alloca(10000);

  TH1F* hMll_A[10][3];
  TH1F* hMll_B[10][3];
  TH1F* hEPt_A[10][3];  
  TH1F* hEPt_B[10][3]; 
  TH1F* hSCPt_A[10][3];
  TH1F* hSCPt_B[10][3];
  TH1F* hEffPt_A[10][3];
  TH1F* hEffPt_B[10][3];
  TH1F* hEEta_A[10][3];  
  TH1F* hEEta_B[10][3];  
  TH1F* hSCEta_A[10][3];
  TH1F* hSCEta_B[10][3];
  TH1F* hEffEta_A[10][3];
  TH1F* hEffEta_B[10][3];
  TH1F* hEPU_A[10][3]; 
  TH1F* hEPU_B[10][3]; 
  TH1F* hSCPU_A[10][3];
  TH1F* hSCPU_B[10][3];
  TH1F* hEffPU_A[10][3];  
  TH1F* hEffPU_B[10][3];
  TH1F* hPU_A[10];
  TH1F* hPU_B[10];
  Double_t xbins[11] = {0,10,20,30,40,50,60,80,100,200,400};
  
  
  for (int i = 0; i <= 9; ++i) {
       for (int j = 0; j <= 2; ++j) {   
            sprintf(str,"Mll_A_%d_%d", i,j);
            sprintf(str1,"Mll_A; M(ee), GeV;Events/%f GeV", (M_max_Mll-M_min_Mll)/M_Delta_Mll );
            hMll_A[i][j] = new TH1F(str,str1, M_Delta_Mll, M_min_Mll, M_max_Mll);
			sprintf(str,"Mll_B_%d_%d", i,j);
            sprintf(str1,"Mll_B; M(ee), GeV;Events/%f GeV", (M_max_Mll-M_min_Mll)/M_Delta_Mll );
            hMll_B[i][j] = new TH1F(str,str1, M_Delta_Mll, M_min_Mll, M_max_Mll);
            sprintf(str,"SCPt_A_%d_%d", i, j);
	        sprintf(str1,"SCPt_A; P_{t}, GeV;Efficiency");  
            hSCPt_A[i][j] = new TH1F(str,str1, M_Delta_Pt, M_min_Pt, M_max_Pt);
			sprintf(str,"SCPt_B_%d_%d", i, j);
	        sprintf(str1,"SCPt_B; P_{t}, GeV;Efficiency");  
            hSCPt_B[i][j] = new TH1F(str,str1, M_Delta_Pt, M_min_Pt, M_max_Pt);
            sprintf(str,"EPt_A_%d_%d", i, j); 
	        sprintf(str1,"EPt_A; P_{t}, GeV;Efficiency" );  
            hEPt_A[i][j] = new TH1F(str,str1, M_Delta_Pt, M_min_Pt, M_max_Pt);
			sprintf(str,"EPt_B_%d_%d", i, j); 
	        sprintf(str1,"EPt_B; P_{t}, GeV;Efficiency" );  
            hEPt_B[i][j] = new TH1F(str,str1, M_Delta_Pt, M_min_Pt, M_max_Pt);
	        sprintf(str,"SCEta_A_%d_%d", i, j);   
            hSCEta_A[i][j] = new TH1F(str,"SCEta_A; #eta; Efficiency", 30, 0, 3);
			sprintf(str,"SCEta_B_%d_%d", i, j);   
            hSCEta_B[i][j] = new TH1F(str,"SCEta_B; #eta; Efficiency", 30, 0, 3);
            sprintf(str,"EEta_A_%d_%d", i, j);
            hEEta_A[i][j] = new TH1F(str,"EEta_A; #eta; Efficiency", 30, 0, 3); 
			sprintf(str,"EEta_B_%d_%d", i, j);
            hEEta_B[i][j] = new TH1F(str,"EEta_B; #eta; Efficiency", 30, 0, 3); 
	        sprintf(str,"SCPU_A_%d_%d", i, j);   
            hSCPU_A[i][j] = new TH1F(str,"SCPU_A; Primary vertex number;Efficiency", 25, 0, 25);
			sprintf(str,"SCPU_B_%d_%d", i, j);   
            hSCPU_B[i][j] = new TH1F(str,"SCPU_B; Primary vertex number;Efficiency", 25, 0, 25);
            sprintf(str,"EPU_A_%d_%d", i, j);
            hEPU_A[i][j] = new TH1F(str,"EPU_A; Primary vertex number;Efficiency", 25, 0, 25); 
            sprintf(str,"EPU_B_%d_%d", i, j);
            hEPU_B[i][j] = new TH1F(str,"EPU_B; Primary vertex number;Efficiency", 25, 0, 25);  			
       }
    sprintf(str,"PU_A_%d", i);
    hPU_A[i] = new TH1F(str, "PU_A; Primary vertex number ; Events", 25, 0, 25);
	sprintf(str,"PU_B_%d", i);
    hPU_B[i] = new TH1F(str, "PU_B; Primary vertex number ; Events", 25, 0, 25);
  }
  const char* fname[10];
  float Lum[10] = {0};
  int n[10] = {0};
  int nEvents[10] = {0};
  float Weight_A,Weight_B;

  
  
//  fname[0] = "/moscow41/heavynu/Data_2011/Electron_2011A.root";
//  fname[1] = "/moscow41/heavynu/Data_2011/DElectron_2011A.root";
 // fname[1] = "/moscow41/heavynu/Data_2011/SFoton_2011A.root";
 // fname[1] = "/moscow41/heavynu/Data_2011/Electron_B_v1.root";
//  fname[1] = "/moscow41/heavynu/Data_2011/DElectron_B_v1.root";
 // fname[1] = "/moscow41/heavynu/Data_2011/SFoton_B_v1.root"; 
//  fname[0] = "/moscow41/heavynu/Data_2011/Electron_2011A+B.root";

  fname[0] = "/moscow41/heavynu/Data_2011/Electron_2011A_e1.root";
  fname[1] = "/moscow41/heavynu/Data_2011/Electron_B_v1_e1.root";


  
  fname[2] = "/moscow31/heavynu/42x-bg/DYToLL_M-50_Sherpa_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept.root";
  fname[3] = "/moscow31/heavynu/42x-bg/TTTo2L2Nu2B_7TeV-powheg-pythia6_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept.root";
  fname[4] = "/moscow31/heavynu/42x-bg/WJetsToLNu_TuneZ2_7TeV-madgraph-tauola_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept.root";
  fname[5] = "/moscow31/heavynu/42x-bg/ZZ_TuneZ2_7TeV_pythia6_tauola_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept.root";
  fname[6] = "/moscow31/heavynu/42x-bg/WZ_TuneZ2_7TeV_pythia6_tauola_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept.root";
  fname[7] = "/moscow31/heavynu/42x-bg/WW_TuneZ2_7TeV_pythia6_tauola_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept.root";
  fname[8] = "/moscow31/heavynu/42x-bg/T_TuneZ2_tW-channel-DR_7TeV-powheg-tauola_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept_1jet.root";
  fname[9] = "/moscow31/heavynu/42x-bg/Tbar_TuneZ2_tW-channel-DR_7TeV-powheg-tauola_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept_1jet.root";
  
  for (int j = 0; j <= 1; ++j) {
      TheEvent e(fname[j]);
      if (!e) return;
      Lum[j] = e.sample.lumi; //Luminocity in pb^-1
      n[j] = 0;
      nEvents[j] = e.totalEvents();
      for (int i = 0; i < nEvents[j]; ++i) {
          e.readEvent(i);    		  
          int flag = 0; 
          //cout<< "Event "<<i<<" Run "<< e.irun<<" Event No "<< e.ievent<<endl;
	  const vector<int>& HLTBits = *e.hltBits;
	  const HLTtable& hlt = e.HLT();
	      
	  for (size_t i_t = 0; i_t < HLTBits.size(); ++i_t) {
	       string Trigger = "";
	       if ((HLTBits[i_t]==1)&&
		   (i_t<hlt.names.size())) {            
		    //cout << i_t << " \t" << HLTBits[i_t]<< " \t" << hlt.names[i_t] << endl;
		    for (unsigned int h_s = 0; h_s < (hlt.names[i_t].length()-1); ++h_s) {
		         Trigger += hlt.names[i_t][h_s];
		    } 
	        }				 
		if ((//(Trigger=="HLT_Ele65_CaloIdVT_TrkIdT_v")//||
		     //(Trigger=="HLT_Ele52_CaloIdVT_TrkIdT_v")//||
		     (Trigger=="HLT_Ele45_CaloIdVT_TrkIdT_v"))){ 
		      flag=1;							
                }						   
	  }				  
	  
	  if (flag==1) {	  
	    //  for (unsigned int k=0; k<e.electrons.size();++k) { 
	           hSCPt_A[j][0]->Fill(e.electrons[0].p4.Pt());
		   hSCEta_A[j][0]->Fill(e.electrons[0].p4.Eta());
		   hSCPU_A[j][0]->Fill(e.vertices.size());
                   int flag_ele = 0;
		   for (size_t k_t = 0; k_t < e.electrons[0].bits.size(); ++k_t) {
                        const int bit = e.electrons[0].bits[k_t];
		        if (bit<hlt.names.size()){
			    string Trigger = "";
		            for (unsigned int h_s = 0; h_s < (hlt.names[bit].length()-1); ++h_s) {
		                 Trigger += hlt.names[bit][h_s];
		            }
			    // if (Trigger=="HLT_Ele65_CaloIdVT_TrkIdT_v"){ 
			    //    if (Trigger=="HLT_DoubleEle33_CaloIdL_v"){ 
                            //    flag = 1;
                            //    cout << k << " \t" << e.electrons[k].p4.Pt() <<" \t"<< bit << " \t"<< hlt.names[bit] << " \t" <<endl;
                            if ((//(Trigger=="HLT_Ele65_CaloIdVT_TrkIdT_v")//||
			         //(Trigger=="HLT_Ele52_CaloIdVT_TrkIdT_v")//||
			          (Trigger=="HLT_Ele45_CaloIdVT_TrkIdT_v"))){ 
				  hEPt_A[j][0]->Fill(e.electrons[0].p4.Pt()); 
				  hEEta_A[j][0]->Fill(e.electrons[0].p4.Eta()); 
				  hEPU_A[j][0]->Fill(e.vertices.size());
			          flag_ele=1;
                            }
          		}		 
	            }
			//if (flag_ele ==1) {cout <<"Electron "<< k << "  Trigger good!!! Event  "<< i << "   P_t="<< e.electrons[k].p4.Pt()<< endl;}
			//else
			//{cout <<"Electron "<< k << "  No particle trigger matching!!! Event  "<< i << "   P_t="<< e.electrons[k].p4.Pt()<< endl;}			
	   //   }
	  }
      }  
  }
  		 
  
  for (int i = 0; i <= 1; ++i) {
       for (int j = 0; j <= 2; ++j) { 
       //TH1* hnew_A = hEPt_A[i][j]->Rebin(10,"hnew",xbins);
       //TH1* hnew1_A = hSCPt_A[i][j]->Rebin(10,"hnew1",xbins);
       TH1* hnew_A = (TH1F*) hEPt_A[i][j]->Clone();
       TH1* hnew1_A = (TH1F*) hSCPt_A[i][j]->Clone();         
       hnew_A -> Divide(hnew1_A);
       hEffPt_A[i][j] = (TH1F*) hnew_A->Clone();
       //TH1* hnew_B = hEPt_B[i][j]->Rebin(10,"hnew",xbins);
       //TH1* hnew1_B = hSCPt_B[i][j]->Rebin(10,"hnew1",xbins);
       TH1* hnew_B = (TH1F*) hEPt_B[i][j]->Clone();
       TH1* hnew1_B = (TH1F*) hSCPt_B[i][j]->Clone();       
       hnew_B -> Divide(hnew1_B);
       hEffPt_B[i][j] = (TH1F*) hnew_B->Clone();	   
       TH1F* hnew2_A = (TH1F*) hEEta_A[i][j]->Clone();
       TH1F* hnew3_A = (TH1F*) hSCEta_A[i][j]->Clone();
       hnew2_A -> Divide(hnew3_A);
       hEffEta_A[i][j] = (TH1F*) hnew2_A->Clone();   
       TH1F* hnew2_B = (TH1F*) hEEta_B[i][j]->Clone();
       TH1F* hnew3_B = (TH1F*) hSCEta_B[i][j]->Clone();
       hnew2_B -> Divide(hnew3_B);
       hEffEta_B[i][j] = (TH1F*) hnew2_B->Clone();  	   
       TH1F* hnew4_A = (TH1F*) hEPU_A[i][j]->Clone();
       TH1F* hnew5_A = (TH1F*) hSCPU_A[i][j]->Clone(); 
       hnew4_A -> Divide(hnew5_A); 
       hEffPU_A[i][j] = (TH1F*) hnew4_A->Clone();   
	   TH1F* hnew4_B = (TH1F*) hEPU_B[i][j]->Clone();
       TH1F* hnew5_B = (TH1F*) hSCPU_B[i][j]->Clone(); 
       hnew4_B -> Divide(hnew5_B); 
       hEffPU_B[i][j] = (TH1F*) hnew4_B->Clone(); 
       }
  }
  
  
    TFile f1("Matching.root", "RECREATE");
    hEffPt_A[0][0]->Clone("Eff_Pt_A")->Write();
    hEffPt_A[1][0]->Clone("Eff_Pt_B")->Write();
    hEffEta_A[0][0]->Clone("Eff_Eta_A")->Write();
    hEffEta_A[1][0]->Clone("Eff_Eta_B")->Write();
    hEffPU_A[0][0]->Clone("Eff_PU_A")->Write();
    hEffPU_A[1][0]->Clone("Eff_PU_B")->Write();    
    f1.Close();
    
    
  
   TCanvas c("OUTPUT","OUTPUT");
   c.Print("OUTPUT.pdf[");
 
  
 
     hEffPt_A[0][0]->SetLineColor(1);
     hEffPt_A[0][0]->SetLineWidth(3); 
  
     hEffPt_A[1][0]->SetLineColor(2);
     hEffPt_A[1][0]->SetLineWidth(3); 
	 
     hEffPt_A[0][0]->Draw("E1");	 
     hEffPt_A[1][0]->Draw("sameE1");

     hEffPt_A[0][0]->GetYaxis()->SetRangeUser(0, 1);
     c.Modified();
  
     c.Print("OUTPUT.pdf");
     
     hEffEta_A[0][0]->SetLineColor(1);
     hEffEta_A[0][0]->SetLineWidth(3); 
  
     hEffEta_A[1][0]->SetLineColor(2);
     hEffEta_A[1][0]->SetLineWidth(3); 

     hEffEta_A[0][0]->Draw("E1"); 
     hEffEta_A[1][0]->Draw("sameE1");

     hEffEta_A[0][0]->GetYaxis()->SetRangeUser(0, 1);
     c.Modified();
  
     c.Print("OUTPUT.pdf");
     
     hEffPU_A[0][0]->SetLineColor(1);
     hEffPU_A[0][0]->SetLineWidth(3); 
  
     hEffPU_A[1][0]->SetLineColor(2);
     hEffPU_A[1][0]->SetLineWidth(3); 
     
     hEffPU_A[0][0]->Draw("E1"); 
     hEffPU_A[1][0]->Draw("sameE1");

     hEffPU_A[0][0]->GetYaxis()->SetRangeUser(0, 1);
     c.Modified();
     c.Print("OUTPUT.pdf");
     
     cout<<"A, Data    "<< hEPt_A[0][0]->Integral()<<"    "<< hSCPt_A[0][0]->Integral()<< "    " << (hEPt_A[0][0]->Integral())/(hSCPt_A[0][0]->Integral())<<endl;	   
     cout<<"Error = "<<sqrt(1./(hEPt_A[0][0]->Integral())+1./(hSCPt_A[0][0]->Integral()))<<endl;
 
     cout<<"B, Data    "<< hEPt_A[1][0]->Integral()<<"    "<< hSCPt_A[1][0]->Integral()<< "    " << (hEPt_A[1][0]->Integral())/(hSCPt_A[1][0]->Integral())<<endl;
     cout<<"Error = "<<sqrt(1./(hEPt_A[1][0]->Integral())+1./(hSCPt_A[1][0]->Integral()))<<endl;
     
     
    c.Print("OUTPUT.pdf]");

}
