#include "TH1.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TObject.h"
#include "TLorentzVector.h"
#include "TVector3.h"

#include "PATDump/src/format.h"
#include <iostream>
using namespace std;

void controldataMuMu()
{ 
  const char* fname = "/moscow31/heavynu/data/2010A_2010B_Mu_Nov4_ReReco.root";

  TheEvent e(fname);
  if (!e) return;
  
  const int nEvents = e.totalEvents();
  cout << "Total events = " << nEvents << endl;
  
  // HLT bits names:
  const int nBits = e.triggerNames.size();
  cout << "Total bits = " << nBits << endl;
  
  // set plain style
  gROOT->SetStyle("Plain");
  
  // histograms
  TH1F hHLT("HLTbits", "HLT efficiency;#bit;%", nBits, 0, nBits);
  TH1F hWR(  "WR", "W_{R} reconstructed mass;mass, GeV/c^{2};events/20 GeV", 100, 0, 2000);
  TH1F hNuR0("NuR0", "nu_{R} reconstructed mass using first E muon;mass, GeV/c^{2};events/10 GeV", 100, 0, 1000);
  TH1F hNuR1("NuR1", "nu_{R} reconstructed mass using second E energized muon;mass, GeV/c^{2};events/10 GeV", 100, 0, 1000);
  
  TH1F hMll("Mll", "muon-muon pair invariant mass;mass, GeV/c^{2};events/10 GeV", 100, 40, 140);
  TH1F hMjj("Mjj", "jet-jet pair invariant mass;mass, GeV/c^{2};events/10 GeV", 100, 0, 1000);
  TH1F hElRes("ElRes", "muon energy reconstruction resolution", 100, -0.3, 0.3);
  TH1F hElMult("ElMult", "muons multiplicity", 10, 0, 10);
  hElMult.GetXaxis()->SetNdivisions(110);
  TH1F hJetMult("JetMult", "jets multiplicity", 20, 0, 20);
  hJetMult.GetXaxis()->SetNdivisions(210);
  TH1F PtWR(  "PtWR", "W_{R} Pt; Pt, GeV/c^{2}; events/2 GeV", 100, 0, 200);
  TH1F PlWR(  "PlWR", "W_{R} Pl;Pl, GeV/c^{2};events/25 GeV", 100, 0, 2500);
  TH1F EtaWR( "EtaWR", "W_{R} Eta; Eta; events", 30, -6, 6);
  TH1F PhiWR( "PhiWR", "W_{R} Phi; Phi; events", 20, -3.1415, 3.1415);
  
  int n = 0;
  int nB1 = 0; 
  int nB2 = 0;  
  int nB3 = 0;
  
  int nQ = 0;
  int nQ1 = 0;
  double LUMI = 0.0;
  
  for (int i = 0; i < nEvents; ++i) {
    e.readEvent(i);
    
    // HLT bits distribution
    for (int j = 0; j < nBits; ++j)
      if ((*e.hltBits)[j])
        hHLT.Fill(j);
    
    // electron energy resolution
    for (unsigned int j = 0; j < e.muons.size(); ++j) {
      const Particle& reco = e.muons[j];
      if (reco.mcId >= 0) {
        const Particle& mc = e.gens[reco.mcId];
        const float r = (reco.p4.E() - mc.p4.E())/mc.p4.E();
        hElRes.Fill(r);
      }
    }
    
    // mass distributions
   //     if ((e.muons.size() >= 1) && (e.jets.size() >= 1)) {
	
	//cuts on muons Pt>=40
	
	//    if (((e.muons[0].p4).Pt()>=20) && ((e.muons[1].p4).Pt()>=0))  { 
    //cuts on jetss Pt>=40      
	//    if (((e.jets[0].p4).Pt()>=20) && ((e.jets[1].p4).Pt()>=0))  { 
	//cuts on muons invariant mass Mll>=180.0	
	//	  if ((e.muons[0].p4 + e.muons[1].p4).M()>=200.0) {
	//cuts on Eta  
	//	   if ((abs(e.muons[0].p4.Eta())<=3.0) && (abs(e.muons[1].p4.Eta())<=3.0) 
	//	         && (abs(e.jets[0].p4.Eta())<=3.0) && (abs(e.jets[1].p4.Eta())<=3.0) )        {
	// Isolation
	//    if ((0.75*e.muons[0].ecalIso + 0.75*e.muons[0].hcalIso + e.muons[0].trackIso)/(e.muons[0].p4.Pt() <= 0.05)&&
    //        (0.75*e.muons[1].ecalIso + 0.75*e.muons[1].hcalIso + e.muons[1].trackIso)/(e.muons[1].p4.Pt() <= 0.05)) {
	// Cut on vertexes
    //        if ((abs(e.muons[0].vertex.z()-e.muons[1].vertex.z())<=0.02) && (abs(e.jets[0].vertex.z()-e.jets[1].vertex.z())<=0.02) 
   //          && (abs(abs(e.muons[0].vertex.z()-e.muons[1].vertex.z())-abs(e.jets[0].vertex.z()-e.jets[1].vertex.z()))<=0.02)) {
	   
      LUMI = 0.0 ;
	 // LUMI = (32964468.942+3180532.050)/1000000.;
	  	  
	  //LUMI = LUMI + (4973950.528)/1000000.;
	  //  if (e.irun<=149181) {
	  	  
      //LUMI = LUMI + (391755.240+ 604993.562+ 2454242.751+ 190179.491+ 290928.847)/1000000.;
	  //  if (e.irun<=149063) {
	    
	  //LUMI = LUMI + (2114190.211+ 1636848.589+ 793575.245)/1000000.;
	  //    if (e.irun<=148952) {
	  
	  //LUMI = LUMI + (950013.047+ 216311.823+ 219753.667+ 1096016.253+ 650587.470+ 107338.259)/1000000.;
	  //    if (e.irun<=148860) {
	  
	  //LUMI = LUMI + (446518.043+ 937981.139+ 254404.376+ 879561.939+ 323516.558+ 872764.458)/1000000.;
	  //    if (e.irun<= 148029) {
	  
	  //LUMI = LUMI + (1054855.831+ 167892.281+ 233007.336+ 67754.467+ 218624.969+ 126964.310+ 683515.893+ 219110.476)/1000000.;
	  //    if (e.irun<= 147755) {
	  
	  //LUMI = LUMI + (461879.570+ 481275.649+ 29465.313+ 135377.274+ 89278.344+ 69158.433+ 206765.795+ 45980.618+ 296519.068+ 371486.516+ 383817.309)/1000000.;
	  //  if (e.irun<= 147284) {
	  
	  //LUMI = LUMI + (216421.987+ 64821.866 +1051881.804 + 21423.391+  94766.225+ 42110.083+ 151109.454 +70253.420 + 2285.366+73549.147 + 560556.097+ 194015.961+ 481088.796+ 251256.184+ 312050.409)/1000000.;
	  //  if (e.irun<= 147048 ) {
	  
	  LUMI = LUMI + (7395.316+ 7129.284+ 1980.971+ 33426.223+ 53889.042+ 260980.745+ 3726.779)/1000000.;
	    if (e.irun<= 146513 ) {
		
	  LUMI = LUMI + (3180532.050)/1000000.;
	  //  if (e.irun<= 144114 ) {
	
 /*	
	//Consruct P4 of WR, leptons ans jets in Laboratory frame	 
		    TLorentzVector P4L1,P4L2,P4J1,P4J2;
			P4L1.SetPxPyPzE((e.muons[0].p4).Px(), (e.muons[0].p4).Py(), (e.muons[0].p4).Pz(), (e.muons[0].p4).E());
	        P4L2.SetPxPyPzE((e.muons[1].p4).Px(), (e.muons[1].p4).Py(), (e.muons[1].p4).Pz(), (e.muons[1].p4).E());
		    P4J1.SetPxPyPzE((e.jets[0].p4).Px(), (e.jets[0].p4).Py(), (e.jets[0].p4).Pz(), (e.jets[0].p4).E());
			P4J2.SetPxPyPzE((e.jets[1].p4).Px(), (e.jets[1].p4).Py(), (e.jets[1].p4).Pz(), (e.jets[1].p4).E());
			TLorentzVector P4WR = P4L1 + P4L2 + P4J1 + P4J2;
    //Two possibility for constraction P4 of NuR	        
			TLorentzVector P4NuR_v1 = P4L1 + P4J1 + P4J2;
            TLorentzVector P4NuR_v2 = P4L2 + P4J1 + P4J2;
	//Construct WR mass from 2 leptons and 2 jets	
            const float WR = P4WR.M();
            hWR.Fill(WR, e.weight);
	//Construct WR Pt from 2 leptons and 2 jets  
	        const float tWR = P4WR.Pt();
            PtWR.Fill(tWR, e.weight);
	//Construct WR Pl from 2 leptons and 2 jets  
	        const float lWR = P4WR.Pz();
            PlWR.Fill(lWR, e.weight);
	//Construct WR Eta from 2 leptons and 2 jets  
	        const float eWR = P4WR.Eta();
            EtaWR.Fill(eWR, e.weight);
	//Construct WR Phi from 2 leptons and 2 jets   
	        const float pWR = P4WR.Phi();
            PhiWR.Fill(pWR, e.weight);
	//Construct Boost Vector for bosting to WR rest frame   
	        TVector3 P3WR = -1.0*P4WR.BoostVector();
	//Consruct P4 of WR, NuR, leptons ans jets in WR rest frame						
			P4WR.Boost(P3WR);
            P4L1.Boost(P3WR);			
            P4L2.Boost(P3WR);
			P4J1.Boost(P3WR);
            P4J2.Boost(P3WR);
			P4NuR_v1.Boost(P3WR);
			P4NuR_v2.Boost(P3WR);
			
	// Choose lepton with the next to highest energy in WR rest frame for constructing right NuR mass (consequence of our process kinematic)		
            const float NuR_l1 = P4NuR_v1.M();
			const float NuR_l2 = P4NuR_v2.M();
			float NuR_l0 = 0.0;
	//		if (P4L1.P()>=P4L2.P()) {
			      hNuR0.Fill(NuR_l1, e.weight);
				  hNuR1.Fill(NuR_l2, e.weight);
				  NuR_l0 = NuR_l2;
	//			  }
	//	    else {
	//		      hNuR0.Fill(NuR_l2, e.weight);
	//			  hNuR1.Fill(NuR_l1, e.weight);
	//		      NuR_l0 = NuR_l1;
	//			  }
		    
			
			
			const float WR2=P4L1.E()-P4L2.E();
			      
            const float Mll = (e.muons[0].p4 + e.muons[1].p4).M();
            hMll.Fill(Mll, e.weight);
      
            const float Mjj = (e.jets[0].p4 + e.jets[1].p4).M();
            hMjj.Fill(Mjj, e.weight);
			*/
			n = n + 1;
			/*
			if ((e.jets[0].bDiscr >= 0.215) && (e.jets[1].bDiscr >= 0.215)) { //  10% = 0.215      1% = 0.459    0.1% = 0.669
			nB2 = nB2 + 1;
			}
			if (((e.jets[0].bDiscr >= 0.215) && (e.jets[1].bDiscr <= 0.215)) || ((e.jets[0].bDiscr <= 0.215) && (e.jets[1].bDiscr >= 0.215)) ) { //  10% = 0.215      1% = 0.459    0.1% = 0.669
			nB1 = nB1 + 1;
			}
			if ((e.jets[0].bDiscr <= 0.215) && (e.jets[1].bDiscr <= 0.215)) { 
			nB3 = nB3 + 1;
			}
			
			if (e.muons[0].charge*e.muons[1].charge == -1) { 
			nQ = nQ +1; }
			else {nQ1 = nQ1 +1;}
			
			cout<<"New event!" << n <<" PtL1 = " <<(e.muons[0].p4).Pt()<<" PtL2 = "<<(e.muons[1].p4).Pt() << " Mll = "<<(e.muons[0].p4 + e.muons[1].p4).M() << endl;
			
			cout<<" PtJ1 = "<<(e.jets[0].p4).Pt()<<" bDiscr J1 = "<< e.jets[0].bDiscr <<" PtJ2 = "<<(e.jets[1].p4).Pt()<<" bDiscr J2 = "<< e.jets[1].bDiscr <<endl;
			cout<<"EtaL1 = "<<(e.muons[0].p4).Eta()<<" PhiL1 = "<<(e.muons[0].p4).Phi()<<" EtaL2 = "<<(e.muons[1].p4).Eta()<<" PhiL2 = "<<(e.muons[1].p4).Phi() << endl;
			cout<<"EtaJ1 = "<<(e.jets[0].p4).Eta()<<" PhiJ1 = "<<(e.jets[0].p4).Phi()<<" EtaJ2 = "<<(e.jets[1].p4).Eta()<<" PhiJ2 = "<<(e.jets[1].p4).Phi() << endl;			
			
	        cout<<"vzL1 = "<<(e.muons[0].vertex).z()<<" VzL2 = "<<(e.muons[1].vertex).z()<<" VzJ1 = "<<(e.jets[0].vertex).z()<<" VzJ2 = "<<(e.jets[1].vertex).z() << endl;
			
	    	cout<<" L1: ecalIso = " <<e.muons[0].ecalIso <<" hcalIso = "<<e.muons[0].hcalIso <<" hcal1Iso = "<<e.muons[0].hcal1Iso << " trackIso = "<< e.muons[0].trackIso << endl; 
			 		
			cout<<" L1: charge = " <<e.muons[0].charge << endl; 
						
			cout<<" L2: ecalIso = " <<e.muons[1].ecalIso <<" hcalIso = "<<e.muons[1].hcalIso << " hcal1Iso = "<<e.muons[1].hcal1Iso <<" trackIso = "<< e.muons[1].trackIso << endl; 
					
			cout<<" L2: charge = " <<e.muons[1].charge << endl;
							
			// cout<<"MWR = "<< WR <<" MNuR = "<< NuR_l1 <<" Mjj = "<< Mjj << endl;
			cout <<"RUN = "<< e.irun << " LUMI = " << e.ilumi << " EVENT = "<< e.ievent<< endl;
		    
			cout << "Number of events with 0 b-jet = " << nB3 << endl; 
			cout << "Number of events with 1 b-jet = " << nB1 << endl;
			cout << "Number of events with 2 b-jet = " << nB2 << endl;	

            cout << "Number of events with same charges of lepton = " << nQ1 << endl;
			cout << "Number of events with opposite charges of lepton = " << nQ << endl;			
      
			
			cout << endl;	
           */
			
         }
   	//	    }
	//      } 
	//      }
	//      }
   //       }
 ///	  }    
	
 ///	 }
    
    // Multiplicity
    hElMult.Fill(e.muons.size());
    hJetMult.Fill(e.jets.size());
  }
  cout << n << endl;
  cout << "LUMI = " << LUMI << "1/pb" << endl;	
  
  /*
  // print all histograms to .ps
  TCanvas c("controldataMuMu");
  c.Print("controldataMuMu.ps[");
  
  //hWR.Fit("gaus");
  hWR.Draw();     c.Print("controldataMuMu.ps");
  PtWR.Draw();     c.Print("controldataMuMu.ps");
  PlWR.Draw();     c.Print("controldataMuMu.ps");
  EtaWR.Draw();     c.Print("controldataMuMu.ps");
  PhiWR.Draw();     c.Print("controldataMuMu.ps");
  
  
  
  hNuR0.Draw();    c.Print("controldataMuMu.ps");
  hNuR1.Draw();    c.Print("controldataMuMu.ps");
  hMll.Draw();    c.Print("controldataMuMu.ps");
  hMjj.Draw();    c.Print("controldataMuMu.ps");
  
  hHLT.Scale(100.0/nEvents);
  hHLT.Draw();   c.Print("controldataMuMu.ps");
  
  //hElRes.Fit("gaus");
  hElRes.Draw(); c.Print("controldataMuMu.ps");
  
  hElMult.Draw();  c.Print("controldataMuMu.ps");
  hJetMult.Draw(); c.Print("controldataMuMu.ps");
  
  c.Print("controldataMuMu.ps]");
  */
}
