// macro for:
//  * plot signal, background and data histograms
//  * fit background by exponent
//
// to run:
//  $ root -b -q -l fit-input.cxx+
//
// NOTE: use recent root version
//       fit do not converge in root 5.22 (CMSSW 38x)

#include "TFile.h"
#include "TH1F.h"
#include "TCanvas.h"
#include "TROOT.h"
#include "TStyle.h"
#include "THStack.h"
#include "TLegend.h"
#include "tdrstyle.C"
#include "heavynuStandard.C"

void fit_input()
{
  TFile* f_A = new TFile("fit-input_MW_A.root");
  TFile* f_B = new TFile("fit-input_MW_B.root");
  TH1F* Other_A = (TH1F*) f_A->Get("Other"); 
  TH1F* TTb_A = (TH1F*) f_A->Get("TTb");
  TH1F* ZJ_A = (TH1F*) f_A->Get("ZJ");  
  TH1F* Other_B = (TH1F*) f_B->Get("Other"); 
  TH1F* TTb_B = (TH1F*) f_B->Get("TTb");
  TH1F* ZJ_B = (TH1F*) f_B->Get("ZJ");
    
  Other_A->Sumw2();
  TTb_A->Sumw2();
  ZJ_A->Sumw2();
	
  Other_B->Sumw2();
  TTb_B->Sumw2();
  ZJ_B->Sumw2();

  
  gROOT->SetStyle("Plain");
  TCanvas c1("OUTPUT1","OUTPUT1");
  setTDRStyle();
  fixOverlay();
  
  gStyle->SetOptStat("11111");
  gStyle->SetOptLogy(1);
   TCanvas c("OUTPUT","OUTPUT");
   
  ZJ_A->SetLineWidth(3);
  ZJ_A->Draw("E");
  ZJ_A->Fit("expo", "S", "", 600, 2500);
//  TF1* g = sig->GetFunction("expo");

  c.Print("ZJ_fit_A_ee.pdf");
  c.Print("ZJ_fit_A_ee.png");
  c.Print("ZJ_fit_A_ee.cxx");
  
  TTb_A->SetLineWidth(3);
  TTb_A->Draw("E");
  TTb_A->Fit("expo", "S", "", 600, 2500);

  c.Print("TTb_A_fit_ee.pdf");
  c.Print("TTb_A_fit_ee.png");
  c.Print("TTb_A_fit_ee.cxx");
  
  Other_A->SetLineWidth(3);
  Other_A->Draw("E");
  Other_A->Fit("expo", "S", "", 600, 2500);

  c.Print("Other_A_fit_ee.pdf");
  c.Print("Other_A_fit_ee.png");
  c.Print("Other_A_fit_ee.cxx");
  
  ZJ_B->SetLineWidth(3);
  ZJ_B->Draw("E");
  ZJ_B->Fit("expo", "S", "", 600, 2500);
//  TF1* g = sig->GetFunction("expo");

  c.Print("ZJ_fit_B_ee.pdf");
  c.Print("ZJ_fit_B_ee.png");
  c.Print("ZJ_fit_B_ee.cxx");
  
  TTb_B->SetLineWidth(3);
  TTb_B->Draw("E");
  TTb_B->Fit("expo", "S", "", 600, 2500);

  c.Print("TTb_B_fit_ee.pdf");
  c.Print("TTb_B_fit_ee.png");
  c.Print("TTb_B_fit_ee.cxx");
  
  Other_B->SetLineWidth(3);
  Other_B->Draw("E");
  Other_B->Fit("expo", "S", "", 600, 2500);

  c.Print("Other_B_fit_ee.pdf");
  c.Print("Other_B_fit_ee.png");
  c.Print("Other_B_fit_ee.cxx");
}
