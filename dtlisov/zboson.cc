// This is a small program to demonstrate a way how to
// select events with Z-bosons.
//
// to run do:
//   $ make zboson
//   $ zboson FILENAME

// Headers:
//
// c++
#include <iostream>

// root
#include "TCanvas.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TStyle.h"
#include "TFile.h"
#include "TROOT.h"
#include "TPad.h"

// heavynu
#include "../PATDump/src/format.h"

//ReWeight

#include "LumiReweightingStandAlone.h"

using namespace std;

vector<double> generate_PileUP_weights(TH1D* data_npu_estimated){
   // For  Spring15 see https://github.com/cms-sw/cmssw/blob/CMSSW_7_4_X/SimGeneral/MixingModule/python/mix_2015_25ns_Startup_PoissonOOTPU_cfi.py copy and paste from there:
   const double npu_probs[52] = {/* 0 -->*/
                        4.8551E-07,
                        1.74806E-06,
                        3.30868E-06,
                        1.62972E-05,
                        4.95667E-05,
                        0.000606966,
                        0.003307249,
                        0.010340741,
                        0.022852296,
                        0.041948781,
                        0.058609363,
                        0.067475755,
                        0.072817826,
                        0.075931405,
                        0.076782504,
                        0.076202319,
                        0.074502547,
                        0.072355135,
                        0.069642102,
                        0.064920999,
                        0.05725576,
                        0.047289348,
                        0.036528446,
                        0.026376131,
                        0.017806872,
                        0.011249422,
                        0.006643385,
                        0.003662904,
                        0.001899681,
                        0.00095614,
                        0.00050028,
                        0.000297353,
                        0.000208717,
                        0.000165856,
                        0.000139974,
                        0.000120481,
                        0.000103826,
                        8.88868E-05,
                        7.53323E-05,
                        6.30863E-05,
                        5.21356E-05,
                        4.24754E-05,
                        3.40876E-05,
                        2.69282E-05,
                        2.09267E-05,
                        1.5989E-05,
                        4.8551E-06,
                        2.42755E-06,
                        4.8551E-07,
                        2.42755E-07,
                        1.21378E-07,
                        4.8551E-08 /* <-- 51 */};
   vector<double> result(50);
   double s = 0.0;
   for(int npu=0; npu<50; ++npu){
       double npu_estimated = data_npu_estimated->GetBinContent(data_npu_estimated->GetXaxis()->FindBin(npu));                              
       result[npu] = npu_estimated / npu_probs[npu];
       s += npu_estimated;
   }
   // normalize weights such that the total sum of weights over thw whole sample is 1.0, i.e., sum_i  result[i] * npu_probs[i] should be 1.0 (!)
   for(int npu=0; npu<50; ++npu){
       result[npu] /= s;
   }
   return result;
}


int main(int argc, char* argv[]) {

// Check that correct number of command-line arguments
  if (argc != 3) {
    cerr << " Unexpected number of command-line arguments. \n"
         << " Usage: zboson FILENAME \n" << endl;
    return 1;
  }

// prepare a picture
  gROOT->SetStyle("Plain");
  TCanvas c("zboson", "zboson", 800, 600);

// define histogrammes
  const int FileNumberTotal = 3; // N+1
  TH1::SetDefaultSumw2();
  TH1F *hMZ_A[FileNumberTotal]; //histogram mass z boson declaration
  TH1F *hMZ_B[FileNumberTotal]; //histogram mass z boson declaration
  TF1* fitf_A[FileNumberTotal]; //histogram mass z boson fit declaration 
  TF1* fitf_B[FileNumberTotal]; //histogram mass z boson fit declaration
  TH1F *hPU_A[FileNumberTotal]; //histogram  primary vertex
  TH1F *hPU_B[FileNumberTotal]; //histogram  primary vertex
  TH1F *hTrueNumInter[FileNumberTotal]; //histogram  primary vertex

  double A_A[FileNumberTotal] = {0}; // area under gaussian
  double A_B[FileNumberTotal] = {0}; // area under gaussian

  char *str = (char*)alloca(10000);
  char *str1 = (char*)alloca(10000);
  double Lum; 

// Reweighting 
  
  // Optian A:
  reweight::LumiReWeighting LumiWeights= reweight::LumiReWeighting("MyMCExamplePileupHistogram.root", //MC histo file
                                                                   "MyDataExamplePileupHistogram.root", // Data histo file
                                                                   "MC_pileup", // MC histoname
                                                                   "pileup"); // Data histoname


 // Option B:

  TFile* f_Data = new TFile("MyDataExamplePileupHistogram.root");
  TH1D* histo_Data = (TH1D*) f_Data->Get("pileup");

 // True Number of interaction 

  TFile* f_MC = new TFile("MyMCExamplePileupHistogram.root");
  TH1D* histo_MC = (TH1D*) f_MC->Get("MC_pileup");


  for (int FileNumber = 1; FileNumber < 3; ++FileNumber) {

      double Weight_A = 0.;
      double Weight_B = 0.;
//book histogrammes
      sprintf(str,"hMZ_A_%d", FileNumber);
      hMZ_A[FileNumber] = new TH1F(str, "M_{ee} mass distribution;M_{ee}, GeV/c^{2};#events", 100, 20, 180); //histogram mass z boson declaration
      sprintf(str,"hMZ_B_%d", FileNumber);
      hMZ_B[FileNumber] = new TH1F(str, "M_{ee} mass distribution;M_{ee}, GeV/c^{2};#events", 100, 20, 180); //histogram mass z boson declaration
      sprintf(str,"fitf_A_%d", FileNumber);
      fitf_A[FileNumber] = new TF1(str, "[0]*exp(-[1]*x) + [2]*exp(-([3] - x)*([3] - x)/(2*[4]*[4]))", 0, 200);//histogram mass z boson fit declaration
      sprintf(str,"fitf_B_%d", FileNumber);
      sprintf(str,"hPU_A_%d", FileNumber);
      hPU_A[FileNumber] = new TH1F(str, "Reconstructed Number of PUs; Primary vertex number ; Events", 35, 0, 35);// Primary vertex bedore weight 
      sprintf(str,"hPU_B_%d", FileNumber);
      hPU_B[FileNumber] = new TH1F(str, "Reconstructed Number of PUs; Primary vertex number ; Events", 35, 0, 35);// Primary vertex bedore weight 
      sprintf(str,"hTrueNumInter_%d", FileNumber);
      hTrueNumInter[FileNumber] = new TH1F(str, "True Number of PUs; Primary vertex number ; Events", 35, 0, 35);// Primary vertex bedore weight 

      cout << " --- Start, opening the input file --- "<< argv[FileNumber] << endl;

      TheEvent e(argv[FileNumber]);
      if (!e) return 1;

      if (!e) {
          cerr << " Error opening the root file " << argv[FileNumber] << "\n" << endl;
      return 1;
      }

// print sample details
      cout << e.sample << endl;
  
// number of events:
     cout << "Total events: " << e.totalEvents() << endl;
     cout << "Luminocity: " << e.sample.lumi << endl; //Luminocity in pb^-1
     cout << "Cross Section: " << e.sample.CS << endl; //Cross Section in pb
  
     for (int i = 0; i < e.totalEvents(); ++i) {
         e.readEvent(i);
//         cout << i << endl;
         if (e.sample.type  < 2) hTrueNumInter[1]->Fill(e.TrueNumInter);
         if (e.electrons.size() >= 2) {
            if ((e.electrons[0].ID == 1) && (e.electrons[1].ID==1)) {
                const float MZ = (e.electrons[0].p4 + e.electrons[1].p4).M();
      
                if ((20 < MZ) && (MZ < 180)) {
                   if (e.sample.type == 2) {Lum = e.sample.lumi; Weight_A = 1.;Weight_B = 1.;} 
                   if (e.sample.type  < 2) {
                      if(e.pileupIT()<50) {Weight_A = e.sample.weight(Lum) * LumiWeights.weight(e.pileupIT()); 
                                           Weight_B = e.sample.weight(Lum) * generate_PileUP_weights(histo_Data)[e.pileupIT()];}
                      else {Weight_A = 0; Weight_B = 0; }//should not happen as we have a weight for all simulated ZJ_e.pileup.size() multiplicities! 
                      hTrueNumInter[2]->Fill(e.TrueNumInter); 
                   }
                   hMZ_A[FileNumber]->Fill(MZ, Weight_A);
                   hMZ_B[FileNumber]->Fill(MZ, Weight_B);
                   hPU_A[FileNumber]->Fill(e.vertices.size(), Weight_A);
                   hPU_B[FileNumber]->Fill(e.vertices.size(), Weight_B);
                          
//                 cout << "event "    << e.ievent << ":"
//                      << " M_Z = "   << MZ
//                      << " Nele = "  << e.electrons.size()
//                      << " Njets = " << e.jets.size()
//                      << endl;
               } //end MZ
           } //end ID
        }//end size
    }// end read event
  
    cout << "Total number of events with [50 < M_Z < 150] = " << hMZ_A[FileNumber]->GetEntries() << endl;
/*  
// fit by exponent + gaussian
    fitf_A[FileNumber]->SetNpx(500);
    fitf_A[FileNumber]->SetParameters(10, 0.01, 10, 90, 10);
    hMZ_A[FileNumber]->Fit(fitf_A[FileNumber], "IMN");
    A_A[FileNumber] = fitf_A[FileNumber]->GetParameter(2); // area under gaussian

// fit by exponent + gaussian
    fitf_B[FileNumber]->SetNpx(500);
    fitf_B[FileNumber]->SetParameters(10, 0.01, 10, 90, 10);
    hMZ_B[FileNumber]->Fit(fitf_B[FileNumber], "IMN");
    A_B[FileNumber] = fitf_B[FileNumber]->GetParameter(2); // area under gaussian
*/  
 } //ent file loop

 c.Print(".pdf[");

//Option A

 hMZ_A[1]->SetMarkerStyle(21);
 hMZ_A[1]->Draw();


/*  
// draw fit to background
// fitf_A[1]->SetLineColor(kBlue - 3);
// fitf_A[1]->SetLineStyle(7);
// fitf_A[1]->SetLineWidth(5);
// fitf_A[1]->SetParameter(2, 0);
// fitf_A[1]->DrawCopy("same");
  
// draw fit to background + signal
 fitf_A[1]->SetLineColor(kRed - 3);
 fitf_A[1]->SetLineStyle(1);
 fitf_A[1]->SetLineWidth(3);
// fitf_A[1]->SetParameter(2, A_A[1]);
 fitf_A[1]->Draw("same");
*/ 
// draw MC points

 hMZ_A[2]->SetMarkerStyle(22);
 hMZ_A[2]->SetMarkerColor(kGreen);
 hMZ_A[2]->Draw("same");
/*  
// draw MC fit to background
// fitf_A[2]->SetLineColor(kYellow - 3);
// fitf_A[2]->SetLineStyle(8);
// fitf_A[2]->SetLineWidth(5);
// fitf_A[2]->SetParameter(2, 0);
// fitf_A[2]->DrawCopy("same");
  
// draw MC fit to background + signal
 fitf_A[2]->SetLineColor(kMagenta - 3);
 fitf_A[2]->SetLineStyle(2);
 fitf_A[2]->SetLineWidth(3);
// fitf_A[2]->SetParameter(2, A_A[2]);
 fitf_A[2]->Draw("same");
*/

// save picture to .pdf file
 c.Print(".pdf");
 
//Option B


 hMZ_B[1]->SetMarkerStyle(21);
 hMZ_B[1]->Draw();

  
// draw fit to background
// fitf_B[1]->SetLineColor(kBlue - 3);
// fitf_B[1]->SetLineStyle(7);
// fitf_B[1]->SetLineWidth(5);
// fitf_B[1]->SetParameter(2, 0);
// fitf_B[1]->DrawCopy("same");
/*  
// draw fit to background + signal
 fitf_B[1]->SetLineColor(kRed - 3);
 fitf_B[1]->SetLineStyle(1);
 fitf_B[1]->SetLineWidth(3);
// fitf_B[1]->SetParameter(2, A_B[1]);
 fitf_B[1]->Draw("same");
*/ 
// draw MC points
 hMZ_B[2]->SetMarkerStyle(22);
 hMZ_B[2]->SetMarkerColor(kGreen);
 hMZ_B[2]->Draw("same");
  
// draw MC fit to background
// fitf_B[2]->SetLineColor(kYellow - 3);
// fitf_B[2]->SetLineStyle(8);
// fitf_B[2]->SetLineWidth(5);
// fitf_B[2]->SetParameter(2, 0);
// fitf_B[2]->DrawCopy("same");
/*  
// draw MC fit to background + signal
 fitf_B[2]->SetLineColor(kMagenta - 3);
 fitf_B[2]->SetLineStyle(2);
 fitf_B[2]->SetLineWidth(3);
// fitf_B[2]->SetParameter(2, A_B[2]);
 fitf_B[2]->Draw("same");
*/
// save picture to .pdf file
 c.Print(".pdf");

//Option A
// draw experimental points

 hPU_A[1]->SetMarkerStyle(21);
 hPU_A[1]->Draw();

// draw MC points
 hPU_A[2]->SetMarkerStyle(22);
 hPU_A[2]->SetMarkerColor(kGreen);
 hPU_A[2]->Draw("same");

// save picture to .pdf file
 c.Print(".pdf");

//Option B
// draw experimental points

 hPU_B[1]->SetMarkerStyle(21);
 hPU_B[1]->Draw();

// draw MC points
 hPU_B[2]->SetMarkerStyle(22);
 hPU_B[2]->SetMarkerColor(kGreen);
 hPU_B[2]->Draw("same");

// save picture to .pdf file
 c.Print(".pdf");

 histo_MC->SetMarkerStyle(21);
 histo_MC->DrawNormalized("");

 hTrueNumInter[1]->SetMarkerStyle(22);
 hTrueNumInter[1]->SetMarkerColor(kGreen);
 hTrueNumInter[1]->DrawNormalized("same");

 hTrueNumInter[2]->SetMarkerStyle(23);
 hTrueNumInter[2]->SetMarkerColor(kBlue);
 hTrueNumInter[2]->DrawNormalized("same");
// save picture to .pdf file 
 c.Print(".pdf");

 c.Print(".pdf]");

// save histogram to .root file
 TFile f("zboson.root", "RECREATE");
 hMZ_A[1]->Write(); 
 hMZ_A[2]->Write();
 hMZ_B[1]->Write(); 
 hMZ_B[2]->Write();
    
 return 0;
}
