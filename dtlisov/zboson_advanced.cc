// This is a small program to demonstrate a way how to
// select events with Z-bosons.
//
// to run do:
//   $ make 
//   $ zboson FILENAME

// Headers:
//
// c++
#include <iostream>

// root
#include "TCanvas.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TStyle.h"
#include "TFile.h"
#include "TROOT.h"
#include "TPad.h"
#include "TLegend.h"

// heavynu
#include "../PATDump/src/format.h"

//ReWeight

#include "LumiReweightingStandAlone.h"

using namespace std;

vector<float> generate_PileUP_weights(TH1D* data_npu_estimated){
   // For  Spring15 see https://github.com/cms-sw/cmssw/blob/CMSSW_7_4_X/SimGeneral/MixingModule/python/mix_2015_25ns_Startup_PoissonOOTPU_cfi.py copy and paste from there:
   const float npu_probs[52] = {/* 0 -->*/
                        4.8551E-07,
                        1.74806E-06,
                        3.30868E-06,
                        1.62972E-05,
                        4.95667E-05,
                        0.000606966,
                        0.003307249,
                        0.010340741,
                        0.022852296,
                        0.041948781,
                        0.058609363,
                        0.067475755,
                        0.072817826,
                        0.075931405,
                        0.076782504,
                        0.076202319,
                        0.074502547,
                        0.072355135,
                        0.069642102,
                        0.064920999,
                        0.05725576,
                        0.047289348,
                        0.036528446,
                        0.026376131,
                        0.017806872,
                        0.011249422,
                        0.006643385,
                        0.003662904,
                        0.001899681,
                        0.00095614,
                        0.00050028,
                        0.000297353,
                        0.000208717,
                        0.000165856,
                        0.000139974,
                        0.000120481,
                        0.000103826,
                        8.88868E-05,
                        7.53323E-05,
                        6.30863E-05,
                        5.21356E-05,
                        4.24754E-05,
                        3.40876E-05,
                        2.69282E-05,
                        2.09267E-05,
                        1.5989E-05,
                        4.8551E-06,
                        2.42755E-06,
                        4.8551E-07,
                        2.42755E-07,
                        1.21378E-07,
                        4.8551E-08 /* <-- 51 */};
   vector<float> result(50);
   float s = 0.0;
   for(int npu=0; npu<50; ++npu){
       float npu_estimated = data_npu_estimated->GetBinContent(data_npu_estimated->GetXaxis()->FindBin(npu));                              
       result[npu] = npu_estimated / npu_probs[npu];
//       s += result[npu];
       s += npu_estimated;
   }
   // normalize weights such that the total sum of weights over thw whole sample is 1.0, i.e., sum_i  result[i] * npu_probs[i] should be 1.0 (!)
   for(int npu=0; npu<50; ++npu){
       result[npu] /= s;
   }
   return result;
}

int main(int argc, char* argv[]) {

// define  files 
  const int FileNumberTotal = 5; // N
  const char* fname[FileNumberTotal];

//  fname[0] = "/afs/cern.ch/user/d/dtlisov/eos/cms/store/group/phys_exotica/leptonsPlusJets/HeavyNu_INR/Collisions_13_TeV_2015/DoubleEG_Run2015D-PromptReco-v4_MINIAOD/DoubleEG_DiEle_p30.root";
//  fname[0] = "/afs/cern.ch/user/d/dtlisov/eos/cms/store/group/phys_exotica/leptonsPlusJets/HeavyNu_INR/Collisions_13_TeV_2015/DoubleEG_Run2015D-PromptReco-v4_MINIAOD/DoubleEG_Run2015D-PromptReco_v4.root";
  fname[0] = "/afs/cern.ch/user/d/dtlisov/eos/cms/store/group/phys_exotica/leptonsPlusJets/HeavyNu_INR/Collisions_13_TeV_2015/DoubleEG_Run2015D-PromptReco-v4_MINIAOD/DoubleEG_Run2015D-PromptReco_v3.root";
//  fname[0] = "/afs/cern.ch/user/d/dtlisov/eos/cms/store/group/phys_exotica/leptonsPlusJets/HeavyNu_INR/Collisions_13_TeV_2015/DoubleEG_Run2015D-PromptReco-v4_MINIAOD/DoubleEG_Run2015D-PromptReco_full_data.root";

//  fname[1] = "/afs/cern.ch/user/d/dtlisov/eos/cms/store/group/phys_exotica/leptonsPlusJets/HeavyNu_INR/Collisions_13_TeV_2015/DYJetsToLL_M-50_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8_MINIAODSIM_74X_mcRun2_asymptotic_v2-v1/Z-bozon_1.root";
  fname[1] = "/afs/cern.ch/user/d/dtlisov/eos/cms/store/group/phys_exotica/leptonsPlusJets/HeavyNu_INR/Collisions_13_TeV_2015/DYJetsToLL_M-50_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8_MINIAODSIM_74X_mcRun2_asymptotic_v2-v1/DYJetsToLL_M-50_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8.root";
//  fname[1] = "/afs/cern.ch/user/d/dtlisov/eos/cms/store/group/phys_exotica/leptonsPlusJets/HeavyNu_INR/Collisions_13_TeV_2015/DYJetsToLL_M-50_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8_MINIAODSIM_74X_mcRun2_asymptotic_v2-v1/DiEle_p30.root";

//  fname[2] = "/afs/cern.ch/user/d/dtlisov/eos/cms/store/group/phys_exotica/leptonsPlusJets/HeavyNu_INR/Collisions_13_TeV_2015/DYToEE_13TeV-amcatnloFXFX-pythia8_RunIISpring15MiniAODv2-74X_mcRun2_asymptotic_v2-v1_MINIAODSIM/Z-bozon_3.root";
  fname[2] = "/afs/cern.ch/user/d/dtlisov/eos/cms/store/group/phys_exotica/leptonsPlusJets/HeavyNu_INR/Collisions_13_TeV_2015/DYToEE_13TeV-amcatnloFXFX-pythia8_RunIISpring15MiniAODv2-74X_mcRun2_asymptotic_v2-v1_MINIAODSIM/DYToEE_13TeV-amcatnloFXFX-pythia8.root";
//  fname[2] = "/afs/cern.ch/user/d/dtlisov/eos/cms/store/group/phys_exotica/leptonsPlusJets/HeavyNu_INR/Collisions_13_TeV_2015/DYToEE_13TeV-amcatnloFXFX-pythia8_RunIISpring15MiniAODv2-74X_mcRun2_asymptotic_v2-v1_MINIAODSIM/DiEle_p30.root";

//  fname[3] = "/afs/cern.ch/user/d/dtlisov/eos/cms/store/group/phys_exotica/leptonsPlusJets/HeavyNu_INR/Collisions_13_TeV_2015/DYJetsToLL_M-50_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_RunIISpring15MiniAODv2-74X_mcRun2_asymptotic_v2-v1_MINIAODSIM/Z-bozon_2.root";
  fname[3] = "/afs/cern.ch/user/d/dtlisov/eos/cms/store/group/phys_exotica/leptonsPlusJets/HeavyNu_INR/Collisions_13_TeV_2015/DYJetsToLL_M-50_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_RunIISpring15MiniAODv2-74X_mcRun2_asymptotic_v2-v1_MINIAODSIM/DYJetsToLL_M-50_TuneCUETP8M1_13TeV-madgraphMLM-pythia8.root";
//  fname[3] = "/afs/cern.ch/user/d/dtlisov/eos/cms/store/group/phys_exotica/leptonsPlusJets/HeavyNu_INR/Collisions_13_TeV_2015/DYJetsToLL_M-50_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_RunIISpring15MiniAODv2-74X_mcRun2_asymptotic_v2-v1_MINIAODSIM/DiEle_p30.root";

//  fname[4] = "/afs/cern.ch/user/d/dtlisov/eos/cms/store/group/phys_exotica/leptonsPlusJets/HeavyNu_INR/Collisions_13_TeV_2015/DYToEE_NNPDF30_13TeV-powheg-pythia8_RunIISpring15MiniAODv2-74X_mcRun2_asymptotic_v2-v1_MINIAODSIM/Z-bozon_4.root";
  fname[4] = "/afs/cern.ch/user/d/dtlisov/eos/cms/store/group/phys_exotica/leptonsPlusJets/HeavyNu_INR/Collisions_13_TeV_2015/DYToEE_NNPDF30_13TeV-powheg-pythia8_RunIISpring15MiniAODv2-74X_mcRun2_asymptotic_v2-v1_MINIAODSIM/DYToEE_NNPDF30_13TeV-powheg-pythia8.root";
//  fname[4] = "/afs/cern.ch/user/d/dtlisov/eos/cms/store/group/phys_exotica/leptonsPlusJets/HeavyNu_INR/Collisions_13_TeV_2015/DYToEE_NNPDF30_13TeV-powheg-pythia8_RunIISpring15MiniAODv2-74X_mcRun2_asymptotic_v2-v1_MINIAODSIM/DiEle_p30.root";



// define histogrammes

  TH1::SetDefaultSumw2();
  TH1F *hMZ[FileNumberTotal]; //histogram mass z boson declaration
  TH1F *hMZ1[FileNumberTotal]; //histogram mass z boson declaration
  TH1F *hMZ2[FileNumberTotal]; //histogram mass z boson declaration
  TH1F *hMZ3[FileNumberTotal]; //histogram mass z boson declaration
  TH1F *hMZ_G[FileNumberTotal]; //histogram mass z boson declaration
  TH1F *hPU[FileNumberTotal]; //histogram  primary vertex
  TH1F *hPU_G[FileNumberTotal]; //histogram  primary vertex
  TH1F *hTrueNumInter[FileNumberTotal]; //histogram  primary vertex
  TH1F *hTrueNumInterWeighted[FileNumberTotal]; //histogram  primary vertex
  TH1F *hTrueNumInterWeighted_G[FileNumberTotal]; //histogram  primary vertex

  char *str = (char*)alloca(10000);
  char *str1 = (char*)alloca(10000);
  double Lum; 

// Reweighting 
 
  reweight::LumiReWeighting LumiWeights= reweight::LumiReWeighting(
                                                                     "MyMCExamplePileupHistogram.root", //MC histo file
//                                                                     "MyMCExamplePileupHistogram_observed.root",// MC observed histo file
//                                                                     "MyDataExamplePileupHistogram_true_xs69.root", // Data histo file
//                                                                     "MyDataExamplePileupHistogram_observed_xs69.root", //Data histo
//                                                                   "MyDataExamplePileupHistogram_true_xs80.root", // Data histo file
//                                                                     "Data2015DPileupHistogram_true_xs69.root", // Data histo file
//                                                                   "Data2015DPileupHistogram_true_xs80.root", // Data histo file
                                                                     "V_3_DataPileupHistogram.root",
//                                                                    "V_4_DataPileupHistogram.root",
                                                                     "MC_pileup", // MC histoname
                                                                     "pileup"); // Data histoname

 // True Number of interaction 

  TFile* f_MC = new TFile("MyMCExamplePileupHistogram.root");
  TH1D* histo_MC = (TH1D*) f_MC->Get("MC_pileup");


//    TFile* f_DA = new TFile("Data2015DPileupHistogram_true_xs69.root");
//  TFile* f_DA = new TFile("MyDataExamplePileupHistogram_true_xs69.root");
//  TFile* f_DA = new TFile("MyDataExamplePileupHistogram_true_xs80.root");
   TFile* f_DA = new TFile("V_3_DataPileupHistogram.root");
//  TFile* f_DA = new TFile("V_4_DataPileupHistogram.root");
   TH1D* histo_DA = (TH1D*) f_DA->Get("pileup");


  for (int FileNumber = 0; FileNumber < FileNumberTotal; ++FileNumber) {
     double Gen_Integral = 0;
     if ((FileNumber == 1)||(FileNumber == 2)) {
        TFile* f_gen_w = new TFile(fname[FileNumber]);
        TDirectory* patdump = f_gen_w->GetDirectory("patdump");
        TH1F* gen_w_hist= (TH1F*) patdump->Get("gen_weight");
        for (int i = 1; i < gen_w_hist->GetNbinsX(); ++i) if (gen_w_hist->GetBinContent(i)>0.) Gen_Integral+=gen_w_hist->GetBinCenter(i)*gen_w_hist->GetBinContent(i);
//        Gen_Integral = gen_w_hist->Integral();
     }
   
     cout <<" Gen Integral " << Gen_Integral << endl;

      double Weight = 0.;
      double Weight_G = 0.;
//book histogrammes
      sprintf(str,"hMZ_%d", FileNumber);
      hMZ[FileNumber] = new TH1F(str, "M_{ee} distribution;M_{ee}, GeV/c^{2};#events", 100, 60, 120); //histogram mass z boson declaration
      sprintf(str,"hMZ1_%d", FileNumber);
      hMZ1[FileNumber] = new TH1F(str, "M_{ee} distribution;M_{ee}, GeV/c^{2};#events", 100, 60, 120); //histogram mass z boson declaration
      sprintf(str,"hMZ2_G_%d", FileNumber);
      hMZ2[FileNumber] = new TH1F(str, "M_{ee} distribution + 1 jet;M_{ee}, GeV/c^{2};#events", 100, 60, 120); //histogram mass z boson declaration
      sprintf(str,"hMZ3_G_%d", FileNumber);
      hMZ3[FileNumber] = new TH1F(str, "M_{ee} distribution + 2 jets;M_{ee}, GeV/c^{2};#events", 100, 60, 120); //histogram mass z boson declaration
      sprintf(str,"hMZ_G_%d", FileNumber);
      hMZ_G[FileNumber] = new TH1F(str, "M_{ee} distribution without PU reweight;M_{ee}, GeV/c^{2};#events", 100, 80, 100); //histogram mass z boson declaration
      sprintf(str,"hPU_%d", FileNumber);
      hPU[FileNumber] = new TH1F(str, "Reconstructed Number of PUs; Primary vertex number ; Events", 35, 0, 35);// Primary vertex bedore weight 
      sprintf(str,"hPU_G_%d", FileNumber);
      hPU_G[FileNumber] = new TH1F(str, "Reconstructed Number of PUs without PU reweight; Primary vertex number ; Events", 35, 0, 35);// Primary vertex bedore weight 
      sprintf(str,"hTrueNumInter_%d", FileNumber);
      hTrueNumInter[FileNumber] = new TH1F(str, "True Number of PUs; Primary vertex number ; Events", 35, 0, 35);// Primary vertex bedore weight 
      sprintf(str,"hTrueNumInterWeighted_%d", FileNumber);
      hTrueNumInterWeighted[FileNumber] = new TH1F(str, "True Number of PUs; Primary vertex number ; Events", 35, 0, 35);// Primary true vertex after weight
      sprintf(str,"hTrueNumInterWeighted_G_%d", FileNumber);
      hTrueNumInterWeighted_G[FileNumber] = new TH1F(str, "True Number of PUs without PU reweight; Primary vertex number ; Events", 35, 0, 35);// Primary true vertex after weight
  
      cout << " --- Start, opening the input file --- "<< fname[FileNumber] << endl;

      TheEvent e(fname[FileNumber]);
      if (!e) return 1;

      if (!e) {
          cerr << " Error opening the root file " << fname[FileNumber] << "\n" << endl;
      return 1;
      }

// print sample details
      cout << e.sample << endl;
  
// number of events:
     cout << "Total events: " << e.totalEvents() << endl;
     cout << "Luminocity: " << e.sample.lumi << endl; //Luminocity in pb^-1
     cout << "Cross Section: " << e.sample.CS << endl; //Cross Section in pb
     cout << "Sample statistics: " << e.sample.stat<< endl;
     if (Gen_Integral == 0) Gen_Integral = e.sample.stat;
//     if (FileNumber == 1) Gen_Integral = 4576.169e+08;
//     if (FileNumber == 1) Gen_Integral = 1.936e+08;
//     if (FileNumber == 2) Gen_Integral = 1.99e+08;

     cout << "Sample Sum of weights: " << Gen_Integral<< endl;
     for (int i = 0; i < e.totalEvents(); ++i) {
         e.readEvent(i);
         if (i%1000000 == 0) cout <<"Event = "<< i << endl;
         if (e.sample.type == 2) {Lum = e.sample.lumi; Weight = 1.; Weight_G = 1.;} 
         if (e.sample.type  < 2) {
                      Weight_G = Lum *e.sample.CS * (e.Gen_weight/Gen_Integral);
                      if(e.TrueNumInter<50) {Weight = Lum *e.sample.CS * (e.Gen_weight/Gen_Integral) * LumiWeights.weight(e.TrueNumInter);}
//                      if(e.TrueNumInter<50) {Weight = Lum *e.sample.CS * (e.Gen_weight/Gen_Integral) * generate_PileUP_weights(histo_DA)[e.TrueNumInter];}
                      else {Weight = 0;} //should not happen as we have a weight for all simulated ZJ_e.pileup.size() multiplicities!
//       cout<<"   "<<e.pileupIT()<<"   "<< e.TrueNumInter<<"   "<< generate_PileUP_weights(histo_DA)[e.TrueNumInter] <<"    "<<e.Gen_weight<<"    "<< Weight<<endl; 
         }
         if (e.sample.type  < 2) {hTrueNumInter[FileNumber]->Fill(e.TrueNumInter); hTrueNumInterWeighted_G[FileNumber]->Fill(e.TrueNumInter, Weight_G); hTrueNumInterWeighted[FileNumber]->Fill(e.TrueNumInter, Weight);}
         hPU_G[FileNumber]->Fill(e.vertices.size(), Weight_G);   
         hPU[FileNumber]->Fill(e.vertices.size(), Weight);  
         if (e.electrons.size() >= 2) {
            if ((e.electrons[0].ID >= 4) && (e.electrons[1].ID>= 4)) {
               if ((e.electrons[0].p4.Pt() > 40.)&& (e.electrons[0].p4.Pt() > 40.)) {
                  const float MZ = (e.electrons[0].p4 + e.electrons[1].p4).M();    
                  if ((60 < MZ) && (MZ < 120)) {
                     hMZ_G[FileNumber]->Fill(MZ, Weight_G);    
                     hMZ[FileNumber]->Fill(MZ, Weight);             
 //                  hPU_G[FileNumber]->Fill(e.vertices.size(), Weight_G);   
 //                  hPU[FileNumber]->Fill(e.vertices.size(), Weight);                                                   
//                   cout << "event "    << e.ievent << ":"
//                      << " M_Z = "   << MZ
//                      << " Nele = "  << e.electrons.size()
//                      << " Njets = " << e.jets.size()
//                      << endl;
                  } //end MZ
               }// end Pt
               if ((e.electrons[0].p4.Pt() > 60.)&& (e.electrons[0].p4.Pt() > 40.)) {
                  const float MZ1 = (e.electrons[0].p4 + e.electrons[1].p4).M();    
                  if ((60 < MZ1) && (MZ1 < 120)) hMZ1[FileNumber]->Fill(MZ1, Weight);
//jets-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                  if (e.jets.size() >= 3) {
                      if ((abs(e.jets[0].p4.Eta()-e.electrons[0].p4.Eta())>0.05) || (abs(e.jets[0].p4.Phi()-e.electrons[0].p4.Phi())>0.05)) {
//                         const float MN1 = (e.electrons[0].p4 + e.electrons[1].p4 + e.jets[0].p4).M();
                         if ((e.jets[0].p4.Pt() > 40.)&&(60 < MZ1) && (MZ1 < 120)) hMZ2[FileNumber]->Fill(MZ1, Weight); 
//                       if ((e.jets[0].p4.Pt() > 40.)&&(MZ1 > 200)) hMN1[FileNumber]->Fill(MN1, Weight);
                         if (e.jets.size() >= 4) {
                              if ((abs(e.jets[0].p4.Eta()-e.electrons[1].p4.Eta())>0.05) || (abs(e.jets[0].p4.Phi()-e.electrons[1].p4.Phi())>0.05)) {
//                                  const float MN2 = (e.electrons[0].p4 + e.electrons[1].p4 + e.jets[1].p4).M();
//                                  MW = (e.electrons[0].p4 + e.electrons[1].p4 + e.jets[0].p4 + e.jets[1].p4).M();
                                  if ((e.jets[0].p4.Pt() > 40.)&&(e.jets[1].p4.Pt() > 40.)&&(60 < MZ1) && (MZ1 < 120)) hMZ3[FileNumber]->Fill(MZ1, Weight);
//                                if ((e.jets[0].p4.Pt() > 40.)&&(e.jets[1].p4.Pt() > 40.)&&(MZ1 > 200)) {hMZ4[FileNumber]->Fill(MZ1, Weight); hMN2[FileNumber]->Fill(MN1, Weight); hMN3[FileNumber]->Fill(MN2, Weight); hMW[FileNumber]->Fill(MW, Weight);}
                              }
                              else {
                                  if ((abs(e.jets[2].p4.Eta()-e.electrons[1].p4.Eta())>0.05) || (abs(e.jets[2].p4.Phi()-e.electrons[1].p4.Phi())>0.05)) {
//                                     const float MN2 = (e.electrons[0].p4 + e.electrons[1].p4 + e.jets[2].p4).M();
//                                     MW = (e.electrons[0].p4 + e.electrons[1].p4 + e.jets[0].p4 + e.jets[2].p4).M();
                                     if ((e.jets[0].p4.Pt() > 40.)&&(e.jets[2].p4.Pt() > 40.)&&(60 < MZ1) && (MZ1 < 120)) hMZ3[FileNumber]->Fill(MZ1, Weight);
//                                   if ((e.jets[0].p4.Pt() > 40.)&&(e.jets[2].p4.Pt() > 40.)&&(MZ1 > 200)) {hMZ4[FileNumber]->Fill(MZ1, Weight); hMN2[FileNumber]->Fill(MN1, Weight); hMN3[FileNumber]->Fill(MN2, Weight); hMW[FileNumber]->Fill(MW, Weight);}
                                  }
                                  else {
//                                     const float MN2 = (e.electrons[0].p4 + e.electrons[1].p4 + e.jets[3].p4).M();
//                                     MW = (e.electrons[0].p4 + e.electrons[1].p4 + e.jets[0].p4 + e.jets[3].p4).M();
                                     if ((e.jets[0].p4.Pt() > 40.)&&(e.jets[3].p4.Pt() > 40.)&&(60 < MZ1) && (MZ1 < 120)) hMZ3[FileNumber]->Fill(MZ1, Weight);
//                                   if ((e.jets[0].p4.Pt() > 40.)&&(e.jets[3].p4.Pt() > 40.)&&(MZ1 > 200)) {hMZ4[FileNumber]->Fill(MZ1, Weight); hMN2[FileNumber]->Fill(MN1, Weight); hMN3[FileNumber]->Fill(MN2, Weight); hMW[FileNumber]->Fill(MW, Weight);}
                                  }//jet2=ele1
                              }//end jet1=ele1
                         }// jet>4
                      }//end jet0=ele0
                      else {
                          if ((abs(e.jets[1].p4.Eta()-e.electrons[1].p4.Eta())>0.05) || (abs(e.jets[1].p4.Phi()-e.electrons[1].p4.Phi())>0.05)) {
/*                            cout<<"1Jet Pt = "<<  e.jets[0].p4.Pt()<<"  "<<"  Jet Eta = "<< e.jets[0].p4.Eta()<<"  "<<"  Jet Phi = "<< e.jets[0].p4.Phi()<< endl;
                              cout<<"1Ele1 Pt = "<<  e.electrons[0].p4.Pt()<<"  "<<"  Ele1 Eta = "<< e.electrons[0].p4.Eta()<<"  "<<"  Ele1 Phi = "<< e.electrons[0].p4.Phi()<< endl;
                              cout<<"1Jet Pt = "<<  e.jets[1].p4.Pt()<<"  "<<"  Jet Eta = "<< e.jets[1].p4.Eta()<<"  "<<"  Jet Phi = "<< e.jets[1].p4.Phi()<< endl;
                              cout<<"1Ele2 Pt = "<<  e.electrons[1].p4.Pt()<<"  "<<"  Ele2 Eta = "<< e.electrons[1].p4.Eta()<<"  "<<"  Ele2 Phi = "<< e.electrons[1].p4.Phi()<< endl;
                              cout<<"1Jet Pt = "<<  e.jets[2].p4.Pt()<<"  "<<"  Jet Eta = "<< e.jets[2].p4.Eta()<<"  "<<"  Jet Phi = "<< e.jets[2].p4.Phi()<< endl;
                              cout<< endl;
*/ //                           const float MN1 = (e.electrons[0].p4 + e.electrons[1].p4 + e.jets[1].p4).M();
                              if ((e.jets[1].p4.Pt() > 40.)&&(60 < MZ1) && (MZ1 < 120)) hMZ2[FileNumber]->Fill(MZ1, Weight);
//                            if ((e.jets[1].p4.Pt() > 40.)&&(MZ1 > 200)) hMN1[FileNumber]->Fill(MN1, Weight); 
                              if (e.jets.size() >= 4) {
                                  if ((abs(e.jets[2].p4.Eta()-e.electrons[1].p4.Eta())>0.05) || (abs(e.jets[2].p4.Phi()-e.electrons[1].p4.Phi())>0.05)) {
//                                     const float MN2 = (e.electrons[0].p4 + e.electrons[1].p4 + e.jets[2].p4).M();
//                                     MW = (e.electrons[0].p4 + e.electrons[1].p4 + e.jets[1].p4 + e.jets[2].p4).M();
                                     if ((e.jets[1].p4.Pt() > 40.)&&(e.jets[2].p4.Pt() > 40.)&&(60 < MZ1) && (MZ1 < 120)) hMZ3[FileNumber]->Fill(MZ1, Weight);
//                                   if ((e.jets[1].p4.Pt() > 40.)&&(e.jets[2].p4.Pt() > 40.)&&(MZ1 > 200)) {hMZ4[FileNumber]->Fill(MZ1, Weight); hMN2[FileNumber]->Fill(MN1, Weight); hMN3[FileNumber]->Fill(MN2, Weight); hMW[FileNumber]->Fill(MW, Weight);}
                                  }
                                  else {
//                                     const float MN2 = (e.electrons[0].p4 + e.electrons[1].p4 + e.jets[3].p4).M();
//                                     MW = (e.electrons[0].p4 + e.electrons[1].p4 + e.jets[1].p4 + e.jets[3].p4).M();
                                     if ((e.jets[1].p4.Pt() > 40.)&&(e.jets[3].p4.Pt() > 40.)&&(60 < MZ1) && (MZ1 < 120)) hMZ3[FileNumber]->Fill(MZ1, Weight);
//                                   if ((e.jets[1].p4.Pt() > 40.)&&(e.jets[3].p4.Pt() > 40.)&&(MZ1 > 200)) {hMZ4[FileNumber]->Fill(MZ1, Weight); hMN2[FileNumber]->Fill(MN1, Weight); hMN3[FileNumber]->Fill(MN2, Weight); hMW[FileNumber]->Fill(MW, Weight);}
                                  }//jet2=ele1
                              }//end jets>4 
                          }//end jet1=ele1
                          else {
                              const float MN1 = (e.electrons[0].p4 + e.electrons[1].p4 + e.jets[2].p4).M();
                              if ((e.jets[2].p4.Pt() > 40.)&&(60 < MZ1) && (MZ1 < 120)) hMZ2[FileNumber]->Fill(MZ1, Weight);
//                              if ((e.jets[2].p4.Pt() > 40.)&&(MZ1 > 200)) hMN1[FileNumber]->Fill(MN1, Weight);
                                if (e.jets.size() >= 4) {
//                                   const float MN2 = (e.electrons[0].p4 + e.electrons[1].p4 + e.jets[3].p4).M();
//                                   MW = (e.electrons[0].p4 + e.electrons[1].p4 + e.jets[2].p4 + e.jets[3].p4).M();
                                   if ((e.jets[2].p4.Pt() > 40.)&&(e.jets[3].p4.Pt() > 40.)&&(60 < MZ1) && (MZ1 < 120)) hMZ3[FileNumber]->Fill(MZ1, Weight);
//                                   if ((e.jets[2].p4.Pt() > 40.)&&(e.jets[3].p4.Pt() > 40.)&&(MZ1 > 200)) {hMZ4[FileNumber]->Fill(MZ1, Weight); hMN2[FileNumber]->Fill(MN1, Weight); hMN3[FileNumber]->Fill(MN2, Weight); hMW[FileNumber]->Fill(MW, Weight);}
                             }//jet>4 
                         }//end jet1=ele1
                      }//end jet0=ele0
                  }// jet>3
//end jets-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
              }//end Pt
           } //end ID
        }//end size
    }// end read event
  
    cout << "Total number of events with [60 < M_Z < 120] = " << hMZ[FileNumber]->GetEntries() << endl;
    cout << "Total number of events with [60 < M_Z < 120] = " << hMZ1[FileNumber]->GetEntries() << endl;
    cout << "Total number of events with [60 < M_Z < 120] + 1 jet = " << hMZ2[FileNumber]->GetEntries() << endl;
    cout << "Total number of events with [60 < M_Z < 120] + 2 jets = " << hMZ3[FileNumber]->GetEntries() << endl;
    cout <<  endl;
    cout <<  endl;
    cout <<  endl;
 } //end file loop


// prepare a picture
  gROOT->SetStyle("Plain");
  gStyle->SetOptStat("");
  gStyle->SetOptLogy(1);
  TCanvas c("zboson_advanced", "zboson_advanced", 800, 600);
//  setTDRStyle();
//  fixOverlay();

  TLegend leg_A(0.60, 0.74, 0.94, 0.94);
  leg_A.SetTextFont(42);
  leg_A.SetTextSize(0.034);
  
  sprintf(str1,"Data:%4.0f", hMZ[0]->Integral()); 
  leg_A.AddEntry(hMZ[0], str1, "EP");
  
  if (FileNumberTotal>1) {
     sprintf(str1,"Z+jets amcatnloFXFX:%4.0f", hMZ[1]->Integral());
     leg_A.AddEntry(hMZ[1], str1,"L");
  }

  if (FileNumberTotal>2) {
     sprintf(str1,"Z->ee amcatnloFXFX:%4.0f", hMZ[2]->Integral());
    leg_A.AddEntry(hMZ[2], str1,"L");
  }

  if (FileNumberTotal>3) {
    sprintf(str1,"Z+jets madgraphMLM:%4.0f", hMZ[3]->Integral());
    leg_A.AddEntry(hMZ[3], str1,"L");
  }
  if (FileNumberTotal>4) {
  sprintf(str1,"Z->ee powheg:%4.0f", hMZ[4]->Integral());
  leg_A.AddEntry(hMZ[4], str1,"L");
  } 
 
  TLegend leg_A_G(0.60, 0.74, 0.94, 0.94);
  leg_A_G.SetTextFont(42);
  leg_A_G.SetTextSize(0.034);
  
  sprintf(str1,"Data:%4.0f", hMZ_G[0]->Integral()); 
  leg_A_G.AddEntry(hMZ_G[0], str1, "EP");
  
  if (FileNumberTotal>1) {
     sprintf(str1,"Z+jets amcatnloFXFX:%4.0f", hMZ_G[1]->Integral());
     leg_A_G.AddEntry(hMZ_G[1], str1,"L");
  }

  if (FileNumberTotal>2) {
     sprintf(str1,"Z->ee amcatnloFXFX:%4.0f", hMZ_G[2]->Integral());
    leg_A_G.AddEntry(hMZ_G[2], str1,"L");
  }

  if (FileNumberTotal>3) {
    sprintf(str1,"Z+jets madgraphMLM:%4.0f", hMZ_G[3]->Integral());
    leg_A_G.AddEntry(hMZ_G[3], str1,"L");
  }
  if (FileNumberTotal>4) {
  sprintf(str1,"Z->ee powheg:%4.0f", hMZ_G[4]->Integral());
  leg_A_G.AddEntry(hMZ_G[4], str1,"L");
  } 


 c.Print(".pdf[");
//    hMZ[0]->SetDefaultSumw2();
    hMZ[0]->SetMarkerStyle(21);
    hMZ[0]->SetMarkerColor(1);
//    hMZ[0]->SetLineStyle(1);
    hMZ[0]->Draw("E1");
 
for (int i = 1; i < FileNumberTotal; ++i) {
    hMZ[i]->SetLineStyle(1);
    hMZ[i]->SetLineColor(1+i);
    hMZ[i]->Draw("HISTsame");
}
// save picture to .pdf file
    leg_A.Draw();
    c.Print(".pdf");

//    hMZ_G[0]->SetDefaultSumw2();
    hMZ_G[0]->SetMarkerStyle(21);
    hMZ_G[0]->SetMarkerColor(1);
//    hMZ_G[0]->SetLineStyle(1);
    hMZ_G[0]->Draw("E1");
 
for (int i = 1; i < FileNumberTotal; ++i) {
    hMZ_G[i]->SetLineStyle(1);
    hMZ_G[i]->SetLineColor(1+i);
    hMZ_G[i]->Draw("HISTsame");
}
// save picture to .pdf file
    leg_A_G.Draw();
    c.Print(".pdf");



//    hPU[0]->SetDefaultSumw2();
    hPU[0]->SetMarkerStyle(21);
    hPU[0]->SetMarkerColor(1);
//    hPU[0]->SetLineStyle(1);
    hPU[0]->Draw("E1");
 for (int i = 1; i < FileNumberTotal; ++i) {
    hPU[i]->SetLineStyle(1);
    hPU[i]->SetLineColor(1+i);
    hPU[i]->Draw("HISTsame");
}
// save picture to .pdf file
    leg_A.Draw();
    c.Print(".pdf");

//    hPU_G[0]->SetDefaultSumw2();
    hPU_G[0]->SetMarkerStyle(21);
    hPU_G[0]->SetMarkerColor(1);
//    hPU_G[0]->SetLineStyle(1);
    hPU_G[0]->Draw("E1");
 for (int i = 1; i < FileNumberTotal; ++i) {
    hPU_G[i]->SetLineStyle(1);
    hPU_G[i]->SetLineColor(1+i);
    hPU_G[i]->Draw("HISTsame");
}
// save picture to .pdf file
    leg_A_G.Draw();
    c.Print(".pdf");

  TLegend leg_B(0.65, 0.74, 0.94, 0.94);
  leg_B.SetTextFont(42);
  leg_B.SetTextSize(0.034);

  leg_B.AddEntry(histo_MC, "True Distribution", "L");
  leg_B.AddEntry(hMZ[0], "Data", "EP");
  if (FileNumberTotal>1) {
     leg_B.AddEntry(hMZ[1], "Z+jets amcatnloFXFX","L");
  }
  if (FileNumberTotal>2) {
    leg_B.AddEntry(hMZ[2], "Z->ee amcatnloFXFX","L");
  }
  if (FileNumberTotal>3) {
    leg_B.AddEntry(hMZ[3], "Z+jets madgraphMLM","L");
  }
  if (FileNumberTotal>4) {
  leg_B.AddEntry(hMZ[4], "Z->ee powheg","L");
  } 

    histo_DA->DrawNormalized("");
    histo_MC->DrawNormalized("same");
    histo_DA->SetMarkerStyle(21);
    histo_DA->SetMarkerColor(1);
    histo_DA->DrawNormalized("E1same");

 for (int i = 1; i < FileNumberTotal; ++i) {
    hTrueNumInter[i]->SetLineStyle(1);
    hTrueNumInter[i]->SetLineColor(1+i);
    hTrueNumInter[i]->DrawNormalized("HISTsame");

 }

// save picture to .pdf file 
    leg_B.Draw();
 c.Print(".pdf");
    histo_DA->SetTitle("True Number of PUs");
    histo_DA->DrawNormalized("");
    histo_MC->DrawNormalized("same");
    histo_DA->SetMarkerStyle(21);
    histo_DA->SetMarkerColor(1);
    histo_DA->DrawNormalized("E1same");
 for (int i = 1; i < FileNumberTotal; ++i) {
    hTrueNumInterWeighted[i]->SetLineStyle(1);
    hTrueNumInterWeighted[i]->SetLineColor(1+i);
    hTrueNumInterWeighted[i]->DrawNormalized("HISTsame");
 }
// save picture to .pdf file 
    leg_B.Draw();
 c.Print(".pdf");

    histo_DA->SetTitle("True Number of PUs without PU reweight");
    histo_DA->DrawNormalized("");
    histo_MC->DrawNormalized("same");
    histo_DA->SetMarkerStyle(21);
    histo_DA->SetMarkerColor(1);
    histo_DA->DrawNormalized("E1same");

 for (int i = 1; i < FileNumberTotal; ++i) {
    hTrueNumInterWeighted_G[i]->SetLineStyle(1);
    hTrueNumInterWeighted_G[i]->SetLineColor(1+i);
    hTrueNumInterWeighted_G[i]->DrawNormalized("HISTsame");
 }
// save picture to .pdf file 
    leg_B.Draw();
 c.Print(".pdf");

 c.Print(".pdf]");

// save histogram to .root file
 TFile f("zboson_advanced.root", "RECREATE");
 for (int i = 0; i < FileNumberTotal; ++i) {
   hMZ[i]->Write();
   hMZ1[i]->Write();
   hMZ2[i]->Write();
   hMZ3[i]->Write();
   hMZ_G[i]->Write();
   hPU[i]->Write();
   hPU_G[i]->Write();
   hTrueNumInter[i]->Write();
   hTrueNumInterWeighted[i]->Write();
   hTrueNumInterWeighted_G[i]->Write();
 }
   histo_MC->Write();
   histo_DA->Write();
    
 return 0;
}

