// This is a small program to demonstrate a way how to
// select events with Z-bosons.
//
// to run do:
//   $ make 
//   $ zboson FILENAME

// Headers:
//
// c++
#include <iostream>

// root
#include "TCanvas.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TStyle.h"
#include "TFile.h"
#include "TROOT.h"
#include "TPad.h"
#include "TLegend.h"

// heavynu
#include "../PATDump/src/format.h"

//ReWeight

#include "LumiReweightingStandAlone.h"

using namespace std;

int main(int argc, char* argv[]) {

// define  files 
  const int FileNumberTotal = 3; // N
  const char* fname[FileNumberTotal];

//  fname[0] = "/afs/cern.ch/user/d/dtlisov/eos1/cms/store/group/phys_exotica/leptonsPlusJets/HeavyNu_INR/Collisions_13_TeV_2015/DoubleMuon_Run2015D-PromptReco-v4_MINIAOD/DoubleMuon_p30.root";
//  fname[0] = "/afs/cern.ch/user/d/dtlisov/eos1/cms/store/group/phys_exotica/leptonsPlusJets/HeavyNu_INR/Collisions_13_TeV_2015/DoubleMuon_Run2015D-PromptReco-v4_MINIAOD/DoubleMuon_Run2015D_PromptReco-v4.root";
  fname[0] = "/afs/cern.ch/user/d/dtlisov/eos1/cms/store/group/phys_exotica/leptonsPlusJets/HeavyNu_INR/Collisions_13_TeV_2015/DoubleMuon_Run2015D-PromptReco-v4_MINIAOD/DoubleMuon_Run2015D_PromptReco-v3.root";
//  fname[0] = "/afs/cern.ch/user/d/dtlisov/eos1/cms/store/group/phys_exotica/leptonsPlusJets/HeavyNu_INR/Collisions_13_TeV_2015/DoubleMuon_Run2015D-PromptReco-v4_MINIAOD/DoubleMuon_Run2015D-PromptReco_full_data.root";

//  fname[1] = "/afs/cern.ch/user/d/dtlisov/eos1/cms/store/group/phys_exotica/leptonsPlusJets/HeavyNu_INR/Collisions_13_TeV_2015/DYJetsToLL_M-50_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8_MINIAODSIM_74X_mcRun2_asymptotic_v2-v1/Z-bozon_1.root";
//  fname[1] = "/afs/cern.ch/user/d/dtlisov/eos1/cms/store/group/phys_exotica/leptonsPlusJets/HeavyNu_INR/Collisions_13_TeV_2015/DYJetsToLL_M-50_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8_MINIAODSIM_74X_mcRun2_asymptotic_v2-v1/DiMu_p30.root";
  fname[1] = "/afs/cern.ch/user/d/dtlisov/eos1/cms/store/group/phys_exotica/leptonsPlusJets/HeavyNu_INR/Collisions_13_TeV_2015/DYJetsToLL_M-50_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8_MINIAODSIM_74X_mcRun2_asymptotic_v2-v1/DYJetsToLL_M-50_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8.root";

//  fname[2] = "/afs/cern.ch/user/d/dtlisov/eos1/cms/store/group/phys_exotica/leptonsPlusJets/HeavyNu_INR/Collisions_13_TeV_2015/DYJetsToLL_M-50_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_RunIISpring15MiniAODv2-74X_mcRun2_asymptotic_v2-v1_MINIAODSIM/Z-bozon_2.root";
//  fname[2] = "/afs/cern.ch/user/d/dtlisov/eos1/cms/store/group/phys_exotica/leptonsPlusJets/HeavyNu_INR/Collisions_13_TeV_2015/DYJetsToLL_M-50_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_RunIISpring15MiniAODv2-74X_mcRun2_asymptotic_v2-v1_MINIAODSIM/DiEle_p30.root";
  fname[2] = "/afs/cern.ch/user/d/dtlisov/eos1/cms/store/group/phys_exotica/leptonsPlusJets/HeavyNu_INR/Collisions_13_TeV_2015/DYJetsToLL_M-50_TuneCUETP8M1_13TeV-madgraphMLM-pythia8_RunIISpring15MiniAODv2-74X_mcRun2_asymptotic_v2-v1_MINIAODSIM/DYJetsToLL_M-50_TuneCUETP8M1_13TeV-madgraphMLM-pythia8.root";


double koef_norm[20] = {1.};

koef_norm[1]=0.8585;
koef_norm[2]=0.7352;

// define histogrammes

  TH1::SetDefaultSumw2();
  TH1F *hMZ[FileNumberTotal]; //histogram mass z boson declaration
  TH1F *hMZ1[FileNumberTotal]; //histogram mass z boson declaration
  TH1F *hMZ2[FileNumberTotal]; //histogram mass z boson declaration
  TH1F *hMZ3[FileNumberTotal]; //histogram mass z boson declaration
  TH1F *hPU[FileNumberTotal]; //histogram  primary vertex
  TH1F *hTrueNumInter[FileNumberTotal]; //histogram  primary vertex
  TH1F *hTrueNumInterWeighted[FileNumberTotal]; //histogram  primary vertex

  char *str = (char*)alloca(10000);
  char *str1 = (char*)alloca(10000);
  double Lum; 

// Reweighting 
 
  reweight::LumiReWeighting LumiWeights= reweight::LumiReWeighting(
                                                                    "MyMCExamplePileupHistogram.root", //MC histo file
//                                                                    "MyDataExamplePileupHistogram_true_xs69.root", // Data histo file
//                                                                   "MyDataExamplePileupHistogram_true_xs80.root", // Data histo file
//                                                                    "Data2015DPileupHistogram_true_xs69.root", // Data histo file
//                                                                   "Data2015DPileupHistogram_true_xs80.root", // Data histo file
                                                                    "V_3_DataPileupHistogram.root",
//                                                                    "V_4_DataPileupHistogram.root",
                                                                    "MC_pileup", // MC histoname
                                                                    "pileup"); // Data histoname

 // True Number of interaction 

   TFile* f_MC = new TFile("MyMCExamplePileupHistogram.root");
   TH1D* histo_MC = (TH1D*) f_MC->Get("MC_pileup");

//    TFile* f_DA = new TFile("Data2015DPileupHistogram_true_xs69.root");
//  TFile* f_DA = new TFile("MyDataExamplePileupHistogram_true_xs69.root");
//  TFile* f_DA = new TFile("MyDataExamplePileupHistogram_true_xs80.root");
  TFile* f_DA = new TFile("V_3_DataPileupHistogram.root");
//   TFile* f_DA = new TFile("V_4_DataPileupHistogram.root");
   TH1D* histo_DA = (TH1D*) f_DA->Get("pileup");


  for (int FileNumber = 0; FileNumber < FileNumberTotal; ++FileNumber) {
     double Gen_Integral = 0;
//     if (FileNumber > 0) {
//     if ((FileNumber == 1)||(FileNumber == 2)) {
     if (FileNumber == 1) {
        TFile* f_gen_w = new TFile(fname[FileNumber]);
        TDirectory* patdump = f_gen_w->GetDirectory("patdump");
        TH1F* gen_w_hist= (TH1F*) patdump->Get("gen_weight");
        for (int i = 1; i < gen_w_hist->GetNbinsX(); ++i) if (gen_w_hist->GetBinContent(i)>0.) Gen_Integral+=gen_w_hist->GetBinCenter(i)*gen_w_hist->GetBinContent(i);
//        Gen_Integral = gen_w_hist->Integral();
     }
   
     cout <<" Gen Integral " << Gen_Integral << endl;

      double Weight = 0.;
//book histogrammes
      sprintf(str,"hMZ_%d", FileNumber);
      hMZ[FileNumber] = new TH1F(str, "M_{#mu#mu} mass distribution;M_{#mu#mu}, GeV/c^{2};#events", 100, 60, 120); //histogram mass z boson declaration
      sprintf(str,"hMZ1_%d", FileNumber);
      hMZ1[FileNumber] = new TH1F(str, "M_{ee} distribution;M_{ee}, GeV/c^{2};#events", 100, 60, 120); //histogram mass z boson declaration
      sprintf(str,"hMZ2_%d", FileNumber);
      hMZ2[FileNumber] = new TH1F(str, "M_{ee} distribution + 1 jet;M_{ee}, GeV/c^{2};#events", 100, 60, 120); //histogram mass z boson declaration
      sprintf(str,"hMZ3_%d", FileNumber);
      hMZ3[FileNumber] = new TH1F(str, "M_{ee} distribution + 2 jets;M_{ee}, GeV/c^{2};#events", 100, 60, 120); //histogram mass z boson declaration
      sprintf(str,"hPU_%d", FileNumber);
      hPU[FileNumber] = new TH1F(str, "Reconstructed Number of PUs; Primary vertex number ; Events", 35, 0, 35);// Primary vertex bedore weight 
      sprintf(str,"hTrueNumInter_%d", FileNumber);
      hTrueNumInter[FileNumber] = new TH1F(str, "True Number of PUs; Primary vertex number ; Events", 35, 0, 35);// Primary true vertex bedore weight 
      sprintf(str,"hTrueNumInterWeighted_%d", FileNumber);
      hTrueNumInterWeighted[FileNumber] = new TH1F(str, "True Number of PUs; Primary vertex number ; Events", 35, 0, 35);// Primary true vertex after weight

      cout << " --- Start, opening the input file --- "<< fname[FileNumber] << endl;

      TheEvent e(fname[FileNumber]);
      if (!e) return 1;

      if (!e) {
          cerr << " Error opening the root file " << fname[FileNumber] << "\n" << endl;
      return 1;
      }

// print sample details
      cout << e.sample << endl;
  
// number of events:
     cout << "Total events: " << e.totalEvents() << endl;
     cout << "Luminocity: " << e.sample.lumi << endl; //Luminocity in pb^-1
     cout << "Cross Section: " << e.sample.CS << endl; //Cross Section in pb
     cout << "Sample statistics: " << e.sample.stat<< endl;

     if (Gen_Integral == 0) Gen_Integral = e.sample.stat;
//     if (FileNumber == 1) Gen_Integral = 4576.169e+08;
//     if (FileNumber == 1) Gen_Integral = 1.936e+08;
 
    cout << "Sample Sum of weights: " << Gen_Integral<< endl;
     for (int i = 0; i < e.totalEvents(); ++i) {
         e.readEvent(i);
         if (i%1000000 == 0) cout <<"Event = "<< i << endl;
         if (e.sample.type == 2) {Lum = e.sample.lumi; Weight = 1.;} 
         if (e.sample.type  < 2) {
                      if(e.TrueNumInter<50) {Weight = Lum *e.sample.CS * (e.Gen_weight/Gen_Integral) * LumiWeights.weight(e.TrueNumInter);}
                      else {Weight = 0; }//should not happen as we have a weight for all simulated ZJ_e.pileup.size() multiplicities!
//                      cout<< e.Gen_weight<<"   " << Weight<<endl; 
         }
         if (e.sample.type  < 2) {hTrueNumInter[FileNumber]->Fill(e.TrueNumInter); hTrueNumInterWeighted[FileNumber]->Fill(e.TrueNumInter, Weight);}
         if (e.muons.size() >= 2) {
            if ((e.muons[0].ID >= 4) && (e.muons[1].ID>=4)) {
               if ((e.muons[0].p4.Pt() > 40.)&& (e.muons[0].p4.Pt() > 40.)) {
                  const float MZ = (e.muons[0].p4 + e.muons[1].p4).M();    
                  if ((80 < MZ) && (MZ < 100)) {
                     hMZ[FileNumber]->Fill(MZ, Weight);
                     hPU[FileNumber]->Fill(e.vertices.size(), Weight);                 
//                   cout << "event "    << e.ievent << ":"
//                      << " M_Z = "   << MZ
//                      << " Nele = "  << e.muns.size()
//                      << " Njets = " << e.jets.size()
//                      << endl;
                   } //end MZ
               }// end Pt
               if ((e.muons[0].p4.Pt() > 60.)&& (e.muons[0].p4.Pt() > 40.)) {
                  const float MZ1 = (e.muons[0].p4 + e.muons[1].p4).M();    
                  if ((60 < MZ1) && (MZ1 < 120)) hMZ1[FileNumber]->Fill(MZ1, Weight);
//jets-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                  if (e.jets.size() >= 3) {
                      if ((abs(e.jets[0].p4.Eta()-e.muons[0].p4.Eta())>0.05) || (abs(e.jets[0].p4.Phi()-e.muons[0].p4.Phi())>0.05)) {
//                         const float MN1 = (e.muons[0].p4 + e.muons[1].p4 + e.jets[0].p4).M();
                         if ((e.jets[0].p4.Pt() > 40.)&&(60 < MZ1) && (MZ1 < 120)) hMZ2[FileNumber]->Fill(MZ1, Weight); 
//                       if ((e.jets[0].p4.Pt() > 40.)&&(MZ1 > 200)) hMN1[FileNumber]->Fill(MN1, Weight);
                         if (e.jets.size() >= 4) {
                              if ((abs(e.jets[0].p4.Eta()-e.muons[1].p4.Eta())>0.05) || (abs(e.jets[0].p4.Phi()-e.muons[1].p4.Phi())>0.05)) {
//                                  const float MN2 = (e.muons[0].p4 + e.muons[1].p4 + e.jets[1].p4).M();
//                                  MW = (e.muons[0].p4 + e.muons[1].p4 + e.jets[0].p4 + e.jets[1].p4).M();
                                  if ((e.jets[0].p4.Pt() > 40.)&&(e.jets[1].p4.Pt() > 40.)&&(60 < MZ1) && (MZ1 < 120)) hMZ3[FileNumber]->Fill(MZ1, Weight);
//                                if ((e.jets[0].p4.Pt() > 40.)&&(e.jets[1].p4.Pt() > 40.)&&(MZ1 > 200)) {hMZ4[FileNumber]->Fill(MZ1, Weight); hMN2[FileNumber]->Fill(MN1, Weight); hMN3[FileNumber]->Fill(MN2, Weight); hMW[FileNumber]->Fill(MW, Weight);}
                              }
                              else {
                                  if ((abs(e.jets[2].p4.Eta()-e.muons[1].p4.Eta())>0.05) || (abs(e.jets[2].p4.Phi()-e.muons[1].p4.Phi())>0.05)) {
//                                     const float MN2 = (e.muons[0].p4 + e.muons[1].p4 + e.jets[2].p4).M();
//                                     MW = (e.muons[0].p4 + e.muons[1].p4 + e.jets[0].p4 + e.jets[2].p4).M();
                                     if ((e.jets[0].p4.Pt() > 40.)&&(e.jets[2].p4.Pt() > 40.)&&(60 < MZ1) && (MZ1 < 120)) hMZ3[FileNumber]->Fill(MZ1, Weight);
//                                   if ((e.jets[0].p4.Pt() > 40.)&&(e.jets[2].p4.Pt() > 40.)&&(MZ1 > 200)) {hMZ4[FileNumber]->Fill(MZ1, Weight); hMN2[FileNumber]->Fill(MN1, Weight); hMN3[FileNumber]->Fill(MN2, Weight); hMW[FileNumber]->Fill(MW, Weight);}
                                  }
                                  else {
//                                     const float MN2 = (e.muons[0].p4 + e.muons[1].p4 + e.jets[3].p4).M();
//                                     MW = (e.muons[0].p4 + e.muons[1].p4 + e.jets[0].p4 + e.jets[3].p4).M();
                                     if ((e.jets[0].p4.Pt() > 40.)&&(e.jets[3].p4.Pt() > 40.)&&(60 < MZ1) && (MZ1 < 120)) hMZ3[FileNumber]->Fill(MZ1, Weight);
//                                   if ((e.jets[0].p4.Pt() > 40.)&&(e.jets[3].p4.Pt() > 40.)&&(MZ1 > 200)) {hMZ4[FileNumber]->Fill(MZ1, Weight); hMN2[FileNumber]->Fill(MN1, Weight); hMN3[FileNumber]->Fill(MN2, Weight); hMW[FileNumber]->Fill(MW, Weight);}
                                  }//jet2=ele1
                              }//end jet1=ele1
                         }// jet>4
                      }//end jet0=ele0
                      else {
                          if ((abs(e.jets[1].p4.Eta()-e.muons[1].p4.Eta())>0.05) || (abs(e.jets[1].p4.Phi()-e.muons[1].p4.Phi())>0.05)) {
/*                            cout<<"1Jet Pt = "<<  e.jets[0].p4.Pt()<<"  "<<"  Jet Eta = "<< e.jets[0].p4.Eta()<<"  "<<"  Jet Phi = "<< e.jets[0].p4.Phi()<< endl;
                              cout<<"1Ele1 Pt = "<<  e.muons[0].p4.Pt()<<"  "<<"  Ele1 Eta = "<< e.muons[0].p4.Eta()<<"  "<<"  Ele1 Phi = "<< e.muons[0].p4.Phi()<< endl;
                              cout<<"1Jet Pt = "<<  e.jets[1].p4.Pt()<<"  "<<"  Jet Eta = "<< e.jets[1].p4.Eta()<<"  "<<"  Jet Phi = "<< e.jets[1].p4.Phi()<< endl;
                              cout<<"1Ele2 Pt = "<<  e.muons[1].p4.Pt()<<"  "<<"  Ele2 Eta = "<< e.muons[1].p4.Eta()<<"  "<<"  Ele2 Phi = "<< e.muons[1].p4.Phi()<< endl;
                              cout<<"1Jet Pt = "<<  e.jets[2].p4.Pt()<<"  "<<"  Jet Eta = "<< e.jets[2].p4.Eta()<<"  "<<"  Jet Phi = "<< e.jets[2].p4.Phi()<< endl;
                              cout<< endl;
*/ //                           const float MN1 = (e.muons[0].p4 + e.muons[1].p4 + e.jets[1].p4).M();
                              if ((e.jets[1].p4.Pt() > 40.)&&(60 < MZ1) && (MZ1 < 120)) hMZ2[FileNumber]->Fill(MZ1, Weight);
//                            if ((e.jets[1].p4.Pt() > 40.)&&(MZ1 > 200)) hMN1[FileNumber]->Fill(MN1, Weight); 
                              if (e.jets.size() >= 4) {
                                  if ((abs(e.jets[2].p4.Eta()-e.muons[1].p4.Eta())>0.05) || (abs(e.jets[2].p4.Phi()-e.muons[1].p4.Phi())>0.05)) {
//                                     const float MN2 = (e.muons[0].p4 + e.muons[1].p4 + e.jets[2].p4).M();
//                                     MW = (e.muons[0].p4 + e.muons[1].p4 + e.jets[1].p4 + e.jets[2].p4).M();
                                     if ((e.jets[1].p4.Pt() > 40.)&&(e.jets[2].p4.Pt() > 40.)&&(60 < MZ1) && (MZ1 < 120)) hMZ3[FileNumber]->Fill(MZ1, Weight);
//                                   if ((e.jets[1].p4.Pt() > 40.)&&(e.jets[2].p4.Pt() > 40.)&&(MZ1 > 200)) {hMZ4[FileNumber]->Fill(MZ1, Weight); hMN2[FileNumber]->Fill(MN1, Weight); hMN3[FileNumber]->Fill(MN2, Weight); hMW[FileNumber]->Fill(MW, Weight);}
                                  }
                                  else {
//                                     const float MN2 = (e.muons[0].p4 + e.muons[1].p4 + e.jets[3].p4).M();
//                                     MW = (e.muons[0].p4 + e.muons[1].p4 + e.jets[1].p4 + e.jets[3].p4).M();
                                     if ((e.jets[1].p4.Pt() > 40.)&&(e.jets[3].p4.Pt() > 40.)&&(60 < MZ1) && (MZ1 < 120)) hMZ3[FileNumber]->Fill(MZ1, Weight);
//                                   if ((e.jets[1].p4.Pt() > 40.)&&(e.jets[3].p4.Pt() > 40.)&&(MZ1 > 200)) {hMZ4[FileNumber]->Fill(MZ1, Weight); hMN2[FileNumber]->Fill(MN1, Weight); hMN3[FileNumber]->Fill(MN2, Weight); hMW[FileNumber]->Fill(MW, Weight);}
                                  }//jet2=ele1
                              }//end jets>4 
                          }//end jet1=ele1
                          else {
                              const float MN1 = (e.muons[0].p4 + e.muons[1].p4 + e.jets[2].p4).M();
                              if ((e.jets[2].p4.Pt() > 40.)&&(60 < MZ1) && (MZ1 < 120)) hMZ2[FileNumber]->Fill(MZ1, Weight);
//                              if ((e.jets[2].p4.Pt() > 40.)&&(MZ1 > 200)) hMN1[FileNumber]->Fill(MN1, Weight);
                                if (e.jets.size() >= 4) {
//                                   const float MN2 = (e.muons[0].p4 + e.muons[1].p4 + e.jets[3].p4).M();
//                                   MW = (e.muons[0].p4 + e.muons[1].p4 + e.jets[2].p4 + e.jets[3].p4).M();
                                   if ((e.jets[2].p4.Pt() > 40.)&&(e.jets[3].p4.Pt() > 40.)&&(60 < MZ1) && (MZ1 < 120)) hMZ3[FileNumber]->Fill(MZ1, Weight);
//                                   if ((e.jets[2].p4.Pt() > 40.)&&(e.jets[3].p4.Pt() > 40.)&&(MZ1 > 200)) {hMZ4[FileNumber]->Fill(MZ1, Weight); hMN2[FileNumber]->Fill(MN1, Weight); hMN3[FileNumber]->Fill(MN2, Weight); hMW[FileNumber]->Fill(MW, Weight);}
                             }//jet>4 
                         }//end jet1=ele1
                      }//end jet0=ele0
                  }// jet>3
//end jets-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
              }//end Pt
           } //end ID
        }//end size
    }// end read event
  
    cout << "Total number of events with [60 < M_Z < 120] = " << hMZ[FileNumber]->GetEntries() << endl;
    cout << "Total number of events with [60 < M_Z < 120] = " << hMZ1[FileNumber]->GetEntries() << endl;
    cout << "Total number of events with [60 < M_Z < 120] + 1 jet = " << hMZ2[FileNumber]->GetEntries() << endl;
    cout << "Total number of events with [60 < M_Z < 120] + 2 jets = " << hMZ3[FileNumber]->GetEntries() << endl;
    cout <<  endl;
    cout <<  endl;
    cout <<  endl;
 } //end file loop


// prepare a picture
  gROOT->SetStyle("Plain");
  gStyle->SetOptStat("");
  gStyle->SetOptLogy(1);
  TCanvas c("zboson_advanced_MuMu", "zboson_advancedMuMu", 800, 600);
//  setTDRStyle();
//  fixOverlay();

  TLegend leg_A(0.56, 0.74, 0.94, 0.94);
  leg_A.SetTextFont(42);
  leg_A.SetTextSize(0.034);

  sprintf(str1,"Data:%4.0f", hMZ[0]->Integral()); 
  leg_A.AddEntry(hMZ[0], str1, "EP");

  if (FileNumberTotal>1) {
     sprintf(str1,"Z+jets amcatnloFXFX:%4.0f", hMZ[1]->Integral());
     leg_A.AddEntry(hMZ[1], str1,"L");
  }

  if (FileNumberTotal>2) {
    sprintf(str1,"Z+jets madgraphMLM:%4.0f", hMZ[2]->Integral());
    leg_A.AddEntry(hMZ[2], str1,"L");
  }

 c.Print(".pdf[");
//    hMZ[0]->SetDefaultSumw2();
    hMZ[0]->SetMarkerStyle(21);
    hMZ[0]->SetMarkerColor(1);
//    hMZ[0]->SetLineStyle(1);
    hMZ[0]->Draw("E1");
 
for (int i = 1; i < FileNumberTotal; ++i) {
    hMZ[i]->SetLineStyle(1);
    hMZ[i]->SetLineColor(1+i);
    hMZ[i]->Draw("HISTsame");
}
// save picture to .pdf file
    leg_A.Draw();
    c.Print(".pdf");

//    hPU[0]->SetDefaultSumw2();
    hPU[0]->SetMarkerStyle(21);
    hPU[0]->SetMarkerColor(1);
//    hPU[0]->SetLineStyle(1);
    hPU[0]->Draw("E1");
 for (int i = 1; i < FileNumberTotal; ++i) {
    hPU[i]->SetLineStyle(1);
    hPU[i]->SetLineColor(1+i);
    hPU[i]->Draw("HISTsame");
}
// save picture to .pdf file
    leg_A.Draw();
    c.Print(".pdf");

    histo_DA->DrawNormalized("");
    histo_MC->DrawNormalized("same");
    histo_DA->DrawNormalized("E1same");

 for (int i = 1; i < FileNumberTotal; ++i) {
    hTrueNumInter[i]->SetLineStyle(1);
    hTrueNumInter[i]->SetLineColor(1+i);
    hTrueNumInter[i]->DrawNormalized("HISTsame");
 }

  TLegend leg_B(0.56, 0.74, 0.94, 0.94);
  leg_B.SetTextFont(42);
  leg_B.SetTextSize(0.034);
  leg_B.AddEntry(histo_MC, "True Distribution", "L");
  leg_B.AddEntry(hMZ[0], "Data", "EP");

  if (FileNumberTotal>1) {
     leg_B.AddEntry(hMZ[1], "Z+jets amcatnloFXFX","L");
  }
  if (FileNumberTotal>2) {
    leg_B.AddEntry(hMZ[2], "Z+jets madgraphMLM","L");
  }

// save picture to .pdf file 
    leg_B.Draw();
 c.Print(".pdf");

    histo_DA->DrawNormalized("");
    histo_MC->DrawNormalized("same");
    histo_DA->DrawNormalized("sameE1");

 for (int i = 1; i < FileNumberTotal; ++i) {
    hTrueNumInterWeighted[i]->SetLineStyle(1);
    hTrueNumInterWeighted[i]->SetLineColor(1+i);
    hTrueNumInterWeighted[i]->DrawNormalized("HISTsame");
 }

// save picture to .pdf file 
    leg_B.Draw();
 c.Print(".pdf");

 c.Print(".pdf]");

// save histogram to .root file
 TFile f("zboson_advanced_MuMu.root", "RECREATE");
 for (int i = 0; i < FileNumberTotal; ++i) {
   hMZ[i]->Write(); 
   hMZ1[i]->Write();
   hMZ2[i]->Write();
   hMZ3[i]->Write();
   hPU[i]->Write();
   hTrueNumInter[i]->Write();
   hTrueNumInterWeighted[i]->Write();
 }
   histo_MC->Write();
   histo_DA->Write();    
 return 0;
}
