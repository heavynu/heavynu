#!/bin/bash

# print usage info
if [[ "$#" < "1" ]]; then
  echo "Usage:"
  echo "  $0 [path]"
  echo "    [path] - place, where the samples situated"
  echo ""
  echo "Example:"
  echo "  $0 /moscow31/heavynu/test/WR1200_nuRe500"
  exit
fi

# 1st parameter - path to directory
src=$1

# check, does samples directory exists
if [[ ! -d $src ]]; then
  echo "ERROR: Directory $src doesn't exists"
  exit 1
fi

# expand path to absolute
fullSrc=$(cd $src && pwd)
dst=$(basename $fullSrc)
echo "Source directory: $fullSrc"

if ls $fullSrc/dump*/$dst.root >& /dev/null ; then
  echo "INFO: dump complete already, skipping processing"
  exit 0
fi

# check, does dump_cfg.py in current directory
if [[ ! -f dump_cfg.py ]]; then
  echo "ERROR: Can't find dump_cfg.py in current directory"
  exit 1
fi

# check, can we run cmsRun
which cmsRun &> /dev/null
if [[ "$?" != 0 ]] ; then
  echo "ERROR: can't find cmsRun"
  echo "       CMSSW initialized not properly"
  exit 1
fi

# get total number of simulated events and files
totalEvents="0"
totalFiles="0"
for i in $(\ls -1 $fullSrc/*/RecoFromHLT_cfg.log) ; do
  # extract number of processed events
  nEvents=$(cat $i | sed -n 's,^Begin[^0-9]*\([0-9]*\).*,\1,p' | tail -n 1)
  let "totalEvents += $nEvents"
  let "totalFiles += 1"
done
echo "Total events    : $totalEvents (in $totalFiles files)"

# check total number of events
if [[ "$totalEvents" = "0" ]] ; then
  echo "ERROR: total number of events is zero"
  echo "       probably, where are no simulated events in source directory"
  echo "         $fullSrc"
  exit 1
fi

# extract global tag (AlCa), HLTmenu and CMSSW version
globalTag=$(grep "global tag" $fullSrc/*/HeavyNuGenHLT_cfg.log | cut -d "'" -f 2 | uniq)
HLTmenu=$(cat $fullSrc/*/HeavyNuGenHLT_cfg.py | grep -o "HLTrigger/Configuration/HLT_.*_cff" | cut -d _ -f 2 | uniq)
CMSSWver=$(cat $fullSrc/*/*.out | grep -o "Test Release based on.*" | grep -o "[^ ]*$" | uniq)

echo "GlobalTag (AlCa): $globalTag"
echo "HLTmenu         : $HLTmenu"
echo "CMSSW version   : $CMSSWver"

# find .log file with results of cross-section calculation
xsfile=$(\ls -1 $fullSrc/*/xs-*.log)
if [[ "$xsfile" != "" ]] ; then
  # extract pythia configuration name from file name
  pythia=$(echo "$xsfile" | sed "s,.*xs-\(.*\)\.log$,\1,")
  # extract statistics of cross-section calculation
  stat=$(cat "$xsfile" | grep "\->" | tail -n 1 | cut -d I -f 3 | tr -s ' ' ' ' | cut -d ' ' -f 2)
  # extract process cross-section in mb
  xs=$(cat "$xsfile" | grep "\->" | tail -n 1 | cut -d I -f 4)
  # convert cross-section unit to pb
  xspb=$(echo "($xs) * 10^9" | sed "s,[ED],*10^," | bc -l | sed "s,0*$,,")
  echo "Process         : $pythia"
  echo "Cross-section   : $xspb pb (stat = $stat events)"
else
  echo "WARNING: can't find file with result of cross-section calculation"
fi

# remove previous (old) dump directory
rm -rf $fullSrc/dump*

# prepare dump directory
dumppath=$fullSrc/dump_$(date +%F_%H.%M.%S)
mkdir -p $dumppath
cp dump_cfg.py $dumppath/
echo "Dump directory  : $dumppath"

# run dump process
cd $dumppath
echo "Dumping..."

# execute dumping
# put stdout and stderr to "dump.log" file
# and print progress
cmsRun dump_cfg.py samplesdir=$fullSrc 2>&1 | tee dump.log | sed -n -u -e "s,Successfully opened file,, p" | nl

# check cmsRun exit code
if [[ "${PIPESTATUS[0]}" != "0" ]]; then
  echo "ERROR: fail to run cmsRun"
  echo "       see logfile at $dumppath/dump.log"
  rm -f heavynu.root
  exit 1
fi

# check, does the all files processed
procFiles=$(cat dump.log | grep "Closed file" | wc -l)
if [[ "$procFiles" != "$totalFiles" ]] ; then
  echo "ERROR: fail to process all files"
  echo "         #processed = $procFiles"
  echo "         #total     = $totalFiles"
  echo ""
  echo "       probably, dumping was interrupted"
  echo "       see logfile at $dumppath/dump.log"
  rm -f heavynu.root
  exit 1
fi

# check output file
if [[ ! -f heavynu.root ]]; then
  echo "ERROR: Can't find dump output file ($dumppath/heavynu.root)"
  exit 1
fi

mv heavynu.root $dst.root
echo "Dump complete   : $dumppath/$dst.root"

# print number of dumped events
total=$(echo -e 'cout << ((TTree*)_file0->Get("patdump/data"))->GetEntries();\n.q\n' | \
        root -b -l $dst.root | \
        sed -n '3 p')
echo "Dumped events   : $total"

# prepare sample description string (for file sampledb.h):
# extract ECM energy
ecmGeV="$(cat $fullSrc/*/HeavyNuGenHLT_cfg.py | grep comEnergy | cut -d '(' -f 2 | cut -d . -f 1 | uniq)"
let ecmTeV=ecmGeV/1000

# extract MW, MNu, channel
wnc=$(echo $pythia | sed -n 's,^WR\([0-9]*\)_nuR\([a-z]*\)\([0-9]*\)$,\1 \3 \2,p')
wnc=( $wnc )
MW=${wnc[0]-0}
MNu=${wnc[1]-0}
channel=${wnc[2]-0}
channel=${channel/e/1}
channel=${channel/mu/2}

sampledb="{1,  $ecmTeV,  $MW,  $MNu,  $channel,  0,  $totalEvents,  $xspb,  \"$pythia\",  0,  \"$dst.root\",  \"$globalTag\",  \"$HLTmenu\",  \"$CMSSWver\",  \"\"},"
echo "sampledb record : $sampledb"


# repeat all messages to log file (dump.log)
(
  echo ""
  echo "   Here is summary of dump.sh"
  echo "   --------------------------"
  echo "Source directory: $fullSrc"
  echo "Total events    : $totalEvents (in $totalFiles files)"
  echo "GlobalTag (AlCa): $globalTag"
  echo "HLTmenu         : $HLTmenu"
  echo "CMSSW version   : $CMSSWver"
  echo "Process         : $pythia"
  echo "Cross-section   : $xspb pb (stat = $stat events)"
  echo "Dump directory  : $dumppath"
  echo "Dump complete   : $dumppath/$dst.root"
  echo "Dumped events   : $total"
  echo "sampledb record : $sampledb"
) >> dump.log
