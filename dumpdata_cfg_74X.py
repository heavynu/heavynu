# this is config file for processing of 7 TeV data samples
# configuration based on:
#   http://cmssw.cvs.cern.ch/cgi-bin/cmssw.cgi/CMSSW/PhysicsTools/PatExamples/test/patLayer1_fromRECO_7TeV_firstdata_cfg.py?view=log

import FWCore.ParameterSet.Config as cms
import sys, os

process = cms.Process("dump")

# Message logger
process.load("FWCore/MessageService/MessageLogger_cfi")
# supress message "Begin processing the ... record" to 1 time from 1000
process.MessageLogger.cerr.FwkReport.reportEvery = 100

process.options = cms.untracked.PSet(
  wantSummary = cms.untracked.bool(False)
)

# Input file
process.source = cms.Source("PoolSource",
  fileNames = cms.untracked.vstring(
#    'file:reco.root'
	'root://eoscms//eos/cms/store/group/phys_exotica/leptonsPlusJets/HeavyNu_INR/Collisions_13_TeV_2015/DoubleEG_Run2015D-PromptReco-v4_MINIAOD/027612B0-306C-E511-BD47-02163E014496.root',
#	'root://eoscms//eos/cms/store/group/phys_exotica/leptonsPlusJets/HeavyNu_INR/Collisions_13_TeV_2015/DoubleMuon_Run2015D-PromptReco-v4_MINIAOD/CAD2CB3F-DB6C-E511-BF85-02163E0143D0.root',
#	'root://eoscms//eos/cms/store/group/phys_exotica/leptonsPlusJets/HeavyNu_INR/Collisions_13_TeV_2015/Tau_Run2015D-PromptReco-v4_MINIAOD/E85DED5A-DD6B-E511-B0F2-02163E011E43.root',
#	'root://eoscms//eos/cms/store/group/phys_exotica/leptonsPlusJets/HeavyNu_INR/Collisions_13_TeV_2015/MuonEG_Run2015D-PromptReco-v4_MINIAOD/C2E3525D-F26B-E511-9A9E-02163E014327.root',
  ),
)

# try to find 'samplesdir' command line parameter
samplesdir = ""
for arg in sys.argv :
  if arg.startswith("samplesdir=") :
    samplesdir = arg.replace("samplesdir=", "")
    break

if samplesdir != "" :
  # parameter 'samplesdir' specified
  process.source.fileNames = []
  
  # list recursively all 'reco.root' files
  for root, dirs, files in os.walk(samplesdir):
    for name in files:
      if name == "reco.root":
        process.source.fileNames.append("file:" + os.path.join(root, name))
  
  # exit if no files found
  if len(process.source.fileNames) == 0:
    print "ERROR: no reco.root files found in " + samplesdir
    sys.exit(1)
  
  # print list of found files
  print len(process.source.fileNames), "files will be processed:"
  for name in process.source.fileNames:
    print "  " + name


process.maxEvents = cms.untracked.PSet(
  input = cms.untracked.int32(-1)
)

#process.SkipEvent = cms.untracked.PSet(
#   option = cms.untracked.vstring('ProductNotFound')
#)

# this is necessary to run:
process.load('Configuration.StandardSequences.GeometryRecoDB_cff')
#process.load('Configuration/StandardSequences/GeometryDB_cff')
#process.load("Configuration.StandardSequences.Geometry_cff")

process.load('Configuration/StandardSequences/FrontierConditions_GlobalTag_cff')
from Configuration.AlCa.GlobalTag import GlobalTag
#process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:run2_data', '')
process.GlobalTag.globaltag = 'GR_R_74_V15::All'
print "global tag  = ", process.GlobalTag.globaltag

process.load('Configuration.StandardSequences.MagneticField_38T_cff')
process.load('Configuration.StandardSequences.Services_cff')



# === HLT trigger event selection ===

import HLTrigger.HLTfilters.hltHighLevel_cfi
process.skimHLTFilter = HLTrigger.HLTfilters.hltHighLevel_cfi.hltHighLevel.clone()
process.skimHLTFilter.HLTPaths = cms.vstring("HLT_DoubleEle33_CaloIdL_GsfTrkIdVL_v*")
#process.skimHLTFilter.HLTPaths = cms.vstring("HLT_Ele23_Ele12_CaloIdL_TrackIdL_IsoVL_v*")
#process.skimHLTFilter.HLTPaths = cms.vstring("HLT_Ele17_Ele12_CaloIdL_TrackIdL_IsoVL_v*")
#process.skimHLTFilter.HLTPaths = cms.vstring("HLT_Ele33_CaloIdL_TrackIdL_IsoVL_PFJet30_v*")
#process.skimHLTFilter.HLTPaths = cms.vstring("HLT_Mu30_TkMu11_v*") 
#process.skimHLTFilter.HLTPaths = cms.vstring("HLT_IsoMu27_v*")  
#process.skimHLTFilter.HLTPaths = cms.vstring("HLT_Mu50_v*")                                             
#process.skimHLTFilter.HLTPaths = cms.vstring("HLT_DoubleMediumIsoPFTau35_Trk1_eta2p1_Reg_v*")
#process.skimHLTFilter.HLTPaths = cms.vstring("HLT_LooseIsoPFTau50_Trk30_eta2p1_v*")   # prescaled!
#process.skimHLTFilter.HLTPaths = cms.vstring("HLT_Mu30_Ele30_CaloIdL_GsfTrkIdVL_v*")
#process.skimHLTFilter.HLTPaths = cms.vstring("HLT_Mu17_TrkIsoVVL_Ele12_CaloIdL_TrackIdL_IsoVL_v*")
process.skimHLTFilter.throw = False
process.skimHLTFilter.TriggerResultsTag = cms.InputTag("TriggerResults","","HLT")



# === PV Filter ===

process.primaryVertexFilter = cms.EDFilter("GoodVertexFilter",
                                           vertexCollection = cms.InputTag('offlineSlimmedPrimaryVertices'),
                                           minimumNDOF = cms.uint32(4) ,
                                           maxAbsZ = cms.double(24),
                                           maxd0 = cms.double(2)
                                           )


# === HCAL Noise Filter === 

process.load('CommonTools.RecoAlgos.HBHENoiseFilterResultProducer_cfi')
process.HBHENoiseFilterResultProducer.minZeros = cms.int32(99999)
process.HBHENoiseFilterResultProducer.IgnoreTS4TS5ifJetInLowBVRegion=cms.bool(False) 
process.HBHENoiseFilterResultProducer.defaultDecision = cms.string("HBHENoiseFilterResultRun2Loose")

process.ApplyBaselineHBHENoiseFilter = cms.EDFilter('BooleanFlagFilter',
   inputLabel = cms.InputTag('HBHENoiseFilterResultProducer','HBHENoiseFilterResult'),
   reverseDecision = cms.bool(False)
)

process.ApplyBaselineHBHEIsoNoiseFilter = cms.EDFilter('BooleanFlagFilter',
   inputLabel = cms.InputTag('HBHENoiseFilterResultProducer','HBHEIsoNoiseFilterResult'),
   reverseDecision = cms.bool(False)
)

# +++++++++Heepifing gsfElectrons +++++++++++
#https://twiki.cern.ch/twiki/bin/view/CMS/CutBasedElectronIdentificationRun2#Recipe74X

from PhysicsTools.SelectorUtils.tools.vid_id_tools import *
#switchOnVIDElectronIdProducer(process, DataFormat.AOD)
switchOnVIDElectronIdProducer(process, DataFormat.MiniAOD)
setupAllVIDIdsInModule(process,'RecoEgamma.ElectronIdentification.Identification.heepElectronID_HEEPV60_cff',setupVIDElectronSelection)
#setupAllVIDIdsInModule(process,'RecoEgamma.ElectronIdentification.Identification.cutBasedElectronID_PHYS14_PU20bx25_V2_cff',setupVIDElectronSelection)
setupAllVIDIdsInModule(process,'RecoEgamma.ElectronIdentification.Identification.cutBasedElectronID_Spring15_25ns_V1_cff',setupVIDElectronSelection)



#===================================================
process.out = cms.untracked.PSet()
process.out.outputCommands = cms.untracked.vstring("")

#=================SuperClusters ===================
process.mergedSuperClusters = cms.EDProducer(
  "EgammaSuperClusterMerger",
  src = cms.VInputTag(
#    cms.InputTag("correctedHybridSuperClusters"),
#    cms.InputTag("correctedMulti5x5SuperClustersWithPreshower")
     cms.InputTag("reducedEgamma:reducedSuperClusters")
  )
)

# === PAT dump module ===
# dump module
process.patdump = cms.EDAnalyzer('PATDump',
  # input collections
#  electronTag = cms.InputTag("gedGsfElectrons"),
  electronTag = cms.InputTag("slimmedElectrons"),
#  superclasterTag = cms.InputTag("mergedSuperClusters"),
  superclasterTag = cms.InputTag("reducedEgamma:reducedSuperClusters"),
#  superclasterTag = cms.InputTag(""),
#  muonTag     = cms.InputTag("muons"),
  muonTag     = cms.InputTag("slimmedMuons"),  
#  tauTag      = cms.InputTag("pfTausEI"),
  tauTag      = cms.InputTag("slimmedTaus"),  
#  jetTag      = cms.InputTag("ak4PFJetsCHS"),
  jetTag      = cms.InputTag("slimmedJets"),
#  photonTag   = cms.InputTag("gedPhotons"),
  photonTag   = cms.InputTag("slimmedPhotons"),    
#  metTag      = cms.InputTag("pfMet"),
  metTag      = cms.InputTag("slimmedMETs"), 
#  metTag      = cms.InputTag("slimmedMETsNoHF"),     
  triggerTag = cms.InputTag("hltTriggerSummaryAOD"),
  pileupTag = cms.InputTag(""),
  GeneratorTag = cms.InputTag(""),
  minLeptons = cms.int32(2),
  minJets    = cms.int32(0),
  electronEcut= cms.double(30.),
  clusterEcut= cms.double(30.),
  muonEcut= cms.double(30.),
  tauEcut= cms.double(30.),
  jetEcut= cms.double(30.),
  photonEcut= cms.double(30.),
  METEcut= cms.double(0.),
#  conversions = cms.InputTag('allConversions'), 
  conversions = cms.InputTag('reducedConversions'),  
#  eleVetoIdMap = cms.InputTag("egmGsfElectronIDs:cutBasedElectronID-PHYS14-PU20bx25-V2-standalone-veto"),
  eleVetoIdMap = cms.InputTag("egmGsfElectronIDs:cutBasedElectronID-Spring15-25ns-V1-standalone-veto"),
#  eleLooseIdMap = cms.InputTag("egmGsfElectronIDs:cutBasedElectronID-PHYS14-PU20bx25-V2-standalone-loose"),
  eleLooseIdMap = cms.InputTag("egmGsfElectronIDs:cutBasedElectronID-Spring15-25ns-V1-standalone-loose"),  
#  eleMediumIdMap = cms.InputTag("egmGsfElectronIDs:cutBasedElectronID-PHYS14-PU20bx25-V2-standalone-medium"),
  eleMediumIdMap = cms.InputTag("egmGsfElectronIDs:cutBasedElectronID-Spring15-25ns-V1-standalone-medium"),
#  eleTightIdMap = cms.InputTag("egmGsfElectronIDs:cutBasedElectronID-PHYS14-PU20bx25-V2-standalone-tight"),
  eleTightIdMap = cms.InputTag("egmGsfElectronIDs:cutBasedElectronID-Spring15-25ns-V1-standalone-tight"),
  eleHEEPIdMap = cms.InputTag("egmGsfElectronIDs:heepElectronID-HEEPV60"),
  debug = cms.bool(False)
  #debug = cms.bool(True)
)

# output file
process.TFileService = cms.Service("TFileService",
  fileName = cms.string('heavynu.root')
)

# Define the paths
process.p = cms.Path(
  process.skimHLTFilter*
  process.primaryVertexFilter*
  process.HBHENoiseFilterResultProducer* #produces HBHE baseline bools
  process.ApplyBaselineHBHENoiseFilter*  #reject events based 
  process.ApplyBaselineHBHEIsoNoiseFilter* #reject events based  < 10e-3 mistake rate 
  #process.mergedSuperClusters*
  process.egmGsfElectronIDSequence*
  process.patdump
)

