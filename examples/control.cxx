#include "TH1.h"
#include "TCanvas.h"
#include "TROOT.h"

#include "PATDump/src/format.h"
#include <iostream>
using namespace std;

void control(const char* fname = "heavynu.root")
{
  TheEvent e(fname);
  if (!e) return;
  
  const int nEvents = e.totalEvents();
  cout << "Total events = " << nEvents << endl;
  
  // HLT bits names:
  const int nBits = e.HLT().names.size();
  cout << "HLT menu = " << e.HLT().HLTmenu << "\n"
       << "HLT bits = " << nBits << "\n"
       << endl;
  
  // set plain style
  gROOT->SetStyle("Plain");
  
  // histograms
  TH1F hHLT("HLTbits", "HLT efficiency;#bit;%", nBits, 0, nBits);
  TH1F hWR(  "WR", "W_{R} reconstructed mass;mass, GeV/c^{2};events/25 GeV", 100, 0, 2500);
  TH1F hNuR("NuR", "nu_{R} reconstructed mass;mass, GeV/c^{2};events/25 GeV", 100, 0, 2500);
  TH1F hMll("Mll", "lepton-lepton pair invariant mass;mass, GeV/c^{2};events/25 GeV", 100, 0, 2500);
  TH1F hMjj("Mjj", "jet-jet pair invariant mass;mass, GeV/c^{2};events/25 GeV", 100, 0, 2500);
  TH1F hElRes("ElRes", "electron energy reconstruction resolution", 100, -0.3, 0.3);
  TH1F hElMult("ElMult", "electrons multiplicity", 10, 0, 10);
  hElMult.GetXaxis()->SetNdivisions(110);
  TH1F hJetMult("JetMult", "jets multiplicity", 20, 0, 20);
  hJetMult.GetXaxis()->SetNdivisions(210);
  TH1F hJetPt("JetPt", "jets pT;pT, GeV/c^{2};#jets/10 GeV", 40, 0, 400);
  
  for (int i = 0; i < nEvents; ++i) {
    e.readEvent(i);
    
    // HLT bits distribution
    for (int j = 0; j < nBits; ++j)
      if ((*e.hltBits)[j])
        hHLT.Fill(j);
    
    // electron energy resolution
    for (unsigned int j = 0; j < e.electrons.size(); ++j) {
      const Particle& reco = e.electrons[j];
      if (reco.mcId >= 0) {
        const Particle& mc = e.gens[reco.mcId];
        const float r = (reco.p4.E() - mc.p4.E())/mc.p4.E();
        hElRes.Fill(r);
      }
    }
    
    // mass distributions
    if ((e.electrons.size() >= 2) && (e.jets.size() >= 2)) {
      const float WR = (e.electrons[0].p4 + e.electrons[1].p4 + e.jets[0].p4 + e.jets[1].p4).M();
      hWR.Fill(WR, e.weight);
      
      //const float NuR_l0 = (e.electrons[0].p4 + e.jets[0].p4 + e.jets[1].p4).M();
      const float NuR_l1 = (e.electrons[1].p4 + e.jets[0].p4 + e.jets[1].p4).M();
      hNuR.Fill(NuR_l1, e.weight);
      
      const float Mll = (e.electrons[0].p4 + e.electrons[1].p4).M();
      hMll.Fill(Mll, e.weight);
      
      const float Mjj = (e.jets[0].p4 + e.jets[1].p4).M();
      hMjj.Fill(Mjj, e.weight);
    }
    
    // Multiplicity
    hElMult.Fill(e.electrons.size());
    hJetMult.Fill(e.jets.size());
    
    for (unsigned int j = 0; j < e.jets.size(); ++j) {
      hJetPt.Fill(e.jets[j].p4.pt());
    }
  }
  
  // print all histograms to .pdf
  TCanvas c("control", "control");
  c.Print(".pdf[");
  
  hWR.Fit("gaus");
  hWR.Draw();     c.Print(".pdf");
  hNuR.Draw();    c.Print(".pdf");
  hMll.Draw();    c.Print(".pdf");
  hMjj.Draw();    c.Print(".pdf");
  
  hHLT.Scale(100.0/nEvents);
  hHLT.Draw();   c.Print(".pdf");
  
  hElRes.Fit("gaus");
  hElRes.Draw(); c.Print(".pdf");
  
  hElMult.Draw();  c.Print(".pdf");
  hJetMult.Draw(); c.Print(".pdf");
  
  hJetPt.Draw(); c.Print(".pdf");
  
  c.Print(".pdf]");
}
