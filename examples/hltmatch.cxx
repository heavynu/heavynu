#include "PATDump/src/format.h"
#include <iostream>
using namespace std;

// to run do:
//   $ cd CMSSW/src/heavynu
//   $ root -b -q -l examples/hltmatch.cxx+


void hltmatch(const char* fname = "heavynu.root")
{
  TheEvent e(fname);
  if (!e) return;
  
  e.readEvent(0);                   // read first event
  const HLTtable& hlt0 = e.HLT();   // HLT table corresponds to first event
  cout << "First HLT table:" << "\n"
       << "L1 menu = " << hlt0.L1menu << "\n"
       << "HLT menu = " << hlt0.HLTmenu << "\n"
       << "#bits = " << hlt0.names.size() << "\n"
       << "bit \tprescale \tname" << endl;
  
  for (size_t i = 0; i < hlt0.names.size(); ++i)
    cout << i << " \t" << hlt0.prescales[i] << " \t" << hlt0.names[i] << endl;
  
  // note: all above text output you can replace by simple
  //   cout << hlt0 << endl;
  
  // print all HLT tables from file <fname>
  cout << "\n"
       << "All HLT tables\n";
  e.printHLT();
  
  // events:
  cout << "Total events: " << e.totalEvents() << endl;
  
  for (int i = 0; i < e.totalEvents(); ++i) {
    e.readEvent(i);                  // read event
    const HLTtable& hlt = e.HLT();   // pick HLT table for the event
    
    cout << "run / lumi / event = " << e.irun << " / " << e.ilumi << " / " << e.ievent << endl;
    
    // print HLT triggers matches for all electrons
    cout << "Electrons HLT bits matches:" << endl;
    for (size_t j = 0; j < e.electrons.size(); ++j) {
      cout << " #" << j << " pt = " << e.electrons[j].p4.Pt() << " matches:" << endl;
      for (size_t k = 0; k < e.electrons[j].bits.size(); ++k) {
        const int bit = e.electrons[j].bits[k];
        cout << "  bit " << bit << " " << hlt.names[bit] << endl;
      }
    }
  }
}
