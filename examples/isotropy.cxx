#include "PATDump/src/format.h"

// c++
#include <iostream>
using namespace std;

// ROOT
#include "TH1.h"
#include "TCanvas.h"
#include "TROOT.h"

void isotropy(const char* fname = "/moscow31/heavynu/data/132605.root")
{
  TheEvent evt(fname);
  if (!evt) return;
  
  // total number of events:
  const int n = evt.totalEvents();
  cout << "Total events: " << n << endl;
  
  // book histograms
  TH1::SetDefaultSumw2();
  TH1F hEvt("hEvt", "Number of events vs. time;time, h;#events/0.1h", 100, -5, 5);
  TH1F hJets("hJets", "Number of jets vs. time;time, h;#jets/0.1h", 100, -5, 5);
  TH1F hJets20("hJets20", "Number of jets (pT > 20 GeV/c) vs. time;time, h;#jets/0.1h", 100, -5, 5);
  TH1F hJetsPt("hJetsPt", "Jets pT distribution; pT, GeV/c;#jets/1 GeV/c", 50, 0, 50);
  
  // get time of first event
  evt.readEvent(0);
  const time_t start = evt.time();
  
  // fill histograms
  for (int i = 0; i < n; ++i) {
    evt.readEvent(i);
    
    // show progress - print '.' each 5%
    if (i % (n / 20) == 0) cout << "." << flush;
    
    const time_t sec = evt.time() - start;
    const float h = sec/3600.;
    hEvt.Fill(h);
    
    hJets.Fill(h, evt.jets.size());
    
    int nJets10 = 0;
    for (size_t j = 0; j < evt.jets.size(); ++j)
      if (evt.jets[j].p4.pt() > 20)
        nJets10++;
    
    hJets20.Fill(h, nJets10);
    
    for (size_t j = 0; j < evt.jets.size(); ++j)
      hJetsPt.Fill(evt.jets[j].p4.pt());
  }
  cout << endl;
  
  // calculate ration histogram
  // hJetsR = hJets20 / hJets
  TH1F hJetsR(hJets20);
  hJetsR.SetNameTitle("hJetsR", "#jets (pT > 20)/#jets");
  hJetsR.Divide(&hJets);
  
  // draw histograms
  gROOT->SetStyle("Plain");
  
  TCanvas c("isotropy", "isotropy");
  c.Print(".ps[");
  
  hJetsR.Draw("E1");
  c.Print(".ps");
  
  hJetsPt.Draw("hist");
  c.Print(".ps");
  
  hEvt.Draw("hist");
  c.Print(".ps");
  
  hJets.Draw("hist");
  c.Print(".ps");
  
  hJets20.Draw("hist");
  c.Print(".ps");
  
  // superimpose previous 3 histograms
  // hEvt.Draw("hist");
  // hJets.SetLineColor(kBlue);
  // hJets.Draw("hist same");
  // hJets20.SetLineColor(kRed);
  // hJets20.Draw("hist same");
  // c.Print(".ps");
  
  c.Print(".ps]");
}
