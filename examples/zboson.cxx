// root
#include "TCanvas.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TStyle.h"
#include "TFile.h"
#include "TROOT.h"

// c++
#include <iostream>
using namespace std;

// heavynu
#include "PATDump/src/format.h"


// This is a small script to demonstrate a way how to
// select events with Z-bosons.
//
// to run do:
//   $ cd CMSSW/src/heavynu
//   $ root -b -q -l 'examples/zboson.cxx+("/moscow31/heavynu/data/Aug17th_EG.root")'

void zboson(const char* fname = "heavynu_data.root")
{
  TheEvent e(fname);
  if (!e) return;
  
  // print sample details
  cout << e.sample << endl;
  
  // number of events:
  cout << "Total events: " << e.totalEvents() << endl;
  
  TH1::SetDefaultSumw2();
  TH1I hMZ("hMZ", "M_{ee} mass distribution;M_{ee}, GeV/c^{2};#events", 100, 50, 150);
  
  for (int i = 0; i < e.totalEvents(); ++i) {
    e.readEvent(i);
    
    if (e.electrons.size() >= 2) {
      const float MZ = (e.electrons[0].p4 + e.electrons[1].p4).M();
      
      if ((50 < MZ) && (MZ < 150)) {
        hMZ.Fill(MZ);
        
        cout << "event "    << e.ievent << ":"
             << " M_Z = "   << MZ
             << " Nele = "  << e.electrons.size()
             << " Njets = " << e.jets.size()
             << endl;
      }
    }
  }
  
  cout << "Total number of events with [50 < M_Z < 150] = " << hMZ.GetEntries() << endl;
  
  // prepare a picture
  gROOT->SetStyle("Plain");
  TCanvas c("zboson", "zboson", 800, 600);
  
  // fit by exponent + gaussian
  TF1* fitf = new TF1("fitf", "[0]*exp(-[1]*x) + [2]*exp(-([3] - x)*([3] - x)/(2*[4]*[4]))", 0, 200);
  fitf->SetNpx(500);
  fitf->SetParameters(10, 0.01, 10, 90, 10);
  hMZ.Fit(fitf, "IMN");
  const double A = fitf->GetParameter(2); // area under gaussian
  
  // draw experimental points
  hMZ.SetMarkerStyle(21);
  hMZ.Draw();
  
  // draw fit to background
  fitf->SetLineColor(kBlue - 3);
  fitf->SetLineStyle(7);
  fitf->SetLineWidth(5);
  fitf->SetParameter(2, 0);
  fitf->DrawCopy("same");
  
  // draw fit to background + signal
  fitf->SetLineColor(kRed - 3);
  fitf->SetLineStyle(1);
  fitf->SetLineWidth(3);
  fitf->SetParameter(2, A);
  fitf->Draw("same");
  
  // save picture to .ps file
  c.Print(".ps");
  
  // save histogram to .root file
  TFile f("zboson.root", "RECREATE");
  hMZ.Write();
}
