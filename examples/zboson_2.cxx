// root
#include "TCanvas.h"
#include "THStack.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TStyle.h"
#include "TFile.h"
#include "TROOT.h"
#include "TPad.h"

// c++
#include <iostream>
#include <fstream>
using namespace std;

// heavynu
#include "/home/ivan/science/CMS/format.h"


// This is a small script to demonstrate a way how to
// select events with Z-bosons.
//
// to run do:
//   $ cd CMSSW/src/heavynu
//   $ root -b -q -l 'examples/zboson.cxx+("/moscow31/heavynu/data/Aug17th_EG.root")'

vector<double> generate_flat10_weights(TH1D* data_npu_estimated){
   // see SimGeneral/MixingModule/python/mix_E7TeV_FlatDist10_2011EarlyData_inTimeOnly_cfi.py; copy and paste from there:
// for spring11 and summer11, if out of time is not important:
//   const double npu_probs[25] = {0.0698146584, 0.0698146584, 0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,0.0698146584,
//          0.0630151648,0.0526654164,0.0402754482,0.0292988928,0.0194384503,0.0122016783,0.007207042,0.004003637,0.0020278322,
//          0.0010739954,0.0004595759,0.0002229748,0.0001028162,4.58337152809607E-05 /* <-- 24 */};
// for summer11 PU_S4, obtained by averaging:
   const double npu_probs[25] = {0.104109, 0.0703573, 0.0698445, 0.0698254, 0.0697054, 0.0697907, 0.0696751, 0.0694486, 0.0680332,
                                 0.0651044, 0.0598036, 0.0527395, 0.0439513, 0.0352202, 0.0266714, 0.019411, 0.0133974, 0.00898536,
                                 0.0057516, 0.00351493, 0.00212087, 0.00122891, 0.00070592, 0.000384744, 0.000219377};
   vector<double> result(25);
   double s = 0.0;
   for(int npu=0; npu<25; ++npu){
       double npu_estimated = data_npu_estimated->GetBinContent(data_npu_estimated->GetXaxis()->FindBin(npu));                              
       result[npu] = npu_estimated / npu_probs[npu];
       s += npu_estimated;
   }
   // normalize weights such that the total sum of weights over thw whole sample is 1.0, i.e., sum_i  result[i] * npu_probs[i] should be 1.0 (!)
   for(int npu=0; npu<25; ++npu){
       result[npu] /= s;
   }
   return result;
}

void zboson_2()
{
  //*********** Pile Up   ********** //

  TFile* f_A1 = new TFile("/home/ivan/science/CMS/PileUp/PileUp.root");
 
  TH1D* histo_A1 = (TH1D*) f_A1->Get("pileup"); // pileup histo

 //file name 
  
 const int FileNumber = 2;

 const char* fname[FileNumber];
 fname[0] = "/home/ivan/science/CMS/CMS_data/heavynu_data.root";  // real data
 fname[1] = "/home/ivan/science/CMS/CMS_data/heavynu_mc.root"; // background Z boson
 //fname[2] = "/home/ivan/science/CMS/CMS_data/T_tW-channel-DR_TuneZ2star_8TeV-powheg-tauola.root"; // backround RD from T_tW channel
 //fname[3] = "/home/ivan/science/CMS/CMS_data/Tbar_tW-channel-DR_TuneZ2star_8TeV-powheg-tauola.root"; 
 //fname[4] = "/home/ivan/science/CMS/CMS_data/WJetsToLNu_TuneZ2Star_8TeV-madgraph-tarball-v2-minus3.root"; 
 //fname[5] = "/home/ivan/science/CMS/CMS_data/WW_TuneZ2star_8TeV_pythia6_tauola_minus7.root"; 

  //variables declaration
  float Tune_A = 1.;
  float Lum[FileNumber] = {0} ; // luminosity
  float Weight_PileUp ; // pileup weight 
  float HEEP_A_B = 0.983; //это надо спросить у Данилы 
  float A_Wei = 2.;  //это надо спросить у Данилы
  //root histogram and function declaration
  TH1::SetDefaultSumw2();
  TH1F *hMZ[FileNumber]; //histogram mass z boson declaration
  TF1* fitf[FileNumber]; //histogram mass z boson fit declaration 
  TH1F *hPU[FileNumber]; //histogram  primary vertex
  TH1F *hPU_A[FileNumber]; //histogram  primary vertex
 
  double A[FileNumber] = {0}; // area under gaussian

  // ********** Read Data ********** //

  for (int j = 0; j < FileNumber; j++ )    { //file loop

 hMZ[j] = new TH1F("hMZ", "M_{ee} mass distribution;M_{ee}, GeV/c^{2};#events", 100, 50, 150); //histogram mass z boson declaration
 fitf[j] = new TF1("fitf", "[0]*exp(-[1]*x) + [2]*exp(-([3] - x)*([3] - x)/(2*[4]*[4]))", 0, 200);//histogram mass z boson fit declaration 

 hPU[j] = new TH1F("", "PU_A; Primary vertex number ; Events", 35, 0, 35);// Primary vertex bedore weight 
 hPU_A[j] = new TH1F("", "PU_A; Primary vertex number ; Events", 35, 0, 35);// Primary vertex after weight 
 
  TheEvent e(fname[j]); //read events from j file 
  if (! e) return;  //считываем этот файл 
  
  // print sample details
    cout << e.sample << endl;
   Lum[j] = e.sample.lumi; //Luminocity in pb^-1
  // number of events:
    cout << "Total events: " << e.totalEvents() << endl;
    cout << "Luminocity: " <<Lum[j]<< endl;

  for (int i = 0; i < e.totalEvents(); ++i) {
    e.readEvent(i);
 
  if (j > 0) { // 0 -file with real data 
    
  if(e.pileupIT()<25){ //это надо спросить у Данилы
  Weight_PileUp = Tune_A*(Lum[0]/100)*generate_flat10_weights(histo_A1)[e.pileupIT()];}
 // Weight_PileUp = Tune_A*generate_flat10_weights(histo_A1)[e.pileupIT()];}
  else{ //should not happen as we have a weight for all simulated ZJ_e.pileup.size() multiplicities!
  Weight_PileUp = 0;}

  //  A_Wei=A_Wei*HEEP_A_B;
  //  Weight_PileUp = Weight_PileUp * A_Wei;
     } 



    if (e.electrons.size() >= 2) {
      const float MZ = (e.electrons[0].p4 + e.electrons[1].p4).M();
      
    if ((50 < MZ) && (MZ < 150)) { // cut z boson mass
     
    hMZ[j]->Fill(MZ);   // 0 -file with real data 
        
    hPU[j]->Fill(e.vertices.size());
    hPU_A[j]->Fill(e.vertices.size(), Weight_PileUp);    
   
   //if (j == 0) { // 0 -file with real data 
   // hPU_A[j]->Fill(e.vertices.size());}
   // else {
   // hPU_A[j]->Fill(e.vertices.size(),Weight_PileUp);}
   
   //     cout << "event "    << e_MC.ievent << ":"
   //          << " M_Z = "   << MZ
   //          << " Nele = "  << e_MC.electrons.size()
   //          << " Njets = " << e_MC.jets.size()
   //          << endl;
      }//cut z boson mass
    }//cut electrons size
  }//events loop
}//files loop

// prepare a picture
  gROOT->SetStyle("Plain");
 // TCanvas cRD("zboson", "zboson", 800, 600);

// save picture to .ps file //
  
  TCanvas c("zboson", "zboson", 800, 600);
//  THStack *hs = new THStack("hMZ", "M_{ee} mass distribution;M_{ee}, GeV/c^{2};#events");
  c.Print(".pdf[");

 for (int l = 0; l < FileNumber; ++l ){

   // fit by exponent + gaussian
  
  fitf[l]->SetNpx(500);
  fitf[l]->SetParameters(10, 0.01, 10, 90, 10);
  hMZ[l]->Fit(fitf[l], "IMN");
  A[l] = fitf[l]->GetParameter(2); // area under gaussian
 
   // draw MC points
  hMZ[l]->SetMarkerStyle(21);
  hMZ[l]->Draw();  //c.Print(".pdf");
  
  // draw fit to background for MC
  fitf[l]->SetLineColor(kBlue - 3);
  fitf[l]->SetLineStyle(7);
  fitf[l]->SetLineWidth(5);
  fitf[l]->SetParameter(2, 0);
  fitf[l]->DrawCopy("same");  //c.Print(".pdf");
  
  // draw fit to background + signal for MC
  fitf[l]->SetLineColor(kRed - 3);
  fitf[l]->SetLineStyle(1);
  fitf[l]->SetLineWidth(3);
  fitf[l]->SetParameter(2, A[l]);
  fitf[l]->Draw("same");  c.Print(".pdf");
  
 } //file loop

  // draw primary vertex

 hPU[0]->SetMarkerStyle(21);
 hPU[0]->SetMarkerColor(kGreen);
 hPU[1]->SetMarkerStyle(21);
 hPU[1]->SetMarkerColor(kRed);

 hPU[0]->Draw(); hPU[1]->Draw("same"); c.Print(".pdf"); 
 
 hPU_A[0]->SetMarkerStyle(21);
 hPU_A[0]->SetMarkerColor(kGreen);
 hPU_A[1]->SetMarkerStyle(21);
 hPU_A[1]->SetMarkerColor(kRed);

 hPU_A[0]->Draw(); hPU_A[1]->Draw("same"); c.Print(".pdf");
  

 
  // draw stack MC 
 /*
 hMZ[1]->SetMarkerStyle(21);
 hMZ[1]->SetMarkerColor(kRed);
 hMZ[2]->SetMarkerStyle(21);
 hMZ[2]->SetMarkerColor(kGreen);
 hs->Add(hMZ[1]);
 hs->Add(hMZ[2]);
 hs->Draw(); c.Print(".pdf");
  */
 // hs->Draw(); c.Print(".pdf");
  c.Print(".pdf]");

  // save histogram to .root file
  TFile f("zboson_2.root", "RECREATE");
for (int l = 0; l < FileNumber; ++l ){
  hMZ[l]->Write();
  hPU_A[l]->Write();}
}
