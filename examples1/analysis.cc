// This is a small program to read and analyse heavynu trees
//
// to run do:
//   $ make analysis
//   $ analysis FILENAME

#include "../PATDump/src/format.h"
#include <iostream>

using namespace std;


int main(int argc, char *argv[])
{
  char fname[300];
  if(argc<1) sprintf(fname,"%s","heavynu.root");
  else sprintf(fname,"%s",argv[1]);
  cout<<fname<<std::endl;

  TheEvent e(fname);

  if (!e) return 1;
  
  // HLT
  const HLTtable& hlt = e.HLT();
  cout << "HLT summary:" << "\n"
       << "menu = " << hlt.HLTmenu << "\n"
       << "bits = " << hlt.names.size() << "\n"
       << "bit \tprescale \tname" << endl;
  
  for (size_t i = 0; i < hlt.names.size(); ++i)
    cout << i << " \t" << hlt.prescales[i] << " \t" << hlt.names[i] << endl;
  
  // events:
  cout << "Total events: " << e.totalEvents() << endl;
  
  for (int i = 0; i < e.totalEvents(); ++i) {
    e.readEvent(i);
    // now we can work with vectors e.electrons[], e.muons[], ...
    
    if ((e.electrons.size() >= 2) && (e.jets.size() >= 2)) {
      const float WR = (e.electrons[0].p4 + e.electrons[1].p4 + e.jets[0].p4 + e.jets[1].p4).M();
      cout << "event "    << e.ievent
           << ": M_WR = " << WR
           << ", MET = "  << e.mets[0].p4.Et()
           << ", pileup = "  << e.pileup.size()
           << ", vertices = "  << e.vertices.size()
           << endl;
    }
  }
}
