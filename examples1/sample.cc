// This is a small program to find samples in sampledb
//
// to run do:
//   $ make sample
//   $ sample

#include "../sampledb.h"
#include <iostream>

using namespace std;


int main()
{
  // database of all samples
  const SampleDB db;
  // print database entries
  cout << db;
  cout << endl;
  
  // select and print samples with MW = 1500 and MNu = 600
  cout << db.find("MW", 1500).find("MNu", 600);
  cout << endl;
  
  // select signal samples from muon channel
  const SampleDB mudb = db.find("type", 1).find("channel", 2);
  
  // print names and weights of this samples
  const float L = 100; // integrated luminosity [pb-1]
  for (int i = 0; i < mudb.size(); ++i)
    cout << "name = " << mudb[i].name << ", "
         << "weight = " << L*mudb[i].CS/mudb[i].stat << "\n";
  cout << endl;
  
  // find and print sample with fale name "WR1500_nuRe600_1e31_pat2.root"
  const Sample sa = db.find("fname", "WR1500_nuRe600_1e31_pat2.root")[0];
  cout << sa;
  cout << endl;
  
  // do two selections and merge (union) result:
  const SampleDB db2 = db.find("ECM", 10).find("name", "Zee") +
                       db.find("ECM", 10).find("name", "Zmumu");
  cout << db2;
  cout << endl;
  
  // another way, but same result:
  const SampleDB db3 = (db.find("name", "Zee") + db.find("name", "Zmumu")).find("ECM", 10);
  cout << db3;
  cout << endl;
  
  return 0;
}
