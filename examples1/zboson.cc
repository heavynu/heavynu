// This is a small program to demonstrate a way how to
// select events with Z-bosons.
//
// to run do:
//   $ make zboson
//   $ zboson FILENAME

// Headers:
//
// c++
#include <iostream>

// root
#include "TCanvas.h"
#include "TH1.h"
#include "TH2.h"
#include "TF1.h"
#include "TStyle.h"
#include "TFile.h"
#include "TROOT.h"

// heavynu
#include "../PATDump/src/format.h"

using namespace std;


int main(int argc, char* argv[]) {

  // Check that correct number of command-line arguments
  if (argc != 2) {
    cerr << " Unexpected number of command-line arguments. \n"
         << " Usage: zboson FILENAME \n" << endl;
    return 1;
  }

  cout << " --- Start, opening the input file --- " << endl;

  TheEvent e(argv[1]);
  if (!e) return 1;

  if (!e) {
    cerr << " Error opening the root file " << argv[1] << "\n" << endl;
    return 1;
  }

  // print sample details
  cout << e.sample << endl;
  
  // number of events:
  cout << "Total events: " << e.totalEvents() << endl;
  
  TH1::SetDefaultSumw2();
  TH1I hMZ("hMZ", "M_{ee} mass distribution;M_{ee}, GeV/c^{2};#events", 100, 50, 150);
  
  for (int i = 0; i < e.totalEvents(); ++i) {
    e.readEvent(i);
    cout << i << endl;
    if (e.electrons.size() >= 2) {
      const float MZ = (e.electrons[0].p4 + e.electrons[1].p4).M();
      
      if ((50 < MZ) && (MZ < 150)) {
        hMZ.Fill(MZ);
        
        cout << "event "    << e.ievent << ":"
             << " M_Z = "   << MZ
             << " Nele = "  << e.electrons.size()
             << " Njets = " << e.jets.size()
             << endl;
      }
    }
  }
  
  cout << "Total number of events with [50 < M_Z < 150] = " << hMZ.GetEntries() << endl;
  
  // prepare a picture
  gROOT->SetStyle("Plain");
  TCanvas c("zboson", "zboson", 800, 600);
  
  // fit by exponent + gaussian
  TF1* fitf = new TF1("fitf", "[0]*exp(-[1]*x) + [2]*exp(-([3] - x)*([3] - x)/(2*[4]*[4]))", 0, 200);
  fitf->SetNpx(500);
  fitf->SetParameters(10, 0.01, 10, 90, 10);
  hMZ.Fit(fitf, "IMN");
  const double A = fitf->GetParameter(2); // area under gaussian
  
  // draw experimental points
  hMZ.SetMarkerStyle(21);
  hMZ.Draw();
  
  // draw fit to background
  fitf->SetLineColor(kBlue - 3);
  fitf->SetLineStyle(7);
  fitf->SetLineWidth(5);
  fitf->SetParameter(2, 0);
  fitf->DrawCopy("same");
  
  // draw fit to background + signal
  fitf->SetLineColor(kRed - 3);
  fitf->SetLineStyle(1);
  fitf->SetLineWidth(3);
  fitf->SetParameter(2, A);
  fitf->Draw("same");
  
  // save picture to .pdf file
  c.Print(".pdf");
  
  // save histogram to .root file
  TFile f("zboson.root", "RECREATE");
  hMZ.Write();

  return 0;
}
