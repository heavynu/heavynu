#!/bin/csh
setenv MCGENERATORS /afs/cern.ch/sw/lcg/external/MCGenerators
setenv LD_LIBRARY_PATH ${MCGENERATORS}/pythia6/419/slc4_ia32_gcc34/lib/:${MCGENERATORS}/lhapdf/5.6.0/slc4_ia32_gcc34/lib/:../lib/
setenv LHAPATH ${MCGENERATORS}/lhapdf/5.6.0/share/lhapdf/PDFsets
setenv MCGREPLICA ${MCGENERATORS}
setenv ROOTSYS ${MCGENERATORS}/../root/5.21.04/slc4_ia32_gcc34/root

if( ! $?LD_LIBRARY_PATH ) then
  setenv LD_LIBRARY_PATH /afs/cern.ch/sw/lcg/external/MCGenerators/../HepMC/2.03.09/slc4_ia32_gcc34/lib
else
  setenv LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:/afs/cern.ch/sw/lcg/external/MCGenerators/../HepMC/2.03.09/slc4_ia32_gcc34/lib
endif
setenv LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:/afs/cern.ch/sw/lcg/external/MCGenerators/../clhep/1.9.3.1/slc4_ia32_gcc34/lib
setenv LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:/afs/cern.ch/sw/lcg/external/MCGenerators/../root/5.16.00/slc4_ia32_gcc34/root/lib
if( ! $?LD_LIBRARY_PATH ) then
  setenv LD_LIBRARY_PATH /afs/cern.ch/sw/lcg/external/MCGenerators/lhapdf/5.6.0/slc4_ia32_gcc34/lib
else
  setenv LD_LIBRARY_PATH ${LD_LIBRARY_PATH}:/afs/cern.ch/sw/lcg/external/MCGenerators/lhapdf/5.6.0/slc4_ia32_gcc34/lib
endif
setenv LHAPATH /afs/cern.ch/sw/lcg/external/MCGenerators/lhapdf/5.6.0/share/PDFsets
