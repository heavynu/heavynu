#include <iostream>
#include <fstream>

#include "HepMC/PythiaWrapper.h"
#include "HepMC/IO_HEPEVT.h"
#include "HepMC/IO_Ascii.h"
#include "HepMC/GenEvent.h"
#include <TLorentzVector.h>
//#include "TFile.h"
//#include "TH1.h"

extern "C" {
  int pycomp_(int *);
  void pylist_(int *);
}

using namespace std;

int main() { 

  //........................................ROOT Histogramming
  //TFile* hOutputFile;
  //hOutputFile = new TFile("heavynu.root","RECREATE");

  //TH1D* LLmass = new TH1D("llmass", "mass (l,l)",100, 0., 2000.);
  //TH1D* WRmass = new TH1D("wrmass", "mass Wr",100, 0., 2000.);

  pyjets.n = 0;
  call_pyhepc(1); 
  HepMC::HEPEVT_Wrapper::set_max_number_entries(pydat1.mstu[8-1]);
  HepMC::HEPEVT_Wrapper::set_sizeof_real(8);
   
  pysubs.msel = 0;
  pysubs.msub[354-1] = 1; // heavy neutrino

  // set random number seed (mandatory!)
  pydatr.mrpy[0] = 55122 ;

  // Tell Pythia not to write multiple copies of particles in event record.
  // pypars.mstp[128-1] = 2;

  float xmwr = 1000.;
  float hnumassgen = 400.;

  int nutype = 12;       // nu_Re
  //int nutype = 14;       // nu_Rmu
  //int nutype = 16;       // nu_Rtau

  int ipdg = 9900024;
  int iw = pycomp_(&ipdg); // ! WR
  pydat2.pmas[1-1][iw-1] = xmwr;

  ipdg = 9900012;
  iw = pycomp_(&ipdg);
  pydat2.pmas[1-1][iw-1] = 100.*hnumassgen;

  ipdg = 9900014;
  iw = pycomp_(&ipdg);
  pydat2.pmas[1-1][iw-1] = 100.*hnumassgen;

  ipdg = 9900016;
  iw = pycomp_(&ipdg);
  pydat2.pmas[1-1][iw-1] = 100.*hnumassgen;

  ipdg = 9900000+nutype;
  iw = pycomp_(&ipdg);
  pydat2.pmas[1-1][iw-1] = hnumassgen;

  ipdg = 9900024;
  iw = pycomp_(&ipdg); // WR; leave open only heavynu modes
  std::cout << "WR: " << iw << " Nchannels = " << pydat3.mdcy[3-1][iw-1] 
	    << std::endl;

  // Select the subprocess with chosen neutrino (the only one)
  for ( int idc = pydat3.mdcy[2-1][iw-1];
        idc < pydat3.mdcy[2-1][iw-1] + pydat3.mdcy[3-1][iw-1]; idc++ ) {
    if ( abs(pydat3.kfdp[1-1][idc-1]) != 9900000+nutype &&
         abs(pydat3.kfdp[2-1][idc-1]) != 9900000+nutype ) {
      if(pydat3.mdme[1-1][idc-1] > 0) {
        std::cout << "closing channel " << idc << std::endl;
        pydat3.mdme[1-1][idc-1] = 0;
      }
    }
  }

  // .......................................connect LHAPDF stuff
  pypars.mstp[52-1] = 2;

  int index = 0;
  double sigma0 = 0.;
  double sigma[40] = {0.};
  double weight0 = 0.;
  double weight[40] = {0.};

  //... Loop over PDF sets:
  int mintest1 = 10000; // cteq 6
  int maxtest1 = 10040;

  TLorentzVector lepton[100], jet[100];
  int leptonID[100];

  ofstream heavynu;
  heavynu.open("heavynu.dat");

  for (int itest = mintest1; itest < maxtest1+1; itest++) {
  //for (int itest = mintest1; itest < mintest1+1; itest++) {
    //... selecting the PDF set
    //... (see http://hepforge.cedar.ac.uk/lhapdf/manual, Appendix A):
    pypars.mstp[51-1] = itest;

    // Call pythia initialization
    call_pyinit("CMS", "p", "p", 7000.);
    
    //int mode = 12;
    //pylist_(&mode);
    
    //........................................HepMC INITIALIZATIONS
    //
    // Instantiate an IO strategy for reading from HEPEVT.
    HepMC::IO_HEPEVT hepevtio;
    
    int NumOfEvents = 100000;

    int Ngood = 0;  // number of selected events in the given PDF set
    int Ntotal = 0; // total number of events in the given PDF set

    //..........................................................................
    //........................................EVENT LOOP........................
    //..........................................................................

    for (int iev = 1; iev <= NumOfEvents; iev++) {

      Ntotal++;
      //if (iev%1000 == 1) cout << "Processing Event Number "
      //                       << iev << endl;
      //cout << " ============== Processing Event Number " << iev << endl;
      call_pyevnt();      // generate one event with Pythia

      // pythia pyhepc routine converts common PYJETS in common HEPEVT
      call_pyhepc(1);

      HepMC::GenEvent* evt = hepevtio.read_next_event();
      // add some information to the event
      evt->set_event_number(iev);
      evt->set_signal_process_id(354);

      //write the event out to the ascii file
      //ascii_io << evt;

      double llmass, wrmass;
      int nlept = 0, njet = 0;

      // looking for leptons (e and mu)
      for (HepMC::GenEvent::particle_iterator p = evt->particles_begin();
           p != evt->particles_end(); ++p) {

        if ((*p)->status() != 1) continue;
        if (abs((*p)->pdg_id()) != 11 && abs((*p)->pdg_id()) != 13) continue;
        if (fabs((*p)->momentum().eta()) > 2.4) continue;

        if (nlept < 100) {
          lepton[nlept].SetPxPyPzE((*p)->momentum().px(),(*p)->momentum().py(),
                                   (*p)->momentum().pz(),(*p)->momentum().e());
	  leptonID[nlept] = (*p)->pdg_id();
          nlept++;
        }
      }
      // looking for jets
      for (HepMC::GenEvent::particle_iterator p = evt->particles_begin();
           p != evt->particles_end(); ++p) {

        if (abs((*p)->pdg_id()) == 0 || abs((*p)->pdg_id()) > 6) continue;

        if (njet < 100) {
          jet[njet].SetPxPyPzE((*p)->momentum().px(),(*p)->momentum().py(),
                               (*p)->momentum().pz(),(*p)->momentum().e());
          njet++;
        }
      }

      int Nlept = 0, NleptHigh = 0, Njet = 0;
      int lepton_index[100] = {-1}, jet_index[100] = {-1};
      int jetmax1 = -1, jetmax2 = -1;

      for (int i = 0; i < nlept; i++) {
        if (lepton[i].Pt() > 20.) {
          lepton_index[Nlept] = i;
          Nlept++;
        }
      }

      if (Nlept == 2 && leptonID[0] == -leptonID[1]) { // 2 leptons SFOS
        for (int i = 0; i < Nlept; i++) {
          if (lepton[lepton_index[i]].Pt() > 80.) NleptHigh++;
        }
	llmass = 0.;
        if (NleptHigh > 0) {
	  //cout << "  lepton1 pt = " << lepton[0].Pt() << "  lepton2 pt = " 
	  //   << lepton[1].Pt() << endl;
          for (int j = 0; j < njet; j++) {
            if (jet[j].Pt() > 40.) {
              jet_index[Njet] = j;
	      //cout << " j = " << j << " jet_index = " << jet_index[Njet] 
	      //   << "  jet Pt = " << jet[j].Pt() << endl;
              Njet++;
            }
          }
          llmass = (lepton[lepton_index[0]]+lepton[lepton_index[1]]).M();
	  //LLmass->Fill(llmass);

	  //cout << "  Njet = " << Njet << endl;
          if (Njet > 1) {
	    double EJetMax = 0.;
	    for (int j = 0; j < Njet; j++) {
	      int jetid = jet_index[j];
	      //cout << "  j = " << j << "  jetid = " << jetid << endl;
	      if (jet[jetid].Pt() > EJetMax) {
		jetmax1 = j;
		EJetMax = jet[jetid].Pt();
	      }
	    }
	    EJetMax = 0.;
	    for (int j = 0; j < Njet; j++) {
	      if (j == jetmax1) continue;
	      int jetid = jet_index[j];
	      if (jet[jetid].Pt() > EJetMax) {
		jetmax2 = j;
		EJetMax = jet[jetid].Pt();
	      }
	    }
	    wrmass = 0.;
	    if (jetmax1 > -1 && jetmax2 > -1) {
	      wrmass = (lepton[lepton_index[0]]+lepton[lepton_index[1]]+
			jet[jet_index[jetmax1]]+jet[jet_index[jetmax2]]).M();
	      //cout << "  jetmax1 = " << jetmax1 << "  jetmax2 = " << jetmax2;
	      //cout << "  jet_index1 = " << jet_index[jetmax1] << "  jet_index2 = " 
	      //   << jet_index[jetmax2] << endl; 
	      //cout << "  Pt jet1 = " << jet[jet_index[jetmax1]].Pt() 
	      //   << "  Pt jet2 = " << jet[jet_index[jetmax2]].Pt() << endl; 
	      //cout << "  llmass = " << llmass << "  wrmass = " << wrmass << endl;
	      //WRmass->Fill(wrmass);
	    }
	    if (llmass > 200. && wrmass > 900.) Ngood++;
          }
        }
      }
      // we also need to delete the created event from memory
      delete evt;
    }

    // write out some information from Pythia to the screen
    call_pystat(1);
    
    if (index == 0) {
      sigma0 = pyint5.xsec[3-1][0];
      weight0 =  double(Ngood)/double(Ntotal);
      cout << " sigma0 = " <<  sigma0 << "  weight0 = " << weight0 << endl; 
    }
    else if (index > 0 && index <= 40) {
      sigma[index-1] = pyint5.xsec[3-1][0];
      weight[index-1] = double(Ngood)/double(Ntotal);
      cout << " index = " << index << " sigma[index-1] = " 
	   <<  pyint5.xsec[3-1][0] << "  weight[index-1] = " 
	   << weight[index-1] << endl; 
    }
    index++;
  }	  
  
  double deltasig = 0;
  double deltasigWeighted = 0;
  for (int i = 0; i < 40; i += 2) {
    double delta = sigma[i]-sigma[i+1];
    deltasig += delta*delta;
    delta = sigma[i]*weight[i]-sigma[i+1]*weight[i+1];
    deltasigWeighted += delta*delta;
  }
  
  deltasig = sqrt(deltasig)/2.;
  deltasigWeighted = sqrt(deltasigWeighted)/2.;
  
  cout << endl << "  xSect[pb] = " << sigma0*1.e9 << "  uncert = " 
       << deltasig*1.e9 << "(" << deltasig/sigma0*100. << "%)" 
       << "  uncertWeighted = " << deltasigWeighted*1.e9 << "(" 
       << deltasigWeighted/(sigma0*weight0)*100. << "%)" << endl <<endl;

  heavynu << "  sigma[i]      weight[i] " << endl;
  for (int i = 0; i < 40; i++) heavynu << " " << sigma[i] << "   " << weight[i] << endl;

  heavynu << "  sigma0      weight0 " << endl;
  heavynu << " " << sigma0 << "   " << weight0 << endl;
  heavynu.close();


  //hOutputFile->Write();
  //hOutputFile->Close();
}
