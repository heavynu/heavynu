#include <iostream>
#include "HepMC/PythiaWrapper.h"
#include "HepMC/IO_HEPEVT.h"
#include "HepMC/IO_Ascii.h"
#include "HepMC/GenEvent.h"
#include "TFile.h"
#include "TH1.h"

#include "ANHEPMC/JetableInputFromHepMC.h"
#include "ANHEPMC/JetFinderUA.h"
#include "ANHEPMC/LeptonAnalyserHepMC.h"

extern "C" {
  int pycomp_(int *);
  void pylist_(int *);
  void settoprex_(void);
}

int main() {

  ofstream fout("mkdst.d");
 
  //........................................ROOT Histogramming
  TFile* f;
  f = new TFile("hist.root","RECREATE");

  TH1D* Invariant = new TH1D("Invariant", "Pt",100, 0., 2000.);

  pyjets.n=0;
  call_pyhepc(1); 
  HepMC::HEPEVT_Wrapper::set_max_number_entries(pydat1.mstu[8-1]);
  HepMC::HEPEVT_Wrapper::set_sizeof_real(8);
   
  settoprex_();
  // set random number seed (mandatory!)
  pydatr.mrpy[0]=55122 ;

  // Tell Pythia not to write multiple copies of particles in event record.
  // pypars.mstp[128-1] = 2;

  int ipdg = 24;
  int iw = pycomp_(&ipdg); // W; leave open only lepton modes
  for ( int idc = pydat3.mdcy[2-1][iw-1];
        idc < pydat3.mdcy[2-1][iw-1] + pydat3.mdcy[3-1][iw-1]; idc++ ) {
    if ( abs(pydat3.kfdp[1-1][idc-1]) != 11 &&
         abs(pydat3.kfdp[1-1][idc-1]) != 13 &&
         abs(pydat3.kfdp[2-1][idc-1]) != 11 &&
         abs(pydat3.kfdp[2-1][idc-1]) != 13 ) {
      if(pydat3.mdme[1-1][idc-1] > 0) {
        pydat3.mdme[1-1][idc-1] = 0;
      }
    }
  }

  // Call pythia initialization
  //call_pyinit( "CMS", "p", "p", 7000. );

  //........................................HepMC INITIALIZATIONS
  //
  // Instantiate an IO strategy for reading from HEPEVT.
  HepMC::IO_HEPEVT hepevtio;

  LeptonAnalyserHepMC LA;
  JetableInputFromHepMC JI;
  JetFinderUA JF;

  //........................................EVENT LOOP
  for ( int i = 1; i <= 100000; i++ ) {
    if ( i%10==1 ) std::cout << "Processing Event Number " 
                             << i << std::endl;
    call_pyevnt();      // generate one event with Pythia
    // pythia pyhepc routine converts common PYJETS in common HEPEVT
    call_pyhepc( 1 );
    HepMC::GenEvent* evt = hepevtio.read_next_event();
    // add some information to the event
    evt->set_event_number(i);
    evt->set_signal_process_id(354);
    //write the event out to the ascii file
    //ascii_io << evt;
       int mode=1;
    //pylist_(&mode);

    double jet1=0, jet2=0;
    int k, n1=-1, n2=-1;
    double invmass, e=0, p1, p2, p3, p=0;
    int leptype1=0;
    vector<JetableObject> objects = JI.readFromHepMC (evt);
    vector<Jet> alljets = JF.findJets (objects);
    vector<Jet>    jets = LA.removeLeptonsFromJets (alljets, evt);
    vector<HepMC::GenParticle> isoleptons = LA.isolatedLeptons(evt);
    if(jets.size() && isoleptons.size() ) {
      fout << i << " event" << endl;
      for (k=0; k<jets.size(); k++) 
        if (jets[k].perp() > jet1) {jet1=jets[k].perp(); n1=k;}
      //cout << "jet1 pt = " << jet1 << endl;
      fout << jets.size() << " 1 1" << endl;
      fout << 0 << " " << jets[n1].px() << " "
                       << jets[n1].py() << " "
                       << jets[n1].pz() << " "
                       << jets[n1].e() << " 0" << endl;
      for (k=0; k<jets.size(); k++) 
        if (jets[k].perp() > jet2 && k!=n1) {jet2=jets[k].perp(); n2=k;}
      if(n2 > -1) {
        //cout << "jet2 pt = " << jet2 << endl;
        fout << 0 << " " << jets[n2].px() << " "
                         << jets[n2].py() << " "
                         << jets[n2].pz() << " "
                         << jets[n2].e() << " 0" <<endl;
      }
      if(jets.size() > 2 ) {
        for (k=0; k<jets.size(); k++) {
          if(k != n1 && k != n2) {
            //cout << "jet pt = " << jets[k].perp() << endl;
            fout << 0 << " " << jets[k].px() << " "
                             << jets[k].py() << " "
                             << jets[k].pz() << " "
                             << jets[k].e() << " 0" <<endl;
          }
        }
      }
      fout << isoleptons.size() << " leptons" << endl;
      for (k=0; k<isoleptons.size(); k++) {
        //cout << "lepton1 pt = " << isoleptons[0].momentum().perp() << endl;
        if (abs(isoleptons[k].pdg_id()) == 11) leptype1=1; else leptype1=2;
        if (isoleptons[k].pdg_id() > 0) leptype1=-1*leptype1;
        fout << leptype1 << " " << isoleptons[k].momentum().px() << " "
                                << isoleptons[k].momentum().py() << " "
                                << isoleptons[k].momentum().pz() << " "
                                << isoleptons[k].momentum().e() << " "
                                << " 0 " << isoleptons[k].pdg_id() << " 40 0 0"
                                << endl;
      }
      //invariant mass
	  p1=(jets[n1].px()+jets[n2].px()+isoleptons[0].momentum().px()+isoleptons[1].momentum().px());
	  p2=(jets[n1].py()+jets[n2].py()+isoleptons[0].momentum().py()+isoleptons[1].momentum().py());
      p3=(jets[n1].pz()+jets[n2].pz()+isoleptons[0].momentum().pz()+isoleptons[1].momentum().pz());
	  p=p1*p1+p2*p2+p3*p3;
	  e=jets[n1].e()+jets[n2].e()+isoleptons[0].momentum().e()+isoleptons[1].momentum().e();
      invmass=sqrt(e*e-p);
      //cout << "inv mass = " << invmass << endl;
	  //histigramming
      Invariant->Fill(invmass);
      
      //missing pt    
      double ptx=0.;
      double pty=0.;
      for ( HepMC::GenEvent::particle_iterator p = evt->particles_begin();
            p != evt->particles_end(); ++p ){

        if ( (*p)->status() != 1 ) continue;
        if ( abs((*p)->pdg_id()) == 12 || abs((*p)->pdg_id()) == 14 ||
             abs((*p)->pdg_id()) == 16 ) continue;
        if(fabs((*p)->momentum().eta()) > 5.) continue;
        if ( abs((*p)->pdg_id()) == 13 &&
             fabs((*p)->momentum().eta()) > 5.) continue;

        ptx += (*p)->momentum().px();
        pty += (*p)->momentum().py();
      }
      fout <<ptx << " " << pty << " " << ptx << " " << pty << endl;
    }
    // we also need to delete the created event from memory
    delete evt;
  }

  // write out some information from Pythia to the screen
  call_pystat( 1 );
 
  Invariant->Write();
  f->Close();
  fout.close();
  return 0;
}
