// main31.cc is a part of the PYTHIA event generator.
// Copyright (C) 2010 Mikhail Kirsanov, Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL version 2, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// This is a simple test program.
// It illustrates how HepMC can be interfaced to Pythia8.
// It studies the charged multiplicity distribution at the LHC.
// HepMC events are output to the hepmcout31.dat file.
// Written by Mikhail Kirsanov based on main01.cc.

#include "Pythia.h"
#include "HepMCInterface.h"

#include "HepMC/GenEvent.h"
#include "HepMC/IO_GenEvent.h"
// Following line to be used with HepMC 2.04 onwards.
//#include "HepMC/Units.h"
// Following two lines are deprecated alternative.
//#include "HepMC/IO_Ascii.h"
//#include "HepMC/IO_AsciiParticles.h"

#include "ANHEPMC/JetableInputFromHepMC.h"
#include "ANHEPMC/JetFinderUA.h"
//#include "ANHEPMC/LeptonAnalyserHepMC.h"

#include "TFile.h"
#include "TH1.h"

using namespace Pythia8; 

int main() {

  int Nev=10000;
  double lumi=0.7;
  double ptlow[8]={20., 50., 100., 200., 300., 500., 800., 1200.};
  double ptup[8]={50., 100., 200., 300., 500., 800., 1200., 2000.};

  TFile* hOutputFile;
  hOutputFile = new TFile("hist.root","RECREATE");
  TH1D* JetPt0 = new TH1D("JetPt0", "Pt", 100, 0., 2000.);
  TH1D* JetPtBarrel0 = new TH1D("JetPtBarrel0", "PtB0", 100, 0., 2000.);
  TH1D* JetPtEndcup0 = new TH1D("JetPtEndcup0", "PtE0", 100, 0., 2000.);
  TH1D* JetPt = new TH1D("JetPt", "Pt", 100, 0., 2000.);
  TH1D* JetPtBarrel = new TH1D("JetPtBarrel", "PtB", 100, 0., 2000.);
  TH1D* JetPtEndcup = new TH1D("JetPtEndcup", "PtE", 100, 0., 2000.);

  // Interface for conversion from Pythia8::Event to HepMC one. 
  HepMC::I_Pythia8 ToHepMC;
  //  ToHepMC.set_crash_on_problem();
  //  ToHepMC.set_nofreepartonwarnings();

  // Specify file where HepMC events will be stored.
//  HepMC::IO_GenEvent ascii_io("hepmcout31.dat", std::ios::out);
  // Following two lines are deprecated alternative.
  // HepMC::IO_Ascii ascii_io("hepmcout31.dat", std::ios::out);
  // HepMC::IO_AsciiParticles ascii_io("hepmcout31.dat", std::ios::out);

  JetableInputFromHepMC JI;
  JetFinderUA JF;

  // Generator. Process selection. LHC initialization. Histogram.
  Pythia pythia;
  pythia.readString("HardQCD:all = on");    

  for (int iptbin=0; iptbin < 8; iptbin++) {

    pythia.settings.parm("PhaseSpace:pTHatMin", ptlow[iptbin]);
    pythia.settings.parm("PhaseSpace:pTHatMax", ptup[iptbin]);
    //pythia.readString("PhaseSpace:pTHatMin = 20.");
    //pythia.readString("PhaseSpace:pTHatMax = 100.");
    pythia.init( 2212, 2212, 7000.);
    cout << "ptmin = " << pythia.parm("PhaseSpace:pTHatMin") << endl;

    // Begin event loop. Generate event. Skip if error. List first one.
    for (int iEvent = 0; iEvent < Nev; ++iEvent) {
      if (!pythia.next()) continue;
      //if (iEvent < 1) {pythia.info.list(); pythia.event.list();} 

      // fill HepMC event
      // meaningful for HepMC 2.04 onwards, and even then unnecessary  
      // if HepMC was built with GeV and mm as units from the onset. 
      HepMC::GenEvent* hepmcevt = new HepMC::GenEvent();
      //HepMC::GenEvent* hepmcevt = new HepMC::GenEvent(
      //  HepMC::Units::GEV, HepMC::Units::MM); 

      // Fill HepMC event, including PDF info.
      ToHepMC.fill_next_event( pythia, hepmcevt );
      // This alternative older method fills event, without PDF info.
      // ToHepMC.fill_next_event( pythia.event, hepmcevt );

      // Write the HepMC event to file. Done with it.
      //ascii_io << hepmcevt;

      vector<JetableObject> objects = JI.readFromHepMC(hepmcevt);
//      cout << "size of jetable objects = " << objects.size() << endl;
      vector<Jet> alljets = JF.findJets(objects);
//      cout << "size of all jets = " << alljets.size() << endl;

      if( alljets.size() == 2) {
        double eta1, eta2, phi1, phi2, r1, r2, dist, ptratio;
        int ijet1, ijet2;
        for(int ijet=0; ijet < 2; ijet++) {
          ijet1 = ijet;
          ijet2 = ijet+1;
          if(ijet > 0) ijet2 = 0;
          eta1 = -alljets[ijet1].eta();
          phi1 = alljets[ijet1].phi() + 3.141;
          eta2 = alljets[ijet2].eta();
          phi2 = alljets[ijet2].phi();
          r1 = phi1 - phi2;
          if(fabs(r1-6.282) < fabs(r1)) r1 = r1-6.282;
          if(fabs(r1+6.282) < fabs(r1)) r1 = r1+6.282;
          r2 = eta1 - eta2;
          dist = sqrt(r1 * r1 + r2 * r2);
          ptratio = alljets[ijet1].et()/alljets[ijet2].et();
          if(dist < 0.6 && ptratio > 0.4 && ptratio < 1.4) {
            if(fabs(alljets[ijet1].eta()) < 2.4)
              JetPt0->Fill(alljets[ijet1].et());
            if(fabs(alljets[ijet1].eta()) < 1.3)
              JetPtBarrel0->Fill(alljets[ijet1].et());
            if(fabs(alljets[ijet1].eta()) >= 1.3 &&
               fabs(alljets[ijet1].eta()) < 2.4)
              JetPtEndcup0->Fill(alljets[ijet1].et());
          }
        }
      }

      delete hepmcevt;

    } // End of event loop. Statistics. Histogram. 

    pythia.statistics();

    double weight = pythia.info.sigmaGen() * 1.e9 * lumi / (double)Nev;

    JetPt->Add(JetPt0, weight);
    JetPtBarrel->Add(JetPtBarrel0, weight);
    JetPtEndcup->Add(JetPtEndcup0, weight);

    JetPt0->Reset();
    JetPtBarrel0->Reset();
    JetPtEndcup0->Reset();

  }

  hOutputFile->Write();
  hOutputFile->Close();

  // Done.
  return 0;
}
