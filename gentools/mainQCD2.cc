// main31.cc is a part of the PYTHIA event generator.
// Copyright (C) 2010 Mikhail Kirsanov, Torbjorn Sjostrand.
// PYTHIA is licenced under the GNU GPL version 2, see COPYING for details.
// Please respect the MCnet Guidelines, see GUIDELINES for details.

// This is a simple test program.
// It illustrates how HepMC can be interfaced to Pythia8.
// It studies the charged multiplicity distribution at the LHC.
// HepMC events are output to the hepmcout31.dat file.
// Written by Mikhail Kirsanov based on main01.cc.

#include "Pythia.h"
#include "HepMCInterface.h"

#include "HepMC/GenEvent.h"
#include "HepMC/IO_GenEvent.h"
// Following line to be used with HepMC 2.04 onwards.
//#include "HepMC/Units.h"
// Following two lines are deprecated alternative.
//#include "HepMC/IO_Ascii.h"
//#include "HepMC/IO_AsciiParticles.h"

#include "ANHEPMC/JetableInputFromHepMC.h"
#include "ANHEPMC/JetFinderUA.h"
//#include "ANHEPMC/LeptonAnalyserHepMC.h"

#include "TFile.h"
#include "TH1.h"

using namespace Pythia8; 

int main() {

  int Nev=1000;
  double lumi=100; // should be 100 1/pb, will be renormalized in rdcms
  double ptlow[8]={20., 50., 100., 200., 300., 500., 800., 1200.};
  double ptup[8]={50., 100., 200., 300., 500., 800., 1200., 2000.};
  double cs[8]={2.68e+08, 6.63e+06, 305451., 9823., 1212., 61., 2.34, 0.0656};

  ofstream csout("cstable.d");
  ofstream fout("mkdst.d");

//  TFile* hOutputFile;
//  hOutputFile = new TFile("hist.root","RECREATE");
//  TH1D* JetPt = new TH1D("JetPt", "Pt", 100, 0., 2000.);

  // Interface for conversion from Pythia8::Event to HepMC one. 
  HepMC::I_Pythia8 ToHepMC;
  //  ToHepMC.set_crash_on_problem();
  //  ToHepMC.set_nofreepartonwarnings();

  // Specify file where HepMC events will be stored.
//  HepMC::IO_GenEvent ascii_io("hepmcout31.dat", std::ios::out);
  // Following two lines are deprecated alternative.
  // HepMC::IO_Ascii ascii_io("hepmcout31.dat", std::ios::out);
  // HepMC::IO_AsciiParticles ascii_io("hepmcout31.dat", std::ios::out);

  JetableInputFromHepMC JI;
  JetFinderUA JF;

  // Generator. Process selection. LHC initialization. Histogram.
  Pythia pythia;
  pythia.readString("HardQCD:all = on");    

  for (int iptbin=0; iptbin < 8; iptbin++) {

    pythia.settings.parm("PhaseSpace:pTHatMin", ptlow[iptbin]);
    pythia.settings.parm("PhaseSpace:pTHatMax", ptup[iptbin]);
    //pythia.readString("PhaseSpace:pTHatMin = 20.");
    //pythia.readString("PhaseSpace:pTHatMax = 100.");
    pythia.init( 2212, 2212, 7000.);
    cout << "ptmin = " << pythia.parm("PhaseSpace:pTHatMin") << endl;

    // Begin event loop. Generate event. Skip if error. List first one.
    for (int iEvent = 0; iEvent < Nev; ++iEvent) {
      if (!pythia.next()) continue;
      //if (iEvent < 1) {pythia.info.list(); pythia.event.list();} 

      // fill HepMC event
      // meaningful for HepMC 2.04 onwards, and even then unnecessary  
      // if HepMC was built with GeV and mm as units from the onset. 
      HepMC::GenEvent* hepmcevt = new HepMC::GenEvent();
      //HepMC::GenEvent* hepmcevt = new HepMC::GenEvent(
      //  HepMC::Units::GEV, HepMC::Units::MM); 

      // Fill HepMC event, including PDF info.
      ToHepMC.fill_next_event( pythia, hepmcevt );
      // This alternative older method fills event, without PDF info.
      // ToHepMC.fill_next_event( pythia.event, hepmcevt );

      // Write the HepMC event to file. Done with it.
      //ascii_io << hepmcevt;

      vector<JetableObject> objects = JI.readFromHepMC(hepmcevt);
//      cout << "size of jetable objects = " << objects.size() << endl;
      vector<Jet> alljets = JF.findJets(objects);
//      cout << "size of all jets = " << alljets.size() << endl;

      double ptot, weight;
      int ijet1, ijet2, k, njgood=0, leptype1, leptype2;
      for(k=0; k < alljets.size(); k++) {
        if(fabs(alljets[k].eta()) <= 2.4) njgood++;
      }
      if( alljets.size() >= 3 && njgood >= 2) {
        for(ijet1=0; ijet1 < alljets.size()-1; ijet1++) {
          if(fabs(alljets[ijet1].eta()) > 2.4) continue;
          for(ijet2=ijet1+1; ijet2 < alljets.size(); ijet2++) {
            if(ijet2 == ijet1) continue;
            if(fabs(alljets[ijet2].eta()) > 2.4) continue;

            fout << iptbin*Nev + iEvent << " event" << endl;
            double prob1=0.0002;
            if(alljets[ijet1].et() > 100. && alljets[ijet1].et() <= 300.)
              prob1 = 0.00013;
            if(alljets[ijet1].et() > 400. && alljets[ijet1].et() <= 600.)
              prob1 = 0.00024;
            if(alljets[ijet1].et() > 600. && alljets[ijet1].et() <= 700.)
              prob1 = 0.0003;
            if(alljets[ijet1].et() > 700. && alljets[ijet1].et() <= 900.)
              prob1 = 0.0004;
            if(alljets[ijet1].et() > 900.) prob1 = 0.0003;

            double prob2=0.0002;
            if(alljets[ijet2].et() > 100. && alljets[ijet2].et() <= 300.)
              prob2 = 0.00013;
            if(alljets[ijet2].et() > 400. && alljets[ijet2].et() <= 600.)
              prob2 = 0.00024;
            if(alljets[ijet2].et() > 600. && alljets[ijet2].et() <= 700.)
              prob2 = 0.0003;
            if(alljets[ijet2].et() > 700. && alljets[ijet2].et() <= 900.)
              prob2 = 0.0004;
            if(alljets[ijet2].et() > 900.) prob2 = 0.0003;

            weight = prob1*prob2*cs[iptbin] * lumi/(double)Nev;
            fout << alljets.size() - 2 << " " << weight << " 27" << endl;
            for (k=0; k < alljets.size(); k++) {
              if(k != ijet1 && k != ijet2) {
                fout << 0 << " " << alljets[k].px() << " "
                                 << alljets[k].py() << " "
                                 << alljets[k].pz() << " "
                                 << alljets[k].e() << " 0" << endl;
              }
            }

            fout << "2 leptons" << endl;
            leptype1=1;
            leptype2=1;
            ptot = sqrt(alljets[ijet1].px()*alljets[ijet1].px() +
                        alljets[ijet1].py()*alljets[ijet1].py() +
                        alljets[ijet1].pz()*alljets[ijet1].pz() );            
            fout << leptype1 << " "
                 << alljets[ijet1].e()*alljets[ijet1].px()/ptot << " "
                 << alljets[ijet1].e()*alljets[ijet1].py()/ptot << " "
                 << alljets[ijet1].e()*alljets[ijet1].pz()/ptot << " "
                 << alljets[ijet1].e() << " "
                 << " 0 0 40 0 0" << endl;

            ptot = sqrt(alljets[ijet2].px()*alljets[ijet2].px() +
                        alljets[ijet2].py()*alljets[ijet2].py() +
                        alljets[ijet2].pz()*alljets[ijet2].pz() );
            fout << leptype1 << " "
                 << alljets[ijet2].e()*alljets[ijet2].px()/ptot << " "
                 << alljets[ijet2].e()*alljets[ijet2].py()/ptot << " "
                 << alljets[ijet2].e()*alljets[ijet2].pz()/ptot << " "
                 << alljets[ijet2].e() << " "
                 << " 0 0 40 0 0" << endl;

            //missing pt
            double ptx=0.;
            double pty=0.;
            for ( HepMC::GenEvent::particle_iterator p =
                                        hepmcevt->particles_begin();
                  p != hepmcevt->particles_end(); ++p ) {

              if ( (*p)->status() != 1 ) continue;
              if ( abs((*p)->pdg_id()) == 12 || abs((*p)->pdg_id()) == 14 ||
                   abs((*p)->pdg_id()) == 16 ) continue;
              if(fabs((*p)->momentum().eta()) > 5.) continue;
              if ( abs((*p)->pdg_id()) == 13 &&
                   fabs((*p)->momentum().eta()) > 5.) continue;

              ptx += (*p)->momentum().px();
              pty += (*p)->momentum().py();
            }
            fout << ptx << " " << pty << " " << ptx << " " << pty << endl;

          }
        }
      }

      delete hepmcevt;

    } // End of event loop. Statistics. Histogram. 

    pythia.statistics();

    csout << pythia.info.sigmaGen() * 1.e9 << endl;

  }

//  hOutputFile->Write();
//  hOutputFile->Close();

  // Done.
  return 0;
}
