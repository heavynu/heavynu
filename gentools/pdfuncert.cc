// program to estimate the theoretical PDF uncertainties for ttbar
// simulation
#include <iostream>
#include <fstream>
#include <vector>

#include "HepMC/PythiaWrapper.h"
#include "HepMC/IO_HEPEVT.h"
#include "HepMC/IO_AsciiParticles.h"
#include "HepMC/GenEvent.h"
#include <TLorentzVector.h>
//#include "TFile.h"
//#include "TH1.h"

using namespace std;

extern "C" {
  int pycomp_(int *);
  void pylist_(int *);
  void pygive_(const char*, int);
  void call_pygive(const std::string &line) {
       pygive_(line.c_str(), line.length());
  }
}

int main() { 

  //........................................ROOT Histogramming
  //TFile* hOutputFile;
  //hOutputFile = new TFile("ttbar.root","RECREATE");

  //TH1D* LLmass = new TH1D("llmass", "mass (l,l)",100, 0., 2000.);
  //TH1D* WRmass = new TH1D("wrmass", "mass Wr",100, 0., 2000.);

  pyjets.n = 0;
  call_pyhepc(1); 
  HepMC::HEPEVT_Wrapper::set_max_number_entries(pydat1.mstu[8-1]);
  HepMC::HEPEVT_Wrapper::set_sizeof_real(8);
   
// ----------- ttbar settings -------------------------------

#if 0
  pysubs.msel = 6; // User defined processes 

  //pysubs.msub[81-1] = 1; // qqbar to QQbar 
  //pysubs.msub[82-1] = 1; // gg to QQbar 
  //pypars.mstp[7-1] = 6;  // flavour = top
  pydat2.pmas[1-1][6-1] = 175.; // top quark mass

  // Select leptonic mode
  call_pygive("24:alloff");
  call_pygive("24:onifany = 11 13 15");
#endif

// ---------- Z + jets settings ----------------------

  pysubs.msel = 0;            // User defined processes',
  pysubs.msub[15-1] = 1;      // Z + jets',
  pysubs.ckin[3-1] = 40.;     // Pt lower cut',
  pysubs.ckin[43-1] = 200.;   // MZ lower cut',
  call_pygive("23:alloff");
  call_pygive("23:onifany = 11 13 15");

// ---------- general settings ------------------------

  pydat1.mstj[11-1] = 3; // Choice of the fragmentation function
  pydat1.mstj[22-1] = 2; 
  pydat1.mstu[21-1] = 1; // Check on possible errors during program execution

  pypars.mstp[2-1] = 1; 
  pypars.mstp[33-1] = 0; 
  pypars.mstp[81-1] = 1; // multiple parton interactions (1 is Pythia default)
  pypars.mstp[82-1] = 4; // multiple parton interactions (see p209 CERN-TH 7112/93) 
  pypars.parp[82-1] = 1.9; 
  pypars.parp[83-1] = 0.5; 
  pypars.parp[84-1] = 0.4; 
  pypars.parp[90-1] = 0.16; 

  // set random number seed (mandatory!)
  pydatr.mrpy[0] = 55122 ;

  // Tell Pythia not to write multiple copies of particles in event record.
  // pypars.mstp[128-1] = 2;

  // .......................................connect LHAPDF stuff
  pypars.mstp[52-1] = 2;
  //pypars.mstp[51-1] = 7; // cteq 5

  int index = 0;
  double sigma0 = 0.;
  double sigma[40] = {0.};
  double weight0 = 0.;
  double weight[40] = {0.};

  //... Loop over PDF sets:
  int mintest1 = 10000; // cteq 6
  int maxtest1 = 10040;

  TLorentzVector lepton[100], jet[100];
  int leptonID[100];

  // open file to write PDF uncertainties
  ofstream ttbar;
  ttbar.open("ttbar.dat");

  for (int itest = mintest1; itest < maxtest1+1; itest++) {
    //for (int itest = mintest1; itest < mintest1+1; itest++) {

    //... selecting the PDF set
    //... (see http://hepforge.cedar.ac.uk/lhapdf/manual, Appendix A):
    
    pypars.mstp[51-1] = itest;

    // Call pythia initialization
    //call_pyinit("CMS", "p", "p", 14000.);
    //call_pyinit("CMS", "p", "p", 10000.);
    call_pyinit("CMS", "p", "p", 7000.);
    
    //int mode = 12;
    //pylist_(&mode);
    
    //........................................HepMC INITIALIZATIONS
    //
    // Instantiate an IO strategy for reading from HEPEVT.
    HepMC::IO_HEPEVT hepevtio;
    
    int NumOfEvents = 20000;

    int Ngood = 0;  // number of selected events in the given PDF set
    int Ntotal = 0; // total number of events in the given PDF set

    //..........................................................................
    //........................................EVENT LOOP........................
    //..........................................................................
    
    for (int iev = 1; iev <= NumOfEvents; iev++) {

      Ntotal++;
      //if (iev%1000 == 1) cout << "Processing Event Number "
      //                       << iev << endl;
      //cout << " ============== Processing Event Number " << iev << endl;
      call_pyevnt();      // generate one event with Pythia

      // pythia pyhepc routine converts common PYJETS in common HEPEVT
      call_pyhepc(1);

      HepMC::GenEvent* evt = hepevtio.read_next_event();
      // add some information to the event
      evt->set_event_number(iev);
      evt->set_signal_process_id(354);

      //write the event out to the ascii file
      //ascii_io << evt;

      double llmass, wrmass;
      int nlept = 0, njet = 0;

      // looking for leptons (e and mu)
      for (HepMC::GenEvent::particle_iterator p = evt->particles_begin();
           p != evt->particles_end(); ++p) {

        if ((*p)->status() != 1) continue;
        if (abs((*p)->pdg_id()) != 11 && abs((*p)->pdg_id()) != 13) continue;
        if (fabs((*p)->momentum().eta()) > 2.4) continue;

        if (nlept < 100) {
          lepton[nlept].SetPxPyPzE((*p)->momentum().px(),(*p)->momentum().py(),
                                   (*p)->momentum().pz(),(*p)->momentum().e());
	  leptonID[nlept] = (*p)->pdg_id();
          nlept++;
        }
      }
      // looking for jets
      for (HepMC::GenEvent::particle_iterator p = evt->particles_begin();
           p != evt->particles_end(); ++p) {

        if (abs((*p)->pdg_id()) == 0 || abs((*p)->pdg_id()) > 6) continue;

        if (njet < 100) {
          jet[njet].SetPxPyPzE((*p)->momentum().px(),(*p)->momentum().py(),
                               (*p)->momentum().pz(),(*p)->momentum().e());
          njet++;
        }
      }

      int Nlept = 0, NleptHigh = 0, Njet = 0;
      int lepton_index[100] = {-1}, jet_index[100] = {-1};
      int jetmax1 = -1, jetmax2 = -1;

      for (int i = 0; i < nlept; i++) {
        if (lepton[i].Pt() > 20.) {
          lepton_index[Nlept] = i;
          Nlept++;
        }
      }

      if (Nlept == 2 && leptonID[0] == -leptonID[1]) { // 2 leptons SFOS
        for (int i = 0; i < Nlept; i++) {
          //if (lepton[lepton_index[i]].Pt() > 80.) NleptHigh++;
          if (lepton[lepton_index[i]].Pt() > 60.) NleptHigh++;
        }
	llmass = 0.;
        if (NleptHigh > 0) {
	  //cout << "  lepton1 pt = " << lepton[0].Pt() << "  lepton2 pt = " 
	  //   << lepton[1].Pt() << endl;
          for (int j = 0; j < njet; j++) {
            if (jet[j].Pt() > 40.) {
              jet_index[Njet] = j;
	      //cout << " j = " << j << " jet_index = " << jet_index[Njet] 
	      //   << "  jet Pt = " << jet[j].Pt() << endl;
              Njet++;
            }
          }
          llmass = (lepton[lepton_index[0]]+lepton[lepton_index[1]]).M();
	  //LLmass->Fill(llmass);

	  //cout << "  Njet = " << Njet << endl;
          if (Njet > 1) {
	    double EJetMax = 0.;
	    for (int j = 0; j < Njet; j++) {
	      int jetid = jet_index[j];
	      //cout << "  j = " << j << "  jetid = " << jetid << endl;
	      if (jet[jetid].Pt() > EJetMax) {
		jetmax1 = j;
		EJetMax = jet[jetid].Pt();
	      }
	    }
	    EJetMax = 0.;
	    for (int j = 0; j < Njet; j++) {
	      if (j == jetmax1) continue;
	      int jetid = jet_index[j];
	      if (jet[jetid].Pt() > EJetMax) {
		jetmax2 = j;
		EJetMax = jet[jetid].Pt();
	      }
	    }
	    wrmass = 0.;
	    if (jetmax1 > -1 && jetmax2 > -1) {
	      wrmass = (lepton[lepton_index[0]]+lepton[lepton_index[1]]+
			jet[jet_index[jetmax1]]+jet[jet_index[jetmax2]]).M();
	      //cout << "  jetmax1 = " << jetmax1 << "  jetmax2 = " << jetmax2;
	      //cout << "  jet_index1 = " << jet_index[jetmax1] << "  jet_index2 = " 
	      //   << jet_index[jetmax2] << endl; 
	      //cout << "  Pt jet1 = " << jet[jet_index[jetmax1]].Pt() 
	      //   << "  Pt jet2 = " << jet[jet_index[jetmax2]].Pt() << endl; 
	      //cout << "  llmass = " << llmass << "  wrmass = " << wrmass << endl;
	      //WRmass->Fill(wrmass);
	    }
	    //if (llmass > 200. && wrmass > 1100.) Ngood++;
	    if (llmass > 200. && wrmass > 800.) Ngood++;
          }
        }
      }
      // we also need to delete the created event from memory
      delete evt;
    }
 
    // write out some information from Pythia to the screen
    call_pystat(1);
    
    if (index == 0) {
      sigma0 = pyint5.xsec[3-1][0];
      weight0 =  double(Ngood)/double(Ntotal);
      cout << " sigma0 = " <<  sigma0 << "  weight0 = " << weight0 << endl; 
    }
    else if (index > 0 && index <= 40) {
      sigma[index-1] = pyint5.xsec[3-1][0];
      weight[index-1] = double(Ngood)/double(Ntotal);
      cout << " index = " << index << " sigma[index-1] = " 
	   <<  pyint5.xsec[3-1][0] << "  weight[index-1] = " 
	   << weight[index-1] << endl; 
    }
    index++;
  }	  
  
  double deltasig = 0;
  double deltasigWeighted = 0;
  for (int i = 0; i < 40; i += 2) {
    double delta = sigma[i]-sigma[i+1];
    deltasig += delta*delta;
    delta = sigma[i]*weight[i]-sigma[i+1]*weight[i+1];
    deltasigWeighted += delta*delta;
  }
  
  deltasig = sqrt(deltasig)/2.;
  deltasigWeighted = sqrt(deltasigWeighted)/2.;
  
  cout << endl << "  xSect[pb] = " << sigma0*1.e9 << "  uncert = " 
       << deltasig*1.e9 << "(" << deltasig/sigma0*100. << "%)" 
       << "  uncertWeighted = " << deltasigWeighted*1.e9 << "(" 
       << deltasigWeighted/(sigma0*weight0)*100. << "%)" << endl <<endl;

  ttbar << "  sigma[i]      weight[i] " << endl;
  for (int i = 0; i < 40; i++) ttbar << " " << sigma[i] << "   " << weight[i] << endl;

  ttbar << "  sigma0      weight0 " << endl;
  ttbar << " " << sigma0 << "   " << weight0 << endl;
  ttbar.close();

  //hOutputFile->Write();
  //hOutputFile->Close();
}
