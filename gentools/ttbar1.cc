// program to estimate the theoretical PDF uncertainties for ttbar & Z+jets
// simulation
#include <iostream>
#include <fstream>
#include <vector>

#include "HepMC/PythiaWrapper.h"
#include "HepMC/IO_HEPEVT.h"
#include "HepMC/IO_AsciiParticles.h"
#include "HepMC/GenEvent.h"
#include <TLorentzVector.h>
//#include "TFile.h"
//#include "TH1.h"

#include "ANHEPMC/LeptonAnalyserHepMC.h"
#include "ANHEPMC/JetableInputFromHepMC.h"
#include "ANHEPMC/JetFinderUA.h"

using namespace std;

extern "C" {
  int pycomp_(int *);
  void pylist_(int *);
  void pygive_(const char*, int);
  void call_pygive(const std::string &line) {
       pygive_(line.c_str(), line.length());
  }
}

int main(int argc, char **argv) { 
  // usage: ./ttbar1.exe 1 out1 16779

  //........................................ROOT Histogramming
  //TFile* hOutputFile;
  //hOutputFile = new TFile("ttbar.root","RECREATE");

  //TH1D* LLmass = new TH1D("llmass", "mass (l,l)",100, 0., 2000.);
  //TH1D* WRmass = new TH1D("wrmass", "mass Wr",100, 0., 2000.);

  int Index = 1;
  if (argc > 1) Index = atoi(argv[1]); 
  char *filename = "out";
  if (argc > 2) filename = argv[2]; 
  int rndnum = 55122;  
  if (argc > 2) rndnum = atoi(argv[3]); 
  cout << "  index = " << Index << "  filename = " << filename 
       << " rndnum = " << rndnum << endl;

  ofstream outpt;
  outpt.open(filename);

  ofstream events;

  if (Index == 0) {
    events.open("0pdfevents");
  }

  pyjets.n = 0;
  call_pyhepc(1); 
  HepMC::HEPEVT_Wrapper::set_max_number_entries(pydat1.mstu[8-1]);
  HepMC::HEPEVT_Wrapper::set_sizeof_real(8);
   
// ----------- ttbar settings -------------------------------

#if 1
  pysubs.msel = 6; // User defined processes 
  pydat2.pmas[1-1][6-1] = 175.; // top quark mass

  // Select leptonic mode
  call_pygive("24:alloff");
  call_pygive("24:onifany = 11 13 15");
#endif

// ---------- Z + jets settings ----------------------
#if 0
  pysubs.msel = 0;            // User defined processes',
  pysubs.msub[15-1] = 1;      // Z + jets',
  pysubs.ckin[3-1] = 40.;     // Pt lower cut',
  pysubs.ckin[43-1] = 200.;   // MZ lower cut',
  call_pygive("23:alloff");
  call_pygive("23:onifany = 11 13 15");
#endif
// ---------- general settings ------------------------

  pydat1.mstj[11-1] = 3; // Choice of the fragmentation function
  pydat1.mstj[22-1] = 2; 
  pydat1.mstu[21-1] = 1; // Check on possible errors during program execution

  pypars.mstp[2-1] = 1; 
  pypars.mstp[33-1] = 0; 
  pypars.mstp[81-1] = 1; // multiple parton interactions (1 is Pythia default)
  pypars.mstp[82-1] = 4; // multiple parton interactions (see p209 CERN-TH 7112/93) 
  pypars.parp[82-1] = 1.9; 
  pypars.parp[83-1] = 0.5; 
  pypars.parp[84-1] = 0.4; 
  pypars.parp[90-1] = 0.16; 

  // set random number seed (mandatory!)
  //pydatr.mrpy[0] = 55122 ;
  pydatr.mrpy[0] = rndnum;

  // Tell Pythia not to write multiple copies of particles in event record.
  // pypars.mstp[128-1] = 2;

  // .......................................connect LHAPDF stuff
  pypars.mstp[52-1] = 2;
  //pypars.mstp[51-1] = 7; // cteq 5

  //........................................HepMC INITIALIZATIONS
  //
  // Instantiate an IO strategy for reading from HEPEVT.
  HepMC::IO_HEPEVT hepevtio;

  LeptonAnalyserHepMC LA;
  JetableInputFromHepMC JI;
  JetFinderUA JF(0.5, 1., 10., 40.);

  int index = 0;
  double sigma0 = 0.;
  double sigma[40] = {0.};
  double weight0 = 0.;
  double weight[40] = {0.};

  //... Loop over PDF sets:
  int mintest1 = 10000; // cteq 6
  //int maxtest1 = 10040;

   index = Index; 
   int itest = mintest1+index; 
   //for (int itest = mintest1; itest < maxtest1+1; itest++) {

    //... selecting the PDF set
    //... (see http://hepforge.cedar.ac.uk/lhapdf/manual, Appendix A):
    
    pypars.mstp[51-1] = itest;

    // Call pythia initialization
    call_pyinit("CMS", "p", "p", 7000.);
    
    //int mode = 12;
    //pylist_(&mode);
    
    int NumOfEvents = 1000000;

    int Ngood = 0;  // number of selected events in the given PDF set
    int Ntotal = 0; // total number of events in the given PDF set

    //..........................................................................
    //........................................EVENT LOOP........................
    //..........................................................................
    
    cout << "event loop" << endl;

    char *selected = " ";

    for (int iev = 1; iev <= NumOfEvents; iev++) {

      Ntotal++;
      //if (iev%1000 == 1) cout << "Processing Event Number "
      //                       << iev << endl;
      //cout << " ============== Processing Event Number " << iev << endl;
      call_pyevnt();      // generate one event with Pythia

      // get pdf info directly from Pythia6 and set it up into HepMC::GenEvent
      // S. Mrenna: Prefer vint block

      selected = "B";
      HepMC::PdfInfo pdf;

      pdf.set_id1(pyint1.mint[14]);
      pdf.set_id2(pyint1.mint[15]);
      pdf.set_x1(pyint1.vint[40]);
      pdf.set_x2(pyint1.vint[41]);
      pdf.set_pdf1(pyint1.vint[38]);
      pdf.set_pdf2(pyint1.vint[39]);
      pdf.set_scalePDF(pyint1.vint[50]);   

      if (Index == 0) { 
        events << "  "  << iev << "  " << pdf.id1() << "  " << pdf.x1() << "  " 
               << pdf.pdf1() << "  " << pdf.id2() << "  " << pdf.x2()
               << "  " << pdf.pdf2() << " " << pdf.scalePDF() << "  ";
      } 
      // pythia pyhepc routine converts common PYJETS in common HEPEVT
      call_pyhepc(1);

      HepMC::GenEvent* evt = hepevtio.read_next_event();
      // add some information to the event
      evt->set_event_number(iev);
      evt->set_signal_process_id(354);

      double llmass, wrmass, pleptmax=0.;
      int nlept = 0;

      vector<HepMC::GenParticle> isoleptons = LA.isolatedLeptons(evt);

      for ( vector<HepMC::GenParticle>::iterator p = isoleptons.begin();
            p != isoleptons.end(); ++p ){
        
        if ( (p)->status() != 1 ) continue;
        if ( abs((p)->pdg_id()) != 11 && abs((p)->pdg_id()) != 13) continue;
        //if(fabs((p)->momentum().eta()) > 2.4) continue; // applied in LA
        //if((p)->momentum().perp() < 20.) continue;      // applied in LA
        nlept++;
        if((p)->momentum().perp() > pleptmax) pleptmax = (p)->momentum().perp();

      }

      // Jets:
      vector<JetableObject> objects = JI.readFromHepMC (evt);
      vector<Jet> alljets = JF.findJets (objects);
      vector<Jet>    jets = LA.removeLeptonsFromJets(alljets, evt);

      if(nlept == 2 && jets.size() >= 2 && pleptmax >= 60.) {

//        HepMC::GenEvent::particle lept1 = isoleptons[0];
//        HepMC::GenEvent::particle lept2 = isoleptons[1];

        TLorentzVector lept1(isoleptons[0].momentum().px(),
                             isoleptons[0].momentum().py(),
                             isoleptons[0].momentum().pz(),
                             isoleptons[0].momentum().e());
        TLorentzVector lept2(isoleptons[1].momentum().px(),
                             isoleptons[1].momentum().py(),
                             isoleptons[1].momentum().pz(),
                             isoleptons[1].momentum().e());
        llmass = (lept1+lept2).M();

        TLorentzVector jet1(jets[0].px(),
                            jets[0].py(),
                            jets[0].pz(),
                            jets[0].e());
        TLorentzVector jet2(jets[1].px(),
                            jets[1].py(),
                            jets[1].pz(),
                            jets[1].e());

        wrmass = (lept1 + lept2 + jet1 + jet2).M();

	if (llmass > 200. && wrmass > 800.) {
           Ngood++;
           selected = "G";
        }
      }
      if (Index == 0) events << selected << endl;
      // we also need to delete the created event from memory
      delete evt;
    }
 
    // write out some information from Pythia to the screen
    call_pystat(1);
    
    char outstring[160] = {" "};
   
    cout << "  Good events = " << Ngood << "  Total events = " << Ntotal << endl;
 
    if (index == 0) {
      sigma0 = pyint5.xsec[3-1][0];
      weight0 =  double(Ngood)/double(Ntotal);
      cout << " index = " << index << " sigma0 = " <<  sigma0 << "  weight0 = " 
	   << weight0 << endl; 
    
      sprintf(outstring," %5d   %20.13g   %20.13g\n", 
	     index, sigma0, weight0);
      cout << outstring;
      outpt << outstring; 
    }
    else if (index > 0 && index <= 40) {
      sigma[index-1] = pyint5.xsec[3-1][0];
      weight[index-1] = double(Ngood)/double(Ntotal);
      cout << " index = " << index << " sigma[index-1] = " 
	   <<  pyint5.xsec[3-1][0] << "  weight[index-1] = " 
	   << weight[index-1] << endl; 
      sprintf(outstring," %5d   %20.13g   %20.13g\n", 
	     index, sigma[index-1], weight[index-1]); 
      cout <<  " outstring = " << outstring;
      outpt << outstring;
    }
    //index++;
    //}	  
  
#if 0
  double deltasig = 0;
  double deltasigWeighted = 0;
  for (int i = 0; i < 40; i += 2) {
    double delta = sigma[i]-sigma[i+1];
    deltasig += delta*delta;
    delta = sigma[i]*weight[i]-sigma[i+1]*weight[i+1];
    deltasigWeighted += delta*delta;
  }
  
  deltasig = sqrt(deltasig)/2.;
  deltasigWeighted = sqrt(deltasigWeighted)/2.;
  
  cout << endl << "  xSect[pb] = " << sigma0*1.e9 << "  uncert = " 
       << deltasig*1.e9 << "(" << deltasig/sigma0*100. << "%)" 
       << "  uncertWeighted = " << deltasigWeighted*1.e9 << "(" 
       << deltasigWeighted/(sigma0*weight0)*100. << "%)" << endl <<endl;

  ttbar << "  sigma[i]      weight[i] " << endl;
  for (int i = 0; i < 40; i++) ttbar << " " << sigma[i] << "   " << weight[i] << endl;

  ttbar << "  sigma0      weight0 " << endl;
  ttbar << " " << sigma0 << "   " << weight0 << endl;
  ttbar.close();
#endif
  //hOutputFile->Write();
  //hOutputFile->Close();
  outpt.close();
  if (Index == 0) events.close();
}
