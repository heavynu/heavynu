// program to estimate the theoretical PDF uncertainties for ttbar & Z+jets
// simulation
#include <iostream>
#include <fstream>
#include <vector>

#include "HepMC/PythiaWrapper.h"
#include "HepMC/IO_HEPEVT.h"
#include "HepMC/IO_AsciiParticles.h"
#include "HepMC/GenEvent.h"
#include "LHAPDF/LHAPDF.h"

using namespace std;

extern "C" {
  int pycomp_(int *);
  void pylist_(int *);
  void pygive_(const char*, int);
  void call_pygive(const std::string &line) {
       pygive_(line.c_str(), line.length());
  }
}

struct pdfs {
  int iev;
  int id1;
  int id2;
  double x1;
  double x2;
  double pdf1;
  double pdf2;
  double scalePDF;
  char selected;
};

int main() { 
  // usage: ./ttbar2.exe

  pyjets.n = 0;
  call_pyhepc(1); 
  HepMC::HEPEVT_Wrapper::set_max_number_entries(pydat1.mstu[8-1]);
  HepMC::HEPEVT_Wrapper::set_sizeof_real(8);
   
// ----------- ttbar settings -------------------------------

#if 0
  pysubs.msel = 6; // User defined processes 
  pydat2.pmas[1-1][6-1] = 175.; // top quark mass

  // Select leptonic mode
  call_pygive("24:alloff");
  call_pygive("24:onifany = 11 13 15");
#endif

// ---------- Z + jets settings ----------------------
#if 1
  pysubs.msel = 0;            // User defined processes',
  pysubs.msub[15-1] = 1;      // Z + jets',
  pysubs.ckin[3-1] = 40.;     // Pt lower cut',
  pysubs.ckin[43-1] = 200.;   // MZ lower cut',
  call_pygive("23:alloff");
  call_pygive("23:onifany = 11 13 15");
#endif
// ---------- general settings ------------------------

  pydat1.mstj[11-1] = 3; // Choice of the fragmentation function
  pydat1.mstj[22-1] = 2; 
  pydat1.mstu[21-1] = 1; // Check on possible errors during program execution

  pypars.mstp[2-1] = 1; 
  pypars.mstp[33-1] = 0; 
  pypars.mstp[81-1] = 1; // multiple parton interactions (1 is Pythia default)
  pypars.mstp[82-1] = 4; // multiple parton interactions (see p209 CERN-TH 7112/93) 
  pypars.parp[82-1] = 1.9; 
  pypars.parp[83-1] = 0.5; 
  pypars.parp[84-1] = 0.4; 
  pypars.parp[90-1] = 0.16; 

  // set random number seed (mandatory!)
  pydatr.mrpy[0] = 55122 ;

  // Tell Pythia not to write multiple copies of particles in event record.
  // pypars.mstp[128-1] = 2;

  // .......................................connect LHAPDF stuff
  pypars.mstp[52-1] = 2;
  //pypars.mstp[51-1] = 7; // cteq 5

  //........................................HepMC INITIALIZATIONS
  //

  int index;
  double sigma[41] = {0.};
  double Acceptance[41] = {0.};
  double sigma1;
  double weight1;
  double Pdf1[40], Pdf2[40], PdfNew1[40], PdfNew2[40];

  //... Loop over PDF sets:
  ifstream inpt;
  int mintest1 = 0; 
  int maxtest1 = 40;
  
  for (int itest = mintest1; itest < maxtest1+1; itest++) {
    char filename[6] = "out  ";
    sprintf(&filename[3],"%d",itest);

    inpt.open(filename);
    inpt >> index >> sigma1 >> weight1;
    inpt.close();
    sigma[index] = sigma1;
  }
 
  ifstream events;
  events.open("0pdfevents");
  vector<pdfs> mypdfs;
  
  //int NumOfEvents = 1000000;
  int NumOfEvents = 100000;
  for (int i = 0; i < NumOfEvents; i++) {
    int iev, id1, id2;
    double x1, x2, pdf1, pdf2, scalePDF;
    char selected;
    
    events >> iev >> id1 >> x1 >> pdf1 >> id2 >> x2 >> pdf2 >> scalePDF >> selected;
    //cout << "  iev = " << iev << "  id1 = " << id1 << "  x1 = " << x1 
    //     << "  pdf1 = " << pdf1 << "  id2 = " << id2 << "  x2 = " 
    //     << x2 << "  pdf2 = " << pdf2 << " scale = " << scalePDF 
    //     << "  selected = " << selected << endl; 
 
    pdfs thePdf;
    thePdf.iev = iev;
    thePdf.id1 = id1;
    thePdf.id2 = id2;
    thePdf.x1 = x1;
    thePdf.x2 = x2;
    thePdf.pdf1 = pdf1;
    thePdf.pdf2 = pdf2;
    thePdf.scalePDF = scalePDF;
    thePdf.selected = selected;
    mypdfs.push_back(thePdf);
  }
  events.close();

  //... Loop over PDF sets:
  mintest1 = 10000; // cteq 6
  maxtest1 = 10040;
  int ind = 0;
  //-------------------------------------------------------
  for (int itest = mintest1; itest < maxtest1+1; itest++) {

    //... selecting the PDF set
    //... (see http://hepforge.cedar.ac.uk/lhapdf/manual, Appendix A):
    pypars.mstp[51-1] = itest;
    
    // Call pythia initialization
    call_pyinit("CMS", "p", "p", 7000.);
   
    int Ngood = 0;  // number of selected events in the given PDF set
    int Ntotal = 0; // total number of events in the given PDF set
    
    //..........................................................................
    //........................................EVENT LOOP........................
    //..........................................................................
    
    cout << "event loop" << endl;
    
    double TotalWeight = 0;
    double SelectedWeight = 0;
    vector<pdfs>::iterator itr;
    for (itr = mypdfs.begin(); itr < mypdfs.end(); itr++) {
      Ntotal++;
      if (Ntotal%100000 == 1) cout << "Processing Event Number " << Ntotal << endl;

      pdfs pdf = *itr;
      int fl1 = (pdf.id1 == 21) ? 0 : pdf.id1;
      double PDFnew1 = LHAPDF::xfx(pdf.x1, pdf.scalePDF, fl1);
      int fl2 = (pdf.id2 == 21) ? 0 : pdf.id2;
      double PDFnew2 = LHAPDF::xfx(pdf.x2, pdf.scalePDF, fl2);
      //cout << "  fl1 = " << fl1 << "  PDF1 = " << PDFnew1 << " (" << pdf.pdf1 << ")" 
      //     << "  fl2 = " << fl2 << "  PDF2 = " << PDFnew2 << " (" << pdf.pdf2 << ")" 
      //     << "  scalePDF = " << pdf.scalePDF << endl;

      if (Ntotal == 200) {
        Pdf1[ind] = pdf.pdf1;
        Pdf2[ind] = pdf.pdf2;
        PdfNew1[ind] = PDFnew1;
        PdfNew2[ind] = PDFnew2;
      }  

      double Weight = PDFnew1*PDFnew2/(pdf.pdf1*pdf.pdf2);
      TotalWeight += Weight;

      if (pdf.selected == 'G') {
	Ngood++;
	SelectedWeight += Weight;
      }
    }

    //cout << "  Total weight = " << TotalWeight << "  SelectedWeight = " << SelectedWeight << endl;

    Acceptance[ind] = SelectedWeight/TotalWeight;
    //cout << "  ------------- ind = " << ind << "  Acceptance = " << Acceptance[ind] << endl;

    //cout << "  calculating Good events " << endl;

    if (ind == 0) {
      cout << "  Good events = " << Ngood << "  Total events = " << Ntotal << endl;
    }
    ind++;
  }
  
   //cout << " -------- PDF ------- PDF -------- PDF --------" << endl;
   //for (int i = 0; i < 40; i++) {
   //  cout << " i = " << i << " pdf1 = " << Pdf1[i] << "  pdf2 = " << Pdf2[i] << "  pdfNew1 = " 
   //       << PdfNew1[i] << "  PdfNew2 = " << PdfNew2[i] << endl;
   //} 

  double deltasig = 0;
  double deltasigWeighted = 0;

  //for (int i = 0; i < 41; i++) {
  //  cout << " Acceptance["  << i << "] = " << Acceptance[i] << endl;
  //}

  for (int i = 1; i < 41; i += 2) {
    double delta = Acceptance[i]-Acceptance[i+1];
    //cout << "  i = " << i << "  delta = " << delta << "  Acc[i] = " << Acceptance[i]
    //     << "  Acc[i+1] = " << Acceptance[i+1] << endl; 
    deltasig += delta*delta;
    delta = sigma[i]*Acceptance[i]-sigma[i+1]*Acceptance[i+1];
    deltasigWeighted += delta*delta;
  }
  
  deltasig = sqrt(deltasig)/2.;
  deltasigWeighted = sqrt(deltasigWeighted)/2.;
  
  cout << endl << "  xSect[pb] = " << sigma[0]*1.e9 << "  uncert = " 
       << deltasig << "(" << deltasig/Acceptance[0]*100. << "%)" 
       << "  uncertWeighted = " << deltasigWeighted << "(" 
       << deltasigWeighted/(sigma[0]*Acceptance[0])*100. << "%)" << endl <<endl;
}
