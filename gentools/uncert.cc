// program to estimate the theoretical PDF uncertainties for ttbar
// simulation
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <math.h>

using namespace std;
 
int main() { 

  int index = 0;
  double sigma0 = 0.;
  double sigma[40] = {0.};
  double weight0 = 0.;
  double weight[40] = {0.};
  double sigma1 = 0, weight1 = 0;

  ifstream inpt;
  inpt.open("out0");
  inpt >> index >> sigma0 >> weight0;
  inpt.close();

  cout << " index = " << index << " sigma0 = " <<  sigma0 << "  weight0 = " 
       << weight0 << endl;

  //... Loop over PDF sets:
  int mintest1 = 1; 
  int maxtest1 = 40;
  
  for (int itest = mintest1; itest < maxtest1+1; itest++) {
    char filename[6] = "out  ";
    sprintf(&filename[3],"%d",itest);
    cout << "  filename = " << filename << endl;

    inpt.open(filename);
    inpt >> index >> sigma1 >> weight1;
    inpt.close();
    sigma[index-1] = sigma1;
    weight[index-1] = weight1;

    cout << " index = " << index << " sigma[index] = " 
	 << sigma1 << "  weight[index] = " 
	 << weight1 << endl; 
  }

  double deltasig = 0;
  double deltasigWeighted = 0;
  
  for (int i = 0; i < 40; i += 2) {
    double delta = sigma[i]-sigma[i+1];
    deltasig += delta*delta;
    delta = sigma[i]*weight[i]-sigma[i+1]*weight[i+1];
    deltasigWeighted += delta*delta;
  }
  
  deltasig = sqrt(deltasig)/2.;
  deltasigWeighted = sqrt(deltasigWeighted)/2.;
  
  cout << endl << "  xSect[pb] = " << sigma0*1.e9 << "  uncert = " 
       << deltasig*1.e9 << "(" << deltasig/sigma0*100. << "%)" 
       << "  uncertWeighted = " << deltasigWeighted*1.e9 << "(" 
       << deltasigWeighted/(sigma0*weight0)*100. << "%)" << endl <<endl;
  
}
