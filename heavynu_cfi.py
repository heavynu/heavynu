import FWCore.ParameterSet.Config as cms
import re

# this function generates pythia cards for given [signalName]
# examples of [signalName]:
#   WR1200_nuRe500   -  e channel, M_WR = 1200, M_NuR = 500
#   WR1000_nuRmu150  - mu channel, M_WR = 1000, M_NuR = 150
def mkHeavyNuSettings(signalName):
  # check format of [signalName]
  m = re.match("^WR([\d]+)_nuR([emutau]+)([\d]+)$", signalName)
  
  # incorrect format, return empty card and exit
  if not m:
    return
  
  wr = m.group(1)
  ch = m.group(2)
  nu = m.group(3)
  
  nue = (nu if (ch == "e")   else "99999")
  num = (nu if (ch == "mu")  else "99999")
  nut = (nu if (ch == "tau") else "99999")
  
  conf = cms.vstring(
    'LeftRightSymmmetry:ffbar2WR = on',
    '9900024:m0 = %s  ! WR Mass' % wr,
    '9900012:m0 = %s  ! nu_Re'   % nue,
    '9900014:m0 = %s  ! nu_Rmu'  % num,
    '9900016:m0 = %s  ! nu_Rtau' % nut,
    '9900024:onMode = off',
    '9900024:onIfAny = 9900012 9900014 9900016'
  )

  return conf


# this setting block consists of pythia cards
# for generation of non-signal samples
customSettingsBlock = cms.PSet(
  
  TTbar_leptonic = cms.vstring(
    'MSEL=0                  ! User defined processes',
    'MSUB(81) = 1            ! qqbar to QQbar',
    'MSUB(82) = 1            ! gg to QQbar',
    'MSTP(7) = 6             ! flavour = top',
    'PMAS(6,1) = 175.        ! top quark mass',
    'MDME(190,1) = 0',
    'MDME(191,1) = 0',
    'MDME(192,1) = 0',
    'MDME(193,1) = 0',
    'MDME(194,1) = 0',
    'MDME(195,1) = 0',
    'MDME(196,1) = 0',
    'MDME(197,1) = 0',
    'MDME(198,1) = 0',
    'MDME(199,1) = 0',
    'MDME(200,1) = 0',
    'MDME(201,1) = 0',
    'MDME(202,1) = 0',
    'MDME(203,1) = 0',
    'MDME(204,1) = 0',
    'MDME(205,1) = 0',
    'MDME(206,1) = 1          ! W decay into e+ nu_e',
    'MDME(207,1) = 1          ! W decay into mu+ nu_mu',
    'MDME(208,1) = 1          ! W decay into tau+ nu_tau',
    'MDME(209,1) = 0',
    '9900024:alloff',
    '9900024:onifany = 9900012 9900014 9900016'
  ),
  
  Zjets = cms.vstring(
    'MSEL=0                  ! User defined processes',
    'MSUB(15) = 1            ! Z + jets',
    'CKIN(3) = 2.            ! Pt lower cut',
    '23:alloff',
    '23:onifany = 11 13 15'
  ),
  
  Zjets_pT20_MZ180 = cms.vstring(
    'MSEL=0                  ! User defined processes',
    'MSUB(15) = 1            ! Z + jets',
    'CKIN(3) = 20.           ! Pt lower cut',
    'CKIN(43) = 180.         ! MZ lower cut',
    '23:alloff',
    '23:onifany = 11 13 15'
  ),
  
  # settings to variate ISR/FSR amount for systematics study
  #   settings are taken from configuration fragments of
  #   corresponding Fall10 ttbar datasets:
  #     to see list of datasets search for "*ISRFSR*Fall10*"
  #     at Data Discovery page
  smallerISRFSR = cms.vstring(
    'MSTP(68)=1   ! choice of maximum virtuality scale and ME matching scheme for ISR',
    'PARP(67)=2.5 ! amount of ISR'
  ),
  
  largerISRFSR = cms.vstring(
    'PARP(64)=1.0  ! whopping factor for lambda_QCD in ISR', 
    'MSTP(3)=1     ! user sets Lambda_QCD',
    'PARP(61)=0.35 ! increase ISR',
    'PARP(72)=0.35 ! increase FSR',
    'PARJ(81)=0.35 ! increase FSR in resonance decays'
  )
)
