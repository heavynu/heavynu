#!/bin/bash

# print usage info
if [[ "$#" < "3" ]]; then
  echo "CRAB submission helper tool"
  echo "Usage:"
  echo "  ./mcrab.sh [list.txt] [crab.cfg] [dump_cfg.py]"
  echo "       [list.txt] - text file with list of datasets to proceed"
  echo "       [crab.cfg] - path to config file for CRAB"
  echo "    [dump_cfg.py] - path to config file for CMSSW"
  echo "  [lumi_mask.txt] - path to lumi mask JSON file (optional, only for data)"
  exit
fi

listtxt=$1
crabcfg=$2
pycfg=$3
lumitxt=$4

# check, does files exist
if [[ ! -e $listtxt ]]; then
  echo "ERROR: File $listtxt dosn't exists"
  exit 1
fi

if [[ ! -e $crabcfg ]]; then
  echo "ERROR: File $crabcfg dosn't exists"
  exit 1
fi

if [[ ! -e $pycfg ]]; then
  echo "ERROR: File $pycfg dosn't exists"
  exit 1
fi

if [[ "$lumitxt" != "" && ! -e $lumitxt ]]; then
  echo "ERROR: File $lumitxt dosn't exists"
  exit 1
fi

echo "CMSSW = $CMSSW_BASE"
echo ""

nFailed="0"

for i in $(cat $listtxt); do
  echo "[$(date)] Submitting $i"
  echo ""
  
  # extract first part of dataset name, replace '/' by '_'
  dir=$(echo $i | cut -d / -f 2- | tr / _)
  
  # create directory with this name
  mkdir $dir
  
  # copy config files
  cp $crabcfg $pycfg $lumitxt $dir/
  
  cd $dir/
  
  # edit crab.cfg to place corresponding datasetpath and pset
  sed -e "s,datasetpath=.*,datasetpath=$i," \
      -e "s,pset=.*,pset=$(basename $pycfg)," \
      -e "s,lumi_mask=.*,lumi_mask=$(basename $lumitxt)," \
      -i $(basename $crabcfg)
  
  # run crab
  crab -create -cfg $(basename $crabcfg) -submit
  
  # check exit code
  if [[ $? != 0 ]]; then
    echo ""
    echo "WARNING: fail to submit $i"
    echo "         see details in failed-$dir/"
    cd ..
    mv -f $dir failed-$dir
    let nFailed++
  else
    cd ..
  fi
  
  echo ""
done

if [[ "$nFailed" == "0" ]] ; then
  echo "INFO: all jobs submitted successfully"
else
  echo "WARNING: submission failed for $nFailed datasets"
  echo "         check above output to see the problem"
fi

echo ""

gridname="$(whoami | sed 's,^.,\U&,')"
echo "INFO: control execution progress from task monitoring page:"
echo "        http://dashb-cms-job-task.cern.ch/dashboard/request.py/taskmonitoring#action=tasksTable&usergridname=$gridname"
