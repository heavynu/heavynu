#!/bin/bash

# print usage info
if [[ "$1" == "" ]]; then
  echo "Usage:"
  echo "  $0 [path]"
  echo "    [path] - place, where backround samples situated"
  echo ""
  echo "Example:"
  echo "  $0 /moscow31/heavynu/22x/TauolaTTbar"
  echo "    the command merge these root files:"
  echo ""
  echo "      /moscow31/heavynu/22x/TauolaTTbar/crab_*/res/*.root"
  echo ""
  echo "    and will create file:"
  echo ""
  echo "      TauolaTTbar.root"
  exit
fi

# 1st parameter - path to directory
src=$1

# check, does directory exists
if [[ ! -d $src ]]; then
  echo "ERROR: Directory $src dosn't exists"
  exit 1
fi

# get number of crab output directories:
nCrabDirs="$(ls -1d $src/crab_* | wc -l)"

# check do we have crab output directory:
if [[ "$nCrabDirs" = "0" ]]; then
  echo "ERROR: can't find crab output directory:"
  echo "         $src/crab_*"
  echo "       probably you forget to create/submit crab jobs"
  exit 1
fi

# check do we have exactly one directory:
if [[ "$nCrabDirs" > "1" ]]; then
  echo "ERROR: found $nCrabDirs crab output directories:"
  ls -1d $src/crab_*
  echo "       this happens then you create crab jobs several times"
  echo "       remove unnecessary directories"
  exit 1
fi

# get number of jobs which have dublicate *.root files:
nDublicates="$(ls -1 $src/crab_*/res/*.root | grep -o '[^/]*$' | cut -d _ -f 2 | sort -n | uniq -d | wc -l)"

# check for number of dublicates:
if [[ "$nDublicates" > "0" ]]; then
  echo "ERROR: found $nDublicates jobs which have several output *.root files:"
  cd $src/crab_*/res
  pwd
  for i in $(ls -1 *.root | cut -d _ -f 2 | sort -n | uniq -d); do
    ls -1 *_${i}_*.root
  done
  echo "       this happens then you do resubmit several times"
  echo "       should be only one .root file per one job"
  echo "       remove unnecessary .root files"
  exit 1
fi

# expand path to absolute
fullSrc=$(cd $src; pwd)

echo "Source directory: $fullSrc"

# get the number of created crab jobs:
nCreated=$(grep -o "[0-9]* jobs created" $src/crab_*/log/crab.log | cut -d ' ' -f 1 | head -n 1)

echo "Total jobs      : $nCreated"

# list all root files:
list=""
totalFiles="0"

for i in $(ls -1 $src/crab_*/res/*.root); do
  let "totalFiles += 1"
  list="$list $i"
done

echo "Total files     : $totalFiles"

# check, do we have output .root files
if [[ "$totalFiles" == "0" ]] ; then
  echo "ERROR: No *.root files found"
  exit 1
fi

# check, does the number of output .root files equal to the number of jobs
if [[ "$totalFiles" != "$nCreated" ]]; then
  echo "WARNING: number of output files not equal to number of jobs"
  echo "         this happens if crab jobs is not complete"
fi

# destination file name
dst="$(basename $fullSrc).root"

echo "Destination file: ./$dst"

echo "Merging..."
hadd -f $dst $list > /dev/null

# check exit code
if [[ "$?" != 0 ]]; then
  rm $dst
  echo "ERROR: Merge failed!"
  exit 1
fi

# check integrity of $dst file
# file could be corrupted in old ROOT versions (https://root.cern.ch/phpBB3/viewtopic.php?t=14396)
# get list of TTrees and count number of all, "data" and "hlt" trees
content=$(echo -e '_file0->cd("patdump");\n_file0->ls()\n.q\n' | root -b -l "$dst" 2>/dev/null | grep TTree)
ntot=$(echo "$content" | wc -l)
ndata=$(echo "$content" | grep data | wc -l)
nhlt=$(echo "$content" | grep hlt | wc -l)

# proper output file should have 2 trees: one "data" and one "hlt"
if [[ "$ntot $ndata $nhlt" != "2 1 1" ]] ; then
  rm $dst
  echo "ERROR: broken file is produced by merge (hadd utility)"
  echo "       use newer ROOT version to run merge"
  echo "       file content:"
  echo "$content"
  exit 1
fi

echo "Merge completed successfully!"

#-> type, ecm, lumi, stat, xs, name, globaltag, cmssw, dataset

cmssw=$(cat $fullSrc/crab_*/res/*.stdout | grep ^CMSSW_VERSION | cut -d _ -f 3- | uniq)
dataset=$(cat $fullSrc/crab_*/share/AnalyzedBlocks.txt | cut -d '#' -f 1 | uniq)
name=$(echo $dataset | sed 's,^/\([^/_-]*\).*,\1,')

#data:
#  {2,  ecm,  0,  0,  0,  lumi,  full_stat,  -1, "name",    0,  "fname.root",  "globaltag",  "hlt",  "cmssw",  "dataset"},

#bkg:
#  {0,  ecm,  0,  0,  0,     0,  full_stat,  xs, "name",  pid,  "fname.root",  "globaltag",  "hlt",  "cmssw", "dataset"},

sampledb="{type,  ecm,  0,  0,  0,  lumi,  totalEvents,  xs,  \"$name\",  0,  \"$dst\",  \"globaltag\",  \"hlt\",  \"$cmssw\",  \"$dataset\"},"
echo "sampledb record : $sampledb"
