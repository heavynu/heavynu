# 1D Analysis Same Sign. 8 TeV 2012 ABCD. signal 8 TeV MWR=2000 MN=800
#
imax 12  number of channels
jmax *   number of backgrounds ('*' = automatic)
kmax *   number of nuisance parameters (sources of systematical uncertainties)
------------
# three channels, each with it's number of observed events 
bin           b600 b800  b1000 b1200  b1400  b1600  b1800  b2000  b2200  b2400  b2600  b2800
observation    38   14     10    2      2      2      0      0      0      0      0      0
------------
# now we list the expected events for signal and all backgrounds in those three bins
# the second 'process' line must have a positive number for backgrounds, and 0 for signal
# for the signal, we normalize the yields to an hypothetical cross section of 1/pb
# so that we get an absolute limit in cross section in units of pb.
# then we list the independent sources of uncertainties, and give their effect (syst. error)
# on each process and bin
bin           b600   b800     b1000   b1200    b1400   b1600   b1800    b2000    b2200   b2400    b2600     b2800   b600   b800   b1000   b1200   b1400  b1600  b1800   b2000   b2200  b2400  b2600   b2800
process        WR     WR        WR      WR       WR      WR      WR       WR       WR      WR       WR        WR     BG     BG        BG      BG       BG      BG      BG       BG       BG      BG       BG        BG
process         0     0         0       0        0       0       0        0        0       0        0         0       1      1          1       1        1       1       1        1        1       1        1        1    
rate 0.0755489 0.317305 0.740379 1.22389 2.3118 6.72385 20.0356 8.77878 0.876367 0.287086 0.0755489 0.0302196   14.1652 5.82487 2.39524 0.984944 0.405018 0.166547 0.0684856 0.0281619 0.0115804 0.00476198 0.00195817 0.000805216
------------
lumi    lnN   1.05   1.05     1.05     1.05      1.05    1.05    1.05    1.05     1.05    1.05     1.05       1.05      -       -        -        -         -      -        -       -        -        -        -       -   A lumi uncertainty, affects signal and MC-driven BG
eff     lnN   1.1    1.11     1.12     1.13      1.14    1.15    1.16     1.17    1.17    1.17     1.17       1.17      -       -        -        -         -       -       -       -        -        -        -       -   Efficiency
BGshape lnN    -      -        -         -        -        -       -       -       -       -        -           -    1.513  1.521    1.531     1.537     1.58    1.6     1.75    1.8     1.85       1.92     1.99     1.99      1.99  BG shape
