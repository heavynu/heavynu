#!/bin/bash
#  
# One input parament: output filename suffix

filename=heavynu_${1}.txt
echo "The output of this script will be saved in a file: " ${filename}


echo "# 2D Analysis. 13 TeV 2015 D. signal 13 TeV MWR= MN=
#
imax 120 number of channels
jmax *   number of backgrounds ('*' = automatic)
kmax *   number of nuisance parameters (sources of systematical uncertainties)
------------
# channels, each with it's number of observed events
bin b06-0 b06-1 b06-2 b06-3 b06-4 b06-5 b06-6 b06-7 b06-8 b06-9 b08-0 b08-1 b08-2 b08-3 b08-4 b08-5 b08-6 b08-7 b08-8 b08-9 b10-0 b10-1 b10-2 b10-3 b10-4 b10-5 b10-6 b10-7 b10-8 b10-9 b12-0 b12-1 b12-2 b12-3 b12-4 b12-5 b12-6 b12-7 b12-8 b12-9 b14-0 b14-1 b14-2 b14-3 b14-4 b14-5 b14-6 b14-7 b14-8 b14-9 b16-0 b16-1 b16-2 b16-3 b16-4 b16-5 b16-6 b16-7 b16-8 b16-9 b18-0 b18-1 b18-2 b18-3 b18-4 b18-5 b18-6 b18-7 b18-8 b18-9 b20-0 b20-1 b20-2 b20-3 b20-4 b20-5 b20-6 b20-7 b20-8 b20-9 b22-0 b22-1 b22-2 b22-3 b22-4 b22-5 b22-6 b22-7 b22-8 b22-9 b24-0 b24-1 b24-2 b24-3 b24-4 b24-5 b24-6 b24-7 b24-8 b24-9 b26-0 b26-1 b26-2 b26-3 b26-4 b26-5 b26-6 b26-7 b26-8 b26-9 b28-0 b28-1 b28-2 b28-3 b28-4 b28-5 b28-6 b28-7 b28-8 b28-9" > ${filename} #heavynu_tmplt.txt



#____Real_data_yields_parser_________________________
tmp1=` grep -nT "Data:" test.output | gawk '{print $1}'`
let data_begin=${tmp1}
echo ${data_begin}
tmp1=` grep -nT "Data_end" test.output | gawk '{print $1}'`
let data_end=${tmp1}-1
echo ${data_end}
let data_block=${data_end}-${data_begin}
block=`head -${data_end} test.output | tail -${data_block} | tr '\n\t' '  '| tr -s ' '`
echo 'observation ' ${block} >> ${filename} #heavynu_tmplt.txt


echo '-----------
# now we list the expected events for signal and all backgrounds in those three bins
# the second 'process' line must have a positive number for backgrounds, and 0 for signal
# for the signal, we normalize the yields to an hypothetical cross section of 1/pb
# so that we get an absolute limit in cross section in units of pb.
# then we list the independent sources of uncertainties, and give their effect (syst. error)
# on each process and bin' >> ${filename} #heavynu_tmplt.txt


echo "bin b06-0 b06-1 b06-2 b06-3 b06-4 b06-5 b06-6 b06-7 b06-8 b06-9 b08-0 b08-1 b08-2 b08-3 b08-4 b08-5 b08-6 b08-7 b08-8 b08-9 b10-0 b10-1 b10-2 b10-3 b10-4 b10-5 b10-6 b10-7 b10-8 b10-9 b12-0 b12-1 b12-2 b12-3 b12-4 b12-5 b12-6 b12-7 b12-8 b12-9 b14-0 b14-1 b14-2 b14-3 b14-4 b14-5 b14-6 b14-7 b14-8 b14-9 b16-0 b16-1 b16-2 b16-3 b16-4 b16-5 b16-6 b16-7 b16-8 b16-9 b18-0 b18-1 b18-2 b18-3 b18-4 b18-5 b18-6 b18-7 b18-8 b18-9 b20-0 b20-1 b20-2 b20-3 b20-4 b20-5 b20-6 b20-7 b20-8 b20-9 b22-0 b22-1 b22-2 b22-3 b22-4 b22-5 b22-6 b22-7 b22-8 b22-9 b24-0 b24-1 b24-2 b24-3 b24-4 b24-5 b24-6 b24-7 b24-8 b24-9 b26-0 b26-1 b26-2 b26-3 b26-4 b26-5 b26-6 b26-7 b26-8 b26-9 b28-0 b28-1 b28-2 b28-3 b28-4 b28-5 b28-6 b28-7 b28-8 b28-9  b06-0 b06-1 b06-2 b06-3 b06-4 b06-5 b06-6 b06-7 b06-8 b06-9 b08-0 b08-1 b08-2 b08-3 b08-4 b08-5 b08-6 b08-7 b08-8 b08-9 b10-0 b10-1 b10-2 b10-3 b10-4 b10-5 b10-6 b10-7 b10-8 b10-9 b12-0 b12-1 b12-2 b12-3 b12-4 b12-5 b12-6 b12-7 b12-8 b12-9 b14-0 b14-1 b14-2 b14-3 b14-4 b14-5 b14-6 b14-7 b14-8 b14-9 b16-0 b16-1 b16-2 b16-3 b16-4 b16-5 b16-6 b16-7 b16-8 b16-9 b18-0 b18-1 b18-2 b18-3 b18-4 b18-5 b18-6 b18-7 b18-8 b18-9 b20-0 b20-1 b20-2 b20-3 b20-4 b20-5 b20-6 b20-7 b20-8 b20-9 b22-0 b22-1 b22-2 b22-3 b22-4 b22-5 b22-6 b22-7 b22-8 b22-9 b24-0 b24-1 b24-2 b24-3 b24-4 b24-5 b24-6 b24-7 b24-8 b24-9 b26-0 b26-1 b26-2 b26-3 b26-4 b26-5 b26-6 b26-7 b26-8 b26-9 b28-0 b28-1 b28-2 b28-3 b28-4 b28-5 b28-6 b28-7 b28-8 b28-9
process    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR  WR    WR    WR    WR   WR    WR    WR    WR    WR    WR  WR    WR    WR    WR    WR    WR    WR    WR    WR    WR  WR    WR    WR    WR    WR    WR    WR    WR    WR    WR   WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR  WR    WR    WR    WR   WR    WR    WR    WR    WR    WR  WR    WR    WR    WR    WR    WR    WR    WR    WR    WR  WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR    WR   BG    BG    BG    BG    BG    BG    BG    BG    BG    BG   BG    BG    BG    BG    BG    BG    BG    BG    BG    BG   BG    BG    BG    BG    BG    BG    BG    BG    BG    BG  BG    BG    BG    BG    BG    BG    BG    BG    BG    BG  BG    BG    BG    BG    BG    BG    BG    BG    BG    BG BG    BG    BG    BG    BG    BG    BG    BG    BG    BG   BG    BG    BG    BG    BG    BG    BG    BG    BG    BG   BG    BG    BG    BG    BG    BG    BG    BG    BG    BG  BG    BG    BG    BG    BG    BG    BG    BG    BG    BG  BG    BG    BG    BG    BG    BG    BG    BG    BG    BG  BG    BG    BG    BG    BG    BG    BG    BG    BG    BG  BG    BG    BG    BG    BG    BG    BG    BG    BG    BG 
process   0     0     0     0     0     0     0     0     0     0    0     0     0     0     0     0     0     0     0     0   0     0     0     0     0     0     0     0     0     0  0     0     0     0     0     0     0     0     0     0  0     0     0     0     0     0     0     0     0     0   0     0     0     0     0     0     0     0     0     0    0     0     0     0     0     0     0     0     0     0   0     0     0     0     0     0     0     0     0     0  0     0     0     0     0     0     0     0     0     0  0     0     0     0     0     0     0     0     0     0   0     0     0     0     0     0     0     0     0     0   0     0     0     0     0     0     0     0     0     0  1     1     1     1     1     1     1     1     1     1    1     1     1     1     1     1     1     1     1     1    1     1     1     1     1     1     1     1     1     1  1     1     1     1     1     1     1     1     1     1  1     1     1     1     1     1     1     1     1     1  1     1     1     1     1     1     1     1     1     1    1     1     1     1     1     1     1     1     1     1    1     1     1     1     1     1     1     1     1     1  1     1     1     1     1     1     1     1     1     1  1     1     1     1     1     1     1     1     1     1   1     1     1     1     1     1     1     1     1     1   1     1     1     1     1     1     1     1     1     1" >> ${filename} #heavynu_tmplt.txt  


tmp=`grep -nT "Signal:" test.output | gawk '{print $1}'`
#echo ${tmp}
let signal_begin=${tmp}
tmp=`grep -nT "Signal_end" test.output | gawk '{print $1}'`
let signal_end=${tmp}-1
let signal_block=${signal_end}-${signal_begin}
block_s=`head -${signal_end} test.output | tail -${signal_block} |  awk '{print "  " $1 "  " $2 "  " $3 "  " $4 "  " $5 "  " $6 "  " $7 "  " $8 "  " $9 "  " $10 "  "}'  | sed 's/ 0 / 0.00001 /g'| tr '\n\t' '  ' | tr -s ' '` 
#echo $block_s
#echo " "

tmp=`grep -nT "BG_from_template:" test.output | gawk '{print $1}'`
#echo $tmp
let background_begin=${tmp}
#echo "bg begin" ${background_begin}
tmp=`grep -nT "BG_from_template_end" test.output | gawk '{print $1}'`
#echo $tmp
let background_end=${tmp}-1
#echo "bg end " ${background_end}
let background_block=${background_end}-${background_begin}
#echo $background_block
block_bg=`head -${background_end} test.output | tail -${background_block} | awk '{print "  " $1 "  " $2 "  " $3 "  " $4 "  " $5 "  " $6 "  " $7 "  " $8 "  " $9 "  " $10 "  "}' | sed 's/ 0 / 0.00001 /g'| tr '\n\t' '  '| tr -s ' '` 
#echo $block_bg
echo "rate " ${block_s} "  " ${block_bg} >> ${filename}  #heavynu_tmplt.txt 
echo " ------------
lumi    lnN   1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05    1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05   1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05   1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05   1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05   1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05   1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05   1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05   1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05   1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05   1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05   1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05 1.05   -    -    -    -    -    -    -    -    -    -     -    -    -    -    -    -    -    -    -    -       -    -    -    -    -    -    -    -    -    -        -    -    -    -    -    -    -    -    -    -        -    -    -    -    -    -    -    -    -    -         -    -    -    -    -    -    -    -    -    -           -    -    -    -    -    -    -    -    -    -          -    -    -    -    -    -    -    -    -    -      -    -    -    -    -    -    -    -    -    -        -    -    -    -    -    -    -    -    -    -      	 -    -    -    -    -    -    -    -    -    -       -    -    -    -    -    -    -    -    -    -   A lumi uncertainty, affects signal and MC-driven BG
eff     lnN   1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17   1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17   1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17   1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17   1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17    1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17    1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17    1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17    1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17    1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17    1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17    1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17 1.17       -    -    -    -    -    -    -    -    -    -       -    -    -    -    -    -    -    -    -    -      -    -    -    -    -    -    -    -    -    -      -    -    -    -    -    -    -    -    -    -      -    -    -    -    -    -    -    -    -    -      -    -    -    -    -    -    -    -    -    -      -    -    -    -    -    -    -    -    -    -      -    -    -    -    -    -    -    -    -    -      -    -    -    -    -    -    -    -    -    -      -    -    -    -    -    -    -    -    -    -      -    -    -    -    -    -    -    -    -    -      -    -    -    -    -    -    -    -    -    -   Efficiency
BGshape lnN     -    -    -    -    -    -    -    -    -    -       -    -    -    -    -    -    -    -    -    -      -    -    -    -    -    -    -    -    -    -      -    -    -    -    -    -    -    -    -    -      -    -    -    -    -    -    -    -    -    -      -    -    -    -    -    -    -    -    -    -      -    -    -    -    -    -    -    -    -    -      -    -    -    -    -    -    -    -    -    -      -    -    -    -    -    -    -    -    -    -      -    -    -    -    -    -    -    -    -    -      -    -    -    -    -    -    -    -    -    -      -    -    -    -    -    -    -    -    -    -       1.1   1.1   1.1   1.1   1.1   1.1   1.1   1.1   1.1   1.1     1.1   1.1   1.1   1.1   1.1   1.1   1.1   1.1   1.1   1.1     1.1   1.1   1.1   1.1   1.1   1.1   1.1   1.1   1.1   1.1     1.1   1.1   1.1   1.1   1.1   1.1   1.1   1.1   1.1   1.1      1.1   1.1   1.1   1.1   1.1   1.1   1.1   1.1   1.1   1.1    1.3   1.3   1.3   1.3   1.3   1.3   1.3   1.3   1.3   1.3     1.46  1.46  1.46  1.46  1.46  1.46  1.46  1.46  1.46  1.46     1.67  1.67  1.67  1.67  1.67  1.67  1.67  1.67  1.67  1.67      1.8   1.8   1.8   1.8   1.8   1.8   1.8   1.8   1.8   1.8     1.8   1.8   1.8   1.8   1.8   1.8   1.8   1.8   1.8   1.8     1.8   1.8   1.8   1.8   1.8   1.8   1.8   1.8   1.8   1.8     1.8   1.8   1.8   1.8   1.8   1.8   1.8   1.8   1.8   1.8  BG shape" >> ${filename} #heavynu_tmplt.txt
