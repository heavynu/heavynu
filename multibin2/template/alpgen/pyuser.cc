#include <iostream>
#include <fstream>
#include <vector>
#include "HepMC/PythiaWrapper.h"
#include "HepMC/IO_HEPEVT.h"
//#include "HepMC/IO_Ascii.h"
#include "HepMC/GenEvent.h"
#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TLorentzVector.h"

#include "ANHEPMC/JetableInputFromHepMC.h"
#include "ANHEPMC/JetFinderUA.h"
#include "ANHEPMC/LeptonAnalyserHepMC.h"

using namespace std;
 

double eff(double pt, double eta)
{
  double ptp[8] = {40., 45., 50., 55., 60., 70., 100., 200.};
  double effp[8]= {0.875, 0.905, 0.915, 0.92, 0.925, 0.93, 0.935, 0.94};
  double effp1[8]={0.85, 0.88, 0.895, 0.902, 0.91, 0.917, 0.926, 0.94};
  int index;

  double effi = 0.;
  if(pt < 40.) return effi;
  if(pt >= ptp[7]) {
    if(fabs(eta) < 1.44) return effp[7];
    effi = effp1[7];
  } else {
    index = -1;
    for (int i=0; i < 7; i++) {
      if(ptp[i] <= pt && ptp[i+1] > pt) {
        index = i;
        break;
      }
    }
    double eff0, eff1;
    if(fabs(eta) < 1.44) {
      eff0 = effp[index];
      eff1 = effp[index+1];
    } else {
      eff0 = effp1[index];
      eff1 = effp1[index+1];
    }
    effi = eff0 + (eff1 - eff0)*(pt - ptp[index])/(ptp[index+1] - ptp[index]);
  }
  if(fabs(eta) > 2.2) effi*=0.93;

  return effi;

}


int main() { 

  unsigned int Nevt = 500000;
  unsigned int i, N_2lep_X, N_2jet_X, N_2lep_1jet, N_2lep_2jet, nold;

  //........................................ROOT Histogramming
  TFile* hOutputFile;
  hOutputFile = new TFile("hist.root","RECREATE");

  int icomb, Ncomb = 1; // number of used combinations in heavynu

  TH1D* MWR = new TH1D("MWR", "MWR",50, 0., 5000.);
  TH1D* MNtoMWR = new TH1D("MNtoMWR", "MNtoMWR",10, 0., 1.);
  TH2D* MNvsMWR = new TH2D("MNvsMWR", "MNvsMWR",15, 0., 3000., 10, 0., 1.);

  pyjets.n=0;
  call_pyhepc(1);
  HepMC::HEPEVT_Wrapper::set_max_number_entries(pydat1.mstu[8-1]);
  HepMC::HEPEVT_Wrapper::set_sizeof_real(8);
//  HepMC::HEPEVT_Wrapper::set_max_number_entries(4000);
//  HepMC::HEPEVT_Wrapper::set_sizeof_real(8);
    
  //........................................PYTHIA INITIALIZATIONS
  // (Some platforms may require the initialization of pythia PYDATA block 
  //  data as external - if you get pythia initialization errors try 
  //  commenting in/out the below call to initpydata() )
  // initpydata();
  //..............................................................

  // set random number seed (mandatory!)
  pydatr.mrpy[0] = 55122 ;

  // Tell Pythia not to write multiple copies of particles in event record.
    pypars.mstp[128-1] = 2;

  // Call pythia initialization
  call_pyinit("USER", " ", " ", 0.);

  //........................................HepMC INITIALIZATIONS
  //
  // Instantiate an IO strategy for reading from HEPEVT.
  HepMC::IO_HEPEVT hepevtio;
  //
  // Instantiate an IO strategy to write the data to file - it uses the 
  //  same ParticleDataTable
  //HepMC::IO_Ascii ascii_io("test_pythia_hepmc.dat",std::ios::out);

  JetableInputFromHepMC JI(3.);
  JetFinderUA JF(0.5, 1., 10., 40.);
  LeptonAnalyserHepMC LA(2.4, 40.);

  N_2lep_X = 0;   
  N_2jet_X = 0;   
  N_2lep_1jet = 0;
  N_2lep_2jet = 0;
  nold=0;

  //........................................EVENT LOOP:
  for (i = 1; i <= Nevt; i++) {
    if (i % 10000 == 1)
      std::cout << "Processing event number " << i << std::endl;
    call_pyevnt();      // generate one event with Pythia
    if(pyint5.ngen[2][0] == nold) break;
    nold = pyint5.ngen[2][0];
// pythia pyhepc routine converts common PYJETS in common HEPEVT
    call_pyhepc(1);
    HepMC::GenEvent* evt = hepevtio.read_next_event();
// add some information to the event
    evt->set_event_number(i);
    evt->set_signal_process_id(20);
// write the event out to the ascii file
//    ascii_io << evt;

// Analysing the event:
    vector<JetableObject> objects = JI.readFromHepMC (evt);
    vector<Jet> alljets = JF.findJets (objects);
    vector<Jet> crudejets = LA.removeLeptonsFromJets (alljets, evt);

    vector<Jet> jets;
    vector<Jet>::iterator ijet;
    for ( ijet = crudejets.begin(); ijet != crudejets.end(); ijet++) {
      if(fabs(ijet->eta()) < 2.5) jets.push_back(*ijet);
    }

    if (jets.size() >= 2) N_2jet_X ++;
    if (LA.nIsolatedLeptons (evt) >= 2) {
       N_2lep_X ++;
       if (jets.size() >= 1)
           N_2lep_1jet ++;
       if (jets.size() >= 2)
           N_2lep_2jet ++;
    }

    //int mode=1;
    //pylist_(&mode);

    // WR analysis:
    vector<HepMC::GenParticle> isoleptons = LA.isolatedLeptons(evt);
    
    int iaccept = 1;
    
    if(jets.size() < 2) iaccept=0;
    if(isoleptons.size() < 2) iaccept=0;
    
    int iaccept0 = iaccept;
    if(iaccept0) {
      if(isoleptons[0].momentum().perp() < 60.) iaccept=0;
    
      if(abs(isoleptons[0].pdg_id()) != 11) iaccept=0; // only electrons
    
      if( abs(isoleptons[0].pdg_id()) != abs(isoleptons[1].pdg_id()) ) iaccept=0; // same flavour, any sign
    
      if(fabs(isoleptons[0].momentum().eta()) > 1.5 &&
         fabs(isoleptons[1].momentum().eta()) > 1.5) iaccept = 0;

      double mllmin=10000.;
      for ( vector<HepMC::GenParticle>::iterator p = isoleptons.begin();
            p != isoleptons.end(); ++p ){
       
        //if(fabs((p)->momentum().eta()) > 2.4) continue; // applied in LA
        //if((p)->momentum().perp() < 20.) continue;      // applied in LA
        
        vector<HepMC::GenParticle>::iterator p1 = p;
        p1++;
        for ( vector<HepMC::GenParticle>::iterator p2 = p1;
              p2 != isoleptons.end(); ++p2 ){
    
          //if ( (p2)->pdg_id() != -(p)->pdg_id() ) continue; // same flavour, different sign
          if ( abs((p2)->pdg_id()) != abs((p)->pdg_id()) ) continue; // same flavour, any sign
          double m2 = 2. * ( (p)->momentum().e()*(p2)->momentum().e() -
                             (p)->momentum().px()*(p2)->momentum().px() -
                             (p)->momentum().py()*(p2)->momentum().py() -
                             (p)->momentum().pz()*(p2)->momentum().pz() );
          double llmass = sqrt(m2);
          //std::cout << " mll = " << llmass << std::endl;  
          if(llmass < mllmin) mllmin = llmass;
        }
      }
        
      if(mllmin < 200.) iaccept = 0;
        
    } // if(iaccept0)
        
    if(iaccept) {
    
      //int mode=1;
      //pylist_(&mode);
          
      TLorentzVector pmylept1(isoleptons[0].momentum().px(),
                              isoleptons[0].momentum().py(),
                              isoleptons[0].momentum().pz(),
                              isoleptons[0].momentum().e());

      TLorentzVector pmylept2(isoleptons[1].momentum().px(),
                              isoleptons[1].momentum().py(),
                              isoleptons[1].momentum().pz(),
                              isoleptons[1].momentum().e());
      
      TLorentzVector pmyjet1(jets[0].px(),
                             jets[0].py(),
                             jets[0].pz(),
                             jets[0].e());

      TLorentzVector pmyjet2(jets[1].px(),
                             jets[1].py(),
                             jets[1].pz(),
                             jets[1].e());

      for (icomb=0; icomb < Ncomb; icomb++) {

        TLorentzVector pmyleptnu;

        if (Ncomb == 1) {
          pmyleptnu = pmylept1;
          if(pmylept2.Pt() < pmyleptnu.Pt()) pmyleptnu = pmylept2;
        }

        if (Ncomb == 2) {
          if (icomb == 0) pmyleptnu = pmylept1;
          if (icomb == 1) pmyleptnu = pmylept2;
        }

        double xmwr = (pmylept1 + pmylept2 + pmyjet1 + pmyjet2).M();
        double xmn = (pmyleptnu + pmyjet1 + pmyjet2).M();
                              
//        std::cout << "candidate, MWR = " << xmwr << " MN = " << xmn << std::endl;
      
        double weight = eff(isoleptons[0].momentum().perp(), isoleptons[0].momentum().eta()) *
                        eff(isoleptons[1].momentum().perp(), isoleptons[1].momentum().eta());

        MWR-> Fill(xmwr, weight);
        MNtoMWR->Fill(xmn/xmwr, weight);
        MNvsMWR->Fill(xmwr, xmn/xmwr, weight);

      }

    }

    // we also need to delete the created event from memory
    delete evt;
  } // end of event loop

  // write out some information from Pythia to the screen
  call_pystat(1);    

  hOutputFile->Write();
  hOutputFile->Close();

  cout << "Nev two isolated leptons:                     " << N_2lep_X << endl;
  cout << "Nev two or more jets:                         " << N_2jet_X << endl;
  cout << "Nev two isolated leptons and jet(s):          " << N_2lep_1jet << endl;
  cout << "Nev two isolated leptons and two or more jets " << N_2lep_2jet << endl;

  return 0;
}
