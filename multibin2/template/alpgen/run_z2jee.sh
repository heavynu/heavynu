#!/bin/sh -e

echo "Generating z2jee ..."
echo ""

echo "Building zjet ..."
make zjet
echo ""

echo "Generating weighted events ..."
cat > inputalpgen1 << EOF
1         ! imode
z2jee   ! label for files
0 ! start with: 0=new grid, 1=previous warmup grid, 2=previous generation grid
200000 3   ! Nevents/iteration,  N(warm-up iterations)
50000000   ! Nevents generated after warm-up
*** The above 5 lines provide mandatory inputs for all processes
*** (Comment lines are introduced by the three asteriscs)
*** The lines below modify existing defaults for the hard process under study
*** For a complete list of accessible parameters and their values,
*** input 'print 1' (to display on the screen) or 'print 2' to write to file
ih2 1
ebeam 6500
irapgap 0            ! tells phase space to force one fwd/ one bkwd jets
etagap 2             ! eta value to separate central and fwd jets
etajmax 5            ! full rap range for jets
njets   2            ! total number of jets
ptjmin  20           ! ptmin for fwd jets
drjmin  0.5
mllmin 200.
mllmax 13000.
EOF

./zjetgen.exe < inputalpgen1
echo ""


echo "Unweighting events ..."
cat > inputalpgen2 << EOF
2
z2jee
izdecmode 1
EOF

./zjetgen.exe < inputalpgen2
echo ""

echo "Event generation finished"
