// File: pythia8_test1.cc
//
#include "Pythia8/Pythia.h"
#include "Pythia8Plugins/HepMC2.h"

#include "HepMC/GenEvent.h"
// Following line to be used with HepMC 2.04 onwards.
#include "HepMC/Units.h"

#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TLorentzVector.h"

#include "ANHEPMC/JetableInputFromHepMC.h"
#include "ANHEPMC/JetFinderUA.h"
#include "ANHEPMC/LeptonAnalyserHepMC.h"

using namespace Pythia8; 

//**************************************************************************

double eff(double pt, double eta)
{
  double ptp[8] = {40., 45., 50., 55., 60., 70., 100., 200.};
  double effp[8]= {0.875, 0.905, 0.915, 0.92, 0.925, 0.93, 0.935, 0.94};
  double effp1[8]={0.85, 0.88, 0.895, 0.902, 0.91, 0.917, 0.926, 0.94};
  int index;

  double effi = 0.;
  if(pt < 40.) return effi;
  if(pt >= ptp[7]) {
    if(fabs(eta) < 1.44) return effp[7];
    effi = effp1[7];
  } else {
    index = -1;
    for (int i=0; i < 7; i++) {
      if(ptp[i] <= pt && ptp[i+1] > pt) {
        index = i;
        break;
      }
    }
    double eff0, eff1;
    if(fabs(eta) < 1.44) {
      eff0 = effp[index];
      eff1 = effp[index+1];
    } else {
      eff0 = effp1[index];
      eff1 = effp1[index+1];
    }
    effi = eff0 + (eff1 - eff0)*(pt - ptp[index])/(ptp[index+1] - ptp[index]);
  }
  if(fabs(eta) > 2.2) effi*=0.93;

  return effi;

}


int main() {

  //........................................ROOT Histogramming
  TFile* hOutputFile;
  hOutputFile = new TFile("hist.root","RECREATE");

  int Nevents = 4000000;
  int icomb, Ncomb = 1; // number of used combinations in heavynu

  TH1D* MWR = new TH1D("MWR", "MWR",50, 0., 5000.);
  TH1D* MNtoMWR = new TH1D("MNtoMWR", "MNtoMWR",10, 0., 1.);
  TH2D* MNvsMWR = new TH2D("MNvsMWR", "MNvsMWR",15, 0., 3000., 10, 0., 1.);

  HepMC::Pythia8ToHepMC ToHepMC;

  JetableInputFromHepMC JI(3.);
  JetFinderUA JF(0.5, 1., 10., 40.);
  LeptonAnalyserHepMC LA(2.4, 40.);

  // Generator. Process selection.
  Pythia pythia;
  // Pick the random number seed
  pythia.readString("Random:setSeed = on");
  pythia.readString("Random:seed = 101"); // if 0 it is random, based on clock
  //
  pythia.readString("Top:gg2ttbar = on");
  pythia.readString("Top:qqbar2ttbar = on");
  // CMS configuration taken from:
  // https://github.com/cms-sw/cmssw/blob/CMSSW_7_4_X/Configuration/Generator/python/
  //         Hadronizer_TuneCUETP8M1_13TeV_generic_LHE_pythia8_cff.py
  pythia.readString("Tune:preferLHAPDF = 2");
  pythia.readString("Tune:pp 14");
  pythia.readString("Tune:ee 7");
  pythia.readString("MultipartonInteractions:pT0Ref=2.4024");
  pythia.readString("MultipartonInteractions:ecmPow=0.25208");
  pythia.readString("MultipartonInteractions:expPow=1.6");
  //
  pythia.readString("Main:timesAllowErrors = 10000");
  pythia.readString("Check:epTolErr = 0.01");
  pythia.readString("Beams:setProductionScalesFromLHEF = off");
  pythia.readString("SLHA:keepSM = on");
  pythia.readString("SLHA:minMassSM = 1000.");
  pythia.readString("ParticleDecays:allowPhotonRadiation = on");
  //'ParticleDecays:limitTau0 = on',  //These two settings are for full simulation
  //'ParticleDecays:tau0Max = 10',

  // suppress full listing of first events in pythia8 > 160
  pythia.readString("Next:numberShowEvent = 0");

  // Choose decay modes for W : switch off everything but W -> electron + X
  pythia.readString("24:onMode = off");
  pythia.readString("24:onIfAny = 11");

  //LHC initialization.

  pythia.readString("Beams:eCM = 13000.");
  pythia.init();

  // Shorthand for the event and for settings.
  Event& event = pythia.event;
  Settings& settings = pythia.settings;

  // Read in commands from external file.
  //pythia.readFile("pythia8_test1.cmnd");

  // Extract settings to be used in the main program.
  int nList = settings.mode("Next:numberShowEvent");
  int nShow = settings.mode("Next:numberCount");
  int nAbort = settings.mode("Main:timesAllowErrors");
  //bool showChangedSettings = settings.flag("Init:showChangedSettings");
  //bool showAllSettings = settings.flag("Init:showAllSettings");
  //bool showAllParticleData = settings.flag("Init:showAllParticleData");

  // List data and particle data. Commented out because by default are listed at the init step
  //if (showChangedSettings) settings.listChanged();
  //if (showAllSettings) settings.listAll();
  //if (showAllParticleData) pythia.particleData.listAll();

  // Begin event loop.
  int nShowPace = max(1,nShow);
  int iAbort = 0; 
  for (int iEvent = 0; iEvent < Nevents; ++iEvent) {
    if (iEvent%nShowPace == 0) cout << " Now begin event " 
      << iEvent << endl;

    // Generate events. Quit if too many failures.
    if (!pythia.next()) {
      if (++iAbort < nAbort) continue;
      cout << " Event generation aborted prematurely, owing to error!\n"; 
      exit(1);
    }
 
    // List first few events, both hard process and complete events.
    if (iEvent < nList) { 
//      pythia.process.list();
//      event.list();
    }

    // HepMC installed on CERN AFS by default have MeV as energy units
    // (ATLAS standard...), though default HepMC units are GeV. For 
    // this reason, if the first line below is used, in pythia8
    // versions > 165 HepMC record will be in MeV
    // and we will have incorrect results

    //HepMC::GenEvent* hepmcevt = new HepMC::GenEvent();
    HepMC::GenEvent* hepmcevt =
      new HepMC::GenEvent(HepMC::Units::GEV, HepMC::Units::MM);

    ToHepMC.fill_next_event( event, hepmcevt );

    // Analysing the event:
    vector<JetableObject> objects = JI.readFromHepMC (hepmcevt);
    vector<Jet> alljets = JF.findJets (objects);
    vector<Jet> crudejets = LA.removeLeptonsFromJets (alljets, hepmcevt);
    vector<HepMC::GenParticle> isoleptons = LA.isolatedLeptons(hepmcevt);

    vector<Jet> jets;
    vector<Jet>::iterator ijet;
    for ( ijet = crudejets.begin(); ijet != crudejets.end(); ijet++) {
      if(fabs(ijet->eta()) < 2.5) jets.push_back(*ijet);
//	TLorentzVector tmpvect(ijet->px(),
//                             ijet->py(),
//                             ijet->pz(),
//                             ijet->e());
    }

    int iaccept = 1;

    if(jets.size() < 2) iaccept=0;
    if(isoleptons.size() < 2) iaccept=0;

    int iaccept0 = iaccept;
    if(iaccept0) {
      if(isoleptons[0].momentum().perp() < 60.) iaccept=0;

      if(abs(isoleptons[0].pdg_id()) != 11) iaccept=0; // only electrons

      if( abs(isoleptons[0].pdg_id()) != abs(isoleptons[1].pdg_id()) ) iaccept=0; // same flavour, any sign

      if(fabs(isoleptons[0].momentum().eta()) > 1.5 &&
         fabs(isoleptons[1].momentum().eta()) > 1.5) iaccept = 0;

      double mllmin=10000.;
      for ( vector<HepMC::GenParticle>::iterator p = isoleptons.begin();
            p != isoleptons.end(); ++p ){

        //if(fabs((p)->momentum().eta()) > 2.4) continue; // applied in LA
        //if((p)->momentum().perp() < 20.) continue;	  // applied in LA

        vector<HepMC::GenParticle>::iterator p1 = p;
        p1++;
	for ( vector<HepMC::GenParticle>::iterator p2 = p1;
              p2 != isoleptons.end(); ++p2 ){

          //if ( (p2)->pdg_id() != -(p)->pdg_id() ) continue; // same flavour, different sign
          if ( abs((p2)->pdg_id()) != abs((p)->pdg_id()) ) continue; // same flavour, any sign
          double m2 = 2. * ( (p)->momentum().e()*(p2)->momentum().e() -
                             (p)->momentum().px()*(p2)->momentum().px() -
                             (p)->momentum().py()*(p2)->momentum().py() -
                             (p)->momentum().pz()*(p2)->momentum().pz() );
          double llmass = sqrt(m2);
          //std::cout << " mll = " << llmass << std::endl;
          if(llmass < mllmin) mllmin = llmass;
        }
      }

      if(mllmin < 200.) iaccept = 0;

    } // if(iaccept0)

    if(iaccept) {

      //int mode=1;
      //pylist_(&mode);

      TLorentzVector pmylept1(isoleptons[0].momentum().px(),
                              isoleptons[0].momentum().py(),
                              isoleptons[0].momentum().pz(),
                              isoleptons[0].momentum().e());

      TLorentzVector pmylept2(isoleptons[1].momentum().px(),
                              isoleptons[1].momentum().py(),
                              isoleptons[1].momentum().pz(),
                              isoleptons[1].momentum().e());

      TLorentzVector pmyjet1(jets[0].px(),
                             jets[0].py(),
                             jets[0].pz(),
                             jets[0].e());

      TLorentzVector pmyjet2(jets[1].px(),
                             jets[1].py(),
                             jets[1].pz(),
                             jets[1].e());

      for (icomb=0; icomb < Ncomb; icomb++) {

        TLorentzVector pmyleptnu;

        if (Ncomb == 1) {
          pmyleptnu = pmylept1;
          if(pmylept2.Pt() < pmyleptnu.Pt()) pmyleptnu = pmylept2;
        }

	if (Ncomb == 2) {
          if (icomb == 0) pmyleptnu = pmylept1;
          if (icomb == 1) pmyleptnu = pmylept2;
        }

	double xmwr = (pmylept1 + pmylept2 + pmyjet1 + pmyjet2).M();
        double xmn = (pmyleptnu + pmyjet1 + pmyjet2).M();

//        std::cout << "candidate, MWR = " << xmwr << " MN = " << xmn << std::endl;

//        std::cout << "electron, pt = " << isoleptons[1].momentum().perp()
//                  << " eta = " << isoleptons[1].momentum().eta()
//                  << " eff = " << eff(isoleptons[1].momentum().perp(), isoleptons[1].momentum().eta()) << endl;

        double weight = eff(isoleptons[0].momentum().perp(), isoleptons[0].momentum().eta()) *
                        eff(isoleptons[1].momentum().perp(), isoleptons[1].momentum().eta());

        MWR-> Fill(xmwr, weight);
        MNtoMWR->Fill(xmn/xmwr, weight);
        MNvsMWR->Fill(xmwr, xmn/xmwr, weight);

      }

    }

    delete hepmcevt;

  // End of event loop.
  }

  // Final statistics.
  pythia.stat();

  hOutputFile->Write();
  hOutputFile->Close();

  // Done.
  return 0;
}
