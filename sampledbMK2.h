#ifndef sampledb_h
#define sampledb_h
#include <algorithm>
#include <iostream>
#include <vector>
#include <string>
#include <sstream>

// originally, signal crossections in the table is LO cross sections
// we use the same k-factor for 7, 10 and 14 TeV samples
//const float k = 1.13; // k-factor for the signal (old version)
//const float k = 1.3; // k-factor for the signal for 1 < MWR < 1.3

// signal samples: dependence of k-factor on WR mass
const float sigk[] =
 {1.33, 1.33, 1.33, 1.33, 1.33,  //    0 ...  400 GeV
  1.32, 1.32, 1.32, 1.32, 1.31,  //  500 ...  900
  1.31, 1.30, 1.30, 1.29, 1.28,  // 1000 ... 1400
  1.28, 1.27, 1.27, 1.27, 1.27,  // 1500 ... 1900
  1.26, 1.26, 1.26, 1.26, 1.25,  // 2000 ... 2400
  1.25, 1.25                     // 2500, 2600
 };

// k-factor for background samples
//const float 1.250000 = 1.370; // old
//const float 1.250000 = 1.250000; // k-factor for Alpgen Z+jets Fall 2010 production (tot = 3048 pb)
//const float 1.236000 = 1.236000;//// k-factor for Alpgen W+jets Fall 2010 production (tot = 31314 pb)

// record of sample properties
struct Sample {
  int type;            // 0 - background (MC), 1 - signal (MC), 2 - data, 3 - unknown
  int ECM;             // beam energy [TeV]
  int MW;        
  // equality operator      // W_R mass [GeV]
  int MNu;             // Nu_R mass [GeV]
  int channel;         // 0 - all, 1 - electron, 2 - muon, 3 -tau
  float lumi;          // Integrated luminosity [pb^-1], the field valid only for data samples
  int stat;            // Number of events
  double CS;            // Cross-section [pb]
  std::string name;    // name
  int pid;             // Process ID
  std::string fname;   // file name
  std::string AlCa;    // frontier conditions (https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideFrontierConditions)
  std::string HLTmenu; // HLT Menu            (https://twiki.cern.ch/twiki/bin/view/CMSPublic/SWGuideGlobalHLT)
  std::string CMSSW;   // version
  std::string DBS;     // sample path in DAS  (https://cmsweb.cern.ch/das/)
  
  // function return weight of event in sample
  //   L - integrated luminosity [pb^-1]
  float weight(const float L) const
  {
    // return unit weight for data or unknown samples
    if (type == 2 || type == 3) return 1;
    
    // return -1 if event weight stored in tree
    if (isIntegratedWeightPid()) return -1;
    
    return L*CS/stat;
  }
  
  // return true if real values of Process ID and Event Weight
  // should be read from the tree for this sample
  // this is necessary for several CSA07 MC background samples
  // (because events from several processes was mixed in one dataset)
  bool isIntegratedWeightPid() const
  {
    return ((type == 0) && (CS < 0) && (pid < 0));
  }
  
  // the function check, if "par" == "val" for this sample
  bool check(const std::string par, const int val) const
  {
         if (par == "type")    return (val == type);
    else if (par == "ECM")     return (val == ECM);
    else if (par == "MW")      return (val == MW);
    else if (par == "MNu")     return (val == MNu);
    else if (par == "channel") return (val == channel);
    else if (par == "stat")    return (val == stat);
    else if (par == "pid")     return (val == pid);
    else return false;
  }
  
  bool check(const std::string par, const std::string val) const
  {
         if (par == "name")    return (val == name);
    else if (par == "fname") {
      // get file name from full path
      std::string fn = val;
      const std::string::size_type pos = fn.find_last_of('/');
      if (pos != std::string::npos)
        fn.erase(0, pos + 1);
      
      return (fn == fname);
    }
    else if (par == "AlCa")    return (val == AlCa);
    else if (par == "HLTmenu") return (val == HLTmenu);
    else if (par == "CMSSW")   return (val == CMSSW);
    else if (par == "DBS")     return (val == DBS);
    else return false;
  }
  
  // equality operator, check only by file name (have to be uniq)
  bool operator == (const Sample& sa) const
  {
    return (fname == sa.fname);
  }
  
  // overload of << operator to allow easy printing of Sample
  friend std::ostream& operator<<(std::ostream& os, const Sample& sa)
  {
    const char* type[4] = {"background", "signal", "data", "unknown"};
    const char* channel[3] = {"all", "electron", "muon"};
    
    os << "[Sample]: " << sa.name << " (" << type[sa.type] << ")\n";
    
    if (sa.type == 0) // background
      os << "  ECM = "   << sa.ECM << " TeV\n"
         << "  stat = "  << sa.stat << " events\n"
         << "  CS = "    << sa.CS << " pb\n"
         << "  fname = " << sa.fname << "\n"
         << "  AlCa = "  << sa.AlCa << "\n"
         << "  HLTmenu = "  << sa.HLTmenu << "\n"
         << "  CMSSW = " << sa.CMSSW << "\n"
         << "  DBS = "   << sa.DBS << "\n";
    
    else if (sa.type == 1) // signal
      os << "  ECM = "   << sa.ECM << " TeV\n"
         << "  channel = "  << channel[sa.channel] << "\n"
         << "  MW / MNu = "  << sa.MW << " / " << sa.MNu << " GeV\n"
         << "  stat = "  << sa.stat << " events\n"
         << "  CS = "    << sa.CS << " pb\n"
         << "  fname = " << sa.fname << "\n"
         << "  AlCa = "  << sa.AlCa << "\n"
         << "  HLTmenu = "  << sa.HLTmenu << "\n"
         << "  CMSSW = " << sa.CMSSW << "\n";
    
    else if (sa.type == 2) // data
      os << "  ECM = "   << sa.ECM << " TeV\n"
         << "  integrated luminosity = "  << sa.lumi << " pb^-1\n"
         << "  stat = "  << sa.stat << " events\n"
         << "  fname = " << sa.fname << "\n"
         << "  AlCa = "  << sa.AlCa << "\n"
         << "  HLTmenu = "  << sa.HLTmenu << "\n"
         << "  CMSSW = " << sa.CMSSW << "\n"
         << "  DBS = "   << sa.DBS << "\n";
    else // unknown
      os << "  fname = " << sa.fname << "\n";
    
    return os;
  }
};

// array of all samples
// Note: for the Run 2 amcatnlo samples stat is the sum of weights normalized to 1 (i.e. stat = N1 - N2 where N1 - N events
// with positive weight, N2 - N events with negative weight)
const Sample AllSamples[] = {
// === data =========================================================================================================================================================  
// type  ECM  MW   MNu   channel      lumi           stat      CS                name            pid          fname                        AlCa        HLTmenu   CMSSW           DBS

   //for test 
   {2,    13,  0,   0,   1,         2.687067768,     99898,    -1,           "DATA_example",      0,     "DATA_example.root",       "GR_R_74_V15::All",  "",   "7_4_14",  "DoubleEG_Run2015D-PromptReco-v4_MINIAOD"},
   {0,    13,  0,   0,   1,         -1,              12222,   2008.4,          "Z-bozon",         0,        "Z-bozon.root",       "MCRUN2_74_V9::All",   "",   "7_4_14",  "DYJetsToLL_M-50_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8_MINIAODSIM"},
   {0,    13,  0,   0,   1,         -1,              38101,   2008.4,          "Z-bozon_3",       0,        "Z-bozon_3.root",       "MCRUN2_74_V9::All",   "5E33",   "7_4_14",  "/DYToEE_13TeV-amcatnloFXFX-pythia8/RunIISpring15MiniAODv2-74X_mcRun2_asymptotic_v2-v1/MINIAODSIM"},

  
  // runs 131511-137436:
  {2,    7,  0,   0,   0,         0,               -1,   -1,          "Jun14th",       0, "Jun14th.root",       "GR_R_36X_V12A::All", "",   "3_6_1_patch4",  "/MinimumBias/Commissioning10-GOODCOLL-Jun14thSkim_v1/RECO"},
  // the same, but with no cut on number of jets:
  {2,    7,  0,   0,   0,         0,               -1,   -1,          "Jun14th_0jets", 0, "Jun14th_0jets.root", "GR_R_36X_V12A::All", "",   "3_6_1_patch4",  "/MinimumBias/Commissioning10-GOODCOLL-Jun14thSkim_v1/RECO"},
  // runs 138564-143328:
  {2,    7,  0,   0,   0,   1.199796007,     20285791,   -1,          "Sep01st_EG",    0, "Sep01st_EG.root",    "", "",   "3_6_1_patch4",  "/EG/Run2010A-PromptReco-v4/RECO"},
  {2,    7,  0,   0,   0,   1.192784844,     10118066,   -1,          "Sep01st_Mu",    0, "Sep01st_Mu.root",    "", "",   "3_6_1_patch4",  "/Mu/Run2010A-PromptReco-v4/RECO"},                                 
  // runs 143657-144114:
  {2,    7,  0,   0,   0,   1.532765582,     18394989,   -1,          "Sep07th_EG",    0, "Sep07th_EG.root",    "", "",   "3_6_1_patch4",  "/EG/Run2010A-PromptReco-v4/RECO"},
  // runs 138564-144114: /contains same runs as for Sep01st_Mu plus new ones. 
  {2,    7,  0,   0,   0,   2.745788035,     16292083,   -1,          "Sep07th_Mu",    0, "Sep07th_Mu.root",    "", "",   "3_6_1_patch4",  "/Mu/Run2010A-PromptReco-v4/RECO"},
  // runs 146428-146644:
  {2,    7,  0,   0,   0,   1.02113987,      3087259,   -1,           "Oct02nd_EG",    0,           "Oct02nd_EG.root",    "GR10_P_V11::All", "2E31",      "3_8_3",  "/Electron/Run2010B-PromptReco-v2/RECO"},
  // runs 146428-146644:
  {2,    7,  0,   0,   0,   1.005746585,      2192691,   -1,          "Oct02nd_Mu",    0,           "Oct02nd_Mu.root",    "GR10_P_V11::All", "2E31",      "3_8_3",  "/Mu/Run2010B-PromptReco-v2/RECO"},
  // runs 146804-147454:
  {2,    7,  0,   0,   0,   4.276753312,      9577128,   -1,          "Oct15th_EG",    0,           "Oct15th_EG.root",    "GR10_P_V11::All", "2E31",      "3_8_3",  "/Electron/Run2010B-PromptReco-v2/RECO"},
  // runs 146804-147454:
  {2,    7,  0,   0,   0,   4.294949975,      7706444,   -1,          "Oct15th_Mu",    0,           "Oct15th_Mu.root",    "GR10_P_V11::All", "2E31",      "3_8_3",  "/Mu/Run2010B-PromptReco-v2/RECO"},
  // runs 145762-148058: All 2010B set includes Oct02th_EG and Oct15th_EG
  {2,    7,  0,   0,   0,  12.026714683,     17130212,   -1,          "Oct28th_EG",    0,           "Oct28th_EG.root",    "GR10_P_V11::All", "2E31",      "3_8_3",  "/Electron/Run2010B-PromptReco-v2/RECO"},
  // runs 145762-148864:  Strong HEEP cuts, Level1GTSeed selection present, btag present
  {2,    7,  0,   0,   0,  18.815987479,     19368199,   -1,          "20pb_Muon",     0,            "20pb_Muon.root",    "GR10_P_V11::All", "6E31",      "3_8_3",  "/Mu/Run2010B-PromptReco-v2/RECO"},
  // runs 145762-148864:  Strong HEEP cuts, Level1GTSeed selection present, btag present
  {2,    7,  0,   0,   0,  18.833747410,     21044589,   -1,          "20pb_Electron", 0,        "20pb_Electron.root",    "GR10_P_V11::All", "6E31",      "3_8_3",  "/Electron/Run2010B-PromptReco-v2/RECO"},
   // runs 145762-148864: Weak HEEP cuts, changed ISO Level1GTSeed removed, btag present 
  {2,    7,  0,   0,   0,  18.833747410,     21044589,   -1, "18pb_ELECTRON_WEAKHEEP", 0,"18pb_ELECTRON_WEAKHEEP.root",   "GR10_P_V11::All", "6E31",      "3_8_3",  "/Electron/Run2010B-PromptReco-v2/RECO"}, 
   // runs 145762-148864: Weak HEEP cuts, changed ISO Level1GTSeed removed, btag present
  {2,    7,  0,   0,   0,  18.815987479,     19368199,   -1,          "18pb_MUON",     0,             "18pb_MUON.root",   "GR10_P_V11::All", "6E31",      "3_8_3",  "/Electron/Run2010B-PromptReco-v2/RECO"}, 
  // runs 145762-148864: digi physics tag removed 
  {2,    7,  0,   0,   0,  31.665421531,     26643831,   -1,          "35pb_ELECTRON", 0,         "35pb_ELECTRON.root",   "GR10_P_V11::All",  "2E32",     "3_8_3",  "/Electron/Run2010B-PromptReco-v2/RECO"}, 
  {2,    7,  0,   0,   0,  31.647661599,     26741407,   -1,          "32pb_MUON",     0,             "32pb_MUON.root",   "GR10_P_V11::All",  "2E32",     "3_8_3",  "/Mu/Run2010B-PromptReco-v2/RECO"}, 
  // runs 145762-148864: in this sample all  filters removed for test
  {2,    7,  0,   0,   0,  31.647661599,     26741407,   -1,"32pb_MUON_NOANYFILTER",   0, "32pb_MUON_NOANYFILTER.root",   "GR10_P_V11::All",  "2E32",     "3_8_3",  "/Mu/Run2010B-PromptReco-v2/RECO"}, 
   // runs 132440-144114: rereco 2010A
  {2,    7,  0,   0,   0,   3.167985107,     20252097,   -1,             "2010A_Mu",   0,              "2010A_Mu.root","FT_R_38X_V14A::All",  "2E32",     "3_8_6",  "/Mu/Run2010A-Nov4ReReco_v1/RECO"}, 
    // runs 132440-148864: 2010A_Mu.root and 32pb_MUON.root merged together
  {2,    7,  0,   0,   0, 34.815646706,     46993594,    -1,     "2010AB_35pb_Mu",     0,             "2010AB_35pb_Mu.root",             "",  "2E32",          "",  ""}, 
    // 2010B Nov4 3.8.6 ReReco: 146428-149294
  {2,    7,  0,   0,   0,  32.964468942,     28420454,   -1,         "2010B_Electron", 0,      "2010B_Electron.root",   "FT_R_38X_V14A::All", "2E32",     "3_8_7",  "/Electron/Run2010B-Nov4ReReco_v1/RECO"}, 
    // 2010A Nov4 3.8.6 ReReco: 136035-144114 
  {2,    7,  0,   0,   0,   3.180532050,     47471884,   -1,         "2010A_EG",       0,            "2010A_EG.root",   "FT_R_38X_V14A::All", "2E32",     "3_8_7",  "/EG/Run2010A-Nov4ReReco_v1/RECO"}, 
   // 2010A & 2010B Nov4 3.8.6 ReReco: 136035-149294
  {2,    7,  0,   0,   0,  36.145000992,     68340422,   -1, "2010A_EG_2010B_Electron_Nov4_ReReco", 0,      "2010A_EG_2010B_Electron_Nov4_ReReco.root",   "FT_R_38X_V14A::All", "2E32",     "3_8_7",  "/EG/Run2010A-Nov4ReReco_v1/RECO & /Electron/Run2010B-Nov4ReReco_v1/RECO"}, 
   // 2010A Sep17 3.8.3 ReReco: 136035-144114 
  {2,    7,  0,   0,   0,   3.180532050,     47471884,   -1, "2010A_EG_Sep17_ReReco",  0, "2010A_EG_Sep17_ReReco.root",  "GR_R_38X_V15::All", "2E32",     "3_8_7",  "/EG/Run2010A-Sep17ReReco_v2/RECO"}, 
   // 2010A Sep17 3.8.3 ReReco: 136035-144114
  {2,    7,  0,   0,   0,   3.180532050,     20868538,   -1, "2010A_Mu_Sep17_ReReco",  0, "2010A_Mu_Sep17_ReReco.root",  "GR_R_38X_V15::All", "2E32",     "3_8_7",  "/Mu/Run2010A-Sep17ReReco_v2/RECO"}, 
   // 2010A Nov4 3.8.3 ReReco: 136035-144114
  {2,    7,  0,   0,   0,   3.180532050,     20868538,   -1, "2010A_Mu_Nov4_ReReco",   0,  "2010A_Mu_Nov4_ReReco.root",  "GR_R_38X_V15::All", "2E32",     "3_8_7",  "/Mu/Run2010A-Nov4ReReco_v1/RECO"}, 
   // 2010B Nov4 3.8.3 ReReco: 146428-149294
  {2,    7,  0,   0,   0,  32.964468942,     28195693,   -1, "2010B_Mu_Nov4_ReReco",   0,  "2010B_Mu_Nov4_ReReco.root",  "FT_R_38X_V14A::All", "2E32",     "3_8_7",  "/Mu/Run2010B-Nov4ReReco_v1/RECO"}, 
   // 2010A & 2010B Nov4 3.8.6 ReReco: 136035-149294
  {2,    7,  0,   0,   0,  36.145000992,     49064231,   -1, "2010A_2010B_Mu_Nov4_ReReco", 0, "2010A_2010B_Mu_Nov4_ReReco.root","FT_R_38X_V14A::All", "2E32",     "3_8_7",  "/Mu/Run2010A-Nov4ReReco_v1/RECO & /Mu/Run2010B-Nov4ReReco_v1/RECO"}, 
   // 2010B Dec22 3.9.7 ReReco: 136035-149294
  {2,    7,  0,   0,   0,  32.964468942,     28420454,   -1,"Electron_Run2010B_Dec22ReReco_v1_CMSSW397", 0,"Electron_Run2010B_Dec22ReReco_v1_CMSSW397.root",   "GR_R_39X_V5::ALL", "2E32",     "3_9_7",  "/Electron/Run2010B-Dec22ReReco_v1/RECO"}, 
   // 2010A Dec22 3.9.7 ReReco: 136035-144114 
  {2,    7,  0,   0,   0,   3.180532050,     47471884,   -1,"Electron_Run2010A_Dec22ReReco_v1_CMSSW397", 0,"Electron_Run2010A_Dec22ReReco_v1_CMSSW397.root",   "GR_R_39X_V5::ALL", "2E32",   "3_9_7",  "/EG/Run2010A-Dec22ReReco_v1/RECO"}, 
  // 2010A & 2010B Nov4 Dec22 3.9.7 ReReco: 136035-149294
  {2,    7,  0,   0,   0,  36.145000992,     68340422,   -1, "Electron_Run2010A_Run2010B_Dec22ReReco", 0, "Electron_Run2010A_Run2010B_Dec22ReReco.root",   "GR_R_39X_V5::ALL", "2E32",     "3_9_7",  "/EG/Run2010A-Dec22ReReco_v1/RECO & /Electron/Run2010B-Dec22ReReco_v1/RECO"}, 
  // 2011A Apr1 4.1.4 PromptReco: 160431-161016
  {2,    7,  0,   0,   0,   5.073825757,       329649,   -1, "SingleElectron_Run2011A_PromptReco_v1_RECO", 0, "SingleElectron_Run2011A_PromptReco_v1_RECO.root",   "GR_R_311_V2::ALL", "2E32",     "4_1_4",  "/SingleElectron/Run2011A-PromptReco-v1/RECO"},
  // 2011A Apr1 4.1.4 PromptReco: 160431-161016
  {2,    7,  0,   0,   0,   5.073825757,      1766813,   -1, "SingleMu_Run2011A_PromptReco_v1_RECO", 0, "SingleMu_Run2011A_PromptReco_v1_RECO.root",   "GR_R_311_V2::ALL", "2E32",     "4_1_4",  "/SingleMu/Run2011A-PromptReco-v1/RECO"},
  // 2011A Apr1 4.1.4 PromptReco: 161103-161312
  {2,    7,  0,   0,   0,  12.700525360,       615962,   -1, "SingleElectron_Run2011A_PromptReco_v1_RECO_no_ES", 0, "SingleElectron_Run2011A_PromptReco_v1_RECO_no_ES.root",   "GR_R_311_V2::ALL", "2E32",     "4_1_4",  "/SingleElectron/Run2011A-PromptReco-v1/RECO"},
  // 2011A Apr1 4.1.4 PromptReco: 161103-161312
  {2,    7,  0,   0,   0,  12.700525360,      2373240,   -1, "SingleMu_Run2011A_PromptReco_v1_RECO_no_ES", 0, "SingleMu_Run2011A_PromptReco_v1_RECO_no_ES.root",   "GR_R_311_V2::ALL", "2E32",     "4_1_4",  "/SingleMu/Run2011A-PromptReco-v1/RECO"},
  // 2011A May6 4.1.4 PromptReco: 160431-161016  
  {2,    7,  0,   0,   0,   5.071585436,       329500,   -1, "ELECTRON_5pb", 0, "ELECTRON_5pb.root",   "GR_R_311_V2::ALL", "2E32",     "4_1_4",  "/SingleElectron/Run2011A-PromptReco-v1/RECO"}, 
  // 2011A May6 4.1.4 PromptReco: 162803-163757
  {2,    7,  0,   0,   0,   147.1158629,      5464421,   -1, "ELECTRON_148pb", 0, "ELECTRON_148pb.root",   "GR_R_311_V3::ALL", "2E32",     "4_1_3_patch3",  "/SingleElectron/Run2011A-PromptReco-v2/RECO"}, 
  // 2011A May6 4.1.4 PromptReco: 160431-161016
  {2,    7,  0,   0,   0,   5.071585436,      1766334,   -1, "MUON_5pb", 0, "MUON_5pb.root",   "GR_R_311_V2::ALL", "2E32",     "4_1_4",  "/SingleMu/Run2011A-PromptReco-v1/RECO"},
  // 2011A May6 4.1.4 PromptReco: 162803-163757
  {2,    7,  0,   0,   0, 146.154532133,     11000423,   -1, "MUON_148pb", 0, "MUON_148pb.root",   "GR_R_311_V3::ALL", "2E32",     "4_1_3_patch3",  "/SingleMu/Run2011A-PromptReco-v2/RECO"}, 
  // 2011A May6 4.1.4 PromptReco: 160431-161016
  {2,    7,  0,   0,   0,   5.071585436,       700872,   -1, "DoubleElectron_5pb", 0, "DoubleElectron_5pb.root",   "GR_R_311_V4::ALL", "2E32",     "4_1_3_patch3",  "/DoubleElectron/Run2011A-PromptReco-v1/RECO"}, 
  // 2011A May6 4.1.4 PromptReco: 162803-163757
  {2,    7,  0,   0,   0,   147.379870912,      7863715,   -1, "DoubleElectron_148pb", 0, "DoubleElectron_148pb.root",   "GR_R_311_V4::ALL", "2E32",     "4_1_3_patch3",  "/DoubleElectron/Run2011A-PromptReco-v2/RECO"},
 // 2011A May6 4.1.4 PromptReco: 163758-163869
  {2,    7,  0,   0,   0,   38.242147308,      1369585,   -1, "SingleElectronPromt", 0, "SingleElectronPromt.root",   "GR_R_311_V3::ALL", "2E32",     "4_1_3_patch3",  "/SingleElectron/Run2011A-PromptReco-v2/AOD"}, 
  // 2011A May6 4.1.4 PromptReco: 163758-163869
  {2,    7,  0,   0,   0,  185.358010208,      6834006,   -1, "ELECTRON_186pb", 0, "ELECTRON_186pb.root",   "GR_R_311_V3::ALL", "2E32",     "4_1_3_patch3",  "/SingleElectron/Run2011A-PromptReco-v2/AOD"}, 

 
  // 2011A May10Rereco 4_2_3: 160404-163869  /photon stream
  {2,    7,  0,   0,   0,   203.667940044,     13253481,   -1, "Photon10MayReReco",         0,         "Photon10MayReReco.root",   "FT_R_42_V13A::All", "5E32",     "4_2_3",  "/Photon/Run2011A-May10ReReco-v1/AOD"},
  
  // 2010A April21Rereco 4_2_3: 136033-149442  /photon stream
  {2,    7,  0,   0,   0,    32.152200232,     20557686,   -1, "Photon21AprReRecoB",         0,         "Photon21AprReRecoB.root",   "FT_R_42_V10A::All", "5E32",     "4_2_3",  "/Photon/Run2010B-Apr21ReReco-v1/AOD"},
  
  
  
  // Apr21ReReco of 2010 data, (threshoulds: min leptons = 1, min jets = 0)
  // incomplete: 171 from 187 jobs:
  {2,  7,  0,  0,  0,   2.897062441,  43200120,  -1,  "EG",  0,  "EG-Run2010A-Apr21ReReco-v1-0jets.root",  "FT_R_42_V10A:All",  "",  "4_2_1_patch1",  "/EG/Run2010A-Apr21ReReco-v1/AOD"},
  // incomplete: 189 from 190:
  {2,  7,  0,  0,  0,  32.096306187,  28021711,  -1,  "Electron",  0,  "Electron-Run2010B-Apr21ReReco-v1-0jets.root",  "FT_R_42_V10A:All",  "",  "4_2_1_patch1",  "/Electron/Run2010B-Apr21ReReco-v1/AOD"},
  // incomplete: 170 from 173:
  {2,  7,  0,  0,  0,   3.079589558,  20439987,  -1,  "Mu",  0,  "Mu-Run2010A-Apr21ReReco-v1-0jets.root",  "FT_R_42_V10A:All",  "",  "4_2_1_patch1",  "/Mu/Run2010A-Apr21ReReco-v1/AOD"},
  // fully complete:
  {2,  7,  0,  0,  0,  31.378549580,  27239810,  -1,  "Mu",  0,  "Mu-Run2010B-Apr21ReReco-v1-0jets.root",  "FT_R_42_V10A:All",  "",  "4_2_1_patch1",  "/Mu/Run2010B-Apr21ReReco-v1/AOD"},
  
  // 2011A May10Rereco 4_2_3: 160404-163869  only two leptons required, GlobalMuonPromptTight and 10% trackerIso reqired 
  {2,    7,  0,   0,   0,   204.214058024,      7818459,   -1, "SingleElectron10MayRereco_204pb", 0, "SingleElectron10MayRereco_204pb.root",   "FT_R_42_V13A::All", "5E32",     "4_2_3",  "/SingleElectron/Run2011A-May10ReReco-v1/AOD"}, 
  {2,    7,  0,   0,   0,   199.767134601,     16881807,   -1,       "SingleMu10MayRereco_204pb", 0,       "SingleMu10MayRereco_204pb.root",   "FT_R_42_V13A::All", "5E32",     "4_2_3",  "/SingleMu/Run2011A-May10ReReco-v1/AOD"}, 

  
   // Apr21ReReco of 2010 data: 160404-163869  only two leptons required, GlobalMuonPromptTight and 10% trackerIso reqired 
  {2,  7,  0,  0,  0,   3.180434374,  47232496,  -1,  "EG",  0,  "EG21AprilRereco_3pb.root",  "FT_R_42_V10A:All",  "",  "4_2_1_patch1",  "/EG/Run2010A-Apr21ReReco-v1/AOD"},
  {2,  7,  0,  0,  0,  32.315408282,  28224456,  -1,  "Electron",  0,  "Electron21AprilRereco_32pb.root",  "FT_R_42_V10A:All",  "",  "4_2_1_patch1",  "/Electron/Run2010B-Apr21ReReco-v1/AOD"},
  {2,  7,  0,  0,  0,  35.49583    ,  75456952,  -1,  "Electron",  0,  "Electron21AprilRereco_36pb.root",  "FT_R_42_V10A:All",  "",  "4_2_1_patch1",  "/Electron/Run2010B-Apr21ReReco-v1/AOD"},

  {2,  7,  0,  0,  0,   3.079589558,  20867715,  -1,  "Mu",  0,  "Mu21AprilRereco_3pb.root",  "FT_R_42_V10A:All",  "",  "4_2_1_patch1",  "/Mu/Run2010A-Apr21ReReco-v1/AOD"},
  {2,  7,  0,  0,  0,  31.378549580,  27239810,  -1,  "Mu",  0,  "Mu21AprilRereco_32pb.root",  "FT_R_42_V10A:All",  "",  "4_2_1_patch1",  "/Mu/Run2010B-Apr21ReReco-v1/AOD"},
  {2,  7,  0,  0,  0,  34.45814    ,  48107525,  -1,  "Mu",  0,  "Mu21AprilRereco_36pb.root",  "FT_R_42_V10A:All",  "",  "4_2_1_patch1",  "/Mu/Run2010B-Apr21ReReco-v1/AOD"},
 
 // 2011A Runs after MayTS 4_2_3: 165088-166861
  {2,    7,  0,   0,   0,   510.880084197,      12361305,   -1, "SingleElectron_500pb", 0, "SingleElectron_500pb.root",   "GR_R_42_V12::All", "1E33",     "4_2_3",  "/SingleElectron/Run2011A-May10ReReco-v4/AOD"}, 
  {2,    7,  0,   0,   0,   510.880084197,      21645300,   -1, "SingleMu_500pb", 0, "SingleMu_500pb.root",   "GR_R_42_V12::All", "1E33",     "4_2_3",  "/SingleMu/Run2011A-May10ReReco-v4/AOD"}, 

  // 2011A Runs after MayTS 4_2_3: 166864-167151
  {2,    7,  0,   0,   0,   158.408487114,     3481690,   -1, "SingleElectron_170pb", 0, "SingleElectron_170pb.root",   "GR_R_42_V12::All", "1E33",     "4_2_3",  "/SingleElectron/Run2011A-May10ReReco-v4/AOD"}, 
  {2,    7,  0,   0,   0,   158.408487114,     6420367,   -1, "SingleMu_170pb", 0, "SingleMu_170pb.root",   "GR_R_42_V12::All", "1E33",     "4_2_3",  "/SingleMu/Run2011A-May10ReReco-v4/AOD"}, 
  
  // 2011A Runs after MayTS 4_2_3: 167282-167784
  {2,    7,  0,   0,   0,    94.431739799,     1395262,   -1, "SingleElectron_110pb", 0, "SingleElectron_110pb.root",   "GR_R_42_V12::All", "1E33",     "4_2_3",  "/SingleElectron/Run2011A-May10ReReco-v4/AOD"}, 
//  {2,    7,  0,   0,   0,   158.408487114,     6420367,   -1, "SingleMu_170pb", 0, "SingleMu_170pb.root",   "GR_R_42_V12::All", "1E33",     "4_2_3",  "/SingleMu/Run2011A-May10ReReco-v4/AOD"}, 

  
    // 2011A All Runs before JulyTS 4_2_3: 160404-167784
  {2,    7,  0,   0,   0,   967.934369134,     25056716,   -1, "SingleElectron_968pb", 0, "SingleElectron_968pb.root",   "GR_R_42_V12::All", "1E33",     "4_2_3",  "/SingleElectron/Run2011A-May10ReReco-v4/AOD"}, 
//  {2,    7,  0,   0,   0,   510.880084197,     21645300,   -1, "SingleMu_170pb", 0, "SingleMu_170pb.root",  "GR_R_42_V12::All", "1E33",     "4_2_3",  "/SingleMu/Run2011A-May10ReReco-v4/AOD"}, 

  
  
   //this sample start new era in our trees with triggers,superclusters and other great improvements  
   // 2011A: 172620-173692 
  {2,    7,  0,   0,   0,   658.886,     6465649,   -1, "Electron_v6", 0, "Electron_v6.root",   "GR_R_42_V22A::All", "3E33",     "4_2_8_patch3",  "/SingleElectron/Run2011A-PromptReco-v6/AOD"}, 
   // 2011A: 170826-172619 
  {2,    7,  0,   0,   0,   336.590,     3621556,   -1, "Electron_v5", 0, "Electron_v5.root",   "GR_R_42_V22A::All", "2E33",     "4_2_8_patch3",  "/SingleElectron/Run2011A-PromptReco-v5/AOD"}, 
   // 2011A: 165088-167913 
  {2,    7,  0,   0,   0,   929.748,    19146697,   -1, "Electron_v4", 0, "Electron_v4.root",   "GR_R_42_V22A::All", "2E33",     "4_2_8_patch3",  "/SingleElectron/Run2011A-PromptReco-v4/AOD"}, 
   // 2011A: 160431-1632869 
  {2,    7,  0,   0,   0,   202.166,     7186827,   -1, "Electron_v2", 0, "Electron_v2.root",   "GR_R_42_V22A::All", "1E33",     "4_2_8_patch3",  "/SingleElectron/Run2011A-May10ReReco-v1/AOD"}, 

   // SingleElectron datasets
   // 2011A: 160431-173692 
  {2,    7,  0,   0,   0,      2272,    37393704,   -1, "Electron_2011A", 0, "Electron_2011A.root",   "GR_R_42_V22A::All", "",     "4_2_8_patch3",  ""}, 
   // 2011B: 175860-177878 
  {2,    7,  0,   0,   0,      2719,    15809330,   -1, "Electron_B_v1", 0, "Electron_B_v1.root",     "GR_R_42_V22A::All", "3E33",     "4_2_8_patch3",  "/SingleElectron/Run2011B-PromptReco-v1/AOD"}, 

   // SingleElectron datasets with only 1 gsf electron
   // 2011A: 160431-173692 
  {2,    7,  0,   0,   0,      2272,    37393704,   -1, "Electron_2011A_e1", 0, "Electron_2011A_e1.root",   "GR_R_42_V22A::All", "",     "4_2_8_patch3",  ""}, 
   // 2011B: 175860-177878 
  {2,    7,  0,   0,   0,      2719,    15809330,   -1, "Electron_B_v1_e1", 0, "Electron_B_v1_e1.root",     "GR_R_42_V22A::All", "3E33",     "4_2_8_patch3",  "/SingleElectron/Run2011B-PromptReco-v1/AOD"}, 

   // DoubleElectron datasets
   // 2011A: 160431-173692
  {2,    7,  0,   0,   0,      2272,    43584738,   -1, "DElectron_2011A", 0, "DElectron_2011A.root",   "GR_R_42_V22A::All", "",     "4_2_8_patch3",  ""},
   // 2011B: 175860-180252
  {2,    7,  0,   0,   0,      2719,    14450296,   -1, "DElectron_B_v1", 0, "DElectron_B_v1.root",    "GR_R_42_V22A::All", "3E33",     "4_2_8_patch3","/DoubleElectron/Run2011B-PromptReco-v1/AOD"},

   // Photon datasets with only 1 Jet
   // 2011A: 160431-173692
  {2,    7,  0,   0,   0,      2272,    63269114,   -1, "SFoton_2011A", 0, "SFoton_2011A.root",   "GR_R_42_V22A::All", "",     "4_2_8_patch3",  ""},
   // 2011B: 175860-180252
  {2,    7,  0,   0,   0,      2719,    26241125,   -1, "SFoton_B_v1", 0, "SFoton_B_v1.root",    "GR_R_42_V22A::All", "3E33",     "4_2_8_patch3",  "/Photon/Run2011B-PromptReco-v1/RECO"},

   // Photon datasets with 2 leptons for trigger study
   // 2011A: 160431-173692
  {2,    7,  0,   0,   0,      2272,    63235905,   -1, "DFoton_2011A", 0, "DFoton_2011A.root",   "GR_R_42_V22A::All", "",     "4_2_8_patch3",  ""},
   // 2011B: 175860-180252
  {2,    7,  0,   0,   0,      2719,    26341933,   -1, "DFoton_B_v1", 0, "DFoton_B_v1.root",    "GR_R_42_V22A::All", "3E33",     "4_2_8_patch3",  "/Photon/Run2011B-PromptReco-v1/RECO"},

  
   // Jet datasets
   // 2011A: 160431-173692
  {2,    7,  0,   0,   0,      2272,    25937885,   -1, "SJet_2011A", 0, "SJet_2011A.root",   "GR_R_42_V22A::All", "",     "4_2_8_patch3",  ""},
   // 2011B: 175860-180252
  {2,    7,  0,   0,   0,      2719,    11913253,   -1, "SJet_B_v1", 0, "SJet_B_v1.root",    "GR_R_42_V22A::All", "3E33",     "4_2_8_patch3",  "/Photon/Run2011B-PromptReco-v1/RECO"},

   // Muon datasets 2 leptons required
   // 2011A: 160431-173692 
  {2,    7,  0,   0,   0,      2272,    77316244,   -1, "Muon_2011A", 0, "Muon_2011A.root",   "GR_R_42_V22A::All", "",     "4_2_8_patch3",  "/Mu/Run2011A-PromptReco-v*/AOD & /Mu/Run2011A-May10ReReco-v1/AOD"}, 
   // 2011B: 175860-177878 
  {2,    7,  0,   0,   0,      2719,    46648380,   -1, "Muon_B_v1", 0, "Muon_B_v1.root",     "GR_R_42_V22A::All", "3E33",     "4_2_8_patch3",  "/Mu/Run2011B-PromptReco-v1/AOD"}, 


// Data 2012, /moscow41/heavynu/Data_2012 ------------------------------------------------

  {2,    8,  0,   0,   0,   808.472,     8572211,   -1, "DoublEle_2012A_13Jul", 0, "DoublEle_2012A_13Jul.root",   "GR_R_53_V21::All", "",  "5_3_X",  ""},

  {2,    8,  0,   0,   0,    82.136,      975890,   -1, "DoublEle_2012A_06Aug", 0, "DoublEle_2012A_06Aug.root",   "GR_R_53_V21::All", "",  "5_3_X",  ""},

  {2,    8,  0,   0,   0,    4413.,     21399025,   -1, "DoublEle_2012B_13Jun", 0, "DoublEle_2012B_13Jun_minus1.root",   "GR_R_53_V21::All", "",  "5_3_X",  ""},

  {2,    8,  0,   0,   0,  7028.245,    31990061,   -1, "DoublEle_2012C", 0, "DoublEle_2012C.root",   "GR_R_53_V21::All", "",  "5_3_X",  ""},

  {2,    8,  0,   0,   0,  7244.,       32183449,   -1, "DoublEle_2012D_promt", 0, "DoublEle_2012D_promt.root",   "GR_R_53_V21::All", "",  "5_3_X",  ""},


// Data 2012 rereco, /moscow41/heavynu/53x-DATA_2012, about 0.002 of run D is missing in the file v2 (lumi 7369 below is for the full run D) ---

  {2,    8,  0,   0,   0,  876.225,      9522277,   -1, "DoublEle_2012A_22Jan2013_v1", 0,  "DoubleElectron_Run2012A-22Jan2013-v1.root",      "FT_53_V21_AN6::All", "",  "5_3_X",  ""},

  {2,    8,  0,   0,   0,    4405.,     21440863,   -1, "DoublEle_2012B_22Jan2013-v1", 0, "DoubleElectron_Run2012B-22Jan2013-v1.root",      "FT_53_V21_AN6::All", "",  "5_3_X",  ""},

  {2,    8,  0,   0,   0,    7028.,     32354828,   -1, "DoublEle_2012C_22Jan2013-v1", 0, "DoubleElectron_Run2012C-22Jan2013-v1.root",      "FT_53_V21_AN6::All", "",  "5_3_X",  ""},

  {2,    8,  0,   0,   0,    7369.,     32568995,   -1, "DoublEle_2012D_22Jan2013-v2", 0, "DoubleElectron_Run2012D-22Jan2013-v2.root",      "FT_53_V21_AN6::All", "",  "5_3_X",  ""},


// Run II Data

// Data 2015 prompt reco

  {2,   13,  0,   0,   0,      2687.,    3150423,   -1, "DoubleEG_Run2015D-PromptReco_v4", 0, "DoubleEG_Run2015D-PromptReco_v4.root", "GR_R_74_V15::All", "",  "7_4_X",  ""},

  {2,   13,  0,   0,   0,      2687.,    3150423,   -1, "MuonEG_Run2015D_PromptReco-full", 0, "MuonEG_Run2015D_PromptReco-full_data.root", "GR_R_74_V15::All", "",  "7_4_X",  ""},


  // background samples
// === CSA07 ==========================================================================================================================================================================
  // Chowder, Gumbo and Stew datasets are mix of several processes
  // real process id stored in .root files
  // https://twiki.cern.ch/twiki/bin/view/CMS/CSA07ProcessId#6_List_of_CSA07_Process_Id_numbe
  // process weight in these datasets correspond to 100 pb^-1
// type  ECM MW  MNu  channel  lumi   stat     CS     name        pid    fname                             AlCa            HLTmenu    CMSSW       DBS
  {0,    14,  0,  0,   0,       0,      -1,    -1,   "Chowder",    -1,  "new-bg-Chowder-PDElectron.root",  "CSA07_100pb",   "1E32",  "1_6_12",   "/CSA07Electron/CMSSW_1_6_7-CSA07-Chowder-A2-PDElectron-ReReco-100pb/AODSIM"},
  {0,    14,  0,  0,   0,       0,      -1,    -1,   "Chowder",    -1,  "new-bg-Chowder-PDMuon.root",      "CSA07_100pb",   "1E32",  "1_6_12",   "/CSA07Muon/CMSSW_1_6_7-CSA07-Chowder-A2-PDMuon-ReReco-100pb/AODSIM"},
  {0,    14,  0,  0,   0,       0,      -1,    -1,   "Gumbo",      -1,  "new-bg-Gumbo-PDElectron.root",    "CSA07_100pb",   "1E32",  "1_6_12",   "/CSA07Electron/CMSSW_1_6_7-CSA07-Gumbo-B1-PDElectron-ReReco-100pb/AODSIM"},
  {0,    14,  0,  0,   0,       0,      -1,    -1,   "Gumbo",      -1,  "new-bg-Gumbo-PDMuon.root",        "CSA07_100pb",   "1E32",  "1_6_12",   "/CSA07Muon/CMSSW_1_6_7-CSA07-Gumbo-B1-PDMuon-ReReco-100pb/AODSIM"},
  {0,    14,  0,  0,   0,       0,      -1,    -1,   "Stew",       -1,  "new-bg-Stew-PDElectron.root",     "CSA07_100pb",   "1E32",  "1_6_12",   "/CSA07Electron/CMSSW_1_6_7-CSA07-Stew-B2-PDElectronReReco-100pb/AODSIM"},
  {0,    14,  0,  0,   0,       0,      -1,    -1,   "Stew",       -1,  "new-bg-Stew-PDMuon.root",         "CSA07_100pb",   "1E32",  "1_6_12",   "/CSA07Muon/CMSSW_1_6_7-CSA07-Stew-B2-PDMuonReReco-100pb/AODSIM"},
  {0,    14,  0,  0,   0,       0,  744261,   114.3, "WW",       1005,  "new-bg-WW_incl.root",             "CSA07_100pb",   "1E32",  "1_6_12",   "/WW_incl/CMSSW_1_6_7-CSA07-1198096665/AODSIM"},
  {0,    14,  0,  0,   0,       0,  362291,   49.9,  "WZ",       1010,  "new-bg-WZ_incl.root",             "CSA07_100pb",   "1E32",  "1_6_12",   "/WZ_incl/CMSSW_1_6_7-CSA07-1198096684/AODSIM"},
  {0,    14,  0,  0,   0,       0,  143113,   16.1,  "ZZ",       1007,  "new-bg-ZZ_incl.root",             "CSA07_100pb",   "1E32",  "1_6_12",   "/ZZ_incl/CMSSW_1_6_7-CSA07-1198096655/AODSIM"},
  
// === CSA08 ==========================================================================================================================================================================
  {0,    10,  0,  0,   0,       0,  877682,   317  , "ttbar",    1000,  "TTJets-madgraph.root",        "IDEAL_V11",   "",      "",    "/TTJets-madgraph/Fall08_IDEAL_V11_redigi_v10/AODSIM"},
  {0,    10,  0,  0,   0,       0,  103253, 241.7  , "ttbar",    1001,  "TauolaTTbar.root",            "IDEAL_V11",   "",      "",    "/TauolaTTbar/Summer08_IDEAL_V11_redigi_v2/AODSIM"},
  {0,    10,  0,  0,   0,       0,  207387,   352  , "ttbar",    1002,  "MCatNLOTTbar.root",           "IDEAL_V11",   "",      "",    "/MCatNLOTTbar/Summer08_IDEAL_V11_redigi_v1/AODSIM"},
  {0,    10,  0,  0,   0,       0, 1216731,   3.7e3, "Z+jets",   1003,  "ZJets-madgraph.root",         "IDEAL_V11",   "",      "",    "/ZJets-madgraph/Summer08_IDEAL_V11_redigi_v1/AODSIM"},
  {0,    10,  0,  0,   0,       0, 9074588,    40e3, "W+jets",   1004,  "WJets-madgraph.root",         "IDEAL_V11",   "",      "",    "/WJets-madgraph/Summer08_IDEAL_V11_redigi_v1/AODSIM"},
  {0,    10,  0,  0,   0,       0,  204722,  44.8  , "WW",       1005,  "WW.root",                     "IDEAL_V11",   "",      "",    "/WW/Summer08_IDEAL_V11_redigi_v1/AODSIM"},
  {0,    10,  0,  0,   0,       0,  112100,     0  , "WW",       1006,  "WW_2l.root",                  "IDEAL_V11",   "",      "",    "/WW_2l/Summer08_IDEAL_V11_redigi_v1/AODSIM"},
  {0,    10,  0,  0,   0,       0,  199810,   7.1  , "ZZ",       1007,  "ZZ.root",                     "IDEAL_V11",   "",      "",    "/ZZ/Summer08_IDEAL_V11_redigi_v1/AODSIM"},
  {0,    10,  0,  0,   0,       0,  115200,     0  , "ZZ",       1008,  "ZZ_2l2n.root",                "IDEAL_V11",   "",      "",    "/ZZ_2l2n/Summer08_IDEAL_V11_redigi_v1/AODSIM"},
  {0,    10,  0,  0,   0,       0,  258350,     0  , "ZZ",       1009,  "ZZ_4l.root",                  "IDEAL_V11",   "",      "",    "/ZZ_4l/Summer08_IDEAL_V11_redigi_v1/AODSIM"},
  {0,    10,  0,  0,   0,       0,  248800,     0  , "WZ",       1010,  "WZ_incl.root",                "IDEAL_V11",   "",      "",    "/WZ_incl/Summer08_IDEAL_V11_redigi_v1/AODSIM"},
  {0,    10,  0,  0,   0,       0, 11635956,   15e6, "QCD",      1011,  "QCD100to250-madgraph.root",   "IDEAL_V11",   "",      "",    "/QCD100to250-madgraph/Fall08_IDEAL_V11-redigi_v1/AODSIM"},
  {0,    10,  0,  0,   0,       0, 4834539,   400e3, "QCD",      1012,  "QCD250to500-madgraph.root",   "IDEAL_V11",   "",      "",    "/QCD250to500-madgraph/Fall08_IDEAL_V11-redigi_v1/AODSIM"},
  {0,    10,  0,  0,   0,       0, 4319134,    14e3, "QCD",      1013,  "QCD500to1000-madgraph.root",  "IDEAL_V11",   "",      "",    "/QCD500to1000-madgraph/Fall08_IDEAL_V11-redigi_v1/AODSIM"},
  {0,    10,  0,  0,   0,       0, 1036863,   370  , "QCD",      1014,  "QCD1000toInf-madgraph.root",  "IDEAL_V11",   "",      "",    "/QCD1000toInf-madgraph/Fall08_IDEAL_V11-redigi_v1/AODSIM"},

// === CSA09/October X ==============================================================================================================================================================================================
// pid - is a CSA09 Sample ID
// if cross section field is given in form "X * CS",
//   this mean prescale was used to produce SD or skim for given sample
// --- 10 TeV samples ---
// type  ECM MW  MNu  channel  lumi   stat            CS    name        pid        fname                                    AlCa       HLTmenu  CMSSW       DBS
  {0,    10,  0,  0,   0,       0, 33638282,        3.2e6, "QCD",      1011010,  "QCD_EMEnriched_Pt20to30-SD_Ele15.root",  "MC_31X_V3", "8E29", "", "/QCD_EMEnriched_Pt20to30/Summer09-MC_31X_V3_SD_Ele15-v1/GEN-SIM-RECO"},
  {0,    10,  0,  0,   0,       0, 38360886,       4.70e6, "QCD",      1011020,  "QCD_EMEnriched_Pt30to80-SD_Ele15.root",  "MC_31X_V3", "8E29", "", "/QCD_EMEnriched_Pt30to80/Summer09-MC_31X_V3_SD_Ele15-v1/GEN-SIM-RECO"},
  {0,    10,  0,  0,   0,       0,  5729547,        285e3, "QCD",      1011030,  "QCD_EMEnriched_Pt80to170-SD_Ele15.root", "MC_31X_V3", "8E29", "", "/QCD_EMEnriched_Pt80to170/Summer09-MC_31X_V3_SD_Ele15-v1/GEN-SIM-RECO"},
  {0,    10,  0,  0,   0,       0,  2383833,       1.92e5, "QCD",      1012010,  "QCD_BCtoE_Pt20to30-SD_Ele15.root",       "MC_31X_V3", "8E29", "", "/QCD_BCtoE_Pt20to30/Summer09-MC_31X_V3_SD_Ele15_QCD-v1/GEN-SIM-RECO"},
  {0,    10,  0,  0,   0,       0,  2035108,       2.40e5, "QCD",      1012020,  "QCD_BCtoE_Pt30to80-SD_Ele15.root",       "MC_31X_V3", "8E29", "", "/QCD_BCtoE_Pt30to80/Summer09-MC_31X_V3_SD_Ele15-v1/GEN-SIM-RECO"},
  {0,    10,  0,  0,   0,       0,  1038080,       2.28e4, "QCD",      1012030,  "QCD_BCtoE_Pt80to170-SD_Ele15.root",      "MC_31X_V3", "8E29", "", "/QCD_BCtoE_Pt80to170/Summer09-MC_31X_V3_SD_Ele15-v1/GEN-SIM-RECO"},
  
  {0,    10,  0,  0,   0,       0,  5072702,       1.46e9, "QCD",      1002010,  "QCD_Pt15-SD_Mu9.root",                   "MC_31X_V3", "8E29", "", "/QCD_Pt15/Summer09-MC_31X_V3_SD_Mu9-v1/GEN-SIM-RECO"},
  {0,    10,  0,  0,   0,       0,  1957216,        109e6, "QCD",      1002020,  "QCD_Pt30-SD_Mu9.root",                   "MC_31X_V3", "8E29", "", "/QCD_Pt30/Summer09-MC_31X_V3_SD_Mu9-v1/GEN-SIM-RECO"},
  {0,    10,  0,  0,   0,       0,  5204815,       3.64e5, "QCD",      1023010,  "InclusiveMu5_Pt50-SD_Mu9.root",          "MC_31X_V3", "8E29", "", "/InclusiveMu5_Pt50/Summer09-MC_31X_V3_SD_Mu9-v1/GEN-SIM-RECO"},
  {0,    10,  0,  0,   0,       0,  6570971,   4 * 1.47e5, "QCD",      1021000,  "InclusiveMu15-SD_Mu9.root",              "MC_31X_V3", "8E29", "", "/InclusiveMu15/Summer09-MC_31X_V3_SD_Mu9-v1/GEN-SIM-RECO"},
  
  // ttbar cross section is NLO as calculated with madgraph
  {0,    10,  0,  0,   0,       0,   529750,        414  , "ttbar",    1040000,  "TTbar.root",                             "MC_31X_V3", "8E29", "", "/TTbar/Summer09-MC_31X_V3-v1/GEN-SIM-RECO"},
  {0,    10,  0,  0,   0,       0,   529750, 100 *  414  , "ttbar",    1040000,  "TTbar-SD_Ele15.root",                    "MC_31X_V3", "8E29", "", "/TTbar/Summer09-MC_31X_V3_SD_Ele15-v1/GEN-SIM-RECO"},
  {0,    10,  0,  0,   0,       0,   529750, 100 *  414  , "ttbar",    1040000,  "TTbar-SD_Mu9.root",                      "MC_31X_V3", "8E29", "", "/TTbar/Summer09-MC_31X_V3_SD_Mu9-v1/GEN-SIM-RECO"},

  {0,    10,  0,  0,   0,       0,  2142960,       8.74e3, "Wenu",     1054000,  "Wenu.root",                              "MC_31X_V3", "8E29", "", "/Wenu/Summer09-MC_31X_V3-v1/GEN-SIM-RECO"},
  {0,    10,  0,  0,   0,       0,  2142960,  20 * 8.74e3, "Wenu",     1054000,  "Wenu-SD_Ele15.root",                     "MC_31X_V3", "8E29", "", "/Wenu/Summer09-MC_31X_V3_SD_Ele15-v1/GEN-SIM-RECO"},
  {0,    10,  0,  0,   0,       0,  2082633,       8.18e3, "Wmunu",    1053000,  "Wmunu.root",                             "MC_31X_V3", "8E29", "", "/Wmunu/Summer09-MC_31X_V3-v1/GEN-SIM-RECO"},
  {0,    10,  0,  0,   0,       0,  2082633,  20 * 8.18e3, "Wmunu",    1053000,  "Wmunu-SD_Mu9.root",                      "MC_31X_V3", "8E29", "", "/Wmunu/Summer09-MC_31X_V3_SD_Mu9-v2/GEN-SIM-RECO"},
  
  {0,    10,  0,  0,   0,       0,  2682355,       1.94e3, "Zee",      1071010,  "Zee.root",                               "MC_31X_V3", "8E29", "", "/Zee/Summer09-MC_31X_V3-v1/GEN-SIM-RECO"},
  {0,    10,  0,  0,   0,       0,  2682355, 100 * 1.94e3, "Zee",      1071010,  "Zee-SD_Ele15.root",                      "MC_31X_V3", "8E29", "", "/Zee/Summer09-MC_31X_V3_SD_Ele15-v1/GEN-SIM-RECO"},
  {0,    10,  0,  0,   0,       0,  2000500,       1.94e3, "Zmumu",    1071020,  "Zmumu.root",                             "MC_31X_V3", "8E29", "", "/Zmumu/Summer09-MC_31X_V3-v1/GEN-SIM-RECO"},
  {0,    10,  0,  0,   0,       0,  2000500, 100 * 1.94e3, "Zmumu",    1071020,  "Zmumu-SD_Mu9.root",                      "MC_31X_V3", "8E29", "", "/Zmumu/Summer09-MC_31X_V3_SD_Mu9-v1/GEN-SIM-RECO"},

  {0,    10,  0,  0,   0,       0,  8600326,       44.4  , "WW",       1072000,  "WW.root",                                "MC_31X_V3",   "8E29",      "",    "/WW/Summer09-MC_31X_V3-v1/GEN-SIM-RECO"},
  {0,    10,  0,  0,   0,       0,  3063080,        7.3  , "ZZ",       1073000,  "ZZ.root",                                "MC_31X_V3",   "8E29",      "",    "/ZZ/Summer09-MC_31X_V3-v1/GEN-SIM-RECO"},
  {0,    10,  0,  0,   0,       0,  5107385,       17.6  , "WZ",       1076000,  "WZ.root",                                "MC_31X_V3",   "8E29",      "",    "/WZ/Summer09-MC_31X_V3-v1/GEN-SIM-RECO"},
 
// --- 7 TeV samples ---
// type  ECM MW  MNu  channel  lumi   stat            CS      name        pid        fname                                    AlCa       HLTmenu  CMSSW       DBS
  {0,    7,  0,  0,   0,       0,  33505929,       1.72E+06, "QCD",    1011010,  "7TeV-QCD_EMEnriched_Pt20to30-SD_Ele15.root",  "MC_31X_V3", "8E29", "", "/QCD_EMEnriched_Pt20to30/Summer09-MC_31X_V3_7TeV_SD_Ele15-v1/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,  32168675,       3.50E+06, "QCD",    1011020,  "7TeV-QCD_EMEnriched_Pt30to80-SD_Ele15.root",  "MC_31X_V3", "8E29", "", "/QCD_EMEnriched_Pt30to80/Summer09-MC_31X_V3_7TeV_SD_Ele15-v1/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,   5551386,       1.34E+05, "QCD",    1011030,  "7TeV-QCD_EMEnriched_Pt80to170-SD_Ele15.root", "MC_31X_V3", "8E29", "", "/QCD_EMEnriched_Pt80to170/Summer09-MC_31X_V3_7TeV_SD_Ele15-v2/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,   2752942,       1.08E+05, "QCD",    1012010,  "7TeV-QCD_BCtoE_Pt20to30-SD_Ele15.root",       "MC_31X_V3", "8E29", "", "/QCD_BCtoE_Pt20to30/Summer09-MC_31X_V3_7TeV_SD_Ele15-v1/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,   2261916,       1.42E+05, "QCD",    1012020,  "7TeV-QCD_BCtoE_Pt30to80-SD_Ele15.root",       "MC_31X_V3", "8E29", "", "/QCD_BCtoE_Pt30to80/Summer09-MC_31X_V3_7TeV_SD_Ele15-v1/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,   1097829,       9.42E+03, "QCD",    1012030,  "7TeV-QCD_BCtoE_Pt80to170-SD_Ele15.root",      "MC_31X_V3", "8E29", "", "/QCD_BCtoE_Pt80to170/Summer09-MC_31X_V3_7TeV_SD_Ele15-v1/GEN-SIM-RECO"},
  
  {0,    7,  0,  0,   0,       0,   6256300,       8.76E+08, "QCD",    1002010,  "7TeV-QCD_Pt15-SD_Mu9.root",                   "MC_31X_V3", "8E29", "3_1_3", "/QCD_Pt15/Summer09-MC_31X_V3_7TeV_SD_Mu9-v1/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,   5252384,       6.04E+07, "QCD",    1002020,  "7TeV-QCD_Pt30-SD_Mu9.root",                   "MC_31X_V3", "8E29", "", "/QCD_Pt30/Summer09-MC_31X_V3_7TeV_SD_Mu9-v1/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,   5031468,       1.79E+05, "QCD",    1023010,  "7TeV-InclusiveMu5_Pt50-SD_Mu9.root",          "MC_31X_V3", "8E29", "3_1_3", "/InclusiveMu5_Pt50/Summer09-MC_31X_V3_7TeV_SD_Mu9-v1/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,   4507485,   3 * 1.10E+05, "QCD",    1021000,  "7TeV-InclusiveMu15-SD_Mu9.root",              "MC_31X_V3", "8E29", "", "/InclusiveMu15/Summer09-MC_31X_V3_7TeV_SD_Mu9-v1/GEN-SIM-RECO"},
 
  // ttbar is simulated with tauola and pythia, LO cs = 94.3
  // ttbar cs is NLO taken from: http://alcaraz.web.cern.ch/alcaraz/CROSS_SECTIONS.txt
  // Zee and Zmumu cs are NLO Mll>20 taken from: http://alcaraz.web.cern.ch/alcaraz/CROSS_SECTIONS.txt, LO cs is 1300
  // Wenu and Wmunu cs are NLO taken from: http://alcaraz.web.cern.ch/alcaraz/CROSS_SECTIONS.txt, LO cs is ~6000
  {0,    7,  0,  0,   0,       0,    626610,       1.62E+02, "ttbar",  1040000,  "7TeV-TTbar.root",                             "MC_31X_V3", "8E29", "3_1_2", "/TTbar/Summer09-MC_31X_V3_7TeV-v1/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,    626610, 100 * 1.62E+02, "ttbar",  1040000,  "7TeV-TTbar-SD_Ele15.root",                    "MC_31X_V3", "8E29", "", "/TTbar/Summer09-MC_31X_V3_7TeV_SD_Ele15-v1/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,    626610, 100 * 1.62E+02, "ttbar",  1040000,  "7TeV-TTbar-SD_Mu9.root",                      "MC_31X_V3", "8E29", "", "/TTbar/Summer09-MC_31X_V3_7TeV_SD_Mu9-v1/GEN-SIM-RECO"},
  
  {0,    7,  0,  0,   0,       0,   2078361,       1.03E+04, "Wenu",   1054000,  "7TeV-Wenu.root",                              "MC_31X_V3", "8E29", "3_1_2", "/Wenu/Summer09-MC_31X_V3_7TeV-v1/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,   2078361,  20 * 1.03E+04, "Wenu",   1054000,  "7TeV-Wenu-SD_Ele15.root",                     "MC_31X_V3", "8E29", "", "/Wenu/Summer09-MC_31X_V3_7TeV_SD_Ele15-v1/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,   2073721,       1.03E+04, "Wmunu",  1053000,  "7TeV-Wmunu.root",                             "MC_31X_V3", "8E29", "", "/Wmunu/Summer09-MC_31X_V3_7TeV-v1/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,   2073721,  20 * 1.03E+04, "Wmunu",  1053000,  "7TeV-Wmunu-SD_Mu9.root",                      "MC_31X_V3", "8E29", "", "/Wmunu/Summer09-MC_31X_V3_7TeV_SD_Mu9-v1/GEN-SIM-RECO"},
  
  {0,    7,  0,  0,   0,       0,   2538855,       1.67E+03, "Zee",    1071010,  "7TeV-Zee.root",                               "MC_31X_V3", "8E29", "", "/Zee/Summer09-MC_31X_V3_7TeV_TrackingParticles-v1/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,   2538855, 100 * 1.67E+03, "Zee",    1071010,  "7TeV-Zee-SD_Ele15.root",                      "MC_31X_V3", "8E29", "", "/Zee/Summer09-MC_31X_V3_7TeV_SD_Ele15-v1/GEN-SIM-RECO"},
  // part (30000 events) of 7TeV-Zmumu dataset are not read due to corrupted .root file
  // for details, see 7TeV-Zmumu/crab_0_091213_003103/res/CMSSW_58.stderr
  {0,    7,  0,  0,   0,       0,   2310156,       1.67E+03, "Zmumu",  1071020,  "7TeV-Zmumu.root",                             "MC_31X_V3", "8E29", "", "/Zmumu/Summer09-MC_31X_V3_7TeV-v1/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,   2310156, 100 * 1.67E+03, "Zmumu",  1071020,  "7TeV-Zmumu-SD_Mu9.root",                      "MC_31X_V3", "8E29", "", "/Zmumu/Summer09-MC_31X_V3_7TeV_SD_Mu9-v1/GEN-SIM-RECO"},
  
  // WW, WZ, ZZ cs are NLO taken from: https://twiki.cern.ch/twiki/bin/viewauth/CMS/CrossSections_3XSeries, LO cs are 28, 10.5, 4.3
  {0,    7,  0,  0,   0,       0,    120280,            43.,    "WW",  1072000,  "7TeV-WW.root",                                "MC_31X_V3", "8E29", "", "/WW/Summer09-MC_31X_V3_7TeV-v1/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,    114070,            18.,    "WZ",  1076000,  "7TeV-WZ.root",                                "MC_31X_V3", "8E29", "", "/WZ/Summer09-MC_31X_V3_7TeV-v1/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,    145368,            5.9,    "ZZ",  1073000,  "7TeV-ZZ.root",                                "MC_31X_V3", "8E29", "", "/ZZ/Summer09-MC_31X_V3_7TeV-v1/GEN-SIM-RECO"},

// === CSA10 ======================================================================================================================================================================

  // In fact trigger table name in the following CSA10 samples is "GRun".
  // This trigger table consists of the 8E29 physics triggers plus the online
  // commissioning triggers migrated from CMSSW_22X_HLT.
// type  ECM MW  MNu  channel  lumi   stat           CS      name        pid        fname                                         AlCa        HLTmenu  CMSSW       DBS
  {0,    7,  0,  0,   0,       0,    613650,   2.264e+08,    "QCD",    2004030,   "QCD_Pt-20_30_7TeV-pythia8.root",             "START36_V10", "8E29", "3_6_2", "/QCD_Pt-20to30_7TeV-pythia8/Summer10-START36_V10_S09-v1/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,    651444,   5.018e+07,    "QCD",    2004040,   "QCD_Pt-30_50_7TeV-pythia8.root",             "START36_V10", "8E29", "3_6_2", "/QCD_Pt-30to50_7TeV-pythia8/Summer10-START36_V10_S09-v2/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,    517758,   6.035e+06,    "QCD",    2004050,   "QCD_Pt-50_80_7TeV-pythia8.root",             "START36_V10", "8E29", "3_6_2", "/QCD_Pt-50to80_7TeV-pythia8/Summer10-START36_V10_S09-v2/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,    635905,   7.519e+05,    "QCD",    2004060,   "QCD_Pt-80_120_7TeV-pythia8.root",            "START36_V10", "8E29", "3_6_2", "/QCD_Pt-80to120_7TeV-pythia8/Summer10-START36_V10_S09-v2/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,    540280,   1.120e+05,    "QCD",    2004070,   "QCD_Pt-120_170_7TeV-pythia8.root",           "START36_V10", "8E29", "3_6_2", "/QCD_Pt-120to170_7TeV-pythia8/Summer10-START36_V10_S09-v2/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,    499306,   1.994e+04,    "QCD",    2004080,   "QCD_Pt-170_230_7TeV-pythia8.root",           "START36_V10", "8E29", "3_6_2", "/QCD_Pt-170to230_7TeV-pythia8/Summer10-START36_V10_S09-v2/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,    499912,   4.123e+03,    "QCD",    2004090,   "QCD_Pt-230_300_7TeV-pythia8.root",           "START36_V10", "8E29", "3_6_2", "/QCD_Pt-230to300_7TeV-pythia8/Summer10-START36_V10_S09-v2/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,    263570,   9.593e+02,    "QCD",    2004100,   "QCD_Pt-300_380_7TeV-pythia8.root",           "START36_V10", "8E29", "3_6_2", "/QCD_Pt-300to380_7TeV-pythia8/Summer10-START36_V10_S09-v2/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,    317924,   2.434e+02,    "QCD",    2004110,   "QCD_Pt-380_470_7TeV-pythia8.root",           "START36_V10", "8E29", "3_6_2", "/QCD_Pt-380to470_7TeV-pythia8/Summer10-START36_V10_S09-v2/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,    252885,   7.410e+01,    "QCD",    2004120,   "QCD_Pt-470_600_7TeV-pythia8.root",           "START36_V10", "8E29", "3_6_2", "/QCD_Pt-470to600_7TeV-pythia8/Summer10-START36_V10_S09-v2/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,    206888,   1.657e+01,    "QCD",    2004130,   "QCD_Pt-600_800_7TeV-pythia8.root",           "START36_V10", "8E29", "3_6_2", "/QCD_Pt-600to800_7TeV-pythia8/Summer10-START36_V10_S09-v2/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,    163350,   1.997e+00,    "QCD",    2004140,   "QCD_Pt-800_1000_7TeV-pythia8.root",          "START36_V10", "8E29", "3_6_2", "/QCD_Pt-800to1000_7TeV-pythia8/Summer10-START36_V10_S09-v2/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,    165000,   3.621e-01,    "QCD",    2004150,   "QCD_Pt-1000_1400_7TeV-pythia8.root",         "START36_V10", "8E29", "3_6_2", "/QCD_Pt-1000to1400_7TeV-pythia8/Summer10-START36_V10_S09-v2/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,    164710,   1.179e-02,    "QCD",    2004160,   "QCD_Pt-1400_1800_7TeV-pythia8.root",         "START36_V10", "8E29", "3_6_2", "/QCD_Pt-1400to1800_7TeV-pythia8/Summer10-START36_V10_S09-v2/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,    164500,   3.743e-04,    "QCD",    2004170,   "QCD_Pt-1800_2200_7TeV-pythia8.root",         "START36_V10", "8E29", "3_6_2", "/QCD_Pt-1800to2200_7TeV-pythia8/Summer10-START36_V10_S09-v2/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,    109800,   7.590e-06,    "QCD",    2004180,   "QCD_Pt-2200_2600_7TeV-pythia8.root",         "START36_V10", "8E29", "3_6_2", "/QCD_Pt-2200to2600_7TeV-pythia8/Summer10-START36_V10_S09-v2/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,    109800,   5.458e-08,    "QCD",    2004190,   "QCD_Pt-2600_3000_7TeV-pythia8.root",         "START36_V10", "8E29", "3_6_2", "/QCD_Pt-2600to3000_7TeV-pythia8/Summer10-START36_V10_S09-v2/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,    107500,   3.283e-11,    "QCD",    2004200,   "QCD_Pt-3000_3500_7TeV-pythia8.root",         "START36_V10", "8E29", "3_6_2", "/QCD_Pt-3000to3500_7TeV-pythia8/Summer10-START36_V10_S09-v2/GEN-SIM-RECO"},
  // NLO ttbar inclusive cross section calculated with MCFM, initial (LO) was 95
  // NNLO Z+jets cross section for Mll > 50, l=e,mu,tau calculated with FEWZ, initial (LO) was 2224
  // NNLO W+jets cross section for l=e,mu,tau  calculated with FEWZ, initial (LO) was 25090
  // https://twiki.cern.ch/twiki/bin/viewauth/CMS/MadGraphStandardModel
  // https://twiki.cern.ch/twiki/pub/CMS/GeneratorMain/ShortXsec.pdf
  {0,    7,  0,  0,   0,       0,   1047008,     3048,        "Z+jets", 4052001,   "ZJets_7TeV-madgraph-tauola.root",            "START36_V9",  "8E29", "3_6_2", "/ZJets_7TeV-madgraph-tauola/Summer10-START36_V9_S09-v2/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,  10218854,    31314,        "W+jets", 4051001,   "WJets_7TeV-madgraph-tauola-36.root",            "START36_V9",  "8E29", "3_6_2", "/WJets_7TeV-madgraph-tauola/Summer10-START36_V9_S09-v1/GEN-SIM-RECO"},
  {0,    7,  0,  0,   0,       0,   1463572,    157.5,        "ttbar",  4040001,   "TTbarJets_7TeV-madgraph-tauola.root",        "START36_V9",  "8E29", "3_6_2", "/TTbarJets_Tauola-madgraph/Summer10-START36_V9_S09-v1/GEN-SIM-RECO"},
  
  
  // https://twiki.cern.ch/twiki/bin/view/CMS/ProductionFall2010
  // NLO ttbar inclusive cross section calculated with MCFM, initial (LO) was 94
  // Total Z+jets CS (LO) = 2436. k factor 1.250000 (calculated from the above LO and NNLO cs) 1.37
  // Total WZ CS: NLO, see above
  
  {0,     7,  0,  0,   0,       0,1435909 ,  1.250000*1929,  "Z+jets",  8000022,   "Z0Jets_TuneZ2_7TeV-alpgen-tauola.root",  "START38_V12::All",  "1E31",   "3_8_5", "/Z0Jets_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},   
  
  {0,     7,  0,  0,   0,       0, 317567 ,  1.250000*380.8,   "Z+jets",  8000023,   "Z1Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola.root",  "START38_V12::All",  "1E31",   "3_8_5", "/Z1Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0, 265406 ,  1.250000*8.721,   "Z+jets",  8000028,   "Z1Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola.root",  "START38_V12::All",  "1E31",   "3_8_5", "/Z1Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0, 110308 ,  1.250000*0.07386,   "Z+jets",  8000033,   "Z1Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola.root",  "START38_V12::All",  "1E31",   "3_8_5", "/Z1Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,  32549 ,  1.250000*0.0001374,   "Z+jets",  8000038,   "Z1Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola.root",  "START38_V12::All",  "1E31",   "3_8_5", "/Z1Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  
  {0,     7,  0,  0,   0,       0, 118361 ,  1.250000*103.5,   "Z+jets",  8000024,   "Z2Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola.root",  "START38_V12::All",  "1E31",   "3_8_5", "/Z2Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0, 129106 ,  1.250000*8.534,   "Z+jets",  8000029,   "Z2Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola.root",  "START38_V12::All",  "1E31",   "3_8_5", "/Z2Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0, 109393 ,  1.250000*0.1151,   "Z+jets",  8000034,   "Z2Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola.root",  "START38_V12::All",  "1E31",   "3_8_5", "/Z2Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,  11040 ,  1.250000*0.0003023,   "Z+jets",  8000039,   "Z2Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola.root",  "START38_V12::All",  "1E31",   "3_8_5", "/Z2Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
 
  {0,     7,  0,  0,   0,       0,  55037 ,  1.250000*22.89,   "Z+jets",  8000025,   "Z3Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola.root",  "START38_V12::All",  "1E31",   "3_8_5", "/Z3Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,  55058 ,  1.250000*3.951,   "Z+jets",  8000030,   "Z3Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola.root",  "START38_V12::All",  "1E31",   "3_8_5", "/Z3Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,  54262 ,  1.250000*0.08344,   "Z+jets",  8000035,   "Z3Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola.root",  "START38_V12::All",  "1E31",   "3_8_5", "/Z3Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,  11328 ,  1.250000*0.0002480,   "Z+jets",  8000040,   "Z3Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola.root",  "START38_V12::All",  "1E31",   "3_8_5", "/Z3Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
 
  {0,     7,  0,  0,   0,       0,  44432 ,  1.250000*4.619,   "Z+jets",  8000026,   "Z4Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola.root",  "START38_V12::All",  "1E31",   "3_8_5", "/Z4Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,  44276 ,  1.250000*1.298,   "Z+jets",  8000031,   "Z4Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola.root",  "START38_V12::All",  "1E31",   "3_8_5", "/Z4Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,  10874 ,  1.250000*0.03935,   "Z+jets",  8000036,   "Z4Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola.root",  "START38_V12::All",  "1E31",   "3_8_5", "/Z4Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,  10622 ,  1.250000*0.0001394,   "Z+jets",  8000041,   "Z4Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola.root",  "START38_V12::All",  "1E31",   "3_8_5", "/Z4Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
 
  {0,     7,  0,  0,   0,       0,  10934 ,  1.250000*1.135,   "Z+jets",  8000027,   "Z5Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola.root",  "START38_V12::All",  "1E31",   "3_8_5", "/Z5Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,  10563 ,  1.250000*0.4758,   "Z+jets",  8000032,   "Z5Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola.root",  "START38_V12::All",  "1E31",   "3_8_5", "/Z5Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,  11146 ,  1.250000*0.01946,   "Z+jets",  8000037,   "Z5Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola.root",  "START38_V12::All",  "1E31",   "3_8_5", "/Z5Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,  11172 ,  1.250000*0.00007195,   "Z+jets",  8000042,   "Z5Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola.root",  "START38_V12::All",  "1E31",   "3_8_5", "/Z5Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
 
  // {0,     7,  0,  0,   0,       0,1404230 ,  1.250000*1917,   "Z+jets",  8000064,   "Z0Jets_TuneD6T_7TeV-alpgen-tauola.root",  "START38_V12::All",  "1E31",   "3_8_5", "/Z0Jets_TuneD6T_7TeV-alpgen-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  // {0,     7,  0,  0,   0,       0,  11094 ,  1.250000*0.0002403,   "Z+jets",  8000081,   "Z2Jets_ptZ-800to1600_TuneD6T_7TeV-alpgen-tauola.root",  "START38_V12::All",  "1E31",   "3_8_5", "/Z2Jets_ptZ-800to1600_TuneD6T_7TeV-alpgen-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  
  
  {0,    7,  0,  0,   0,        0,10218854,         31314,   "W+jets",  4051001,   "WJets_7TeV-madgraph-tauola.root",                          "START38_V14::All",  "1E31",   "3_8_7", "/WJets_7TeV-madgraph-tauola/Summer10-START36_V9_S09-v1/GEN-SIM-RECO"},
 
  {0,     7,  0,  0,   0,       0, 1099550,          167,   "ttbar",  1000454,   "TT_TuneZ2_7TeV-pythia6-tauola.root",                       "START38_V12::All",  "1E31",   "3_8_5", "/TT_TuneZ2_7TeV-pythia6-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0, 1164732,          167,   "ttbar",  4000040,   "TTJets_TuneZ2_7TeV-madgraph-tauola.root",                  "START38_V12::All",  "1E31",   "3_8_5", "/TTJets_TuneZ2_7TeV-madgraph-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
 
  {0,     7,  0,  0,   0,       0, 2194752,           18.2,      "WZ",  1000031,   "WZtoAnything_TuneZ2_7TeV-pythia6-tauola.root",             "START38_V12::All",  "1E31",   "3_8_5", "/WZtoAnything_TuneZ2_7TeV-pythia6-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0, 2113368,            5.9,      "ZZ",  1000032,   "ZZtoAnything_TuneZ2_7TeV-pythia6-tauola.root",             "START38_V12::All",  "1E31",   "3_8_7", "/ZZtoAnything_TuneZ2_7TeV-pythia6-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0, 2061760,             43,      "WW",  1000030,   "WWtoAnything_TuneZ2_7TeV-pythia6-tauola.root",             "START38_V12::All",  "1E31",   "3_8_5", "/WWtoAnything_TuneZ2_7TeV-pythia6-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
 
  {0,     7,  0,  0,   0,       0,  494961,           10.6,      "tW",  4000018,   "TToBLNu_TuneZ2_tW-channel_7TeV-madgraph.root",             "START38_V14::All",  "1E31",   "3_8_7", "/TToBLNu_TuneZ2_tW-channel_7TeV-madgraph/Fall10-START38_V12-v2/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,  871720,           0.1835,    "tW",  9000374,   "TW_dr_7TeV-mcatnlo.root",                                  "START38_V14::All",  "1E31",   "3_8_7", "/TW_dr_7TeV-mcatnlo/Fall10-START38_V12-v2/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,  861319,           0.1782,    "tW",  9000375,   "TW_ds_7TeV-mcatnlo.root",                                  "START38_V14::All",  "1E31",   "3_8_7", "/TW_dr_7TeV-mcatnlo/Fall10-START38_V12-v2/GEN-SIM-RECO"},
  
  // background samples for systematics study:
  {0,     7,  0,  0,   0,       0,  1662884,      2205,     "Z+jets",   4000036,    "DYJetsToLL_TuneD6T_matchingdown_7TeV-madgraph-tauola.root",    "START38_V12::All",  "",   "3_8_5", "/DYJetsToLL_TuneD6T_matchingdown_7TeV-madgraph-tauola/Fall10-START38_V12-v2/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,  1667367,      2366,     "Z+jets",   4000037,    "DYJetsToLL_TuneD6T_matchingup_7TeV-madgraph-tauola.root",      "START38_V12::All",  "",   "3_8_5", "/DYJetsToLL_TuneD6T_matchingup_7TeV-madgraph-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,  1436150,      2541,     "Z+jets",   4000034,    "DYJetsToLL_TuneD6T_scaledown_7TeV-madgraph-tauola.root",       "START38_V12::All",  "",   "3_8_5", "/DYJetsToLL_TuneD6T_scaledown_7TeV-madgraph-tauola/Fall10-START38_V12-v2/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,  1329028,      2583,     "Z+jets",   4000035,    "DYJetsToLL_TuneD6T_scaleup_7TeV-madgraph-tauola.root",         "START38_V12::All",  "",   "3_8_5", "/DYJetsToLL_TuneD6T_scaleup_7TeV-madgraph-tauola/Fall10-START38_V12-v2/GEN-SIM-RECO"},
  
  {0,     7,  0,  0,   0,       0,  1209681,      94.6,     "ttbar",    4000024,    "TTJets_TuneD6T_smallerISRFSR_7TeV-madgraph-tauola.root",       "START38_V12::All",  "",   "3_8_5", "/TTJets_TuneD6T_smallerISRFSR_7TeV-madgraph-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,  1394010,     100.5,     "ttbar",    4000025,    "TTJets_TuneD6T_largerISRFSR_7TeV-madgraph-tauola.root",        "START38_V12::All",  "",   "3_8_5", "/TTJets_TuneD6T_largerISRFSR_7TeV-madgraph-tauola/Fall10-START38_V12-v2/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,   938005,     111.1,     "ttbar",    4000028,    "TTJets_TuneD6T_matchingdown_7TeV-madgraph-tauola.root",        "START38_V12::All",  "",   "3_8_5", "/TTJets_TuneD6T_matchingdown_7TeV-madgraph-tauola/Fall10-START38_V12-v2/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,  1036968,     105.9,     "ttbar",    4000029,    "TTJets_TuneD6T_matchingup_7TeV-madgraph-tauola.root",          "START38_V12::All",  "",   "3_8_5", "/TTJets_TuneD6T_matchingup_7TeV-madgraph-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,  1096071,     186.6,     "ttbar",    4000026,    "TTJets_TuneD6T_scaledown_7TeV-madgraph-tauola.root",           "START38_V12::All",  "",   "3_8_5", "/TTJets_TuneD6T_scaledown_7TeV-madgraph-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,  1146546,      75.0,     "ttbar",    4000027,    "TTJets_TuneD6T_scaleup_7TeV-madgraph-tauola.root",             "START38_V12::All",  "",   "3_8_5", "/TTJets_TuneD6T_scaleup_7TeV-madgraph-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,  1098152,     106.4,     "ttbar",    1000481,    "TT_smallerISRFSR_TuneZ2_7TeV-pythia6-tauola.root",             "START38_V12::All",  "",   "3_8_5", "/TT_smallerISRFSR_TuneZ2_7TeV-pythia6-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,  1100000,     108.3,     "ttbar",    1000480,    "TT_largerISRFSR-TuneZ2_7TeV-pythia6-tauola.root",              "START38_V12::All",  "",   "3_8_5", "/TT_largerISRFSR-TuneZ2_7TeV-pythia6-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  
  {0,     7,  0,  0,   0,       0,  6303252,     27230,     "W+jets",   4000031,    "WJets_TuneD6T_scaleup_7TeV-madgraph-tauola.root",              "START38_V12::All",  "",   "3_8_5", "/WJets_TuneD6T_scaleup_7TeV-madgraph-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,  5165729,     26530,     "W+jets",   4000030,    "WJets_TuneD6T_scaledown_7TeV-madgraph-tauola.root",            "START38_V12::All",  "",   "3_8_5", "/WJets_TuneD6T_scaledown_7TeV-madgraph-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,  9882119,     25950,     "W+jets",   4000033,    "WJets_TuneD6T_matchingup_7TeV-madgraph-tauola.root",           "START38_V12::All",  "",   "3_8_5", "/WJets_TuneD6T_matchingup_7TeV-madgraph-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,  2689051,     24990,     "W+jets",   4000032,    "WJets_TuneD6T_matchingdown_7TeV-madgraph-tauola.root",         "START38_V12::All",  "",   "3_8_5", "/WJets_TuneD6T_matchingdown_7TeV-madgraph-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  
  {0,     7,  0,  0,   0,       0,  9254955,     22700,     "Gamma+Jets",   8000085,    "G1Jet_Pt-20to60_TuneZ2_7TeV-alpgen.root",         "START38_V14::All",  "",   "3_8_7_patch2", "/G1Jet_Pt-20to60_TuneZ2_7TeV-alpgen/Fall10-START38_V12-v1/GEN-SIM-RECO"}, 
  {0,     7,  0,  0,   0,       0,   328617,     799.3,     "Gamma+Jets",   8000086,    "G1Jet_Pt-60to120_TuneZ2_7TeV-alpgen.root",         "START38_V14::All",  "",   "3_8_7_patch2","/G1Jets_Pt-60to120_TuneZ2_7TeV-alpgen/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,   329416,     40.11,     "Gamma+Jets",   8000087,    "G1Jet_Pt-120to180_TuneZ2_7TeV-alpgen.root",         "START38_V14::All",  "",   "3_8_7_patch2","/G1Jets_Pt-120to180_TuneZ2_7TeV-alpgen/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,   328519,     5.612,     "Gamma+Jets",   8000088,    "G1Jet_Pt-180to240_TuneZ2_7TeV-alpgen.root",         "START38_V14::All",  "",   "3_8_7_patch2","/G1Jets_Pt-180to240_TuneZ2_7TeV-alpgen/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,   331485,     1.224,     "Gamma+Jets",   8000089,    "G1Jet_Pt-240to300_TuneZ2_7TeV-alpgen.root",         "START38_V14::All",  "",   "3_8_7_patch2","/G1Jets_Pt-240to300_TuneZ2_7TeV-alpgen/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,   331587,     0.545,     "Gamma+Jets",   8000090,    "G1Jet_Pt-300to5000_TuneZ2_7TeV-alpgen.root",         "START38_V14::All",  "",   "3_8_7_patch2","/G1Jets_Pt-300to5000_TuneZ2_7TeV-alpgen/Fall10-START38_V12-v1/GEN-SIM-RECO"},

  {0,     7,  0,  0,   0,       0,  1782042, 4.080e+03,     "Gamma+Jets",   8000091,    "G2Jets_Pt-20to60_TuneZ2_7TeV-alpgen.root",          "START38_V14::All",  "",   "3_8_7_patch2","/G2Jets_Pt-20to60_TuneZ2_7TeV-alpgen/Fall10-START38_V12-v1/AODSIM"},
  {0,     7,  0,  0,   0,       0,   338909, 4.258e+02,     "Gamma+Jets",   8000092,    "G2Jets_Pt-60to120_TuneZ2_7TeV-alpgen.root",         "START38_V14::All",  "",   "3_8_7_patch2","/G2Jets_Pt-60to120_TuneZ2_7TeV-alpgen/Fall10-START38_V12-v1/AODSIM"},
  {0,     7,  0,  0,   0,       0,   286860, 3.556e+01,     "Gamma+Jets",   8000083,    "G2Jet_Pt-120to180_TuneZ2_7TeV-alpgen.root",         "START38_V14::All",  "",   "3_8_7_patch2","/G2Jets_Pt-120to180_TuneZ2_7TeV-alpgen/Fall10-START38_V12-v1/AODSIM"}, 
 {0,     7,  0,  0,   0,       0,    329962, 5.941e+00,     "Gamma+Jets",   8000094,    "G2Jets_Pt-180to240_TuneZ2_7TeV-alpgen.root",         "START38_V14::All",  "", "3_8_7_patch2","/G2Jets_Pt-180to240_TuneZ2_7TeV-alpgen/Fall10-START38_V12-v1/AODSIM"},
  {0,     7,  0,  0,   0,       0,   331339, 1.448e+00,     "Gamma+Jets",   8000095,    "G2Jets_Pt-240to300_TuneZ2_7TeV-alpgen.root",         "START38_V14::All",  "", "3_8_7_patch2","/G2Jets_Pt-240to300_TuneZ2_7TeV-alpgen/Fall10-START38_V12-v1/AODSIM"},
  {0,     7,  0,  0,   0,       0,   241144, 7.233e-01,     "Gamma+Jets",   8000096,    "G2Jet_Pt-300to5000_TuneZ2_7TeV-alpgen.root",         "START38_V14::All",  "",   "3_8_7_patch2","/G2Jets_Pt-300to5000_TuneZ2_7TeV-alpgen/Fall10-START38_V12-v1/AODSIM"},

  {0,     7,  0,  0,   0,       0,  322557,  7.947e+02,     "Gamma+Jets",   8000097,    "G3Jets_Pt-20to60_TuneZ2_7TeV-alpgen.root",          "START38_V14::All",  "",   "3_8_7_patch2","/G3Jets_Pt-20to60_TuneZ2_7TeV-alpgen/Fall10-START38_V12-v1/AODSIM"},
  {0,     7,  0,  0,   0,       0,  333681,  1.187e+02,     "Gamma+Jets",   8000098,    "G3Jets_Pt-60to120_TuneZ2_7TeV-alpgen.root",         "START38_V14::All",  "",   "3_8_7_patch2","/G3Jets_Pt-60to120_TuneZ2_7TeV-alpgen/Fall10-START38_V12-v1/AODSIM"},
  {0,     7,  0,  0,   0,       0,  331691,  1.527e+01,     "Gamma+Jets",   8000099,    "G3Jets_Pt-120to180_TuneZ2_7TeV-alpgen.root",         "START38_V14::All",  "", "3_8_7_patch2","/G3Jets_Pt-120to180_TuneZ2_7TeV-alpgen/Fall10-START38_V12-v1/AODSIM"},
  {0,     7,  0,  0,   0,       0,  324607,  3.090e+00,     "Gamma+Jets",   8000100,    "G3Jets_Pt-180to240_TuneZ2_7TeV-alpgen.root",         "START38_V14::All",  "", "3_8_7_patch2","/G3Jets_Pt-180to240_TuneZ2_7TeV-alpgen/Fall10-START38_V12-v1/AODSIM"},
  {0,     7,  0,  0,   0,       0,  339273,  7.587e-01,     "Gamma+Jets",   8000101,    "G3Jets_Pt-240to300_TuneZ2_7TeV-alpgen.root",         "START38_V14::All",  "", "3_8_7_patch2","/G3Jets_Pt-240to300_TuneZ2_7TeV-alpgen/Fall10-START38_V12-v1/AODSIM"}, 
  {0,     7,  0,  0,   0,       0,  259105,  4.663e-01,     "Gamma+Jets",   8000102,    "G3Jet_Pt-300to5000_TuneZ2_7TeV-alpgen.root",         "START38_V14::All",  "", "3_8_7_patch2","/G3Jets_Pt-300to5000_TuneZ2_7TeV-alpgen/Fall10-START38_V12-v1/AODSIM"},

  {0,     7,  0,  0,   0,       0,  335546,  1.483e+02,     "Gamma+Jets",   8000103,    "G4Jets_Pt-20to60_TuneZ2_7TeV-alpgen.root",          "START38_V14::All",  "",   "3_8_7_patch2","/G4Jets_Pt-20to60_TuneZ2_7TeV-alpgen/Fall10-START38_V12-v1/AODSIM"},
  {0,     7,  0,  0,   0,       0,  333214,  3.208e+01,     "Gamma+Jets",   8000104,    "G4Jets_Pt-60to120_TuneZ2_7TeV-alpgen.root",         "START38_V14::All",  "",   "3_8_7_patch2","/G4Jets_Pt-60to120_TuneZ2_7TeV-alpgen/Fall10-START38_V12-v1/AODSIM"},
  {0,     7,  0,  0,   0,       0,  328253,  5.732e+00,     "Gamma+Jets",   8000105,    "G4Jets_Pt-120to180_TuneZ2_7TeV-alpgen.root",         "START38_V14::All",  "", "3_8_7_patch2","/G4Jets_Pt-120to180_TuneZ2_7TeV-alpgen/Fall10-START38_V12-v1/AODSIM"},
  {0,     7,  0,  0,   0,       0,  331020,  1.398e+00,     "Gamma+Jets",   8000106,    "G4Jets_Pt-180to240_TuneZ2_7TeV-alpgen.root",         "START38_V14::All",  "", "3_8_7_patch2","/G4Jets_Pt-180to240_TuneZ2_7TeV-alpgen/Fall10-START38_V12-v1/AODSIM"},
  {0,     7,  0,  0,   0,       0,  333241,  4.199e-01,     "Gamma+Jets",   8000107,    "G4Jets_Pt-240to300_TuneZ2_7TeV-alpgen.root",         "START38_V14::All",  "", "3_8_7_patch2","/G4Jets_Pt-240to300_TuneZ2_7TeV-alpgen/Fall10-START38_V12-v1/AODSIM"},
  {0,     7,  0,  0,   0,       0,  331599,  2.488e-01,     "Gamma+Jets",   8000108,  "G4Jets_Pt-300to5000_TuneZ2_7TeV-alpgen.root",         "START38_V14::All",  "", "3_8_7_patch2","/G4Jets_Pt-300to5000_TuneZ2_7TeV-alpgen/Fall10-START38_V12-v1/AODSIM"},

  {0,     7,  0,  0,   0,       0,  4199091, 1.236000*2.024e+04,     "W+0 Jets",     8000001,    "W0Jets_TuneZ2_7TeV-alpgen-tauola.root",             "START38_V14::All",  "",   "3_8_7_patch2", "/W0Jets_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v2/GEN-SIM-RECO"},

  {0,     7,  0,  0,   0,       0,   883023,      1.236000*3693,     "W+1 Jets",   8000002,    "W1Jets_ptW-0to100_TuneZ2_7TeV-alpgen-tauola.root",         "START38_V14::All",  "",   "3_8_7_patch2", "/W1Jets_ptW-0to100_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0, 21060182,     1.236000*71.97,     "W+1 Jets",   8000007,    "W1Jets_ptW-100to300_TuneZ2_7TeV-alpgen-tauola.root",         "START38_V14::All",  "",   "3_8_7_patch2", "/W1Jets_ptW-100to300_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v2/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,  5694804,    1.236000*0.5658,     "W+1 Jets",   8000012,    "W1Jets_ptW-300to800_TuneZ2_7TeV-alpgen-tauola.root",         "START38_V14::All",  "",   "3_8_7_patch2", "/W1Jets_ptW-300to800_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v2/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,   317150,   1.236000*0.00109,     "W+1 Jets",   8000017,    "W1Jets_ptW-800to1600_TuneZ2_7TeV-alpgen-tauola.root",         "START38_V14::All",  "",   "3_8_7_patch2", "/W1Jets_ptW-800to1600_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v2/GEN-SIM-RECO"},

  {0,     7,  0,  0,   0,       0,   2065969,     1.236000*943.4,     "W+2 Jets",   8000003,    "W2Jets_ptW-0to100_TuneZ2_7TeV-alpgen-tauola.root",         "START38_V14::All",  "",   "3_8_7_patch2", "/W2Jets_ptW-0to100_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v2/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,   1658420,     1.236000*67.18,     "W+2 Jets",   8000008,    "W2Jets_ptW-100to300_TuneZ2_7TeV-alpgen-tauola.root",         "START38_V14::All",  "",   "3_8_7_patch2", "/W2Jets_ptW-100to300_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v2/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,    745242,    1.236000*0.8553,     "W+2 Jets",   8000013,    "W2Jets_ptW-300to800_TuneZ2_7TeV-alpgen-tauola.root",         "START38_V14::All",  "",   "3_8_7_patch2", "/W2Jets_ptW-300to800_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v2/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,     39900,    1.236000*0.0022,     "W+2 Jets",   8000018,    "W2Jets_ptW-800to1600_TuneZ2_7TeV-alpgen-tauola.root",         "START38_V14::All",  "",   "3_8_7_patch2", "/W2Jets_ptW-800to1600_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v2/GEN-SIM-RECO"},

  {0,     7,  0,  0,   0,       0,   783313,    1.236000*208.7,     "W+3 Jets",   8000004,    "W3Jets_ptW-0to100_TuneZ2_7TeV-alpgen-tauola.root",         "START38_V14::All",  "",   "3_8_7_patch2", "/W3Jets_ptW-0to100_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v2/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,    33974,     1.236000*32.43,     "W+3 Jets",   8000009,    "W3Jets_ptW-100to300_TuneZ2_7TeV-alpgen-tauola.root",         "START38_V14::All",  "",   "3_8_7_patch2", "/W3Jets_ptW-100to300_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v2/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,   174115,    1.236000*0.6229,     "W+3 Jets",   8000014,    "W3Jets_ptW-300to800_TuneZ2_7TeV-alpgen-tauola.root",         "START38_V14::All",  "",   "3_8_7_patch2", "/W3Jets_ptW-300to800_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v2/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,    62501,  1.236000*0.001974,     "W+3 Jets",   8000019,    "W3Jets_ptW-800to1600_TuneZ2_7TeV-alpgen-tauola.root",         "START38_V14::All",  "",   "3_8_7_patch2", "/W3Jets_ptW-800to1600_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v2/GEN-SIM-RECO"},

  {0,     7,  0,  0,   0,       0,   154846,     1.236000*44.46,     "W+4 Jets",   8000005,    "W4Jets_ptW-0to100_TuneZ2_7TeV-alpgen-tauola.root",         "START38_V14::All",  "",   "3_8_7_patch2", "/W4Jets_ptW-0to100_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v2/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,    75536,     1.236000*11.38,     "W+4 Jets",   8000010,    "W4Jets_ptW-100to300_TuneZ2_7TeV-alpgen-tauola.root",         "START38_V14::All",  "",   "3_8_7_patch2", "/W4Jets_ptW-100to300_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v2/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,    26679,     1.236000*0.295,     "W+4 Jets",   8000015,    "W4Jets_ptW-300to800_TuneZ2_7TeV-alpgen-tauola.root",         "START38_V14::All",  "",   "3_8_7_patch2", "/W4Jets_ptW-300to800_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v2/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,    21932,  1.236000*0.001025,     "W+4 Jets",   8000020,    "W4Jets_ptW-800to1600_TuneZ2_7TeV-alpgen-tauola.root",         "START38_V14::All",  "",   "3_8_7_patch2", "/W4Jets_ptW-800to1600_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v2/GEN-SIM-RECO"},
 
  {0,     7,  0,  0,   0,       0,    41761,     1.236000*11.11,     "W+5 Jets",   8000006,    "W5Jets_ptW-0to100_TuneZ2_7TeV-alpgen-tauola.root",         "START38_V14::All",  "",   "3_8_7_patch2", "/W5Jets_ptW-0to100_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v2/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,    54565,     1.236000*3.789,     "W+5 Jets",   8000011,    "W5Jets_ptW-100to300_TuneZ2_7TeV-alpgen-tauola.root",         "START38_V14::All",  "",   "3_8_7_patch2", "/W5Jets_ptW-100to300_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v2/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,    23959,    1.236000*0.1565,     "W+5 Jets",   8000016,    "W5Jets_ptW-300to800_TuneZ2_7TeV-alpgen-tauola.root",         "START38_V14::All",  "",   "3_8_7_patch2", "/W5Jets_ptW-300to800_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v2/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0,    15163, 1.236000*0.0005882,     "W+5 Jets",   8000021,    "W5Jets_ptW-800to1600_TuneZ2_7TeV-alpgen-tauola.root",         "START38_V14::All",  "",   "3_8_7_patch2", "/W5Jets_ptW-800to1600_TuneZ2_7TeV-alpgen-tauola/Fall10-START38_V12-v2/GEN-SIM-RECO"},
  {0,     7,  0,  0,   0,       0, 14923740,        31314,     "W + Jets -> Leptons",4000041, "WJetsToLNu_TuneZ2_7TeV-madgraph-tauola.root",         "START38_V14::All",  "",   "3_8_7_patch2", "/WJetsToLNu_TuneZ2_7TeV-madgraph-tauola/Fall10-START38_V12-v1/GEN-SIM-RECO"},

  // === CSA11 ======================================================================================================================================================================
  // Spring11 production:

  // ttbar cross section taken from CMS PAS TOP-10-005
  
  {0,  7,  0,  0,  0,  0,  1165716,  167,   "ttbar",  4000040,   "spring11_ttjets_tunez2_7tev-madgraph-tauola-dump.root",                  "START41_V0::All",  "",   "4_1_3", "/TTJets_TuneZ2_7TeV-madgraph-tauola/Fall10-START38_V12-v3/GEN-SIM-RECO"},
  {0,  7,  0,  0,  0,  0,  1286491,  167,  "TTJets",  0,  "TTJets_TuneD6T_7TeV-madgraph-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/TTJets_TuneD6T_7TeV-madgraph-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  1164208,  167,  "TTJets",  0,  "TTJets_TuneZ2_7TeV-madgraph-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/TTJets_TuneZ2_7TeV-madgraph-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},

  {0,  7,  0,  0,  0,  0,  1435909,  1435909*1.250000*1.929e+03/3./478666.,  "ZJetToEE",  0,  "Z0Jets_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z0Jets_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},

  {0,  7,  0,  0,  0,  0,  14904390, 14904390*1.250000*3.808e+02/3./14299454,  "ZJetToEE",  0,  "Z1Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z1Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  11994815, 11994815*1.250000*8.721e+00/3./11806764,  "ZJetToEE",  0,  "Z1Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z1Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  5683177, 5683177*1.250000*7.386e-02/3./5581364,  "ZJetToEE",  0,  "Z1Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z1Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  502517,  502517*1.250000*1.374e-04/3./480803,  "ZJetToEE",  0,  "Z1Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z1Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},

  {0,  7,  0,  0,  0,  0,  1335287, 1335287*1.250000*1.035e+02/3./1165914,  "ZJetToEE",  0,  "Z2Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z2Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  1124019, 1124019*1.250000*8.534e+00/3./1021498,  "ZJetToEE",  0,  "Z2Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z2Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  655292,  655292*1.250000*1.151e-01/3./571713,  "ZJetToEE",  0,  "Z2Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z2Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,   62244,  62244*1.250000*3.023e-04/3./51509,  "ZJetToEE",  0,  "Z2Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z2Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},

  {0,  7,  0,  0,  0,  0,  322554,  322554*1.250000*2.289e+01/3./268322,  "ZJetToEE",  0,  "Z3Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z3Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  293815,  293815*1.250000*3.951e+00/3./228961,  "ZJetToEE",  0,  "Z3Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z3Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  183370,  183370*1.250000*8.344e-02/3./145902,  "ZJetToEE",  0,  "Z3Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z3Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,   24439,  24439*1.250000*2.480e-04/3./13250,  "ZJetToEE",  0,  "Z3Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z3Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},

  {0,  7,  0,  0,  0,  0,  109008, 109008*1.250000*4.619e+00/3./39539.,  "ZJetToEE",  0,  "Z4Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z4Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  121573, 121573*1.250000*1.298e+00/3./79250.,  "ZJetToEE",  0,  "Z4Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z4Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  45752,  45752*1.250000*3.935e-02/3./36754.,  "ZJetToEE",  0,  "Z4Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z4Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  13084,  13084*1.250000*1.394e-04/3./5402.,  "ZJetToEE",  0,  "Z4Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z4Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},

  {0,  7,  0,  0,  0,  0,  31100,  31100*1.250000*1.135e+00/3./21605.,  "ZJetToEE",  0,  "Z5Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z5Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  30657,  30657*1.250000*4.758e-01/3./22803.,  "ZJetToEE",  0,  "Z5Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z5Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  24866,  24866*1.250000*1.946e-02/3./15254.,  "ZJetToEE",  0,  "Z5Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z5Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  31666,  31666*1.250000*7.195e-05/3./23232.,  "ZJetToEE",  0,  "Z5Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z5Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  
  {0,  7,  0,  0,  0,  0,  1435909,  1435909*1.250000*1.929e+03/3./478753.,   "ZJetToMuMu",  0,  "Z0Jets_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z0Jets_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  
  {0,  7,  0,  0,  0,  0,  14904390,  14904390*1.250000*3.808e+02/3./302244.,  "ZJetToMuMu",  0,  "Z1Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z1Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  11994815,  11994815*1.250000*8.721e+00/3./93929.,  "ZJetToMuMu",  0,  "Z1Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z1Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,   5683177,   5683177*1.250000*7.386e-02/3./50636.,  "ZJetToMuMu",  0,  "Z1Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z1Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,    502517,    502517*1.250000*1.374e-04/3./10806.,  "ZJetToMuMu",  0,  "Z1Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z1Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},

  {0,  7,  0,  0,  0,  0,  1335287,  1335287*1.250000*1.035e+02/3./84724.,  "ZJetToMuMu",  0,  "Z2Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z2Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  1124019,  1124019*1.250000*8.534e+00/3./51054.,  "ZJetToMuMu",  0,  "Z2Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z2Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,   655292,   655292*1.250000*1.151e-01/3./41610.,  "ZJetToMuMu",  0,  "Z2Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z2Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,    62244,    62244*1.250000*3.023e-04/3./5422.,  "ZJetToMuMu",  0,  "Z2Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z2Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},

  {0,  7,  0,  0,  0,  0,  322554,  322554*1.250000*2.289e+01/3./27416.,  "ZJetToMuMu",  0,  "Z3Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z3Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  293815,  293815*1.250000*3.951e+00/3./32446.,  "ZJetToMuMu",  0,  "Z3Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z3Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  183370,  183370*1.250000*8.344e-02/3./18658.,  "ZJetToMuMu",  0,  "Z3Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z3Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,   24439,   24439*1.250000*2.480e-04/3./5657.,  "ZJetToMuMu",  0,  "Z3Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z3Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},

  {0,  7,  0,  0,  0,  0,  109008, 109008*1.250000*4.619e+00/3./34794.,  "ZJetToMuMu",  0,  "Z4Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z4Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  121573, 121573*1.250000*1.298e+00/3./21178.,  "ZJetToMuMu",  0,  "Z4Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z4Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  45752,  45752*1.250000*3.935e-02/3./4467.,  "ZJetToMuMu",  0,  "Z4Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z4Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  13084,  13084*1.250000*1.394e-04/3./3875.,  "ZJetToMuMu",  0,  "Z4Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z4Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},

  {0,  7,  0,  0,  0,  0,  31100, 31100*1.250000*1.135e+00/3./4666.,  "ZJetToMuMu",  0,  "Z5Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z5Jets_ptZ-0to100_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  30657, 30657*1.250000*4.758e-01/3./4012.,  "ZJetToMuMu",  0,  "Z5Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z5Jets_ptZ-100to300_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  24866, 24866*1.250000*1.946e-02/3./4831.,  "ZJetToMuMu",  0,  "Z5Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z5Jets_ptZ-300to800_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  31666, 31666*1.250000*7.195e-05/3./4169.,  "ZJetToMuMu",  0,  "Z5Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM_1.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/Z5Jets_ptZ-800to1600_TuneZ2_7TeV-alpgen-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},

  {0,  7,  0,  0,  0,  0,   489417,  10.6,  "TToBLNu",       0,  "TToBLNu_TuneZ2_tW-channel_7TeV-madgraph_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/TToBLNu_TuneZ2_tW-channel_7TeV-madgraph/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  2061760,    43.,  "WWtoAnything",  0,  "WWtoAnything_TuneZ2_7TeV-pythia6-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/WWtoAnything_TuneZ2_7TeV-pythia6-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  2108416,   18.2,  "WZtoAnything",  0,  "WZtoAnything_TuneZ2_7TeV-pythia6-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/WZtoAnything_TuneZ2_7TeV-pythia6-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  2108608,    5.9,  "ZZtoAnything",  0,  "ZZtoAnything_TuneZ2_7TeV-pythia6-tauola_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/ZZtoAnything_TuneZ2_7TeV-pythia6-tauola/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  
  // gamma+jets:
  {0,  7,  0,  0,  0,  0,  328617,  7.993e+02,  "Gamma+Jets",  0,  "G1Jet_Pt-60to120_TuneZ2_7TeV-alpgen_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/G1Jet_Pt-60to120_TuneZ2_7TeV-alpgen/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  329416,  4.011e+01,  "Gamma+Jets",  0,  "G1Jet_Pt-120to180_TuneZ2_7TeV-alpgen_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/G1Jet_Pt-120to180_TuneZ2_7TeV-alpgen/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  328519,  5.612e+00,  "Gamma+Jets",  0,  "G1Jet_Pt-180to240_TuneZ2_7TeV-alpgen_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/G1Jet_Pt-180to240_TuneZ2_7TeV-alpgen/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  331485,  1.224e+00,  "Gamma+Jets",  0,  "G1Jet_Pt-240to300_TuneZ2_7TeV-alpgen_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/G1Jet_Pt-240to300_TuneZ2_7TeV-alpgen/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  325230,  5.450e-01,  "Gamma+Jets",  0,  "G1Jet_Pt-300to5000_TuneZ2_7TeV-alpgen_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/G1Jet_Pt-300to5000_TuneZ2_7TeV-alpgen/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},

  {0,  7,  0,  0,  0,  0, 1782042,  4.080e+03,  "Gamma+Jets",  0,  "G2Jets_Pt-20to60_TuneZ2_7TeV-alpgen_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/G2Jets_Pt-20to60_TuneZ2_7TeV-alpgen/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  334388,  4.258e+02,  "Gamma+Jets",  0,  "G2Jets_Pt-60to120_TuneZ2_7TeV-alpgen_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/G2Jets_Pt-60to120_TuneZ2_7TeV-alpgen/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  312293,  3.556e+01,  "Gamma+Jets",  0,  "G2Jets_Pt-120to180_TuneZ2_7TeV-alpgen_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/G2Jets_Pt-120to180_TuneZ2_7TeV-alpgen/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  329962,  5.941e+00,  "Gamma+Jets",  0,  "G2Jets_Pt-180to240_TuneZ2_7TeV-alpgen_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/G2Jets_Pt-180to240_TuneZ2_7TeV-alpgen/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  331339,  1.448e+00,  "Gamma+Jets",  0,  "G2Jets_Pt-240to300_TuneZ2_7TeV-alpgen_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/G2Jets_Pt-240to300_TuneZ2_7TeV-alpgen/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  333630,  7.233e-01,  "Gamma+Jets",  0,  "G2Jets_Pt-300to5000_TuneZ2_7TeV-alpgen_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/G2Jets_Pt-300to5000_TuneZ2_7TeV-alpgen/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},

  {0,  7,  0,  0,  0,  0,  280686,  7.947e+02,  "Gamma+Jets",  0,  "G3Jets_Pt-20to60_TuneZ2_7TeV-alpgen_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/G3Jets_Pt-20to60_TuneZ2_7TeV-alpgen/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  322188,  1.187e+02,  "Gamma+Jets",  0,  "G3Jets_Pt-60to120_TuneZ2_7TeV-alpgen_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/G3Jets_Pt-60to120_TuneZ2_7TeV-alpgen/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  299192,  1.527e+01,  "Gamma+Jets",  0,  "G3Jets_Pt-120to180_TuneZ2_7TeV-alpgen_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/G3Jets_Pt-120to180_TuneZ2_7TeV-alpgen/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  314350,  3.090e+00,  "Gamma+Jets",  0,  "G3Jets_Pt-180to240_TuneZ2_7TeV-alpgen_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/G3Jets_Pt-180to240_TuneZ2_7TeV-alpgen/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  319663,  7.587e-01,  "Gamma+Jets",  0,  "G3Jets_Pt-240to300_TuneZ2_7TeV-alpgen_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/G3Jets_Pt-240to300_TuneZ2_7TeV-alpgen/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  311653,  4.663e-01,  "Gamma+Jets",  0,  "G3Jets_Pt-300to5000_TuneZ2_7TeV-alpgen_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/G3Jets_Pt-300to5000_TuneZ2_7TeV-alpgen/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},

  {0,  7,  0,  0,  0,  0,  327107,  1.483e+02,  "Gamma+Jets",  0,  "G4Jets_Pt-20to60_TuneZ2_7TeV-alpgen_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/G4Jets_Pt-20to60_TuneZ2_7TeV-alpgen/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  325193,  3.208e+01,  "Gamma+Jets",  0,  "G4Jets_Pt-60to120_TuneZ2_7TeV-alpgen_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/G4Jets_Pt-60to120_TuneZ2_7TeV-alpgen/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  322067,  5.732e+00,  "Gamma+Jets",  0,  "G4Jets_Pt-120to180_TuneZ2_7TeV-alpgen_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/G4Jets_Pt-120to180_TuneZ2_7TeV-alpgen/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  331020,  1.398e+00,  "Gamma+Jets",  0,  "G4Jets_Pt-180to240_TuneZ2_7TeV-alpgen_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/G4Jets_Pt-180to240_TuneZ2_7TeV-alpgen/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  333241,  4.199e-01,  "Gamma+Jets",  0,  "G4Jets_Pt-240to300_TuneZ2_7TeV-alpgen_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/G4Jets_Pt-240to300_TuneZ2_7TeV-alpgen/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  339789,  2.488e-01,  "Gamma+Jets",  0,  "G4Jets_Pt-300to5000_TuneZ2_7TeV-alpgen_Spring11-PU_S1_START311_V1G1-v1_AODSIM.root",  "START41_V0::All",  "GRun",  "4_1_6",  "/G4Jets_Pt-300to5000_TuneZ2_7TeV-alpgen/Spring11-PU_S1_START311_V1G1-v1/AODSIM"},
  

  // Summer11 production: 
  {0,  7,  0,  0,  0,  0,  165000,  1.250000*1.305e+02,  "ZJetToEE",  0,  "ZJetToEE_Pt-20to30_TuneZ2_7TeV_pythia6_Summer11-PU_S3_START42_V11-v2_AODSIM.root",  "START42_V11::All",  "GRun",  "4_2_3_patch2",  "/ZJetToEE_Pt-20to30_TuneZ2_7TeV_pythia6/Summer11-PU_S3_START42_V11-v2/AODSIM"},
  {0,  7,  0,  0,  0,  0,  165000,  1.250000*8.397e+01,  "ZJetToEE",  0,  "ZJetToEE_Pt-30to50_TuneZ2_7TeV_pythia6_Summer11-PU_S3_START42_V11-v2_AODSIM.root",  "START42_V11::All",  "GRun",  "4_2_3_patch2",  "/ZJetToEE_Pt-30to50_TuneZ2_7TeV_pythia6/Summer11-PU_S3_START42_V11-v2/AODSIM"},
  {0,  7,  0,  0,  0,  0,  110000,  1.250000*3.227e+01,  "ZJetToEE",  0,  "ZJetToEE_Pt-50to80_TuneZ2_7TeV_pythia6_Summer11-PU_S3_START42_V11-v2_AODSIM.root",  "START42_V11::All",  "GRun",  "4_2_3_patch2",  "/ZJetToEE_Pt-50to80_TuneZ2_7TeV_pythia6/Summer11-PU_S3_START42_V11-v2/AODSIM"},
  {0,  7,  0,  0,  0,  0,  110000,  1.250000*9.986e+00,  "ZJetToEE",  0,  "ZJetToEE_Pt-80to120_TuneZ2_7TeV_pythia6_Summer11-PU_S3_START42_V11-v2_AODSIM.root",  "START42_V11::All",  "GRun",  "4_2_3_patch2",  "/ZJetToEE_Pt-80to120_TuneZ2_7TeV_pythia6/Summer11-PU_S3_START42_V11-v2/AODSIM"},
  {0,  7,  0,  0,  0,  0,  107304,  1.250000*2.735e+00,  "ZJetToEE",  0,  "ZJetToEE_Pt-120to170_TuneZ2_7TeV_pythia6_Summer11-PU_S3_START42_V11-v2_AODSIM.root",  "START42_V11::All",  "GRun",  "4_2_3_patch2",  "/ZJetToEE_Pt-120to170_TuneZ2_7TeV_pythia6/Summer11-PU_S3_START42_V11-v2/AODSIM"},
  {0,  7,  0,  0,  0,  0,  110000,  1.250000*7.216e-01,  "ZJetToEE",  0,  "ZJetToEE_Pt-170to230_TuneZ2_7TeV_pythia6_Summer11-PU_S3_START42_V11-v2_AODSIM.root",  "START42_V11::All",  "GRun",  "4_2_3_patch2",  "/ZJetToEE_Pt-170to230_TuneZ2_7TeV_pythia6/Summer11-PU_S3_START42_V11-v2/AODSIM"},
  {0,  7,  0,  0,  0,  0,  110000,  1.250000*1.938e-01,  "ZJetToEE",  0,  "ZJetToEE_Pt-230to300_TuneZ2_7TeV_pythia6_Summer11-PU_S3_START42_V11-v2_AODSIM.root",  "START42_V11::All",  "GRun",  "4_2_3_patch2",  "/ZJetToEE_Pt-230to300_TuneZ2_7TeV_pythia6/Summer11-PU_S3_START42_V11-v2/AODSIM"},

  {0,  7,  0,  0,  0,  0,  165000,  1.250000*1.305e+02, "ZJetToMuMu",  0,  "ZJetToMuMu_Pt-20to30_TuneZ2_7TeV_pythia6_Summer11-PU_S3_START42_V11-v2_AODSIM.root",  "START42_V11::All",  "GRun",  "4_2_3_patch2",  "/ZJetToMuMu_Pt-20to30_TuneZ2_7TeV_pythia6/Summer11-PU_S3_START42_V11-v2/AODSIM"},
  {0,  7,  0,  0,  0,  0,  110000,  1.250000*9.984e+00, "ZJetToMuMu",  0,  "ZJetToMuMu_Pt-80to120_TuneZ2_7TeV_pythia6_Summer11-PU_S3_START42_V11-v2_AODSIM.root",  "START42_V11::All",  "GRun",  "4_2_3_patch2",  "/ZJetToMuMu_Pt-80to120_TuneZ2_7TeV_pythia6/Summer11-PU_S3_START42_V11-v2/AODSIM"},
  {0,  7,  0,  0,  0,  0,  110000,  1.250000*2.734e+00, "ZJetToMuMu",  0,  "ZJetToMuMu_Pt-120to170_TuneZ2_7TeV_pythia6_Summer11-PU_S3_START42_V11-v2_AODSIM.root",  "START42_V11::All",  "GRun",  "4_2_3_patch2",  "/ZJetToMuMu_Pt-120to170_TuneZ2_7TeV_pythia6/Summer11-PU_S3_START42_V11-v2/AODSIM"},
  {0,  7,  0,  0,  0,  0,  108360,  1.250000*1.939e-01, "ZJetToMuMu",  0,  "ZJetToMuMu_Pt-230to300_TuneZ2_7TeV_pythia6_Summer11-PU_S3_START42_V11-v2_AODSIM.root",  "START42_V11::All",  "GRun",  "4_2_3_patch2",  "/ZJetToMuMu_Pt-230to300_TuneZ2_7TeV_pythia6/Summer11-PU_S3_START42_V11-v2/AODSIM"},

  {0,  7,  0,  0,  0,  0,  5204108,      1.236000*7899,    "WtoENu",  0,  "WtoENu_TuneD6T_7TeV-pythia6_Summer11-PU_S3_START42_V11-v2_AODSIM.root",               "START42_V11::All",  "GRun",  "4_2_3_patch2",  "/WtoENu_TuneD6T_7TeV-pythia6/Summer11-PU_S3_START42_V11-v2/AODSIM"},

  {0,  7,  0,  0,  0,  0,  1089625,          167,     "ttbar",  0,  "TT_TuneZ2_7TeV-pythia6-tauola_Summer11-PU_S3_START42_V11-v2_AODSIM.root", "START42_V11::All",  "GRun",  "4_2_3_patch2",  "/TT_TuneZ2_7TeV-pythia6-tauola/Summer11-PU_S3_START42_V11-v2/AODSIM"},


  // Summer11 production for the Fall 2011 analysis. Directory /moscow31/heavynu/42x-bg


  {0,  7,  0,  0,  0,  0,  2589571,        3048.,          "DYToLL",  0,  "DYToLL_M-50_Sherpa_Summer11-PU_S4_START42_V11-v1_AODSIM_half.root",  "START42_V15B::All",  "hlt",  "4_2_8_patch3",  "/DYToLL_M-50_1jEnh2_2jEnh35_3jEnh40_4jEnh50_7TeV-sherpa/Summer11-PU_S4_START42_V11-v1/AODSIM"},
   //Sherpa sample, 259 jobs, 15189900 events (recalculated from report multiplying by 259/260)
   //sum of weights of all events (525 jobs) events from Joe 5249131, recalculated here to 2589571

  {0,  7,  0,  0,  0,  0,  5239132,        3048.,          "DYToLL",  0,    "DYToLL_M-50_Sherpa_Summer11-PU_S4_START42_V11-v1_AODSIM.root",  "START42_V15B::All",  "hlt",    "4_2_8_patch3",  "/DYToLL_M-50_1jEnh2_2jEnh35_3jEnh40_4jEnh50_7TeV-sherpa/Summer11-PU_S4_START42_V11-v1/AODSIM"},
   //Sherpa sample, almost full, 524 jobs, 30785848 events (recalculated from report multiplying by 524/525)   
   //sum of weights of all events (525 jobs) events from Joe 5249131, recalculated here to 5239132

  {0,  7,  0,  0,  0,  0,  5238920,        3048.,          "DYToLL",  0,    "DYToLL_M-50_Sherpa_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept.root",    "START42_V15B::All",  "hlt",    "4_2_8_patch3",  "/DYToLL_M-50_1jEnh2_2jEnh35_3jEnh40_4jEnh50_7TeV-sherpa/Summer11-PU_S4_START42_V11-v1/AODSIM"},
   //Sherpa sample, 1 lepton required, 524 jobs, 30784600 events from total of 30844600
   //sum of weights of all events (525 jobs) events from Joe 5249131, recalculated here to 5238920

  {0,  7,  0,  0,  0,  0,  5198077,        3048.,          "DYToLL",  0,    "DYToLL_M-50_Sherpa_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept_1jet.root",    "START42_V15B::All",    "hlt",    "4_2_8_patch3",  "/DYToLL_M-50_1jEnh2_2jEnh35_3jEnh40_4jEnh50_7TeV-sherpa/Summer11-PU_S4_START42_V11-v1/AODSIM"},
   //Sherpa sample, 1 lepton and 1 jet required, 525 jobs, 6 jobs crashed, 30544600 events from total of 30844600
   //sum of weights of all events (525 jobs) events from Joe 5249131, recalculated here to 5198077

  {0,  7,  0,  0,  0,  0,  10339374,   154.*0.125,    "TTTo2L2Nu2B", 0, "TTTo2L2Nu2B_7TeV-powheg-pythia6_Summer11-PU_S4_START42_V11-v1_AODSIM.root",  "START42_V15B::All",  "hlt",  "4_2_8_patch7",  "/TTTo2L2Nu2B_7TeV-powheg-pythia6/Summer11-PU_S4_START42_V11-v1/AODSIM"},

  {0,  7,  0,  0,  0,  0,  10339374,   154.*0.125,    "TTTo2L2Nu2B", 0, "TTTo2L2Nu2B_7TeV-powheg-pythia6_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept.root",    "START42_V15B::All",  "hlt",  "4_2_8_patch7",  "/TTTo2L2Nu2B_7TeV-powheg-pythia6/Summer11-PU_S4_START42_V11-v1/AODSIM"},

  {0,  7,  0,  0,  0,  0,  54342070,       31314.,  "WJetsToLNu",  0, "WJetsToLNu_TuneZ2_7TeV-madgraph-tauola_Summer11-PU_S4_START42_V11-v1_AODSIM.root",  "START42_V15B::All",   "hlt",  "4_2_8_patch7",  "/WJetsToLNu_TuneZ2_7TeV-madgraph-tauola/Summer11-PU_S4_START42_V11-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  78486477,       31314.,  "WJetsToLNu",  0, "WJetsToLNu_TuneZ2_7TeV-madgraph-tauola_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept_1jet.root",  "START42_V15B::All",      "hlt",  "4_2_8_patch7",  "/WJetsToLNu_TuneZ2_7TeV-madgraph-tauola/Summer11-PU_S4_START42_V11-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  28835480,       31314.,  "WJetsToLNu",  0, "WJetsToLNu_TuneZ2_7TeV-madgraph-tauola_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept.root",  "START42_V15B::All",            "hlt",  "4_2_8_patch7",  "/WJetsToLNu_TuneZ2_7TeV-madgraph-tauola/Summer11-PU_S4_START42_V11-v1/AODSIM"},

  {0,  7,  0,  0,  0,  0, 12730972,        25690.,   "GJets",  0,  "GJets_TuneZ2_40_HT_100_7TeV-madgraph_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept_1jet.root",  "START42_V15B::All",  "hlt",  "4_2_8_patch7",  "/GJets_TuneZ2_40_HT_100_7TeV-madgraph/Summer11-PU_S4_START42_V11-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  1536287,        5213.,    "GJets",  0,  "GJets_TuneZ2_100_HT_200_7TeV-madgraph_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept_1jet.root",  "START42_V15B::All",  "hlt", "4_2_8_patch7",  "/GJets_TuneZ2_100_HT_200_7TeV-madgraph/Summer11-PU_S4_START42_V11-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  9377170,        798.3,    "GJets",  0,  "GJets_TuneZ2_200_HT_inf_7TeV-madgraph_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept_1jet.root",  "START42_V15B::All",  "hlt", "4_2_8_patch7",  "/GJets_TuneZ2_200_HT_inf_7TeV-madgraph/Summer11-PU_S4_START42_V11-v1/AODSIM"},

  {0,  7,  0,  0,  0,  0,  3925916,          43.,       "WW",  0,  "WW_TuneZ2_7TeV_pythia6_tauola_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept.root", "START42_V15B::All",  "hlt",  "4_2_8_patch7",  "/WW_TuneZ2_7TeV_pythia6_tauola/Summer11-PU_S4_START42_V11-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  4265243,         18.2,       "WZ",  0,  "WZ_TuneZ2_7TeV_pythia6_tauola_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept.root",  "START42_V15B::All",  "hlt",  "4_2_8_patch7",  "/WZ_TuneZ2_7TeV_pythia6_tauola/Summer11-PU_S4_START42_V11-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,  2482785,          5.9,       "ZZ",  0,  "ZZ_TuneZ2_7TeV_pythia6_tauola_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept.root",  "START42_V15B::All",  "hlt",  "4_2_8_patch7",  "/ZZ_TuneZ2_7TeV_pythia6_tauola/Summer11-PU_S4_START42_V11-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,   814390,        7.466,       "tW",  0,  "T_TuneZ2_tW-channel-DR_7TeV-powheg-tauola_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept_1jet.root",  "START42_V15B::All",  "hlt",  "4_2_8_patch7",  "/T_TuneZ2_tW-channel-DR_7TeV-powheg-tauola/Summer11-PU_S4_START42_V11-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,   809984,        7.46,        "tW",  0,  "Tbar_TuneZ2_tW-channel-DR_7TeV-powheg-tauola_Summer11-PU_S4_START42_V11-v1_AODSIM_1lept_1jet.root","START42_V15B::All", "hlt",  "4_2_8_patch7",  "/Tbar_TuneZ2_tW-channel-DR_7TeV-powheg-tauolaSummer11-PU_S4_START42_V11-v1AODSIM"},


 // Summer11 production for the Fall 2011 analysis. HEEP 3.1  Directory /moscow31/heavynu/42x-bg

  {0,  7,  0,  0,  0,  0,  16439970,   154.,    "TTbar", 0, "TT_TuneZ2_7TeV-powheg-tauola-Fall11.root",  "START42_V17::All",  "hlt",  "4_2_8_patch7",  "/TT_TuneZ2_7TeV-powheg-tauola/Fall11-PU_S6_START42_V14B-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,   5249131,  3048.,   "DYToLL", 0, "DYToLL_M-50_1jEnh2_2jEnh35_3jEnh40_4jEnh50_7TeV-sherpa-Fall11.root", "START42_V15B::All",  "hlt",    "4_2_8_patch7",  "/DYToLL_M-50_1jEnh2_2jEnh35_3jEnh40_4jEnh50_7TeV-sherpa/Fall11-PU_S6_START42_V14B-v1/AODSIM"},
 
 
 
 // Summer11 production for the Fall 2011 analysis. HEEP 3.2 Directory /moscow41/heavynu/42x-bg-Fall11
 
  {0,  7,  0,  0,  0,  0,  16439970,   154.,    "TTbar", 0, "TT_TuneZ2_7TeV-powheg-tauola-Fall11-HEEP32.root",  "START42_V17::All",  "hlt",  "4_2_8_patch7",  "/TT_TuneZ2_7TeV-powheg-tauola/Fall11-PU_S6_START42_V14B-v1/AODSIM"},
  {0,  7,  0,  0,  0,  0,   5249131,  3048.,   "DYToLL", 0, "DYToLL_M-50_1jEnh2_2jEnh35_3jEnh40_4jEnh50_7TeV-sherpa-Fall11-HEEP32.root", "START42_V15B::All",  "hlt",    "4_2_8_patch7",  "/DYToLL_M-50_1jEnh2_2jEnh35_3jEnh40_4jEnh50_7TeV-sherpa/Fall11-PU_S6_START42_V14B-v1/AODSIM"},


 // Summer12 production for the analysis of 2012 data. HEEP 4.2. Directory /moscow41/heavynu/BG2012

  {0,  8,  0,  0,  0,  0,  30459503, 3503.71,  "DYToLL", 0, "DYJetsToLL_M-50_TuneZ2Star_8TeV-madgraph-tarball.root", "START53_V15::All",  "hlt",    "5_3_7",  "/DYJetsToLL_M-50_TuneZ2Star_8TeV-madgraph-tarball/Summer12_DR53X-PU_S10_START53_V7A-v1/AODSIM"},

  //DY sample with correct matching of leptons to MC
  {0,  8,  0,  0,  0,  0,  25479503, 3503.71,  "DYToLL", 0, "DYJetsToLL_M-50_TuneZ2Star_8TeV-madgraph-tarball_minus151.root", "START53_V15::All",    "hlt",    "5_3_7",  "/DYJetsToLL_M-50_TuneZ2Star_8TeV-madgraph-tarball/Summer12_DR53X-PU_S10_START53_V7A-v1/AODSIM"},

  {0,  8,  0,  0,  0,  0,  10783509, 225.197*0.125,  "TTTo2L2Nu2B", 0, "TTTo2L2Nu2B_8TeV-powheg-pythia6.root", "START53_V15::All",  "hlt",   "5_3_7",  "/TTTo2L2Nu2B_8TeV-powheg-pythia6/Summer12_DR53X-PU_S10_START53_V7A-v1/AODSIM"},

  {0,  8,  0,  0,  0,  0,  18393090, 37509.,  "WJetsToLNu", 0, "WJetsToLNu_TuneZ2Star_8TeV-madgraph-tarball.root", "START53_V15::All",  "hlt",   "5_3_7",  "/WJetsToLNu_TuneZ2Star_8TeV-madgraph-tarball/Summer12_DR53X-PU_S10_START53_V7A-v1/AODSIM"},

  {0,  8,  0,  0,  0,  0,  57589905, 37509.,  "WJetsToLNu", 0, "WJetsToLNu_TuneZ2Star_8TeV-madgraph-tarball-v2-minus3.root", "START53_V15::All",  "hlt",   "5_3_7",    "/WJetsToLNu_TuneZ2Star_8TeV-madgraph-tarball/Summer12_DR53X-PU_S10_START53_V7A-v1/AODSIM"},

  {0,  8,  0,  0,  0,  0,  9650431,  54.838,  "WW", 0, "WW_TuneZ2star_8TeV_pythia6_tauola_minus7.root", "START53_V15::All",  "hlt",   "5_3_7",    "/WW_TuneZ2star_8TeV_pythia6_tauola/Summer12_DR53X-PU_S10_START53_V7A-v1/AODSIM"},

  //LO cs is 12.63, did not find cs for this m(ll) cut, for the moment use k-factor 1.55 taken from ZZ
  {0,  8,  0,  0,  0,  0,  10000283, 19.577,  "WZ", 0, "WZ_TuneZ2star_8TeV_pythia6_tauola.root", "START53_V15::All",  "hlt",   "5_3_7",    "/WZ_TuneZ2star_8TeV_pythia6_tauola/Summer12_DR53X-PU_S10_START53_V7A-v1/AODSIM"},

  //LO cs is ~5, I put cs for NNLO cs for m(ll)>40, PDF=CTEQ (for other PDF there are numbers like 8.297, 8.258)
  {0,  8,  0,  0,  0,  0,  9799908,   8.059,  "ZZ", 0, "ZZ_TuneZ2star_8TeV_pythia6_tauola.root", "START53_V15::All",  "hlt",   "5_3_7",  "/ZZ_TuneZ2star_8TeV_pythia6_tauola/Summer12_DR53X-PU_S10_START53_V7A-v1/AODSIM"},

  {0,  8,  0,  0,  0,  0,  497658,     11.1,  "tW", 0, "T_tW-channel-DR_TuneZ2star_8TeV-powheg-tauola.root", "START53_V15::All",  "hlt",      "5_3_7",  "/T_tW-channel-DR_TuneZ2star_8TeV-powheg-tauola/Summer12_DR53X-PU_S10_START53_V7A-v1/AODSIM"},

  {0,  8,  0,  0,  0,  0,  493460,     11.1,  "tW", 0, "Tbar_tW-channel-DR_TuneZ2star_8TeV-powheg-tauola.root", "START53_V15::All",  "hlt",   "5_3_7",  "/Tbar_tW-channel-DR_TuneZ2star_8TeV-powheg-tauola/Summer12_DR53X-PU_S10_START53_V7A-v1/AODSIM"},


 // RUN II

 // Spring15 production for the analysis of 2015 data /moscow41/heavynu/MC_2015  tbar mc norm = 14140200, emu suggests 10520000

  {0, 13,  0,  0,  0,  0, 14140200,    815.96, "TTJets", 0,  "TTJets_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8.root",  "MCRUN2_74_V9::All", "", "7_4_14",  "TTJets_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8_MINIAODSIM"},

  {0, 13,  0,  0,  0,  0, 19190800,  3*2008.4, "DYToLL", 0,  "DYJetsToLL_M-50_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8.root", "MCRUN2_74_V9::All", "", "7_4_14",  "DYJetsToLL_M-50_TuneCUETP8M1_13TeV-amcatnloFXFX-pythia8_MINIAODSIM"},

  {0, 13,  0,  0,  0,  0,  1958325,    12.178, "WWTo2L2Nu", 0,  "WWTo2L2Nu_13TeV-powheg.root", "MCRUN2_74_V9::All", "", "7_4_14",  "WWTo2L2Nu_13TeV-powheg.root"},

  {0, 13,  0,  0,  0,  0, 19000000,     5.595, "WZ", 0,  "WZTo2L2Q_13TeV_amcatnloFXFX_madspin_pythia8.root", "MCRUN2_74_V9::All",  "", "7_4_14",  "WZTo2L2Q_13TeV_amcatnloFXFX_madspin_pythia8"},

  {0, 13,  0,  0,  0,  0,  1974295,     4.43,  "WZ", 0,  "WZTo3LNu_TuneCUETP8M1_13TeV-powheg-pythia8.root", "MCRUN2_74_V9::All", "", "7_4_14",  "WZTo3LNu_TuneCUETP8M1_13TeV-powheg-pythia8"},

 // === signal 2015 central production /moscow41/heavynu/MC_2015/Signal =========================================

  {1, 13,  2000,  1000,  1,  0,  48882,  .07871,  "WR2000_nuRe1000",  0, "WRToNuEToEEJJ_MW-2000_MNu-1000_TuneCUETP8M1_13TeV-pythia8.root", "MCRUN2_74_V9::All",  "hlt",  "7_4_14",  ""},



// === signal summer11 central production =========================================
//         /moscow31/heavynu/42x-signal-central/

  {1,  7,  2000,  1100,  1,  0,  17295,  .005721,  "WR2000_nuRl1100",  0,  "WRToNuLeptonToLLJJ_MW-2000_MNu-1100_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",  "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  2000,  1100,  2,  0,  17598,  .005721,  "WR2000_nuRl1100",  0,  "WRToNuLeptonToLLJJ_MW-2000_MNu-1100_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",  "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  2500,  1300,  1,  0,  17649,  .0008297,  "WR2500_nuRl1300",  0, "WRToNuLeptonToLLJJ_MW-2500_MNu-1300_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",   "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  2500,  1300,  2,  0,  17499,  .0008297,  "WR2500_nuRl1300",  0, "WRToNuLeptonToLLJJ_MW-2500_MNu-1300_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",   "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  2400,  1300,  1,  0,  18925,  .001169,  "WR2400_nuRl1300",  0, "WRToNuLeptonToLLJJ_MW-2400_MNu-1300_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",    "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  2400,  1300,  2,  0,  19019,  .001169,  "WR2400_nuRl1300",  0, "WRToNuLeptonToLLJJ_MW-2400_MNu-1300_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",   "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  2300,  1200,  1,  0,  17507,  .001851,  "WR2300_nuRl1200",  0, "WRToNuLeptonToLLJJ_MW-2300_MNu-1200_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",       "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  2300,  1200,  2,  0,  17526,  .001851,  "WR2300_nuRl1200",  0, "WRToNuLeptonToLLJJ_MW-2300_MNu-1200_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",    "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  2200,  1200,  1,  0,  18335,  .002591,  "WR2200_nuRl1200",  0, "WRToNuLeptonToLLJJ_MW-2200_MNu-1200_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",              "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  2200,  1200,  2,  0,  18176,  .002591,  "WR2200_nuRl1200",  0, "WRToNuLeptonToLLJJ_MW-2200_MNu-1200_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",       "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  2100,  1100,  1,  0,  17896,  .004097,  "WR2100_nuRl1100",  0, "WRToNuLeptonToLLJJ_MW-2100_MNu-1100_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",                              "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  2100,  1100,  2,  0,  18015,  .004097,  "WR2100_nuRl1100",  0, "WRToNuLeptonToLLJJ_MW-2100_MNu-1100_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",                              "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  1900,  1000,  1,  0,  15571,  .009057,  "WR1900_nuRl1000",  0, "WRToNuLeptonToLLJJ_MW-1900_MNu-1000_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",                              "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  1900,  1000,  2,  0,  15303,  .009057,  "WR1900_nuRl1000",  0, "WRToNuLeptonToLLJJ_MW-1900_MNu-1000_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",                              "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  1800,  1000,  1,  0,  17588,  .01255,   "WR1800_nuRl1000",  0, "WRToNuLeptonToLLJJ_MW-1800_MNu-1000_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",                              "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  1800,  1000,  2,  0,  17475,  .01255,   "WR1800_nuRl1000",  0, "WRToNuLeptonToLLJJ_MW-1800_MNu-1000_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",                              "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  1700,   900,  1,  0,  18081,  .02024,   "WR1700_nuRl900",  0, "WRToNuLeptonToLLJJ_MW-1700_MNu-900_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",                              "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  1700,   900,  2,  0,  18058,  .02024,   "WR1700_nuRl900",  0, "WRToNuLeptonToLLJJ_MW-1700_MNu-900_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",                              "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  1600,   900,  1,  0,  18405,   .0278,   "WR1600_nuRl900",  0, "WRToNuLeptonToLLJJ_MW-1600_MNu-900_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",                                          "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  1600,   900,  2,  0,  18113,   .0278,   "WR1600_nuRl900",  0, "WRToNuLeptonToLLJJ_MW-1600_MNu-900_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",                              "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  1500,   800,  1,  0,  16833,   .04567,   "WR1500_nuRl800",  0, "WRToNuLeptonToLLJJ_MW-1500_MNu-800_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",                                                      "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  1500,   800,  2,  0,  16526,   .04567,   "WR1500_nuRl800",  0, "WRToNuLeptonToLLJJ_MW-1500_MNu-800_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",                                          "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  1400,   800,  1,  0,  18421,   .06318,   "WR1400_nuRl800",  0, "WRToNuLeptonToLLJJ_MW-1400_MNu-800_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",                                          "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  1400,   800,  2,  0,  18452,   .06318,   "WR1400_nuRl800",  0, "WRToNuLeptonToLLJJ_MW-1400_MNu-800_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",                                          "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  1300,   700,  1,  0,  17501,   .107,     "WR1300_nuRl700",  0, "WRToNuLeptonToLLJJ_MW-1300_MNu-700_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",                                                     "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  1300,   700,  2,  0,  17365,   .107,     "WR1300_nuRl700",  0, "WRToNuLeptonToLLJJ_MW-1300_MNu-700_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",                                          "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  1200,   700,  1,  0,  18253,   .1463,    "WR1200_nuRl700",  0, "WRToNuLeptonToLLJJ_MW-1200_MNu-700_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",                                                                "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  1200,   700,  2,  0,  18310,   .1463,    "WR1200_nuRl700",  0, "WRToNuLeptonToLLJJ_MW-1200_MNu-700_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",                                                     "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  1100,   600,  1,  0,  17827,   .2605,    "WR1100_nuRl600",  0, "WRToNuLeptonToLLJJ_MW-1100_MNu-600_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",                                                                           "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  1100,   600,  2,  0,  18127,   .2605,    "WR1100_nuRl600",  0, "WRToNuLeptonToLLJJ_MW-1100_MNu-600_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",                                                                "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  1000,   600,  1,  0,  18161,   .3602,    "WR1000_nuRl600",  0, "WRToNuLeptonToLLJJ_MW-1000_MNu-600_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",                                                                                      "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

  {1,  7,  1000,   600,  2,  0,  18151,   .3602,    "WR1000_nuRl600",  0, "WRToNuLeptonToLLJJ_MW-1000_MNu-600_TuneZ2_7TeV-pythia6-tauola_AODSIM_PU_S4_START42_V11-v1.root",                                                                                      "START42_V15B::All",  "hlt",  "4_2_8_patch7",  ""},

// === private background samples ================================================================================================================ 	 
// type  ECM  MW  MNu  channel  lumi   stat      CS        name     pid    fname                              AlCa               HLTmenu   CMSSW   DBS 	 
  // based on Zjets_pT20_MZ180 configuration from heavynu_cfi.py 	 
  {0,     7,  0,  0,   0,       0,    83650,    0.9277,   "Z+jets",   0,    "Zjets_pT20_MZ180.root",          "START36_V10::All",  "8E29",   "3_6_3_patch2", ""},
  // Private production, see directory /Alpgen , electron and muon decay modes
  {0,     7,  0,  0,   0,       0,     4000, 1.250000*2.*0.1644,   "Z+jets",   0,    "ZjetsAlpgen_Mll180.root",        "START38_V12::All",  "1E31",   "3_8_5",        ""},
  // FastSim production by Minnesota (see config in FastSim/)
  {0,     7,  0,  0,   0,       0,  9900000,     157.5,    "ttbar",   0,    "38x-ttbar-tauola-fastsim.root",  "MC_38Y_V10::All",   "1E31",   "3_8_2",        ""},
  
  // Z+jets samples with ISR/FSR variations for systematics study:
  {0,  7,  0,  0,  0,  0,  5000,  9.6420e+03,  "Z+2jets largerISRFSR",  0,  "largerISRFSR_2Electrons.root",  "START38_V14::All",  "",  "CMSSW_3_8_7_patch2",  ""},
  {0,  7,  0,  0,  0,  0,  5000,  9.6420e+03,  "Z+2jets largerISRFSR",  0,  "largerISRFSR_2Muons.root",  "START38_V14::All",  "",  "CMSSW_3_8_7_patch2",  ""},
  {0,  7,  0,  0,  0,  0,  4500,  9.6420e+03,  "Z+2jets smallerISRFSR",  0, "smallerISRFSR_2Electrons.root",  "START38_V14::All",  "",  "CMSSW_3_8_7_patch2",  ""},
  {0,  7,  0,  0,  0,  0,  5000,  9.6420e+03,  "Z+2jets smallerISRFSR", 0,  "smallerISRFSR_2Muons.root",  "START38_V14::All",  "",  "CMSSW_3_8_7_patch2",  ""},
  {0,  7,  0,  0,  0,  0,  5000,  9.6420e+03,  "Z+2jets defaultISRFSR",  0,  "defaultISRFSR_2Electrons.root",  "START38_V14::All",  "",  "CMSSW_3_8_7_patch2",  ""},
  {0,  7,  0,  0,  0,  0,  5000,  9.6420e+03,  "Z+2jets defaultISRFSR",  0,  "defaultISRFSR_2Muons.root",  "START38_V14::All",  "",  "CMSSW_3_8_7_patch2",  ""},
  
  // Z+2jets (Z/gamma -> e+ e-), crossection taken from alpgen z2jee_unw.par file
  // with Pile-Up "Flat to 10 plus tail" scenario
  {0,  7,  0,  0,  0,  0, 56000,  1.250000*0.1649,  "Z+jets",  0,  "416-START41_V0-PU-alpgen-z2jee.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  // same, Z/gamma -> mu mu
  {0,  7,  0,  0,  0,  0, 55000,  1.250000*0.1649,  "Z+jets",  0,  "416-START41_V0-PU-alpgen-z2jmumu.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  

// === signal =====================================================================================================================================================================
// type  ECM   MW    MNu  channel  lumi   stat     CS       name                 pid    fname                              AlCa                HLTmenu  CMSSW          DBS
// --- 14 TeV ---
  // Software: gen, sim 1_4_12; hlt, reco 1_6_12; susyanalysis 1_3_0 @ 1_6_12
  // Cross section: calculated with pythia on 50 000 events.
  // Jet corrections: MCJetCorJetIcone5
  // strict isolation conditions for electron and muons (0.3 instead of default 0.1)
  // muons = tracks from tracker only
  // electrons = track from gsfTrack
  {1,    14,  1200,  500,  1,       0,    3000,  1.180,  "WR1200_nuRe500",      3,   "new-nu_Re=500-WR=1200-topDiLepton2Electron.root",  "CSA07_100pb",  "1E32",  "1_6_12",       ""},
  {1,    14,  2000,  500,  1,       0,    3000,  0.148,  "WR2000_nuRe500",      6,   "new-nu_Re=500-WR=2000-topDiLepton2Electron.root",  "CSA07_100pb",  "1E32",  "1_6_12",       ""},
  {1,    14,  1500,  600,  1,       0,    3000,  0.470,  "WR1500_nuRe600",      5,   "new-nu_Re=600-WR=1500-topDiLepton2Electron.root",  "CSA07_100pb",  "1E32",  "1_6_12",       ""},
  
  {1,    14,  1200,  500,  2,       0,    3000,  1.186,  "WR1200_nuRmu500",   103,   "new-nu_Rmu=500-WR=1200-topDiLeptonMuonX.root",     "CSA07_100pb",  "1E32",  "1_6_12",       ""},
  {1,    14,  2000,  500,  2,       0,    3750,  0.148,  "WR2000_nuRmu500",   106,   "new-nu_Rmu=500-WR=2000-topDiLeptonMuonX.root",     "CSA07_100pb",  "1E32",  "1_6_12",       ""},
  {1,    14,  1500,  600,  2,       0,    3000,  0.473,  "WR1500_nuRmu600",   105,   "new-nu_Rmu=600-WR=1500-topDiLeptonMuonX.root",     "CSA07_100pb",  "1E32",  "1_6_12",       ""},
  
// --- 10 TeV ---
//nuRe
  {1,    10,  1000,  300,  1,       0,    1500,   1.544,  "WR1000_nuRe300",      1,   "WR1000_nuRe300.root",             "STARTUP_V10::All",  "2E30",  "2_2_8",       ""},
  {1,    10,  1200,  300,  1,       0,    1500,   0.7292,  "WR1200_nuRe300",      2,   "WR1200_nuRe300.root",             "STARTUP_V10::All",  "2E30",  "2_2_8",       ""},
  {1,    10,  1200,  500,  1,       0,    1500,   0.5935,  "WR1200_nuRe500",      3,   "WR1200_nuRe500.root",             "STARTUP_V10::All",  "2E30",  "2_2_8",       ""},
  {1,    10,  1200,  700,  1,       0,    1500,   0.4103,  "WR1200_nuRe700",      4,   "WR1200_nuRe700.root",             "STARTUP_V10::All",  "2E30",  "2_2_8",       ""},
  {1,    10,  1500,  600,  1,       0,    1500,   0.2069,  "WR1500_nuRe600",      5,   "WR1500_nuRe600.root",             "STARTUP_V10::All",  "2E30",  "2_2_8",       ""},
  {1,    10,  1500,  600,  1,       0,    1500,   0.2069,  "WR1500_nuRe600",      5,   "WR1500_nuRe600_HLT_1E31.root",    "STARTUP_V10::All",  "1E31",  "2_2_6_HLT",   ""},
  
  {1,    10,  1500,  250,  1,       0,    1500,   0.2651,  "WR1500_nuRe250",      7,   "WR1500_nuRe250_1e31_pat2.root",   "STARTUP_V13::All",  "1E31",  "2_2_13_HLT",  ""},
  {1,    10,  1500,  450,  1,       0,    1500,   0.2360,  "WR1500_nuRe450",      8,   "WR1500_nuRe450_1e31_pat2.root",   "STARTUP_V13::All",  "1E31",  "2_2_13_HLT",  ""},
  {1,    10,  1500,  600,  1,       0,    1500,   0.2069,  "WR1500_nuRe600",      5,   "WR1500_nuRe600_1e31_pat2.root",   "STARTUP_V13::All",  "1E31",  "2_2_13_HLT",  ""},
  {1,    10,  1500,  900,  1,       0,    1500,   0.1332,  "WR1500_nuRe900",      9,   "WR1500_nuRe900_1e31_pat2.root",   "STARTUP_V13::All",  "1E31",  "2_2_13_HLT",  ""},
  {1,    10,  1500, 1200,  1,       0,    1500,   0.04734,  "WR1500_nuRe1200",    10,   "WR1500_nuRe1200_1e31_pat2.root",  "STARTUP_V13::All",  "1E31",  "2_2_13_HLT",  ""},
  
//nuRmu
  {1,    10,  1200,  500,  2,       0,    1500,   0.5907,  "WR1200_nuRmu500",   103,   "WR1200_nuRmu500.root",            "STARTUP_V10::All",  "2E30",  "2_2_8",       ""},
  {1,    10,  1500,  600,  2,       0,    1500,   0.2056,  "WR1500_nuRmu600",   105,   "WR1500_nuRmu600.root",            "STARTUP_V10::All",  "2E30",  "2_2_8",       ""},
  {1,    10,  1500,  600,  2,       0,    1500,   0.2056,  "WR1500_nuRmu600",   105,   "WR1500_nuRmu600_HLT_1E31.root",   "STARTUP_V10::All",  "1E31",  "2_2_6_HLT",   ""},
  {1,    10,  2000,  500,  2,       0,    1500,   0.05101,  "WR2000_nuRmu500",   106,   "WR2000_nuRmu500_HLT_1E31.root",   "STARTUP_V10::All",  "1E31",  "2_2_6_HLT",   ""},
  
//nuRe
  {1,    10,  1000,  300,  1,       0,    1500,   1.555,  "WR1000_nuRe300",      1,   "31X-WR1000_nuRe300.root",         "STARTUP31X_V4::All",  "8E29",  "3_1_5",  ""},
  {1,    10,  1200,  300,  1,       0,    1500,   0.7230,  "WR1200_nuRe300",      2,   "31X-WR1200_nuRe300.root",         "STARTUP31X_V4::All",  "8E29",  "3_1_5",  ""},
  {1,    10,  1200,  500,  1,       0,    1500,   0.5938,  "WR1200_nuRe500",      3,   "31X-WR1200_nuRe500.root",         "STARTUP31X_V4::All",  "8E29",  "3_1_5",  ""},
  {1,    10,  1200,  700,  1,       0,    1500,   0.4105,  "WR1200_nuRe700",      4,   "31X-WR1200_nuRe700.root",         "STARTUP31X_V4::All",  "8E29",  "3_1_5",  ""},
  {1,    10,  1500,  600,  1,       0,    1500,   0.2076,  "WR1500_nuRe600",      5,   "31X-WR1500_nuRe600.root",         "STARTUP31X_V4::All",  "8E29",  "3_1_5",  ""},
  {1,    10,  1500,  1200, 1,       0,    1500,   0.04759, "WR1500_nuRe1200",    10,   "31X-WR1500_nuRe1200.root",        "STARTUP31X_V4::All",  "8E29",  "3_1_5",  ""},
  {1,    10,  1500,  900,  1,       0,    1500,   0.1327,  "WR1500_nuRe900",      9,   "31X-WR1500_nuRe900.root",         "STARTUP31X_V4::All",  "8E29",  "3_1_5",  ""},
  {1,    10,  1500,  450,  1,       0,    1500,   0.2352,  "WR1500_nuRe450",      8,   "31X-WR1500_nuRe450.root",         "STARTUP31X_V4::All",  "8E29",  "3_1_5",  ""},
  {1,    10,  1500,  250,  1,       0,    1500,   0.2653,  "WR1500_nuRe250",      7,   "31X-WR1500_nuRe250.root",         "STARTUP31X_V4::All",  "8E29",  "3_1_5",  ""},
  
//nuRmu
  {1,    10,  1200,  500,  2,       0,    1500,   0.5926,  "WR1200_nuRmu500",   103,   "31X-WR1200_nuRmu500.root",        "STARTUP31X_V4::All",  "8E29",  "3_1_5",  ""},
  {1,    10,  1500,  600,  2,       0,    1500,   0.2082,  "WR1500_nuRmu600",   105,   "31X-WR1500_nuRmu600.root",        "STARTUP31X_V4::All",  "8E29",  "3_1_5",  ""},
  {1,    10,  2000,  500,  2,       0,    1500,   0.05115, "WR2000_nuRmu500",   106,   "31X-WR2000_nuRmu500.root",        "STARTUP31X_V4::All",  "8E29",  "3_1_5",  ""},
  
// --- 7 TeV ---
  // CMSSW 3_1_x
  {1,    7,  1200,  300,  1,       0,    2000,    0.2635,  "WR1200_nuRe300",      2,  "7TeV-31x-8e29-WR1200_nuRe300.root",   "STARTUP31X_V4::All",  "8E29",  "3_1_4",  ""},
  {1,    7,  1200,  500,  1,       0,    2000,    0.2123,  "WR1200_nuRe500",      3,  "7TeV-31x-8e29-WR1200_nuRe500.root",   "STARTUP31X_V4::All",  "8E29",  "3_1_4",  ""},
  {1,    7,  1200,  700,  1,       0,    2000,    0.1480,  "WR1200_nuRe700",      4,  "7TeV-31x-8e29-WR1200_nuRe700.root",   "STARTUP31X_V4::All",  "8E29",  "3_1_4",  ""},
  {1,    7,  1500,  250,  1,       0,    2000,    0.07862, "WR1500_nuRe250",      7,  "7TeV-31x-8e29-WR1500_nuRe250.root",   "STARTUP31X_V4::All",  "8E29",  "3_1_4",  ""},
  {1,    7,  1500,  450,  1,       0,    2000,    0.0688,  "WR1500_nuRe450",      8,  "7TeV-31x-8e29-WR1500_nuRe450.root",   "STARTUP31X_V4::All",  "8E29",  "3_1_4",  ""},
  {1,    7,  1500,  600,  1,       0,    2000,    0.05952, "WR1500_nuRe600",      5,  "7TeV-31x-8e29-WR1500_nuRe600.root",   "STARTUP31X_V4::All",  "8E29",  "3_1_4",  ""},
  {1,    7,  1500,  900,  1,       0,    2000,    0.03786, "WR1500_nuRe900",      9,  "7TeV-31x-8e29-WR1500_nuRe900.root",   "STARTUP31X_V4::All",  "8E29",  "3_1_4",  ""},
  {1,    7,  1500,  1200, 1,       0,    2000,    0.01349, "WR1500_nuRe1200",    10,  "7TeV-31x-8e29-WR1500_nuRe1200.root",  "STARTUP31X_V4::All",  "8E29",  "3_1_4",  ""},
  
  {1,    7,  1200,  300,  2,       0,    2000,    0.2648,  "WR1200_nuRmu300",   102,  "7TeV-31x-8e29-WR1200_nuRmu300.root",  "STARTUP31X_V4::All",  "8E29",  "3_1_4",  ""},
  {1,    7,  1200,  500,  2,       0,    2000,    0.2145,  "WR1200_nuRmu500",   103,  "7TeV-31x-8e29-WR1200_nuRmu500.root",  "STARTUP31X_V4::All",  "8E29",  "3_1_4",  ""},
  {1,    7,  1200,  700,  2,       0,    2000,    0.1453,  "WR1200_nuRmu700",   104,  "7TeV-31x-8e29-WR1200_nuRmu700.root",  "STARTUP31X_V4::All",  "8E29",  "3_1_4",  ""},
  {1,    7,  1500,  250,  2,       0,    2000,    0.07752, "WR1500_nuRmu250",   107,  "7TeV-31x-8e29-WR1500_nuRmu250.root",  "STARTUP31X_V4::All",  "8E29",  "3_1_4",  ""},
  {1,    7,  1500,  450,  2,       0,    2000,    0.06827, "WR1500_nuRmu450",   108,  "7TeV-31x-8e29-WR1500_nuRmu450.root",  "STARTUP31X_V4::All",  "8E29",  "3_1_4",  ""},
  {1,    7,  1500,  600,  2,       0,    2000,    0.05989, "WR1500_nuRmu600",   105,  "7TeV-31x-8e29-WR1500_nuRmu600.root",  "STARTUP31X_V4::All",  "8E29",  "3_1_4",  ""},
  {1,    7,  1500,  900,  2,       0,    2000,    0.03775, "WR1500_nuRmu900",   109,  "7TeV-31x-8e29-WR1500_nuRmu900.root",  "STARTUP31X_V4::All",  "8E29",  "3_1_4",  ""},
  {1,    7,  1500,  1200, 2,       0,    2000,    0.01337, "WR1500_nuRmu1200",  110,  "7TeV-31x-8e29-WR1500_nuRmu1200.root", "STARTUP31X_V4::All",  "8E29",  "3_1_4",  ""},
  
  
  // CMSSW 3_6_x
  {1,    7,  1000,  150,  1,       0,    3000,    0.725,  "WR1000_nuRe150",      0,  "7TeV-36x-8e29-WR1000_nuRe150.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1000,  300,  1,       0,    3000,    0.638,  "WR1000_nuRe300",      0,  "7TeV-36x-8e29-WR1000_nuRe300.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1000,  800,  1,       0,    3000,    0.130,  "WR1000_nuRe800",      0,  "7TeV-36x-8e29-WR1000_nuRe800.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1000,  900,  1,       0,    3000,    0.041,  "WR1000_nuRe900",      0,  "7TeV-36x-8e29-WR1000_nuRe900.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1100,  200,  1,       0,    3000,    0.436,  "WR1100_nuRe200",      0,  "7TeV-36x-8e29-WR1100_nuRe200.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1100,  800,  1,       0,    3000,    0.129,  "WR1100_nuRe800",      0,  "7TeV-36x-8e29-WR1100_nuRe800.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1200,  200,  1,       0,    3000,    0.282,  "WR1200_nuRe200",      0,  "7TeV-36x-8e29-WR1200_nuRe200.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1200,  300,  1,       0,    3000,    0.2654,  "WR1200_nuRe300",     0,  "7TeV-36x-8e29-WR1200_nuRe300.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1200,  500,  1,       0,    3000,    0.2144,  "WR1200_nuRe500",     0,  "7TeV-36x-8e29-WR1200_nuRe500.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1200,  700,  1,       0,    3000,    0.1452,  "WR1200_nuRe700",     0,  "7TeV-36x-8e29-WR1200_nuRe700.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1300,  300,  1,       0,    3000,    0.173,  "WR1300_nuRe300",      0,  "7TeV-36x-8e29-WR1300_nuRe300.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1300,  600,  1,       0,    3000,    0.127,  "WR1300_nuRe600",      0,  "7TeV-36x-8e29-WR1200_nuRe600.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1300,  700,  1,       0,    3000,    0.107,  "WR1300_nuRe700",      0,  "7TeV-36x-8e29-WR1200_nuRe700.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1400,  400,  1,       0,    3000,    0.106,  "WR1400_nuRe400",      0,  "7TeV-36x-8e29-WR1400_nuRe400.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1400,  600,  1,       0,    3000,    0.088,  "WR1400_nuRe600",      0,  "7TeV-36x-8e29-WR1400_nuRe600.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1500,  250,  1,       0,    3000,    0.07795, "WR1500_nuRe250",     0,  "7TeV-36x-8e29-WR1500_nuRe250.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1500,  450,  1,       0,    3000,    0.06883, "WR1500_nuRe450",     0,  "7TeV-36x-8e29-WR1500_nuRe450.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1500,  500,  1,       0,    3000,    0.0666,  "WR1500_nuRe500",     0,  "7TeV-36x-8e29-WR1500_nuRe500.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1500,  600,  1,       0,    3000,    0.05993, "WR1500_nuRe600",     0,  "7TeV-36x-8e29-WR1500_nuRe600.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1500,  900,  1,       0,    3000,    0.03783, "WR1500_nuRe900",     0,  "7TeV-36x-8e29-WR1500_nuRe900.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1500,  1200, 1,       0,    3000,    0.01346, "WR1500_nuRe1200",    0,  "7TeV-36x-8e29-WR1500_nuRe1200.root",  "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  2000,  500,  1,       0,    3000,    0.00998,  "WR2000_nuRe500",    0,  "7TeV-36x-8e29-WR2000_nuRe500.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  
  {1,    7,  1000,  150,  2,       0,    3000,    0.724,  "WR1000_nuRmu150",     0,  "7TeV-36x-8e29-WR1000_nuRmu150.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1000,  300,  2,       0,    3000,    0.639,  "WR1000_nuRmu300",     0,  "7TeV-36x-8e29-WR1000_nuRmu300.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1000,  800,  2,       0,    3000,    0.131,  "WR1000_nuRmu800",     0,  "7TeV-36x-8e29-WR1000_nuRmu800.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1000,  900,  2,       0,    3000,    0.042,  "WR1000_nuRmu900",     0,  "7TeV-36x-8e29-WR1000_nuRmu900.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1100,  200,  2,       0,    3000,    0.436,  "WR1100_nuRmu200",     0,  "7TeV-36x-8e29-WR1100_nuRmu200.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1100,  800,  2,       0,    3000,    0.129,  "WR1100_nuRmu800",     0,  "7TeV-36x-8e29-WR1100_nuRmu800.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1200,  200,  2,       0,    3000,    0.282,  "WR1200_nuRmu200",     0,  "7TeV-36x-8e29-WR1200_nuRmu200.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1200,  300,  2,       0,    3000,    0.2636,  "WR1200_nuRmu300",    0,  "7TeV-36x-8e29-WR1200_nuRmu300.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1200,  500,  2,       0,    3000,    0.2163,  "WR1200_nuRmu500",    0,  "7TeV-36x-8e29-WR1200_nuRmu500.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1200,  700,  2,       0,    3000,    0.1461,  "WR1200_nuRmu700",    0,  "7TeV-36x-8e29-WR1200_nuRmu700.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1300,  300,  2,       0,    3000,    0.172,  "WR1300_nuRmu300",     0,  "7TeV-36x-8e29-WR1200_nuRmu300.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1300,  600,  2,       0,    3000,    0.128,  "WR1300_nuRmu600",     0,  "7TeV-36x-8e29-WR1200_nuRmu600.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1300,  700,  2,       0,    3000,    0.106,  "WR1300_nuRmu700",     0,  "7TeV-36x-8e29-WR1200_nuRmu700.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1400,  400,  2,       0,    3000,    0.106,  "WR1400_nuRmu400",     0,  "7TeV-36x-8e29-WR1400_nuRmu400.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1400,  600,  2,       0,    3000,    0.087,  "WR1400_nuRmu600",     0,  "7TeV-36x-8e29-WR1400_nuRmu600.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1500,  250,  2,       0,    3000,    0.07831, "WR1500_nuRmu250",    0,  "7TeV-36x-8e29-WR1500_nuRmu250.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1500,  450,  2,       0,    3000,    0.06870, "WR1500_nuRmu450",    0,  "7TeV-36x-8e29-WR1500_nuRmu450.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1500,  500,  2,       0,    3000,    0.0665,  "WR1500_nuRmu500",    0,  "7TeV-36x-8e29-WR1500_nuRmu500.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1500,  600,  2,       0,    3000,    0.06013, "WR1500_nuRmu600",    0,  "7TeV-36x-8e29-WR1500_nuRmu600.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1500,  900,  2,       0,    3000,    0.03742, "WR1500_nuRmu900",    0,  "7TeV-36x-8e29-WR1500_nuRmu900.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  1500,  1200, 2,       0,    3000,    0.01332, "WR1500_nuRmu1200",   0,  "7TeV-36x-8e29-WR1500_nuRmu1200.root",  "START36_V10::All",  "8E29",  "3_6_3",  ""},
  {1,    7,  2000,  500,  2,       0,    3000,    0.00981,  "WR2000_nuRmu500",   0,  "7TeV-36x-8e29-WR2000_nuRmu500.root",   "START36_V10::All",  "8E29",  "3_6_3",  ""},
  
  
  // CMSSW 3_8_x
  // mu channel, 3_8_5
  {1,    7,  1000,  150,  2,       0,    4000,    0.724,  "WR1000_nuRmu150",     0,  "7TeV-38x-8e29-WR1000_nuRmu150.root",   "START38_V12::All",  "8E29",  "3_8_5",  ""},
  {1,    7,  1000,  300,  2,       0,    4000,    0.639,  "WR1000_nuRmu300",     0,  "7TeV-38x-8e29-WR1000_nuRmu300.root",   "START38_V12::All",  "8E29",  "3_8_5",  ""},
  {1,    7,  1000,  800,  2,       0,    4000,    0.131,  "WR1000_nuRmu800",     0,  "7TeV-38x-8e29-WR1000_nuRmu800.root",   "START38_V12::All",  "8E29",  "3_8_5",  ""},
  {1,    7,  1000,  900,  2,       0,    4000,    0.042,  "WR1000_nuRmu900",     0,  "7TeV-38x-8e29-WR1000_nuRmu900.root",   "START38_V12::All",  "8E29",  "3_8_5",  ""},
  {1,    7,  1100,  200,  2,       0,    4000,    0.436,  "WR1100_nuRmu200",     0,  "7TeV-38x-8e29-WR1100_nuRmu200.root",   "START38_V12::All",  "8E29",  "3_8_5",  ""},
  {1,    7,  1100,  800,  2,       0,    4000,    0.129,  "WR1100_nuRmu800",     0,  "7TeV-38x-8e29-WR1100_nuRmu800.root",   "START38_V12::All",  "8E29",  "3_8_5",  ""},
  {1,    7,  1200,  200,  2,       0,    4000,    0.282,  "WR1200_nuRmu200",     0,  "7TeV-38x-8e29-WR1200_nuRmu200.root",   "START38_V12::All",  "8E29",  "3_8_5",  ""},
  {1,    7,  1200,  300,  2,       0,    4000,    0.2636,  "WR1200_nuRmu300",    0,  "7TeV-38x-8e29-WR1200_nuRmu300.root",   "START38_V12::All",  "8E29",  "3_8_5",  ""},
  {1,    7,  1200,  500,  2,       0,    4000,    0.2163,  "WR1200_nuRmu500",    0,  "7TeV-38x-8e29-WR1200_nuRmu500.root",   "START38_V12::All",  "8E29",  "3_8_5",  ""},
  {1,    7,  1200,  700,  2,       0,    4000,    0.1461,  "WR1200_nuRmu700",    0,  "7TeV-38x-8e29-WR1200_nuRmu700.root",   "START38_V12::All",  "8E29",  "3_8_5",  ""},
  {1,    7,  1300,  300,  2,       0,    4000,    0.172,  "WR1300_nuRmu300",     0,  "7TeV-38x-8e29-WR1200_nuRmu300.root",   "START38_V12::All",  "8E29",  "3_8_5",  ""},
  {1,    7,  1300,  600,  2,       0,    4000,    0.128,  "WR1300_nuRmu600",     0,  "7TeV-38x-8e29-WR1200_nuRmu600.root",   "START38_V12::All",  "8E29",  "3_8_5",  ""},
  {1,    7,  1300,  700,  2,       0,    4000,    0.106,  "WR1300_nuRmu700",     0,  "7TeV-38x-8e29-WR1200_nuRmu700.root",   "START38_V12::All",  "8E29",  "3_8_5",  ""},
  {1,    7,  1400,  400,  2,       0,    4000,    0.106,  "WR1400_nuRmu400",     0,  "7TeV-38x-8e29-WR1400_nuRmu400.root",   "START38_V12::All",  "8E29",  "3_8_5",  ""},
  {1,    7,  1400,  600,  2,       0,    4000,    0.087,  "WR1400_nuRmu600",     0,  "7TeV-38x-8e29-WR1400_nuRmu600.root",   "START38_V12::All",  "8E29",  "3_8_5",  ""},
  {1,    7,  1500,  250,  2,       0,    4000,    0.07831, "WR1500_nuRmu250",    0,  "7TeV-38x-8e29-WR1500_nuRmu250.root",   "START38_V12::All",  "8E29",  "3_8_5",  ""},
  {1,    7,  1500,  450,  2,       0,    4000,    0.06870, "WR1500_nuRmu450",    0,  "7TeV-38x-8e29-WR1500_nuRmu450.root",   "START38_V12::All",  "8E29",  "3_8_5",  ""},
  {1,    7,  1500,  500,  2,       0,    4000,    0.0665,  "WR1500_nuRmu500",    0,  "7TeV-38x-8e29-WR1500_nuRmu500.root",   "START38_V12::All",  "8E29",  "3_8_5",  ""},
  {1,    7,  1500,  600,  2,       0,    4000,    0.06013, "WR1500_nuRmu600",    0,  "7TeV-38x-8e29-WR1500_nuRmu600.root",   "START38_V12::All",  "8E29",  "3_8_5",  ""},
  {1,    7,  1500,  900,  2,       0,    4000,    0.03742, "WR1500_nuRmu900",    0,  "7TeV-38x-8e29-WR1500_nuRmu900.root",   "START38_V12::All",  "8E29",  "3_8_5",  ""},
  {1,    7,  1500,  1200, 2,       0,    4000,    0.01332, "WR1500_nuRmu1200",   0,  "7TeV-38x-8e29-WR1500_nuRmu1200.root",  "START38_V12::All",  "8E29",  "3_8_5",  ""},
  {1,    7,  2000,  500,  2,       0,    4000,    0.00981,  "WR2000_nuRmu500",   0,  "7TeV-38x-8e29-WR2000_nuRmu500.root",   "START38_V12::All",  "8E29",  "3_8_5",  ""},
  
  // mu channel, 3_8_7_patch1
  {1,  7,  1000,  50,   2,  0,  1900,  .7539,  "WR1000_nuRmu50",  0,  "38x-7TeV-V14-WR1000_nuRmu50.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch1",  ""},
  {1,  7,  1000,  100,  2,  0,  2000,  .7446,  "WR1000_nuRmu100",  0,  "38x-7TeV-V14-WR1000_nuRmu100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch1",  ""},
  {1,  7,  1000,  200,  2,  0,  2000,  .7026,  "WR1000_nuRmu200",  0,  "38x-7TeV-V14-WR1000_nuRmu200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch1",  ""},
  {1,  7,  1000,  300,  2,  0,  2000,  .6423,  "WR1000_nuRmu300",  0,  "38x-7TeV-V14-WR1000_nuRmu300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch1",  ""},
  {1,  7,  1000,  400,  2,  0,  2000,  .5663,  "WR1000_nuRmu400",  0,  "38x-7TeV-V14-WR1000_nuRmu400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch1",  ""},
  {1,  7,  1000,  500,  2,  0,  2000,  .4672,  "WR1000_nuRmu500",  0,  "38x-7TeV-V14-WR1000_nuRmu500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch1",  ""},
  {1,  7,  1000,  600,  2,  0,  2000,  .3594,  "WR1000_nuRmu600",  0,  "38x-7TeV-V14-WR1000_nuRmu600.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch1",  ""},
  {1,  7,  1000,  700,  2,  0,  2000,  .2437,  "WR1000_nuRmu700",  0,  "38x-7TeV-V14-WR1000_nuRmu700.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch1",  ""},
  {1,  7,  1000,  800,  2,  0,  1900,  .1305,  "WR1000_nuRmu800",  0,  "38x-7TeV-V14-WR1000_nuRmu800.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch1",  ""},
  {1,  7,  1000,  900,  2,  0,  2000,  .04127,  "WR1000_nuRmu900",  0,  "38x-7TeV-V14-WR1000_nuRmu900.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch1",  ""},
  {1,  7,  1000,  950,  2,  0,  2000,  .01418,  "WR1000_nuRmu950",  0,  "38x-7TeV-V14-WR1000_nuRmu950.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch1",  ""},
  
  
  // CMSSW 3_8_7 hi-stat samples
  // Generated with START38_V12::All global tag, dumped with START38_V14::All
  {1,    7,  500,   50,  1,       0,     20000,    15.86,   "WR500_nuRe50",      0,   "7TeV-38x-8e29-V14-WR500_nuRe50.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  500,  100,  1,       0,     20000,    14.52,  "WR500_nuRe100",      0,  "7TeV-38x-8e29-V14-WR500_nuRe100.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  500,  200,  1,       0,     20000,    11.86,  "WR500_nuRe200",      0,  "7TeV-38x-8e29-V14-WR500_nuRe200.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  500,  300,  1,       0,     20000,    7.608,  "WR500_nuRe300",      0,  "7TeV-38x-8e29-V14-WR500_nuRe300.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  500,  400,  1,       0,     20000,    2.797,  "WR500_nuRe400",      0,  "7TeV-38x-8e29-V14-WR500_nuRe400.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  500,  450,  1,       0,     20000,    0.905,  "WR500_nuRe450",      0,  "7TeV-38x-8e29-V14-WR500_nuRe450.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},

  {1,    7,  600,   50,  1,       0,     10000,    7.201,   "WR600_nuRe50",      0,   "7TeV-38x-8e29-V14-WR600_nuRe50.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  600,  100,  1,       0,     10000,    7.122,  "WR600_nuRe100",      0,  "7TeV-38x-8e29-V14-WR600_nuRe100.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},  
  {1,    7,  600,  200,  1,       0,     10000,    6.138,  "WR600_nuRe200",      0,  "7TeV-38x-8e29-V14-WR600_nuRe200.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  600,  300,  1,       0,     10000,    4.666,  "WR600_nuRe300",      0,  "7TeV-38x-8e29-V14-WR600_nuRe300.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  600,  400,  1,       0,     10000,    2.848,  "WR600_nuRe400",      0,  "7TeV-38x-8e29-V14-WR600_nuRe400.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  600,  500,  1,       0,     20000,    0.987,  "WR600_nuRe500",      0,  "7TeV-38x-8e29-V14-WR600_nuRe500.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  600,  550,  1,       0,     10000,    0.316,  "WR600_nuRe550",      0,  "7TeV-38x-8e29-V14-WR600_nuRe550.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},

  {1,    7,  700,   50,  1,       0,     10000,    3.808,   "WR700_nuRe50",      0,   "7TeV-38x-8e29-V14-WR700_nuRe50.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  700,  100,  1,       0,     10000,    3.714,  "WR700_nuRe100",      0,  "7TeV-38x-8e29-V14-WR700_nuRe100.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},  
  {1,    7,  700,  200,  1,       0,     10000,    3.388,  "WR700_nuRe200",      0,  "7TeV-38x-8e29-V14-WR700_nuRe200.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  700,  300,  1,       0,     10000,    2.770,  "WR700_nuRe300",      0,  "7TeV-38x-8e29-V14-WR700_nuRe300.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  700,  400,  1,       0,     10000,    2.032,  "WR700_nuRe400",      0,  "7TeV-38x-8e29-V14-WR700_nuRe400.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  700,  500,  1,       0,     10000,    1.187,  "WR700_nuRe500",      0,  "7TeV-38x-8e29-V14-WR700_nuRe500.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  700,  600,  1,       0,     10000,    0.393,  "WR700_nuRe600",      0,  "7TeV-38x-8e29-V14-WR700_nuRe600.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  700,  650,  1,       0,     10000,    0.128,  "WR700_nuRe650",      0,  "7TeV-38x-8e29-V14-WR700_nuRe650.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
 
  {1,    7,  800,   50,  1,       0,     10000,    2.105,   "WR800_nuRe50",      0,   "7TeV-38x-8e29-V14-WR800_nuRe50.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  800,  100,  1,       0,     10000,    2.089,  "WR800_nuRe100",      0,  "7TeV-38x-8e29-V14-WR800_nuRe100.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  800,  200,  1,       0,     10000,    1.917,  "WR800_nuRe200",      0,  "7TeV-38x-8e29-V14-WR800_nuRe200.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  800,  300,  1,       0,     10000,    1.669,  "WR800_nuRe300",      0,  "7TeV-38x-8e29-V14-WR800_nuRe300.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  800,  400,  1,       0,     10000,    1.356,  "WR800_nuRe400",      0,  "7TeV-38x-8e29-V14-WR800_nuRe400.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  800,  500,  1,       0,     10000,    0.959,  "WR800_nuRe500",      0,  "7TeV-38x-8e29-V14-WR800_nuRe500.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  800,  600,  1,       0,     10000,    0.536,  "WR800_nuRe600",      0,  "7TeV-38x-8e29-V14-WR800_nuRe600.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  800,  700,  1,       0,     10000,    0.174,  "WR800_nuRe700",      0,  "7TeV-38x-8e29-V14-WR800_nuRe700.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  800,  750,  1,       0,     10000,    0.058,  "WR800_nuRe750",      0,  "7TeV-38x-8e29-V14-WR800_nuRe750.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
    
  {1,    7,  900,   50,  1,       0,     10000,    1.246,   "WR900_nuRe50",      0,   "7TeV-38x-8e29-V14-WR900_nuRe50.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  900,  100,  1,       0,     10000,    1.209,  "WR900_nuRe100",      0,  "7TeV-38x-8e29-V14-WR900_nuRe100.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  900,  200,  1,       0,     10000,    1.142,  "WR900_nuRe200",      0,  "7TeV-38x-8e29-V14-WR900_nuRe200.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  900,  300,  1,       0,     10000,    1.021,  "WR900_nuRe300",      0,  "7TeV-38x-8e29-V14-WR900_nuRe300.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  900,  400,  1,       0,     10000,    0.884,  "WR900_nuRe400",      0,  "7TeV-38x-8e29-V14-WR900_nuRe400.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  900,  500,  1,       0,     10000,    0.684,  "WR900_nuRe500",      0,  "7TeV-38x-8e29-V14-WR900_nuRe500.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  900,  600,  1,       0,     10000,    0.468,  "WR900_nuRe600",      0,  "7TeV-38x-8e29-V14-WR900_nuRe600.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  900,  700,  1,       0,     10000,    0.256,  "WR900_nuRe700",      0,  "7TeV-38x-8e29-V14-WR900_nuRe700.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  900,  800,  1,       0,     10000,    0.082,  "WR900_nuRe800",      0,  "7TeV-38x-8e29-V14-WR900_nuRe800.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  900,  850,  1,       0,     10000,    0.028,  "WR900_nuRe850",      0,  "7TeV-38x-8e29-V14-WR900_nuRe850.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
 
  {1,    7,  1000,   50,  1,       0,    10000,    0.755,   "WR1000_nuRe50",      0,   "7TeV-38x-8e29-V14-WR1000_nuRe50.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""}, 
  {1,    7,  1000,  100,  1,       0,    10000,    0.735,  "WR1000_nuRe100",      0,  "7TeV-38x-8e29-V14-WR1000_nuRe100.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1000,  200,  1,       0,    10000,    0.695,  "WR1000_nuRe200",      0,  "7TeV-38x-8e29-V14-WR1000_nuRe200.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""}, 
  {1,    7,  1000,  300,  1,       0,    10000,    0.651,  "WR1000_nuRe300",      0,  "7TeV-38x-8e29-V14-WR1000_nuRe300.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1000,  400,  1,       0,    10000,    0.564,  "WR1000_nuRe400",      0,  "7TeV-38x-8e29-V14-WR1000_nuRe400.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1000,  500,  1,       0,    10000,    0.467,  "WR1000_nuRe500",      0,  "7TeV-38x-8e29-V14-WR1000_nuRe500.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1000,  600,  1,       0,    10000,    0.357,  "WR1000_nuRe600",      0,  "7TeV-38x-8e29-V14-WR1000_nuRe600.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1000,  700,  1,       0,    10000,    0.243,  "WR1000_nuRe700",      0,  "7TeV-38x-8e29-V14-WR1000_nuRe700.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1000,  800,  1,       0,    10000,    0.105,  "WR1000_nuRe800",      0,  "7TeV-38x-8e29-V14-WR1000_nuRe800.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1000,  900,  1,       0,    10000,    0.041,  "WR1000_nuRe900",      0,  "7TeV-38x-8e29-V14-WR1000_nuRe900.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1000,  950,  1,       0,    10000,    0.014,  "WR1000_nuRe950",      0,  "7TeV-38x-8e29-V14-WR1000_nuRe950.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},

  {1,    7,  1100,   50,  1,       0,    10000,    0.476,   "WR1100_nuRe50",      0,   "7TeV-38x-8e29-V14-WR1100_nuRe50.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1100,  100,  1,       0,    10000,    0.462,  "WR1100_nuRe100",      0,  "7TeV-38x-8e29-V14-WR1100_nuRe100.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1100,  200,  1,       0,    10000,    0.436,  "WR1100_nuRe200",      0,  "7TeV-38x-8e29-V14-WR1100_nuRe200.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1100,  300,  1,       0,    10000,    0.408,  "WR1100_nuRe300",      0,  "7TeV-38x-8e29-V14-WR1100_nuRe300.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1100,  400,  1,       0,    10000,    0.365,  "WR1100_nuRe400",      0,  "7TeV-38x-8e29-V14-WR1100_nuRe400.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1100,  500,  1,       0,    10000,    0.320,  "WR1100_nuRe500",      0,  "7TeV-38x-8e29-V14-WR1100_nuRe500.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1100,  600,  1,       0,    10000,    0.261,  "WR1100_nuRe600",      0,  "7TeV-38x-8e29-V14-WR1100_nuRe600.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1100,  700,  1,       0,    10000,    0.196,  "WR1100_nuRe700",      0,  "7TeV-38x-8e29-V14-WR1100_nuRe700.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1100,  800,  1,       0,    10000,    0.132,  "WR1100_nuRe800",      0,  "7TeV-38x-8e29-V14-WR1100_nuRe800.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1100,  900,  1,       0,    10000,    0.068,  "WR1100_nuRe900",      0,  "7TeV-38x-8e29-V14-WR1100_nuRe900.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1100, 1000,  1,       0,    10000,    0.021, "WR1100_nuRe1000",      0, "7TeV-38x-8e29-V14-WR1100_nuRe1000.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1100, 1050,  1,       0,    10000,    0.0076,"WR1100_nuRe1050",      0, "7TeV-38x-8e29-V14-WR1100_nuRe1050.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
 
  {1,    7,  1200,   50,  1,       0,    10000,    0.301,   "WR1200_nuRe50",      0,   "7TeV-38x-8e29-V14-WR1200_nuRe50.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1200,  100,  1,       0,    10000,    0.294,  "WR1200_nuRe100",      0,  "7TeV-38x-8e29-V14-WR1200_nuRe100.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1200,  200,  1,       0,    10000,    0.283,  "WR1200_nuRe200",      0,  "7TeV-38x-8e29-V14-WR1200_nuRe200.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1200,  300,  1,       0,    10000,    0.263,  "WR1200_nuRe300",      0,  "7TeV-38x-8e29-V14-WR1200_nuRe300.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1200,  400,  1,       0,    10000,    0.242,  "WR1200_nuRe400",      0,  "7TeV-38x-8e29-V14-WR1200_nuRe400.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1200,  500,  1,       0,    10000,    0.217,  "WR1200_nuRe500",      0,  "7TeV-38x-8e29-V14-WR1200_nuRe500.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1200,  600,  1,       0,    10000,    0.184,  "WR1200_nuRe600",      0,  "7TeV-38x-8e29-V14-WR1200_nuRe600.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1200,  700,  1,       0,    10000,    0.146,  "WR1200_nuRe700",      0,  "7TeV-38x-8e29-V14-WR1200_nuRe700.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1200,  800,  1,       0,    10000,    0.109,  "WR1200_nuRe800",      0,  "7TeV-38x-8e29-V14-WR1200_nuRe800.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1200,  900,  1,       0,    10000,    0.071,  "WR1200_nuRe900",      0,  "7TeV-38x-8e29-V14-WR1200_nuRe900.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1200, 1000,  1,       0,    10000,    0.037, "WR1200_nuRe1000",      0, "7TeV-38x-8e29-V14-WR1200_nuRe1000.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1200, 1100,  1,       0,    10000,    0.012, "WR1200_nuRe1100",      0, "7TeV-38x-8e29-V14-WR1200_nuRe1100.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1200, 1150,  1,       0,    10000,    0.0042, "WR1200_nuRe1150",      0, "7TeV-38x-8e29-V14-WR1200_nuRe1150.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},

  {1,    7,  1300,   50,  1,       0,    10000,    0.195,   "WR1300_nuRe50",      0,   "7TeV-38x-8e29-V14-WR1300_nuRe50.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1300,  100,  1,       0,    10000,    0.189,  "WR1300_nuRe100",      0,  "7TeV-38x-8e29-V14-WR1300_nuRe100.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},  
  {1,    7,  1300,  200,  1,       0,    10000,    0.182,  "WR1300_nuRe200",      0,  "7TeV-38x-8e29-V14-WR1300_nuRe200.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1300,  300,  1,       0,    10000,    0.173,  "WR1300_nuRe300",      0,  "7TeV-38x-8e29-V14-WR1300_nuRe300.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1300,  400,  1,       0,    10000,    0.160,  "WR1300_nuRe400",      0,  "7TeV-38x-8e29-V14-WR1300_nuRe400.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1300,  500,  1,       0,    10000,    0.144,  "WR1300_nuRe500",      0,  "7TeV-38x-8e29-V14-WR1300_nuRe500.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1300,  600,  1,       0,    10000,    0.127,  "WR1300_nuRe600",      0,  "7TeV-38x-8e29-V14-WR1300_nuRe600.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1300,  700,  1,       0,    10000,    0.107,  "WR1300_nuRe700",      0,  "7TeV-38x-8e29-V14-WR1300_nuRe700.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1300,  800,  1,       0,    10000,    0.085,  "WR1300_nuRe800",      0,  "7TeV-38x-8e29-V14-WR1300_nuRe800.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1300,  900,  1,       0,    10000,    0.062,  "WR1300_nuRe900",      0,  "7TeV-38x-8e29-V14-WR1300_nuRe900.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1300, 1000,  1,       0,    10000,    0.040, "WR1300_nuRe1000",      0, "7TeV-38x-8e29-V14-WR1300_nuRe1000.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1300, 1100,  1,       0,    10000,    0.021, "WR1300_nuRe1100",      0, "7TeV-38x-8e29-V14-WR1300_nuRe1100.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1300, 1200,  1,       0,    10000,    0.0065, "WR1300_nuRe1200",      0, "7TeV-38x-8e29-V14-WR1300_nuRe1200.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1300, 1250,  1,       0,    10000,    0.0023, "WR1300_nuRe1250",      0, "7TeV-38x-8e29-V14-WR1300_nuRe1250.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},

  {1,    7,  1400,   50,  1,       0,    10000,    0.128,   "WR1400_nuRe50",      0,   "7TeV-38x-8e29-V14-WR1400_nuRe50.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1400,  100,  1,       0,    10000,    0.125,  "WR1400_nuRe100",      0,  "7TeV-38x-8e29-V14-WR1400_nuRe100.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1400,  200,  1,       0,    10000,    0.121,  "WR1400_nuRe200",      0,  "7TeV-38x-8e29-V14-WR1400_nuRe200.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1400,  300,  1,       0,    10000,    0.115,  "WR1400_nuRe300",      0,  "7TeV-38x-8e29-V14-WR1400_nuRe300.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1400,  400,  1,       0,    10000,    0.106,  "WR1400_nuRe400",      0,  "7TeV-38x-8e29-V14-WR1400_nuRe400.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1400,  500,  1,       0,    10000,    0.098,  "WR1400_nuRe500",      0,  "7TeV-38x-8e29-V14-WR1400_nuRe500.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1400,  600,  1,       0,    10000,    0.086,  "WR1400_nuRe600",      0,  "7TeV-38x-8e29-V14-WR1400_nuRe600.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1400,  700,  1,       0,    10000,    0.076,  "WR1400_nuRe700",      0,  "7TeV-38x-8e29-V14-WR1400_nuRe700.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1400,  800,  1,       0,    10000,    0.063,  "WR1400_nuRe800",      0,  "7TeV-38x-8e29-V14-WR1400_nuRe800.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1400,  900,  1,       0,    10000,    0.049,  "WR1400_nuRe900",      0,  "7TeV-38x-8e29-V14-WR1400_nuRe900.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1400, 1000,  1,       0,    10000,    0.036, "WR1400_nuRe1000",      0, "7TeV-38x-8e29-V14-WR1400_nuRe1000.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1400, 1100,  1,       0,    10000,    0.023, "WR1400_nuRe1100",      0, "7TeV-38x-8e29-V14-WR1400_nuRe1100.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1400, 1200,  1,       0,    10000,    0.012, "WR1400_nuRe1200",      0, "7TeV-38x-8e29-V14-WR1400_nuRe1200.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1400, 1300,  1,       0,    10000,    0.0037,"WR1400_nuRe1300",      0, "7TeV-38x-8e29-V14-WR1400_nuRe1300.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1400, 1350,  1,       0,    10000,    0.0014,"WR1400_nuRe1350",      0, "7TeV-38x-8e29-V14-WR1400_nuRe1350.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  
  {1,    7,  1500,   50,  1,       0,    10000,    0.0842,   "WR1500_nuRe50",      0,   "7TeV-38x-8e29-V14-WR1500_nuRe50.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1500,  100,  1,       0,    10000,    0.0831,  "WR1500_nuRe100",      0,  "7TeV-38x-8e29-V14-WR1500_nuRe100.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1500,  200,  1,       0,    10000,    0.0799,  "WR1500_nuRe200",      0,  "7TeV-38x-8e29-V14-WR1500_nuRe200.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1500,  300,  1,       0,    10000,    0.0766,  "WR1500_nuRe300",      0,  "7TeV-38x-8e29-V14-WR1500_nuRe300.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1500,  400,  1,       0,    10000,    0.0706,  "WR1500_nuRe400",      0,  "7TeV-38x-8e29-V14-WR1500_nuRe400.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1500,  500,  1,       0,    10000,    0.0661,  "WR1500_nuRe500",      0,  "7TeV-38x-8e29-V14-WR1500_nuRe500.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1500,  600,  1,       0,    10000,    0.0597,  "WR1500_nuRe600",      0,  "7TeV-38x-8e29-V14-WR1500_nuRe600.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1500,  700,  1,       0,    10000,    0.0532,  "WR1500_nuRe700",      0,  "7TeV-38x-8e29-V14-WR1500_nuRe700.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1500,  800,  1,       0,    10000,    0.0458,  "WR1500_nuRe800",      0,  "7TeV-38x-8e29-V14-WR1500_nuRe800.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1500,  900,  1,       0,    10000,    0.0378,  "WR1500_nuRe900",      0,  "7TeV-38x-8e29-V14-WR1500_nuRe900.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1500, 1000,  1,       0,    10000,    0.0297, "WR1500_nuRe1000",      0, "7TeV-38x-8e29-V14-WR1500_nuRe1000.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1500, 1100,  1,       0,    10000,    0.0210, "WR1500_nuRe1100",      0, "7TeV-38x-8e29-V14-WR1500_nuRe1100.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1500, 1200,  1,       0,    10000,    0.0134, "WR1500_nuRe1200",      0, "7TeV-38x-8e29-V14-WR1500_nuRe1200.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1500, 1300,  1,       0,    10000,    0.0067,"WR1500_nuRe1300",      0, "7TeV-38x-8e29-V14-WR1500_nuRe1300.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1500, 1400,  1,       0,    10000,    0.0021,"WR1500_nuRe1400",      0, "7TeV-38x-8e29-V14-WR1500_nuRe1400.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1500, 1450,  1,       0,    10000,    0.0008,"WR1500_nuRe1450",      0, "7TeV-38x-8e29-V14-WR1500_nuRe1450.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  
  {1,    7,  1600,   50,  1,       0,    10000,    0.0569,   "WR1600_nuRe50",      0,   "7TeV-38x-8e29-V14-WR1600_nuRe50.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1600,  100,  1,       0,    10000,    0.0558,  "WR1600_nuRe100",      0,  "7TeV-38x-8e29-V14-WR1600_nuRe100.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1600,  200,  1,       0,    10000,    0.0531,  "WR1600_nuRe200",      0,  "7TeV-38x-8e29-V14-WR1600_nuRe200.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1600,  300,  1,       0,    10000,    0.0514,  "WR1600_nuRe300",      0,  "7TeV-38x-8e29-V14-WR1600_nuRe300.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1600,  400,  1,       0,    10000,    0.0481,  "WR1600_nuRe400",      0,  "7TeV-38x-8e29-V14-WR1600_nuRe400.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1600,  500,  1,       0,    10000,    0.0446,  "WR1600_nuRe500",      0,  "7TeV-38x-8e29-V14-WR1600_nuRe500.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1600,  600,  1,       0,    10000,    0.0411,  "WR1600_nuRe600",      0,  "7TeV-38x-8e29-V14-WR1600_nuRe600.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1600,  700,  1,       0,    10000,    0.0377,  "WR1600_nuRe700",      0,  "7TeV-38x-8e29-V14-WR1600_nuRe700.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1600,  800,  1,       0,    10000,    0.0326,  "WR1600_nuRe800",      0,  "7TeV-38x-8e29-V14-WR1600_nuRe800.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1600,  900,  1,       0,    10000,    0.0277,  "WR1600_nuRe900",      0,  "7TeV-38x-8e29-V14-WR1600_nuRe900.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1600, 1000,  1,       0,    10000,    0.0227, "WR1600_nuRe1000",      0, "7TeV-38x-8e29-V14-WR1600_nuRe1000.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1600, 1100,  1,       0,    10000,    0.0177, "WR1600_nuRe1100",      0, "7TeV-38x-8e29-V14-WR1600_nuRe1100.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1600, 1200,  1,       0,    10000,    0.0124, "WR1600_nuRe1200",      0, "7TeV-38x-8e29-V14-WR1600_nuRe1200.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1600, 1300,  1,       0,    10000,    0.0079,"WR1600_nuRe1300",      0, "7TeV-38x-8e29-V14-WR1600_nuRe1300.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1600, 1400,  1,       0,    10000,    0.0040,"WR1600_nuRe1400",      0, "7TeV-38x-8e29-V14-WR1600_nuRe1400.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1600, 1500,  1,       0,    10000,    0.0012,"WR1600_nuRe1500",      0, "7TeV-38x-8e29-V14-WR1600_nuRe1500.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  {1,    7,  1600, 1550,  1,       0,    10000,    0.005,"WR1600_nuRe1550",      0, "7TeV-38x-8e29-V14-WR1600_nuRe1550.root",   "START38_V14::All",  "8E29",  "3_8_7",  ""},
  
  
  // CMSSW_3_8_7_patch2
  // full grid: WR = [500 ... 2500], nuR = [50 ... WR], step 100 GeV (336 mass points in total)
  {1,  7,  500,  50,  1,  0,  5000,  15.21,  "WR500_nuRe50",  0,  "38x-7TeV-V14-WR500_nuRe50.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  500,  100,  1,  0,  5000,  14.42,  "WR500_nuRe100",  0,  "38x-7TeV-V14-WR500_nuRe100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  500,  200,  1,  0,  5000,  11.79,  "WR500_nuRe200",  0,  "38x-7TeV-V14-WR500_nuRe200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  500,  300,  1,  0,  5000,  7.603,  "WR500_nuRe300",  0,  "38x-7TeV-V14-WR500_nuRe300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  500,  400,  1,  0,  5000,  2.791,  "WR500_nuRe400",  0,  "38x-7TeV-V14-WR500_nuRe400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  500,  450,  1,  0,  5000,  .901,  "WR500_nuRe450",  0,  "38x-7TeV-V14-WR500_nuRe450.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  600,  50,  1,  0,  5000,  7.32,  "WR600_nuRe50",  0,  "38x-7TeV-V14-WR600_nuRe50.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  600,  100,  1,  0,  5000,  7.071,  "WR600_nuRe100",  0,  "38x-7TeV-V14-WR600_nuRe100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  600,  200,  1,  0,  5000,  6.081,  "WR600_nuRe200",  0,  "38x-7TeV-V14-WR600_nuRe200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  600,  300,  1,  0,  5000,  4.658,  "WR600_nuRe300",  0,  "38x-7TeV-V14-WR600_nuRe300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  600,  400,  1,  0,  5000,  2.834,  "WR600_nuRe400",  0,  "38x-7TeV-V14-WR600_nuRe400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  600,  500,  1,  0,  5000,  .9805,  "WR600_nuRe500",  0,  "38x-7TeV-V14-WR600_nuRe500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  600,  550,  1,  0,  4900,  .3183,  "WR600_nuRe550",  0,  "38x-7TeV-V14-WR600_nuRe550.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  700,  50,  1,  0,  5000,  3.839,  "WR700_nuRe50",  0,  "38x-7TeV-V14-WR700_nuRe50.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  700,  100,  1,  0,  5000,  3.712,  "WR700_nuRe100",  0,  "38x-7TeV-V14-WR700_nuRe100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  700,  200,  1,  0,  5000,  3.35,  "WR700_nuRe200",  0,  "38x-7TeV-V14-WR700_nuRe200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  700,  300,  1,  0,  5000,  2.779,  "WR700_nuRe300",  0,  "38x-7TeV-V14-WR700_nuRe300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  700,  400,  1,  0,  5000,  2.042,  "WR700_nuRe400",  0,  "38x-7TeV-V14-WR700_nuRe400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  700,  500,  1,  0,  5000,  1.177,  "WR700_nuRe500",  0,  "38x-7TeV-V14-WR700_nuRe500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  700,  600,  1,  0,  5000,  .3918,  "WR700_nuRe600",  0,  "38x-7TeV-V14-WR700_nuRe600.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  700,  650,  1,  0,  5000,  .128,  "WR700_nuRe650",  0,  "38x-7TeV-V14-WR700_nuRe650.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  800,  50,  1,  0,  5000,  2.134,  "WR800_nuRe50",  0,  "38x-7TeV-V14-WR800_nuRe50.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  800,  100,  1,  0,  5000,  2.092,  "WR800_nuRe100",  0,  "38x-7TeV-V14-WR800_nuRe100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  800,  200,  1,  0,  5000,  1.935,  "WR800_nuRe200",  0,  "38x-7TeV-V14-WR800_nuRe200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  800,  300,  1,  0,  5000,  1.68,  "WR800_nuRe300",  0,  "38x-7TeV-V14-WR800_nuRe300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  800,  400,  1,  0,  5000,  1.35,  "WR800_nuRe400",  0,  "38x-7TeV-V14-WR800_nuRe400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  800,  500,  1,  0,  5000,  .9541,  "WR800_nuRe500",  0,  "38x-7TeV-V14-WR800_nuRe500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  800,  600,  1,  0,  5000,  .5333,  "WR800_nuRe600",  0,  "38x-7TeV-V14-WR800_nuRe600.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  800,  700,  1,  0,  5000,  .1733,  "WR800_nuRe700",  0,  "38x-7TeV-V14-WR800_nuRe700.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  800,  750,  1,  0,  5000,  .05734,  "WR800_nuRe750",  0,  "38x-7TeV-V14-WR800_nuRe750.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  900,  50,  1,  0,  5000,  1.247,  "WR900_nuRe50",  0,  "38x-7TeV-V14-WR900_nuRe50.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  900,  100,  1,  0,  5000,  1.225,  "WR900_nuRe100",  0,  "38x-7TeV-V14-WR900_nuRe100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  900,  200,  1,  0,  5000,  1.147,  "WR900_nuRe200",  0,  "38x-7TeV-V14-WR900_nuRe200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  900,  300,  1,  0,  5000,  1.03,  "WR900_nuRe300",  0,  "38x-7TeV-V14-WR900_nuRe300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  900,  400,  1,  0,  5000,  .8728,  "WR900_nuRe400",  0,  "38x-7TeV-V14-WR900_nuRe400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  900,  500,  1,  0,  5000,  .6838,  "WR900_nuRe500",  0,  "38x-7TeV-V14-WR900_nuRe500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  900,  600,  1,  0,  5000,  .4744,  "WR900_nuRe600",  0,  "38x-7TeV-V14-WR900_nuRe600.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  900,  700,  1,  0,  5000,  .2572,  "WR900_nuRe700",  0,  "38x-7TeV-V14-WR900_nuRe700.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  900,  800,  1,  0,  5000,  .08219,  "WR900_nuRe800",  0,  "38x-7TeV-V14-WR900_nuRe800.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  900,  850,  1,  0,  5000,  .0278,  "WR900_nuRe850",  0,  "38x-7TeV-V14-WR900_nuRe850.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1000,  50,  1,  0,  5000,  .7564,  "WR1000_nuRe50",  0,  "38x-7TeV-V14-WR1000_nuRe50.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1000,  100,  1,  0,  5000,  .7406,  "WR1000_nuRe100",  0,  "38x-7TeV-V14-WR1000_nuRe100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1000,  200,  1,  0,  5000,  .7018,  "WR1000_nuRe200",  0,  "38x-7TeV-V14-WR1000_nuRe200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1000,  300,  1,  0,  5000,  .6447,  "WR1000_nuRe300",  0,  "38x-7TeV-V14-WR1000_nuRe300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1000,  400,  1,  0,  5000,  .5635,  "WR1000_nuRe400",  0,  "38x-7TeV-V14-WR1000_nuRe400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1000,  500,  1,  0,  5000,  .4699,  "WR1000_nuRe500",  0,  "38x-7TeV-V14-WR1000_nuRe500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1000,  600,  1,  0,  5000,  .3594,  "WR1000_nuRe600",  0,  "38x-7TeV-V14-WR1000_nuRe600.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1000,  700,  1,  0,  5000,  .2426,  "WR1000_nuRe700",  0,  "38x-7TeV-V14-WR1000_nuRe700.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1000,  800,  1,  0,  5000,  .1294,  "WR1000_nuRe800",  0,  "38x-7TeV-V14-WR1000_nuRe800.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1000,  900,  1,  0,  5000,  .04134,  "WR1000_nuRe900",  0,  "38x-7TeV-V14-WR1000_nuRe900.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1000,  950,  1,  0,  5000,  .01422,  "WR1000_nuRe950",  0,  "38x-7TeV-V14-WR1000_nuRe950.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1100,  50,  1,  0,  4900,  .4702,  "WR1100_nuRe50",  0,  "38x-7TeV-V14-WR1100_nuRe50.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1100,  100,  1,  0,  5000,  .464,  "WR1100_nuRe100",  0,  "38x-7TeV-V14-WR1100_nuRe100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1100,  200,  1,  0,  5000,  .4416,  "WR1100_nuRe200",  0,  "38x-7TeV-V14-WR1100_nuRe200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1100,  300,  1,  0,  5000,  .4082,  "WR1100_nuRe300",  0,  "38x-7TeV-V14-WR1100_nuRe300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1100,  400,  1,  0,  5000,  .367,  "WR1100_nuRe400",  0,  "38x-7TeV-V14-WR1100_nuRe400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1100,  500,  1,  0,  5000,  .3176,  "WR1100_nuRe500",  0,  "38x-7TeV-V14-WR1100_nuRe500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1100,  600,  1,  0,  5000,  .2605,  "WR1100_nuRe600",  0,  "38x-7TeV-V14-WR1100_nuRe600.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1100,  700,  1,  0,  5000,  .1966,  "WR1100_nuRe700",  0,  "38x-7TeV-V14-WR1100_nuRe700.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1100,  800,  1,  0,  5000,  .1294,  "WR1100_nuRe800",  0,  "38x-7TeV-V14-WR1100_nuRe800.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1100,  900,  1,  0,  5000,  .06859,  "WR1100_nuRe900",  0,  "38x-7TeV-V14-WR1100_nuRe900.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1100,  1000,  1,  0,  5000,  .02153,  "WR1100_nuRe1000",  0,  "38x-7TeV-V14-WR1100_nuRe1000.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1100,  1050,  1,  0,  5000,  .007602,  "WR1100_nuRe1050",  0,  "38x-7TeV-V14-WR1100_nuRe1050.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1200,  50,  1,  0,  5000,  .2999,  "WR1200_nuRe50",  0,  "38x-7TeV-V14-WR1200_nuRe50.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1200,  100,  1,  0,  5000,  .2936,  "WR1200_nuRe100",  0,  "38x-7TeV-V14-WR1200_nuRe100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1200,  200,  1,  0,  5000,  .281,  "WR1200_nuRe200",  0,  "38x-7TeV-V14-WR1200_nuRe200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1200,  300,  1,  0,  5000,  .2631,  "WR1200_nuRe300",  0,  "38x-7TeV-V14-WR1200_nuRe300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1200,  400,  1,  0,  5000,  .2413,  "WR1200_nuRe400",  0,  "38x-7TeV-V14-WR1200_nuRe400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1200,  500,  1,  0,  5000,  .2146,  "WR1200_nuRe500",  0,  "38x-7TeV-V14-WR1200_nuRe500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1200,  600,  1,  0,  5000,  .1829,  "WR1200_nuRe600",  0,  "38x-7TeV-V14-WR1200_nuRe600.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1200,  700,  1,  0,  5000,  .1475,  "WR1200_nuRe700",  0,  "38x-7TeV-V14-WR1200_nuRe700.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1200,  800,  1,  0,  5000,  .1084,  "WR1200_nuRe800",  0,  "38x-7TeV-V14-WR1200_nuRe800.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1200,  900,  1,  0,  5000,  .07117,  "WR1200_nuRe900",  0,  "38x-7TeV-V14-WR1200_nuRe900.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1200,  1000,  1,  0,  5000,  .03707,  "WR1200_nuRe1000",  0,  "38x-7TeV-V14-WR1200_nuRe1000.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1200,  1100,  1,  0,  5000,  .01168,  "WR1200_nuRe1100",  0,  "38x-7TeV-V14-WR1200_nuRe1100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1200,  1150,  1,  0,  5000,  .004174,  "WR1200_nuRe1150",  0,  "38x-7TeV-V14-WR1200_nuRe1150.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1300,  50,  1,  0,  5000,  .1929,  "WR1300_nuRe50",  0,  "38x-7TeV-V14-WR1300_nuRe50.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1300,  100,  1,  0,  5000,  .1903,  "WR1300_nuRe100",  0,  "38x-7TeV-V14-WR1300_nuRe100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1300,  200,  1,  0,  5000,  .1828,  "WR1300_nuRe200",  0,  "38x-7TeV-V14-WR1300_nuRe200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1300,  300,  1,  0,  4800,  .1724,  "WR1300_nuRe300",  0,  "38x-7TeV-V14-WR1300_nuRe300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1300,  400,  1,  0,  4900,  .1597,  "WR1300_nuRe400",  0,  "38x-7TeV-V14-WR1300_nuRe400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1300,  500,  1,  0,  4800,  .1448,  "WR1300_nuRe500",  0,  "38x-7TeV-V14-WR1300_nuRe500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1300,  600,  1,  0,  4800,  .1262,  "WR1300_nuRe600",  0,  "38x-7TeV-V14-WR1300_nuRe600.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1300,  700,  1,  0,  4900,  .1067,  "WR1300_nuRe700",  0,  "38x-7TeV-V14-WR1300_nuRe700.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1300,  800,  1,  0,  4800,  .08491,  "WR1300_nuRe800",  0,  "38x-7TeV-V14-WR1300_nuRe800.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1300,  900,  1,  0,  5000,  .06246,  "WR1300_nuRe900",  0,  "38x-7TeV-V14-WR1300_nuRe900.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1300,  1000,  1,  0,  5000,  .04024,  "WR1300_nuRe1000",  0,  "38x-7TeV-V14-WR1300_nuRe1000.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1300,  1100,  1,  0,  5000,  .02063,  "WR1300_nuRe1100",  0,  "38x-7TeV-V14-WR1300_nuRe1100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1300,  1200,  1,  0,  5000,  .006446,  "WR1300_nuRe1200",  0,  "38x-7TeV-V14-WR1300_nuRe1200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1300,  1250,  1,  0,  4900,  .002374,  "WR1300_nuRe1250",  0,  "38x-7TeV-V14-WR1300_nuRe1250.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1400,  50,  1,  0,  5000,  .1273,  "WR1400_nuRe50",  0,  "38x-7TeV-V14-WR1400_nuRe50.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1400,  100,  1,  0,  5000,  .1247,  "WR1400_nuRe100",  0,  "38x-7TeV-V14-WR1400_nuRe100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1400,  200,  1,  0,  4800,  .1199,  "WR1400_nuRe200",  0,  "38x-7TeV-V14-WR1400_nuRe200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1400,  300,  1,  0,  5000,  .114,  "WR1400_nuRe300",  0,  "38x-7TeV-V14-WR1400_nuRe300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1400,  400,  1,  0,  5000,  .106,  "WR1400_nuRe400",  0,  "38x-7TeV-V14-WR1400_nuRe400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1400,  500,  1,  0,  5000,  .09719,  "WR1400_nuRe500",  0,  "38x-7TeV-V14-WR1400_nuRe500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1400,  600,  1,  0,  4900,  .08742,  "WR1400_nuRe600",  0,  "38x-7TeV-V14-WR1400_nuRe600.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1400,  700,  1,  0,  4900,  .07587,  "WR1400_nuRe700",  0,  "38x-7TeV-V14-WR1400_nuRe700.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1400,  800,  1,  0,  5000,  .06346,  "WR1400_nuRe800",  0,  "38x-7TeV-V14-WR1400_nuRe800.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1400,  900,  1,  0,  4900,  .04956,  "WR1400_nuRe900",  0,  "38x-7TeV-V14-WR1400_nuRe900.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1400,  1000,  1,  0,  4900,  .03608,  "WR1400_nuRe1000",  0,  "38x-7TeV-V14-WR1400_nuRe1000.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1400,  1100,  1,  0,  5000,  .02295,  "WR1400_nuRe1100",  0,  "38x-7TeV-V14-WR1400_nuRe1100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1400,  1200,  1,  0,  4900,  .01171,  "WR1400_nuRe1200",  0,  "38x-7TeV-V14-WR1400_nuRe1200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1400,  1300,  1,  0,  5000,  .003683,  "WR1400_nuRe1300",  0,  "38x-7TeV-V14-WR1400_nuRe1300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1400,  1350,  1,  0,  4900,  .001373,  "WR1400_nuRe1350",  0,  "38x-7TeV-V14-WR1400_nuRe1350.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1500,  50,  1,  0,  5000,  .08515,  "WR1500_nuRe50",  0,  "38x-7TeV-V14-WR1500_nuRe50.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1500,  100,  1,  0,  5000,  .08297,  "WR1500_nuRe100",  0,  "38x-7TeV-V14-WR1500_nuRe100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1500,  200,  1,  0,  5000,  .07967,  "WR1500_nuRe200",  0,  "38x-7TeV-V14-WR1500_nuRe200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1500,  300,  1,  0,  5000,  .07575,  "WR1500_nuRe300",  0,  "38x-7TeV-V14-WR1500_nuRe300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1500,  400,  1,  0,  5000,  .07104,  "WR1500_nuRe400",  0,  "38x-7TeV-V14-WR1500_nuRe400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1500,  500,  1,  0,  5000,  .06608,  "WR1500_nuRe500",  0,  "38x-7TeV-V14-WR1500_nuRe500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1500,  600,  1,  0,  4800,  .05988,  "WR1500_nuRe600",  0,  "38x-7TeV-V14-WR1500_nuRe600.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1500,  700,  1,  0,  4700,  .05315,  "WR1500_nuRe700",  0,  "38x-7TeV-V14-WR1500_nuRe700.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1500,  800,  1,  0,  4800,  .04579,  "WR1500_nuRe800",  0,  "38x-7TeV-V14-WR1500_nuRe800.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1500,  900,  1,  0,  4900,  .03802,  "WR1500_nuRe900",  0,  "38x-7TeV-V14-WR1500_nuRe900.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1500,  1000,  1,  0,  5000,  .02956,  "WR1500_nuRe1000",  0,  "38x-7TeV-V14-WR1500_nuRe1000.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1500,  1100,  1,  0,  5000,  .02115,  "WR1500_nuRe1100",  0,  "38x-7TeV-V14-WR1500_nuRe1100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1500,  1200,  1,  0,  5000,  .01347,  "WR1500_nuRe1200",  0,  "38x-7TeV-V14-WR1500_nuRe1200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1500,  1300,  1,  0,  5000,  .00677,  "WR1500_nuRe1300",  0,  "38x-7TeV-V14-WR1500_nuRe1300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1500,  1400,  1,  0,  5000,  .002126,  "WR1500_nuRe1400",  0,  "38x-7TeV-V14-WR1500_nuRe1400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1500,  1450,  1,  0,  5000,  .0008078,  "WR1500_nuRe1450",  0,  "38x-7TeV-V14-WR1500_nuRe1450.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1600,  50,  1,  0,  5000,  .05695,  "WR1600_nuRe50",  0,  "38x-7TeV-V14-WR1600_nuRe50.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1600,  100,  1,  0,  5000,  .05573,  "WR1600_nuRe100",  0,  "38x-7TeV-V14-WR1600_nuRe100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1600,  200,  1,  0,  5000,  .05332,  "WR1600_nuRe200",  0,  "38x-7TeV-V14-WR1600_nuRe200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1600,  300,  1,  0,  5000,  .05101,  "WR1600_nuRe300",  0,  "38x-7TeV-V14-WR1600_nuRe300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1600,  400,  1,  0,  5000,  .04799,  "WR1600_nuRe400",  0,  "38x-7TeV-V14-WR1600_nuRe400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1600,  500,  1,  0,  5000,  .04485,  "WR1600_nuRe500",  0,  "38x-7TeV-V14-WR1600_nuRe500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1600,  600,  1,  0,  5000,  .04124,  "WR1600_nuRe600",  0,  "38x-7TeV-V14-WR1600_nuRe600.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1600,  700,  1,  0,  5000,  .03704,  "WR1600_nuRe700",  0,  "38x-7TeV-V14-WR1600_nuRe700.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1600,  800,  1,  0,  5000,  .03274,  "WR1600_nuRe800",  0,  "38x-7TeV-V14-WR1600_nuRe800.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1600,  900,  1,  0,  5000,  .02793,  "WR1600_nuRe900",  0,  "38x-7TeV-V14-WR1600_nuRe900.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1600,  1000,  1,  0,  5000,  .02291,  "WR1600_nuRe1000",  0,  "38x-7TeV-V14-WR1600_nuRe1000.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1600,  1100,  1,  0,  5000,  .01779,  "WR1600_nuRe1100",  0,  "38x-7TeV-V14-WR1600_nuRe1100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1600,  1200,  1,  0,  4800,  .01259,  "WR1600_nuRe1200",  0,  "38x-7TeV-V14-WR1600_nuRe1200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1600,  1300,  1,  0,  5000,  .007888,  "WR1600_nuRe1300",  0,  "38x-7TeV-V14-WR1600_nuRe1300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1600,  1400,  1,  0,  5000,  .003966,  "WR1600_nuRe1400",  0,  "38x-7TeV-V14-WR1600_nuRe1400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1600,  1500,  1,  0,  5000,  .001245,  "WR1600_nuRe1500",  0,  "38x-7TeV-V14-WR1600_nuRe1500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1600,  1550,  1,  0,  5000,  .0004842,  "WR1600_nuRe1550",  0,  "38x-7TeV-V14-WR1600_nuRe1550.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1700,  50,  1,  0,  5000,  .03863,  "WR1700_nuRe50",  0,  "38x-7TeV-V14-WR1700_nuRe50.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1700,  100,  1,  0,  5000,  .03783,  "WR1700_nuRe100",  0,  "38x-7TeV-V14-WR1700_nuRe100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1700,  200,  1,  0,  5000,  .03608,  "WR1700_nuRe200",  0,  "38x-7TeV-V14-WR1700_nuRe200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1700,  300,  1,  0,  5000,  .03441,  "WR1700_nuRe300",  0,  "38x-7TeV-V14-WR1700_nuRe300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1700,  400,  1,  0,  4900,  .03261,  "WR1700_nuRe400",  0,  "38x-7TeV-V14-WR1700_nuRe400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1700,  500,  1,  0,  5000,  .03059,  "WR1700_nuRe500",  0,  "38x-7TeV-V14-WR1700_nuRe500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1700,  600,  1,  0,  5000,  .02823,  "WR1700_nuRe600",  0,  "38x-7TeV-V14-WR1700_nuRe600.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1700,  700,  1,  0,  5000,  .0259,  "WR1700_nuRe700",  0,  "38x-7TeV-V14-WR1700_nuRe700.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1700,  800,  1,  0,  5000,  .02315,  "WR1700_nuRe800",  0,  "38x-7TeV-V14-WR1700_nuRe800.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1700,  900,  1,  0,  5000,  .0202,  "WR1700_nuRe900",  0,  "38x-7TeV-V14-WR1700_nuRe900.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1700,  1000,  1,  0,  5000,  .0172,  "WR1700_nuRe1000",  0,  "38x-7TeV-V14-WR1700_nuRe1000.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1700,  1100,  1,  0,  5000,  .014,  "WR1700_nuRe1100",  0,  "38x-7TeV-V14-WR1700_nuRe1100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1700,  1200,  1,  0,  5000,  .0107,  "WR1700_nuRe1200",  0,  "38x-7TeV-V14-WR1700_nuRe1200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1700,  1300,  1,  0,  5000,  .007535,  "WR1700_nuRe1300",  0,  "38x-7TeV-V14-WR1700_nuRe1300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1700,  1400,  1,  0,  5000,  .004695,  "WR1700_nuRe1400",  0,  "38x-7TeV-V14-WR1700_nuRe1400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1700,  1500,  1,  0,  5000,  .002352,  "WR1700_nuRe1500",  0,  "38x-7TeV-V14-WR1700_nuRe1500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1700,  1600,  1,  0,  4800,  .0007399,  "WR1700_nuRe1600",  0,  "38x-7TeV-V14-WR1700_nuRe1600.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1700,  1650,  1,  0,  5000,  .0002909,  "WR1700_nuRe1650",  0,  "38x-7TeV-V14-WR1700_nuRe1650.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1800,  50,  1,  0,  5000,  .02635,  "WR1800_nuRe50",  0,  "38x-7TeV-V14-WR1800_nuRe50.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1800,  100,  1,  0,  4900,  .02567,  "WR1800_nuRe100",  0,  "38x-7TeV-V14-WR1800_nuRe100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1800,  200,  1,  0,  5000,  .02469,  "WR1800_nuRe200",  0,  "38x-7TeV-V14-WR1800_nuRe200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1800,  300,  1,  0,  5000,  .02347,  "WR1800_nuRe300",  0,  "38x-7TeV-V14-WR1800_nuRe300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1800,  400,  1,  0,  5000,  .02222,  "WR1800_nuRe400",  0,  "38x-7TeV-V14-WR1800_nuRe400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1800,  500,  1,  0,  5000,  .02091,  "WR1800_nuRe500",  0,  "38x-7TeV-V14-WR1800_nuRe500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1800,  600,  1,  0,  5000,  .01952,  "WR1800_nuRe600",  0,  "38x-7TeV-V14-WR1800_nuRe600.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1800,  700,  1,  0,  5000,  .01797,  "WR1800_nuRe700",  0,  "38x-7TeV-V14-WR1800_nuRe700.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1800,  800,  1,  0,  5000,  .01625,  "WR1800_nuRe800",  0,  "38x-7TeV-V14-WR1800_nuRe800.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1800,  900,  1,  0,  5000,  .01444,  "WR1800_nuRe900",  0,  "38x-7TeV-V14-WR1800_nuRe900.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1800,  1000,  1,  0,  5000,  .01255,  "WR1800_nuRe1000",  0,  "38x-7TeV-V14-WR1800_nuRe1000.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1800,  1100,  1,  0,  5000,  .01056,  "WR1800_nuRe1100",  0,  "38x-7TeV-V14-WR1800_nuRe1100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1800,  1200,  1,  0,  5000,  .008524,  "WR1800_nuRe1200",  0,  "38x-7TeV-V14-WR1800_nuRe1200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1800,  1300,  1,  0,  5000,  .006509,  "WR1800_nuRe1300",  0,  "38x-7TeV-V14-WR1800_nuRe1300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1800,  1400,  1,  0,  4900,  .004564,  "WR1800_nuRe1400",  0,  "38x-7TeV-V14-WR1800_nuRe1400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1800,  1500,  1,  0,  5000,  .002827,  "WR1800_nuRe1500",  0,  "38x-7TeV-V14-WR1800_nuRe1500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1800,  1600,  1,  0,  5000,  .001406,  "WR1800_nuRe1600",  0,  "38x-7TeV-V14-WR1800_nuRe1600.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1800,  1700,  1,  0,  5000,  .0004428,  "WR1800_nuRe1700",  0,  "38x-7TeV-V14-WR1800_nuRe1700.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1800,  1750,  1,  0,  5000,  .0001767,  "WR1800_nuRe1750",  0,  "38x-7TeV-V14-WR1800_nuRe1750.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1900,  50,  1,  0,  5000,  .01827,  "WR1900_nuRe50",  0,  "38x-7TeV-V14-WR1900_nuRe50.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1900,  100,  1,  0,  4900,  .01778,  "WR1900_nuRe100",  0,  "38x-7TeV-V14-WR1900_nuRe100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1900,  200,  1,  0,  4800,  .0169,  "WR1900_nuRe200",  0,  "38x-7TeV-V14-WR1900_nuRe200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1900,  300,  1,  0,  4800,  .01603,  "WR1900_nuRe300",  0,  "38x-7TeV-V14-WR1900_nuRe300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1900,  400,  1,  0,  5000,  .01515,  "WR1900_nuRe400",  0,  "38x-7TeV-V14-WR1900_nuRe400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1900,  500,  1,  0,  5000,  .01433,  "WR1900_nuRe500",  0,  "38x-7TeV-V14-WR1900_nuRe500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1900,  600,  1,  0,  5000,  .01339,  "WR1900_nuRe600",  0,  "38x-7TeV-V14-WR1900_nuRe600.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1900,  700,  1,  0,  5000,  .01241,  "WR1900_nuRe700",  0,  "38x-7TeV-V14-WR1900_nuRe700.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1900,  800,  1,  0,  5000,  .01143,  "WR1900_nuRe800",  0,  "38x-7TeV-V14-WR1900_nuRe800.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1900,  900,  1,  0,  5000,  .01025,  "WR1900_nuRe900",  0,  "38x-7TeV-V14-WR1900_nuRe900.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1900,  1000,  1,  0,  5000,  .009057,  "WR1900_nuRe1000",  0,  "38x-7TeV-V14-WR1900_nuRe1000.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1900,  1100,  1,  0,  5000,  .00784,  "WR1900_nuRe1100",  0,  "38x-7TeV-V14-WR1900_nuRe1100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1900,  1200,  1,  0,  5000,  .006523,  "WR1900_nuRe1200",  0,  "38x-7TeV-V14-WR1900_nuRe1200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1900,  1300,  1,  0,  5000,  .005235,  "WR1900_nuRe1300",  0,  "38x-7TeV-V14-WR1900_nuRe1300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1900,  1400,  1,  0,  5000,  .003982,  "WR1900_nuRe1400",  0,  "38x-7TeV-V14-WR1900_nuRe1400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1900,  1500,  1,  0,  5000,  .002791,  "WR1900_nuRe1500",  0,  "38x-7TeV-V14-WR1900_nuRe1500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1900,  1600,  1,  0,  5000,  .001715,  "WR1900_nuRe1600",  0,  "38x-7TeV-V14-WR1900_nuRe1600.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1900,  1700,  1,  0,  4900,  .0008482,  "WR1900_nuRe1700",  0,  "38x-7TeV-V14-WR1900_nuRe1700.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1900,  1800,  1,  0,  5000,  .0002676,  "WR1900_nuRe1800",  0,  "38x-7TeV-V14-WR1900_nuRe1800.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  1900,  1850,  1,  0,  5000,  .000109,  "WR1900_nuRe1850",  0,  "38x-7TeV-V14-WR1900_nuRe1850.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2000,  50,  1,  0,  5000,  .01275,  "WR2000_nuRe50",  0,  "38x-7TeV-V14-WR2000_nuRe50.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2000,  100,  1,  0,  5000,  .01235,  "WR2000_nuRe100",  0,  "38x-7TeV-V14-WR2000_nuRe100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2000,  200,  1,  0,  5000,  .0116,  "WR2000_nuRe200",  0,  "38x-7TeV-V14-WR2000_nuRe200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2000,  300,  1,  0,  4900,  .01101,  "WR2000_nuRe300",  0,  "38x-7TeV-V14-WR2000_nuRe300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2000,  400,  1,  0,  5000,  .01043,  "WR2000_nuRe400",  0,  "38x-7TeV-V14-WR2000_nuRe400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2000,  500,  1,  0,  4900,  .009829,  "WR2000_nuRe500",  0,  "38x-7TeV-V14-WR2000_nuRe500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2000,  600,  1,  0,  5000,  .009236,  "WR2000_nuRe600",  0,  "38x-7TeV-V14-WR2000_nuRe600.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2000,  700,  1,  0,  5000,  .008606,  "WR2000_nuRe700",  0,  "38x-7TeV-V14-WR2000_nuRe700.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2000,  800,  1,  0,  5000,  .007916,  "WR2000_nuRe800",  0,  "38x-7TeV-V14-WR2000_nuRe800.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2000,  900,  1,  0,  5000,  .007201,  "WR2000_nuRe900",  0,  "38x-7TeV-V14-WR2000_nuRe900.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2000,  1000,  1,  0,  5000,  .006453,  "WR2000_nuRe1000",  0,  "38x-7TeV-V14-WR2000_nuRe1000.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2000,  1100,  1,  0,  5000,  .005721,  "WR2000_nuRe1100",  0,  "38x-7TeV-V14-WR2000_nuRe1100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2000,  1200,  1,  0,  5000,  .00492,  "WR2000_nuRe1200",  0,  "38x-7TeV-V14-WR2000_nuRe1200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2000,  1300,  1,  0,  5000,  .00405,  "WR2000_nuRe1300",  0,  "38x-7TeV-V14-WR2000_nuRe1300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2000,  1400,  1,  0,  5000,  .003236,  "WR2000_nuRe1400",  0,  "38x-7TeV-V14-WR2000_nuRe1400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2000,  1500,  1,  0,  4900,  .002449,  "WR2000_nuRe1500",  0,  "38x-7TeV-V14-WR2000_nuRe1500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2000,  1600,  1,  0,  5000,  .001688,  "WR2000_nuRe1600",  0,  "38x-7TeV-V14-WR2000_nuRe1600.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2000,  1700,  1,  0,  5000,  .001041,  "WR2000_nuRe1700",  0,  "38x-7TeV-V14-WR2000_nuRe1700.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2000,  1800,  1,  0,  5000,  .0005106,  "WR2000_nuRe1800",  0,  "38x-7TeV-V14-WR2000_nuRe1800.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2000,  1900,  1,  0,  4900,  .0001619,  "WR2000_nuRe1900",  0,  "38x-7TeV-V14-WR2000_nuRe1900.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2000,  1950,  1,  0,  5000,  .00006679,  "WR2000_nuRe1950",  0,  "38x-7TeV-V14-WR2000_nuRe1950.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2100,  50,  1,  0,  5000,  .008989,  "WR2100_nuRe50",  0,  "38x-7TeV-V14-WR2100_nuRe50.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2100,  100,  1,  0,  4700,  .008643,  "WR2100_nuRe100",  0,  "38x-7TeV-V14-WR2100_nuRe100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2100,  200,  1,  0,  5000,  .008082,  "WR2100_nuRe200",  0,  "38x-7TeV-V14-WR2100_nuRe200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2100,  300,  1,  0,  4800,  .007603,  "WR2100_nuRe300",  0,  "38x-7TeV-V14-WR2100_nuRe300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2100,  400,  1,  0,  5000,  .007226,  "WR2100_nuRe400",  0,  "38x-7TeV-V14-WR2100_nuRe400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2100,  500,  1,  0,  5000,  .006804,  "WR2100_nuRe500",  0,  "38x-7TeV-V14-WR2100_nuRe500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2100,  600,  1,  0,  4900,  .006372,  "WR2100_nuRe600",  0,  "38x-7TeV-V14-WR2100_nuRe600.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2100,  700,  1,  0,  5000,  .005946,  "WR2100_nuRe700",  0,  "38x-7TeV-V14-WR2100_nuRe700.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2100,  800,  1,  0,  5000,  .005501,  "WR2100_nuRe800",  0,  "38x-7TeV-V14-WR2100_nuRe800.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2100,  900,  1,  0,  5000,  .005078,  "WR2100_nuRe900",  0,  "38x-7TeV-V14-WR2100_nuRe900.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2100,  1000,  1,  0,  5000,  .004594,  "WR2100_nuRe1000",  0,  "38x-7TeV-V14-WR2100_nuRe1000.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2100,  1100,  1,  0,  5000,  .004097,  "WR2100_nuRe1100",  0,  "38x-7TeV-V14-WR2100_nuRe1100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2100,  1200,  1,  0,  5000,  .003577,  "WR2100_nuRe1200",  0,  "38x-7TeV-V14-WR2100_nuRe1200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2100,  1300,  1,  0,  4900,  .003056,  "WR2100_nuRe1300",  0,  "38x-7TeV-V14-WR2100_nuRe1300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2100,  1400,  1,  0,  4900,  .002531,  "WR2100_nuRe1400",  0,  "38x-7TeV-V14-WR2100_nuRe1400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2100,  1500,  1,  0,  4900,  .002013,  "WR2100_nuRe1500",  0,  "38x-7TeV-V14-WR2100_nuRe1500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2100,  1600,  1,  0,  4900,  .001506,  "WR2100_nuRe1600",  0,  "38x-7TeV-V14-WR2100_nuRe1600.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2100,  1700,  1,  0,  4500,  .001039,  "WR2100_nuRe1700",  0,  "38x-7TeV-V14-WR2100_nuRe1700.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2100,  1800,  1,  0,  4800,  .0006346,  "WR2100_nuRe1800",  0,  "38x-7TeV-V14-WR2100_nuRe1800.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2100,  1900,  1,  0,  4900,  .0003125,  "WR2100_nuRe1900",  0,  "38x-7TeV-V14-WR2100_nuRe1900.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2100,  2000,  1,  0,  4900,  .00009916,  "WR2100_nuRe2000",  0,  "38x-7TeV-V14-WR2100_nuRe2000.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2100,  2050,  1,  0,  5000,  .00004114,  "WR2100_nuRe2050",  0,  "38x-7TeV-V14-WR2100_nuRe2050.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2200,  50,  1,  0,  5000,  .006405,  "WR2200_nuRe50",  0,  "38x-7TeV-V14-WR2200_nuRe50.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2200,  100,  1,  0,  5000,  .006093,  "WR2200_nuRe100",  0,  "38x-7TeV-V14-WR2200_nuRe100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2200,  200,  1,  0,  5000,  .005663,  "WR2200_nuRe200",  0,  "38x-7TeV-V14-WR2200_nuRe200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2200,  300,  1,  0,  5000,  .005294,  "WR2200_nuRe300",  0,  "38x-7TeV-V14-WR2200_nuRe300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2200,  400,  1,  0,  4900,  .004972,  "WR2200_nuRe400",  0,  "38x-7TeV-V14-WR2200_nuRe400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2200,  500,  1,  0,  5000,  .004677,  "WR2200_nuRe500",  0,  "38x-7TeV-V14-WR2200_nuRe500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2200,  600,  1,  0,  4900,  .004406,  "WR2200_nuRe600",  0,  "38x-7TeV-V14-WR2200_nuRe600.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2200,  700,  1,  0,  5000,  .004116,  "WR2200_nuRe700",  0,  "38x-7TeV-V14-WR2200_nuRe700.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2200,  800,  1,  0,  5000,  .003845,  "WR2200_nuRe800",  0,  "38x-7TeV-V14-WR2200_nuRe800.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2200,  900,  1,  0,  5000,  .003538,  "WR2200_nuRe900",  0,  "38x-7TeV-V14-WR2200_nuRe900.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2200,  1000,  1,  0,  5000,  .003231,  "WR2200_nuRe1000",  0,  "38x-7TeV-V14-WR2200_nuRe1000.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2200,  1100,  1,  0,  4900,  .002895,  "WR2200_nuRe1100",  0,  "38x-7TeV-V14-WR2200_nuRe1100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2200,  1200,  1,  0,  5000,  .002591,  "WR2200_nuRe1200",  0,  "38x-7TeV-V14-WR2200_nuRe1200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2200,  1300,  1,  0,  5000,  .002254,  "WR2200_nuRe1300",  0,  "38x-7TeV-V14-WR2200_nuRe1300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2200,  1400,  1,  0,  5000,  .001911,  "WR2200_nuRe1400",  0,  "38x-7TeV-V14-WR2200_nuRe1400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2200,  1500,  1,  0,  5000,  .001579,  "WR2200_nuRe1500",  0,  "38x-7TeV-V14-WR2200_nuRe1500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2200,  1600,  1,  0,  5000,  .001242,  "WR2200_nuRe1600",  0,  "38x-7TeV-V14-WR2200_nuRe1600.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2200,  1700,  1,  0,  5000,  .0009351,  "WR2200_nuRe1700",  0,  "38x-7TeV-V14-WR2200_nuRe1700.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2200,  1800,  1,  0,  5000,  .0006404,  "WR2200_nuRe1800",  0,  "38x-7TeV-V14-WR2200_nuRe1800.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2200,  1900,  1,  0,  5000,  .0003885,  "WR2200_nuRe1900",  0,  "38x-7TeV-V14-WR2200_nuRe1900.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2200,  2000,  1,  0,  5000,  .0001902,  "WR2200_nuRe2000",  0,  "38x-7TeV-V14-WR2200_nuRe2000.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2200,  2100,  1,  0,  5000,  .00006031,  "WR2200_nuRe2100",  0,  "38x-7TeV-V14-WR2200_nuRe2100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2200,  2150,  1,  0,  5000,  .00002544,  "WR2200_nuRe2150",  0,  "38x-7TeV-V14-WR2200_nuRe2150.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2300,  50,  1,  0,  5000,  .004587,  "WR2300_nuRe50",  0,  "38x-7TeV-V14-WR2300_nuRe50.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2300,  100,  1,  0,  5000,  .004363,  "WR2300_nuRe100",  0,  "38x-7TeV-V14-WR2300_nuRe100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2300,  200,  1,  0,  5000,  .004007,  "WR2300_nuRe200",  0,  "38x-7TeV-V14-WR2300_nuRe200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2300,  300,  1,  0,  5000,  .0037,  "WR2300_nuRe300",  0,  "38x-7TeV-V14-WR2300_nuRe300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2300,  400,  1,  0,  5000,  .003475,  "WR2300_nuRe400",  0,  "38x-7TeV-V14-WR2300_nuRe400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2300,  500,  1,  0,  5000,  .003256,  "WR2300_nuRe500",  0,  "38x-7TeV-V14-WR2300_nuRe500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2300,  600,  1,  0,  5000,  .003049,  "WR2300_nuRe600",  0,  "38x-7TeV-V14-WR2300_nuRe600.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2300,  700,  1,  0,  5000,  .002858,  "WR2300_nuRe700",  0,  "38x-7TeV-V14-WR2300_nuRe700.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2300,  800,  1,  0,  5000,  .002652,  "WR2300_nuRe800",  0,  "38x-7TeV-V14-WR2300_nuRe800.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2300,  900,  1,  0,  5000,  .002446,  "WR2300_nuRe900",  0,  "38x-7TeV-V14-WR2300_nuRe900.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2300,  1000,  1,  0,  5000,  .002261,  "WR2300_nuRe1000",  0,  "38x-7TeV-V14-WR2300_nuRe1000.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2300,  1100,  1,  0,  5000,  .002056,  "WR2300_nuRe1100",  0,  "38x-7TeV-V14-WR2300_nuRe1100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2300,  1200,  1,  0,  5000,  .001851,  "WR2300_nuRe1200",  0,  "38x-7TeV-V14-WR2300_nuRe1200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2300,  1300,  1,  0,  5000,  .001639,  "WR2300_nuRe1300",  0,  "38x-7TeV-V14-WR2300_nuRe1300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2300,  1400,  1,  0,  5000,  .001415,  "WR2300_nuRe1400",  0,  "38x-7TeV-V14-WR2300_nuRe1400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2300,  1500,  1,  0,  5000,  .001195,  "WR2300_nuRe1500",  0,  "38x-7TeV-V14-WR2300_nuRe1500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2300,  1600,  1,  0,  5000,  .0009846,  "WR2300_nuRe1600",  0,  "38x-7TeV-V14-WR2300_nuRe1600.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2300,  1700,  1,  0,  5000,  .0007732,  "WR2300_nuRe1700",  0,  "38x-7TeV-V14-WR2300_nuRe1700.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2300,  1800,  1,  0,  4900,  .0005741,  "WR2300_nuRe1800",  0,  "38x-7TeV-V14-WR2300_nuRe1800.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2300,  1900,  1,  0,  5000,  .0003933,  "WR2300_nuRe1900",  0,  "38x-7TeV-V14-WR2300_nuRe1900.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2300,  2000,  1,  0,  5000,  .0002382,  "WR2300_nuRe2000",  0,  "38x-7TeV-V14-WR2300_nuRe2000.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2300,  2100,  1,  0,  5000,  .0001156,  "WR2300_nuRe2100",  0,  "38x-7TeV-V14-WR2300_nuRe2100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2300,  2200,  1,  0,  5000,  .00003695,  "WR2300_nuRe2200",  0,  "38x-7TeV-V14-WR2300_nuRe2200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2300,  2250,  1,  0,  5000,  .00001576,  "WR2300_nuRe2250",  0,  "38x-7TeV-V14-WR2300_nuRe2250.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2400,  50,  1,  0,  5000,  .003318,  "WR2400_nuRe50",  0,  "38x-7TeV-V14-WR2400_nuRe50.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2400,  100,  1,  0,  5000,  .003166,  "WR2400_nuRe100",  0,  "38x-7TeV-V14-WR2400_nuRe100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2400,  200,  1,  0,  5000,  .002853,  "WR2400_nuRe200",  0,  "38x-7TeV-V14-WR2400_nuRe200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2400,  300,  1,  0,  5000,  .002614,  "WR2400_nuRe300",  0,  "38x-7TeV-V14-WR2400_nuRe300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2400,  400,  1,  0,  5000,  .002427,  "WR2400_nuRe400",  0,  "38x-7TeV-V14-WR2400_nuRe400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2400,  500,  1,  0,  5000,  .002271,  "WR2400_nuRe500",  0,  "38x-7TeV-V14-WR2400_nuRe500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2400,  600,  1,  0,  5000,  .002113,  "WR2400_nuRe600",  0,  "38x-7TeV-V14-WR2400_nuRe600.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2400,  700,  1,  0,  5000,  .001986,  "WR2400_nuRe700",  0,  "38x-7TeV-V14-WR2400_nuRe700.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2400,  800,  1,  0,  5000,  .001843,  "WR2400_nuRe800",  0,  "38x-7TeV-V14-WR2400_nuRe800.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2400,  900,  1,  0,  5000,  .001705,  "WR2400_nuRe900",  0,  "38x-7TeV-V14-WR2400_nuRe900.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2400,  1000,  1,  0,  5000,  .001573,  "WR2400_nuRe1000",  0,  "38x-7TeV-V14-WR2400_nuRe1000.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2400,  1100,  1,  0,  5000,  .001441,  "WR2400_nuRe1100",  0,  "38x-7TeV-V14-WR2400_nuRe1100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2400,  1200,  1,  0,  5000,  .001306,  "WR2400_nuRe1200",  0,  "38x-7TeV-V14-WR2400_nuRe1200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2400,  1300,  1,  0,  5000,  .001169,  "WR2400_nuRe1300",  0,  "38x-7TeV-V14-WR2400_nuRe1300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2400,  1400,  1,  0,  5000,  .00103,  "WR2400_nuRe1400",  0,  "38x-7TeV-V14-WR2400_nuRe1400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2400,  1500,  1,  0,  5000,  .0008899,  "WR2400_nuRe1500",  0,  "38x-7TeV-V14-WR2400_nuRe1500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2400,  1600,  1,  0,  5000,  .0007479,  "WR2400_nuRe1600",  0,  "38x-7TeV-V14-WR2400_nuRe1600.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2400,  1700,  1,  0,  5000,  .0006091,  "WR2400_nuRe1700",  0,  "38x-7TeV-V14-WR2400_nuRe1700.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2400,  1800,  1,  0,  5000,  .0004822,  "WR2400_nuRe1800",  0,  "38x-7TeV-V14-WR2400_nuRe1800.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2400,  1900,  1,  0,  5000,  .0003534,  "WR2400_nuRe1900",  0,  "38x-7TeV-V14-WR2400_nuRe1900.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2400,  2000,  1,  0,  5000,  .000242,  "WR2400_nuRe2000",  0,  "38x-7TeV-V14-WR2400_nuRe2000.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2400,  2100,  1,  0,  5000,  .0001455,  "WR2400_nuRe2100",  0,  "38x-7TeV-V14-WR2400_nuRe2100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2400,  2200,  1,  0,  5000,  .00007082,  "WR2400_nuRe2200",  0,  "38x-7TeV-V14-WR2400_nuRe2200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2400,  2300,  1,  0,  5000,  .00002277,  "WR2400_nuRe2300",  0,  "38x-7TeV-V14-WR2400_nuRe2300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2400,  2350,  1,  0,  5000,  .000009732,  "WR2400_nuRe2350",  0,  "38x-7TeV-V14-WR2400_nuRe2350.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2500,  50,  1,  0,  5000,  .002473,  "WR2500_nuRe50",  0,  "38x-7TeV-V14-WR2500_nuRe50.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2500,  100,  1,  0,  5000,  .002301,  "WR2500_nuRe100",  0,  "38x-7TeV-V14-WR2500_nuRe100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2500,  200,  1,  0,  5000,  .002047,  "WR2500_nuRe200",  0,  "38x-7TeV-V14-WR2500_nuRe200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2500,  300,  1,  0,  5000,  .001866,  "WR2500_nuRe300",  0,  "38x-7TeV-V14-WR2500_nuRe300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2500,  400,  1,  0,  5000,  .001715,  "WR2500_nuRe400",  0,  "38x-7TeV-V14-WR2500_nuRe400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2500,  500,  1,  0,  5000,  .001585,  "WR2500_nuRe500",  0,  "38x-7TeV-V14-WR2500_nuRe500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2500,  600,  1,  0,  5000,  .001471,  "WR2500_nuRe600",  0,  "38x-7TeV-V14-WR2500_nuRe600.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2500,  700,  1,  0,  5000,  .001376,  "WR2500_nuRe700",  0,  "38x-7TeV-V14-WR2500_nuRe700.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2500,  800,  1,  0,  5000,  .001279,  "WR2500_nuRe800",  0,  "38x-7TeV-V14-WR2500_nuRe800.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2500,  900,  1,  0,  5000,  .001191,  "WR2500_nuRe900",  0,  "38x-7TeV-V14-WR2500_nuRe900.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2500,  1000,  1,  0,  5000,  .001095,  "WR2500_nuRe1000",  0,  "38x-7TeV-V14-WR2500_nuRe1000.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2500,  1100,  1,  0,  5000,  .001006,  "WR2500_nuRe1100",  0,  "38x-7TeV-V14-WR2500_nuRe1100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2500,  1200,  1,  0,  5000,  .0009173,  "WR2500_nuRe1200",  0,  "38x-7TeV-V14-WR2500_nuRe1200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2500,  1300,  1,  0,  5000,  .0008297,  "WR2500_nuRe1300",  0,  "38x-7TeV-V14-WR2500_nuRe1300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2500,  1400,  1,  0,  5000,  .0007344,  "WR2500_nuRe1400",  0,  "38x-7TeV-V14-WR2500_nuRe1400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2500,  1500,  1,  0,  5000,  .0006459,  "WR2500_nuRe1500",  0,  "38x-7TeV-V14-WR2500_nuRe1500.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2500,  1600,  1,  0,  5000,  .0005576,  "WR2500_nuRe1600",  0,  "38x-7TeV-V14-WR2500_nuRe1600.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2500,  1700,  1,  0,  5000,  .0004665,  "WR2500_nuRe1700",  0,  "38x-7TeV-V14-WR2500_nuRe1700.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2500,  1800,  1,  0,  5000,  .0003809,  "WR2500_nuRe1800",  0,  "38x-7TeV-V14-WR2500_nuRe1800.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2500,  1900,  1,  0,  5000,  .0002966,  "WR2500_nuRe1900",  0,  "38x-7TeV-V14-WR2500_nuRe1900.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2500,  2000,  1,  0,  5000,  .0002185,  "WR2500_nuRe2000",  0,  "38x-7TeV-V14-WR2500_nuRe2000.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2500,  2100,  1,  0,  5000,  .0001484,  "WR2500_nuRe2100",  0,  "38x-7TeV-V14-WR2500_nuRe2100.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2500,  2200,  1,  0,  4900,  .00008879,  "WR2500_nuRe2200",  0,  "38x-7TeV-V14-WR2500_nuRe2200.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2500,  2300,  1,  0,  5000,  .00004323,  "WR2500_nuRe2300",  0,  "38x-7TeV-V14-WR2500_nuRe2300.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2500,  2400,  1,  0,  5000,  .00001385,  "WR2500_nuRe2400",  0,  "38x-7TeV-V14-WR2500_nuRe2400.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},
  {1,  7,  2500,  2450,  1,  0,  5000,  .000005998,  "WR2500_nuRe2450",  0,  "38x-7TeV-V14-WR2500_nuRe2450.root",  "START38_V14::All",  "GRun",  "CMSSW_3_8_7_patch2",  ""},

  // signal samples with variation of ISR/FSR
  {1,    7,   1000,  200,  1,       0,    3800,    0.695,  "WR1000_nuRe200",      0, "38x-7TeV-V14-WR1000_nuRe200-largerISRFSR.root",   "START38_V14::All",  "",  "3_8_7_patch1",  ""},
  {1,    7,   1000,  500,  1,       0,    4000,    0.467,  "WR1000_nuRe500",      0, "38x-7TeV-V14-WR1000_nuRe500-largerISRFSR.root",   "START38_V14::All",  "",  "3_8_7_patch1",  ""},
  {1,    7,   1000,  800,  1,       0,    4000,    0.130,  "WR1000_nuRe800",      0, "38x-7TeV-V14-WR1000_nuRe800-largerISRFSR.root",   "START38_V14::All",  "",  "3_8_7_patch1",  ""},
  {1,    7,   1000,  200,  1,       0,    4000,    0.695,  "WR1000_nuRe200",      0, "38x-7TeV-V14-WR1000_nuRe200-smallerISRFSR.root",   "START38_V14::All",  "",  "3_8_7_patch1",  ""},
  {1,    7,   1000,  500,  1,       0,    3850,    0.467,  "WR1000_nuRe500",      0, "38x-7TeV-V14-WR1000_nuRe500-smallerISRFSR.root",   "START38_V14::All",  "",  "3_8_7_patch1",  ""},
  {1,    7,   1000,  800,  1,       0,    3900,    0.130,  "WR1000_nuRe800",      0, "38x-7TeV-V14-WR1000_nuRe800-smallerISRFSR.root",   "START38_V14::All",  "",  "3_8_7_patch1",  ""},

  {1,    7,   1500,  200,  1,       0,    3900,    0.07923,  "WR1500_nuRe200",      0, "38x-7TeV-V14-WR1500_nuRe200-largerISRFSR.root",   "START38_V14::All",  "",  "3_8_7_patch1",  ""},
  {1,    7,   1500,  800,  1,       0,    3900,    0.04542,  "WR1500_nuRe800",      0, "38x-7TeV-V14-WR1500_nuRe800-largerISRFSR.root",   "START38_V14::All",  "",  "3_8_7_patch1",  ""},
  {1,    7,   1500, 1300,  1,       0,    3800,    0.006757, "WR1500_nuRe1300",    0, "38x-7TeV-V14-WR1500_nuRe1300-largerISRFSR.root",   "START38_V14::All",  "",  "3_8_7_patch1",  ""},
  {1,    7,   1500,  200,  1,       0,    3950,    0.07923,  "WR1500_nuRe200",      0, "38x-7TeV-V14-WR1500_nuRe200-smallerISRFSR.root",   "START38_V14::All",  "",  "3_8_7_patch1",  ""},
  {1,    7,   1500,  800,  1,       0,    3950,    0.04542,  "WR1500_nuRe800",      0, "38x-7TeV-V14-WR1500_nuRe800-smallerISRFSR.root",   "START38_V14::All",  "",  "3_8_7_patch1",  ""},
  {1,    7,   1500, 1300,  1,       0,    3950,    0.006757, "WR1500_nuRe1300",     0, "38x-7TeV-V14-WR1500_nuRe1300-smallerISRFSR.root",   "START38_V14::All",  "",  "3_8_7_patch1",  ""},

  {1,    7,   500,   100,  1,       0,    3850,    14.52, "WR500_nuRe100",      0, "38x-7TeV-V14-WR500_nuRe100-largerISRFSR.root",   "START38_V14::All",  "",  "3_8_7_patch1",  ""},
  {1,    7,   500,   300,  1,       0,    3900,    7.608, "WR500_nuRe300",      0, "38x-7TeV-V14-WR500_nuRe300-largerISRFSR.root",   "START38_V14::All",  "",  "3_8_7_patch1",  ""},
  {1,    7,   500,   400,  1,       0,    3650,    2.797, "WR500_nuRe400",      0, "38x-7TeV-V14-WR500_nuRe400-largerISRFSR.root",   "START38_V14::All",  "",  "3_8_7_patch1",  ""},
  {1,    7,   500,   100,  1,       0,    4000,    14.52, "WR500_nuRe100",      0, "38x-7TeV-V14-WR500_nuRe100-smallerISRFSR.root",   "START38_V14::All",  "",  "3_8_7_patch1",  ""},
  {1,    7,   500,   300,  1,       0,    3950,    7.608, "WR500_nuRe300",      0, "38x-7TeV-V14-WR500_nuRe300-smallerISRFSR.root",   "START38_V14::All",  "",  "3_8_7_patch1",  ""},
  {1,    7,   500,   400,  1,       0,    3900,    2.797, "WR500_nuRe400",      0, "38x-7TeV-V14-WR500_nuRe400-smallerISRFSR.root",   "START38_V14::All",  "",  "3_8_7_patch1",  ""},


  // CMSSW_3_9_7
  {1,  7,  1200,  500,  1,  0,  10000,  .214,  "WR1200_nuRe500",  0,  "39x-7TeV-V9-WR1200_nuRe500-Z2.root",  "START39_V9::All",  "GRun",  "CMSSW_3_9_7",  ""},
  {1,  7,  1200,  600,  1,  0,  10000,  .1824,  "WR1200_nuRe600",  0,  "39x-7TeV-V9-WR1200_nuRe600-Z2.root",  "START39_V9::All",  "GRun",  "CMSSW_3_9_7",  ""},
  {1,  7,  1200,  700,  1,  0,  10000,  .1463,  "WR1200_nuRe700",  0,  "39x-7TeV-V9-WR1200_nuRe700-Z2.root",  "START39_V9::All",  "GRun",  "CMSSW_3_9_7",  ""},
  {1,  7,  1200,  800,  1,  0,  10000,  .1091,  "WR1200_nuRe800",  0,  "39x-7TeV-V9-WR1200_nuRe800-Z2.root",  "START39_V9::All",  "GRun",  "CMSSW_3_9_7",  ""},

  {1,  7,  1300,  500,  1,  0,  10000,  .144,  "WR1300_nuRe500",  0,  "39x-7TeV-V9-WR1300_nuRe500-Z2.root",  "START39_V9::All",  "GRun",  "CMSSW_3_9_7",  ""},
  {1,  7,  1300,  600,  1,  0,  10000,  .1263,  "WR1300_nuRe600",  0,  "39x-7TeV-V9-WR1300_nuRe600-Z2.root",  "START39_V9::All",  "GRun",  "CMSSW_3_9_7",  ""},
  {1,  7,  1300,  700,  1,  0,  10000,  .107,  "WR1300_nuRe700",  0,  "39x-7TeV-V9-WR1300_nuRe700-Z2.root",  "START39_V9::All",  "GRun",  "CMSSW_3_9_7",  ""},
  {1,  7,  1300,  800,  1,  0,  10000,  .0851,  "WR1300_nuRe800",  0,  "39x-7TeV-V9-WR1300_nuRe800-Z2.root",  "START39_V9::All",  "GRun",  "CMSSW_3_9_7",  ""},

  {1,  7,  1400,  500,  1,  0,  10000,  .0974,  "WR1400_nuRe500",  0,  "39x-7TeV-V9-WR1400_nuRe500-Z2.root",  "START39_V9::All",  "GRun",  "CMSSW_3_9_7",  ""},
  {1,  7,  1400,  600,  1,  0,  10000,  .08721,  "WR1400_nuRe600",  0,  "39x-7TeV-V9-WR1400_nuRe600-Z2.root",  "START39_V9::All",  "GRun",  "CMSSW_3_9_7",  ""},
  {1,  7,  1400,  700,  1,  0,   9500,  .07571,  "WR1400_nuRe700",  0,  "39x-7TeV-V9-WR1400_nuRe700-Z2.root",  "START39_V9::All",  "GRun",  "CMSSW_3_9_7",  ""},
  {1,  7,  1400,  800,  1,  0,  10000,  .06294,  "WR1400_nuRe800",  0,  "39x-7TeV-V9-WR1400_nuRe800-Z2.root",  "START39_V9::All",  "GRun",  "CMSSW_3_9_7",  ""},

  {1,  7,  1500,  500,  1,  0,  10000,  .06638,  "WR1500_nuRe500",  0,  "39x-7TeV-V9-WR1500_nuRe500-Z2.root",  "START39_V9::All",  "GRun",  "CMSSW_3_9_7",  ""},
  {1,  7,  1500,  600,  1,  0,  10000,  .05989,  "WR1500_nuRe600",  0,  "39x-7TeV-V9-WR1500_nuRe600-Z2.root",  "START39_V9::All",  "GRun",  "CMSSW_3_9_7",  ""},
  {1,  7,  1500,  700,  1,  0,  10000,  .05337,  "WR1500_nuRe700",  0,  "39x-7TeV-V9-WR1500_nuRe700-Z2.root",  "START39_V9::All",  "GRun",  "CMSSW_3_9_7",  ""},
  {1,  7,  1500,  800,  1,  0,  10000,  .04605,  "WR1500_nuRe800",  0,  "39x-7TeV-V9-WR1500_nuRe800-Z2.root",  "START39_V9::All",  "GRun",  "CMSSW_3_9_7",  ""},

  {1,  7,  1600,  500,  1,  0,   9500,  .04491,  "WR1600_nuRe500",  0,  "39x-7TeV-V9-WR1600_nuRe500-Z2.root",  "START39_V9::All",  "GRun",  "CMSSW_3_9_7",  ""},
  {1,  7,  1600,  600,  1,  0,   8500,   .0411,  "WR1600_nuRe600",  0,  "39x-7TeV-V9-WR1600_nuRe600-Z2.root",  "START39_V9::All",  "GRun",  "CMSSW_3_9_7",  ""},
  {1,  7,  1600,  700,  1,  0,   6500,   .0371,  "WR1600_nuRe700",  0,  "39x-7TeV-V9-WR1600_nuRe700-Z2.root",  "START39_V9::All",  "GRun",  "CMSSW_3_9_7",  ""},
  {1,  7,  1600,  800,  1,  0,  6000,  .03264,  "WR1600_nuRe800",  0,  "39x-7TeV-V9-WR1600_nuRe800-Z2.root",  "START39_V9::All",  "GRun",  "CMSSW_3_9_7",  ""},

  {1,  7,  1200,  500,  1,  0,  10000,  .2158,  "WR1200_nuRe500",  0,  "39x-7TeV-V9-WR1200_nuRe500-D6.root",  "START39_V9::All",  "GRun",  "CMSSW_3_9_7",  ""},
  {1,  7,  1200,  600,  1,  0,  10000,  .1824,  "WR1200_nuRe600",  0,  "39x-7TeV-V9-WR1200_nuRe600-D6.root",  "START39_V9::All",  "GRun",  "CMSSW_3_9_7",  ""},

  {1,  7,  1300,  500,  1,  0,  10000,  .1448,  "WR1300_nuRe500",  0,  "39x-7TeV-V9-WR1300_nuRe500-D6.root",  "START39_V9::All",  "GRun",  "CMSSW_3_9_7",  ""},
  {1,  7,  1300,  600,  1,  0,  10000,  .1265,  "WR1300_nuRe600",  0,  "39x-7TeV-V9-WR1300_nuRe600-D6.root",  "START39_V9::All",  "GRun",  "CMSSW_3_9_7",  ""},


  // CMSSW_4_1_5
  {1,  7,  500,  50,  1,  0,  5000,  15.15,  "WR500_nuRe50",  0,  "41x-7TeV-START311_V2-WR500_nuRe50.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  500,  100,  1,  0,  5000,  14.43,  "WR500_nuRe100",  0,  "41x-7TeV-START311_V2-WR500_nuRe100.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  500,  400,  1,  0,  5000,  2.785,  "WR500_nuRe400",  0,  "41x-7TeV-START311_V2-WR500_nuRe400.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  500,  450,  1,  0,  5000,  .8947,  "WR500_nuRe450",  0,  "41x-7TeV-START311_V2-WR500_nuRe450.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  800,  50,  1,  0,  5000,  2.136,  "WR800_nuRe50",  0,  "41x-7TeV-START311_V2-WR800_nuRe50.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  800,  100,  1,  0,  5000,  2.085,  "WR800_nuRe100",  0,  "41x-7TeV-START311_V2-WR800_nuRe100.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  800,  400,  1,  0,  5000,  1.342,  "WR800_nuRe400",  0,  "41x-7TeV-START311_V2-WR800_nuRe400.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  800,  700,  1,  0,  5000,  .1735,  "WR800_nuRe700",  0,  "41x-7TeV-START311_V2-WR800_nuRe700.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  800,  750,  1,  0,  5000,  .05753,  "WR800_nuRe750",  0,  "41x-7TeV-START311_V2-WR800_nuRe750.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1100,  50,  1,  0,  5000,  .468,  "WR1100_nuRe50",  0,  "41x-7TeV-START311_V2-WR1100_nuRe50.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1100,  100,  1,  0,  5000,  .4619,  "WR1100_nuRe100",  0,  "41x-7TeV-START311_V2-WR1100_nuRe100.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1100,  400,  1,  0,  5000,  .3669,  "WR1100_nuRe400",  0,  "41x-7TeV-START311_V2-WR1100_nuRe400.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1100,  700,  1,  0,  5000,  .1968,  "WR1100_nuRe700",  0,  "41x-7TeV-START311_V2-WR1100_nuRe700.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1100,  1000,  1,  0,  5000,  .02148,  "WR1100_nuRe1000",  0,  "41x-7TeV-START311_V2-WR1100_nuRe1000.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1100,  1050,  1,  0,  5000,  .007606,  "WR1100_nuRe1050",  0,  "41x-7TeV-START311_V2-WR1100_nuRe1050.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1400,  50,  1,  0,  5000,  .1272,  "WR1400_nuRe50",  0,  "41x-7TeV-START311_V2-WR1400_nuRe50.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1400,  100,  1,  0,  4900,  .125,  "WR1400_nuRe100",  0,  "41x-7TeV-START311_V2-WR1400_nuRe100.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1400,  400,  1,  0,  5000,  .1061,  "WR1400_nuRe400",  0,  "41x-7TeV-START311_V2-WR1400_nuRe400.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1400,  700,  1,  0,  5000,  .07602,  "WR1400_nuRe700",  0,  "41x-7TeV-START311_V2-WR1400_nuRe700.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1400,  1000,  1,  0,  5000,  .03601,  "WR1400_nuRe1000",  0,  "41x-7TeV-START311_V2-WR1400_nuRe1000.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1400,  1300,  1,  0,  5000,  .003668,  "WR1400_nuRe1300",  0,  "41x-7TeV-START311_V2-WR1400_nuRe1300.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1400,  1350,  1,  0,  4800,  .001369,  "WR1400_nuRe1350",  0,  "41x-7TeV-START311_V2-WR1400_nuRe1350.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1700,  50,  1,  0,  5000,  .03859,  "WR1700_nuRe50",  0,  "41x-7TeV-START311_V2-WR1700_nuRe50.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1700,  100,  1,  0,  5000,  .03779,  "WR1700_nuRe100",  0,  "41x-7TeV-START311_V2-WR1700_nuRe100.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1700,  400,  1,  0,  5000,  .0327,  "WR1700_nuRe400",  0,  "41x-7TeV-START311_V2-WR1700_nuRe400.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1700,  700,  1,  0,  5000,  .02588,  "WR1700_nuRe700",  0,  "41x-7TeV-START311_V2-WR1700_nuRe700.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1700,  1000,  1,  0,  5000,  .0171,  "WR1700_nuRe1000",  0,  "41x-7TeV-START311_V2-WR1700_nuRe1000.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1700,  1300,  1,  0,  5000,  .007529,  "WR1700_nuRe1300",  0,  "41x-7TeV-START311_V2-WR1700_nuRe1300.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1700,  1600,  1,  0,  5000,  .0007384,  "WR1700_nuRe1600",  0,  "41x-7TeV-START311_V2-WR1700_nuRe1600.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1700,  1650,  1,  0,  5000,  .0002912,  "WR1700_nuRe1650",  0,  "41x-7TeV-START311_V2-WR1700_nuRe1650.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2000,  50,  1,  0,  5000,  .01276,  "WR2000_nuRe50",  0,  "41x-7TeV-START311_V2-WR2000_nuRe50.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2000,  100,  1,  0,  5000,  .01235,  "WR2000_nuRe100",  0,  "41x-7TeV-START311_V2-WR2000_nuRe100.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2000,  400,  1,  0,  5000,  .01041,  "WR2000_nuRe400",  0,  "41x-7TeV-START311_V2-WR2000_nuRe400.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2000,  700,  1,  0,  5000,  .008595,  "WR2000_nuRe700",  0,  "41x-7TeV-START311_V2-WR2000_nuRe700.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2000,  1000,  1,  0,  5000,  .006482,  "WR2000_nuRe1000",  0,  "41x-7TeV-START311_V2-WR2000_nuRe1000.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2000,  1300,  1,  0,  5000,  .004069,  "WR2000_nuRe1300",  0,  "41x-7TeV-START311_V2-WR2000_nuRe1300.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2000,  1600,  1,  0,  5000,  .0017,  "WR2000_nuRe1600",  0,  "41x-7TeV-START311_V2-WR2000_nuRe1600.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2000,  1900,  1,  0,  5000,  .0001625,  "WR2000_nuRe1900",  0,  "41x-7TeV-START311_V2-WR2000_nuRe1900.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2000,  1950,  1,  0,  5000,  .00006677,  "WR2000_nuRe1950",  0,  "41x-7TeV-START311_V2-WR2000_nuRe1950.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2300,  50,  1,  0,  4900,  .004591,  "WR2300_nuRe50",  0,  "41x-7TeV-START311_V2-WR2300_nuRe50.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2300,  100,  1,  0,  5000,  .004357,  "WR2300_nuRe100",  0,  "41x-7TeV-START311_V2-WR2300_nuRe100.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2300,  400,  1,  0,  5000,  .003476,  "WR2300_nuRe400",  0,  "41x-7TeV-START311_V2-WR2300_nuRe400.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2300,  700,  1,  0,  5000,  .002841,  "WR2300_nuRe700",  0,  "41x-7TeV-START311_V2-WR2300_nuRe700.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2300,  1000,  1,  0,  5000,  .002258,  "WR2300_nuRe1000",  0,  "41x-7TeV-START311_V2-WR2300_nuRe1000.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2300,  1300,  1,  0,  5000,  .001633,  "WR2300_nuRe1300",  0,  "41x-7TeV-START311_V2-WR2300_nuRe1300.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2300,  1600,  1,  0,  5000,  .0009792,  "WR2300_nuRe1600",  0,  "41x-7TeV-START311_V2-WR2300_nuRe1600.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2300,  1900,  1,  0,  5000,  .0003925,  "WR2300_nuRe1900",  0,  "41x-7TeV-START311_V2-WR2300_nuRe1900.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2300,  2200,  1,  0,  5000,  .00003701,  "WR2300_nuRe2200",  0,  "41x-7TeV-START311_V2-WR2300_nuRe2200.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2300,  2250,  1,  0,  5000,  .00001579,  "WR2300_nuRe2250",  0,  "41x-7TeV-START311_V2-WR2300_nuRe2250.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  
  // with Pile-Up "Flat to 10 plus tail" scenario, details:
  //   https://twiki.cern.ch/twiki/bin/view/CMS/PileupInformation#2011_Pileup_Scenarios
  {1,  7,  500,  50,  1,  0,  5000,  15.26,  "WR500_nuRe50",  0,  "41x-7TeV-START311_V2-PU-WR500_nuRe50.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  500,  100,  1,  0,  5000,  14.5,  "WR500_nuRe100",  0,  "41x-7TeV-START311_V2-PU-WR500_nuRe100.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  500,  400,  1,  0,  4900,  2.785,  "WR500_nuRe400",  0,  "41x-7TeV-START311_V2-PU-WR500_nuRe400.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  500,  450,  1,  0,  5000,  .9006,  "WR500_nuRe450",  0,  "41x-7TeV-START311_V2-PU-WR500_nuRe450.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  800,  50,  1,  0,  4900,  2.135,  "WR800_nuRe50",  0,  "41x-7TeV-START311_V2-PU-WR800_nuRe50.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  800,  100,  1,  0,  4900,  2.086,  "WR800_nuRe100",  0,  "41x-7TeV-START311_V2-PU-WR800_nuRe100.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  800,  400,  1,  0,  4900,  1.341,  "WR800_nuRe400",  0,  "41x-7TeV-START311_V2-PU-WR800_nuRe400.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  800,  700,  1,  0,  4400,  .1731,  "WR800_nuRe700",  0,  "41x-7TeV-START311_V2-PU-WR800_nuRe700.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  800,  750,  1,  0,  5000,  .05756,  "WR800_nuRe750",  0,  "41x-7TeV-START311_V2-PU-WR800_nuRe750.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1100,  50,  1,  0,  4600,  .4701,  "WR1100_nuRe50",  0,  "41x-7TeV-START311_V2-PU-WR1100_nuRe50.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1100,  100,  1,  0,  4700,  .4619,  "WR1100_nuRe100",  0,  "41x-7TeV-START311_V2-PU-WR1100_nuRe100.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1100,  400,  1,  0,  4600,  .3678,  "WR1100_nuRe400",  0,  "41x-7TeV-START311_V2-PU-WR1100_nuRe400.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1100,  700,  1,  0,  3900,  .1963,  "WR1100_nuRe700",  0,  "41x-7TeV-START311_V2-PU-WR1100_nuRe700.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1100,  1000,  1,  0,  4400,  .02159,  "WR1100_nuRe1000",  0,  "41x-7TeV-START311_V2-PU-WR1100_nuRe1000.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1100,  1050,  1,  0,  2000,  .007594,  "WR1100_nuRe1050",  0,  "41x-7TeV-START311_V2-PU-WR1100_nuRe1050.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1400,  50,  1,  0,  1900,  .1277,  "WR1400_nuRe50",  0,  "41x-7TeV-START311_V2-PU-WR1400_nuRe50.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1400,  100,  1,  0,  2100,  .1249,  "WR1400_nuRe100",  0,  "41x-7TeV-START311_V2-PU-WR1400_nuRe100.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1400,  400,  1,  0,  2800,  .106,  "WR1400_nuRe400",  0,  "41x-7TeV-START311_V2-PU-WR1400_nuRe400.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1400,  700,  1,  0,  2200,  .07578,  "WR1400_nuRe700",  0,  "41x-7TeV-START311_V2-PU-WR1400_nuRe700.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1400,  1000,  1,  0,  3300,  .03622,  "WR1400_nuRe1000",  0,  "41x-7TeV-START311_V2-PU-WR1400_nuRe1000.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1400,  1300,  1,  0,  3200,  .003661,  "WR1400_nuRe1300",  0,  "41x-7TeV-START311_V2-PU-WR1400_nuRe1300.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1400,  1350,  1,  0,  3800,  .001372,  "WR1400_nuRe1350",  0,  "41x-7TeV-START311_V2-PU-WR1400_nuRe1350.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1700,  50,  1,  0,  3600,  .03856,  "WR1700_nuRe50",  0,  "41x-7TeV-START311_V2-PU-WR1700_nuRe50.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1700,  100,  1,  0,  2800,  .03793,  "WR1700_nuRe100",  0,  "41x-7TeV-START311_V2-PU-WR1700_nuRe100.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1700,  400,  1,  0,  3800,  .03266,  "WR1700_nuRe400",  0,  "41x-7TeV-START311_V2-PU-WR1700_nuRe400.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1700,  700,  1,  0,  4100,  .02589,  "WR1700_nuRe700",  0,  "41x-7TeV-START311_V2-PU-WR1700_nuRe700.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1700,  1000,  1,  0,  3800,  .01716,  "WR1700_nuRe1000",  0,  "41x-7TeV-START311_V2-PU-WR1700_nuRe1000.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1700,  1300,  1,  0,  3300,  .007573,  "WR1700_nuRe1300",  0,  "41x-7TeV-START311_V2-PU-WR1700_nuRe1300.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1700,  1600,  1,  0,  2500,  .0007372,  "WR1700_nuRe1600",  0,  "41x-7TeV-START311_V2-PU-WR1700_nuRe1600.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  1700,  1650,  1,  0,  3600,  .0002917,  "WR1700_nuRe1650",  0,  "41x-7TeV-START311_V2-PU-WR1700_nuRe1650.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2000,  50,  1,  0,  4200,  .01275,  "WR2000_nuRe50",  0,  "41x-7TeV-START311_V2-PU-WR2000_nuRe50.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2000,  100,  1,  0,  2600,  .01232,  "WR2000_nuRe100",  0,  "41x-7TeV-START311_V2-PU-WR2000_nuRe100.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2000,  400,  1,  0,  1900,  .01039,  "WR2000_nuRe400",  0,  "41x-7TeV-START311_V2-PU-WR2000_nuRe400.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2000,  700,  1,  0,  1300,  .008603,  "WR2000_nuRe700",  0,  "41x-7TeV-START311_V2-PU-WR2000_nuRe700.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2000,  1000,  1,  0,  1300,  .006492,  "WR2000_nuRe1000",  0,  "41x-7TeV-START311_V2-PU-WR2000_nuRe1000.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2000,  1300,  1,  0,  1300,  .004067,  "WR2000_nuRe1300",  0,  "41x-7TeV-START311_V2-PU-WR2000_nuRe1300.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2000,  1600,  1,  0,  500,  .001696,  "WR2000_nuRe1600",  0,  "41x-7TeV-START311_V2-PU-WR2000_nuRe1600.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2000,  1900,  1,  0,  200,  .0001622,  "WR2000_nuRe1900",  0,  "41x-7TeV-START311_V2-PU-WR2000_nuRe1900.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2000,  1950,  1,  0,  200,  .00006685,  "WR2000_nuRe1950",  0,  "41x-7TeV-START311_V2-PU-WR2000_nuRe1950.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2300,  50,  1,  0,  600,  .004585,  "WR2300_nuRe50",  0,  "41x-7TeV-START311_V2-PU-WR2300_nuRe50.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2300,  100,  1,  0,  800,  .004343,  "WR2300_nuRe100",  0,  "41x-7TeV-START311_V2-PU-WR2300_nuRe100.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2300,  400,  1,  0,  500,  .003468,  "WR2300_nuRe400",  0,  "41x-7TeV-START311_V2-PU-WR2300_nuRe400.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2300,  700,  1,  0,  500,  .002839,  "WR2300_nuRe700",  0,  "41x-7TeV-START311_V2-PU-WR2300_nuRe700.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2300,  1000,  1,  0,  800,  .002264,  "WR2300_nuRe1000",  0,  "41x-7TeV-START311_V2-PU-WR2300_nuRe1000.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2300,  1300,  1,  0,  1000,  .001635,  "WR2300_nuRe1300",  0,  "41x-7TeV-START311_V2-PU-WR2300_nuRe1300.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2300,  1600,  1,  0,  1800,  .000984,  "WR2300_nuRe1600",  0,  "41x-7TeV-START311_V2-PU-WR2300_nuRe1600.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2300,  1900,  1,  0,  1600,  .0003926,  "WR2300_nuRe1900",  0,  "41x-7TeV-START311_V2-PU-WR2300_nuRe1900.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2300,  2200,  1,  0,  1700,  .00003703,  "WR2300_nuRe2200",  0,  "41x-7TeV-START311_V2-PU-WR2300_nuRe2200.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},
  {1,  7,  2300,  2250,  1,  0,  1500,  .00001577,  "WR2300_nuRe2250",  0,  "41x-7TeV-START311_V2-PU-WR2300_nuRe2250.root",  "START311_V2::All",  "GRun",  "CMSSW_4_1_5",  ""},

  // CMSSW_4_1_6 (now with mc and reco primary vertices)
  // full grid: WR = [700 ... 2000], nuR = [50 ... WR-50], step 100 GeV (203 mass points in total)
  // with Pile-Up "Flat to 10 plus tail" scenario
  {1,  7,  700,  50,  1,  0,  13200,  3.826,  "WR700_nuRe50",  0,  "4_1_6-START41_V0-PU-WR700_nuRe50.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  700,  100,  1,  0,  13500,  3.718,  "WR700_nuRe100",  0,  "4_1_6-START41_V0-PU-WR700_nuRe100.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  700,  200,  1,  0,  13200,  3.337,  "WR700_nuRe200",  0,  "4_1_6-START41_V0-PU-WR700_nuRe200.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  700,  300,  1,  0,  12400,  2.791,  "WR700_nuRe300",  0,  "4_1_6-START41_V0-PU-WR700_nuRe300.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  700,  400,  1,  0,  13200,  2.045,  "WR700_nuRe400",  0,  "4_1_6-START41_V0-PU-WR700_nuRe400.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  700,  500,  1,  0,  13600,  1.185,  "WR700_nuRe500",  0,  "4_1_6-START41_V0-PU-WR700_nuRe500.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  700,  600,  1,  0,  13700,  .3936,  "WR700_nuRe600",  0,  "4_1_6-START41_V0-PU-WR700_nuRe600.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  700,  650,  1,  0,  12300,  .1284,  "WR700_nuRe650",  0,  "4_1_6-START41_V0-PU-WR700_nuRe650.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},

  {1,  7,  800,  50,  1,  0,  10800,  2.144,  "WR800_nuRe50",  0,  "4_1_6-START41_V0-PU-WR800_nuRe50.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  800,  100,  1,  0,  11700,  2.091,  "WR800_nuRe100",  0,  "4_1_6-START41_V0-PU-WR800_nuRe100.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  800,  200,  1,  0,  11300,  1.927,  "WR800_nuRe200",  0,  "4_1_6-START41_V0-PU-WR800_nuRe200.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  800,  300,  1,  0,  10600,  1.683,  "WR800_nuRe300",  0,  "4_1_6-START41_V0-PU-WR800_nuRe300.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  800,  400,  1,  0,  9400,  1.359,  "WR800_nuRe400",  0,  "4_1_6-START41_V0-PU-WR800_nuRe400.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  800,  500,  1,  0,  8200,  .9553,  "WR800_nuRe500",  0,  "4_1_6-START41_V0-PU-WR800_nuRe500.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  800,  600,  1,  0,  7600,  .5328,  "WR800_nuRe600",  0,  "4_1_6-START41_V0-PU-WR800_nuRe600.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  800,  700,  1,  0,  5600,  .1729,  "WR800_nuRe700",  0,  "4_1_6-START41_V0-PU-WR800_nuRe700.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  800,  750,  1,  0,  4800,  .05741,  "WR800_nuRe750",  0,  "4_1_6-START41_V0-PU-WR800_nuRe750.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},

  {1,  7,  900,  50,  1,  0,  4200,  1.248,  "WR900_nuRe50",  0,  "4_1_6-START41_V0-PU-WR900_nuRe50.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  900,  100,  1,  0,  4900,  1.224,  "WR900_nuRe100",  0,  "4_1_6-START41_V0-PU-WR900_nuRe100.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  900,  200,  1,  0,  5800,  1.147,  "WR900_nuRe200",  0,  "4_1_6-START41_V0-PU-WR900_nuRe200.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  900,  300,  1,  0,  5200,  1.029,  "WR900_nuRe300",  0,  "4_1_6-START41_V0-PU-WR900_nuRe300.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  900,  400,  1,  0,  5000,  .874,  "WR900_nuRe400",  0,  "4_1_6-START41_V0-PU-WR900_nuRe400.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  900,  500,  1,  0,  4800,  .6858,  "WR900_nuRe500",  0,  "4_1_6-START41_V0-PU-WR900_nuRe500.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  900,  600,  1,  0,  4800,  .4724,  "WR900_nuRe600",  0,  "4_1_6-START41_V0-PU-WR900_nuRe600.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  900,  700,  1,  0,  3600,  .2573,  "WR900_nuRe700",  0,  "4_1_6-START41_V0-PU-WR900_nuRe700.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  900,  800,  1,  0,  4900,  .082,  "WR900_nuRe800",  0,  "4_1_6-START41_V0-PU-WR900_nuRe800.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  900,  850,  1,  0,  4900,  .0279,  "WR900_nuRe850",  0,  "4_1_6-START41_V0-PU-WR900_nuRe850.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},

  {1,  7,  1000,  50,  1,  0,  5100,  .7525,  "WR1000_nuRe50",  0,  "4_1_6-START41_V0-PU-WR1000_nuRe50.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1000,  100,  1,  0,  4500,  .7405,  "WR1000_nuRe100",  0,  "4_1_6-START41_V0-PU-WR1000_nuRe100.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1000,  200,  1,  0,  4800,  .699,  "WR1000_nuRe200",  0,  "4_1_6-START41_V0-PU-WR1000_nuRe200.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1000,  300,  1,  0,  7300,  .6397,  "WR1000_nuRe300",  0,  "4_1_6-START41_V0-PU-WR1000_nuRe300.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1000,  400,  1,  0,  8600,  .5658,  "WR1000_nuRe400",  0,  "4_1_6-START41_V0-PU-WR1000_nuRe400.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1000,  500,  1,  0,  8300,  .4703,  "WR1000_nuRe500",  0,  "4_1_6-START41_V0-PU-WR1000_nuRe500.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1000,  600,  1,  0,  7000,  .3602,  "WR1000_nuRe600",  0,  "4_1_6-START41_V0-PU-WR1000_nuRe600.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1000,  700,  1,  0,  8600,  .2436,  "WR1000_nuRe700",  0,  "4_1_6-START41_V0-PU-WR1000_nuRe700.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1000,  800,  1,  0,  5500,  .1296,  "WR1000_nuRe800",  0,  "4_1_6-START41_V0-PU-WR1000_nuRe800.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1000,  900,  1,  0,  5000,  .0413,  "WR1000_nuRe900",  0,  "4_1_6-START41_V0-PU-WR1000_nuRe900.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1000,  950,  1,  0,  5700,  .01423,  "WR1000_nuRe950",  0,  "4_1_6-START41_V0-PU-WR1000_nuRe950.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},

  {1,  7,  1100,  50,  1,  0,  5500,  .4691,  "WR1100_nuRe50",  0,  "4_1_6-START41_V0-PU-WR1100_nuRe50.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1100,  100,  1,  0,  5300,  .4644,  "WR1100_nuRe100",  0,  "4_1_6-START41_V0-PU-WR1100_nuRe100.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1100,  200,  1,  0,  4100,  .4406,  "WR1100_nuRe200",  0,  "4_1_6-START41_V0-PU-WR1100_nuRe200.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1100,  300,  1,  0,  4100,  .4084,  "WR1100_nuRe300",  0,  "4_1_6-START41_V0-PU-WR1100_nuRe300.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1100,  400,  1,  0,  1400,  .3649,  "WR1100_nuRe400",  0,  "4_1_6-START41_V0-PU-WR1100_nuRe400.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1100,  500,  1,  0,  1800,  .3176,  "WR1100_nuRe500",  0,  "4_1_6-START41_V0-PU-WR1100_nuRe500.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1100,  600,  1,  0,  1000,  .2601,  "WR1100_nuRe600",  0,  "4_1_6-START41_V0-PU-WR1100_nuRe600.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1100,  700,  1,  0,  3700,  .1971,  "WR1100_nuRe700",  0,  "4_1_6-START41_V0-PU-WR1100_nuRe700.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1100,  800,  1,  0,  6800,  .1296,  "WR1100_nuRe800",  0,  "4_1_6-START41_V0-PU-WR1100_nuRe800.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1100,  900,  1,  0,  6100,  .0682,  "WR1100_nuRe900",  0,  "4_1_6-START41_V0-PU-WR1100_nuRe900.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1100,  1000,  1,  0,  5400,  .02144,  "WR1100_nuRe1000",  0,  "4_1_6-START41_V0-PU-WR1100_nuRe1000.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1100,  1050,  1,  0,  4800,  .007555,  "WR1100_nuRe1050",  0,  "4_1_6-START41_V0-PU-WR1100_nuRe1050.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},

  {1,  7,  1200,  50,  1,  0,  3600,  .2986,  "WR1200_nuRe50",  0,  "4_1_6-START41_V0-PU-WR1200_nuRe50.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1200,  100,  1,  0,  2800,  .2937,  "WR1200_nuRe100",  0,  "4_1_6-START41_V0-PU-WR1200_nuRe100.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1200,  200,  1,  0,  1900,  .2825,  "WR1200_nuRe200",  0,  "4_1_6-START41_V0-PU-WR1200_nuRe200.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1200,  300,  1,  0,  3100,  .2641,  "WR1200_nuRe300",  0,  "4_1_6-START41_V0-PU-WR1200_nuRe300.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1200,  400,  1,  0,  3300,  .2405,  "WR1200_nuRe400",  0,  "4_1_6-START41_V0-PU-WR1200_nuRe400.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1200,  500,  1,  0,  1400,  .2139,  "WR1200_nuRe500",  0,  "4_1_6-START41_V0-PU-WR1200_nuRe500.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1200,  600,  1,  0,  1900,  .1823,  "WR1200_nuRe600",  0,  "4_1_6-START41_V0-PU-WR1200_nuRe600.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1200,  700,  1,  0,  2200,  .1479,  "WR1200_nuRe700",  0,  "4_1_6-START41_V0-PU-WR1200_nuRe700.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1200,  800,  1,  0,  4200,  .1089,  "WR1200_nuRe800",  0,  "4_1_6-START41_V0-PU-WR1200_nuRe800.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1200,  900,  1,  0,  6100,  .07178,  "WR1200_nuRe900",  0,  "4_1_6-START41_V0-PU-WR1200_nuRe900.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1200,  1000,  1,  0,  8200,  .03684,  "WR1200_nuRe1000",  0,  "4_1_6-START41_V0-PU-WR1200_nuRe1000.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1200,  1100,  1,  0,  8500,  .01159,  "WR1200_nuRe1100",  0,  "4_1_6-START41_V0-PU-WR1200_nuRe1100.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1200,  1150,  1,  0,  5800,  .004198,  "WR1200_nuRe1150",  0,  "4_1_6-START41_V0-PU-WR1200_nuRe1150.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},

  {1,  7,  1300,  50,  1,  0,  3300,  .194,  "WR1300_nuRe50",  0,  "4_1_6-START41_V0-PU-WR1300_nuRe50.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1300,  100,  1,  0,  3100,  .1908,  "WR1300_nuRe100",  0,  "4_1_6-START41_V0-PU-WR1300_nuRe100.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1300,  200,  1,  0,  2700,  .1825,  "WR1300_nuRe200",  0,  "4_1_6-START41_V0-PU-WR1300_nuRe200.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1300,  300,  1,  0,  2200,  .1726,  "WR1300_nuRe300",  0,  "4_1_6-START41_V0-PU-WR1300_nuRe300.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1300,  400,  1,  0,  2400,  .16,  "WR1300_nuRe400",  0,  "4_1_6-START41_V0-PU-WR1300_nuRe400.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1300,  500,  1,  0,  2100,  .1446,  "WR1300_nuRe500",  0,  "4_1_6-START41_V0-PU-WR1300_nuRe500.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1300,  600,  1,  0,  3300,  .1269,  "WR1300_nuRe600",  0,  "4_1_6-START41_V0-PU-WR1300_nuRe600.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1300,  700,  1,  0,  4600,  .1067,  "WR1300_nuRe700",  0,  "4_1_6-START41_V0-PU-WR1300_nuRe700.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1300,  800,  1,  0,  4600,  .08494,  "WR1300_nuRe800",  0,  "4_1_6-START41_V0-PU-WR1300_nuRe800.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1300,  900,  1,  0,  4700,  .06217,  "WR1300_nuRe900",  0,  "4_1_6-START41_V0-PU-WR1300_nuRe900.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1300,  1000,  1,  0,  4200,  .04035,  "WR1300_nuRe1000",  0,  "4_1_6-START41_V0-PU-WR1300_nuRe1000.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1300,  1100,  1,  0,  3200,  .02069,  "WR1300_nuRe1100",  0,  "4_1_6-START41_V0-PU-WR1300_nuRe1100.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1300,  1200,  1,  0,  2100,  .006459,  "WR1300_nuRe1200",  0,  "4_1_6-START41_V0-PU-WR1300_nuRe1200.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1300,  1250,  1,  0,  3200,  .002375,  "WR1300_nuRe1250",  0,  "4_1_6-START41_V0-PU-WR1300_nuRe1250.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},

  {1,  7,  1400,  50,  1,  0,  1700,  .1281,  "WR1400_nuRe50",  0,  "4_1_6-START41_V0-PU-WR1400_nuRe50.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1400,  100,  1,  0,  1200,  .1251,  "WR1400_nuRe100",  0,  "4_1_6-START41_V0-PU-WR1400_nuRe100.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1400,  200,  1,  0,  2100,  .1202,  "WR1400_nuRe200",  0,  "4_1_6-START41_V0-PU-WR1400_nuRe200.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1400,  300,  1,  0,  1400,  .1133,  "WR1400_nuRe300",  0,  "4_1_6-START41_V0-PU-WR1400_nuRe300.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1400,  400,  1,  0,  3400,  .1068,  "WR1400_nuRe400",  0,  "4_1_6-START41_V0-PU-WR1400_nuRe400.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1400,  500,  1,  0,  6700,  .0972,  "WR1400_nuRe500",  0,  "4_1_6-START41_V0-PU-WR1400_nuRe500.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1400,  600,  1,  0,  10200,  .08723,  "WR1400_nuRe600",  0,  "4_1_6-START41_V0-PU-WR1400_nuRe600.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1400,  700,  1,  0,  9600,  .07574,  "WR1400_nuRe700",  0,  "4_1_6-START41_V0-PU-WR1400_nuRe700.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1400,  800,  1,  0,  10500,  .06318,  "WR1400_nuRe800",  0,  "4_1_6-START41_V0-PU-WR1400_nuRe800.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1400,  900,  1,  0,  7600,  .04968,  "WR1400_nuRe900",  0,  "4_1_6-START41_V0-PU-WR1400_nuRe900.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1400,  1000,  1,  0,  7500,  .03615,  "WR1400_nuRe1000",  0,  "4_1_6-START41_V0-PU-WR1400_nuRe1000.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1400,  1100,  1,  0,  3000,  .02306,  "WR1400_nuRe1100",  0,  "4_1_6-START41_V0-PU-WR1400_nuRe1100.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1400,  1200,  1,  0,  2400,  .01174,  "WR1400_nuRe1200",  0,  "4_1_6-START41_V0-PU-WR1400_nuRe1200.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1400,  1300,  1,  0,  2200,  .003669,  "WR1400_nuRe1300",  0,  "4_1_6-START41_V0-PU-WR1400_nuRe1300.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1400,  1350,  1,  0,  3400,  .001372,  "WR1400_nuRe1350",  0,  "4_1_6-START41_V0-PU-WR1400_nuRe1350.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},

  {1,  7,  1500,  50,  1,  0,  4200,  .08509,  "WR1500_nuRe50",  0,  "4_1_6-START41_V0-PU-WR1500_nuRe50.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1500,  100,  1,  0,  4900,  .08346,  "WR1500_nuRe100",  0,  "4_1_6-START41_V0-PU-WR1500_nuRe100.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1500,  200,  1,  0,  6000,  .07959,  "WR1500_nuRe200",  0,  "4_1_6-START41_V0-PU-WR1500_nuRe200.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1500,  300,  1,  0,  11200,  .07575,  "WR1500_nuRe300",  0,  "4_1_6-START41_V0-PU-WR1500_nuRe300.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1500,  400,  1,  0,  7600,  .07129,  "WR1500_nuRe400",  0,  "4_1_6-START41_V0-PU-WR1500_nuRe400.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1500,  500,  1,  0,  5600,  .06592,  "WR1500_nuRe500",  0,  "4_1_6-START41_V0-PU-WR1500_nuRe500.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1500,  600,  1,  0,  3500,  .05986,  "WR1500_nuRe600",  0,  "4_1_6-START41_V0-PU-WR1500_nuRe600.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1500,  700,  1,  0,  4300,  .05296,  "WR1500_nuRe700",  0,  "4_1_6-START41_V0-PU-WR1500_nuRe700.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1500,  800,  1,  0,  3000,  .04567,  "WR1500_nuRe800",  0,  "4_1_6-START41_V0-PU-WR1500_nuRe800.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1500,  900,  1,  0,  2600,  .0378,  "WR1500_nuRe900",  0,  "4_1_6-START41_V0-PU-WR1500_nuRe900.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1500,  1000,  1,  0,  5100,  .02946,  "WR1500_nuRe1000",  0,  "4_1_6-START41_V0-PU-WR1500_nuRe1000.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1500,  1100,  1,  0,  5000,  .02123,  "WR1500_nuRe1100",  0,  "4_1_6-START41_V0-PU-WR1500_nuRe1100.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1500,  1200,  1,  0,  8400,  .01341,  "WR1500_nuRe1200",  0,  "4_1_6-START41_V0-PU-WR1500_nuRe1200.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1500,  1300,  1,  0,  9400,  .006765,  "WR1500_nuRe1300",  0,  "4_1_6-START41_V0-PU-WR1500_nuRe1300.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1500,  1400,  1,  0,  5900,  .002131,  "WR1500_nuRe1400",  0,  "4_1_6-START41_V0-PU-WR1500_nuRe1400.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1500,  1450,  1,  0,  2800,  .0008075,  "WR1500_nuRe1450",  0,  "4_1_6-START41_V0-PU-WR1500_nuRe1450.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},

  {1,  7,  1600,  50,  1,  0,  2800,  .05662,  "WR1600_nuRe50",  0,  "4_1_6-START41_V0-PU-WR1600_nuRe50.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1600,  100,  1,  0,  900,  .05574,  "WR1600_nuRe100",  0,  "4_1_6-START41_V0-PU-WR1600_nuRe100.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1600,  200,  1,  0,  1700,  .05349,  "WR1600_nuRe200",  0,  "4_1_6-START41_V0-PU-WR1600_nuRe200.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1600,  300,  1,  0,  1900,  .05098,  "WR1600_nuRe300",  0,  "4_1_6-START41_V0-PU-WR1600_nuRe300.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1600,  400,  1,  0,  1000,  .04806,  "WR1600_nuRe400",  0,  "4_1_6-START41_V0-PU-WR1600_nuRe400.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1600,  500,  1,  0,  1400,  .04487,  "WR1600_nuRe500",  0,  "4_1_6-START41_V0-PU-WR1600_nuRe500.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1600,  600,  1,  0,  1600,  .04118,  "WR1600_nuRe600",  0,  "4_1_6-START41_V0-PU-WR1600_nuRe600.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1600,  700,  1,  0,  3000,  .0373,  "WR1600_nuRe700",  0,  "4_1_6-START41_V0-PU-WR1600_nuRe700.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1600,  800,  1,  0,  8300,  .03266,  "WR1600_nuRe800",  0,  "4_1_6-START41_V0-PU-WR1600_nuRe800.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1600,  900,  1,  0,  10600,  .0278,  "WR1600_nuRe900",  0,  "4_1_6-START41_V0-PU-WR1600_nuRe900.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1600,  1000,  1,  0,  11300,  .02273,  "WR1600_nuRe1000",  0,  "4_1_6-START41_V0-PU-WR1600_nuRe1000.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1600,  1100,  1,  0,  8500,  .01761,  "WR1600_nuRe1100",  0,  "4_1_6-START41_V0-PU-WR1600_nuRe1100.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1600,  1200,  1,  0,  5400,  .01261,  "WR1600_nuRe1200",  0,  "4_1_6-START41_V0-PU-WR1600_nuRe1200.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1600,  1300,  1,  0,  3500,  .007927,  "WR1600_nuRe1300",  0,  "4_1_6-START41_V0-PU-WR1600_nuRe1300.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1600,  1400,  1,  0,  2300,  .00397,  "WR1600_nuRe1400",  0,  "4_1_6-START41_V0-PU-WR1600_nuRe1400.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1600,  1500,  1,  0,  4500,  .001244,  "WR1600_nuRe1500",  0,  "4_1_6-START41_V0-PU-WR1600_nuRe1500.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1600,  1550,  1,  0,  6200,  .0004801,  "WR1600_nuRe1550",  0,  "4_1_6-START41_V0-PU-WR1600_nuRe1550.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},

  {1,  7,  1700,  50,  1,  0,  6500,  .03881,  "WR1700_nuRe50",  0,  "4_1_6-START41_V0-PU-WR1700_nuRe50.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1700,  100,  1,  0,  8800,  .03777,  "WR1700_nuRe100",  0,  "4_1_6-START41_V0-PU-WR1700_nuRe100.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1700,  200,  1,  0,  8200,  .03613,  "WR1700_nuRe200",  0,  "4_1_6-START41_V0-PU-WR1700_nuRe200.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1700,  300,  1,  0,  5400,  .03454,  "WR1700_nuRe300",  0,  "4_1_6-START41_V0-PU-WR1700_nuRe300.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1700,  400,  1,  0,  6200,  .03252,  "WR1700_nuRe400",  0,  "4_1_6-START41_V0-PU-WR1700_nuRe400.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1700,  500,  1,  0,  5300,  .03053,  "WR1700_nuRe500",  0,  "4_1_6-START41_V0-PU-WR1700_nuRe500.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1700,  600,  1,  0,  5300,  .02832,  "WR1700_nuRe600",  0,  "4_1_6-START41_V0-PU-WR1700_nuRe600.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1700,  700,  1,  0,  5700,  .02583,  "WR1700_nuRe700",  0,  "4_1_6-START41_V0-PU-WR1700_nuRe700.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1700,  800,  1,  0,  4300,  .02326,  "WR1700_nuRe800",  0,  "4_1_6-START41_V0-PU-WR1700_nuRe800.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1700,  900,  1,  0,  6800,  .02024,  "WR1700_nuRe900",  0,  "4_1_6-START41_V0-PU-WR1700_nuRe900.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1700,  1000,  1,  0,  4300,  .01719,  "WR1700_nuRe1000",  0,  "4_1_6-START41_V0-PU-WR1700_nuRe1000.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1700,  1100,  1,  0,  6100,  .01392,  "WR1700_nuRe1100",  0,  "4_1_6-START41_V0-PU-WR1700_nuRe1100.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1700,  1200,  1,  0,  6300,  .01068,  "WR1700_nuRe1200",  0,  "4_1_6-START41_V0-PU-WR1700_nuRe1200.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1700,  1300,  1,  0,  3800,  .007529,  "WR1700_nuRe1300",  0,  "4_1_6-START41_V0-PU-WR1700_nuRe1300.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1700,  1400,  1,  0,  2500,  .004734,  "WR1700_nuRe1400",  0,  "4_1_6-START41_V0-PU-WR1700_nuRe1400.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1700,  1500,  1,  0,  1100,  .002354,  "WR1700_nuRe1500",  0,  "4_1_6-START41_V0-PU-WR1700_nuRe1500.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1700,  1600,  1,  0,  200,  .0007379,  "WR1700_nuRe1600",  0,  "4_1_6-START41_V0-PU-WR1700_nuRe1600.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1700,  1650,  1,  0,  300,  .0002917,  "WR1700_nuRe1650",  0,  "4_1_6-START41_V0-PU-WR1700_nuRe1650.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},

  {1,  7,  1800,  50,  1,  0,  1000,  .02647,  "WR1800_nuRe50",  0,  "4_1_6-START41_V0-PU-WR1800_nuRe50.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1800,  100,  1,  0,  800,  .02577,  "WR1800_nuRe100",  0,  "4_1_6-START41_V0-PU-WR1800_nuRe100.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1800,  200,  1,  0,  400,  .02453,  "WR1800_nuRe200",  0,  "4_1_6-START41_V0-PU-WR1800_nuRe200.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1800,  300,  1,  0,  1000,  .02336,  "WR1800_nuRe300",  0,  "4_1_6-START41_V0-PU-WR1800_nuRe300.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1800,  400,  1,  0,  2500,  .02233,  "WR1800_nuRe400",  0,  "4_1_6-START41_V0-PU-WR1800_nuRe400.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1800,  500,  1,  0,  2300,  .02087,  "WR1800_nuRe500",  0,  "4_1_6-START41_V0-PU-WR1800_nuRe500.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1800,  600,  1,  0,  3900,  .01943,  "WR1800_nuRe600",  0,  "4_1_6-START41_V0-PU-WR1800_nuRe600.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1800,  700,  1,  0,  3700,  .01794,  "WR1800_nuRe700",  0,  "4_1_6-START41_V0-PU-WR1800_nuRe700.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1800,  800,  1,  0,  2000,  .01627,  "WR1800_nuRe800",  0,  "4_1_6-START41_V0-PU-WR1800_nuRe800.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1800,  900,  1,  0,  800,  .01446,  "WR1800_nuRe900",  0,  "4_1_6-START41_V0-PU-WR1800_nuRe900.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1800,  1000,  1,  0,  800,  .01254,  "WR1800_nuRe1000",  0,  "4_1_6-START41_V0-PU-WR1800_nuRe1000.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1800,  1100,  1,  0,  400,  .01057,  "WR1800_nuRe1100",  0,  "4_1_6-START41_V0-PU-WR1800_nuRe1100.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1800,  1200,  1,  0,  1000,  .008508,  "WR1800_nuRe1200",  0,  "4_1_6-START41_V0-PU-WR1800_nuRe1200.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1800,  1300,  1,  0,  500,  .006535,  "WR1800_nuRe1300",  0,  "4_1_6-START41_V0-PU-WR1800_nuRe1300.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1800,  1400,  1,  0,  2500,  .004572,  "WR1800_nuRe1400",  0,  "4_1_6-START41_V0-PU-WR1800_nuRe1400.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1800,  1500,  1,  0,  2400,  .002832,  "WR1800_nuRe1500",  0,  "4_1_6-START41_V0-PU-WR1800_nuRe1500.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1800,  1600,  1,  0,  3000,  .00141,  "WR1800_nuRe1600",  0,  "4_1_6-START41_V0-PU-WR1800_nuRe1600.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1800,  1700,  1,  0,  6500,  .0004421,  "WR1800_nuRe1700",  0,  "4_1_6-START41_V0-PU-WR1800_nuRe1700.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1800,  1750,  1,  0,  5700,  .000177,  "WR1800_nuRe1750",  0,  "4_1_6-START41_V0-PU-WR1800_nuRe1750.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},

  {1,  7,  1900,  50,  1,  0,  5700,  .01823,  "WR1900_nuRe50",  0,  "4_1_6-START41_V0-PU-WR1900_nuRe50.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1900,  100,  1,  0,  5900,  .01788,  "WR1900_nuRe100",  0,  "4_1_6-START41_V0-PU-WR1900_nuRe100.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1900,  200,  1,  0,  2600,  .01684,  "WR1900_nuRe200",  0,  "4_1_6-START41_V0-PU-WR1900_nuRe200.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1900,  300,  1,  0,  3500,  .01599,  "WR1900_nuRe300",  0,  "4_1_6-START41_V0-PU-WR1900_nuRe300.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1900,  400,  1,  0,  1400,  .0152,  "WR1900_nuRe400",  0,  "4_1_6-START41_V0-PU-WR1900_nuRe400.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1900,  500,  1,  0,  1800,  .01433,  "WR1900_nuRe500",  0,  "4_1_6-START41_V0-PU-WR1900_nuRe500.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1900,  600,  1,  0,  4300,  .01343,  "WR1900_nuRe600",  0,  "4_1_6-START41_V0-PU-WR1900_nuRe600.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1900,  700,  1,  0,  2600,  .01239,  "WR1900_nuRe700",  0,  "4_1_6-START41_V0-PU-WR1900_nuRe700.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1900,  800,  1,  0,  3700,  .01136,  "WR1900_nuRe800",  0,  "4_1_6-START41_V0-PU-WR1900_nuRe800.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1900,  900,  1,  0,  3900,  .01025,  "WR1900_nuRe900",  0,  "4_1_6-START41_V0-PU-WR1900_nuRe900.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1900,  1000,  1,  0,  3400,  .00904,  "WR1900_nuRe1000",  0,  "4_1_6-START41_V0-PU-WR1900_nuRe1000.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1900,  1100,  1,  0,  3100,  .007849,  "WR1900_nuRe1100",  0,  "4_1_6-START41_V0-PU-WR1900_nuRe1100.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1900,  1200,  1,  0,  3900,  .006542,  "WR1900_nuRe1200",  0,  "4_1_6-START41_V0-PU-WR1900_nuRe1200.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1900,  1300,  1,  0,  5500,  .005242,  "WR1900_nuRe1300",  0,  "4_1_6-START41_V0-PU-WR1900_nuRe1300.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1900,  1400,  1,  0,  5000,  .003994,  "WR1900_nuRe1400",  0,  "4_1_6-START41_V0-PU-WR1900_nuRe1400.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1900,  1500,  1,  0,  3000,  .002774,  "WR1900_nuRe1500",  0,  "4_1_6-START41_V0-PU-WR1900_nuRe1500.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1900,  1600,  1,  0,  4400,  .001712,  "WR1900_nuRe1600",  0,  "4_1_6-START41_V0-PU-WR1900_nuRe1600.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1900,  1700,  1,  0,  1800,  .0008457,  "WR1900_nuRe1700",  0,  "4_1_6-START41_V0-PU-WR1900_nuRe1700.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1900,  1800,  1,  0,  1800,  .000268,  "WR1900_nuRe1800",  0,  "4_1_6-START41_V0-PU-WR1900_nuRe1800.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  1900,  1850,  1,  0,  1500,  .0001084,  "WR1900_nuRe1850",  0,  "4_1_6-START41_V0-PU-WR1900_nuRe1850.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},

  {1,  7,  2000,  50,  1,  0,  400,  .0128,  "WR2000_nuRe50",  0,  "4_1_6-START41_V0-PU-WR2000_nuRe50.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  2000,  100,  1,  0,  2400,  .01236,  "WR2000_nuRe100",  0,  "4_1_6-START41_V0-PU-WR2000_nuRe100.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  2000,  200,  1,  0,  3200,  .01162,  "WR2000_nuRe200",  0,  "4_1_6-START41_V0-PU-WR2000_nuRe200.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  2000,  300,  1,  0,  4400,  .01102,  "WR2000_nuRe300",  0,  "4_1_6-START41_V0-PU-WR2000_nuRe300.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  2000,  400,  1,  0,  2500,  .01042,  "WR2000_nuRe400",  0,  "4_1_6-START41_V0-PU-WR2000_nuRe400.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  2000,  500,  1,  0,  3100,  .009843,  "WR2000_nuRe500",  0,  "4_1_6-START41_V0-PU-WR2000_nuRe500.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  2000,  600,  1,  0,  3200,  .009222,  "WR2000_nuRe600",  0,  "4_1_6-START41_V0-PU-WR2000_nuRe600.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  2000,  700,  1,  0,  8600,  .00861,  "WR2000_nuRe700",  0,  "4_1_6-START41_V0-PU-WR2000_nuRe700.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  2000,  800,  1,  0,  4500,  .007923,  "WR2000_nuRe800",  0,  "4_1_6-START41_V0-PU-WR2000_nuRe800.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  2000,  900,  1,  0,  2500,  .007227,  "WR2000_nuRe900",  0,  "4_1_6-START41_V0-PU-WR2000_nuRe900.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  2000,  1000,  1,  0,  2100,  .006461,  "WR2000_nuRe1000",  0,  "4_1_6-START41_V0-PU-WR2000_nuRe1000.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  2000,  1100,  1,  0,  3400,  .005681,  "WR2000_nuRe1100",  0,  "4_1_6-START41_V0-PU-WR2000_nuRe1100.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  2000,  1200,  1,  0,  2000,  .004872,  "WR2000_nuRe1200",  0,  "4_1_6-START41_V0-PU-WR2000_nuRe1200.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  2000,  1300,  1,  0,  2900,  .004068,  "WR2000_nuRe1300",  0,  "4_1_6-START41_V0-PU-WR2000_nuRe1300.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  2000,  1400,  1,  0,  2900,  .003236,  "WR2000_nuRe1400",  0,  "4_1_6-START41_V0-PU-WR2000_nuRe1400.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  2000,  1500,  1,  0,  4000,  .00244,  "WR2000_nuRe1500",  0,  "4_1_6-START41_V0-PU-WR2000_nuRe1500.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  2000,  1600,  1,  0,  3400,  .001702,  "WR2000_nuRe1600",  0,  "4_1_6-START41_V0-PU-WR2000_nuRe1600.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  2000,  1700,  1,  0,  3600,  .001042,  "WR2000_nuRe1700",  0,  "4_1_6-START41_V0-PU-WR2000_nuRe1700.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  2000,  1800,  1,  0,  7300,  .0005101,  "WR2000_nuRe1800",  0,  "4_1_6-START41_V0-PU-WR2000_nuRe1800.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  2000,  1900,  1,  0,  9300,  .000162,  "WR2000_nuRe1900",  0,  "4_1_6-START41_V0-PU-WR2000_nuRe1900.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  {1,  7,  2000,  1950,  1,  0,  8600,  .0000667,  "WR2000_nuRe1950",  0,  "4_1_6-START41_V0-PU-WR2000_nuRe1950.root",  "START41_V0::All",  "GRun",  "CMSSW_4_1_6",  ""},
  
  
  // CMSSW_4_2_3
  {1,  7,  500,  50,  1,  0,  5000,  15.17,  "WR500_nuRe50",  0,  "4_2_3-START42_V11-WR500_nuRe50.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  500,  100,  1,  0,  5000,  14.5,  "WR500_nuRe100",  0,  "4_2_3-START42_V11-WR500_nuRe100.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  500,  400,  1,  0,  4900,  2.781,  "WR500_nuRe400",  0,  "4_2_3-START42_V11-WR500_nuRe400.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  500,  450,  1,  0,  5000,  .8967,  "WR500_nuRe450",  0,  "4_2_3-START42_V11-WR500_nuRe450.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  800,  50,  1,  0,  4900,  2.142,  "WR800_nuRe50",  0,  "4_2_3-START42_V11-WR800_nuRe50.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  800,  100,  1,  0,  5000,  2.09,  "WR800_nuRe100",  0,  "4_2_3-START42_V11-WR800_nuRe100.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  800,  400,  1,  0,  5000,  1.344,  "WR800_nuRe400",  0,  "4_2_3-START42_V11-WR800_nuRe400.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  800,  700,  1,  0,  4900,  .1736,  "WR800_nuRe700",  0,  "4_2_3-START42_V11-WR800_nuRe700.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  800,  750,  1,  0,  5000,  .0575,  "WR800_nuRe750",  0,  "4_2_3-START42_V11-WR800_nuRe750.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  1100,  50,  1,  0,  5000,  .469,  "WR1100_nuRe50",  0,  "4_2_3-START42_V11-WR1100_nuRe50.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  1100,  100,  1,  0,  4800,  .4643,  "WR1100_nuRe100",  0,  "4_2_3-START42_V11-WR1100_nuRe100.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  1100,  400,  1,  0,  5000,  .3667,  "WR1100_nuRe400",  0,  "4_2_3-START42_V11-WR1100_nuRe400.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  1100,  700,  1,  0,  5000,  .1962,  "WR1100_nuRe700",  0,  "4_2_3-START42_V11-WR1100_nuRe700.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  1100,  1000,  1,  0,  5000,  .02151,  "WR1100_nuRe1000",  0,  "4_2_3-START42_V11-WR1100_nuRe1000.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  1100,  1050,  1,  0,  5000,  .007564,  "WR1100_nuRe1050",  0,  "4_2_3-START42_V11-WR1100_nuRe1050.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  1400,  50,  1,  0,  4800,  .1281,  "WR1400_nuRe50",  0,  "4_2_3-START42_V11-WR1400_nuRe50.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  1400,  100,  1,  0,  4900,  .1253,  "WR1400_nuRe100",  0,  "4_2_3-START42_V11-WR1400_nuRe100.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  1400,  400,  1,  0,  5000,  .1062,  "WR1400_nuRe400",  0,  "4_2_3-START42_V11-WR1400_nuRe400.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  1400,  700,  1,  0,  5000,  .07585,  "WR1400_nuRe700",  0,  "4_2_3-START42_V11-WR1400_nuRe700.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  1400,  1000,  1,  0,  5000,  .03612,  "WR1400_nuRe1000",  0,  "4_2_3-START42_V11-WR1400_nuRe1000.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  1400,  1300,  1,  0,  5000,  .003672,  "WR1400_nuRe1300",  0,  "4_2_3-START42_V11-WR1400_nuRe1300.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  1400,  1350,  1,  0,  5000,  .001372,  "WR1400_nuRe1350",  0,  "4_2_3-START42_V11-WR1400_nuRe1350.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  1700,  50,  1,  0,  4900,  .0387,  "WR1700_nuRe50",  0,  "4_2_3-START42_V11-WR1700_nuRe50.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  1700,  100,  1,  0,  4800,  .03758,  "WR1700_nuRe100",  0,  "4_2_3-START42_V11-WR1700_nuRe100.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  1700,  400,  1,  0,  5000,  .03261,  "WR1700_nuRe400",  0,  "4_2_3-START42_V11-WR1700_nuRe400.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  1700,  700,  1,  0,  4400,  .02577,  "WR1700_nuRe700",  0,  "4_2_3-START42_V11-WR1700_nuRe700.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  1700,  1000,  1,  0,  4900,  .01705,  "WR1700_nuRe1000",  0,  "4_2_3-START42_V11-WR1700_nuRe1000.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  1700,  1300,  1,  0,  4900,  .00756,  "WR1700_nuRe1300",  0,  "4_2_3-START42_V11-WR1700_nuRe1300.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  1700,  1600,  1,  0,  4800,  .0007391,  "WR1700_nuRe1600",  0,  "4_2_3-START42_V11-WR1700_nuRe1600.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  1700,  1650,  1,  0,  5000,  .0002915,  "WR1700_nuRe1650",  0,  "4_2_3-START42_V11-WR1700_nuRe1650.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  2000,  50,  1,  0,  5000,  .0128,  "WR2000_nuRe50",  0,  "4_2_3-START42_V11-WR2000_nuRe50.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  2000,  100,  1,  0,  5000,  .01234,  "WR2000_nuRe100",  0,  "4_2_3-START42_V11-WR2000_nuRe100.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  2000,  400,  1,  0,  5000,  .01043,  "WR2000_nuRe400",  0,  "4_2_3-START42_V11-WR2000_nuRe400.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  2000,  700,  1,  0,  4800,  .008572,  "WR2000_nuRe700",  0,  "4_2_3-START42_V11-WR2000_nuRe700.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  2000,  1000,  1,  0,  4600,  .006471,  "WR2000_nuRe1000",  0,  "4_2_3-START42_V11-WR2000_nuRe1000.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  2000,  1300,  1,  0,  4900,  .004049,  "WR2000_nuRe1300",  0,  "4_2_3-START42_V11-WR2000_nuRe1300.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  2000,  1600,  1,  0,  4900,  .001701,  "WR2000_nuRe1600",  0,  "4_2_3-START42_V11-WR2000_nuRe1600.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  2000,  1900,  1,  0,  4700,  .0001624,  "WR2000_nuRe1900",  0,  "4_2_3-START42_V11-WR2000_nuRe1900.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  2000,  1950,  1,  0,  5000,  .00006692,  "WR2000_nuRe1950",  0,  "4_2_3-START42_V11-WR2000_nuRe1950.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  2300,  50,  1,  0,  4900,  .004572,  "WR2300_nuRe50",  0,  "4_2_3-START42_V11-WR2300_nuRe50.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  2300,  100,  1,  0,  4800,  .004343,  "WR2300_nuRe100",  0,  "4_2_3-START42_V11-WR2300_nuRe100.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  2300,  400,  1,  0,  4800,  .003475,  "WR2300_nuRe400",  0,  "4_2_3-START42_V11-WR2300_nuRe400.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  2300,  700,  1,  0,  5000,  .002849,  "WR2300_nuRe700",  0,  "4_2_3-START42_V11-WR2300_nuRe700.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  2300,  1000,  1,  0,  5000,  .002248,  "WR2300_nuRe1000",  0,  "4_2_3-START42_V11-WR2300_nuRe1000.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  2300,  1300,  1,  0,  5000,  .001631,  "WR2300_nuRe1300",  0,  "4_2_3-START42_V11-WR2300_nuRe1300.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  2300,  1600,  1,  0,  5000,  .0009825,  "WR2300_nuRe1600",  0,  "4_2_3-START42_V11-WR2300_nuRe1600.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  2300,  1900,  1,  0,  5000,  .0003924,  "WR2300_nuRe1900",  0,  "4_2_3-START42_V11-WR2300_nuRe1900.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  2300,  2200,  1,  0,  5000,  .00003694,  "WR2300_nuRe2200",  0,  "4_2_3-START42_V11-WR2300_nuRe2200.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  {1,  7,  2300,  2250,  1,  0,  5000,  .00001573,  "WR2300_nuRe2250",  0,  "4_2_3-START42_V11-WR2300_nuRe2250.root",  "START42_V11::All",  "GRun",  "CMSSW_4_2_3_patch2",  ""},
  
  // 4_2_8 signal samples with Summer11 PU_S4 pileup scenario:
  {1,  7,  800,  100,  1,  0,  300,  2.095,  "WR800_nuRe100",  0,  "4_2_8-START42_V13-WR800_nuRe100.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  800,  200,  1,  0,  600,  1.927,  "WR800_nuRe200",  0,  "4_2_8-START42_V13-WR800_nuRe200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  800,  400,  1,  0,  2400,  1.349,  "WR800_nuRe400",  0,  "4_2_8-START42_V13-WR800_nuRe400.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  800,  600,  1,  0,  1200,  .5327,  "WR800_nuRe600",  0,  "4_2_8-START42_V13-WR800_nuRe600.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  800,  700,  1,  0,  1200,  .1737,  "WR800_nuRe700",  0,  "4_2_8-START42_V13-WR800_nuRe700.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  900,  100,  1,  0,  600,  1.225,  "WR900_nuRe100",  0,  "4_2_8-START42_V13-WR900_nuRe100.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  900,  200,  1,  0,  300,  1.144,  "WR900_nuRe200",  0,  "4_2_8-START42_V13-WR900_nuRe200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  900,  400,  1,  0,  100,  .8701,  "WR900_nuRe400",  0,  "4_2_8-START42_V13-WR900_nuRe400.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  900,  600,  1,  0,  300,  .4696,  "WR900_nuRe600",  0,  "4_2_8-START42_V13-WR900_nuRe600.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  900,  800,  1,  0,  1600,  .08219,  "WR900_nuRe800",  0,  "4_2_8-START42_V13-WR900_nuRe800.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1000,  100,  1,  0,  9300,  .7447,  "WR1000_nuRe100",  0,  "4_2_8-START42_V13-WR1000_nuRe100.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1000,  200,  1,  0,  8100,  .6997,  "WR1000_nuRe200",  0,  "4_2_8-START42_V13-WR1000_nuRe200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1000,  400,  1,  0,  7400,  .5633,  "WR1000_nuRe400",  0,  "4_2_8-START42_V13-WR1000_nuRe400.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1000,  600,  1,  0,  4700,  .3609,  "WR1000_nuRe600",  0,  "4_2_8-START42_V13-WR1000_nuRe600.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1000,  800,  1,  0,  1400,  .13,  "WR1000_nuRe800",  0,  "4_2_8-START42_V13-WR1000_nuRe800.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1100,  100,  1,  0,  200,  .4631,  "WR1100_nuRe100",  0,  "4_2_8-START42_V13-WR1100_nuRe100.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1100,  200,  1,  0,  100,  .4403,  "WR1100_nuRe200",  0,  "4_2_8-START42_V13-WR1100_nuRe200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1100,  600,  1,  0,  4400,  .2606,  "WR1100_nuRe600",  0,  "4_2_8-START42_V13-WR1100_nuRe600.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1100,  800,  1,  0,  9900,  .1297,  "WR1100_nuRe800",  0,  "4_2_8-START42_V13-WR1100_nuRe800.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1100,  1000,  1,  0,  200,  .0215,  "WR1100_nuRe1000",  0,  "4_2_8-START42_V13-WR1100_nuRe1000.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1200,  100,  1,  0,  10000,  .2939,  "WR1200_nuRe100",  0,  "4_2_8-START42_V13-WR1200_nuRe100.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1200,  200,  1,  0,  10000,  .2815,  "WR1200_nuRe200",  0,  "4_2_8-START42_V13-WR1200_nuRe200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1200,  400,  1,  0,  9900,  .241,  "WR1200_nuRe400",  0,  "4_2_8-START42_V13-WR1200_nuRe400.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1200,  600,  1,  0,  9900,  .1818,  "WR1200_nuRe600",  0,  "4_2_8-START42_V13-WR1200_nuRe600.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1200,  800,  1,  0,  9900,  .1093,  "WR1200_nuRe800",  0,  "4_2_8-START42_V13-WR1200_nuRe800.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1200,  1000,  1,  0,  9800,  .03706,  "WR1200_nuRe1000",  0,  "4_2_8-START42_V13-WR1200_nuRe1000.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1200,  1100,  1,  0,  9900,  .01162,  "WR1200_nuRe1100",  0,  "4_2_8-START42_V13-WR1200_nuRe1100.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1300,  100,  1,  0,  8200,  .1912,  "WR1300_nuRe100",  0,  "4_2_8-START42_V13-WR1300_nuRe100.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1300,  200,  1,  0,  1400,  .1825,  "WR1300_nuRe200",  0,  "4_2_8-START42_V13-WR1300_nuRe200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1300,  400,  1,  0,  100,  .1593,  "WR1300_nuRe400",  0,  "4_2_8-START42_V13-WR1300_nuRe400.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1300,  600,  1,  0,  400,  .1267,  "WR1300_nuRe600",  0,  "4_2_8-START42_V13-WR1300_nuRe600.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1300,  800,  1,  0,  500,  .08478,  "WR1300_nuRe800",  0,  "4_2_8-START42_V13-WR1300_nuRe800.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1300,  1000,  1,  0,  3600,  .04009,  "WR1300_nuRe1000",  0,  "4_2_8-START42_V13-WR1300_nuRe1000.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1300,  1200,  1,  0,  4200,  .006465,  "WR1300_nuRe1200",  0,  "4_2_8-START42_V13-WR1300_nuRe1200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1400,  100,  1,  0,  600,  .1256,  "WR1400_nuRe100",  0,  "4_2_8-START42_V13-WR1400_nuRe100.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1400,  200,  1,  0,  3800,  .1199,  "WR1400_nuRe200",  0,  "4_2_8-START42_V13-WR1400_nuRe200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1400,  400,  1,  0,  9700,  .1062,  "WR1400_nuRe400",  0,  "4_2_8-START42_V13-WR1400_nuRe400.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1400,  600,  1,  0,  9800,  .08723,  "WR1400_nuRe600",  0,  "4_2_8-START42_V13-WR1400_nuRe600.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1400,  800,  1,  0,  10000,  .06301,  "WR1400_nuRe800",  0,  "4_2_8-START42_V13-WR1400_nuRe800.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1400,  1000,  1,  0,  1200,  .03615,  "WR1400_nuRe1000",  0,  "4_2_8-START42_V13-WR1400_nuRe1000.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1400,  1200,  1,  0,  1500,  .01177,  "WR1400_nuRe1200",  0,  "4_2_8-START42_V13-WR1400_nuRe1200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1400,  1300,  1,  0,  1400,  .003665,  "WR1400_nuRe1300",  0,  "4_2_8-START42_V13-WR1400_nuRe1300.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1500,  100,  1,  0,  10000,  .08307,  "WR1500_nuRe100",  0,  "4_2_8-START42_V13-WR1500_nuRe100.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1500,  200,  1,  0,  6100,  .07949,  "WR1500_nuRe200",  0,  "4_2_8-START42_V13-WR1500_nuRe200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1500,  400,  1,  0,  6500,  .07114,  "WR1500_nuRe400",  0,  "4_2_8-START42_V13-WR1500_nuRe400.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1500,  600,  1,  0,  5100,  .0599,  "WR1500_nuRe600",  0,  "4_2_8-START42_V13-WR1500_nuRe600.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1500,  800,  1,  0,  2200,  .04571,  "WR1500_nuRe800",  0,  "4_2_8-START42_V13-WR1500_nuRe800.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1500,  1000,  1,  0,  9900,  .0296,  "WR1500_nuRe1000",  0,  "4_2_8-START42_V13-WR1500_nuRe1000.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1500,  1200,  1,  0,  6400,  .01337,  "WR1500_nuRe1200",  0,  "4_2_8-START42_V13-WR1500_nuRe1200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1500,  1400,  1,  0,  9500,  .00212,  "WR1500_nuRe1400",  0,  "4_2_8-START42_V13-WR1500_nuRe1400.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1600,  100,  1,  0,  3100,  .05594,  "WR1600_nuRe100",  0,  "4_2_8-START42_V13-WR1600_nuRe100.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1600,  200,  1,  0,  1900,  .05363,  "WR1600_nuRe200",  0,  "4_2_8-START42_V13-WR1600_nuRe200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1600,  400,  1,  0,  1900,  .04815,  "WR1600_nuRe400",  0,  "4_2_8-START42_V13-WR1600_nuRe400.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1600,  600,  1,  0,  3800,  .04138,  "WR1600_nuRe600",  0,  "4_2_8-START42_V13-WR1600_nuRe600.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1600,  800,  1,  0,  2400,  .03269,  "WR1600_nuRe800",  0,  "4_2_8-START42_V13-WR1600_nuRe800.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1600,  1000,  1,  0,  5800,  .02288,  "WR1600_nuRe1000",  0,  "4_2_8-START42_V13-WR1600_nuRe1000.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1600,  1200,  1,  0,  3500,  .01256,  "WR1600_nuRe1200",  0,  "4_2_8-START42_V13-WR1600_nuRe1200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1600,  1400,  1,  0,  6200,  .003965,  "WR1600_nuRe1400",  0,  "4_2_8-START42_V13-WR1600_nuRe1400.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1600,  1500,  1,  0,  3100,  .001246,  "WR1600_nuRe1500",  0,  "4_2_8-START42_V13-WR1600_nuRe1500.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1700,  100,  1,  0,  8100,  .03775,  "WR1700_nuRe100",  0,  "4_2_8-START42_V13-WR1700_nuRe100.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1700,  200,  1,  0,  9900,  .03615,  "WR1700_nuRe200",  0,  "4_2_8-START42_V13-WR1700_nuRe200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1700,  400,  1,  0,  9400,  .03269,  "WR1700_nuRe400",  0,  "4_2_8-START42_V13-WR1700_nuRe400.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1700,  600,  1,  0,  5600,  .02825,  "WR1700_nuRe600",  0,  "4_2_8-START42_V13-WR1700_nuRe600.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1700,  800,  1,  0,  7200,  .02305,  "WR1700_nuRe800",  0,  "4_2_8-START42_V13-WR1700_nuRe800.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1700,  1000,  1,  0,  9900,  .0172,  "WR1700_nuRe1000",  0,  "4_2_8-START42_V13-WR1700_nuRe1000.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1700,  1200,  1,  0,  9900,  .01074,  "WR1700_nuRe1200",  0,  "4_2_8-START42_V13-WR1700_nuRe1200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1700,  1400,  1,  0,  9000,  .004721,  "WR1700_nuRe1400",  0,  "4_2_8-START42_V13-WR1700_nuRe1400.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1700,  1600,  1,  0,  9500,  .0007407,  "WR1700_nuRe1600",  0,  "4_2_8-START42_V13-WR1700_nuRe1600.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1800,  100,  1,  0,  2400,  .02587,  "WR1800_nuRe100",  0,  "4_2_8-START42_V13-WR1800_nuRe100.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1800,  200,  1,  0,  8700,  .02468,  "WR1800_nuRe200",  0,  "4_2_8-START42_V13-WR1800_nuRe200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1800,  400,  1,  0,  9700,  .02213,  "WR1800_nuRe400",  0,  "4_2_8-START42_V13-WR1800_nuRe400.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1800,  600,  1,  0,  8100,  .01944,  "WR1800_nuRe600",  0,  "4_2_8-START42_V13-WR1800_nuRe600.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1800,  800,  1,  0,  9000,  .01624,  "WR1800_nuRe800",  0,  "4_2_8-START42_V13-WR1800_nuRe800.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1800,  1000,  1,  0,  1300,  .01257,  "WR1800_nuRe1000",  0,  "4_2_8-START42_V13-WR1800_nuRe1000.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1800,  1200,  1,  0,  500,  .008556,  "WR1800_nuRe1200",  0,  "4_2_8-START42_V13-WR1800_nuRe1200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1800,  1400,  1,  0,  6200,  .004548,  "WR1800_nuRe1400",  0,  "4_2_8-START42_V13-WR1800_nuRe1400.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1800,  1600,  1,  0,  4700,  .001402,  "WR1800_nuRe1600",  0,  "4_2_8-START42_V13-WR1800_nuRe1600.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1800,  1700,  1,  0,  2300,  .0004431,  "WR1800_nuRe1700",  0,  "4_2_8-START42_V13-WR1800_nuRe1700.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1900,  100,  1,  0,  7200,  .01781,  "WR1900_nuRe100",  0,  "4_2_8-START42_V13-WR1900_nuRe100.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1900,  200,  1,  0,  3800,  .01682,  "WR1900_nuRe200",  0,  "4_2_8-START42_V13-WR1900_nuRe200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1900,  400,  1,  0,  9700,  .01516,  "WR1900_nuRe400",  0,  "4_2_8-START42_V13-WR1900_nuRe400.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1900,  600,  1,  0,  9900,  .01337,  "WR1900_nuRe600",  0,  "4_2_8-START42_V13-WR1900_nuRe600.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1900,  800,  1,  0,  9700,  .01139,  "WR1900_nuRe800",  0,  "4_2_8-START42_V13-WR1900_nuRe800.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1900,  1000,  1,  0,  7200,  .009078,  "WR1900_nuRe1000",  0,  "4_2_8-START42_V13-WR1900_nuRe1000.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1900,  1200,  1,  0,  10000,  .006518,  "WR1900_nuRe1200",  0,  "4_2_8-START42_V13-WR1900_nuRe1200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1900,  1400,  1,  0,  5700,  .00398,  "WR1900_nuRe1400",  0,  "4_2_8-START42_V13-WR1900_nuRe1400.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1900,  1600,  1,  0,  2200,  .001714,  "WR1900_nuRe1600",  0,  "4_2_8-START42_V13-WR1900_nuRe1600.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  1900,  1800,  1,  0,  3100,  .0002675,  "WR1900_nuRe1800",  0,  "4_2_8-START42_V13-WR1900_nuRe1800.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2000,  100,  1,  0,  900,  .01238,  "WR2000_nuRe100",  0,  "4_2_8-START42_V13-WR2000_nuRe100.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2000,  200,  1,  0,  7200,  .01164,  "WR2000_nuRe200",  0,  "4_2_8-START42_V13-WR2000_nuRe200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2000,  400,  1,  0,  7000,  .01045,  "WR2000_nuRe400",  0,  "4_2_8-START42_V13-WR2000_nuRe400.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2000,  600,  1,  0,  3500,  .009228,  "WR2000_nuRe600",  0,  "4_2_8-START42_V13-WR2000_nuRe600.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2000,  800,  1,  0,  3200,  .007932,  "WR2000_nuRe800",  0,  "4_2_8-START42_V13-WR2000_nuRe800.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2000,  1100,  1,  0,  8700,  .005678,  "WR2000_nuRe1100",  0,  "4_2_8-START42_V13-WR2000_nuRe1100.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2000,  1200,  1,  0,  7900,  .004905,  "WR2000_nuRe1200",  0,  "4_2_8-START42_V13-WR2000_nuRe1200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2000,  1400,  1,  0,  6800,  .003224,  "WR2000_nuRe1400",  0,  "4_2_8-START42_V13-WR2000_nuRe1400.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2000,  1600,  1,  0,  1600,  .001694,  "WR2000_nuRe1600",  0,  "4_2_8-START42_V13-WR2000_nuRe1600.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2000,  1800,  1,  0,  1100,  .0005125,  "WR2000_nuRe1800",  0,  "4_2_8-START42_V13-WR2000_nuRe1800.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2000,  1900,  1,  0,  7200,  .0001618,  "WR2000_nuRe1900",  0,  "4_2_8-START42_V13-WR2000_nuRe1900.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2100,  100,  1,  0,  8400,  .008611,  "WR2100_nuRe100",  0,  "4_2_8-START42_V13-WR2100_nuRe100.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2100,  200,  1,  0,  700,  .008091,  "WR2100_nuRe200",  0,  "4_2_8-START42_V13-WR2100_nuRe200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2100,  400,  1,  0,  1700,  .007203,  "WR2100_nuRe400",  0,  "4_2_8-START42_V13-WR2100_nuRe400.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2100,  600,  1,  0,  700,  .006373,  "WR2100_nuRe600",  0,  "4_2_8-START42_V13-WR2100_nuRe600.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2100,  800,  1,  0,  800,  .005516,  "WR2100_nuRe800",  0,  "4_2_8-START42_V13-WR2100_nuRe800.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2100,  1000,  1,  0,  9600,  .004584,  "WR2100_nuRe1000",  0,  "4_2_8-START42_V13-WR2100_nuRe1000.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2100,  1100,  1,  0,  9500,  .004095,  "WR2100_nuRe1100",  0,  "4_2_8-START42_V13-WR2100_nuRe1100.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2100,  1200,  1,  0,  3300,  .003579,  "WR2100_nuRe1200",  0,  "4_2_8-START42_V13-WR2100_nuRe1200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2100,  1400,  1,  0,  1700,  .002525,  "WR2100_nuRe1400",  0,  "4_2_8-START42_V13-WR2100_nuRe1400.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2100,  1600,  1,  0,  2400,  .001512,  "WR2100_nuRe1600",  0,  "4_2_8-START42_V13-WR2100_nuRe1600.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2100,  1800,  1,  0,  2200,  .0006329,  "WR2100_nuRe1800",  0,  "4_2_8-START42_V13-WR2100_nuRe1800.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2100,  2000,  1,  0,  3700,  .00009909,  "WR2100_nuRe2000",  0,  "4_2_8-START42_V13-WR2100_nuRe2000.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2200,  100,  1,  0,  600,  .006092,  "WR2200_nuRe100",  0,  "4_2_8-START42_V13-WR2200_nuRe100.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2200,  200,  1,  0,  2200,  .005645,  "WR2200_nuRe200",  0,  "4_2_8-START42_V13-WR2200_nuRe200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2200,  400,  1,  0,  900,  .004962,  "WR2200_nuRe400",  0,  "4_2_8-START42_V13-WR2200_nuRe400.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2200,  600,  1,  0,  3200,  .004402,  "WR2200_nuRe600",  0,  "4_2_8-START42_V13-WR2200_nuRe600.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2200,  800,  1,  0,  2400,  .003813,  "WR2200_nuRe800",  0,  "4_2_8-START42_V13-WR2200_nuRe800.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2200,  1000,  1,  0,  2200,  .00323,  "WR2200_nuRe1000",  0,  "4_2_8-START42_V13-WR2200_nuRe1000.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2200,  1200,  1,  0,  4100,  .002586,  "WR2200_nuRe1200",  0,  "4_2_8-START42_V13-WR2200_nuRe1200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2200,  1400,  1,  0,  400,  .001909,  "WR2200_nuRe1400",  0,  "4_2_8-START42_V13-WR2200_nuRe1400.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2200,  1600,  1,  0,  500,  .001243,  "WR2200_nuRe1600",  0,  "4_2_8-START42_V13-WR2200_nuRe1600.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2200,  1800,  1,  0,  400,  .0006446,  "WR2200_nuRe1800",  0,  "4_2_8-START42_V13-WR2200_nuRe1800.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2200,  2000,  1,  0,  2200,  .0001893,  "WR2200_nuRe2000",  0,  "4_2_8-START42_V13-WR2200_nuRe2000.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2200,  2100,  1,  0,  1700,  .0000605,  "WR2200_nuRe2100",  0,  "4_2_8-START42_V13-WR2200_nuRe2100.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2300,  100,  1,  0,  400,  .004362,  "WR2300_nuRe100",  0,  "4_2_8-START42_V13-WR2300_nuRe100.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2300,  200,  1,  0,  4200,  .003995,  "WR2300_nuRe200",  0,  "4_2_8-START42_V13-WR2300_nuRe200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2300,  400,  1,  0,  300,  .003457,  "WR2300_nuRe400",  0,  "4_2_8-START42_V13-WR2300_nuRe400.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2300,  600,  1,  0,  100,  .003044,  "WR2300_nuRe600",  0,  "4_2_8-START42_V13-WR2300_nuRe600.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2300,  800,  1,  0,  400,  .002667,  "WR2300_nuRe800",  0,  "4_2_8-START42_V13-WR2300_nuRe800.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2300,  1000,  1,  0,  200,  .002257,  "WR2300_nuRe1000",  0,  "4_2_8-START42_V13-WR2300_nuRe1000.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2300,  1200,  1,  0,  900,  .001842,  "WR2300_nuRe1200",  0,  "4_2_8-START42_V13-WR2300_nuRe1200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2300,  1400,  1,  0,  400,  .001414,  "WR2300_nuRe1400",  0,  "4_2_8-START42_V13-WR2300_nuRe1400.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2300,  1600,  1,  0,  100,  .0009833,  "WR2300_nuRe1600",  0,  "4_2_8-START42_V13-WR2300_nuRe1600.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2300,  1800,  1,  0,  1000,  .0005772,  "WR2300_nuRe1800",  0,  "4_2_8-START42_V13-WR2300_nuRe1800.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2300,  2000,  1,  0,  1000,  .000237,  "WR2300_nuRe2000",  0,  "4_2_8-START42_V13-WR2300_nuRe2000.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2300,  2200,  1,  0,  800,  .000037,  "WR2300_nuRe2200",  0,  "4_2_8-START42_V13-WR2300_nuRe2200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2400,  100,  1,  0,  1500,  .003123,  "WR2400_nuRe100",  0,  "4_2_8-START42_V13-WR2400_nuRe100.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2400,  200,  1,  0,  300,  .002838,  "WR2400_nuRe200",  0,  "4_2_8-START42_V13-WR2400_nuRe200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2400,  400,  1,  0,  500,  .002416,  "WR2400_nuRe400",  0,  "4_2_8-START42_V13-WR2400_nuRe400.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2400,  600,  1,  0,  600,  .002113,  "WR2400_nuRe600",  0,  "4_2_8-START42_V13-WR2400_nuRe600.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2400,  800,  1,  0,  2000,  .001843,  "WR2400_nuRe800",  0,  "4_2_8-START42_V13-WR2400_nuRe800.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2400,  1000,  1,  0,  800,  .001582,  "WR2400_nuRe1000",  0,  "4_2_8-START42_V13-WR2400_nuRe1000.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2400,  1200,  1,  0,  4400,  .001312,  "WR2400_nuRe1200",  0,  "4_2_8-START42_V13-WR2400_nuRe1200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2400,  1300,  1,  0,  900,  .001169,  "WR2400_nuRe1300",  0,  "4_2_8-START42_V13-WR2400_nuRe1300.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2400,  1400,  1,  0,  100,  .001027,  "WR2400_nuRe1400",  0,  "4_2_8-START42_V13-WR2400_nuRe1400.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2400,  1600,  1,  0,  400,  .0007457,  "WR2400_nuRe1600",  0,  "4_2_8-START42_V13-WR2400_nuRe1600.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2400,  1800,  1,  0,  300,  .0004798,  "WR2400_nuRe1800",  0,  "4_2_8-START42_V13-WR2400_nuRe1800.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2400,  2000,  1,  0,  400,  .0002421,  "WR2400_nuRe2000",  0,  "4_2_8-START42_V13-WR2400_nuRe2000.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2400,  2200,  1,  0,  300,  .00007079,  "WR2400_nuRe2200",  0,  "4_2_8-START42_V13-WR2400_nuRe2200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2400,  2300,  1,  0,  200,  .0000227,  "WR2400_nuRe2300",  0,  "4_2_8-START42_V13-WR2400_nuRe2300.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2500,  100,  1,  0,  3200,  .002292,  "WR2500_nuRe100",  0,  "4_2_8-START42_V13-WR2500_nuRe100.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2500,  200,  1,  0,  700,  .00205,  "WR2500_nuRe200",  0,  "4_2_8-START42_V13-WR2500_nuRe200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2500,  400,  1,  0,  1500,  .001714,  "WR2500_nuRe400",  0,  "4_2_8-START42_V13-WR2500_nuRe400.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2500,  600,  1,  0,  800,  .001478,  "WR2500_nuRe600",  0,  "4_2_8-START42_V13-WR2500_nuRe600.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2500,  800,  1,  0,  100,  .001277,  "WR2500_nuRe800",  0,  "4_2_8-START42_V13-WR2500_nuRe800.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2500,  1000,  1,  0,  300,  .001094,  "WR2500_nuRe1000",  0,  "4_2_8-START42_V13-WR2500_nuRe1000.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2500,  1200,  1,  0,  300,  .0009176,  "WR2500_nuRe1200",  0,  "4_2_8-START42_V13-WR2500_nuRe1200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2500,  1300,  1,  0,  700,  .0008289,  "WR2500_nuRe1300",  0,  "4_2_8-START42_V13-WR2500_nuRe1300.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2500,  1400,  1,  0,  3900,  .0007406,  "WR2500_nuRe1400",  0,  "4_2_8-START42_V13-WR2500_nuRe1400.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2500,  1600,  1,  0,  200,  .0005562,  "WR2500_nuRe1600",  0,  "4_2_8-START42_V13-WR2500_nuRe1600.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2500,  1800,  1,  0,  400,  .0003801,  "WR2500_nuRe1800",  0,  "4_2_8-START42_V13-WR2500_nuRe1800.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2500,  2000,  1,  0,  400,  .0002178,  "WR2500_nuRe2000",  0,  "4_2_8-START42_V13-WR2500_nuRe2000.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2500,  2200,  1,  0,  500,  .00008922,  "WR2500_nuRe2200",  0,  "4_2_8-START42_V13-WR2500_nuRe2200.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  {1,  7,  2500,  2400,  1,  0,  2900,  .00001387,  "WR2500_nuRe2400",  0,  "4_2_8-START42_V13-WR2500_nuRe2400.root",  "START42_V13::All",  "GRun",  "CMSSW_4_2_8_patch7",  ""},
  
  // CMSSW_5_3_18 (sross sections calculated for statistics of 50k events)
  {1,  8,  2000,  100,  1,  0,  10000,  0.02353,  "WR2000_nuRe100",  0,  "5_3_18-START53_V15A-WR2000_nuRe100.root",  "START53_V15A::All",  "GRun",  "CMSSW_5_3_18",  ""},
  {1,  8,  2000,  200,  1,  0,  10000,  0.02261,  "WR2000_nuRe200",  0,  "5_3_18-START53_V15A-WR2000_nuRe200.root",  "START53_V15A::All",  "GRun",  "CMSSW_5_3_18",  ""},
  {1,  8,  2000,  400,  1,  0,  10000,  0.02074,  "WR2000_nuRe400",  0,  "5_3_18-START53_V15A-WR2000_nuRe400.root",  "START53_V15A::All",  "GRun",  "CMSSW_5_3_18",  ""},
  {1,  8,  2000,  600,  1,  0,  10000,  0.01871,  "WR2000_nuRe600",  0,  "5_3_18-START53_V15A-WR2000_nuRe600.root",  "START53_V15A::All",  "GRun",  "CMSSW_5_3_18",  ""},
  {1,  8,  2000,  800,  1,  0,  10000,  0.01613,  "WR2000_nuRe800",  0,  "5_3_18-START53_V15A-WR2000_nuRe800.root",  "START53_V15A::All",  "GRun",  "CMSSW_5_3_18",  ""},
  {1,  8,  2000,  1000,  1,  0,  10000,  0.01328,  "WR2000_nuRe1000",  0,  "5_3_18-START53_V15A-WR2000_nuRe1000.root",  "START53_V15A::All",  "GRun",  "CMSSW_5_3_18",  ""},
  {1,  8,  2000,  1200,  1,  0,  10000,  0.01004,  "WR2000_nuRe1200",  0,  "5_3_18-START53_V15A-WR2000_nuRe1200.root",  "START53_V15A::All",  "GRun",  "CMSSW_5_3_18",  ""},
  {1,  8,  2000,  1400,  1,  0,  10000,  0.006701,  "WR2000_nuRe1400",  0,  "5_3_18-START53_V15A-WR2000_nuRe1400.root",  "START53_V15A::All",  "GRun",  "CMSSW_5_3_18",  ""},
  {1,  8,  2000,  1600,  1,  0,  10000,  0.00354,  "WR2000_nuRe1600",  0,  "5_3_18-START53_V15A-WR2000_nuRe1600.root",  "START53_V15A::All",  "GRun",  "CMSSW_5_3_18",  ""},
  {1,  8,  2000,  1800,  1,  0,  10000,  0.00108,  "WR2000_nuRe1800",  0,  "5_3_18-START53_V15A-WR2000_nuRe1800.root",  "START53_V15A::All",  "GRun",  "CMSSW_5_3_18",  ""},
  {1,  8,  2600,  100,  1,  0,  10000,  0.003589,  "WR2600_nuRe100",  0,  "5_3_18-START53_V15A-WR2600_nuRe100.root",  "START53_V15A::All",  "GRun",  "CMSSW_5_3_18",  ""},
  {1,  8,  2600,  200,  1,  0,  10000,  0.003337,  "WR2600_nuRe200",  0,  "5_3_18-START53_V15A-WR2600_nuRe200.root",  "START53_V15A::All",  "GRun",  "CMSSW_5_3_18",  ""},
  {1,  8,  2600,  400,  1,  0,  10000,  0.002917,  "WR2600_nuRe400",  0,  "5_3_18-START53_V15A-WR2600_nuRe400.root",  "START53_V15A::All",  "GRun",  "CMSSW_5_3_18",  ""},
  {1,  8,  2600,  600,  1,  0,  10000,  0.002625,  "WR2600_nuRe600",  0,  "5_3_18-START53_V15A-WR2600_nuRe600.root",  "START53_V15A::All",  "GRun",  "CMSSW_5_3_18",  ""},
  {1,  8,  2600,  800,  1,  0,  10000,  0.002337,  "WR2600_nuRe800",  0,  "5_3_18-START53_V15A-WR2600_nuRe800.root",  "START53_V15A::All",  "GRun",  "CMSSW_5_3_18",  ""},
  {1,  8,  2600,  1000,  1,  0,  10000,  0.002051,  "WR2600_nuRe1000",  0,  "5_3_18-START53_V15A-WR2600_nuRe1000.root",  "START53_V15A::All",  "GRun",  "CMSSW_5_3_18",  ""},
  {1,  8,  2600,  1200,  1,  0,  10000,  0.001758,  "WR2600_nuRe1200",  0,  "5_3_18-START53_V15A-WR2600_nuRe1200.root",  "START53_V15A::All",  "GRun",  "CMSSW_5_3_18",  ""},
  {1,  8,  2600,  1400,  1,  0,  10000,  0.001457,  "WR2600_nuRe1400",  0,  "5_3_18-START53_V15A-WR2600_nuRe1400.root",  "START53_V15A::All",  "GRun",  "CMSSW_5_3_18",  ""},
  {1,  8,  2600,  1600,  1,  0,  10000,  0.001143,  "WR2600_nuRe1600",  0,  "5_3_18-START53_V15A-WR2600_nuRe1600.root",  "START53_V15A::All",  "GRun",  "CMSSW_5_3_18",  ""},
  {1,  8,  2600,  1800,  1,  0,  10000,  0.0008225,  "WR2600_nuRe1800",  0,  "5_3_18-START53_V15A-WR2600_nuRe1800.root",  "START53_V15A::All",  "GRun",  "CMSSW_5_3_18",  ""},
  {1,  8,  2600,  2000,  1,  0,  10000,  0.0005236,  "WR2600_nuRe2000",  0,  "5_3_18-START53_V15A-WR2600_nuRe2000.root",  "START53_V15A::All",  "GRun",  "CMSSW_5_3_18",  ""},
  {1,  8,  2600,  2200,  1,  0,  9800,  0.0002611,  "WR2600_nuRe2200",  0,  "5_3_18-START53_V15A-WR2600_nuRe2200.root",  "START53_V15A::All",  "GRun",  "CMSSW_5_3_18",  ""},
  {1,  8,  2600,  2400,  1,  0,  10000,  0.00007761,  "WR2600_nuRe2400",  0,  "5_3_18-START53_V15A-WR2600_nuRe2400.root",  "START53_V15A::All",  "GRun",  "CMSSW_5_3_18",  ""},
};

// macro for array length calculation
#define DIM(a) (sizeof(a)/sizeof(a[0]))

// class for samples array managing
class SampleDB {
public:
  SampleDB()
  : info("All Samples"),
    samples(AllSamples, AllSamples + DIM(AllSamples))
  {
    recalcSignalsCS();
    
    // TODO: add a procedure to check samples
    // for example, all file names have to be uniq
  }
  
  // return i-th sample
  Sample operator [] (int i) const {return samples[i];}
  
  // number of samples in database
  int size() const {return samples.size();}
  
  // select samples for which "par" == "val"
  template<typename T>
  SampleDB find(const std::string par, const T val) const
  {
    std::vector<Sample> s;
    
    for (size_t i = 0; i < samples.size(); ++i)
      if (samples[i].check(par, val))
        s.push_back(samples[i]);
    
    return SampleDB(info + " / " + par + " = " + toString(val), s);
  }
  
  // union operation
  SampleDB operator + (const SampleDB& db) const
  {
    std::vector<Sample> s(samples);
    
    for (int i = 0; i < db.size(); ++i)
      if (std::find(s.begin(), s.end(), db[i]) == s.end())
        s.push_back(db[i]);
    
    return SampleDB("(" + info + ") + (" + db.info + ")", s);
  }
  
  std::string info;

private:
  SampleDB(const std::string i, const std::vector<Sample> s)
  : info(i), samples(s)
  {}
  
  std::vector<Sample> samples;
  
  // recalculate cross sections of all signal samples according to k-factor
  void recalcSignalsCS()
  {
    for (size_t i = 0; i < samples.size(); ++i) {
      Sample& s = samples[i];
      
      if (s.type == 1) {
        const size_t j = s.MW / 100;
        
        if (j < DIM(sigk)) s.CS *= sigk[j];
        else {
          std::cerr << "ERROR: can't find k-factor for M_WR = " << s.MW << "\n"
                    << "       edit sampledb.h to add missing k-factor\n";
        }
      }
    }
  }
  
  // convert to string
  template<typename T>
  std::string toString(const T x) const
  {
    std::ostringstream ss;
    ss << x;
    return ss.str();
  }
  
  // overload of << operator to allow easy printing of SampleDB
  friend std::ostream& operator<<(std::ostream& os, const SampleDB& db)
  {
    os << "[SampleDB]: " << db.info << ", total = " << db.size() << " samples\n";
    
    for (int i = 0; i < db.size(); ++i) {
      const Sample s = db[i];
      os << "  " << i << ": "
           << s.name << "  " << s.fname << "\n";
    }
    
    return os;
  }
};

#endif
