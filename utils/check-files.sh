#!/bin/bash

# scan heavynu data files to detect file integrity problems
# correct file shoud have one 'data' TTree and one (optional) 'hlt' TTree

# to be run on the pcrfcms14 machine

find /moscow41/heavynu -name '*.root' -a ! -name 'reco.root' | while read f ; do
  echo -n "."
  text=$(echo -e '_file0->cd("patdump");\n_file0->ls()\n.q\n' | root -b -l "$f" 2>/dev/null | grep TTree)
  ntot=$(echo "$text" | wc -l)
  ndata=$(echo "$text" | grep data | wc -l)
  nhlt=$(echo "$text" | grep hlt | wc -l)
  good=0
  if [[ "$ntot" == "2" && "$ndata" == "1" && "$nhlt" == "1" ]] ; then
    good=1
  fi
  if [[ "$ntot" == "1" && "$ndata" == "1" && "$nhlt" == "0" ]] ; then
    good=1
  fi
  
  if [[ "$good" != "1" ]] ; then
    echo
    echo $f
    echo $ntot $ndata $nhlt
    echo "$text"
  fi
done
