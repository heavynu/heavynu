// This script intended to convert root trees with old format (of CMSSW_1_6)
// to new format (of CMSSW_3_x)
// Usage:
//   root -b -q -l 'convert16x.cxx+("old.root", "hlt-map.txt", "new.root")'

// heavynu
#include "../PATDump/src/format.h"

// ROOT
#include "TFile.h"
#include "TTree.h"
#include "Math/Vector3D.h"
using ROOT::Math::XYZVector;
using ROOT::Math::XYZTVector;
using ROOT::Math::XYZPoint;

// c++
#include <iostream>
#include <vector>
#include <fstream>
using namespace std;

// convert old dump format (age of CMSSW_1_6)
// to new one (age of CMSSW_2_x)
void convert16x(const char* inName,   // input .root file
                const char* hltName,  // text file with HLT trigger names
                const char* outName)  // output .root file
{
  // --- input tree ---
  cout << "Opening input tree: " << inName << " ..." << endl;
  
  TFile inFile(inName);
  if (!inFile.IsOpen()) {
    cerr << "ERROR: can't open input file\n"
         << "       inName = " << inName << "\n"
         << endl;
    return;
  }
  
  TTree* inTree = (TTree*) inFile.Get("heavynu");
  if (!inTree) {
    cerr << "ERROR: can't find tree 'heavynu' in input file\n"
         << "       inName = " << inName << "\n"
         << endl;
    return;
  }
  
  cout << inName << ": " << inTree->GetEntries() << " entries" << endl;
  
  // event specific
  vector<int>* l1Bits = new vector<int>;
  vector<int>* hltBits = new vector<int>;
  float weight;
  int pid;
  int n;
  XYZVector* mcMET = new XYZVector;
  XYZVector* caloMET = new XYZVector;;
  XYZVector* recoilMET = new XYZVector;
  
  // particles specific
  enum {MAX = 50};
  float px[MAX];
  float py[MAX];
  float pz[MAX];
  float E[MAX];
  float isoValue[MAX];
  float etEm[MAX];
  float etHad[MAX];
  int type[MAX];
  int validHits[MAX];
  float normChi2[MAX];
  int charge[MAX];
  vector<XYZVector>* vertex = new vector<XYZVector>;
  int numTracks[MAX];
  int lostHits[MAX];
  float chi2[MAX];
  int mcPDG[MAX];
  
  // bind tree branches to variables
  inTree->SetBranchAddress("l1Bits", &l1Bits); // not used
  inTree->SetBranchAddress("hltBits", &hltBits);
  inTree->SetBranchAddress("weight", &weight);
  inTree->SetBranchAddress("pid", &pid);
  inTree->SetBranchAddress("n", &n);
  inTree->SetBranchAddress("mcMET", &mcMET);
  inTree->SetBranchAddress("caloMET", &caloMET);
  inTree->SetBranchAddress("recoilMET", &recoilMET); // not used
  
  inTree->SetBranchAddress("px", px);
  inTree->SetBranchAddress("py", py);
  inTree->SetBranchAddress("pz", pz);
  inTree->SetBranchAddress("E", E);
  inTree->SetBranchAddress("isoValue", isoValue);
  inTree->SetBranchAddress("etEm", etEm);
  inTree->SetBranchAddress("etHad", etHad);
  inTree->SetBranchAddress("type", type);
  inTree->SetBranchAddress("validHits", validHits);
  inTree->SetBranchAddress("normChi2", normChi2);
  inTree->SetBranchAddress("charge", charge);
  inTree->SetBranchAddress("vertex", &vertex);
  inTree->SetBranchAddress("numTracks", numTracks);
  inTree->SetBranchAddress("lostHits", lostHits);
  inTree->SetBranchAddress("chi2", chi2);
  inTree->SetBranchAddress("mcPDG", mcPDG);
  
  // --- output tree ---
  TFile outFile(outName, "RECREATE");
  outFile.mkdir("patdump")->cd();
  TTree* outTree = new TTree("data", "data");
  
  TheEvent evt;
  evt.book(outTree);
  
  // get size of HLT bits array
  inTree->GetEntry(0);
  const int sizeHLT = hltBits->size();
  
  // --- loop over input tree ---
  const int N = inTree->GetEntries();
  
  for (int i = 0; i < inTree->GetEntries(); ++i) {
    // print dot each 5%
    if (i % (N / 20) == 0) cout << "." << flush;
    
    inTree->GetEntry(i);
    
    evt.clear();
    
    // weight and process_id
    evt.pid = pid;
    evt.weight = weight;
  
    // Event and Run numbers
    evt.irun   = 0;
    evt.ievent = i;
    evt.ilumi  = 0;
    
    if (int(hltBits->size()) != sizeHLT) {
      cerr << "ERROR: incorrect HLT bits size\n"
           << "         event = " << i << "\n"
           << "         size = " << hltBits->size() << "\n"
           << "         correct size = " << sizeHLT << "\n"
           << endl;
      return;
    }
    
    // hlt trigger bits
    for (size_t j = 0; j < hltBits->size(); ++j)
      evt.hltBits->push_back(hltBits->at(j));
    
    // particles: electrons, muons, jets
    for (int j = 0; j < n; ++j) {
    
      int mcId = -1;
      if (mcPDG[j] != 0) {
        
        // resolve charge by PDG ID code
        int charge_ = 0;
        switch (mcPDG[j]) {
          case 11:
          case 13: charge_ = -1; break;
          case -11:
          case -13: charge_ = 1; break;
        }
        
        mcId = evt.push(TheEvent::GEN,
                        mcPDG[j],
                        -10,
                        XYZTVector(),
                        XYZPoint(),
                        charge_);
      }
      
      int type_;
      switch (type[j]) {
        case 10: type_ = TheEvent::ELECTRON; break; // electron
        case 20: type_ = TheEvent::MUON; break; // muon
        case 50: // jet
        case 60: type_ = TheEvent::JET; break; // b-jet
        default:
          cerr << "ERROR: unknow type = " << type[j] << endl;
          return;
      }
      
      int pdgid = 0;
      if (type[j] == 10) { // electron
        if (charge[j] < 0) pdgid = 11;       // e-
        else if (charge[j] > 0) pdgid = -11; // e+
      }
      else if (type[j] == 20) { // muon
        if (charge[j] < 0) pdgid = 13;       // mu-
        else if (charge[j] > 0) pdgid = -13; // mu+
      }
      
      const XYZTVector p4(px[j], py[j], pz[j], E[j]);
      
      // NOTE: in old trees we have only relative isolation field, which is calculated as:
      //         relIso = (0.75*ecalIso + 0.75*hcalIso + trackIso)/pt
      //       to fill fields ecalIso, hcalIso, caloIso, trackIso in new tree
      //       we assume ecalIso = hcalIso = trackIso = caloIso/2
      //       that is why this fields themself don't have any meaning
      //       So, use only Particle::iso() which will return the original relIso
      const float iso = isoValue[j] * p4.pt() / 2.5;
      
      const int ndof = (type_ != TheEvent::JET) ? int(chi2[j]/normChi2[j] + 0.5) : 0;
      
      evt.push(type_, pdgid, mcId,
               p4, XYZPoint((*vertex)[j]), charge[j],
               iso, iso, iso, 2*iso,
               chi2[j], ndof,
               validHits[j], lostHits[j],
               etEm[j]/(etEm[j] + etHad[j]), numTracks[j]);
    }
    
    // METs
    // TODO: check conversion XYZVector -> XYZTVector
    const int mcId = evt.push(TheEvent::GEN, 0, -10,
                      XYZTVector(mcMET->x(), mcMET->y(), mcMET->z(), mcMET->r()), XYZPoint(), 0);
    
    evt.push(TheEvent::MET, 0, mcId,
             XYZTVector(caloMET->x(), caloMET->y(), caloMET->z(), caloMET->r()), XYZPoint(), 0);
    
    // Fill the tree
    outTree->Fill();
  }
  
  outTree->Write();
  
  cout << endl;
  
  // --- output HLT trigger names ---
  ifstream hltFile(hltName);
  if (!hltFile) {
    cerr << "ERROR: can't open HLT names file\n"
         << "       hltName = " << hltName << "\n"
         << endl;
    return;
  }
  
  // skip first two lines:
  string line;
  getline(hltFile, line);
  getline(hltFile, line);
  
  TObjArray* hltNames = new TObjArray(sizeHLT);
  while (!hltFile.eof()) {
    int bit;
    string name;
    hltFile >> bit >> name;
    
    if (name != "")
      hltNames->Add(new TObjString(name.c_str()));
  }
  
  if (hltFile.bad()) {
    cerr << "ERROR: error reading HLT map file\n"
         << endl;
    return;
  }
  
  if (sizeHLT != hltNames->GetEntries()) {
    cerr << "ERROR: number of HLT bits in input tree not equal to number of bits in map file\n"
         << "         input tree bits = " << sizeHLT << "\n"
         << "         map file bits = " << hltNames->GetEntries() << "\n"
         << endl;
    return;
  }
  
  hltNames->Write("hltNames", TObject::kSingleKey);
}
