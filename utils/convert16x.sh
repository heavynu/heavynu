#!/bin/sh

srcDir=/moscow31/heavynu/16x
dstDir=/moscow31/heavynu/16x/new

echo "Source = $srcDir"
echo "Destination = $dstDir"
echo

if [ ! -d $srcDir ] ; then
  echo "ERROR: can't find source directory"
  echo "       srcDir = $srcDir"
  echo "       login to pcrfcms10 first"
  exit 1
fi

mkdir -p $dstDir

for i in $srcDir/*.root ; do
  echo "Processing $i ..."
  
  fname=$(basename $i)
  if [[ "$(echo $fname | cut -d - -f 1)" == "new" ]] ; then
    echo "This is new format file, skipped."
    continue
  fi
  
  outFile=$dstDir/new-$fname
  root -b -q -l "convert16x.cxx+(\"$i\", \"$srcDir/16x-HLT_1e32.txt\", \"$outFile\")"
  if [[ "$?" != "0" ]] ; then
    echo "ERROR: conversion error"
    exit 1
  fi
done
