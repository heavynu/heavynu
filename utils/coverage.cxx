// This script do a plot which shows current
// coverages of mass space (MW - MNu) by our signal samples.
// To prepare plot do:
//   $ root -b -q -l coverage.cxx+

#include "../sampledb.h"

#include "TCanvas.h"
#include "TGraph.h"
#include "TMultiGraph.h"
#include "TLegend.h"
#include "TStyle.h"

#include <list>
#include <vector>
#include <sstream>
using namespace std;

// prepare list of available ECM
vector<int> uniqECM(const SampleDB& db)
{
  list<int> ecm;
  for (int i = 0; i < db.size(); ++i)
    ecm.push_back(db[i].ECM);
  ecm.sort();
  ecm.unique();
  
  return vector<int>(ecm.begin(), ecm.end());
}

// conver number to string
string toString(int x)
{
  ostringstream ss;
  ss << x;
  return ss.str();
}

void coverage()
{
  // find all signal samples
  const SampleDB sig = SampleDB().find("type", 1);
  
  const vector<int> ecm = uniqECM(sig);
  TMultiGraph mg("mg", "Signal samples mass coverage;M_{W} [GeV/c^{2}];M_{N} [GeV/c^{2}]");
  TLegend leg(0.8, 0.8, 0.95, 0.95);
  
  for (size_t i = 0; i < ecm.size(); ++i) {
    const SampleDB db = sig.find("ECM", ecm[i]);
    
    TGraph* g = new TGraph(db.size());
    g->SetMarkerStyle(kCircle);
    g->SetMarkerColor(i + 1);
    
    const float pi = 3.141592;
    const float a = pi/4 + 2*pi*i/ecm.size();
    const float dx = 10*cos(a);
    const float dy = 10*sin(a);
    
    for (int j = 0; j < db.size(); ++j)
      g->SetPoint(j, db[j].MW + dx, db[j].MNu + dy);
    
    mg.Add(g);
    leg.AddEntry(g, (toString(ecm[i]) + " TeV").c_str(), "p");
  }
  
  // do a draw
  gStyle->SetPaperSize(20, 15);
  gStyle->SetTitleOffset(1.2, "y");
  TCanvas c("coverage", "coverage", 800, 600);
  mg.Draw("AP");
  leg.Draw();
  c.Print(".eps");
}
