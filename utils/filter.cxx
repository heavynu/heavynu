#include "PATDump/src/format.h"
#include <iostream>
using namespace std;

struct HLTTester {
  HLTTester(const vector<string>& names, // array of all HLT bits names
            const char* hlt[],           // array of HLT bits names we want to select
            const int nhlt)              // size of hlt[]
  {
    // resolve hlt[] bits names to bits numbers
    for (int i = 0; i < nhlt; ++i)
      for (size_t j = 0; j < names.size(); ++j)
        if (hlt[i] == names[j]) {
          bits.push_back(j);
          cout << names[j] << " = " << j << endl;
        }
  }
  
  // check, does the event has at least one of hlt[] bits active
  bool pass(const TheEvent& evt)
  {
    for (size_t i = 0; i < bits.size(); ++i)
      if ((*evt.hltBits)[bits[i]]) {
        cout << evt.triggerNames[bits[i]] << endl;
        return true;
      }
    
    return false;
  }
  
private:
  vector<int> bits;
};

void filter(const char* fname = "heavynu.root")
{
  TheEvent e(fname);
  if (!e) return;
  
  const char* hlt[] = {"HLT_Mu9",                // SD_Mu9
                       "HLT_Ele15_LW_L1R",       // SD_Ele15
                       "HLT_Ele15_SC10_LW_L1R",  //   -/-
                       "HLT_Ele20_LW_L1R"};      //   -/-
  
  #define DIM(x) sizeof(x)/sizeof(*x)
  HLTTester tester(e.triggerNames, hlt, DIM(hlt));
  
  // total events:
  const int nEvents = e.totalEvents();
  cout << "Total events: " << nEvents << endl;
  
  // number of events which pass hlt skim
  int nSkim = 0;

  for (int i = 0; i < nEvents; ++i) {
    e.readEvent(i);
    
    if (tester.pass(e))
      nSkim++;
  }
  
  cout << "Skim events: " << nSkim << endl;
}
