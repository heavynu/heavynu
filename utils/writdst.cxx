#include "PATDump/src/format.h"

#include <fstream>
using namespace std;

void writdst()
{
  TheEvent evt("heavynu.root");
  if (!evt) return;
  cout << "Total events: " << evt.totalEvents() << endl;
  
  ofstream mkdst("mkdst.d");

  int noutev=0;

  for (int j = 0; j < evt.totalEvents(); ++j) {
    evt.readEvent(j);
    if(j < 5)evt.print();

    unsigned int k;
    int mcid;
    float dum1=0.;

    if( evt.electrons.size()+evt.muons.size() < 2 ) continue;
    if( evt.jets.size() < 2 ) continue;

//    if( !(*(evt.hltBits))[29] ) continue;  // Check trigger ---

    noutev++;

    mkdst << j << " event" << endl;
    mkdst << evt.jets.size() << " " << evt.weight << " " << evt.pid << endl;
    for (k = 0; k < evt.jets.size(); k++) {
        mkdst << dum1 << " "
              << evt.jets[k].p4.px() << " " << evt.jets[k].p4.py() << " "
              << evt.jets[k].p4.pz() << " " << evt.jets[k].p4.e() << " "
              << dum1 << endl;
    }

    mkdst << evt.electrons.size()+evt.muons.size() << " leptons" << endl;

    for (k = 0; k < evt.electrons.size(); k++) {
      mcid=0;
      if(evt.electrons[k].mcId > -1) mcid = evt.gens[evt.electrons[k].mcId].id;
      mkdst << evt.electrons[k].charge << " "
            << evt.electrons[k].p4.px() << " "
            << evt.electrons[k].p4.py() << " "
            << evt.electrons[k].p4.pz() << " "
            << evt.electrons[k].p4.e() << " "
            << dum1 << " " << mcid << " "
            << evt.electrons[k].validHits << " "
            << evt.electrons[k].chi2 << " "
            << evt.electrons[k].iso() << " " << endl;
    }

    for (k = 0; k < evt.muons.size(); k++) {
      mcid=0;
      if(evt.muons[k].mcId > -1) mcid = evt.gens[evt.muons[k].mcId].id;
      mkdst << 2*evt.muons[k].charge << " "
            << evt.muons[k].p4.px() << " "
            << evt.muons[k].p4.py() << " "
            << evt.muons[k].p4.pz() << " "
            << evt.muons[k].p4.e() << " "
            << dum1 << " " << mcid << " "
            << evt.muons[k].validHits << " "
            << evt.muons[k].chi2 << " "
            << evt.muons[k].iso() << " " << endl;
    }

    mkdst << evt.mets[0].p4.px() << " " << evt.mets[0].p4.py() << " "
          << dum1 << " " << dum1 << endl;

  }

  cout << "Number of events written to dst: " << noutev << endl;

}
